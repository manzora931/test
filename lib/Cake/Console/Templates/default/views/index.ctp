<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="<?php echo $pluralVar; ?> index">
	<h2><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h2>
	<p>
		<?php echo "<?php
	echo \$this->paginator->counter(array(
		'format' => __('P�gina {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));"; ?>
	?></p>

	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = $pluralVar;
	?>
	<div id='search_box'>
		<?php echo "
		<?php
			if(isset(\$_SESSION['$tabla']))
			{
			\$real_url = 'http://'.\$_SERVER['HTTP_HOST'].\$this->request->base.'/'.\$this->params->controller.'/';
			echo \$this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \\''.\$real_url.'vertodos\\''));
			}
		?>"; ?>
	</div>
	<table cellpadding="0" cellspacing="0">
	<tr>
	<?php foreach ($fields as $field): ?>
		<th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
	<?php endforeach; ?>
		<th class="actions"><?php echo "<?php echo __('Acciones'); ?>"; ?></th>
	</tr>
	<?php
	echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
	echo "\t<tr>\n";
		foreach ($fields as $field) {
			$isKey = false;
			if (!empty($associations['belongsTo'])) {
				foreach ($associations['belongsTo'] as $alias => $details) {
					if ($field === $details['foreignKey']) {
						$isKey = true;
						echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
						break;
					}
				}
			}
			if ($isKey !== true) {
				echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
			}
		}
?>

	<td class="actions">
		<?php echo "<?php echo \$this->Html->link(__(' ', true), array('action'=>'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('class'=>'ver')); ?>"; ?>
		<?php echo "<?php echo \$this->Html->link(__(' ', true), array('action'=>'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('class'=>'editar')); ?>"; ?>
		</td>
<?php
	echo "<?php endforeach; ?>\n";
	?>
	</table>
	<div class="paging">
		<?php echo "
		<?php echo \$this->Paginator->prev('<< '.__('previo', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo \$this->Paginator->numbers();?>
		<?php echo \$this->Paginator->next(__('siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
		"; ?>
	</div>
</div>

<div class="actions">
	<ul>
		<li><?php echo "<?php echo \$this->Html->link(__('Nuevo $pluralHumanName'), array('action' => 'add')); ?>"; ?></li>
	</ul>
</div>
