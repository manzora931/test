<?php
App::uses('AppModel', 'Model');
/**
 * Torneo Model
 *
 * @property Tipotorneo $Tipotorneo
 * @property Paise $Paise
 * @property Juego $Juego
 * @property Jugadoresxequipo $Jugadoresxequipo
 */
class Torneo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombrecorto';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Tipotorneo' => array(
            'className' => 'Tipotorneo',
            'foreignKey' => 'tipotorneo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Paise' => array(
            'className' => 'Paise',
            'foreignKey' => 'paise_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Juego' => array(
            'className' => 'Juego',
            'foreignKey' => 'torneo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Jugadoresxequipo' => array(
            'className' => 'Jugadoresxequipo',
            'foreignKey' => 'torneo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Gruposxtorneo' => array(
            'className' => 'Gruposxtorneo',
            'foreignKey' => 'torneo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
