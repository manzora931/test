<?php
App::uses('AppModel', 'Model');
/**
 * Detevento Model
 *
 * @property Evento $Evento
 * @property Juegoevento $Juegoevento
 */
class Detevento extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'detevento';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Evento' => array(
			'className' => 'Evento',
			'foreignKey' => 'evento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Juegoevento' => array(
			'className' => 'Juegoevento',
			'foreignKey' => 'detevento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
