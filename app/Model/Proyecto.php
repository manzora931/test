<?php
App::uses('AppModel', 'Model');
/**
 * Proyecto Model
 *
 * @property Tipoproyecto $Tipoproyecto
 * @property Paise $Paise
 * @property Estado $Estado
 * @property Fuentesfinanciamiento $Fuentesfinanciamiento
 * @property Module $Module
 */
class Proyecto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombrecorto';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Tipoproyecto' => array(
            'className' => 'Tipoproyecto',
            'foreignKey' => 'tipoproyecto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Paise' => array(
            'className' => 'Paise',
            'foreignKey' => 'paise_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Estado' => array(
            'className' => 'Estado',
            'foreignKey' => 'estado_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

        'Institucion' => array(
            'className' => 'Institucion',
            'foreignKey' => 'institucion_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Fuentesfinanciamiento' => array(
            'className' => 'Fuentesfinanciamiento',
            'foreignKey' => 'proyecto_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Module' => array(
            'className' => 'Module',
            'foreignKey' => 'proyecto_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
