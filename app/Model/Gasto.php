<?php
App::uses('AppModel', 'Model');
/**
 * Gasto Model
 *
 * @property Catgasto $Catgasto
 * @property Actividade $Actividade
 */
class Gasto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'catgasto_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Catgasto' => array(
			'className' => 'Catgasto',
			'foreignKey' => 'catgasto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actividade' => array(
			'className' => 'Actividade',
			'foreignKey' => 'actividade_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
