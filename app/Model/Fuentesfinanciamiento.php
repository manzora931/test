<?php
App::uses('AppModel', 'Model');
/**
 * Fuentesfinanciamiento Model
 *
 * @property Financista $Financista
 * @property Proyecto $Proyecto
 * @property Actividade $Actividade
 */
class Fuentesfinanciamiento extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'financista_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Financista' => array(
			'className' => 'Financista',
			'foreignKey' => 'financista_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'proyecto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Actividade' => array(
			'className' => 'Actividade',
			'foreignKey' => 'fuentesfinanciamiento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
