<?php
App::uses('AppModel', 'Model');
/**
 * Incidencia Model
 *
 * @property Actividade $Actividade
 * @property Generoincidencia $Generoincidencia
 * @property Rangoedadincidencia $Rangoedadincidencia
 */
class Incidencia extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'actividade_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Actividade' => array(
			'className' => 'Actividade',
			'foreignKey' => 'actividade_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Generoincidencia' => array(
			'className' => 'Generoincidencia',
			'foreignKey' => 'incidencia_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Rangoedadincidencia' => array(
			'className' => 'Rangoedadincidencia',
			'foreignKey' => 'incidencia_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
