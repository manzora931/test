<?php
App::uses('AppModel', 'Model');
/**
 * Transicion Model
 *
 * @property Torneo $Torneo
 * @property Estadio $Estadio
 * @property Etapa $Etapa
 * @property Grupo $Grupo
 * @property Equipo $Equipo1
 * @property Equipo $Equipo2
 * @property Juegnota $Juegnota
 * @property Juegoevento $Juegoevento
 * @property Juegoalineacione $Juegoalineacione
 */
class Juego extends AppModel {

/**
 * Use database config
 *
 * @var string
 */

/**
 * Display field
 *
 * @var string
 */
	// public $displayField = 'transicion';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estadio' => array(
			'className' => 'Estadio',
			'foreignKey' => 'estadio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Etapa' => array(
			'className' => 'Etapa',
			'foreignKey' => 'etapa_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Grupo' => array(
			'className' => 'Grupo',
			'foreignKey' => 'grupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Equipo1' => array(
			'className' => 'Equipo',
			'foreignKey' => 'equipo1_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Equipo2' => array(
			'className' => 'Equipo',
			'foreignKey' => 'equipo2_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Juegnota' => array(
			'className' => 'Juegnota',
			'foreignKey' => 'juego_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Juegoevento' => array(
			'className' => 'Juegoevento',
			'foreignKey' => 'juego_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Juegoalineacione' => array(
			'className' => 'Juegoalineacione',
			'foreignKey' => 'juego_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
