<?php
App::uses('AppModel', 'Model');
/**
 * Detrecurso Model
 *
 */
class Detrecurso extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';
    var $belongsTo = array(
        'Recurso' => array('className' => 'Recurso',
            'foreignKey' => 'recurso_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
}
