<?php
App::uses('AppModel', 'Model');
/**
 * Etapasxtorneo Model
 *
 * @property Torneo $Torneo
 * @property Etapa $Etapa
 */
class Etapasxtorneo extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Etapa' => array(
			'className' => 'Etapa',
			'foreignKey' => 'etapa_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
