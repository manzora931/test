<?php
App::uses('AppModel', 'Model');
/**
 * Logproyecto Model
 *
 * @property Proyecto $Proyecto
 */
class Logproyecto extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'proyecto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

    public function registro_bitacora($modelo = null, $tabla = null, $pantalla = null) {
        $data = $_SESSION['logsproyecto'];
        $ip  =  (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']))?$_SERVER['REMOTE_ADDR']:'';
        $json = [];

        if($data['proyecto_id'] != '') {
            $new_bitacora['Logproyecto']['proyecto_id'] = $data['proyecto_id'];
            $new_bitacora['Logproyecto']['ip'] = $ip;
            $new_bitacora['Logproyecto']['created'] = date('Y-m-d H:i:s');
            $new_bitacora['Logproyecto']['usuario'] = AuthComponent::user('username');

            $json['accion'] = $data['accion'];
            $json['modelo'] = $modelo;
            $json['tabla'] = $tabla;
            $json['pantalla'] = $pantalla;

            unset($data['data']['created']);
            unset($data['data']['usuario']);
            unset($data['data']['modified']);
            unset($data['data']['usuariomodif']);

            switch ($json['accion']) {
                case "Agregar":
                    $json['data'] = $data['data'];
                    $log = json_encode($json);
                    $new_bitacora['Logproyecto']['log'] = $log;
                    break;
                case "Modificar":
                    $mod = [];
                    foreach ($data['data'] as $key => $row) {
                        if(isset($data['anterior'][$modelo][$key])) {
                            if($data['anterior'][$modelo][$key] != $row){
                                array_push($mod, $key);
                            }
                        }
                    }

                    $json['campos'] = $mod;
                    $json['data'] = $data['data'];
                    $log = json_encode($json);
                    $new_bitacora['Logproyecto']['log'] = $log;
                    break;
                case "Eliminar":
                    $json['data'] = $data['data'];
                    $log = json_encode($json);
                    $new_bitacora['Logproyecto']['log'] = $log;
                    break;
            }

            $this->create();
            $this->save($new_bitacora);
        }

        unset( $_SESSION['logsproyecto'] );
    }
}
