<?php
App::uses('AppModel', 'Model');
/**
 * Destinatario Model
 *
 * @property Contacto $Contacto
 * @property Desembolso $Desembolso
 */
class Destinatario extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'contacto_id';

    // Campo virtual que almacena el nombre completo del contacto
    public $virtualFields = array('nombre' => 'concat(Contacto.nombres, " ", Contacto.apellidos)');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Contacto' => array(
			'className' => 'Contacto',
			'foreignKey' => 'contacto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Desembolso' => array(
			'className' => 'Desembolso',
			'foreignKey' => 'destinatario_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
