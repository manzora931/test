<?php
App::uses('AppModel', 'Model');
/**
 * Trasladofondo Model
 *
 * @property Actividade1 $Actividade1
 * @property Actividade2 $Actividade2
 * @property Proyecto $Proyecto
 */
class Trasladofondo extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Actividade1' => array(
			'className' => 'Actividade1',
			'foreignKey' => 'actividade1_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actividade2' => array(
			'className' => 'Actividade2',
			'foreignKey' => 'actividade2_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'proyecto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
