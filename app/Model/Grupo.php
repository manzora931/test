<?php
App::uses('AppModel', 'Model');
/**
 * Tipotorneo Model
 *
 * @property Juego $Juego
 */
class Grupo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'grupo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Juego' => array(
			'className' => 'Juego',
			'foreignKey' => 'grupo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Gruposxtorneo' => array(
            'className' => 'Gruposxtorneo',
            'foreignKey' => 'grupo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);

}
