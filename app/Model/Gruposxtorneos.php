<?php
App::uses('AppModel', 'Model');
/**
 * Gruposxtorneos Model
 *
 * @property Torneo $Torneo
 * @property Grupo $Grupo
 */
class Gruposxtorneos extends AppModel {
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Grupo' => array(
			'className' => 'Grupo',
			'foreignKey' => 'grupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
