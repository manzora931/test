<?php
App::uses('AppModel', 'Model');
/**
 * Organizacion Model
 *
 * @property Paise $Paise
 * @property Institucion $Institucion
 * @property Modulo $Modulo
 * @property Recurso $Recurso
 */
class Organizacion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'organizacion';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
        ,'Institucion' => array(
            'className' => 'Institucion',
            'foreignKey' => 'institucion_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'organizacion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Modulo' => array(
			'className' => 'Modulo',
			'foreignKey' => 'organizacion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Recurso' => array(
			'className' => 'Recurso',
			'foreignKey' => 'organizacion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    public function getConfig ($key = NULL)
    {
        $this->id = 1;
        $config = json_decode($this->field('config'), true);

        return $config[$key];
    }
}
