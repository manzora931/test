<?php
App::uses('AppModel', 'Model');
/**
 * Rangoedade Model
 *
 */
class Rangoedade extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';

}
