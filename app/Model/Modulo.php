<?php
App::uses('AppModel', 'Model');
/**
 * Modulo Model
 *
 * @property Organizacion $Organizacion
 * @property Recurso $Recurso
 */
class Modulo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'modulo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
/*	public $validate = array(
		'modulo' => array(
			'notEmpty' => array(
				'rule' => array('isUnique'),
				'message' => 'Modulo duplicado. Intente ingresando un modulo diferente.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);*/



/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Recurso' => array(
			'className' => 'Recurso',
			'foreignKey' => 'modulo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    public $belongsTo = array(
        'Organizacion' => array(
            'className' => 'Organizacion',
            'foreignKey' => 'organizacion_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
