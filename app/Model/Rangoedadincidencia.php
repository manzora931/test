<?php
App::uses('AppModel', 'Model');
/**
 * Rangoedadincidencia Model
 *
 * @property Incidencia $Incidencia
 * @property Rangoedad $Rangoedad
 */
class Rangoedadincidencia extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Incidencia' => array(
			'className' => 'Incidencia',
			'foreignKey' => 'incidencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Rangoedade' => array(
			'className' => 'Rangoedade',
			'foreignKey' => 'rangoedad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
