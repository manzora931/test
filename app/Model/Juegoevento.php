<?php
App::uses('AppModel', 'Model');
/**
 * Jugadoresxequipo Model
 *
 * @property Juego $Juego
 * @property Evento $Evento
 * @property Detevento $Detevento
 */
class Juegoevento extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Juego' => array(
			'className' => 'Juego',
			'foreignKey' => 'juego_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Evento' => array(
			'className' => 'Evento',
			'foreignKey' => 'evento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Detevento' => array(
			'className' => 'Detevento',
			'foreignKey' => 'detevento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
