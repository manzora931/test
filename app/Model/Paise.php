<?php
App::uses('AppModel', 'Model');
/**
 * Paise Model
 *
 * @property Actividade $Actividade
 * @property Banco $Banco
 * @property Departamento $Departamento
 * @property Institucion $Institucion
 * @property Organizacion $Organizacion
 * @property Proyecto $Proyecto
 */
class Paise extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'pais';

    public $validate = array(
        'pais' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'este País ya existe',
                'allowEmpty' => true,
            )
        )
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Actividade' => array(
			'className' => 'Actividade',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Banco' => array(
			'className' => 'Banco',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Departamento' => array(
			'className' => 'Departamento',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Organizacion' => array(
			'className' => 'Organizacion',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'paise_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
