<?php
App::uses('AppModel', 'Model');
/**
 * Transicion Model
 *
 * @property Workflow $Workflow
 * @property Transicionnotificacion $Transicionnotificacion
 * @property Transicionpostfunction $Transicionpostfunction
 */
class Transicion extends AppModel {

/**
 * Use database config
 *
 * @var string
 */

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'transicion';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Workflow' => array(
			'className' => 'Workflow',
			'foreignKey' => 'workflow_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estado1' => array(
			'className' => 'Estado',
			'foreignKey' => 'estado1_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estado2' => array(
			'className' => 'Estado',
			'foreignKey' => 'estado2_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
