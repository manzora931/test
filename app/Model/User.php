<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new SimplePasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
}

    public $belongsTo = array(
        'Perfile' => array(
            'className' => 'Perfile',
            'foreignKey' => 'perfile_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
	    ),
        'Contacto' => [
            'className' => 'Contacto',
            'foreignKey' => 'contacto_id'
        ]
    );
    public $hasMany = array(
        'Detusuario' => array(
            'className' => 'Detusuario',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
}
