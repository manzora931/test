<?php
App::uses('AppModel', 'Model');
/**
 * Jugadoresxequipo Model
 *
 * @property Torneo $Torneo
 * @property Equipo $Equipo
 * @property Jugadore $Jugadore
 */
class Jugadoresxequipo extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Equipo' => array(
			'className' => 'Equipo',
			'foreignKey' => 'equipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Jugadore' => array(
			'className' => 'Jugadore',
			'foreignKey' => 'jugadore_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
