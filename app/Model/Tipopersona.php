<?php
App::uses('AppModel', 'Model');
/**
 * Tipopersona Model
 *
 * @property Persona $Persona
 */
class Tipopersona extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tipopersonas';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Persona' => array(
			'className' => 'Persona',
			'foreignKey' => 'tipopersona_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
