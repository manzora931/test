<?php
App::uses('AppModel', 'Model');
/**
 * Rubro Model
 *
 * @property Institucion $Institucion
 */
class Rubro extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'rubro';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'rubro_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
