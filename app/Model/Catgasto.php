<?php
App::uses('AppModel', 'Model');
/**
 * Catgasto Model
 *
 * @property Gasto $Gasto
 */
class Catgasto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Gasto' => array(
			'className' => 'Gasto',
			'foreignKey' => 'catgasto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
