<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 *
 * @property Proyecto $Proyecto
 */
class Document extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'documento';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'proyecto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
