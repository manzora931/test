<?php
App::uses('AppModel', 'Model');
/**
 * Estado Model
 *
 * @property Proyecto $Proyecto
 */
class Estado extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'estado';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'estado_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
