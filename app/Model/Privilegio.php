<?php
App::uses('AppModel', 'Model');
/**
 * Privilegio Model
 *
 */
class Privilegio extends AppModel
{
	var $belongsTo = array(
				'Perfile' => array('className' => 'Perfile',
									'foreignKey' => 'perfile_id',
									'conditions' => '',
									'fields' => '',
									'order' => ''
				),
				'Recurso' => array('className' => 'Recurso',
									'foreignKey' => 'recurso_id',
									'conditions' => '',
									'fields' => '',
									'order' => ''
				)
			);
    var $hasMany = array(
        'Detprivilegio' => array(
            'className' => 'Detprivilegio',
            'foreignKey' => 'privilegio_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );
}
