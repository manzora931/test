<?php
App::uses('AppModel', 'Model');
/**
 * Vequipotaquillero Model
 *
 */
class Vequipotaquillero extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'taquilla';

}
