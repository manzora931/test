<?php
App::uses('AppModel', 'Model');
/**
 * Persona Model
 *
 * @property Tipopersona $Tipopersona
 * @property Paise $Paise
 * @property Juego $Juego
 */
class Persona extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre_persona';

    // Campo virtual que almacena el nombre completo de la persona
    public $virtualFields = array('nombre_persona' => 'concat(Persona.nombre, " ", Persona.apellido)');

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Tipopersona' => array(
            'className' => 'Tipopersona',
            'foreignKey' => 'tipopersona_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Paise' => array(
            'className' => 'Paise',
            'foreignKey' => 'paise_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Juego' => array(
            'className' => 'Juego',
            'foreignKey' => 'referi',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
