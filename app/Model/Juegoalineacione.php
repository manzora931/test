<?php
App::uses('AppModel', 'Model');
/**
 * Jugadoresxequipo Model
 *
 * @property Juego $Juego
 * @property Equipo $Equipo
 * @property Posicione $Posicione
 * @property Jugadore $Jugadore
 */
class Juegoalineacione extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Juego' => array(
			'className' => 'Juego',
			'foreignKey' => 'juego_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Equipo' => array(
			'className' => 'Equipo',
			'foreignKey' => 'equipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Posicione' => array(
			'className' => 'Posicione',
			'foreignKey' => 'posicione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Jugadore' => array(
			'className' => 'Jugadore',
			'foreignKey' => 'jugadore_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipopersona' => array(
			'className' => 'Tipopersona',
			'foreignKey' => 'tipopersona_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Persona' => array(
			'className' => 'Persona',
			'foreignKey' => 'persona_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
