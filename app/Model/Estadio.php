<?php
App::uses('AppModel', 'Model');
/**
 * Estadio Model
 *
 * @property Paise $Paise
 * @property Juego $Juego
 */
class Estadio extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'estadio';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Juego' => array(
			'className' => 'Juego',
			'foreignKey' => 'estadio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
