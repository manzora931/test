<?php
App::uses('AppModel', 'Model');
/**
 * Vactividadgasto Model
 *
 * @property Institucion $Institucion
 */
class Vactividadgasto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombrecorto';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'institucion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
