<?php
App::uses('AppModel', 'Model');
/**
 * Tipoequipo Model
 *
 * @property Equipo $Equipo
 */
class Tipoequipo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tipoequipo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Equipo' => array(
			'className' => 'Equipo',
			'foreignKey' => 'tipoequipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
