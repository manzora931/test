<?php
App::uses('AppModel', 'Model');
/**
* Recurso Model
*
*/
class Recurso extends AppModel {
	/**
	* Display field
	*
	* @var string
	*/
	public $displayField = 'nombre';

	var $belongsTo = array(
		'Modulo' => array(
			'className' => 'Modulo',
			'foreignKey' => 'modulo_id'
		),
		'Organizacion' => [
			'className' => 'Organizacion',
			'foreignKey' => 'organizacion_id'
		]
	);

	var $hasMany = array(
		'Detrecurso' => array(
			'className' => 'Detrecurso',
			'foreignKey' => 'recurso_id'
		),
	);
}
