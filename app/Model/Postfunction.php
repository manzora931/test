<?php
App::uses('AppModel', 'Model');
/**
 * Postfunction Model
 *
 * @property Transicionpostfunction $Transicionpostfunction
 */
class Postfunction extends AppModel {

/**
 * Use database config
 *
 * @var string
 */


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Transicionpostfunction' => array(
			'className' => 'Transicionpostfunction',
			'foreignKey' => 'postfunction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
