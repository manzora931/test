<?php
App::uses('AppModel', 'Model');
/**
 * Genero Model
 *
 * @property Generoincidencia $Generoincidencia
 */
class Genero extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'genero';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Generoincidencia' => array(
			'className' => 'Generoincidencia',
			'foreignKey' => 'genero_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
