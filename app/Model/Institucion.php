<?php
App::uses('AppModel', 'Model');
/**
 * Institucion Model
 *
 * @property Organizacion $Organizacion
 * @property Paise $Paise
 * @property Contacto $Contacto
 */
class Institucion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Organizacion' => array(
			'className' => 'Organizacion',
			'foreignKey' => 'organizacion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Tipoinstitucion' => array(
            'className' => 'Tipoinstitucion',
            'foreignKey' => 'tipoinstitucion_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Rubro' => array(
            'className' => 'Rubro',
            'foreignKey' => 'rubro_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Contacto' => array(
			'className' => 'Contacto',
			'foreignKey' => 'institucion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Proyecto' => array(
            'className' => 'Proyecto',
            'foreignKey' => 'institucion_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);

}
