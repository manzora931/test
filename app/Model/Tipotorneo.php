<?php
App::uses('AppModel', 'Model');
/**
 * Tipotorneo Model
 *
 * @property Torneo $Torneo
 */
class Tipotorneo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tipotorneo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'tipotorneo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
