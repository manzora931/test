<?php
App::uses('AppModel', 'Model');
/**
 * Contacto Model
 *
 * @property Institucion $Institucion
 * @property Municipio $Municipio
 * @property Destinatario $Destinatario
 * @property User $User
 */
class Contacto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombres';

    // Campo virtual que almacena el nombre completo del contacto
    public $virtualFields = array('nombre_contacto' => 'concat(Contacto.nombres, " ", Contacto.apellidos)');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'institucion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Municipio' => array(
			'className' => 'Municipio',
			'foreignKey' => 'municipio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'contacto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
