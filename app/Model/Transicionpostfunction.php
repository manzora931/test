<?php
App::uses('AppModel', 'Model');
/**
 * Transicionpostfunction Model
 *
 * @property Transicion $Transicion
 * @property Postfunction $Postfunction
 */
class Transicionpostfunction extends AppModel {

/**
 * Use database config
 *
 * @var string
 */



	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Transicion' => array(
			'className' => 'Transicion',
			'foreignKey' => 'transicion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Postfunction' => array(
			'className' => 'Postfunction',
			'foreignKey' => 'postfunction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
