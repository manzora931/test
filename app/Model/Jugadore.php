<?php
App::uses('AppModel', 'Model');
/**
 * Jugadore Model
 *
 * @property Paise $Paise
 * @property Posicione $Posicione
 */
class Jugadore extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'nombre';

	// Campo virtual que almacena el nombre completo del jugador
	public $virtualFields = array('nombre_jugador' => 'concat(Jugadore.nombre, " ", Jugadore.apellido)');

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Posicione' => array(
			'className' => 'Posicione',
			'foreignKey' => 'posicione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Persona' => array(
            'className' => 'Persona',
            'foreignKey' => 'persona_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
    public $hasMany = array(
        'Foraneo' => array(
            'className' => 'Foraneo',
            'foreignKey' => 'jugadore_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
