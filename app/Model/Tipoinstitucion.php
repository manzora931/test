<?php
App::uses('AppModel', 'Model');
/**
 * Tipoinstitucion Model
 *
 * @property Institucion $Institucion
 */
class Tipoinstitucion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tipoinstitucions';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Institucion' => array(
			'className' => 'Institucion',
			'foreignKey' => 'tipoinstitucion_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
