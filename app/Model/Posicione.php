<?php
App::uses('AppModel', 'Model');
/**
 * Posicione Model
 *
 * @property Jugadore $Jugadore
 */
class Posicione extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'posicion';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Jugadore' => array(
			'className' => 'Jugadore',
			'foreignKey' => 'posicione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
