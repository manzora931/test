<?php
App::uses('AppModel', 'Model');
/**
 * Generoincidencia Model
 *
 * @property Genero $Genero
 * @property Incidencia $Incidencia
 */
class Generoincidencia extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Genero' => array(
			'className' => 'Genero',
			'foreignKey' => 'genero_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Incidencia' => array(
			'className' => 'Incidencia',
			'foreignKey' => 'incidencia_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
