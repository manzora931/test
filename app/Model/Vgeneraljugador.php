<?php
App::uses('AppModel', 'Model');
/**
 * Vgeneraljugador Model
 *
 */
class Vgeneraljugador extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'equipocontrario';

}
