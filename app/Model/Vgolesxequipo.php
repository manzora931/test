<?php
App::uses('AppModel', 'Model');
/**
 * Vgolesxequipo Model
 *
 */
class Vgolesxequipo extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'vgolesxequipo';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'Goles';

}
