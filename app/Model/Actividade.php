<?php
App::uses('AppModel', 'Model');
/**
 * Actividade Model
 *
 * @property Paise $Paise
 * @property Fuentesfinanciamiento $Fuentesfinanciamiento
 * @property Intervencione $Intervencione
 * @property Gasto $Gasto
 * @property Incidencia $Incidencia
 * @property Presupuesto $Presupuesto
 */
class Actividade extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fuentesfinanciamiento' => array(
			'className' => 'Fuentesfinanciamiento',
			'foreignKey' => 'fuentesfinanciamiento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Intervencione' => array(
			'className' => 'Intervencione',
			'foreignKey' => 'intervencione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Gasto' => array(
			'className' => 'Gasto',
			'foreignKey' => 'actividade_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Incidencia' => array(
			'className' => 'Incidencia',
			'foreignKey' => 'actividade_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Presupuesto' => array(
			'className' => 'Presupuesto',
			'foreignKey' => 'actividade_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
