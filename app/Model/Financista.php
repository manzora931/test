<?php
App::uses('AppModel', 'Model');
/**
 * Financista Model
 *
 * @property Fuentesfinanciamiento $Fuentesfinanciamiento
 */
class Financista extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Fuentesfinanciamiento' => array(
			'className' => 'Fuentesfinanciamiento',
			'foreignKey' => 'financista_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
