<?php
App::uses('AppModel', 'Model');
/**
 * Desembolso Model
 *
 * @property Fuentesfinanciamiento $Fuentefinanciamiento
 * @property Banco $Bancorigen
 * @property Banco $Bancdestino
 * @property Destinatario $Destinatario
 */
class Desembolso extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'codigo';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Fuentesfinanciamiento' => array(
			'className' => 'Fuentesfinanciamiento',
			'foreignKey' => 'fuentefinanciamiento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bancoorigen' => array(
			'className' => 'Banco',
			'foreignKey' => 'bancorigen_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bancodestino' => array(
			'className' => 'Banco',
			'foreignKey' => 'bancdestino_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Destinatario' => array(
			'className' => 'Destinatario',
			'foreignKey' => 'destinatario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
