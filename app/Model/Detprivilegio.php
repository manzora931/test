<?php
App::uses('AppModel', 'Model');
/**
 * Detprivilegio Model
 *
 */
class Detprivilegio extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';
    var $belongsTo = array(
        'Privilegio' => array('className' => 'Privilegio',
            'foreignKey' => 'privilegio_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Detrecurso' => array('className' => 'Detrecurso',
            'foreignKey' => 'detrecurso_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
}
