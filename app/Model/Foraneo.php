<?php
App::uses('AppModel', 'Model');
/**
 * Foraneo Model
 *
 * @property Jugadore $Jugadore
 */
class Foraneo extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Jugadore' => array(
			'className' => 'Jugadore',
			'foreignKey' => 'jugadore_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
