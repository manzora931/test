<?php
App::uses('AppModel', 'Model');
/**
 * Subtarea Model
 *
 * @property Tarea $Tarea
 */
class Subtarea extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombre';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tarea' => array(
			'className' => 'Tarea',
			'foreignKey' => 'tarea_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
