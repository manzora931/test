<?php
App::uses('AppModel', 'Model');
/**
 * Jugadoresxequipo Model
 *
 * @property Juego $Juego
 * @property Jugadore $Jugadore
 */
class Juegnota extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Juego' => array(
			'className' => 'Juego',
			'foreignKey' => 'juego_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Jugadore' => array(
			'className' => 'Jugadore',
			'foreignKey' => 'jugadore_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
