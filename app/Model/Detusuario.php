<?php
App::uses('AppModel', 'Model');
/**
 * Detusuario Model
 *
 * @property Torneo $Torneo
 */
class Detusuario extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Torneo' => array(
			'className' => 'Torneo',
			'foreignKey' => 'torneo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);

}
