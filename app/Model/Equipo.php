<?php
App::uses('AppModel', 'Model');
/**
 * Equipo Model
 *
 * @property Tipoequipo $Tipoequipo
 * @property Estadio $Estadio
 * @property Paise $Paise
 * @property Jugadoresxequipo $Jugadoresxequipo
 */
class Equipo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombrecorto';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Tipoequipo' => array(
			'className' => 'Tipoequipo',
			'foreignKey' => 'tipoequipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Estadio' => array(
			'className' => 'Estadio',
			'foreignKey' => 'estadio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Paise' => array(
			'className' => 'Paise',
			'foreignKey' => 'paise_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Jugadoresxequipo' => array(
			'className' => 'Jugadoresxequipo',
			'foreignKey' => 'equipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
