<?php
App::uses('AppModel', 'Model');

class Evento extends AppModel {

	public $displayField = 'evento';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Juego' => array(
			'className' => 'Detevento',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
