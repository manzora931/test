<?php echo $this->Html->Script('buscar',FALSE); ?>
<?php echo $this->Html->Script('jquery',FALSE); ?>
<?php echo $this->Html->Script('jquery-ui.js',FALSE); ?>
<div class="socionegocios index">
<h2><?php echo __('Socios de Negocios');?></h2>
<p>
<?php
echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
    ));
?>	</p>
<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/ //desde aca
	$tabla = "socionegocios";
	?>
	<div id='search_box'>
	<?php
		echo $this->Form->create($tabla,array('action'=>'','value'=>1));
		echo "<table>";
		echo "<tr>";
		echo "<td>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: CODIGO, NOMBRE, NOMBRE COMERCIAL', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text));
		echo "</td>";
		echo "<td style='width: 100px;'>";
		//echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked'>";	
		}else{
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";	
		}
		echo "</td>";
		echo "<td>";
		echo $this->Form->hidden('Controller', array('value'=>'Socionegocio', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'codigo,nombre,nombcomercial', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo $this->Form->end('Buscar');
		echo "</td>";
		echo "</tr>";		
		echo "</table>";
	?>
	<?php
	if(isset($_SESSION["$tabla"]))
	{
		echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href = location + \'/vertodos\''));
	}//hasta aca
	?>
	</div> 
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->paginator->sort('id');?></th>
	<th><?php echo $this->paginator->sort('codigo', 'Código');?></th>
	<th><?php echo $this->paginator->sort('nombre', 'Razon Social');?></th>
	<th><?php echo $this->paginator->sort('nombcomercial','Nombre Comercial');?></th>
	<th><?php echo $this->paginator->sort('activo');?></th>
	<th><?php echo $this->paginator->sort('catsocionegocio_id','Categoría');?></th>
	
	<th class="actions"><?php echo __('Acciones');?></th>
</tr>
<?php
$i = 0;
foreach ($socionegocios as $socionegocio):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	if ($socionegocio['Socionegocio']['activo']==1) $vactivo = 'Si';
	else $vactivo = 'No';
?>
	<tr><?php //echo $class;?>
		<td>
			<?php echo $socionegocio['Socionegocio']['id']; ?>
		</td>
		<td>
			<?php echo $socionegocio['Socionegocio']['codigo']; ?>
		</td>
		<td>
			<b><?php echo $socionegocio['Socionegocio']['nombre']; ?></b>
		</td>
		<td>
			<?php echo $socionegocio['Socionegocio']['nombcomercial']; ?>
		</td>
		<td class="valortabla">
			<?php echo $vactivo; ?>
		</td>
		<td>
			<?php echo $this->Html->link($socionegocio['Catsocionegocio']['catsocionegocio'], array('controller'=> 'catsocionegocios', 'action'=>'view', $socionegocio['Catsocionegocio']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__(''), array('action' => 'view', $socionegocio['Socionegocio']['id']),array('class'=>'ver')); ?>
			<?php echo $this->Html->link(__(''), array('action' => 'edit', $socionegocio['Socionegocio']['id']),array('class'=>'editar')); ?>
		</td>


	</tr>
<?php endforeach; ?>
</table>

</div>
<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __(' Anterior '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array(' separator ' => ''));
		echo $this->Paginator->next(__(' Siguiente ') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo Socio de Negocios', true), array('action'=>'add')); ?></li>
		<li><?php echo $this->Html->link(__('Categoría de Socios de Negocios', true), array('controller'=> 'catsocionegocios', 'action'=>'index')); ?> </li>
		<li><A HREF="javascript:javascript:history.go(-1)">Regresar</A></li>
	</ul>
</div>
