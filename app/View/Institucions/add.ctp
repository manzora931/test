<?php $real_url = (isset($_SERVER['HTTPS']) ? "https:" : "http:") . '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<?= $this->Html->Script('jquery.maskedinput.js',FALSE); ?>
<script >
    function getURL(){
        return '<?=$real_url;?>';
    }
    $(document).ready(function(){

    });


</script>
<style>
    div.form{
        width: 100%;
        margin-bottom: 15px;
    }
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 15px;
        border:1px solid #cb071a;
        color: #011880;
    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }
    div.actions>div{
        display: inline-block;
    }
    .form-group{
        width:700px;
    }
    select.form-control {
      width: 300px;
    }
</style>
<div class="institucions form container">

    <?php echo $this->Form->create('Institucion' ,array('id'=>'frm','class'=>"add-edit"));?>
    <fieldset>
        <legend><?php echo __('Adicionar Institución');?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>

        <div class="col-xs-12">
            <?php

            echo	$this->Form->input('nombre',
                array('label' => 'Institución',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));

            echo	$this->Form->input('paise_id',
            array('label' => 'País',
            'class' => 'form-control',
            'div' => ['class'=>'form-group'],
            'value'=>'',
            'id' => 'pais',
            'empty'=>'Seleccionar',
            'required'=>'required'));

            echo $this->Form->input('tipoinstitucion_id',
                array('label' => 'Tipo',
                    'class' => 'form-control',
                    'id' => 'tipoinstitucion_id',
                    'div' => ['class'=>'form-group'],
                    "empty"=>"Seleccionar"
                ));
            echo $this->Form->input('rubro_id',
                array('label' => 'Rubro',
                    'class' => 'form-control',
                    'id' => 'rubro_id',
                    'div' => ['class'=>'form-group'],
                    "empty"=>"Seleccionar"
                ));
            echo	$this->Form->input('telefono',
                array('label' => 'Teléfono',
                    'class' => 'form-control',
                    'id' => 'telefono',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                ));

            ?>

            <div class="alert alert-danger col-xs-6" id="alert" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="message"></span>
                <button type="button" class="close" data-dismiss="alert"></button>
            </div>
          <?php

            echo	$this->Form->input('email',
                array('label' => 'Correo Electrónico',
                    'class' => 'form-control',
                    'id' => 'ccorreo',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                   ));

            echo	$this->Form->input('direccion',
                array('label' => 'Dirección',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'rows' => 3,
                    'value'=>'',
                    ));
          echo	$this->Form->input('sitioweb',
              array('label' => 'Sitio Web',
                  'class' => 'form-control',
                  'id' => 'sitioerb',
                  'div' => ['class'=>'form-group'],
              ));
          echo	$this->Form->input('url1',
              array('label' => 'URL 1',
                  'class' => 'form-control',
                  'id' => 'url1',
                  'div' => ['class'=>'form-group'],
              ));
          echo	$this->Form->input('url2',
              array('label' => 'URL 2',
                  'class' => 'form-control',
                  'id' => 'url2',
                  'div' => ['class'=>'form-group'],
              ));
          echo	$this->Form->input('url3',
              array('label' => 'URL 3',
                  'class' => 'form-control',
                  'id' => 'url3',
                  'div' => ['class'=>'form-group'],
              ));
            echo	$this->Form->input('descripcion',
                array('label' => 'Descripción',
                    'class' => 'form-control',
                    'rows' => 3,
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    ));

            ?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activa',
                    'type' => 'checkbox',
                    'checked' => 'checked',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>


    </fieldset>
</div>
<div class="actions">

    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Instituciones'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script>

    jQuery(function(){
        $('#ccorreo').change(function() {
            // Expresion regular para validar el correo
            if($("#ccorreo").val().indexOf('@', 0) == -1 || $("#ccorreo").val().indexOf('.', 0) == -1) {
                $("#alert .message").text('Correo Invalido');
                $("#alert").slideDown();
                $(validate.id).val("");

                setTimeout(function () {
                    $("#alert").slideUp();
                }, 4000);
                $("#ccorreo").val('');
                return false;
            }

        });
        var validate = {
            "id" : "#InstitucionNombre",
            "form" : "#InsitucionAddForm",
            "message" : "La Institución Ingresada ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_per';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_per';
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val:val, id: id},
                cache: false,
                success: function(resp) {
                    if ( resp == "error" ) {
                        $("#alerta .message").text(validate.message);
                        $("#alerta").slideDown();
                        $(validate.id).val("");

                        setTimeout(function () {
                            $("#alerta").slideUp();
                        }, 4000);
                    } else {
                        form.submit();
                    }
                }
            });
        }

            $("#pais").change(function(){
                var pais = $(this).val();
                var url = getURL() + 'get_mask';

                $.ajax({
                    url: url,
                    type: 'post',
                    data: { pais:pais},
                    cache: false,
                    success: function(resp) {
                        if (resp ) {
                            var codigo = resp;
                            $("#telefono").mask("("+codigo+") 9999-9999");
                        }
                    }
                });
            });

    });
</script>