<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
  
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="perfiles view container">
    <h2><?php  echo __('Institución');?></h2>
    <table width="100%">
        <tr>
            <td colspan="2">
                <?php
                if( isset( $_SESSION['inst_save'] ) ) {
                    if( $_SESSION['inst_save'] == 1 ){
                        unset( $_SESSION['inst_save'] );
                        ?>
                        <div class="alert alert-success" id="alerta">
                            <span class="icon icon-check-circled"></span>
                            Insitución Almacenada
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
                    <?php }
                } ?>
            </td>
        </tr>
    </table>
    <table id="tblview">

        <tr>
            <td>Id</td>
            <td><?= $institucion['Institucion']['id']; ?></td>
        </tr>
        <tr>
            <td>Institución</td>
            <td><?=$institucion['Institucion']['nombre']; ?></td>
        </tr>
        <tr>
            <td>Tipo</td>
            <td><?=$institucion['Tipoinstitucion']['tipoinstitucions']; ?></td>
        </tr>
        <tr>
            <td>Rubro</td>
            <td><?=$institucion['Rubro']['rubro']; ?></td>
        </tr>
        <tr>
            <td>País</td>
            <td><?=$institucion['Paise']['pais']; ?></td>
        </tr>
        <tr>
            <td>Teléfono</td>
            <td><?=$institucion['Institucion']['telefono']; ?></td>
        </tr>
        <tr>
            <td>Correo Electrónico</td>
            <td><?=$institucion['Institucion']['email']; ?></td>
        </tr>

        <tr>
            <td>Dirección</td>
            <td><?=$institucion['Institucion']['direccion']; ?></td>
        </tr>
        <tr>
            <td>Sitio web</td>
            <td><?=$institucion['Institucion']['sitioweb']; ?></td>
        </tr>
        <tr>
            <td>URL 1</td>
            <td><?=$institucion['Institucion']['url1']; ?></td>
        </tr>
        <tr>
            <td>URL 2</td>
            <td><?=$institucion['Institucion']['url2']; ?></td>
        </tr>
        <tr>
            <td>URL 3</td>
            <td><?=$institucion['Institucion']['url3']; ?></td>
        </tr>
        <tr>
            <td>Activa</td>
            <td><?= ($institucion['Institucion']['activo']==1)?"Si":"No"; ?></td>
        </tr>
        <tr>
            <td>Descripción</td>
            <td><?=$institucion['Institucion']['descripcion']; ?></td>
        </tr>
        <tr>
            <td>Creado</td>
            <td><?= $institucion['Institucion']['usuario']." (".$institucion['Institucion']['created'].")"; ?></td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td><?= ($institucion['Institucion']['usuariomodif']!='')?$institucion['Institucion']['usuariomodif']." (".$institucion['Institucion']['modified'].")":""; ?></td>
        </tr>
    </table>


</div>
<div class="actions">

    <div><?php echo $this->Html->link(__('Editar Institución'), array('action' => 'edit', $institucion['Institucion']['id']),array('class'=>'btn btn-default')); ?> </div>
    <div><?php echo $this->Html->link(__('Listado de Instituciones'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<div id="tblvr">

    <h2>Contactos Asociados</h2>
    <br>
    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>

            <th>Id</th>
            <th>Nombre del Contacto</th>
            <th>Correo Electrónico</th>
            <th>Cargo</th>
            <th class="activo">Activo</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $b=0;
        if(isset($contactos)){

        foreach ($contactos as $data):
            $b=1;
           ?>
            <tr>
                <td class="bdr">
                    <?php echo $data['Contacto']['id']; ?>
                </td>
                <td style="text-align: left;">
                    <?php echo $data['Contacto']['nombres'] . ' ' . $data['Contacto']['apellidos']; ?>
                </td>
                <td style="text-align: left;">
                    <?php echo $data['Contacto']['email']; ?>
                </td>

                <td style="text-align: left;">
                    <?php echo $data['Contacto']['cargo']; ?>
                </td>
                <td align='center' class="bdr">
                    <?php
                    if($data['Contacto']['activo']==1){
                        $act='Si';
                    }else{
                        $act='No';
                    }

                    echo $act; ?>
                </td>

            </tr>
        <?php endforeach;
        } if($b==0) {
            echo "<tr><td colspan='6'> <p>No hay usuarios relacionados a esta institución</p></td></tr>";
        }?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>