
<style>
    table.table tbody tr td.select {
        vertical-align: middle;
    }
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-stripe>tbody>tr:nth-child(4n) > td, table.table-stripe>tbody>tr:nth-child(4n-1) > td{
        background-color: #fff ;
    }
    table.table-stripe>tbody>tr:nth-child(4n-2) > td, table.table-stripe>tbody>tr:nth-child(4n-3) > td{
        background-color: #ebebeb ;
    }


    table td.acciones {
        vertical-align: middle;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        display: table;
        width: 100%;
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }

    #search_box div {
        float: none;
        display: table-cell;
    }

    select.form-control {
        width: 120px;
       margin-left: 50px;
    }
    div.select >label{
        margin-left: 50px;
    }

    .btn-acciones {
        background-color: #011880;
        color: #fff;
        border-radius: 5px;
        width: 120px;
    }

    .btn-acciones:hover {
        color: #fff;
    }
    .btn-crear {
        background-color: #4160a3;
        color: #fff;
        border-radius: 5px;
        width: 120px;
    }
    .btn-crear:hover {
        color: #fff;
    }


</style>
<div class="institucions index container">
    <h2><?php echo __('Instituciones'); ?></h2>
	<span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));	?>
	</span>
    <?php
    if( isset( $_SESSION['contact_notDelete'] ) ) {
        if( $_SESSION['contact_notDelete'] == 1 ) {
            unset( $_SESSION['contact_notDelete'] );
            ?>
            <div class="alert alert-danger" id="contacto-nodelete">
                <span class="icon icon-cross-circled"></span>
                El contacto no puede ser eliminado porque tiene usuario relacionado

            </div>
        <?php }
    } ?>
    <?php
    if( isset( $_SESSION['contact_delete'] ) ) {
        if( $_SESSION['contact_delete'] == 1 ){
            unset( $_SESSION['contact_delete'] );
            ?>
            <div class="alert alert-success" id="contacto-delete">
                <span class="icon icon-check-circled"></span>
                El contacto se ha eliminado correctamente

            </div>
        <?php }
    } ?>
    <?php $table = "institucions";	?>
    <?php $session = $this->Session->read('tabla[perfiles]'); ?>
    <?php $search_text = $session['search_text'] ?>
    <?php $real_url = 'http://'. $_SERVER['HTTP_HOST'] . $this->request->base .'/'. $this->params->controller .'/'; ?>

    <div id='search_box'>
        <div class="col-sm-6 search">
            <?php
            /*Inicia formulario de busqueda*/
            $tabla = "institucions";
            ?>
            <?php
            echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 100%;'));
            echo "<input type='hidden' name='_method' value='POST' />";
            echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
            echo "<tr>";
            echo "<td>";
            $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
            echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Institución, contacto', 'style' => 'width: 100%'));
            echo "</td>";
            echo "<td>";
            echo $this->Form->input('paise_id',array('label'=>'País', 'empty'=>'Seleccionar','class'=>'form-control'));
            echo "</td>";
            echo "<td id='check'>";

            //echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
            if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
                echo "<br>Inactivas <input type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked' style='margin: 5px 10px 6px 25px;'>";
            }else{
                echo "<br>Inactivas <input type='checkbox' name='data[$tabla][activo]' value='0' style='margin: 5px 10px 6px 25px;'>";
            }
            echo "</td>";
            echo "<td style='float:right; padding-top: 20px; '>";
            echo $this->Form->hidden('Controller', array('value'=>'Institucion', 'name'=>'data[tabla][controller]')); //hacer cambio
            echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
            echo $this->Form->hidden('Parametro1', array('value'=>'nombre', 'name'=>'data[tabla][parametro]'));
            echo $this->Form->hidden('Parametro1', array('value'=>'Institucion,Contacto', 'name'=>'data[tabla][tipobusqueda]'));
            echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
            echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
            echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
            echo " <input type='submit' class='btn btn-default' value='Buscar' style='width: 100px;border-color: #606060;'>";

            echo "</td>";
            echo "</tr>";
            echo "</table>";
            ?>
            <?php
            if(isset($_SESSION['institucions']))
            {
                $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
                echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
            }
            ?>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-5 div-actions">
            <table width="100%">
                <td class="acciones form-group">
                    <?php echo $this->Html->link(__('Crear'), array('action'=>'add'),array('class'=>'btn btn-crear')) ?>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones" id="edit-details" name="button">Modificar</button>
                </td>
                <td class="acciones form-group">
                    <button type="submit" class="btn btn-acciones" id="view-details" name="button">Ver Detalles</button>
                </td>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>

    <script type="text/javascript">
        var searchBy = $("#search-by");
        var active = $("#active");

        searchBy.val('<?= $session['search-by'] ?>');
        active.val('<?= $session['activo'] ?>');
    </script>
    <table class="table table-accordion table-condensed table-stripe">
        <thead>
        <tr>
            <th class="select bdr1"></th>
            <th class="id" style="width: 4%;"><?php echo $this->paginator->sort('id');?></th>
            <th style="width: 27%;"><?php echo $this->paginator->sort('nombre','Institución');?></th>
            <th style="width: 12%;"><?php echo $this->paginator->sort('Tipoinstitucion.tipoinstitucions','Tipo');?></th>
            <th style="width: 10%;"><?php echo $this->paginator->sort('paise_id','País');?></th>
            <th style="width: 26%;"><?php echo $this->paginator->sort('direccion','Dirección');?></th>
            <th style="width: 13%;"><?php echo $this->paginator->sort('correo','Correo Electrónico');?></th>
            <th style="width: 10%;"><?php echo $this->paginator->sort('Rubro.rubro','Rubro');?></th>
            <th class="activo bdr" style="width: 5%;"><?php echo $this->paginator->sort('activo','Activa');?></th>
            <!--<th class="bdr" style="width: 5%;"><?php // echo $this->paginator->sort('Acciones');?></th>-->
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($institucions as $institucion):
        ?>
            <tr>
                <td class="select">
                    <input type="radio" name="item-selected" class="item-selected" value="<?= $institucion['Institucion']['id'] ?>">
                </td>
                <td>
                    <?php echo $institucion['Institucion']['id']; ?>
                </td>
                <td style="text-align: left;" class="plus-td" data-child="#contacts-<?= $institucion['Institucion']['id'] ?>">
                    <span class="plus" ></span><?php echo $institucion['Institucion']['nombre']; ?>
                </td>
                <td style="text-align: left;" >
                    <?= $institucion['Tipoinstitucion']['tipoinstitucions'];?>
                </td>
                <td style="text-align: center;">
                    <?php echo $institucion['Paise']['pais']; ?>
                </td>
                <td style="text-align: left;">
                    <?php echo $institucion['Institucion']['direccion']; ?>
                </td>
                <td style="text-align: left;">
                   <a href="mailto:<?php echo$institucion['Institucion']['email'];?>"> <?php echo$institucion['Institucion']['email'];?></a>
                </td>
                <td style="text-align: left;">
                    <?php echo $institucion['Rubro']['rubro']; ?>
                </td>
                <td align='center'>
                    <?php
                    if($institucion['Institucion']['activo']==1){
                        $act='Si';
                    }else{
                        $act='No';
                    }

                    echo $act; ?>
                </td>
            </tr>
            <tr id="contacts-<?= $institucion['Institucion']['id'] ?>" class="contacts table-accordion-item hide">
                <td colspan="9" class="table-accordion-child">
                    <?php /* revisar si tiene contactos para agregar tabla de contactos */ ?>
                    <?php if (isset($contacts[$institucion['Institucion']['id']])) { ?>
                        <table class="table table-condensed">
                            <thead>
                            <th class="select"></th>
                            <th class="id">ID</th>
                            <th class="main">Nombre del Contacto</th>
                            <th>Cargo</th>
                            <th>Correo electrónico</th>
                            <th>Teléfono</th>
                            <th class="activo">Activo</th>
                            </thead>
                            <tbody>
                            <?php //contactos ?>
                            <?php foreach ($contacts[$institucion['Institucion']['id']] as $contact) { ?>
                                <tr class="contact">
                                    <td><input name="contactSelected" class="contactSelected" value="<?= $contact['id'] ?>" type="radio"></td>
                                    <td>
                                        <span id="show_id_<?= $contact['id'] ?>"><?= $contact['id'] ?></span>
                                    </td>
                                    <td><?= $contact['nombres'] .' '.$contact['apellidos'] ?></td>
                                    <td><?= $contact['cargo'] ?></td>
                                    <td><?= $contact['email'] ?></td>
                                    <td><?= $contact['telefono'] ?></td>
                                    <td class="text-center"><?= $contact['activo'] == 1 ? 'Si' : 'No' ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <?php // Opciones para edición/adición de contactos ?>
                    <div id="options-<?= $institucion['Institucion']['id'] ?>" class="options contacts-options container-fluid table-accordion-child-options col-xs-12 clearfix">
                        <div class="col-xs-12">
                            <button class="btn btn-default addContactBtn" data-id="<?= $institucion["Institucion"]["id"] ?>">
                                Crear Contacto
                            </button>
                            <button class="editContact btn btn-default">
                                Modificar
                            </button>
                            <button class="deleteContact btn btn-default">
                                Eliminar
                            </button>
                            <button class="viewContactBtn btn btn-default">
                                Ver Detalles
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior |', true), array(), null, array('class'=>'disabled'));?>
        <?php echo $this->Paginator->numbers(); ?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <!--<div class="actions">
        <ul>
            <li class="style-btn"><?php // echo $this->Html->link(__('Crear Institución'), array('action'=>'add'),array( 'style'=>'width:50px;','class'=>'btn btn-default')) ?></li>
        </ul>
    </div>-->
    <script type="text/javascript">
        $(function () {
            var viewDetails = $("#view-details");
            var editDetails = $("#edit-details");
            var cloneEntry = $("#clone-entry");

            viewDetails.click( function () {
                if ($(".item-selected").is(":checked")) {
                    window.location.assign( "<?= $real_url ?>" + "view/" + $(".item-selected:checked").val());
                }
                return false;
            });

            editDetails.click( function () {
                if ($(".item-selected").is(":checked")) {
                    window.location.assign( "<?= $real_url ?>" + "edit/" + $(".item-selected:checked").val());
                }
            });

            cloneEntry.click( function () {
                if ($(".item-selected").is(":checked")) {
                    window.location.assign( "<?= $real_url ?>" + "clonar/" + $(".item-selected:checked").val());
                }
            });

            //Acción al presionar el botón Eliminar institución
            $("#delete").click( function () {
                if ($(".item-selected").is(":checked")) {
                    if(confirm("Está seguro que desea eliminar la institución")){
                        window.location.assign( "<?= Router::url(['action' => 'delete']) ?>" + "/" + $(".item-selected:checked").val());
                    }
                }else{
                    $("#txt").text("Seleccione una institución para completar la operación");
                    $("#alertNot").slideDown();
                    setTimeout(function () {
                        $("#alertNot").slideUp();
                    }, 5000);
                }

                return false;
            });

            //función para mostrar tabla de contactos de institución
            $(".plus-td").click( function () {
                $($(this).data("child")).toggleClass('hide');
            });



            //Acción al presionar el botón crear Contacto
            $(".addContactBtn").click( function () {
                idInstitucion = $(this).attr('data-id');

                var url = "<?= Router::url(array('controller' => 'contactos', 'action' => 'add')); ?>";
                window.location.assign(url + "/" + idInstitucion);

                return false;
            });

            //Acción al presionar el botón Ver Contacto
            $(".viewContactBtn").click( function () {
                var url = "<?= Router::url(array('controller' => 'contactos', 'action' => 'view')); ?>";

                if ($(".contactSelected").is(":checked")) {
                    window.location.assign( url + "/" + $(".contactSelected:checked").val());
                }

                return false;
            });

            //Acción al presionar modificar contacto
            $(".editContact").click( function () {
                var id = $(".contactSelected:checked").val();
                var url = "<?= Router::url(array('controller' => 'contactos', 'action' => 'edit')); ?>";

                if ($(".contactSelected").is(":checked")) {
                    window.location.assign(url + "/" + id);
                }

                return false;
            });

            //Acción al presionar eliminar contacto
            $(".deleteContact").click( function () {
                var id = $(".contactSelected:checked").val();
                var url = "<?= Router::url(array('controller' => 'contactos', 'action' => 'delete')); ?>";

                if ($(".contactSelected").is(":checked")) {
                    if(confirm("Está seguro que desea eliminar el contacto")) {
                        window.location.assign(url + "/" + id);
                    }
                }

                return false;
            });

            $("#contacto-nodelete").slideDown();
            setTimeout(function () {
                $("#contacto-nodelete").slideUp();
            }, 4000);

            $("#contacto-delete").slideDown();
            setTimeout(function () {
                $("#contacto-delete").slideUp();
            }, 4000);
        });
    </script>
</div>