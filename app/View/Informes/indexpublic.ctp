<?= $this->Html->css('proyecto/impresion') ?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
$real_url_base = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';?>
<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',"plantillas/".$host.".css"]);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->css(['public.default']);?>
<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
    function getURLBase(){
        return '<?=$real_url_base;?>';
    }
</script>
<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 50%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 0.83em;

    }
    .table thead tr th {
        font-size: 0.81em;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
    #raphael-paper-140, #raphael-paper-95, #raphael-paper-15
    {
        border: 1px solid #6690f1;
        box-shadow: 0 0 4px 1px #c8c9d0;
    }
    .escudoEQ{
        width: 30px;
    }
    /*clase cuando los puntos son menores o igual a 9*/
    .radio1{
        background: #484848;
        padding: 9px 16px 9px 16px;
        border-radius: 50%;
        color:#ffffff;
        font-weight: bold;
    }
    /**CLASE CUANDO LOS PUNTOS SON MAYORES A 9**/
    .radio2{
        background: #484848;
        padding: 9px 12px 9px 12px;
        border-radius: 50%;
        color:#ffffff;
        font-weight: bold;
    }
</style>
<div class="informes index container-fluid">
    <h2 class="titulo-personalizado">
        <?php echo __('Estadísticas Generales por Torneo');?>
    </h2>
</div>
<!---- 	PIE CHART    ----->
<?php echo  $this->html->Script(['fusioncharts','fusioncharts.theme.fint']);   ?>

<script type="text/javascript">
    $(function () {
        $( "#torneo_id" ).on( "change", function() {
            var id = $(this).val();
            window.location.assign(getURL()+"informes/index/" + id);
        });
    });
</script>
<?php
include("fusioncharts.php");
if ($golexequipos) {
    // The `$arrData` array holds the chart attributes and data
    $arrData4 = array(
        "chart" => array(
            "caption" => "",
            "paletteColors" => "#0075c2",
            "bgColor" => "#ffffff",
            "borderAlpha"=> "20",
            "canvasBorderAlpha"=> "0",
            "usePlotGradientColor"=> "0",
            "plotBorderAlpha"=> "10",
            "showXAxisLine"=> "1",
            "xAxisLineColor" => "#999999",
            "yaxisname"=> "Goles",
            "showValues" => "0",
            "divlineColor" => "#999999",
            "divLineIsDashed" => "1",
            "showAlternateHGridColor" => "0"
        )
    );

    $arrData4["data"] = array();
    $vineta = "";
    // Push the data into the array
    foreach ($golexequipos as $golesxequipo):
        array_push($arrData4["data"], array(
                "label" => $golesxequipo['Vgolesxequipo']['equipo'],
                "value" => $golesxequipo['Vgolesxequipo']['golafavor']
            )
        );
    endforeach;

    $jsonEncodedData4 = json_encode($arrData4);

    $columnChart4 = new FusionCharts("bar2D", "chart1" , 600, 300, "chart-1", "json", $jsonEncodedData4);
    // Render the chart
    $columnChart4->render();
    // Close the database connection
    //$dbhandle->close();
}
if ($posesion) {
    // The `$arrData` array holds the chart attributes and data
    $arrData4 = array(
        "chart" => array(
            "caption" => "",
            "paletteColors" => "#0075c2",
            "bgColor" => "#ffffff",
            "borderAlpha"=> "20",

            "canvasBorderAlpha"=> "0",
            "usePlotGradientColor"=> "0",
            "plotBorderAlpha"=> "10",
            "showXAxisLine"=> "1",
            "xAxisLineColor" => "#999999",
            "yaxisname"=> "Posesión de balón",
            "showValues" => "0",
            "divlineColor" => "#999999",
            "divLineIsDashed" => "1",
            "showAlternateHGridColor" => "0"
        )
    );

    $arrData4["data"] = array();
    $vineta = "";
    // Push the data into the array
    foreach ($posesion as $row):
        array_push($arrData4["data"], array(
                "label" => $row['Vposesionbalon']['nombrecorto'],
                "value" => $row['Vposesionbalon']['posesion']
            )
        );
    endforeach;

    $jsonEncodedData4 = json_encode($arrData4);

    $columnChart4 = new FusionCharts("column2D", "chart2" , 600, 300, "chart-2", "json", $jsonEncodedData4);
    // Render the chart
    $columnChart4->render();
    // Close the database connection
    //$dbhandle->close();
}
if($taquileros){
    $arrData5 = array(
        "chart" => array(
            "caption"=> "",
            "xAxisname"=> "",
            "yAxisName"=> "",
            "numberPrefix"=> "",
            "plotFillAlpha"=> "80",
            "paletteColors"=> "#0075c2,#1aaf5d",
            "baseFontColor"=> "#333333",
            "baseFont"=> "Helvetica Neue,Arial",
            "captionFontSize"=> "14",
            "subcaptionFontSize"=> "14",
            "subcaptionFontBold"=> "0",
            "showBorder"=> "0",
            "bgColor"=> "#ffffff",
            "showShadow"=> "0",
            "canvasBgColor"=> "#ffffff",
            "canvasBorderAlpha"=> "0",
            "divlineAlpha"=> "100",
            "divlineColor"=> "#999999",
            "divlineThickness"=> "1",
            "divLineIsDashed"=> "1",
            "divLineDashLen"=> "1",
            "divLineGapLen"=> "1",
            "usePlotGradientColor"=> "0",
            "showplotborder"=> "0",
            "valueFontColor"=> "#ffffff",
            "placeValuesInside"=> "1",
            "showHoverEffect"=> "1",
            "rotateValues"=> "1",
            "showXAxisLine"=> "1",
            "xAxisLineThickness"=> "1",
            "xAxisLineColor"=> "#999999",
            "showAlternateHGridColor"=> "0",
            "legendBgAlpha"=> "0",
            "legendBorderAlpha"=> "0",
            "legendShadow"=> "0",
            "legendItemFontSize"=> "10",
            "legendItemFontColor"=> "#666666"
        )
    );
    $arrData5["categories"]=array();
    array_push($arrData5["categories"],array(
        "category"=>array()
    ));

    foreach ($taquileros as $mod):
        array_push($arrData5["categories"][0]["category"], array(
                "label"=>$mod["Vequipotaquillero"]["nombrecorto"]
            )
        );
    endforeach;

    $arrData5["dataset"]=array();
    array_push($arrData5["dataset"],array(
        "seriesname"=>"Asistencia",
        "data"=>array()
    ));

    //$arrData1["dataset"]["data"]=array();
    foreach ($taquileros as $mod2):
        array_push($arrData5["dataset"][0]["data"], [
            "value"=>$mod2["Vequipotaquillero"]["asistencia"]
        ]);
    endforeach;

    array_push($arrData5["dataset"],array(
        "seriesname"=>"Taquilla",
        "data"=>array()
    ));

    foreach ($taquileros as $mod3):
        array_push($arrData5["dataset"][1]["data"], [
            "value"=>$mod3["Vequipotaquillero"]["taquilla"]
        ]);
    endforeach;

    $dataJson = json_encode($arrData5);
    $columnChart1 = new FusionCharts("msbar2d", "taquilleros" , 600, 400, "chart-3", "json", $dataJson);
    // Render the chart
    $columnChart1->render();
}
?>
<div class="container-fluit" style="background-color:#f4f4f4; padding-bottom: 20px;padding-top: 20px;margin-left: 17px;margin-right: 21px;"">
<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-12 col-lg-12 col-xs-12 text-center seccion-btn">
        <button id="btnEstadistica" onclick="regresar();" class="btn btn-primary btn-redirec">Regresar</button>
    </div>
    <div class="col-sm-10 col-sm-offset-1 col-md-5 col-lg-5 seccion-btn" style="text-align: center;padding-left: 30px;padding-right: 0px;">
        <span class="title-graf subtitulo-view">Máximos Goleadores</span>
        <br><br>
        <table cellpadding="0" cellspacing="0" id="table-listjuegos" class="table table-condensed table-striped tb-publics" style="box-shadow: 0 0 7px 0px #c8c9d0;">
            <thead>
            <tr>
                <th width="50%">Jugador</th>
                <th width="35%" class="text-center">Equipo</th>
                <th width="15%" class="text-center">Goles</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;
            foreach ($goleadores as $item): ?>
                <tr>
                    <td class="text-left"><?= $item['Vregistrogole']['jugador']; ?>&nbsp</td>
                    <td class="text-left"><?= $item["Vregistrogole"]["equipo"]; ?></td>
                    <td class="text-center"><?= $item["Vregistrogole"]["goles"] ?></td>
                </tr>
                <?php
                $i++;
            endforeach; ?>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-offset-1 col-sm-12 col-md-5 col-lg-5" style="text-align: center;">
        <span class="title-graf subtitulo-view">Equipos más Goleadores</span>
        <br><br>
        <div id="chart-1"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-1 col-xs-10 col-xs-offset-1" style="text-align: center;">
        <span class="title-graf subtitulo-view">Tabla de Posiciones</span>
        <br><br>
        <table cellpadding="0" cellspacing="0" class="table table-position tb-publics">
            <thead>
            <tr>
                <th style="width:20px;">N°</th>
                <th width="50px"></th>
                <th class="text-center">EQUIPO</th>
                <th class="text-center">PTS.</th>
                <th class="text-center">PJ</th>
                <th class="text-center">PG</th>
                <th class="text-center">PE</th>
                <th class="text-center">PP</th>
                <th class="text-center">GF</th>
                <th class="text-center">GC</th>
                <th class="text-center">DF</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $cont=1;
            $cls="";
            $puntos=0;
            $extra=0;
            foreach ($tabposision as $row):
                $cls = ($cont>1)?"line-shadow":"";
                ?>
                <tr>
                    <td class="<?=$cls?>"><span class="colum-num"><?= $cont;?></span></td>
                    <td class="<?=$cls?>">
                        <?php if($imgEquipos[$row["x"]["equipo_id"]]["img"]!=null){ ?>
                            <img class="escudoEQ" src="../../../<?=$imgEquipos[$row["x"]["equipo_id"]]["img"]?>">
                        <?php   } ?>
                    </td>
                    <td class="text-left <?=$cls?>">
                        <?= $row["x"]["nombrecorto"]; ?>
                    </td>
                    <td class="<?=$cls?>">
                        <span style="font-weight: bold;font-size: 14px;">
                        <?php
                        if($row["pet"]["puntosextra"]!=null){  ?>

                                <?php
                                $puntos = $row[0]["puntos"];
                                $extra=($row["pet"]["puntosextra"]>0)?"+".$row["pet"]["puntosextra"]:$row["pet"]["puntosextra"];
                                echo $puntos." <a style='color: #3d3d3d; ' title='".$row["pet"]["comentario"]."'>(".$extra.")</a>";?>
<?php                    }else{  ?>
                                <?=(($cont%2)==0)?$row[0]["puntos"]:$row[0]["puntos"]; ?>
<?php                   }                        ?>
                        </span>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["partidosjugados"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["partidoganado"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["partidoempatado"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["partidoperdido"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["golafavor"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["golencontra"]?></strong>
                    </td>
                    <td class="<?=$cls?>">
                        <strong><?=$row["x"]["goldiferencia"]?></strong>
                    </td>
                </tr>
                <?php   $cont++;
                $puntos=0;
            endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row" >
    <div class="col-sm-12 col-md-5 col-lg-5" style="text-align: center;margin: 0px; padding:0px;">
        <span class="title-graf subtitulo-view">Equipos con más posesión de balón</span>
        <br><br>
        <div id="chart-2"></div>
    </div>
    <div class="col-md-offset-1 col-sm-12 col-md-5 col-lg-5" style="text-align: center; margin-bottom: 20px;">
        <br>
        <span class="title-graf subtitulo-view">Equipos más taquilleros</span>
        <br><br>
        <div id="chart-3" ></div>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 text-center seccion-btn">
        <button id="btnEstadistica" onclick="regresar();" class="btn btn-primary btn-redirec">Regresar</button>
    </div>
</div>
</div>
<script>
    function regresar() {
        location.href = getURLBase()+"publics/index/<?=$torneo?>/10/<?=$host?>";
    }
</script>
