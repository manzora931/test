<?php $this->layout = 'ajax' ?>
<?php if ($totalDenuncias) { ?>
    <div class="row">
        <?php //edades ?>
        <div class="col-xs-6">
            <div class="panel panel-primary informe">
                <div class="panel-heading">&#11163; Edades </div>
                <div class="panel-body">
                    <div class="container-fluid" style="margin-bottom: 37px;">
                        <div class="col-md-4">
                            <canvas id="edadesChart" width="400" height="400"></canvas>
                        </div>
                        <div class="col-xs-8">
                            <table id="informeEdades" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Edades</th>
                                        <th>Cantidad</th>
                                        <th>Color</th>
                                        <th>Porcentaje</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($dataEdades as $key => $dataEdad) { ?>
                                    <tr>
                                        <td class="text-left"><?= $dataEdad['titulo'] ?></td>
                                        <td><?= $dataEdad['cantidad'] ?></td>
                                        <td><span class="color"
                                        style="background-color: <?= $dataEdad['color'] ?>"></span></td>
                                        <td><?= round(($dataEdad['cantidad'] / $totalDenuncias) * 100, 2) ?>%</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(function () {
                    createChart({
                        id: "#edadesChart",
                        labels: [
                            <?php foreach ($dataEdades as $key => $data) { ?>
                                "<?= $data['titulo'] ?>",
                            <?php } ?>
                        ],
                        data: [
                            <?php foreach ($dataEdades as $key => $data) { ?>
                                "<?= $data['cantidad'] ?>",
                            <?php } ?>
                        ]
                    });
                });
            </script>
        </div>
        <?php //géneros ?>
        <?php if ($totalEntrevistas) { ?>    
            <div class="col-xs-6">
                <div class="panel panel-primary informe">
                    <div class="panel-heading">&#11163; Géneros </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="col-md-4">
                                <canvas id="generosChart" width="400" height="400"></canvas>
                            </div>
                            <div class="col-xs-8">
                                <table id="informeGeneros" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Género</th>
                                            <th>Cantidad</th>
                                            <th>Color</th>
                                            <th>Porcentaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dataGeneros as $key => $dataGenero) { ?>
                                        <tr>
                                            <td class="text-left"><?= $dataGenero['titulo'] ?></td>
                                            <td><?= $dataGenero['cantidad'] ?></td>
                                            <td><span class="color"
                                            style="background-color: <?= $dataGenero['color'] ?>"></span></td>
                                            <td><?= round(($dataGenero['cantidad'] / $totalEntrevistas) * 100, 2) ?>%</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    createChart({
                        id: "#generosChart",
                        labels: [
                            <?php foreach ($dataGeneros as $key => $data) { ?>
                                "<?= $data['titulo'] ?>",
                            <?php } ?>
                        ],
                        data: [
                            <?php foreach ($dataGeneros as $key => $data) { ?>
                                "<?= $data['cantidad'] ?>",
                            <?php } ?>
                        ]
                    });
                </script>
            </div>
        <?php } ?>
    </div>
    <?php if ($totalEntrevistas) { ?>  
        <div class="row">
            <?php //orientación sexual ?>
            <div class="col-xs-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">&#11163; Orientación Sexual </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="col-md-4">
                                <canvas id="orientacionChart" width="400" height="400"></canvas>
                            </div>
                            <div class="col-xs-8">
                                <table id="informeOrientacion" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Orientación Sexual</th>
                                            <th>Cantidad</th>
                                            <th>Color</th>
                                            <th>Porcentaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dataOrientacionSexual as $key => $orientacionSexual) { ?>
                                            <tr>
                                                <td class="text-left"><?= $orientacionSexual['titulo'] ?></td>
                                                <td><?= $orientacionSexual['cantidad'] ?></td>
                                                <td><span class="color"
                                                style="background-color: <?= $orientacionSexual['color'] ?>"></span></td>
                                                <td><?= round(($orientacionSexual['cantidad'] / $totalEntrevistas) * 100, 2) ?>%</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    createChart({
                        id: "#orientacionChart",
                        labels: [
                            <?php foreach ($dataOrientacionSexual as $key => $data) { ?>
                                "<?= $data['titulo'] ?>",
                            <?php } ?>
                        ],
                        data: [
                            <?php foreach ($dataOrientacionSexual as $key => $data) { ?>
                                "<?= $data['cantidad'] ?>",
                            <?php } ?>
                        ]
                    });
                </script>
            </div>
            <?php //tipo de derecho violentado ?>
            <div class="col-xs-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">&#11163; Tipo de derecho violentado </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="col-md-4">
                                <canvas id="tipoDerechoChart" width="400" height="400"></canvas>
                            </div>
                            <div class="col-xs-8">
                                <table id="informeDerechos" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Tipo de Derecho</th>
                                            <th>Cantidad</th>
                                            <th>Color</th>
                                            <th>Porcentaje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dataDerechoViolentado as $key => $derechoViolentado) { ?>
                                            <tr>
                                                <td class="text-left"><?= $derechoViolentado['titulo'] ?></td>
                                                <td><?= $derechoViolentado['cantidad'] ?></td>
                                                <td><span class="color"
                                                style="background-color: <?= $derechoViolentado['color'] ?>"></span></td>
                                                <td><?= round(($derechoViolentado['cantidad'] / $totalEntrevistas) * 100, 2) ?>%</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    createChart({
                        id: "#tipoDerechoChart",
                        labels: [
                            <?php foreach ($dataDerechoViolentado as $key => $data) { ?>
                                "<?= $data['titulo'] ?>",
                            <?php } ?>
                        ],
                        data: [
                            <?php foreach ($dataDerechoViolentado as $key => $data) { ?>
                                "<?= $data['cantidad'] ?>",
                            <?php } ?>
                        ]
                    });
                </script>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <?php //instituciones ?>
        <div class="col-xs-6">
            <table id="informeInstituciones" class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre de la Institución donde han sucedido los hechos</th>
                        <th>Cantidad</th>
                        <th>Porcentaje</th>
                        <th>País</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataInstitucionesPorPais as $pais_id => $instituciones) { ?>
                        <?php foreach ($instituciones as $nombre => $institucion) { ?>
                            <tr>
                                <td class="text-left"><?= $nombre ?></td>
                                <td><?= $institucion['cantidad'] ?></td>
                                <td><?= round(($institucion['cantidad'] / $totalDenuncias) * 100, 2) ?>%</td>
                                <td><?= $paises[$pais_id] ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php //Paises ?>
        <div class="col-xs-6">
            <table id="informePaises" class="table table-striped">
                <thead>
                    <tr>
                        <th>País</th>
                        <th>Cantidad</th>
                        <th>Porcentaje</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataPaises as $pais_id => $cantidad) { ?>
                        <tr>
                            <td class="text-left"><?= $paises[$pais_id] ?></td>
                            <td><?= $cantidad ?></td>
                            <td><?= round(($cantidad / $totalDenuncias) * 100, 2) ?>%</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php } else { ?>
    <h3 class="text-center">El país seleccionado no posee denuncias</h3>
<?php } ?>