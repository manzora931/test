<?php
$now = date("Y-m-d H:i:s");
pack("CCC",0xef,0xbb,0xbf);
header ("Expires: ".date('d-m-Y')." GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-type: application/x-msexcel; charset=utf-8");
header ("Content-Disposition: attachment; filename=\"Cotizaciones_estados_por_pais_".$now.".xls" );
header ("Content-Description: solicitud_cotizacion_pais" ); ?>
<?php $this->layout = false ?>
<?php $principal = '' //campo principal en la tabla ?>
<?php switch ($informe) { 
    case 'edades':
        $principal = 'Edades';
        break;
} ?>
<table>
    <thead style="background-color: red">
        <th><?= $principal ?></th>
        <th>Cantidad</th>
        <th>Porcentaje</th>
    </thead>
    <tbody>
        <?php foreach ($data as $key => $data) { ?>
            <tr>
                <td><?= $data['titulo'] ?></td>
                <td><?= $data['cantidad'] ?></td>
                <td><?= round(($data['cantidad'] / $totalDenuncias) * 100, 2) ?>%</td>
            </tr>
        <?php } ?>
    </tbody>
</table>