<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'proyecto/impresion')); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php
    $page = 1;
    $meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
    $fecha = date('d-m-Y');
    $fechaactual = explode('-', $fecha);
    $mes=$meses[$fechaactual[1]];

    //debug($resultados);
;?>
<div class="container" style="margin-bottom: 35px;">
    <div class="col-print-12">
        <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
        <label class="org"><?= $org ?></label>
        <br>
        <label class="tittle"><?= __('Software Sports Media Analytics'); ?></label>
        <div class="linea"></div>
        <label class="encabezado"><?= 'Estadísticas Generales por Torneo'; ?></label>
    </div>
    <div class="clearfix"></div>
    <div class="col-print-12" >
        <div class="actions do-not-print">
            <ul>
                <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-print-12" >
        <label class="tab-encabezado"></label>
    </div>
    <div class="col-print-12" style="margin-bottom: 15px;">
        <span class="title-graf">Máximos Goleadores</span>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped"  style="box-shadow: 0 0 7px 0px #c8c9d0;">
            <thead>
            <tr>
                <th width="50%">Jugador</th>
                <th width="35%" class="text-center">Equipo</th>
                <th width="15%" class="text-center">Goles</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;
            foreach ($goleadores as $item): ?>
            <tr>
                <td class="text-left"><?= $item['Vregistrogole']['jugador']; ?>&nbsp</td>
                <td class="text-left"><?= $item["Vregistrogole"]["equipo"]?></td>
                <td class="text-center"><?= $item["Vregistrogole"]["goles"] ?></td>
            </tr>
                <?php
                $i++;
                endforeach; ?>
        </table>
    </div>
    <div class="col-print-12" style="margin-bottom: 15px;">
        <span class="title-graf">Equipos más Goleadores</span>
        <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped"  style="box-shadow: 0 0 7px 0px #c8c9d0;">
            <thead>
            <tr>
                <th width="80%" class="text-center">Equipo</th>
                <th width="15%" class="text-center">Goles</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=0;
            foreach ($golexequipos as $item): ?>
                <tr>
                    <td class="text-left"><?= $item["Vgolesxequipo"]["equipo"]?></td>
                    <td class="text-center"><?= $item["Vgolesxequipo"]["golafavor"] ?></td>
                </tr>
                <?php
                $i++;
            endforeach; ?>
        </table>
    </div>
</div>
