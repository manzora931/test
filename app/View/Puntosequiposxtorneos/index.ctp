<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';?>
<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="recursos index container-fluid">

    <?php
    if( isset( $_SESSION['punto_delete'] ) ) {
        if( $_SESSION['punto_delete'] == 1 ){
            unset( $_SESSION['punto_delete'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro eliminado
            </div>
        <?php }
    } ?>
    <h2><?php echo __('Puntos de Equipo por Torneo'); ?></h2>
    <p>
        <?php
        echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));
        ?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "puntosequiposxtorneos";
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table style='width: 50%;'>";
        echo "<tr>";
        //echo "<td width='35%'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array("type"=>"hidden",'label'=>'Buscar por: Nombre, Apellido, Apodo', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre, Apellido, Apodo'));
        //echo "</td>";
        echo "<td style='width: 15%; padding-left: 15px;'>";
        echo $this->Form->input('torneo_id',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][torneo_id]',
            'options'=>$torneos,
            'label'=>'Torneo',
            'type'=>'select',
            "onchange"=>"getEquipos(this.value,0)",
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['torneo_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";
        echo "<td style='width: 15%; padding-left: 4px;'>";
        echo $this->Form->input('equipo_id',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][equipo_id]',
            'options'=>[],
            'label'=>'Equipo',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['equipo_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";
        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Puntosequiposxtorneo', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
    <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('torneo.nombrecorto', 'Torneo'); ?></th>
            <th><?php echo $this->Paginator->sort('equipo.nombrecorto', 'Equipo'); ?></th>
            <th><?php echo $this->Paginator->sort('puntos', 'Puntos'); ?></th>
            <th><?php echo $this->Paginator->sort('activo', 'Activo'); ?></th>
            <th class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i=0;
        foreach ($puntosequiposxtorneos as $row):
            $class=null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?= $class;?> >
                <td><?= h($row['Puntosequiposxtorneo']['id']); ?>&nbsp;</td>
                <td style='text-align:left'><?= h($row['Torneo']['nombrecorto']); ?>&nbsp;</td>
                <td style='text-align:left'><?= h($row['Equipo']['nombrecorto']); ?>&nbsp;</td>
                <td style='text-align:center;'><?= h($row['Puntosequiposxtorneo']['puntos']); ?>&nbsp;</td>
                <td style='text-align:center'><?= ($row['Puntosequiposxtorneo']['activo']==1)?"Si":"No"; ?></td>
                <td class="">
                    <?php echo $this->Html->link(__(' '), array('action' => 'view', $row['Puntosequiposxtorneo']['id']),array('class'=>'ver')); ?>
                    <?php echo $this->Html->link(__(' '), array('action' => 'edit', $row['Puntosequiposxtorneo']['id']),array('class'=>'editar')); ?>
                    <?php // echo $this->Html->link(__(' '), array('action' => 'delete', $row['Puntosequiposxtorneo']['id']),array('class'=>'deleteDet')); ?>

                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Agregar'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
    </div>
</div>

<script>
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);


    });
    function getEquipos(torneo,equipo) {
        var url=getURL()+"puntosequiposxtorneos/getEquipos";

        if(torneo!==''){
            $.ajax({
                url: url,
                type: 'post',
                data: {torneo: torneo,equipo:equipo},
                cache: false,
                dataType: 'json',
                success: function (resp) {
                    if(resp["error"]===0){
                        console.log(resp);
                        $("#puntosequiposxtorneosEquipoId").html(resp["option"]);
                    }
                }
            });
        }
    }
</script>
<?php
if (isset($_SESSION['tabla['.$tabla.']']['torneo_id'])){    ?>
    <script>
        var torneo_id = <?=$_SESSION['tabla['.$tabla.']']['torneo_id'];?>;
        var equipo_id = <?=$_SESSION['tabla['.$tabla.']']['equipo_id'];?>;
        console.log(torneo_id);
        getEquipos(torneo_id,equipo_id);
    </script>
<?php } ?>