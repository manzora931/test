<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style>
<div class="bancos view container-fluid">
    <h2><?= "Puntos de Equipo por Torneo"; ?></h2>
    <?php
    if( isset( $_SESSION['punto_save'] ) ) {
        if( $_SESSION['punto_save'] == 1 ){
            unset( $_SESSION['punto_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($puntosequiposxtorneo['Puntosequiposxtorneo']['id']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Torneo'); ?></td>
            <td>
                <?php echo h($puntosequiposxtorneo['Torneo']['nombrecorto']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Equipo'); ?></td>
            <td>
                <?php echo h($puntosequiposxtorneo['Equipo']['nombrecorto']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Puntos'); ?></td>
            <td>
                <?php echo h($puntosequiposxtorneo['Puntosequiposxtorneo']['puntos']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Comentario'); ?></td>
            <td>
                <?php echo h($puntosequiposxtorneo['Puntosequiposxtorneo']['comentario']); ?>
            </td>
        </tr>

        <tr>
            <td scope="row"><?php echo __('Activo'); ?></td>
            <td>
                <?php echo ($puntosequiposxtorneo['Puntosequiposxtorneo']['activo']==1)?"Si":"No"; ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Creado'); ?></td>
            <td>
                <?= $puntosequiposxtorneo['Puntosequiposxtorneo']['usuario']." (".$puntosequiposxtorneo['Puntosequiposxtorneo']['created'].")"; ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($puntosequiposxtorneo['Puntosequiposxtorneo']['usuariomodif']!='')?$puntosequiposxtorneo['Puntosequiposxtorneo']['usuariomodif']." (".$puntosequiposxtorneo['Puntosequiposxtorneo']['modified'].")":""; ?>
            </td>
        </tr>
    </table>

    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar puntos'), array('action' => 'edit', $puntosequiposxtorneo['Puntosequiposxtorneo']['id']),['class'=>"btn btn-default"]); ?> </div>
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),['class'=>"btn btn-default"]); ?></div>

    </div>
</div>



<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>

