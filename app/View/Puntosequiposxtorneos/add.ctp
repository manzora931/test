<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="tareas form container-fluid">
    <?php echo $this->Form->create('Puntosequiposxtorneo',array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Puntos'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $this->Form->input('torneo_id',[
                    'label'=>'Torneo',
                    'class'=>'form-control validate[required]',
                    "onchange"=>"getEquipos(this.value,0)",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('equipo_id',[
                    'label'=>'Equipo',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"valUnique(this.value);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('puntos',[
                    'label'=>'Puntos',
                    'required'=>true,
                    'placeholder'=>'Puntos',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'type'=>"number"
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('comentario',[
                    'label'=>'Comentario',
                    'required'=>true,
                    'placeholder'=>'Comentario',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>300
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
            <?= $this->Form->input('activo',[
                'label'=>'Activo',
                'class' => 'form-control',
                "type"=>"checkbox",
                'div' => ['class'=>'form-group']
            ]);
            ?>
            </div>
        </div>
    </fieldset>
    <div class="actions">
        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    function getEquipos(torneo,equipo) {
        var url=getURL()+"getEquipos";
        if(torneo!==''){
            $.ajax({
                url: url,
                type: 'post',
                data: {torneo: torneo,equipo:equipo},
                cache: false,
                dataType: 'json',
                success: function (resp) {
                    if(resp["error"]===0){
                        $("#PuntosequiposxtorneoEquipoId").html(resp["option"]);
                    }
                }
            });
        }
    }
    function valUnique(equipo) {
        var torneo = $("#PuntosequiposxtorneoTorneoId").val();
        if(equipo!=='' && torneo!==''){
            var url = getURL()+"valUnique";
            var id = 0;
            $.ajax({
                url: url,
                type: 'post',
                data: {torneo:torneo,equipo:equipo,id:id},
                cache:false,
                dataType: 'json',
                success: function(resp){
                    if(resp["error"]==1){
                        $("#msjalert").text("El equipo seleccionado ya tiene un registro en el torneo");
                        $("#alerta").slideDown();
                        setTimeout(function () {
                            $("#alerta").slideUp();
                        },4000);
                        $("#PuntosequiposxtorneoEquipoId").val("");
                        $("#PuntosequiposxtorneoPuntos").val("");
                    }
                }
            });
        }
    }
</script>