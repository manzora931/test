<?= $this->Html->css(['proyecto/main']) ?>
<div class="row">
    <div class="alert alert-danger" id="error_presupuesto" style="display: none">
        <span class="icon icon-cross-circled"></span>
        <span class="txtpres"></span>
    </div>
</div>
<?=$this->Form->input('id',['type'=>'hidden','value'=>$this->data['Presupuesto']['id'],'id'=>'idpre']);?>
<?=
$this->Form->input('actividade_id',[
    'label'=>"Actividad",
    "required"=>true,
    "empty"=>"Seleccionar",
    "class"=>"form-control",
    'selected'=>$this->data['Presupuesto']['actividade_id'],
    "div"=>["class"=>"form-group",
    'options'=>$actividades,
    ]
]);
?>
<?=
$this->Form->input('totalasiganado',[
    'label'=>"Monto Asignado",
    "required"=>true,
    "empty"=>"Seleccionar",
    "class"=>"form-control",
    "div"=>["class"=>"form-group"],
    'type'=>"number",
    'min'=>0,
    'value'=>$this->data['Presupuesto']['totalasignado']
]);
?>
<script>
    jQuery(function(){
        $("#actividade_id").change(function(){
            var actividad = $(this).val();
            var proyecto = '<?=$idProyecto?>';
            var id = '<?=$id?>';
            var url = getURL()+"presupuestos/val";
            $.ajax({
                url:url,
                type:'post',
                data:{actividad:actividad,proyecto:proyecto,id:id},
                success:function(resp){
                    if(resp==="error"){
                        $("#actividade_id").val("");
                        $(".txtpres").text("La actividad seleccionada ya tiene un presupuesto");
                        $("#error_presupuesto").slideDown();
                        setTimeout(function(){
                            $("#error_presupuesto").slideUp();
                        },4000);
                    }
                }
            });
        });
    });
</script>