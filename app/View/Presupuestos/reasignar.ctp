<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning" id="alertMsj" style="display: none">
            <span class="icon icon-check-circled"></span>
            <span class="txt"></span>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input("actividade1_id",[
                    'label'=>'Actividades',
                    'required'=>true,
                    'div'=>['class'=>'groupp-control'],
                    "class"=>"form-control",
                    'empty'=>'Seleccionar',
                    'options'=>$actividades
            ]);?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input("monto",[
                'label'=>'Saldo disponible',
                'required'=>true,
                'id'=>'montoActividad',
                'div'=>['class'=>'groupp-control'],
                "class"=>"form-control"
            ]);?>
        </div>
        <div class="col-md-3">
            <?= $this->Form->input("actividad2_id",[
                'label'=>'Actividad destino',
                'required'=>true,
                'div'=>['class'=>'groupp-control'],
                "class"=>"form-control",
                'empty'=>"Seleccionar"
            ]);?>
        </div>
    </div>
</div>
<script>
    jQuery(function(){
        $("#montoActividad").change(function(){
            var monto = $(this).val();
            var actividad = $("#actividade1_id").val();
            var url = getURL()+"presupuestos/valMontoTranf";
            $.ajax({
                url:url,
                type:'post',
                data:{monto:monto,actividad:actividad},
                cache:false,
                dataType:'json',
                success:function(resp){
                    if(parseFloat(resp.disponible) < parseFloat(monto)){
                        $(".txt").text(resp.msj);
                        $("#alertMsj").slideDown();
                        setTimeout(function(){
                            $("#alertMsj").slideUp();
                        },4000);
                        var disp = parseFloat(resp.disponible);
                        $("#montoActividad").val(disp.toFixed(2));
                    }
                }
            });
        });
        $("#actividade1_id").change(function(){
            var url = getURL()+"presupuestos/getDisponible";
            var actividad_id = $(this).val();
            $.ajax({
                url:url,
                type:'post',
                data:{actividad_id:actividad_id},
                cache:false,
                dataType:'json',
                success:function(resp){
                    var monto = parseFloat(resp['monto']);
                    $("#montoActividad").val(monto.toFixed(2));
                    getAct(resp['fuente'], actividad_id);
                }
            });
        });
    });
    function  getAct(id, actividad) {
        //LA FUNCION RECIBE EL ID DE LA FUENTE DE FINANCIAMIENTO
        var url= getURL()+"presupuestos/getActiv";
        $.ajax({
            url:url,
            type:'post',
            data:{id:id,actividad:actividad},
            cache:false,
            dataType:'json',
            success:function(resp){
                $("#actividad2_id").html("");
                $("#actividad2_id").append(new Option("Seleccionar", ""));
                $.each(resp, function(index, item){
                    $("#actividad2_id").append(new Option(item.Actividade.nombre, item.Actividade.id));
                });
            }
        });
    }
</script>