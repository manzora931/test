<?= $this->Html->css(['proyecto/main']) ?>
<div class="row">
    <div class="alert alert-danger" id="error_presupuesto" style="display: none">
        <span class="icon icon-cross-circled"></span>
        <span class="txtpres"></span>
    </div>
</div>
<?=$this->Form->input('id',['type'=>'hidden','value'=>0,'id'=>'idpre']);?>
<?=
$this->Form->input('actividade_id',[
        'label'=>"Actividad",
        "required"=>true,
        "empty"=>"Seleccionar",
        "class"=>"form-control",
        "div"=>["class"=>"form-group",
        'value'=>$actividades
        ]
]);
?>
<?=
$this->Form->input('totalasiganado',[
    'label'=>"Monto Asignado",
    "required"=>true,
    "empty"=>"Seleccionar",
    "class"=>"form-control",
    "div"=>["class"=>"form-group"],
    'type'=>"number",
    'min'=>0
]);
?>
