<div class="presupuestos index container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger" id="error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txterror"></span>
            </div>
        </div>
        <?php  if(isset($_SESSION['savepresu'])){
            unset($_SESSION['savepresu']);
            ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="alerta" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Presupuesto Almacenada</span>
                </div>
            </div>
        <?php }
        if(isset($_SESSION['presDelete'])) {
            unset($_SESSION['presDelete']); ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="alerta" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Presupuesto Eliminado</span>
                </div>
            </div>
            <?php
        }
        if(isset($_SESSION['savetransfe'])){
            unset($_SESSION['savetransfe']);        ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="alerta" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Reasinación almacenada correctamente</span>
                </div>
            </div>
<?php    }        ?>
    </div>
    <div class="row">
        <?php if($infoP['Proyecto']['estado_id']==1){   ?>
        <div class="col-sm-4">
            <h2 style="font-size: 1.4em;color:#cb071a;">Presupuestos</h2>
        </div><br>
        <div class="col-sm-offset-1 col-sm-7 div-actions">
            <table width="100%">
                <td class="acciones form-group">
                    <button type="button" class="btn btn-crear acciones-shadow"  id="add-details" data-toggle="modal" data-target="#modal-presupuesto">Agregar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="edit-details">Modificar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                </td>
                <td class="acciones form-group">
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/presupuestos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>

            </table>
        <?php   }else{   ?>
        <div class="col-sm-7">
            <h2 style="font-size: 1.4em;color:#cb071a;">Presupuestos</h2>
        </div><br>
        <div class="col-sm-offset-2 col-sm-2 div-actions">
            <table width="100%">
                <?php if($admin == 1){ ?>
                    <td class="acciones form-group">
                        <button type="button" class="btn btn-acciones acciones-shadow" id="asignarF-details" name="button">Reasignar</button>
                    </td>
                <?php } ?>
                <td class="acciones form-group">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/presupuestos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>
            </table>
        <?php   }   ?>
        </div>
        <div class="clearfix"></div>
        <br><br>

        <div class="row">
            <?php
            /**RECORRIDO DE LOS MODULOS DEL PROYECTO**/
            foreach ($modules as $mod){ ?>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <!--a id="btn-proy" role="button" class="" data-toggle="collapse" href="" data-target="#frm"-->
                            <span class="stage-title"><?=$mod['Module']['nombre']?></span>
                            <!--/a-->
                        </h4>
                    </div>
                    <div id="frm" class="" role="tabpanel">
                        <div class="panel-body" id="">
                            <?php
                            /**RECORRIDO DE LAS INTERVENCIONES DE CADA MODULO**/
                            if(count($intervenciones[$mod['Module']['id']])>0){
                                foreach ($intervenciones[$mod['Module']['id']] as $int){
                                    ?>
                                    <div class="panel-default" style="background-color: #606060;">
                                        <div class="panel-heading" role="tab">
                                            <h4 class="panel-title">
                                                <!--a role="button" class="" data-toggle="collapse" data-parent="#formularios"  href=""1-->
                                                <span class="stage-title"><?= $int['nombre'] ?> </span>
                                                <!--/a-->
                                            </h4>
                                        </div>
                                    </div>
                            <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <?= ($infoP['Proyecto']['estado_id']==1)?"<th colspan='2'>Actividad</th>":"<th>Actividad</th>";?>
                                    <th>Total Asignado</th>
                                    <!--th width="">Desembolsado</th-->
                                    <th width="">Disponible</th>
                                    <th width="">Ejecutado</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">
                                    <?php
                                    $sum_asignado=0.00;
                                    $totdesembolso=0.00;
                                    $totdisponible=0.00;
                                    $totejecutado=0.00;
                                    if(count($presupuestos[$int['id']])>0){
                                    foreach ($presupuestos[$int['id']] as $info){
                                        ?>
                                        <tr>
                                            <?php   if($infoP['Proyecto']['estado_id']==1){ ?>
                                            <td>
                                                <input type="radio" name="item-selected" class="item-selected" value="<?=$info['Presupuesto']['id']?>">
                                            </td>
                                            <?php   }   ?>
                                            <td class="text-left"><?=$info['Actividade']['nombre']?></td>
                                            <td style="text-align: right;">$ <?=number_format($info['Presupuesto']['totalasignado'],2)?></td>
                                            <!--td style="text-align: right;">$ <?php //number_format($info['Desembolso']['monto'],2)?></td-->
                                            <td style="text-align: right;">$ <?=number_format($info['Desembolso']['disponible'],2)?></td>
                                            <td style="text-align: right;">$ <?=number_format($info['Desembolso']['ejecutado'],2)?></td>
                                        </tr>
<?php                               $sum_asignado+=$info['Presupuesto']['totalasignado'];
                                    $totdesembolso+=$info['Desembolso']['monto'];
                                    $totdisponible+=$info['Desembolso']['disponible'];
                                    $totejecutado+=$info['Desembolso']['ejecutado'];
                                    }
                                    ?>
                                    <tr class="totales">
                                        <?= ($infoP['Proyecto']['estado_id']==1)?"<td></td>":""; ?>
                                        <td style="text-align: right;"><b>Totales</b></td>
                                        <td style="text-align: right;">$ <?=number_format($sum_asignado,2);?></td>
                                        <!--td style="text-align: right;">$ <?php //number_format($totdesembolso,2)?></td-->
                                        <td style="text-align: right;">$ <?=number_format($totdisponible,2)?></td>
                                        <td style="text-align: right;">$ <?=number_format($totejecutado,2)?></td>
                                    </tr>
                            <?php   }else{
                                        $cols =  ($infoP['Proyecto']['estado_id']==1)?5:4;
                                        ?>
                                        <tr>
                                            <td colspan="<?=$cols?>">Sin Información</td>
                                        </tr>
                            <?php   }        ?>
                                </tbody>
                            </table>
<?php                                           ?>
<?php                           }
                            }
                            ?>

                        </div>
                    </div>


                </div>
            </div>
<?php       }     ?>
        </div>

    </div>

    <div class="modal fade" id="modal-presupuesto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="" id="myModalLabel">Adicionar Presupuesto</h2>
                </div>
                <div class="modal-body">
                    <div id="form-presupuesto"></div>
                    <!-------------------------------------------------------------------->
                </div>
                <!--div class="modal-footer"-->
                <table width="20%">
                    <td class="acciones form-group"><a href="#" class="btn btn-primary" id="save-presupuesto">Almacenar</a></td>
                    <td class="acciones form-group"><a href="#" id="cerrar" class="btn btn-default" data-dismiss="modal">Cancelar</a></td>
                </table><br>
                <!--/div-->
            </div>
        </div>
    </div>
<!--/****************************modal*********************************************************************************-->
    <div class="modal fade" id="modal-reasignar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="" id="myModalLabel">Reasignar Fondos</h2>
                </div>
                <div class="modal-body">
                    <div id="form-reasignacion"></div>
                    <!-------------------------------------------------------------------->
                </div>
                <!--div class="modal-footer"-->
                <table width="20%">
                    <td class="acciones form-group"><a href="#" class="btn btn-primary" id="save-reasignacion">Almacenar</a></td>
                    <td class="acciones form-group"><a href="#" id="closeR" class="btn btn-default" data-dismiss="modal">Cancelar</a></td>
                </table><br>
                <!--/div-->
            </div>
        </div>
    </div>

</div>
<script>
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);

        /*******REASIGNAR FONDOS********/
        $("#asignarF-details").click(function(){

            $("#form-reasignacion").load(getURL()+"presupuestos/reasignar/<?=$idProyecto?>");
            $("#modal-reasignar").modal("show");
        });
        $("#save-reasignacion").click(function(){
            var activ_origen = $("#actividade1_id").val();
            var activ_dest = $("#actividad2_id").val();
            var monto = $("#montoActividad").val();
            var url = getURL()+"presupuestos/transferir";
            var proy = '<?=$idProyecto?>';
            if(activ_origen!='' && activ_dest!=''&& monto>0) {
                $.ajax({
                    url:url,
                    type:'post',
                    data:{activ_origen:activ_origen,activ_dest:activ_dest,monto:monto,proy:proy},
                    cache:false,
                    success:function(resp){
                        if(resp==1){
                            $("#closeR").click();
                            $("#childs").load(getURL()+"presupuestos/index/<?=$idProyecto?>");
                        }
                    }
                });
            }else{

            }
        });
    /****************************************************************************************************************/
        $("#add-details").click(function(){
            $("#form-presupuesto").load(getURL()+"presupuestos/add/<?=$idProyecto?>");
        });
        $("#edit-details").click(function(){
            if($(".item-selected").is(":checked")){
                var idpres = $(".item-selected:checked").val();
                $("#form-presupuesto").load(getURL()+"presupuestos/edit/"+idpres+"-<?=$idProyecto?>");
                $("#modal-presupuesto").modal("show");
            }else{
                $(".txterror").text("Seleccione un presupuesto");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });
        $("#delete-details").click(function(){
            if($(".item-selected").is(":checked")){
                var idpres = $(".item-selected:checked").val();
                var url= getURL()+"presupuestos/delete/";
                var quest = confirm("¿Desea eliminar el presupuesto?");
                if(quest){
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{idpres:idpres},
                        cache:false,
                        success:function (resp) {
                            console.log(resp);
                            var res = parseInt(resp);
                            if(res===1){
                                $("#childs").load(getURL()+"presupuestos/index/<?=$idProyecto?>");
                            }
                        }
                    });
                }
            }else{
                $(".txterror").text("Seleccione un presupuesto");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });


        $("#save-presupuesto").click(function(){
            var id = $("#idpre").val();
            var actividad = $("#actividade_id").val();
            var nactividad = $("#actividade_id  option:selected").text();
            var monto = $("#totalasiganado").val();
            if(monto === '' || actividad===''){
                $(".txtpres").text("Debe completar los campos requeridos");
                $("#error_presupuesto").slideDown();
                setTimeout(function () {
                    $("#error_presupuesto").slideUp();
                }, 4000);
            }else{
                if(monto < 0){
                    $(".txtpres").text("El total asignado no puede ser negativo");
                    $("#error_presupuesto").slideDown();
                    setTimeout(function () {
                        $("#error_presupuesto").slideUp();
                    }, 4000);
                }else{
                    var url = getURL()+"presupuestos/savepresupuesto";
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{actividad:actividad,monto:monto,id:id, nactividad:nactividad},
                        success:function(resp){
                            if(resp==="exito"){
                                $("#cerrar").click();
                                setTimeout(function(){
                                    $("#childs").load(getURL()+"presupuestos/index/<?=$idProyecto?>");
                                },1000);
                            }else{
                                $(".txtpres").text("La fuente de financiamiento no puede cubrir el presupuesto.");
                                $("#error_presupuesto").slideDown();
                                setTimeout(function () {
                                    $("#error_presupuesto").slideUp();
                                }, 4000);
                            }
                        }
                    });
                }
            }
        });
    });
</script>