<?= $this->Html->css(['https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css','proyecto/main']) ?>
    <script>
        jQuery(function() {
            $('.datepicker').datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "1960:<?=date("Y")?>"
            });
            $.datepicker.regional["es"];
        });
    </script>
<div class="row">
    <div class="alert alert-danger" id="error_ac" style="display: none">
        <span class="icon icon-cross-circled"></span>
        <span class="txtact"></span>
    </div>
</div>
<?=$this->Form->input('id',['type'=>'hidden','value'=>0,'id'=>'idrecurso']);?>
<?= $this->Form->input("nombre",[
    'label'=>"Actividad",
    'id'=>'nameact',
    'required'=>true,
    'div'=>['class'=>'form-group'],
    'class'=>'form-control'
]);?>
<?=
$this->Form->input("inicio",[
    'label'=>"Fecha de Inicio",
    'required'=>true,
    'id'=>'finicio',
    'onBlur'=>'RangoFecha();',
    'div'=>['class'=>'form-group'],
    'class'=>'form-control datepicker'
]);
?><br>
<?=
$this->Form->input("limite",[
    'label'=>"Fecha Limite",
    'required'=>true,
    'onBlur'=>'RangoFecha();',
    'id'=>'flimite',
    'div'=>['class'=>'form-group'],
    'class'=>'form-control datepicker'
]);
?><br>
<?=
$this->Form->input("paise_id",[
    'label'=>"País",
    'required'=>true,
    'id'=>'apais_id',
    'div'=>['class'=>'form-group'],
    'options'=>$paises,
    'empty'=>"Seleccionar",
    'class'=>'form-control']);
?>
<?=
$this->Form->input("intervencione_id",[
    'label'=>"Intervención",
    'required'=>true,
    'id'=>'intervencion_id',
    'div'=>['class'=>'form-group'],
    'options'=>$interdata,
    'empty'=>"Seleccionar",
    'class'=>'form-control']);
?>
<?=
$this->Form->input("fuentesfinanciamiento_id",[
    'label'=>"Fuente de Financiamiento",
    'required'=>true,
    'id'=>'ffinanciamiento_id',
    'div'=>['class'=>'form-group'],
    'options'=>$fuentesfinan,
    'empty'=>"Seleccionar",
    'class'=>'form-control']);
?>
<?=
$this->Form->input("finalizado",[
    'label'=>"Finalizado",
    'required'=>true,
    'id'=>'afinalizado',
    'div'=>['class'=>'form-group'],
    'class'=>'form-control',
    'type'=>'checkbox']);
?>
<script>
    function RangoFecha(){
        var desde = $("#finicio").val();
        var hasta = $("#flimite").val();
        if(desde !== '' && hasta!==''){
            var cad1 = desde.split("-");
            var cad2 = hasta.split("-");
            var dateStart=new Date(cad1[2]+"/"+(cad1[1])+"/"+cad1[0]+" 00:00");
            var dateEnd=new Date(cad2[2]+"/"+(cad2[1])+"/"+cad2[0]+" 00:00");
            if(dateStart > dateEnd){
                $("#flimite").val("");
                $("#txtact").text("La fecha limite no puede ser menor a la fecha de inicio.");
                $("#error_ac").slideDown();
                setTimeout(function () {
                    $("#error_ac").slideUp();
                }, 4000);
            }
        }
    }
</script>

