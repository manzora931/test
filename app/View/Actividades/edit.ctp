<?= $this->Html->css(['https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css','proyecto/main']) ?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:<?=date("Y")?>"
        });
        $.datepicker.regional["es"];
    });
</script>
<div class="row">
    <div class="alert alert-danger" id="error_ac" style="display: none">
        <span class="icon icon-cross-circled"></span>
        <span class="txtact"></span>
    </div>
</div>
<?= $this->Form->input('id',['type'=>'hidden','value'=>$this->data['Actividade']['id'],'id'=>'idrecurso']);?>
<?= $this->Form->input("nombre",[
    'label'=>"Actividad",
    'id'=>'nameact',
    'required'=>true,
    'div'=>['class'=>'form-group'],
    'class'=>'form-control',
    'value'=>$this->data['Actividade']['nombre']
]);?>
<?=
$this->Form->input("inicio",[
    'label'=>"Fecha de Inicio",
    'required'=>true,
    'id'=>'finicio',
    'div'=>['class'=>'form-group'],
    'class'=>'form-control datepicker',
    'value'=>date("d-m-Y",strtotime($this->data['Actividade']['inicio']))
]);
?><br>
<?=
$this->Form->input("limite",[
    'label'=>"Fecha Limite",
    'required'=>true,
    'id'=>'flimite',
    'div'=>['class'=>'form-group'],
    'class'=>'form-control datepicker',
    'value'=>date("d-m-Y",strtotime($this->data['Actividade']['limite']))
]);
?><br>
<?=
$this->Form->input("paise_id",[
    'label'=>"País",
    'required'=>true,
    'id'=>'apais_id',
    'div'=>['class'=>'form-group'],
    'options'=>$paises,
    'empty'=>"Seleccionar",
    'class'=>'form-control',
    'selected'=>$this->data['Actividade']['paise_id']
    ]);
?>
<?=
$this->Form->input("intervencione_id",[
    'label'=>"Intervención",
    'required'=>true,
    'id'=>'intervencion_id',
    'div'=>['class'=>'form-group'],
    'options'=>$interdata,
    'empty'=>"Seleccionar",
    'class'=>'form-control',
    'selected'=>$this->data['Actividade']['intervencione_id']]);
?>
<?=
$this->Form->input("fuentesfinanciamiento_id",[
    'label'=>"Fuente de Financiamiento",
    'required'=>true,
    'id'=>'ffinanciamiento_id',
    'div'=>['class'=>'form-group'],
    'options'=>$fuentesfinan,
    'empty'=>"Seleccionar",
    'class'=>'form-control',
    'selected'=>$this->data['Actividade']['fuentesfinanciamiento_id']]);
?>
<?php
if($this->data['Actividade']['finalizado']){
    echo $this->Form->input("finalizado",[
        'label'=>"Finalizado",
        'required'=>true,
        'id'=>'afinalizado',
        'div'=>['class'=>'form-group'],
        'class'=>'form-control',
        'type'=>'checkbox',
        'checked'=>true]);
}else{
    echo $this->Form->input("finalizado",[
        'label'=>"Finalizado",
        'required'=>true,
        'id'=>'afinalizado',
        'div'=>['class'=>'form-group'],
        'class'=>'form-control',
        'type'=>'checkbox']);
}

?>

