<table class="table table-condensed table-striped" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th width="40%">Tarea</th>
            <th>Sub Tarea</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if(count($tareas)>0) {
        foreach ($tareas as $tarea) { ?>
            <tr>
                <td class="text-left"><?= $tarea['Tarea']["nombre"] ?></td>
                <td class="text-left">
                    <?php
                    if(isset($subtareas[$tarea['Tarea']['id']]) && count($subtareas[$tarea['Tarea']['id']])>0){
                        foreach ($subtareas[$tarea['Tarea']['id']] as $sub){
                            echo $sub["Subtarea"]["nombre"]."<br>";
                        }
                    }
                    ?>
                </td>
            </tr>
        <?php }
    }else{  ?>
        <tr><td colspan="2">No información disponible</td></tr>
<?php   }    ?>
    </tbody>
</table>