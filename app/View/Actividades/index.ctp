<?= $this->Html->css(['main','proyecto/main']);?>
<style>
    .oculto{
        display: none;
    }
    .cursormod{
        cursor: pointer;
    }
</style>
<div class="actividades index container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger" id="error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txterror"></span>
            </div>
        </div>
        <?php  if(isset($_SESSION['saveact'])){
            unset($_SESSION['saveact']);
            ?>
        <div class="col-sm-12">
            <div class="alert alert-success" id="alerta" style="display: none">
                <span class="icon icon-check-circled"></span>
                <span class="txt">Actividad Almacenada</span>
            </div>
        </div>
        <?php } ?>
        <?php   if(isset($_SESSION['deleteAct'])){
                unset($_SESSION['deleteAct']);  ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="actDelete" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Actividad Eliminada</span>
                </div>
            </div>
<?php           }
                if(isset($_SESSION["actFinal"])){
                    unset($_SESSION["actFinal"]);?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="actFin" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Actividad Finalizada</span>
                </div>
            </div>
<?php           }
                if(isset($_SESSION['actActivar'])){
                    unset($_SESSION["actActivar"]); ?>
             <div class="col-sm-12">
                <div class="alert alert-success" id="actActivada" style="display: none">
                   <span class="icon icon-check-circled"></span>
                      <span class="txt">Actividad Re Activada</span>
                </div>
             </div>
<?php           }
                if(isset($_SESSION["alertaAct"])){
                    $msj = ($_SESSION["alertaAct"]=="Intervencione")?"Intervención Almacenada":"Módulo Almacenado";
                    unset($_SESSION["alertaAct"]); ?>
                    <div class="col-sm-12">
                        <div class="alert alert-success" id="AlertMod" style="display: none">
                            <span class="icon icon-check-circled"></span>
                            <span class="txt"><?=$msj;?></span>
                        </div>
                    </div>
<?php            }   ?>
        <?php
        if(isset($_SESSION["deleteMod"])){
            $msj = ($_SESSION["deleteMod"]=="Intervencione")?"Intervención Eliminada":"Módulo Eliminado";
            unset($_SESSION["deleteMod"]); ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="deleteMod" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt"><?=$msj;?></span>
                </div>
            </div>
        <?php            }   ?>
    </div>

    <div class="row">
        <?php   if($infoP['Proyecto']['estado_id']==1 && $admin==1){ ?>
        <div class="col-sm-4">
            <h2 style="font-size: 1.4em;color:#cb071a;">Actividades</h2>
        </div><br>
        <div class="col-sm-offset-2 col-sm-6 div-actions">

            <table width="100%">
                <td class="acciones form-group">
                    <button type="button" class="btn btn-crear acciones-shadow"  id="add-details" data-toggle="modal" data-target="#miModal">Agregar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="edit-details">Modificar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                </td>
                <td class="acciones form-group">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/actividades', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>
            </table>
            <?php }else{ ?>
            <div class="col-sm-5">
                <h2 style="font-size: 1.4em;color:#cb071a;">Actividades</h2>
            </div><br>
            <div class="col-sm-offset-2 col-sm-5 div-actions">
                <table width="100%">
                    <td class="acciones form-group">
                        <button type="button" class="btn-crear acciones-shadow"  id="finalizarAct">Finalizar Actividad</button>
                    </td>
                    <td class="acciones form-group">
                        <button type="button" class="btn-acciones acciones-shadow" id="activarAct">Re Activar Actividad</button>
                    </td>
                    <td class="acciones form-group">
                        <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/actividades', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                    </td>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <br><br>

    </div>
    <div class="row">
        <?php
        foreach($modules as $mod){  ?>
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <!--a id="btn-proy" role="button" class="" data-toggle="collapse" href="" data-target="#frm"-->
                        <div id="name-icon-<?=$mod['Module']['id'];?>">
                            <span class="stage-title"><?=$mod['Module']['nombre']?></span>
                            <?php   if($infoP['Proyecto']['estado_id']==1 && $admin==1){    ?>
                            <a class="editar Act-ed" onclick="edMod('<?=$mod['Module']['id']?>')"></a>
                            <a class="deleteMod" onclick="deleteMod('<?=$mod['Module']['id']?>')"><img class="icon-delete" src="../../img/delete_forever_rojo.png"></a>
                            <?php   }   ?>
                        </div>
                        <table class="oculto" id="tbmod<?=$mod['Module']['id']?>">
                            <tr>
                                <td>Módulo</td>
                                <td width="10px"></td>
                                <td><input type="text" class="form-control" id="nombremod<?=$mod['Module']['id'];?>" value="<?=$mod['Module']['nombre']?>" maxlength="200"></td>
                                <td width="10px"></td>
                                <td><button class="btn btn-default" onclick="submitMod('<?=$mod['Module']['id'];?>')">Almacenar</button></td>
                                <td width="10px"></td>
                                <td><button class="btn btn-default" onclick="cerrarEd('<?=$mod['Module']['id']?>')">Cancelar</button></td>
                            </tr>
                        </table>
                        <!--/a-->
                    </h4>
                </div>
                <div id="frm" class="" role="tabpanel">
                    <div class="panel-body" id="">
                        <?php
                        /*********************RECORRIDO DE LAS INTERVENCIONES*************************/
                        if(count($intervenciones[$mod['Module']['id']])>0){
                            foreach($intervenciones[$mod['Module']['id']] as $inter){   ?>
                                <div class="panel-default" style="background-color: #606060;">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <!--a role="button" class="" data-toggle="collapse" data-parent="#formularios"  href=""1-->
                                            <div id="icon-name-int<?=$inter['id']?>">
                                                <span class="stage-title"><?= $inter['nombre'] ?> </span>
                                                <?php   if($infoP['Proyecto']['estado_id']==1 && $admin==1){    ?>
                                                <a class="editar ed-Int" onclick="edInt('<?=$inter['id']?>')"></a>
                                                <a class="deleteMod" onclick="deleteInt('<?=$inter['id']?>')"><img class="icon-delete" src="../../img/delete_forever_rojo.png"></a>
                                                <?php   }   ?>
                                            </div>
                                            <table class="oculto" id="tbint<?=$inter['id']?>">
                                                <tr>
                                                    <td>Intervención</td>
                                                    <td width="10px"></td>
                                                    <td><input type="text" class="form-control" id="nombreInt<?=$inter['id'];?>" value="<?=$inter['nombre']?>" maxlength="200"></td>
                                                    <td width="10px"></td>
                                                    <td><button class="btn btn-default" onclick="submitInt('<?=$inter['id'];?>')">Almacenar</button></td>
                                                    <td width="10px"></td>
                                                    <td><button class="btn btn-default" onclick="cerrarInt('<?=$inter['id']?>')">Cancelar</button></td>
                                                </tr>
                                            </table>
                                            <!--/a-->
                                        </h4>
                                    </div>
                                </div>
                                <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th colspan='2'>Actividad</th>
                                        <th><?php echo h('País'); ?></th>
                                        <th width=""><?php echo h('Fecha Inicio'); ?></th>
                                        <th width=""><?php echo h('Fecha Limite'); ?></th>
                                        <th width=""><?php echo h('Fuente de Financiamiento'); ?></th>
                                        <th width=""><?php echo h('Finalizada'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody">
                                    <?php
                                /*************************RECORRIDO DE ACTIVIDADES************************/
                                    if(isset($actividades[$inter['id']])){
                                        foreach($actividades[$inter['id']] as $act){    ?>
                                            <tr>
                                                <?php
                                                //if($infoP['Proyecto']['estado_id']==1){    ?>
                                                <td>
                                                    <input type="radio" name="item-selected" class="item-selected" value="<?= $act['Actividade']['id'] ?>">
                                                </td>
                                        <?php //} ?>
                                                <td class="text-left"><span class="cursormod" onclick="ShowT(<?= $act['Actividade']['id'] ?>, '<?= $act['Actividade']['nombre']?>')"><?= $act['Actividade']['nombre']?></span></td>
                                                <td class="text-left"><?= $act['Paise']['pais']?></td>
                                                <td class="text-left"><?php
                                                    $mes = date("n",strtotime($act['Actividade']['inicio']));
                                                    echo date("d",strtotime($act['Actividade']['inicio']))."-".$meses[$mes].date("Y",strtotime($act['Actividade']['inicio']));?></td>
                                                <td class="text-left"><?php
                                                    $mes2 = date("n",strtotime($act['Actividade']['limite']));
                                                    echo date("d",strtotime($act['Actividade']['limite']))."-".$meses[$mes2].date("Y",strtotime($act['Actividade']['limite']));
                                                    ?></td>
                                                <td class="text-left">
                                                    <?= $fuentesfinan[$act['Actividade']['fuentesfinanciamiento_id']];?>
                                                </td>
                                                <td><?= ($act["Actividade"]['finalizado']==1)?"Si":"No";?></td>
                                            </tr>
<?php                                   }
                                    }   ?>
                                    </tbody>
                                </table>
                                <br>

  <?php                     }
                            if($infoP['Proyecto']['estado_id']==1){ ?>

                            <div class="panel-default" style="background-color: #606060;">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <!--a role="button" class="" data-toggle="collapse" data-parent="#formularios"  href=""1-->
                                        <span class="stage-title cursormod" id="lblnewInt<?=$mod['Module']['id'];?>" onclick="nuewInt(<?=$mod['Module']['id'];?>);" data-module="<?=$mod['Module']['id'];?>">Crear Intervención</span>
                                        <table class="oculto" id="tbinter<?=$mod['Module']['id'];?>">
                                            <tr>
                                                <td>Intervención</td>
                                                <td width="10px"></td>
                                                <td><input type="text" maxlength="200" class="form-control" id="intervencion<?=$mod['Module']['id'];?>"></td>
                                                <td width="10px"></td>
                                                <td><button class="btn btn-default" onclick="saveInt(<?=$mod['Module']['id'];?>);" id="btnsaveinter">Almacenar</button></td>
                                                <td width="10px"></td>
                                                <td><button class="btn btn-default" onclick="cancelInt(<?=$mod['Module']['id'];?>);" id="cancelarinter">Cancelar</button></td>
                                            </tr>
                                        </table>
                                        <!--/a-->
                                    </h4>
                                </div>
                            </div>
<?php                       }
                        }else{
                            if($infoP['Proyecto']['estado_id']==1) {   ?>
                            <div class="panel-default" style="background-color: #606060;">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <!--a role="button" class="" data-toggle="collapse" data-parent="#formularios"  href=""1-->
                                        <span class="stage-title cursormod" id="lblnewInt<?=$mod['Module']['id'];?>" onclick="nuewInt(<?=$mod['Module']['id'];?>);" data-module="<?=$mod['Module']['id'];?>">Crear Intervención</span>
                                        <table class="oculto" id="tbinter<?=$mod['Module']['id'];?>">
                                            <tr>
                                                <td>Intervención</td>
                                                <td width="10px"></td>
                                                <td><input type="text" maxlength="200" class="form-control" id="intervencion<?=$mod['Module']['id'];?>"></td>
                                                <td width="10px"></td>
                                                <td><button class="btn btn-default" onclick="saveInt(<?=$mod['Module']['id'];?>);" id="btnsaveinter">Almacenar</button></td>
                                                <td width="10px"></td>
                                                <td><button class="btn btn-default" onclick="cancelInt(<?=$mod['Module']['id'];?>);" id="cancelarinter">Cancelar</button></td>
                                            </tr>
                                        </table>
                                        <!--/a-->
                                    </h4>
                                </div>
                            </div>
<?php                       }
                        }                 ?>
                    </div>
                </div>
            </div>

        </div>
<?php   }
        /***************************************Seccion para agregar modulos nuevos****************************************/
        if($infoP['Proyecto']['estado_id']==1){        ?>
        <div class="col-md-12">
            <div class="panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <span class="stage-title cursormod" id="newMod">Crear Módulo</span>
                        <table class="oculto" id="tbmod">
                            <tr>
                                <td>Módulo</td>
                                <td width="10px"></td>
                                <td><input type="text" class="form-control" id="nombremod" maxlength="200"></td>
                                <td width="10px"></td>
                                <td><button class="btn btn-default" id="btnsavemod">Almacenar</button></td>
                                <td width="10px"></td>
                                <td><button class="btn btn-default" id="cancelarsave">Cancelar</button></td>
                            </tr>
                        </table>

                    </h4>
                </div>
            </div>
        </div>
<?php   }   ?>
    </div>
    <div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="" id="myModalLabel">Adicionar Actividad</h2>
                </div>
                <div class="modal-body">
                    <div id="form-actividad"></div>
                    <!-------------------------------------------------------------------->
                </div>
                <!--div class="modal-footer"-->
                <table width="20%">
                    <td class="acciones form-group"><a href="#" class="btn btn-primary" id="saveact">Almacenar</a></td>
                    <td class="acciones form-group"><a href="#" id="cerrar" class="btn btn-default" data-dismiss="modal">Cancelar</a></td>
                </table><br>
                <!--/div-->
            </div>
        </div>
    </div>
    <!------------------------------------------------------------------------------>
        <div class="modal fade" id="miModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="" id="titulo_actividad"></h2>
                    </div>
                    <div class="modal-body">
                        <div id="list_tareas"></div>
                        <!-------------------------------------------------------------------->
                    </div>
                    <!--div class="modal-footer"-->
                    <table width="20%">

                        <td class="acciones form-group"><a href="#" id="cerrar" class="btn btn-default" data-dismiss="modal">Cerrar</a></td>
                    </table><br>
                    <!--/div-->
                </div>
            </div>
        </div>
</div>
<script>
    /**ELIMINAR DE INTERVENCIONES**/
    function deleteInt(id){
        var confi = confirm("¿Esta seguro que desea eliminar la intervención?");
        if(confi){
            var url = getURL()+"/actividades/deleteInt";
            $.ajax({
                url:url,
                type:'post',
                data:{id:id},
                cache:false,
                success:function(resp){
                    var band = parseInt(resp);
                    if(band === 0){
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                    }else{
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $(".txterror").text("No puede borrar la intervención porque contiene actividades con gastos realizados.");
                        $("#error").slideDown();
                        setTimeout(function(){
                            $("#error").slideUp();
                        },4000);
                    }
                }
            });
        }
    }
    /**ELIMINAR MODULO**/
    function deleteMod(id){
        var confi = confirm("¿Esta seguro que desea eliminar el módulo?");
        if(confi){
            var url = getURL()+"/actividades/deleteMod";
            $.ajax({
                url:url,
                type:'post',
                data:{id:id},
                cache:false,
                success:function(resp){
                    var band = parseInt(resp);
                    if(band === 0){
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                    }else{
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $(".txterror").text("No puede borrar el módulo porque contiene actividades con gastos realizados.");
                        $("#error").slideDown();
                        setTimeout(function(){
                            $("#error").slideUp();
                        },4000);
                    }
                }

            });
        }
    }
    /***FUNCIONES PARA EDITAR UNA INTERVENCION***/
    function edInt(id){
        $("#tbint"+id).removeClass("oculto");
        $("#icon-name-int"+id).css({'display':'none'});
    }
    function cerrarInt(id){
        $("#tbint"+id).addClass("oculto");
        $("#icon-name-int"+id).css({'display':'block'});
    }
    function submitInt(id){
        var inter = $("#nombreInt"+id).val();
        if(inter !== '' && inter !== ' '){
            var url = getURL()+"actividades/updateInt";
            $.ajax({
                url:url,
                type:'post',
                data:{inter:inter,id:id},
                cache:false,
                success:function(resp){
                    var band = parseInt(resp);
                    if(band === 0){
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                    }
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: $(".marg-panel").offset().top
            }, 500);
            $(".txterror").text("Ingrese el nombre de la intervención");
            $("#error").slideDown();
            setTimeout(function(){
                $("#error").slideUp();
            },4000);
            $("#nombreint"+id).focus();
        }
    }
    /**FUNCIONES PARA EDITAR UN MODULO***/
    function edMod(id){
        $("#tbmod"+id).removeClass("oculto");
        $("#name-icon-"+id).css({'display':'none'});
    }
    function cerrarEd(id){
        $("#tbmod"+id).addClass("oculto");
        $("#name-icon-"+id).css({'display':'block'});
    }
    function submitMod(id){
        var mod = $("#nombremod"+id).val();
        if(mod !== '' && mod !== ' '){
            var url = getURL()+"actividades/updateMod";
            $.ajax({
                url:url,
                type:'post',
                data:{mod:mod,id:id},
                cache:false,
                success:function(resp){
                    var band = parseInt(resp);
                    if(band === 0){
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                    }
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: $(".marg-panel").offset().top
            }, 500);
            $(".txterror").text("Ingrese el nombre del módulo");
            $("#error").slideDown();
            setTimeout(function(){
                $("#error").slideUp();
            },4000);
            $("#nombremod"+id).focus();
        }
    }
    /***************************************/
    function ShowT(id, name){
        $("#list_tareas").load(getURL()+"actividades/showt/"+id);
        $("#titulo_actividad").text(name);
        $("#miModal2").modal("show");

    }
    function nuewInt(mod){
        $("#lblnewInt"+mod).css({"display":"none"});
        $("#tbinter"+mod).removeClass("oculto");
    }
    function cancelInt(mod){
        $("#lblnewInt"+mod).css({"display":"block"});
        $("#tbinter"+mod).addClass("oculto");
        $("#intervencion"+mod).val("");
    }
    function saveInt(mod){
        var intervencion = $("#intervencion"+mod).val();
        var proy = $("#lblnewInt"+mod).data("module");
        var url = getURL()+"actividades/savemod";
        $.ajax({
            url:url,
            type:'post',
            data:{proy:proy,name:intervencion,model:"Intervencione",band:2},
            dataType:'json',
            success:function(resp){
                if(resp[0] === "exito"){
                    $('html, body').animate({
                        scrollTop: $(".marg-panel").offset().top
                    }, 500);

                    $("#childs").load('<?= Router::url(['controller'=>'actividades','action'=>'index',$idProyecto])?>');
                }
            }
        });
    }
    jQuery(function(){
        /***RE ACTIVAR ACTIVIDAD***/
        $("#activarAct").click(function(){
            if($(".item-selected").is(":checked")) {
                var conf = confirm("¿Está seguro que desea re activar la actividad?");
                if(conf){
                    var idact = $(".item-selected:checked").val();
                    var url = getURL() + "actividades/activar";
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{idact:idact},
                        cache:false,
                        success:function(resp){
                            var bandera = parseInt(resp);
                            if(bandera===0){
                                $('html, body').animate({
                                    scrollTop: $(".marg-panel").offset().top
                                }, 500);
                                $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                            }else{
                                $('html, body').animate({
                                    scrollTop: $(".marg-panel").offset().top
                                }, 500);
                                $(".txterror").text("La actividad se encuentra activa.");
                                $("#error").slideDown();
                                setTimeout(function () {
                                    $("#error").slideUp();
                                }, 4000);
                            }
                        }
                    });
                }
            }else{
                $(".txterror").text("Seleccione una actividad");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });

        /**FINALIZAR**/
        $("#finalizarAct").click(function(){
            if($(".item-selected").is(":checked")) {
                var conf = confirm("¿Está seguro que desea finalizar la actividad?");
                if(conf){
                    var idact = $(".item-selected:checked").val();
                    var url = getURL() + "actividades/finalizar";
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{idact:idact},
                        cache:false,
                        success:function(resp){
                            var bandera = parseInt(resp);
                            if(bandera===0){
                                $('html, body').animate({
                                    scrollTop: $(".marg-panel").offset().top
                                }, 500);
                                $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                            }else{
                                $('html, body').animate({
                                    scrollTop: $(".marg-panel").offset().top
                                }, 500);
                                $(".txterror").text("La actividad ya fue finalizada.");
                                $("#error").slideDown();
                                setTimeout(function () {
                                    $("#error").slideUp();
                                }, 4000);
                            }
                        }
                    });
                }
            }else{
                $(".txterror").text("Seleccione una actividad");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });
        /**DELETE**/
        $("#delete-details").click(function(){
            /***
             ERRORES SEGUN LA BANDERA
             * 1 => LA ACTIVIDAD TIENE PRESUPUESTO
             * 2 => LA ACTIVIDAD TIENE GASTOS ASIGNADOS
             * 3 => LA ACTIVIDAD TIENE INCIDENCIAS REGISTRADAS
             * 4 => NO TIENE PERMISO DE BORRAR
             ***/
            if($(".item-selected").is(":checked")){
                var idact = $(".item-selected:checked").val();
                var url = getURL()+"actividades/delete/";
                $.ajax({
                    url:url,
                    type:'post',
                    data:{idact:idact},
                    success:function(resp){
                        if(parseInt(resp)===0){
                            $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                        }else{
                            switch (parseInt(resp)) {
                                case 1:
                                    $(".txterror").text("No puede eliminar la actividad porque tiene un presupuesto asignado.");
                                    $("#error").slideDown();
                                    setTimeout(function(){
                                        $("#error").slideUp();
                                    },4000);
                                    break;
                                case 2:

                                    $(".txterror").text("No puede eliminar la actividad porque tiene un gasto realizado.");
                                    $("#error").slideDown();
                                    setTimeout(function(){
                                        $("#error").slideUp();
                                    },4000);
                                    break;
                                case 3:

                                    $(".txterror").text("No puede eliminar la actividad porque tiene incidencias.");
                                    $("#error").slideDown();
                                    setTimeout(function(){
                                        $("#error").slideUp();
                                    },4000);
                                    break;
                                default:

                                    $(".txterror").text("No tiene privilegios para realizar esta acción.");
                                    $("#error").slideDown();
                                    setTimeout(function(){
                                        $("#error").slideUp();
                                    },4000);
                                    break;
                            }
                        }
                    }
                });
            }else{
                $(".txterror").text("Seleccione una actividad");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });

        /****************--------------------***************/
        $("#edit-details").click(function(){
            if($(".item-selected").is(":checked")){
                var idrec = $(".item-selected:checked").val();
                $("#form-actividad").load(getURL()+"actividades/edit/"+idrec+"-<?=$idProyecto?>");
                $("#miModal").modal("show");
            }else{
                $(".txterror").text("Seleccione una actividad");
                $("#error").slideDown();
                setTimeout(function () {
                    $("#error").slideUp();
                }, 4000);
            }
        });
        $("#add-details").click(function(){
            $("#form-actividad").load(getURL()+"actividades/add/<?=$idProyecto?>");
        });
        /******---------------------*****/
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
        /**-----------------------------**/
        $("#actDelete").slideDown();
        setTimeout(function () {
            $("#actDelete").slideUp();
        }, 4000);
        $("#actFin").slideDown();
        setTimeout(function () {
            $("#actFin").slideUp();
        }, 4000);
        $("#actActivada").slideDown();
        setTimeout(function(){
            $("#actActivada").slideUp();
        },4000);

        $("#AlertMod").slideDown();
        setTimeout(function(){
            $("#AlertMod").slideUp();
        },4000);
        $("#deleteMod").slideDown();
        setTimeout(function(){
            $("#deleteMod").slideUp();
        },4000);
        /***-----------------------------***/
        $("#saveact").click(function(){
            var id = $("#idrecurso").val();
            var actividad = $("#nameact").val();
            var finicio = $("#finicio").val();
            var flimite = $("#flimite").val();
            var apais_id = $("#apais_id").val();
            var npais = $("#apais_id  option:selected").text();
            var intervencion_id = $("#intervencion_id").val();
            var nintervencion = $("#intervencion_id  option:selected").text();
            var ffinanciamiento_id = $("#ffinanciamiento_id").val();
            var ffinanciamiento = $("#ffinanciamiento_id  option:selected").text();
            var afinalizado = ($("#afinalizado").is(":checked"))?1:0;
            var url = getURL()+"actividades/save_actividad";
            if(actividad!==''&&finicio!==''&&flimite!==''&&apais_id!==''&&intervencion_id!==''&&ffinanciamiento_id!==''){
                $.ajax({
                    url:url,
                    type:'post',
                    data:{id:id,actividad:actividad,finicio:finicio,flimite:flimite,apais_id:apais_id,intervencion_id:intervencion_id,ffinanciamiento_id:ffinanciamiento_id,afinalizado:afinalizado, npais:npais, nintervencion:nintervencion, ffinanciamiento:ffinanciamiento},
                    success:function(resp){
                        if(resp==="exito"){
                            $("#cerrar").click();
                            setTimeout(function(){
                                $("#childs").load(getURL()+"actividades/index/<?=$idProyecto?>");
                            },1000);
                        }
                    }
                });
            }else{
                /**LLENAR CAMPOS REQUERIDOS**/
                $(".txtact").text("Debe llenar los campos requeridos.");
                $("#error_ac").slideDown();
                setTimeout(function () {
                    $("#error_ac").slideUp();
                }, 4000);
            }

        });
        $("#newMod").click(function(){
            $(this).css({'display':'none'});
            $("#tbmod").removeClass("oculto");
        });
        $("#newinter").click(function(){
            $(this).css({"display":"none"});
            var mod = $("#newinter").data("module");
            console.log(mod);
            $("#tbinter").removeClass("oculto");
        });
       /* $("#btnsaveinter").click(function(){
            var intervencion = $("#intervencion").val();
            var proy = $("#newinter").data("module");
            console.log(proy);
            var url = getURL()+"actividades/savemod";
            $.ajax({
                url:url,
                type:'post',
                data:{proy:proy,name:intervencion,model:"Intervencione",band:2},
                dataType:'json',
                success:function(resp){
                    if(resp[0] === "exito"){
                        $("#tbinter").addClass("oculto");
                        $("#newinter").css({'display':'inline-block'});
                        $("#success .txt").text('Intervención Almacenado exitosamente');
                        $("#success").slideDown();
                        $("#newinter").text(intervencion);
                        setTimeout(function () {
                            $("#success").slideUp();
                            $("#childs").load('');
                        }, 4000);
                    }
                }
            });
        });*/
        /*******************/
        $("#btnsavemod").click(function(){
            var modulo = $("#nombremod").val();
            var proy = '<?=$idProyecto?>';
            var url = getURL()+"actividades/savemod";
            $.ajax({
                url:url,
                type:'post',
                data:{name:modulo,proy:proy,model:'Module',band:1},
                dataType:'json',
                success:function(resp){
                    if(resp[0] == "exito"){
                        $('html, body').animate({
                            scrollTop: $(".marg-panel").offset().top
                        }, 500);
                        $("#childs").load('<?= Router::url(['controller'=>'actividades','action'=>'index',$idProyecto])?>');
                        /*$("#tbmod").addClass("oculto");
                        $("#newMod").css({'display':'inline-block'});
                        $("#newMod").text(modulo);
                        $("#success .txt").text('Módulo Almacenado exitosamente');
                        $("#success").slideDown();
                        setTimeout(function () {
                            $("#success").slideUp();

                        }, 4000);*/
                    }
                }
            });
        });
        $("#cancelarsave").click(function(){
            $("#nombremod").val("");
            $("#tbmod").addClass("oculto");
            $("#newMod").css({'display':'inline-block'});
        });
        /*$("#cancelarinter").click(function(){
            $("#intervencion").val("");
            $("#tbinter").addClass("oculto");
            $("#newinter").css({'display':'inline-block'});
        });*/
    });
</script>