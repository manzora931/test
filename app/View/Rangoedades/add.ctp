<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="rangoedades form container">
    <?php echo $this->Form->create('Rangoedade', array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Rango de Edad'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('nombre',
                array('label' => 'Nombre',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'type' => 'text',
                    'required'=>'required'));?>
            <?php echo $this->Form->input('desde',
                array('label' => 'Desde',
                    'class' => 'form-control entero',
                    'id' => 'desde',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'type' => 'text',
                    'required'=>'required'));?>
            <?php echo $this->Form->input('hasta',
                array('label' => 'Hasta',
                    'class' => 'form-control entero',
                    'id' => 'hasta',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'type' => 'text',
                    'required'=>'required'));?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

    </fieldset>
</div>
<div class="actions">

    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Rangos de Edades'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script type="text/javascript">
    $(function () {
        $('#desde').change(function(){
            var desde = parseInt($(this).val());
            var hasta = parseInt($('#hasta').val());

            if(!isNaN(hasta) && (desde >= hasta) ) {
                $(this).val('');

                $("#alerta .message").text('Error, El valor del campo desde debe ser menor al campo de hasta');
                $("#alerta").slideDown();
                setTimeout(function () {
                    $("#alerta").slideUp();
                }, 4000);
            }
        });

        $('#hasta').change(function(){
            var desde = parseInt($('#desde').val());
            var hasta = parseInt($(this).val());

            if(!isNaN(hasta) && (hasta <= desde) ) {
                $(this).val('');

                $("#alerta .message").text('Error, El valor del campo hasta debe ser mayor al campo de desde');
                $("#alerta").slideDown();
                setTimeout(function () {
                    $("#alerta").slideUp();
                }, 4000);
            }
        });

        $('#almacenar').click(function(){
            $('#formulario').submit();
        });

        $(".entero").keydown(function (e) {
            var edad = $(this).val().length + 1;

            // Allow: backspace, delete, tab, escape, and enter
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }

            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) || (edad > 3)) {
                e.preventDefault();
            }
        });
    });
</script>