<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';?>

<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
</style>
<div class="rangoedades index container">
    <h2><?php echo __('Rangos de Edades'); ?></h2>
    <span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));	?>
	</span>

    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "rangoedades";
    $session = $this->Session->read('tabla[rangoedades]');
    $search_text = $session['search_text'] ;
    ?>
    <div id='search_box'>
        <?php
        /*Inicia formulario de busqueda*/
        $tabla = "rangoedades";
        ?>
        <?php
        echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
        echo "<tr>";
        echo "<td width='400px'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Nombre'));
        echo "</td>";
        echo "<td id='check'>";

        //echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
        if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
            echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked' style='margin: 5px 10px 6px 25px;'>";
        }else{
            echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' style='margin: 5px 10px 6px 25px;'>";
        }
        echo "</td>";
        echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
        echo $this->Form->hidden('Controller', array('value'=>'Rangoedade', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'nombre', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo " <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>";

        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>
        <?php
        if(isset($_SESSION['rangoedades']))
        {
            $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
        }
        ?>	</div>
    <script type="text/javascript">
        var searchBy = $("#search-by");
        var active = $("#active");

        searchBy.val('<?= $session['search-by'] ?>');
        active.val('<?= $session['activo'] ?>');
    </script>
    <table id="tblin" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th class="id bdr1" width="10%"><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nombre','Nombre'); ?></th>
            <th>Rangos de Edades</th>
            <th width="10%"><?php echo $this->Paginator->sort('activo'); ?></th>
            <th class="bdr" width="20%"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rangoedades as $rangoedad): ?>
            <tr>
                <td><?php echo h($rangoedad['Rangoedade']['id']); ?>&nbsp;</td>
                <td class="text-left"><?php echo h($rangoedad['Rangoedade']['nombre']); ?>&nbsp;</td>
                <td class="text-left">De <?php echo h($rangoedad['Rangoedade']['desde']); ?> Años a <?php echo h($rangoedad['Rangoedade']['hasta']); ?>&nbsp;Años</td>
                <td class="text-center"><?php
                    if ($rangoedad['Rangoedade']['activo'] == 1) {
                        echo "Si";
                    }else{
                        echo "No";
                    }
                    ?>
                </td>
                <td width="10%">
                    <?php echo $this->Html->link(__(' '), array('action' => 'view', $rangoedad['Rangoedade']['id']), array('class'=>'ver')); ?>
                    <?php echo $this->Html->link(__(' '), array('action' => 'edit', $rangoedad['Rangoedade']['id']), array('class'=>'editar')); ?>
                    <?php #echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $rangoedad['Rangoedade']['id']), null, __('¿Está seguro de eliminar el rango de edad # %s?', $rangoedad['Rangoedade']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <div class="actions">
        <ul>
            <li class="style-btn"><?php echo $this->Html->link(__('Crear Rango de Edad'), array('action' => 'add'),array('class'=>'btn btn-default')); ?></li>
        </ul>
    </div>
</div>