
<style>
	div.index{
		margin-top: 15px;
		width: 100%;
	}
	#search-by{
		width: 110px;
	}
	table#tblin{
		width: 100%;
	}
	table tr td.form-group {
		padding-left: 15px;
	}
	.table > thead > tr > th.bdr {
		border-top-right-radius: 5px;
		}
	.table > thead > tr > th.bdr1 {
		border-top-left-radius: 5px;
	}
	div.input-group > div{
		display: inline-block;
	}
	div.input-group{
		display: inline-block;
	}
	td#check{
		width: 20%;
		padding-left: 35px;

	}
	.table tbody tr td {
		font-size: 16px;

	}
	.table tbody tr td:first-child {
		width:80px;
	}

	.table tbody tr td:last-child {
		width:110px;

	}

	table.table tbody{
		border:1px solid #ccc;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb ;
	}

	.actions ul .style-btn a{
		font-family: 'Calibri', sans-serif;
		background-image: none;
		background-color: #c0c0c0 ;
		border-color: #606060	;
		border-radius: 1px;
		font-size: 14px;
	}
	.actions {
		margin: -33px 0 0 0;
	}

	.actions ul .style-btn a:hover{
		background: #c0c0c0;
		color:#000;
		border-color: #606060;
	}
	.form-control {
		height: 25px;
		border-radius: 0;
		border: solid 1px #4160a3;
	}
	h2{
		color:#cb071a;
		font-size: 1.4em;
	}
	#search_box {
		border-top: solid 6px;
		border-bottom-color: white;
		border-top-color: #4160a3;

	}

</style>
<div class="tipoinstitucions index container">
	<h2><?php echo __('Perfiles'); ?></h2>
	<span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));	?>
	</span>
	<?php $table = "perfiles";	?>
	<?php $session = $this->Session->read('tabla[perfiles]'); ?>
	<?php $search_text = $session['search_text'] ?>
	<?php $real_url = 'http://'. $_SERVER['HTTP_HOST'] . $this->request->base .'/'. $this->params->controller .'/'; ?>
	<div id='search_box'>
		<?php
		/*Inicia formulario de busqueda*/
		$tabla = "perfiles";
		?>
		<?php
		echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
		echo "<tr>";
		echo "<td width='400px'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Id, Perfil'));
		echo "</td>";
		echo "<td id='check'>";

		//echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked' style='margin: 5px 10px 6px 25px;'>";
		}else{
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' style='margin: 5px 10px 6px 25px;'>";
		}
		echo "</td>";
		echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
		echo $this->Form->hidden('Controller', array('value'=>'Perfile', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'id,perfil', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo " <input type='submit' class='btn btn-default' value='Buscar' style='width: 100px;border-color: #606060;'>";

		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>
		<?php
		if(isset($_SESSION['perfiles']))
		{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
		}
		?>	</div>
	<script type="text/javascript">
		var searchBy = $("#search-by");
		var active = $("#active");

		searchBy.val('<?= $session['search-by'] ?>');
		active.val('<?= $session['activo'] ?>');
	</script>
	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
		<thead>
		<tr>

			<th class="id bdr1"><?php echo $this->paginator->sort('id');?></th>
			<th><?php echo $this->paginator->sort('perfil','Nombre del Perfil');?></th>
            <th class="activo"><?php echo $this->paginator->sort('admin','Administrador');?></th>
			<th class="activo"><?php echo $this->paginator->sort('activo','Activo');?></th>
			<th class="bdr"><?php echo $this->paginator->sort('Acciones');?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = 0;
		foreach ($perfiles as $perfil):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr<?php echo $class;?>>
				<!--<td><input type="radio" name="item-selected" class="item-selected" value="<?= $perfil['Perfile']['id'] ?>"></td>-->
				<td>
					<?php echo $perfil['Perfile']['id']; ?>
				</td>
				<td style="text-align: left;">
					<?php echo $perfil['Perfile']['perfil']; ?>
				</td>
                <td align='center'>
                    <?php
                    if($perfil['Perfile']['admin']==1){
                        $act='Si';
                    }else{
                        $act='No';
                    }

                    echo $act; ?>
                </td>
				<td align='center'>
					<?php
					if($perfil['Perfile']['activo']==1){
						$act='Si';
					}else{
						$act='No';
					}

					echo $act; ?>
				</td>
				<td  width="10%">
					<?php echo $this->Html->link(__(''), array('action' => 'view', $perfil['Perfile']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(''), array('action' => 'edit', $perfil['Perfile']['id']),array('class'=>'editar')); ?>
				</td>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior |', true), array(), null, array('class'=>'disabled'));?>
		<?php echo $this->Paginator->numbers(); ?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<ul>
			<li class="style-btn"><?php echo $this->Html->link(__('Crear Perfil'), array('action'=>'add'),array( 'style'=>'width:50px;','class'=>'btn btn-default')) ?></li>
		</ul>
	</div>
	<script type="text/javascript">
		$(function () {
			var viewDetails = $("#view-details");
			var editDetails = $("#edit-details");
			var cloneEntry = $("#clone-entry");

			viewDetails.click( function () {
				if ($(".item-selected").is(":checked")) {
					window.location.assign( "<?= $real_url ?>" + "view/" + $(".item-selected:checked").val());
				}
			});

			editDetails.click( function () {
				if ($(".item-selected").is(":checked")) {
					window.location.assign( "<?= $real_url ?>" + "edit/" + $(".item-selected:checked").val());
				}
			});

			cloneEntry.click( function () {
				if ($(".item-selected").is(":checked")) {
					window.location.assign( "<?= $real_url ?>" + "clonar/" + $(".item-selected:checked").val());
				}
			});
		});
	</script>
</div>

