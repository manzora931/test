<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>
<style>
	div.form{
		width: 100%;
		margin-bottom: 15px;
	}
	.btn-default{
		font-family: 'Calibri', sans-serif;
		margin-left: 15px;
		padding: 0 5px 0 5px;
		background-color: #c0c0c0 ;
		border:1px solid #606060 ;
		font-size: 14px;
		color: #000;
		border-radius: 1px;

	}
	.btn-default:hover{
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border:1px solid #606060 ;
		color: #000;
		border-radius: 1px;
	}
	fieldset{
		width: 80%;
		margin-top: 15px;
		padding: 15px 15px 15px 15px;
		border:1px solid #cb071a;
		color: #011880;

	}
	fieldset > div label{
		padding-top: 2px;
		color: #011880;
	}
	legend{
		color: #cb071a;
		font-size: 1.4em;
		font-weight: bold;
	}

	div.actions>div{
		display: inline-block;
	}
	.form-group{
		width:700px;
	}
</style>
<div class="perfiles form container">

	<?php echo $this->Form->create('Perfile' ,array('id'=>'frm','class'=>"add-edit"));?>
	<fieldset>
		<legend><?php echo "Editar Perfil";?></legend>
		<div class="alert alert-danger col-xs-12 col-md-12" id="alerta" style="display: none">
			<span class="icon icon-cross-circled"></span>
			<span class="message"></span>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<div class="col-xs-12">
			<?php
			echo $this->Form->input('id');
			echo $this->Form->input('perfil',
				array('label' => 'Perfil',
					'class' => 'form-control',
					'div' => ['class'=>'form-group'],
					'required'=>'required'));
            echo	$this->Form->input('admin',
                array('label' => 'Administrador',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>''));
			?>
			<div class="form-group">

				<?= $this->Form->input('activo', [
					'label' => 'Activo',
					'type' => 'checkbox',
					'div' => false,
					'style'=>'margin:5px;'
				]); ?>
			</div>
		</div>
	</fieldset>
</div>
<div class="actions">

	<div><?= $this->Form->button('Almacenar', [
			'label' => false,
			'type' => 'submit',
			'class' => 'btn btn-default',
			'div' => [
				'class' => 'form-group'
			]
		]); ?>
		<?php echo $this->Form->end();?></div>
	<div><?php echo $this->Html->link(__('Listado de Perfiles'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script>
	jQuery(function(){
		var validate = {
			id : "#PerfilePerfil",
			form: "#frm",
			id_per:"#PerfileId",
			message : "El perfil ingresado ya existe, ingrese uno diferente"
		};

		$(validate.id).change( function(){
			var val = $(validate.id).val();
			var id =  $(validate.id_per).val();
			console.log(id);
			var url = getURL() + 'val_per';

			validate_name(val, id, url, '');
		});

		$(validate.form).on("submit", function (e) {
			var val = $(validate.id).val();
			var id = $(validate.id_per).val();
			var url = getURL() + 'val_per';
			var form = this;

			e.preventDefault();
			validate_name(val, id, url, form);
		});

		// funcion para validar si nombre ya existe en la base de datos
		function validate_name(val, id, url, form) {
			$.ajax({
				url: url,
				type: 'post',
				data: { val:val, id:id},
				cache: false,
				success: function(resp) {
					if ( resp == "error" ) {
						$("#alerta .message").text(validate.message);
						$("#alerta").slideDown();
						$(validate.id).val("");

						setTimeout(function () {
							$("#alerta").slideUp();
						}, 4000);
					} else {
						form.submit();
					}
				}
			});
		}



        $(document).on("change", "#PerfileAdmin", function()
        {
            if( $(this).is(':checked'))
            {
                var id =$("#PerfileId").val();
                var url=getURL() + 'valPri';
                $.ajax({
                    data: {dato: $(this).val(),id:id},
                    url: url,
                    timeout: 15000,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {

                        if (data== "error" ) {
                            $("#alerta .message").text("El perfil administrador ya existe.");
                            $("#alerta").slideDown();
                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                            $("#PerfileAdmin").prop("checked", "");
                        }
                    }
                });
            }
        });

	});
</script>
