<?php
echo $this->Html->Script('jquery', FALSE);
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="pefiles form container">
    <h2>Perfiles</h2>
    <hr>
    <?php echo $this->Form->create('Perfile' ,array('id'=>'frm','class'=>'add-edit'));?>
    <fieldset>
        <legend><?php echo __('Nuevo Registro');?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="floating-buttons">
            <?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-info btn-icon btn-sm btn-save',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?=  $this->Html->link('Cancelar', [
                'action' => 'index'
            ], [
                'class' => 'btn btn-info btn-icon btn-sm btn-cancel'
            ]); ?>
        </div>
        <div class="col-xs-12">
            <?php echo	$this->Form->input('perfil',
                array('label' => 'Perfil',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));?>
            <div class="form-group">
                <label for="PerfileActivo">Activo</label>
                <?= $this->Form->input('activo', [
                    'label' => false,
                    'type' => 'checkbox',
                    'div' => false,
                    'checked' => true
                ]); ?>
            </div>
            <?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-info btn-icon btn-save btn-sm',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
        </div>


    </fieldset>
    <?php echo $this->Form->end();?>
</div>
<script>

    jQuery(function(){
        var validate = {
            "id" : "#PerfilePerfil",
            "form" : "#PerfileAddForm",
            "message" : "El perfil ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_per';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_per';
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val, id},
                cache: false,
                success: function(resp) {
                    if ( resp == "error" ) {
                        $("#alerta .message").text(validate.message);
                        $("#alerta").slideDown();
                        $(validate.id).val("");

                        setTimeout(function () {
                            $("#alerta").slideUp();
                        }, 4000);
                    } else {
                        form.submit();
                    }
                }
            });
        }
    });
</script>