<style>
	.btn-default{
		font-family: 'Calibri', sans-serif;
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border: 1px solid #606060	;
		font-size: 14px;
		color: #000;
		border-radius: 1px;

	}
	.btn-default:hover{
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border:1px solid #606060 ;
		color: #000;
		border-radius: 1px;
	}
	div.view{
		margin-top:15px;
		width: 100%;
	}
	table#tblview tr td:first-child{
		width: 200px;
		text-align: left;
		font-weight: bold;
	}
	h2{
		color:#cb071a;
		font-size: 1.4em;
	}
	div.actions{
		display: inline-block;
	}
	div.actions div > a{
		padding: 0 5px 0 5px;
	}

	div.actions>div{
		display: inline-block;
	}
	table#tblview tr td:first-child {
		width: 200px;
		text-align: left;
		font-weight: bold;
		color: #011880;
	}

</style>
<div class="perfiles view container">
	<h2><?php  echo __('Perfil');?></h2>
	<table width="100%">
		<tr>
			<td colspan="2">
				<?php
				if( isset( $_SESSION['perfil_save'] ) ) {
					if( $_SESSION['perfil_save'] == 1 ){
						unset( $_SESSION['perfil_save'] );
						?>
						<div class="alert alert-success" id="alerta">
							<span class="icon icon-check-circled"></span>
							Perfil Almacenado
							<button type="button" class="close" data-dismiss="alert"></button>
						</div>
					<?php }
				} ?>
			</td>
		</tr>
	</table>
	<table id="tblview">
		<tr>

			<td colspan="2">
				<?php
				if( isset( $_SESSION['perfil_save'] ) ) {
					if( $_SESSION['perfil_save'] == 1 ){
						unset( $_SESSION['perfil_save'] );
						?>
						<div class="alert alert-success" id="alerta">
							<span class="icon icon-check-circled"></span>
							Perfil Almacenado
							<button type="button" class="close" data-dismiss="alert"></button>
						</div>
					<?php }
				} ?>
			</td>
		</tr>
		<tr>
			<td>Id</td>
			<td><?= $perfile['Perfile']['id']; ?></td>
		</tr>
		<tr>
			<td>Perfil</td>
			<td><?=$perfile['Perfile']['perfil']; ?></td>
		</tr>
        <tr>
            <td>Administrador</td>
            <td><?=($perfile['Perfile']['admin']==1)?"Si":"No"; ?></td>
        </tr>
		<tr>
			<td>Activo</td>
			<td><?= ($perfile['Perfile']['activo']==1)?"Si":"No"; ?></td>
		</tr>
		<tr>
			<td>Creado</td>
			<td><?= $perfile['Perfile']['usuario']." (".$perfile['Perfile']['created'].")"; ?></td>
		</tr>
		<tr>
			<td>Modificado</td>
			<td><?= ($perfile['Perfile']['usuariomodif']!='')?$perfile['Perfile']['usuariomodif']." (".$perfile['Perfile']['modified'].")":""; ?></td>
		</tr>
	</table>


</div>
<div class="actions">

		<div><?php echo $this->Html->link(__('Editar Perfil'), array('action' => 'edit', $perfile['Perfile']['id']),array('class'=>'btn btn-default')); ?> </div>
		<div><?php echo $this->Html->link(__('Listado de Perfiles'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>

<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>
