<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
	<h2><?php echo __('Personas'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "personas";
	?>
	<div id='search_box' class="table-responsive">
		<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Nombre, Apellido, Apodo', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre, Apellido, Apodo'));
		echo "</td>";
        echo "<td style='width: 15%; padding-left: 15px;'>";
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][paise_id]',
            'options'=>$paises,
            'label'=>'País',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['paise_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";
        echo "<td style='width: 15%; padding-left: 4px;'>";
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][tipopersona_id]',
            'options'=>$tipopersonas,
            'label'=>'Tipo',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['tipopersona_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";
		/*echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('module_id',array('label'=>'Módulo:', 'onchange'=>"loadInt(this.id);", 'name'=> 'data['.$tabla.'][module_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('intervencione_id',array('label'=>'Intervención:', 'name'=> 'data['.$tabla.'][intervencione_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('actividade_id',array('label'=>'Actividad:', 'name'=> 'data['.$tabla.'][actividade_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";*/

		echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
		echo $this->Form->hidden('Controller', array('value'=>'Persona', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'nombre,apellido,apodo', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>

		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
		}
		?>
	</div>
	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('apellido', 'Apellido'); ?></th>
			<th><?php echo $this->Paginator->sort('apodo', 'Apodo'); ?></th>
			<th><?php echo $this->Paginator->sort('paise_id', 'País'); ?></th>
			<th><?php echo $this->Paginator->sort('tipopersona_id', 'Tipo de Persona'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($personas as $persona):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?= h($persona['Persona']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($persona['Persona']['nombre']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($persona['Persona']['apellido']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($persona['Persona']['apodo']); ?>&nbsp;</td>
				<td style='text-align:center'><?= $paises[$persona['Persona']['paise_id']]; ?></td>
				<td style='text-align:center'><?= $tipopersonas[$persona['Persona']['tipopersona_id']]; ?></td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $persona['Persona']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $persona['Persona']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<div><?php echo $this->Html->link(__('Nueva Persona'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>