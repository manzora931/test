<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="tareas view container">
<h2><?php echo __('Persona'); ?></h2>
    <?php
    if( isset( $_SESSION['persona_save'] ) ) {
        if( $_SESSION['persona_save'] == 1 ){
            unset( $_SESSION['persona_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Persona almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($persona['Persona']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>País</td>
            <td>
                <?=$paises[$persona['Persona']['paise_id']]?>
            </td>
        </tr>
        <tr>
            <td>País de Residencia</td>
            <td>
                <?=($persona['Persona']['paisresidencia_id'] != '')?$paises[$persona['Persona']['paisresidencia_id']] : ""; ?>
            </td>
        </tr>
        <tr>
            <td>Tipo de Persona</td>
            <td>
                <?=$tipopersonas[$persona['Persona']['tipopersona_id']]?>
            </td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td><?=$persona['Persona']['nombre']?></td>
        </tr>
        <tr>
            <td>Apellido</td>
            <td><?=$persona['Persona']['apellido']?></td>
        </tr>
        <tr>
            <td>Apodo</td>
            <td><?=$persona['Persona']['apodo']?></td>
        </tr>
        <tr>
            <td>Fecha de Nacimiento</td>
            <td><?=$persona['Persona']['fechanacimiento']?></td>
        </tr>
        <tr>
            <td>Correo</td>
            <td><?=$persona['Persona']['correo']?></td>
        </tr>
        <tr>
            <td>Teléfono</td>
            <td><?=$persona['Persona']['telefono']?></td>
        </tr>
        <tr>
            <td><?php echo __('Información'); ?></td>
            <td>
                <?php echo h($persona['Persona']['informacion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $persona['Persona']['usuario']." (".$persona['Persona']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($persona['Persona']['usuariomodif']!='')?$persona['Persona']['usuariomodif']." (".$persona['Persona']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Persona'), array('action' => 'edit', $persona["Persona"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Personas'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
