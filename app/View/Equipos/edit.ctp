<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
    .btn-file{
        margin-left: 0;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }

    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: '1880:2030'
        });
        $.datepicker.regional["es"];
    });
</script>
<div class="tareas form container-fluid">
    <?php echo $this->Form->create('Equipo',array('id'=>'tarea','enctype'=>"multipart/form-data")); ?>
    <fieldset>
        <legend><?php echo __('Actualizar Equipo'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <?php echo $this->Form->input('id'); ?>
            <div class="col-sm-3">
                <?= $this->Form->input('paise_id',[
                    'label'=>'País',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"load_inter(this.id,0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('tipoequipo_id',[
                    'label'=>'Tipo de Equipo',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"load_act(this.id, 0, 0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('equipo',[
                    'label'=>'Nombre',
                    'required'=>true,
                    'placeholder'=>'Nombre',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>300
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('nombrecorto',[
                    'label'=>'Nombre Corto',
                    'required'=>true,
                    'placeholder'=>'Nombre Corto',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>150
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('estadio_id',[
                    'label'=>'Estadio',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"load_act(this.id, 0, 0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('fotoeq',[
                    'type'=>'file',
                    'label'=>'Foto del Equipo',
                    'class'=>'btn btn-default btn-file',
                    'div'=>['class'=>'form-group']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('fotoes',[
                    'type'=>'file',
                    'label'=>'Imagen Escudo',
                    'class'=>'btn btn-default btn-file',
                    'div'=>['class'=>'form-group']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 line-space">
                    <div class="col-md-3"><label class="lblproy">Fecha de Fundación</label></div>
                    <div class="col-md-5"><?= $this->Form->input("fechafundacion",[
                            'type'=>'text',
                            'class'=>"form-control datepicker",
                            'required'=>true,
                            'label'=>"",
                            'value' => explode('-', $this->data['Equipo']['fechafundacion'])[2] . '-' . explode('-', $this->data['Equipo']['fechafundacion'])[1] . '-' . explode('-', $this->data['Equipo']['fechafundacion'])[0],
                            'readonly'
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('ciudad',[
                    'label'=>'Ciudad',
                    'required'=>true,
                    'placeholder'=>'Ciudad',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('direccion',['rows'=>3, 'label'=>"Dirección", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"Dirección", 'style'=>'margin-bottom: 15px']); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('telefono',[
                    'placeholder'=>'Teléfono',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('email',[
                    'label'=>'Correo Electrónico',
                    'placeholder'=>'Correo Electrónico',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('sitioweb',[
                    'label'=>'Sitio Web',
                    'placeholder'=>'Sitio Web',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('informacion',['rows'=>3, 'label'=>"Información", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"Información", 'style'=>'margin-bottom: 15px']); ?>
            </div>
        </div>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Equipos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    jQuery(function(){
        $("#EquipoFotoeq").change(function(){
            var input = document.getElementById('EquipoFotoeq');
            var file = input.files[0];
            var data = new FormData();
            data.append('archivo',file);
            var url = getURL()+"valFoto";
            $.ajax({
                url: url,
                type: 'post',
                contentType:false,
                data: data,
                cache: false,
                processData:false,
                success: function(resp){
                    if(parseInt(resp)===1){
                        $("#EquipoFotoeq").val("");
                        $('body,html').animate({scrollTop : 0}, 500);
                        $("#msjalert").text("El formato de la foto no es valido.");
                        $("#alerta").slideDown();
                        setTimeout(function(){
                            $("#alerta").slideUp();
                        },4000);

                    }
                }
            });
        });
        $("#EquipoFotoes").change(function(){
            var input = document.getElementById('EquipoFotoes');
            var file = input.files[0];
            var data = new FormData();
            data.append('archivo',file);
            var url = getURL()+"valFoto";
            $.ajax({
                url: url,
                type: 'post',
                contentType:false,
                data: data,
                cache: false,
                processData:false,
                success: function(resp){
                    if(parseInt(resp)===1){
                        $("#EquipoFotoes").val("");
                        $('body,html').animate({scrollTop : 0}, 500);
                        $("#msjalert").text("El formato de la foto no es valido.");
                        $("#alerta").slideDown();
                        setTimeout(function(){
                            $("#alerta").slideUp();
                        },4000);

                    }
                }
            });
        });
    });
</script>
