<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
	<h2><?php echo __('Equipos'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "equipos";
	?>
	<div id='search_box' class="table-responsive">
		<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Nombre', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre'));
		echo "</td>";
        echo "<td style='width: 15%; padding-left: 4px;'>";
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][tipoequipo_id]',
            'options'=>$tipoequipos,
            'label'=>'Tipo de Equipo',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['tipoequipo_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";
        echo "<td style='width: 15%; padding-left: 4px;'>";
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][paise_id]',
            'options'=>$paises,
            'label'=>'País',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['paise_id']:'',
            'empty' => array( 'Seleccionar')));
        echo "</td>";

		/*echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('module_id',array('label'=>'Módulo:', 'onchange'=>"loadInt(this.id);", 'name'=> 'data['.$tabla.'][module_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('intervencione_id',array('label'=>'Intervención:', 'name'=> 'data['.$tabla.'][intervencione_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('actividade_id',array('label'=>'Actividad:', 'name'=> 'data['.$tabla.'][actividade_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";*/

		echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
		echo $this->Form->hidden('Controller', array('value'=>'Equipo', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'nombrecorto', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>

		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
		}
		?>
	</div>
	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombrecorto', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('paise_id', 'País'); ?></th>
			<th><?php echo $this->Paginator->sort('tipoequipo_id', 'Tipo de Equipo'); ?></th>
			<th><?php echo $this->Paginator->sort('fechafundacion', 'Fecha de Fundación'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($equipos as $equipo):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?= h($equipo['Equipo']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($equipo['Equipo']['nombrecorto']); ?>&nbsp;</td>
				<td style='text-align:center'><?= $paises[$equipo['Equipo']['paise_id']]; ?></td>
				<td style='text-align:center'><?= $tipoequipos[$equipo['Equipo']['tipoequipo_id']]; ?></td>
				<td style='text-align:center'><?= explode('-', $equipo['Equipo']['fechafundacion'])[2] . '-' . explode('-', $equipo['Equipo']['fechafundacion'])[1] . '-' . explode('-', $equipo['Equipo']['fechafundacion'])[0]; ?></td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $equipo['Equipo']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $equipo['Equipo']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<div><?php echo $this->Html->link(__('Nuevo Equipo'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>