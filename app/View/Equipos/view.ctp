<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

</style>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<div class="tareas view container">
<h2><?php echo __('Equipo'); ?></h2>
    <?php
    if( isset( $_SESSION['equipo_save'] ) ) {
        if( $_SESSION['equipo_save'] == 1 ){
            unset( $_SESSION['equipo_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Equipo almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($equipo['Equipo']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>País</td>
            <td>
                <?=$paises[$equipo['Equipo']['paise_id']]?>
            </td>
        </tr>
        <tr>
            <td>Tipo de Equipo</td>
            <td>
                <?=$tipoequipos[$equipo['Equipo']['tipoequipo_id']]?>
            </td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td><?=$equipo['Equipo']['equipo']?></td>
        </tr>
        <tr>
            <td>Nombre corto</td>
            <td><?=$equipo['Equipo']['nombrecorto']?></td>
        </tr>
        <tr>
            <td>Estadio</td>
            <td>
                <?=$estadios[$equipo['Equipo']['estadio_id']]?>
            </td>
        </tr>
        <tr>
            <td>Foto del Equipo</td>
            <td>
                <?php   if($equipo['Equipo']['fotoequipo']!=''){  ?>
                <a href="#" data-url="<?=$real_url?>fotoOrig/<?=$equipo['Equipo']['id']?>" class="fotoequipo"><img class="fotoJugador" src="../../<?=$equipo['Equipo']['fotoequipo']?>"></a></td>
            <?php } ?>
        </tr>
        <tr>
            <td>Imagen Escudo</td>
            <td>
                <?php   if($equipo['Equipo']['fotoescudo']!=''){  ?>
                <a href="#" data-url="<?=$real_url?>fotoOrig2/<?=$equipo['Equipo']['id']?>" class="fotoescudo"><img class="fotoEscudo" src="../../<?=$equipo['Equipo']['fotoescudo']?>"></a></td>
            <?php } ?>
        </tr>
        <tr>
            <td>Fecha de Fundación</td>
            <td><?=$equipo['Equipo']['fechafundacion']?></td>
        </tr>
        <tr>
            <td>Ciudad</td>
            <td><?=$equipo['Equipo']['ciudad']?></td>
        </tr>
        <tr>
            <td><?php echo __('Dirección'); ?></td>
            <td>
                <?php echo h($equipo['Equipo']['direccion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td>Teléfono</td>
            <td><?=$equipo['Equipo']['telefono']?></td>
        </tr>
        <tr>
            <td>Correo Electrónico</td>
            <td><?=$equipo['Equipo']['email']?></td>
        </tr>
        <tr>
            <td>Sitio Web</td>
            <td><?=$equipo['Equipo']['sitioweb']?></td>
        </tr>
        <tr>
            <td><?php echo __('Información'); ?></td>
            <td>
                <?php echo h($equipo['Equipo']['informacion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $equipo['Equipo']['usuario']." (".$equipo['Equipo']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($equipo['Equipo']['usuariomodif']!='')?$equipo['Equipo']['usuariomodif']." (".$equipo['Equipo']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Equipo'), array('action' => 'edit', $equipo["Equipo"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Equipos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
    <div class="tblvr">
        <?php if (!empty($tarea['Subtarea'])): ?>
            <h2><?php echo __('Sub tareas'); ?></h2><br>
            <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th width="7px"><?php echo __('Id'); ?></th>
                    <th width="25%"><?php echo __('Nombre'); ?></th>
                    <th><?php echo __('Descripción'); ?></th>
                </tr>
                </thead>
                <?php foreach ($tarea['Subtarea'] as $subtarea): ?>
                    <tr>
                        <td><?php echo $subtarea['id']; ?></td>
                        <td class="text-left"><?php echo $subtarea['nombre']; ?></td>
                        <td class="text-left"><?php echo $subtarea['descripcion']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
        $(".fotoequipo").click(function (e) {
            e.preventDefault();
            var url = $(this).data("url");
            window.open(url, "Foto del Equipo", 'width=700,height=500');
        });
        $(".fotoescudo").click(function (e) {
            e.preventDefault();
            var url = $(this).data("url");
            window.open(url, "Foto del Escudo", 'width=700,height=500');
        });
    });
</script>
