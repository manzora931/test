<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="modulos form container">
<?php echo $this->Form->create('Modulo', array('id'=>'formulario')); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Módulo'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('organizacion_id',
                array('label' => 'Organización',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'required'=>'required'));?>
            <?php echo $this->Form->input('modulo',
                array('label' => 'Módulo',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

	</fieldset>
</div>
<div class="actions">

    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Módulos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<!--<script type="text/javascript">
    jQuery(function(){
        var validate = {
            "id" : "#modulo",
            "form" : "#formulario",
            "message" : "El módulo ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_name';

            validate_name(val, id, url, false);
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_name';
            // Nodo del form
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para verificar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val, id},
                cache: false,
                success: function(resp) {
                    if ( resp == 1 ) {
                        $(validate.id).validationEngine("showPrompt", validate.message,"AlertText");
                        $(validate.id).val("");
                    } else {
                        if (form) {
                            form.submit();
                        }
                    }
                }
            });
        }
    });
</script>-->

<script>
    jQuery(function(){
            var validate = {
            "id" : "#ModuloModulo",
            "form" : "#formulario",
            "message" : "El módulo ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_mod';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_mod';
            var form = this;
            console.log(form);
            console.log(val);

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
           // console.log('function');
            $.ajax({
                url: url,
                type: 'post',
                data: { val:val, id:id},
                cache: false,
                success: function(resp) {
                    if (resp == "error") {
                        $("#alerta .message").text(validate.message);
                        $("#alerta").slideDown();
                        $(validate.id).val("");

                        setTimeout(function () {
                            $("#alerta").slideUp();
                        }, 4000);
                    }
                    else {
                            form.submit();
                    }
                }
            });
        }
    });
</script>
