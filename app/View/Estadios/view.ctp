<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<div class="contactos view container">
    <h2><?php echo __('Estadio'); ?></h2>
    <table width="100%">
        <tr>
            <td colspan="2">
                <?php
                if( isset( $_SESSION['estadio_save'] ) ) {
                    if( $_SESSION['estadio_save'] == 1 ){
                        unset( $_SESSION['estadio_save'] );
                        ?>
                        <div class="alert alert-success" id="alerta">
                            <span class="icon icon-check-circled"></span>
                            Estadio Almacenado
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
                    <?php }
                } ?>
            </td>
        </tr>
    </table>
    <table id="tblview">
        <tr>
            <td>Id</td>
            <td><?= $estadio['Estadio']['id']; ?></td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td><?php echo h($estadio['Estadio']['estadio']); ?></td>
        </tr>
        <tr>
            <td>País</td>
            <td><?php echo h($estadio['Paise']['pais']); ?></td>
        </tr>
        <tr>
            <td>Dirección</td>
            <td><?php echo h($estadio['Estadio']['direccion']); ?></td>
        </tr>
        <tr>
            <td>Foto</td>
            <td>
                <?php   if($estadio['Estadio']['foto']!=''){  ?>
                <a href="#" data-url="<?=$real_url?>fotoOrig/<?=$estadio['Estadio']['id']?>" class="foto">
                    <img class="fotoJugador" src="../../<?=$estadio['Estadio']['foto']?>">
                </a>
            <?php } ?>
            </td>
        </tr>
        <tr>
            <td>Capacidad</td>
            <td><?php echo h($estadio['Estadio']['capacidad']); ?></td>
        </tr>
        <tr>
            <td>Información</td>
            <td><?=$estadio['Estadio']['informacion']; ?></td>
        </tr>
        <tr>
            <td>Creado</td>
            <td><?= $estadio['Estadio']['usuario']." (".$estadio['Estadio']['created'].")"; ?></td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td><?= ($estadio['Estadio']['usuariomodif']!='')?$estadio['Estadio']['usuariomodif']." (".$estadio['Estadio']['modified'].")":""; ?></td>
        </tr>
    </table>
</div>
<div class="actions">

    <div><?php echo $this->Html->link(__('Editar Estadio'), array('action' => 'edit', $estadio['Estadio']['id']),array('class'=>'btn btn-default')); ?> </div>
    <div><?php echo $this->Html->link(__('Listado de Estadios'), array('controller' => 'estadios', 'action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
        $(".foto").click(function (e) {
            e.preventDefault();
            var url = $(this).data("url");
            window.open(url, "Foto del Estadio", 'width=700,height=500');
        });
    });
</script>
