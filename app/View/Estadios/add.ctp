<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<script >
    function getURL(){
        return '<?=$real_url;?>';
    }
    $(document).ready(function(){

    });
</script>
<style>
    div.form{
        width: 100%;
        margin-bottom: 15px;
    }
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 15px;
        border:1px solid #cb071a;
        color: #011880;
    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }
    div.actions>div{
        display: inline-block;
    }
    .form-group{
        width:700px;
    }
    select.form-control {
        width: 300px;
    }
</style>
<div class="contactos form container">
<?php echo $this->Form->create('Estadio', array('id'=>'formulario','enctype'=>"multipart/form-data")); ?>
	<fieldset>
		<legend><?php echo __('Crear Estadio'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo	$this->Form->input('estadio',
                array('label' => 'Nombre',
                    'class' => 'form-control txtNombre',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));
            ?>

            <?php echo	$this->Form->input('paise_id',
                array('label' => 'País',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'id' => 'pais',
                    'empty'=>'Seleccionar',
                    'required'=>'required'
                ));
            ?>
            <?php echo	$this->Form->input('direccion',
                array('label' => 'Dirección',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>''
                ));
            ?>
            <?php echo $this->Form->input('fotoes',
                array('label' => 'Foto',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'type' => 'file',
                    'style' => 'border: none; height: 32px'
                )); ?>
            <?php echo	$this->Form->input('capacidad',
                array('label' => 'Capacidad',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>''
                ));
            ?>
            <?php echo $this->Form->input('informacion',
                array('label' => 'Información',
                    'class' => 'form-control',
                    'rows' => 3,
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                ));
            ?>
        </div>
	</fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Estadios'), array('controller' => 'estadios', 'action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script>
    jQuery(function(){

       /* $("#almacenar").on('click', function(evt){
            var values=0;

                 var url = "<?= Router::url(array('controller' => 'contactos', 'action' => 'validarCotacto')); ?>";
            if(($('.txtNombre').val()=="") || ($('.txtApellido').val()=="")||($('.txtEmail').val()=="")||($('#municipio').val()==""))
            {
                $("#alerta .message").text("Completar campos requeridos");
                $("#alerta").slideDown();
                $("html, body").animate({
                    scrollTop: 0
                }, 1000);
                setTimeout(function () {
                    $("#alerta").slideUp();
                }, 5000);
            }
        else {
                $.ajax({
                    data: {
                        contact: 0,
                        nom: $('.txtNombre').val(),
                        ap: $('.txtApellido').val(),
                        em: $('.txtEmail').val()
                    },
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        values = data.valor;
                        if (data.valor == 1) {

                            $("#alerta .message").text("Nombre de contacto '" + data.data[0][0].nombre + "' ya existe en una institución.");
                            $("#alerta").slideDown();
                            $("html, body").animate({
                                scrollTop: 0
                            }, 1000);
                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 5000);
                            $(".txtNombre").val('');
                            $(".txtApellido").val('');


                        }
                        else if (data.valor == 2) {

                            $("#alerta .message").text("Correo electrónico de contacto '" + data.data[0]['c'].email + "' ya existe en una institución.");
                            $("#alerta").slideDown();
                            $("html, body").animate({
                                scrollTop: 0
                            }, 1000);
                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 5000);

                            $(".txtEmail").val('');
                        }
                        else if (data.valor == 0) {

                            $("#formulario").submit();
                        }
                    }
                });
            }
        });  */
    });
</script>
