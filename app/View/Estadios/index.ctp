<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';?>

<style>
	div.index{
		margin-top: 15px;
		width: 100%;
	}
	#search-by{
		width: 110px;
	}
	table#tblin{
		width: 100%;
	}
	table tr td.form-group {
		padding-left: 15px;
	}
	.table > thead > tr > th.bdr {
		border-top-right-radius: 5px;
	}
	.table > thead > tr > th.bdr1 {
		border-top-left-radius: 5px;
	}
	div.input-group > div{
		display: inline-block;
	}
	div.input-group{
		display: inline-block;
	}
	td#check{
		width: 20%;
		padding-left: 35px;

	}
	.table tbody tr td {
		font-size: 16px;

	}
	.table tbody tr td:first-child {
		width:80px;
	}

	.table tbody tr td:last-child {
		width:110px;

	}

	table.table tbody{
		border:1px solid #ccc;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb ;
	}

	.actions ul .style-btn a{
		font-family: 'Calibri', sans-serif;
		background-image: none;
		background-color: #c0c0c0 ;
		border-color: #606060	;
		border-radius: 1px;
		font-size: 14px;
	}
	.actions {
		margin: -33px 0 0 0;
	}

	.actions ul .style-btn a:hover{
		background: #c0c0c0;
		color:#000;
		border-color: #606060;
	}
	.form-control {
		height: 25px;
		border-radius: 0;
		border: solid 1px #4160a3;
	}
	h2{
		color:#cb071a;
		font-size: 1.4em;
	}
	#search_box {
		border-top: solid 6px;
		border-bottom-color: white;
		border-top-color: #4160a3;

	}
	.index table tr:hover td {
		background: transparent;
	}
</style>
<div class="estadios index container">
	<h2><?php echo __('Estadios'); ?></h2>
    <span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));	?>
	</span>

	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "estadios";
	$session = $this->Session->read('tabla[estadios]');
	$search_text = $session['search_text'] ;
	?>
	<div id='search_box'>
		<?php 
		/*Inicia formulario de busqueda*/
		$tabla = "estadios";
		?>
		<?php
		echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
		echo "<tr>";
		echo "<td width='400px'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Nombre'));
		echo "</td>";
		echo "<td style='width: 210px; padding-left: 4px;'>";
		echo $this->Form->input('',array('class'=>'form-control','name'=>'data['.$tabla.'][paise_id]','options'=>$paises,'label'=>'País', 'type'=>'select', 'empty' => array( '(Seleccionar país)')));
		echo "</td>";

		echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
		echo $this->Form->hidden('Controller', array('value'=>'Estadio', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'estadio', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo " <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>";

		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>
		<?php
		if(isset($_SESSION['estadios']))
		{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
		}
		?>	</div>
	<script type="text/javascript">
		var searchBy = $("#search-by");
		var active = $("#active");

		searchBy.val('<?= $session['search-by'] ?>');
		active.val('<?= $session['activo'] ?>');
	</script>
	<table id="tblin" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
		<thead>
		<tr>
			<th class="id bdr1" width="10%"><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('estadio','Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('paise_id','País'); ?></th>
			<th class="bdr" width="20%"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($estadios as $estadio): ?>
			<tr>
				<td><?php echo h($estadio['Estadio']['id']); ?>&nbsp;</td>
				<td class="text-left"><?php echo h($estadio['Estadio']['estadio']); ?>&nbsp;</td>
				<td class="text-left"><?php echo h($estadio['Paise']['pais']); ?>&nbsp;</td>
				<td width="10%">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $estadio['Estadio']['id']), array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $estadio['Estadio']['id']), array('class'=>'editar')); ?>
					<?php #echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $estadio['Estadio']['id']), null, __('¿Está seguro de eliminar al tipo de proyecto # %s?', $tipoproyecto['Tipoproyecto']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<ul>
			<li class="style-btn"><?php echo $this->Html->link(__('Crear Estadio'), array('action' => 'add'),array('class'=>'btn btn-default')); ?></li>
		</ul>
	</div>
</div>