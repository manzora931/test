<?php
	/**
	 *
	 *
	 * @link          http://cakephp.org CakePHP(tm) Project
	 * @package       app.View.Pages
	 * @since         CakePHP(tm) v 0.10.0.1076
	 */

	 /*
	if (!Configure::read('debug')):
		throw new NotFoundException();
	endif;
	*/

	App::uses('Debugger', 'Utility');
?>

<?php
if (Configure::read('debug') > 0):
	Debugger::checkSecurityKeys();
endif;
?>
<div class="container">
	<h3>Lorem Ipsum</h3>
	<?= $this->Html->image('logos/redca.png', [
		'class' => 'pull-right col-xs-5'
	]) ?>
	<p>Morbi ullamcorper a vestibulum urna pharetra suspendisse fusce parturient a eros eleifend ac a at posuere libero vestibulum varius egestas a praesent id sem condimentum. A a est habitasse risus placerat vestibulum nascetur adipiscing id a interdum vitae posuere quis curabitur convallis facilisis a sociosqu ullamcorper parturient laoreet a. Magnis scelerisque a habitasse a pulvinar a adipiscing ac eros sapien vitae luctus inceptos parturient a enim ac ut viverra.</p>
	<p>Dapibus adipiscing nisi facilisi mi a dui fermentum congue cubilia sed proin porta convallis ultrices maecenas viverra a integer non nisi consectetur. Libero fames vestibulum interdum netus accumsan lobortis scelerisque adipiscing libero ullamcorper gravida consectetur a suspendisse hendrerit nascetur sit sem scelerisque. Adipiscing in dui turpis dapibus natoque metus est a ut feugiat sem a urna nunc purus duis sociosqu a potenti facilisis a adipiscing eu. Vestibulum suscipit elit nam tincidunt inceptos tellus a ac netus condimentum risus quis dui vel massa magna at ante pharetra a. Velit nullam maecenas dui suspendisse vitae parturient auctor facilisis parturient urna a feugiat faucibus scelerisque eu a ridiculus.</p>
	<p>Morbi ullamcorper a vestibulum urna pharetra suspendisse fusce parturient a eros eleifend ac a at posuere libero vestibulum varius egestas a praesent id sem condimentum. A a est habitasse risus placerat vestibulum nascetur adipiscing id a interdum vitae posuere quis curabitur convallis facilisis a sociosqu ullamcorper parturient laoreet a. Magnis scelerisque a habitasse a pulvinar a adipiscing ac eros sapien vitae luctus inceptos parturient a enim ac ut viverra.</p>
	<p>Dapibus adipiscing nisi facilisi mi a dui fermentum congue cubilia sed proin porta convallis ultrices maecenas viverra a integer non nisi consectetur. Libero fames vestibulum interdum netus accumsan lobortis scelerisque adipiscing libero ullamcorper gravida consectetur a suspendisse hendrerit nascetur sit sem scelerisque. Adipiscing in dui turpis dapibus natoque metus est a ut feugiat sem a urna nunc purus duis sociosqu a potenti facilisis a adipiscing eu. Vestibulum suscipit elit nam tincidunt inceptos tellus a ac netus condimentum risus quis dui vel massa magna at ante pharetra a. Velit nullam maecenas dui suspendisse vitae parturient auctor facilisis parturient urna a feugiat faucibus scelerisque eu a ridiculus.</p>
</div>

