<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="generos form container">
    <?php echo $this->Form->create('Genero', array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Género'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('genero',
                array('label' => 'Nombre',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

    </fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Géneros'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#almacenar').click(function(){
            $('#formulario').submit();
        });
    });
</script>