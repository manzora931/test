
<?php
echo $this->Html->Script('jquery',FALSE);
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es');
echo $this->Html->script('jquery.validationEngine');

?>
<script type="text/javascript">
    jQuery(document).ready(function(){

        // binds form submission and fields to the validation engine
        jQuery("#formulario").validationEngine();
    });

</script>
<div class="users form">
    <?php echo $this->Form->create('User',array('id'=>'formulario')); ?>

    <fieldset>
        <legend><?php echo __('Generar url'); ?></legend>
        <?php
        /* $tabla = "users";*/
        echo $this->Form->input('user', array('label' => 'Usuario','id'=>'user' ,"class"=>"validate[required]",'required','empty'=>'Seleccione un Usuario'));
        echo $this->Form->input('Generar',array('type'=>'submit','id'=>'btn','label'=>false));
        ?>
        <div id='capa'>
                <br><label id="lbl" style="display: none;">URL:</label><input type="hidden" id="url" class="fieldname" size='15' style='border: solid 1px #1B82FF'/>
        </div>

    </fieldset>
    <?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#btn").click(function(e){
            e.preventDefault();
            var user = $("#user").val();
            if(user != ''){
                var url = 'generar';
                $.ajax({
                    url: url,
                    type:'POST',
                    data:{user:user},
                    success: function(resp){
                        var spl = resp.split("-");
                        if (spl[1]!=''){
                            $("#lbl").removeAttr("style");
                            $("#url").attr('type','text');
                            $("#url").val(resp);

                        }else{
                            alert("El usuario seleccionado no solicitado reestablecer la contraseña");
                            $("#lbl").attr("style",'display:none');
                            $("#url").attr('type','hidden');
                            $("#url").val('');
                        }
                    }

                });
            }else{
                alert("Seleccione un Usuario");
            }

        });
    });
</script>
