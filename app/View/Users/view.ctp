<div class="users view container">
	<h2><?php echo __('Usuario'); ?></h2>
	<?php
	if(isset($_SESSION['user_save'])){
		if($_SESSION['user_save']==1){
			unset($_SESSION['user_save']);
			?>
			<div class="alert alert-success " id="alerta" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Usuario almacenado
			</div>
		<?php		}
	}
	?>
	<style>
		table {
			border:none;
		}
		 table tr td {
			 border:none;
		 }
		table tr td:first-child{
			color: #011880;
			width: 200px;
		}
		div.view{
			margin-top: 15px;
			width: 100%;
		}
		h2{
			color:#cb071a;
			font-size: 1.4em;
		}
		.btn-default{
			font-family: 'Calibri', sans-serif;
			background-image: none;
			background-color: #c0c0c0 ;
			border:1px solid #606060 ;
			border-radius: 1px;
			font-size: 14px;

		}
		.btn-default{
			background-color: #c0c0c0 ;
			border:1px solid #606060 ;
			color:#000;

		}
		.
		div.actions{
			display: inline-block;
		}
		div.actions div > a{
			padding: 0 5px 0 5px;

		}
		div.actions div.style-btn > a{
			padding: 0 5px 0 5px;
			margin-left: 15px;
		}

		div.actions>div{
			display: inline-block;
		}
	</style>
	<table id="tblview">
		<tr>
			<td><Strong>Id</Strong></td><td><?php echo h($user['User']['id']); ?></td>
		</tr>
        <tr>
            <td><Strong>Perfil</Strong></td><td><?php echo h($user['Perfile']['perfil']); ?></td>
        </tr>
        <tr>
            <td><Strong>Nombre Completo</Strong></td><td><?php echo h($user['User']['nombrecompleto']); ?></td>
        </tr>
		<tr>
			<td><Strong>Usuario</Strong></td><td><?php echo h($user['User']['username']); ?></td>
		</tr>
        <tr>
            <td><Strong>Género</Strong></td><td>
                <?php
                $vgenero = 'Masculino';
                if ($user['User']['genero'] == 2) $vgenero = 'Femenino';
                ?>
                <?php echo $vgenero; ?></td>
        </tr>
		<tr>
			<td><Strong>Correo Electrónico</Strong></td><td><?php echo h($user['User']['email']); ?></td>
		</tr>
        <tr>
            <td><Strong>Nombre de Contacto</Strong></td><td><?php echo h($user['Contacto']['nombres'] . $user['Contacto']['apellidos']); ?></td>
        </tr>
        <tr>
            <td><Strong>Institución</Strong></td><td><?php echo $Inst['ins']['nombre'];?></td>
        </tr>
        <tr>
            <td><strong>Torneos</strong></td>
            <td>
                <?php
                foreach ($det as $item){
                    echo $torneos[$item["Detusuario"]["torneo_id"]]."<br>";
                }
                ?>
            </td>
        </tr>
		<td><Strong>Activo</Strong></td><td>
				<?php
				$vactivo = 'No';
				if ($user['User']['activo']) $vactivo = 'Si';
				?>
				<?php echo $vactivo; ?></td>
		</tr>
		<tr>
			<td><Strong>Creado</Strong></td>
			<td><?php echo h($user['User']['usuario']); ?>
				<?php echo "(".h($user['User']['created']); echo ")"; ?></td>
		</tr>
		<tr>
			<td><Strong>Modificado</Strong></td>
			<td>
				<?php
				if($user['User']['modified']!=''){
					echo h($user['User']['usuariomodif']);
					echo "(".h($user['User']['modified']); echo ")";
				}?></td>
		</tr>

	</table>
<div class="actions">

		<div><?php echo $this->Html->link(__('Editar Usuario'), array('action' => 'edit', $user['User']['id']),array('class'=>'btn btn-default')); ?> </div>
		<div class="style-btn"><?php echo $this->Html->link(__('Listado de Usuarios'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>
