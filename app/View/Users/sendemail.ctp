<?php $this->layout = "ajax"; ?>
<?= $this->Html->css('bootstrap/bootstrap.css') ?>
<?= $this->Html->css(['bootstrap/bootstrap', 'cake.generic', 'main']) ?>
<?= $this->Html->Script('jquery') ?>
<?= $this->Html->Script('bootstrap.min.js') ?>
<script type="text/javascript">



</script>
<style>
	div.actions{
		display: inline-block;
	}

	div.actions>div{
		display: inline-block;
	}
	#email .login-form .logo {
		margin: auto;
		width: 230px;
	}
	header .logo img {
		width: 230px;
		margin-left: -75px;
	}
	#email .login-form #UserSendemailForm .ssl-logos {
		margin: 20px 0;
	}
	#email {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 450px;
		height:inherit;
		margin: auto;
	}
	#email .login-form {
		margin-top:85px;
		position: relative;
		padding: 20px;
		border: solid 1px #011880;
		box-shadow: 5px 5px 10px 0 #011880;
	}
	#email .login-form #UserSendemailForm .ssl-logos div {
		float: left;
		clear: none;
		width: 33%;
	}
	#email .login-form #UserSendemailForm .ssl-logos div:first-child {
		margin-left: 20px;
	}
	#email .login-form #UserSendemailForm .ssl-logos div:last-child {
		float: right;
		margin-right: 20px;
	}
	form div.ssl-logos{
		 display: block;
		 clear: both;
		 /* margin-bottom: 1em; */
		 /* padding: .5em; */
		 vertical-align: text-top;
	 }
	 label{
		color:#011880;
	}
	#email .login-form .form-footer {
		position: absolute;
		bottom: 0;
		left: 0;
		height: 15px;
		width: 100%;
		background-image: url(../img/footer.png);
		background-size: 100%;
	}
	#email .login-links  {
		margin-top: 10px;
	}
	#email .login-links a {
		color: #101010;
		font-size: 11px;
	}
	#email .login-form #UserSendemailForm {
		float: none;
		margin: 15px auto;
		width: 270px;
	}
	.form-control {
		height: 25px;
		border-radius: 5px;
		border: solid 1px #4160a3;
	}

	label{
		font-size: 15px;
		font-family: 'Calibri', sans-serif;
		white-space: nowrap;
	}
	.modal-dialog{
		overflow-y: initial !important
	}
	.modal-body div#text {
		height: 250px;
		overflow-y: auto;
	}
	.modal-body{
		height: 460px;

	}

	.modal-header{
		background-color:#011880 ;
		height: 25px;
	}
	.modal-header{
		background-color:#011880 ;
		height: 25px;
	}
	h3#myModalLabel{
		background-color:#cb071a ;
		color:white;
		padding-left: 245px;
		padding-right:10px;
		font-weight: normal;
		font-size: 20px;
		margin-right: -15px;
		line-height: 1.7em;
		float: right;
	}
	header#header{
		margin-top: 10px;
		margin-left: 170px;
	}
	label#title{
		text-align: center;
		color:#011880;
		font-size: 20px;
		font-weight: bold;
	}
	#email .login-links a {
		margin-left: 160px;
	}
	div#text > p{
		text-align: justify;
		font-size: 16px;
		margin: 20px 50px;
	}
	footer .ssl-logos1 div {
		float: none;
		display: inline-block;
		clear: none;
		width: 10%;
		margin-top: 0;
	}
	footer .ssl-logos1 div:first-child {
		margin-left: 25px;
	}
	footer .ssl-logos1 div img {
		width: 50%;
	}
	footer .ssl-logos1 div span{
		font-size: 12px;
	}
	*{vertical-align: middle}


	footer {
		margin-top: 0 !important;
		padding-top: 25px !important;
	}
	.modal-content{
		margin-top: 75px;
		max-width: 450px;
		position: relative;
		background-color: #fff;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		border: solid 1px #011880;
		box-shadow: 5px 5px 10px 0 #011880 !important;
		border-radius: 1px !important;
		outline: 0;

	}
    div#tittle-system {
        text-align:center;
        background-color:#011880;
        color:white;
        width: 418px;
        margin-left: -20px;
        margin-top: 20px;
        margin-bottom: 0;
        padding: 15px 45px;
    }

    div#tittle-system label {
        color: #fff !important;
        padding-top: 5px;
    }

    h4 {
        font-size:14px !important;
        color: #fff !important;
    }
</style>
<?php $this->layout = "ajax"; ?>
<?= $this->Html->css(['bootstrap/bootstrap', 'cake.generic', 'main']) ?>
<?= $this->Html->Script(['jquery']) ?>
<div id="email" class="clearfix">
	<div class="col-xs-12 login-container">
		<div class="login-form clearfix">
			<?= $this->Html->image('LOGOS-UNIDOS.png', [
				'class' => 'img-responsive logo',
			]) ?>
            <div id="tittle-system" class="container col-xs-12 ">
                <h4 class="text-center">¿Olvido su contraseña?</h4>
                <label>Ingrese su  correo electrónico o usuario <br> para resetear contraseña</label>
            </div>
            <div class="clearfix"></div>

			<?php echo $this->Form->create('User', [
				'class' => 'form col-xs-12 clearfix text-center',
			]); ?>

			<?= $this->Form->input('param', [
				"label" => false,
				'placeholder' => 'Correo electrónico o usuario',
				'class' => 'form-control  validate[required]',
				'style' => 'border-radius:5px;'
			]) ?>
			<div class="actions">
				<div><?= $this->Form->input('Enviar', [
						'type' => 'submit',
						'label' => false,
						'class' => 'btn btn-primary btn-block',
						'style' => 'width:10%;margin-right: 25px;    font-family: Calibri , sans-serif;'
					]) ?></div>
				<div><?= $this->Html->link('Regresar',['action'=>'login'], [
						'type'=>'bottom',
						'label' => false,
						'class' => 'btn btn-primary btn-block','style' => 'width:10%;'
					]) ?></div>
			</div>


			<div class="ssl-logos">
				<div>
					<?= $this->Html->image('logos/ssl-logo.png', ['class' => 'img-responsive']) ?>
				</div>
				<div>
					<?= $this->Html->image('logos/100-secure.png', ['class' => 'img-responsive']) ?>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>

			<div class="col-sm-12 col-xs-12 text-center">
				<a  id="soporte" data-toggle='modal' href='mailto:soporte@sportsma.info'>Soporte o Consultas</a>
			</div>
			<div class="form-footer"></div>
		</div>
		<div class="login-links">


		</div>
	</div>

</div>
<div class="modal fade" id='login_modal' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:450px;" role="document">
		<div class="modal-content">
			<div class="modal-header">

				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
				<h3 class="modal-title" style="display: inline-block" id="myModalLabel">
					<i class="icon-edit"></i>Términos de Uso
					<label for="recipient-name" style="display: inline-block" class="form-control-label"></label>
				</h3>
			</div>


			<div class="modal-body">
				<header id="header">
					<a href='<?= Router::url('/')?>' class="logo" title="">
						<?= $this->Html->image('LOGOS-UNIDOS.png') ?>
					</a>
				</header>
				<hr>
				<div id="text">
					<label id="title"></label>
					<p id="body"></p>
				</div>
			</div>
			<footer >
				<div class="ssl-logos1">
					<div>
						<?= $this->Html->image('logos/ssl-logo.png', ['style'=>'margin-left: 20px;','class' => 'img-responsive']) ?>
					</div>
					<span class="text-center" style="font-size: 13px">2017 - Derechos Reservados, Programa Regional REDCA+</span>
					<div>
						<?= $this->Html->image('logos/100-secure.png', ['style'=>' margin-left: 5px;','class' => 'img-responsive']) ?>
					</div>
				</div>

			</footer>

		</div>
	</div>
</div>
<div id="modal"></div>
<script>
	jQuery(function(){
		$("#terminos").click(function(){
			$("#myModalLabel").text('Términos de Uso');
			$("#title").text('Términos de Uso');
			$("#body").text('Partiendo de dicha acepción nos encontraríamos con que dentro del de la tecnologíase pueden incluir un amplio número de modalidades o disciplinas tales como la informática, ' +
				'la robótica, la domótica,la neumática, la electrónica, la urbótica o la inmótica, entre otras muchas más');

		});
		$("#soporte").click(function(){
			$("#myModalLabel").text('Soporte o Consultas');
			$("#title").text('Soporte o Consultas');
			$("#body").text('De esta manera, nos encontraríamos con el hecho de que tecnología es la aplicación de un conjunto de conocimientos y habilidades con un claro objetivo: conseguir una solución que permita al ser humano desde resolver un problema ' +
				'determinado hasta el lograr satisfacer una necesidad en un ámbito concreto.');
		});
		$("#reco").click(function(){
			$("#myModalLabel").text('Recomendaciones');
			$("#title").text('Recomendaciones');
			$("#body").text('La tecnología está presente en todos los ámbitos de la vida cotidiana. De una forma u otra, casi todas las actividades que realizamos ' +
				'a lo largo del día implican la utilización de algún dispositivo tecnológico.');
		});
	});
</script>






