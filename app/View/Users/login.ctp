<?php $this->layout = "login"; ?>
<?= $this->Html->css('bootstrap/bootstrap.css') ?>
<?= $this->Html->css(['bootstrap/bootstrap', 'cake.generic', 'main']) ?>
<?= $this->Html->Script('jquery') ?>
<?= $this->Html->Script('bootstrap.min.js') ?>
<style>
	.modal-content {
		margin-top: 75px;
		max-width: 450px;
		height: 555px;
		position: relative;
		background-color: #fff;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		border: solid 1px #011880;
		box-shadow: 5px 5px 10px 0 #011880 !important;
		border-radius: 1px !important;
		outline: 0;
	}
	#login .login-links a {
		color: #101010;
		font-size: 11px;
	}
	header .logo img {
		width: 150px;
		margin-left: -75px;
	}
	.modal-dialog{
		overflow-y: initial !important
	}
	.modal-body div#text {
		height: 320px;
		overflow-y: auto;
	}
	.modal-body{
		height: 460px;

	}
	.modal-header{
		background-color:#011880 ;
		height: 25px;
	}
	h3#myModalLabel{
		background-color:#cb071a ;
		color:white;
		padding-left: 245px;
		padding-right:10px;
		font-weight: normal;
		font-size: 20px;
		margin-right: -15px;
		line-height: 1.7em;
		float: right;
	}
	header#header{
		margin-top: 10px;
		margin-left: 170px;
	}
	label#title{
		text-align: center;
		color:#011880;
		font-size: 20px;
		font-weight: bold;
	}
	div#text > p{
		text-align: justify;
		font-size: 16px;
		margin: 20px 50px;
	}
	footer .ssl-logos1 div {
		float: none;
		display: inline-block;
		clear: none;
		width: 10%;
		margin-top: 0;
	}
	footer .ssl-logos1 div:first-child {
		margin-left: 25px;
	}
	footer .ssl-logos1 div img {
		width: 50%;
	}
	footer .ssl-logos1 div span{
		font-size: 12px;
	}
	*{vertical-align: middle}

	#email .login-links a {
		margin-left: 160px;
	}
	footer {
		padding-top: 25px;
		margin-top: 0 !important;
	}

    div#tittle-system {
        text-align:center;
        background-color:#011880;
        color:white;
        width: 418px;
        margin-left: -20px;
        margin-top: 20px;
        margin-bottom: 0;
        padding: 15px 45px;
    }

     h4 {
        font-size:16px;
        color: #fff;
    }

div#antfooter{
	text-align:center;
	background-color:#fff;
	color:white;
	width: 418px;
    margin-left: -20px;
	margin-bottom:-6px;
}
div#antfooter > label{
	font-size:14px;
	width: 399px;
    padding-left: 15px;
	
}
div#antfooter > div{
	background-color:#fff;
	margin-bottom: -5px;
	padding-bottom: 15px;
    padding-top: 10px;
}
div#antfooter > div a{
	color:#011880;
}
.iniciosesion {
    border-radius: 5px;
}

.btn-danger{
	background-color:#cb071a;
}
@media screen and (max-width: 520px){
    .right {
          width: 340px !important;
    margin-left: -10px !important;
    }
	.lb{
		  width: 300px !important;
	}
	#login .login-container{
	height: 100%;
	width:100%;
	}
}

</style>
<div id="login" class="clearfix">
	<div class="col-xs-12 login-container">
		<div class="login-form clearfix">
			<?= $this->Html->image('LOGOS-UNIDOS.png', [
				'class' => 'img-responsive logo',
			]) ?>
            <div id="tittle-system" class="container col-xs-12 ">
                <h4 class="text-center">Software Sports Media Analytics</h4>
            </div>
            <div class="clearfix"></div>
            <?php
			$b=0;
            if(isset($_SESSION['datos_incorrec'])){
                unset($_SESSION['datos_incorrec']);
                ?>
                <div class="alert alert-danger" id="alerta" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
                    <span class="icon icon-check-circled"></span>
                    Usuario o contraseña incorrecto.
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
            <?php		}else if(isset($_SESSION['user_inac'])){
                unset($_SESSION['user_inac']);	?>
                <div class="alert alert-danger" id="alerta2" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
                    <span class="icon icon-check-circled"></span>
                    Usuario inactivo, consulte con el administrador.
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
            <?php
			}
            else if(isset($_SESSION['msj'])){
                unset($_SESSION['msj']);	?>
                <div class="alert alert-success" id="alerta2" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
                    <span class="icon icon-check-circled"></span>
                    Se envío un correo a: <?php echo $_SESSION['em'];?>
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
                <?php
            }
            ?>
			<?php

			if(isset($_SESSION['perfil_inac'])){
				unset($_SESSION['perfil_inac']);
				?>
				<div class="alert alert-danger" id="alerta3" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
					<span class="icon icon-check-circled"></span>
					Perfil inactivo, consulte con el administrador.
					<button type="button" class="close" data-dismiss="alert"></button>
				</div>
				<?php
			}
			?>
			<?php

			if(isset($_SESSION['passwordRestoreNoExist'])){
				unset($_SESSION['passwordRestoreNoExist']);
				?>
				<div class="alert alert-danger" id="alerta4" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
					<span class="icon icon-check-circled"></span>
					El usuario no existe, consulte con el administrador.
					<button type="button" class="close" data-dismiss="alert"></button>
				</div>
				<?php
			}
			?>

			<?php

			if(isset($_SESSION['passwordRestoreNoEmail'])){
				unset($_SESSION['passwordRestoreNoEmail']);
				?>
				<div class="alert alert-danger" id="alerta5" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
					<span class="icon icon-check-circled"></span>
					El Correo no existe , consulte con el administrador.
					<button type="button" class="close" data-dismiss="alert"></button>
				</div>
				<?php
			}
			?>
			<?php

			if(isset($_SESSION['passwordRestoreInactive'])){
				unset($_SESSION['passwordRestoreInactive']);
				?>
				<div class="alert alert-danger" id="alerta6" style="font-size: 12px;width: 300px;margin-top: 8px;margin-left: 45px;">
					<span class="icon icon-check-circled"></span>
					Usuario Inactivo , consulte con el administrador.
					<button type="button" class="close" data-dismiss="alert"></button>
				</div>
				<?php
			}
			?>
            <?php echo $this->Form->create('User', [
				'class' => 'form col-xs-12 clearfix text-center',
			]); ?>
					<?= $this->Form->input('username', [
						"label" => false,
						'placeholder' => 'Usuario',
						'class' => 'form-control iniciosesion'
					]) ?>
					<?= $this->Form->input('password', [
						"label" => false,
						'placeholder' => 'Contraseña',
						'class' => 'form-control iniciosesion'
					]) ?>
					<?= $this->Form->input('Iniciar', [
						'id' => 'iniciosesion',
						'type' => 'submit',
						'label' => false,
						'class' => 'btn btn-primary btn-block iniciosesion'
					]) ?>
					<?= $this->Html->link('¿Olvido su contraseña?', [
						'action' => 'sendemail'
					]) ?>
					
					
					<div class="ssl-logos">
						<div>
							<?= $this->Html->image('logos/ssl-logo.png', ['class' => 'img-responsive']) ?>
						</div>
						<div>
							<?= $this->Html->image('logos/100-secure.png', ['class' => 'img-responsive']) ?>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>
				
			<div id="antfooter" class="container col-sm-6 col-xs-12 right">

					<div class="col-sm-12 col-xs-12 text-center ">
						<a  id="soporte" data-toggle='modal' href='mailto:soporte@sportsma.info'>Soporte o Consultas</a>
					</div>
				</div>
			<div class="form-footer">
			</div>
		</div>
	</div>
	<div class="modal fade" id='login_modal' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="width:450px;" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title" style="display: inline-block" id="myModalLabel">
						<i class="icon-edit"></i>Términos de Uso
						<label for="recipient-name" style="display: inline-block" class="form-control-label"></label>
					</h3>
				</div>


				<div class="modal-body">
					<header id="header">
						<a href='<?= Router::url('/')?>' class="logo" title="">
							<?= $this->Html->image('LOGOS-UNIDOS.png') ?>
						</a>
					</header>
					<hr>
					<div id="text">
						<label id="title"></label>
						<p id="body"></p>
					</div>
				</div>
				<footer >
					<div class="ssl-logos1">
						<div>
							<?= $this->Html->image('logos/ssl-logo.png', ['style'=>'margin-left: 20px;','class' => 'img-responsive']) ?>
						</div>
						<span class="text-center" style="font-size: 13px"><?= date('Y') ?> - Derechos Reservados, Masconazo.com</span>
						<div>
							<?= $this->Html->image('logos/100-secure.png', ['style'=>' margin-left: 5px;','class' => 'img-responsive']) ?>
						</div>
					</div>

				</footer>

			</div>
		</div>
	</div>
</div>
<script>
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
        $("#alerta2").slideDown();
        setTimeout(function () {
            $("#alerta2").slideUp();
        }, 4000);
		$("#alerta3").slideDown();
		setTimeout(function () {
			$("#alerta3").slideUp();
		}, 4000);
		$("#alerta4").slideDown();
		setTimeout(function () {
			$("#alerta4").slideUp();
		}, 4000);
		$("#alerta5").slideDown();
		setTimeout(function () {
			$("#alerta5").slideUp();
		}, 4000);
		$("#alerta6").slideDown();
		setTimeout(function () {
			$("#alerta6").slideUp();
		}, 4000);
		$("#terminos").click(function(){
			$("#myModalLabel").text('Términos de Uso');
			$("#title").text('Términos de Uso');
			$("#body").text('Partiendo de dicha acepción nos encontraríamos con que dentro del de la tecnologíase pueden incluir un amplio número de modalidades o disciplinas tales como la informática, ' +
				'la robótica, la domótica,la neumática, la electrónica, la urbótica o la inmótica, entre otras muchas más');

		});
		$("#soporte").click(function(){
			$("#myModalLabel").text('Soporte o Consultas');
			$("#title").text('Soporte o Consultas');
			$("#body").text('De esta manera, nos encontraríamos con el hecho de que tecnología es la aplicación de un conjunto de conocimientos y habilidades con un claro objetivo: conseguir una solución que permita al ser humano desde resolver un problema ' +
				'determinado hasta el lograr satisfacer una necesidad en un ámbito concreto.');
		});
		$("#reco").click(function(){
			$("#myModalLabel").text('Recomendaciones');
			$("#title").text('Recomendaciones');
				$("#body").text('La tecnología está presente en todos los ámbitos de la vida cotidiana. De una forma u otra, casi todas las actividades que realizamos ' +
					'a lo largo del día implican la utilización de algún dispositivo tecnológico.');
		});
    });
</script>
