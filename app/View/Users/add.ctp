<?php
echo $this->Html->Script('validar_usuario',FALSE);
echo $this->Html->Script('user/complete');
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?= $real_url; ?>';
    }
</script>
<style>
    div.users{
        width: 100%;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 25px;
        border:1px solid #cb071a;
        color: #011880;

    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }

    .btn-default {
        margin-top: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        border-radius: 1px;
    }
    .btn-default:hover {
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;

    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border:1px solid #ccc;
        width: 700px;
    }
    select.form-control {
        width: 200px;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        margin-left: 15px;
    }

    div.actions>div{
        display: inline-block;
    }

    .btn-crear {
        background-color: #4160a3;
        color: #fff;
        border-radius: 5px;
        width: 120px;
        margin-left: 5px;
        margin-top: 20px;
        text-decoration: none;
    }
    .btn-crear:hover {
        color: #fff;
    }
    .modal-dialog {
        width: 700px;
    }
    #crearContacto {
        margin-top: 0px;
    }
    .form-contacto {
        width: 100%;
        display: inline;
    }
    select.form-contacto {
        width: 100%;
    }
</style>
<div class="users form container">
    <?php echo $this->Form->create('User',array('id'=>'formulario')); ?>
    <fieldset>
        <div class="alert alert-warning " id="alerta" style="display: none;">
            <p id="txt"></p>
        </div>
        <legend><?php echo __('Adicionar Usuario'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alert" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <?php
        echo $this->Form->input('nombrecompleto', array('label' => 'Nombre Completo','id'=>'nombre' ,"class"=>"  form-control validate[required]", 'required'=>'required'));
        echo $this->Form->input('username', array('label' => 'Usuario','id'=>'user' ,"class"=>"  form-control validate[required]", 'required'=>'required'));
        echo $this->Form->input('password', array('label' => 'Contraseña', 'id'=>'clave',"class"=>" form-control validate[required]", 'required'=>'required'));
        echo $this->Form->input('repertir_password', array('label' => 'Verificar Contraseña', 'type'=>'password','id'=>'verificarclave',"class"=>"form-control validate[required]", 'required'=>'required'));
        echo $this->Form->input('email', array(
            'label' => 'Correo electrónico',
            'id'=>'email',
            'class'=>' form-control',
            'required'=>'required'
        ));
        echo $this->Form->input('genero', array(
            'label' => 'Género',
            'type' => 'select',
            'id'=>'genero',
            'empty' => 'Seleccionar',
            'options' => array(1 => 'Masculino', 2 => 'Femenino'),
            'class'=>'form-control',
            'required'=>'required'
        ));
        echo $this->Form->input('perfile_id', array(
            'label' => 'Perfil',
            'id'=>'per',
            'empty' => 'Seleccionar',
            'class'=>'form-control',
            'required'=>'required'
        )); ?>
        <div style="display: inline-block">
            <?php echo $this->Form->input('contacto_id', array(
                'label' => 'Contacto',
                'id'=>'contacto',
                'empty' => 'Seleccionar',
                'class'=>'form-control'
            )); ?>
        </div>
        <div style="display: inline-block">
            <a data-toggle="modal" href="#addContacto" class="btn btn-crear" id="add-contacto">Nuevo Contacto</a>
        </div>
        <div class="clearfix"><label for="per" class="lblIns"></label></div>
        <div style="display: inline-block;">
            <?= $this->Form->input("torneo0_id", [
                "label"   => 'Torneos',
                "id"      => 'torneo0_id',
                "name"    => 'data[Torneo][0][torneo_id]',
                "empty"   => 'Seleccionar',
                "class"   => 'form-control torneo',
                "options" => $torneos,
                "onchange"=>"valTorneo(this.id, 0);"
            ]); ?>
        </div>
        <div style="display: inline-block;">
            <a href="#" class="btn btn-crear" id="add-torneo">Adicionar Torneo</a>
        </div>
        <div class="clearfix"></div>
        <div id="container-torneos"></div>
        <div class="clearfix"></div>
        <?=  $this->Form->input('instituciondefault', [
            'type' => 'checkbox',
            'label' => 'Usuario notificación',
            'div' => false,
            'style'=>'margin:0; margin-right:4px; margin-top:1px;'
        ]); ?>

        <?=  $this->Form->input('activo', [
            'type' => 'checkbox',
            'checked' => 'checked',
            'label' => 'Activo',
            'div' => false,
            'style'=>'margin:0; margin-right:4px; margin-top:1px;'
        ]); ?>
        <br>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Usuarios'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

    </div>
</div>
<!-- Modal Para Crear un Nuevo Contacto -->
<div class="modal fade" id="addContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    <i class="icon-edit"></i> Crear Nuevo Contacto
                </h3>
            </div>
            <div class="modal-body">
                <form id="formulario-contacto" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="clearfix"></div>
                            <div class="col-xs-9">
                            <div class="form-group">
                                <label for="nombres">Nombre</label>
                                <input name="data[nombres]" class=" form-contacto" value="" required="required" type="text" id="nombres">
                                 </div>
                            </div>
                            <div class="col-xs-9">
                            <div class="form-group">
                                <label for="apellidos">Apellido</label>
                                <input name="data[apellidos]" class="form-contacto" value="" required="required" type="text" id="apellidos">
                                 </div>
                            </div>
                            <div class="col-xs-9">
                                <?php echo	$this->Form->input('institucion_id',
                                    array('label'   => 'Institución',
                                        'class'   => 'form-control form-contacto',
                                         'div' => ['class'=>'form-group'],
                                        'value'   => '',
                                        'id'      => 'institucion',
                                        'empty'   =>'Seleccionar',
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?php echo	$this->Form->input('paise_id',
                                    array('label' => 'País',
                                        'class' => 'form-control form-contacto',
                                        'div' => ['class'=>'form-group'],
                                        'value'=>'',
                                        'id' => 'pais',
                                        'empty'=>'Seleccionar',
                                        'required'=>'required'
                                    ));
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?php echo	$this->Form->input('departamento_id',
                                    array('label' => 'Departamento',
                                        'class' => 'form-control form-contacto',
                                        'div' => ['class'=>'form-group'],
                                        'value'=>'',
                                        'id' => 'departamento',
                                        'empty'=>'Seleccionar',
                                        'required'=>'required'
                                    ));
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?php echo	$this->Form->input('municipio_id',
                                    array('label' => 'Municipio',
                                        'class' => 'form-control form-contacto',
                                        'div' => ['class'=>'form-group'],
                                        'value'=>'',
                                        'id' => 'municipio',
                                        'empty'=>'Seleccionar',
                                        'required'=>'required'
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-danger" id="crearContacto">Almacenar</button>
            </div>
        </div>
    </div>
</div>
<script>
    /**
     * Author: Manuel Anzora
     * description: funcion para validar que no selecciona mas de una ves el mismo
     * torneo.
     * date: 30-06-2019***/
    function valTorneo(id, line){
        var conta = $(".torneo").length;//numero de select
        var torneo_id = $("#"+id).val();//torneo seleccionado en la lista desplegable(select)
        var temp=0;
        var error=0;
        if(torneo_id!==''){
            for(var i = 0; i<conta; i++){
                if(i!==line){
                    temp = $("#torneo"+i+"_id").val();
                    if(temp==torneo_id){
                        error = 1;
                        break;
                    }
                }
            }
            if(error>0){
                $("#"+id).val("");
                $("#alert .message").text("No puede seleccionar el mismo torneo.");
                $("#alert").slideDown();
                setTimeout(function () {
                    $("#alert").slideUp();
                },4000);

            }
        }

    }
    jQuery(function(){
        //Procedimiento para adicionar torneos
        $("#add-torneo").click(function (e) {
            e.preventDefault();
            var cont = $(".torneo").length;
            var options = "";
            var url = getURL()+"users/getTorneos";
            $.ajax({
                url: url,
                type: 'post',
                async: false,
                cache: false,
                success: function(resp){
                    options = resp;
                }
            });
            $("#container-torneos").append("<div style='display: inline-block;margin-bottom: 8px;'>" +
                "<div class='input select'>"+
                "<select onchange='valTorneo(this.id, "+cont+");' class='form-control torneo' name='data[Torneo]["+cont+"][torneo_id]' id='torneo"+cont+"_id'>" +
                options +
                "</select>"+
                "</div>" +
                "</div>" +
                "<div class='clearfix'></div>");
        });


        $(".form-group").css("width","100%");
        $('#email').change(function() {
            // Expresion regular para validar el correo
            if($("#email").val().indexOf('@', 0) == -1 || $("#email").val().indexOf('.', 0) == -1) {
                $("#alert .message").text('Correo Invalido');
                $("#alert").slideDown();

                setTimeout(function () {
                    $("#alert").slideUp();
                }, 4000);
                $("#email").val('');
                return false;
            }
        });

        //Al momento de hacer click en almacenar un nuevo contacto
        $("#crearContacto").click(function() {
            var url = "<?= Router::url(array('controller' => 'users', 'action' => 'crear_contacto')); ?>";

            // Se obtiene la informacion ingresada del contacto
            var formData = $('#formulario-contacto').serializeArray();

            // Se obtiene el correo electronico del usuario para asignarlo al nuevo contacto
            var email = ($('#email').val() != '') ? $('#email').val() : '';
            formData.push({name: 'data[email]', value: email});

            // Se obtienen todas las opciones del combobox de contactos
            var contactos = $('#contacto').html();

            // Variable que almacena la opcion del nuevo contacto registrado
            var item = '';

            request = $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: formData,
                dataType: 'json'
            });

            request.done(function (response, textStatus, jqXHR){
                if(response.msg != 'error') {
                    // Opcion del nuevo contacto
                    item+='<option value="' + response.id + '">' + response.data[0].contactos.nombres  + ' ' + response.data[0].contactos.apellidos +'</option>';

                    // Se agrega la opcion del nuevo contacto a las opciones existentes
                    contactos+=item;
                    $('#contacto').html(contactos);

                    // Se selecciona al nuevo contacto en el combobox y se cierra el modal
                    $('#contacto').val(response.id).change();
                    $('#addContacto').modal('toggle');

                    $('#nombres').val('');
                    $('#apellidos').val('');
                    $('#institucion').val('');
                    $('#pais').val('');
                    $('#departamento').val('');
                    $('#municipio').val('');
                } else {
                    $('#addContacto').modal('toggle');
                    $('#nombres').val('');
                    $('#apellidos').val('');
                    $('#institucion').val('');
                    $('#pais').val('');
                    $('#departamento').val('');
                    $('#municipio').val('');

                    $("#alert .message").text('Error al crear nuevo contacto');
                    $("#alert").slideDown();

                    setTimeout(function () {
                        $("#alert").slideUp();
                    }, 4000);
                }
            });

            return false;
        });

        //función para poblar el campo departamentos según país seleccionado
        $("#pais").change(function(){
            var pais = $(this).val();
            var empty = '<option value="" >Seleccionar</option>';
            var url = "<?= Router::url(array('controller' => 'departamentos', 'action' => 'lista_deptos')); ?>";

            $.ajax({
                url: url,
                type:'post',
                data:{
                    pais: pais
                },
                success:function(resp){
                    $("#departamento").html(resp);
                    $("#municipio").html(empty);
                    $("#municipio").val('');
                }
            });
        });

        //función para poblar el campo municipios según departamento seleccionado
        $("#departamento").change(function(){
            var departamento = $(this).val();
            var url = "<?= Router::url(array('controller' => 'municipios', 'action' => 'lista_municipios')); ?>";

            $.ajax({
                url: url,
                type:'post',
                data:{
                    departamento: departamento
                },
                success:function(resp){
                    $("#municipio").html(resp);
                }
            });
        });


        $(document).on("change", "#contacto", function()
        {
            $("#UserInstituciondefault").prop("checked", "");
                var url=getURL() + 'users/getinstitucion';
                if($(this).val()!="") {
                    $.ajax({
                        data: {dato: $(this).val()},
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        success: function (data) {
                            $(".lblIns").text("Institución: " + data.ins.nombre);
                        }
                    });
                }
                else
                { $(".lblIns").text(" " );}

        });

        $(document).on("change", "#UserInstituciondefault", function()
                    {
                        if( $(this).is(':checked'))
                        {
                        var url=getURL() + 'users/valNotificion';
                        if($("#contacto").val()!="") {
                            $.ajax({
                                data: {dato: $(this).val(), idCont: $("#contacto").val(), ban: 0},
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                                success: function (data) {

                                    if (data == "error") {
                                        $("#txt").text("El usuario no puede recibir notificaciones.");
                                        $("#alerta").slideDown();
                                        setTimeout(function () {
                                            $("#alerta").slideUp();
                                        }, 4000);
                                        $("#UserInstituciondefault").prop("checked", "");
                                    }
                                }
                            });
                        }
                        else
                        {
                            $("#txt").text("Seleccione un cantacto.");
                            $("#alerta").slideDown();
                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                            $("#UserInstituciondefault").prop("checked", "");

                        }
                    }
             });


    });

    function val_default (){
        var user_id = 0;
        var insti = $("#in").val();
        var pred = $("#pred").val();
        var band = $("#UserInstituciondefault").is(":checked");
        if(insti != ''){
            if($("#UserInstituciondefault").is(":checked") == false){
                ///SE QUITA PREDETERMINADO
                if(pred==1){
                    $("#UserInstituciondefault").attr("checked",true);
                    $("#txt").text("No puede quitar las notificaciones al usuario");
                    $("#alerta").slideDown();
                    setTimeout(function () {
                        $("#alerta").slideUp();
                    }, 4000);
                }
            }else{
                var url = getURL()+"users/val_default";
                $.ajax({
                    url: url,
                    type: "post",
                    data: {insti:insti,user_id:user_id},
                    success: function (resp){
                        if(resp>0){
                            var resp = confirm("¿Ya existe un usuario con notificaciones, desea continuar?");
                            if(resp == false){
                                $("#UserInstituciondefault").attr("checked",false);
                            }
                        }
                    }
                });
            }
        }else{
            $("#txt").text("Selecciones una institución");
            $("#alerta").slideDown();
            setTimeout(function () {
                $("#alerta").slideUp();
            }, 4000);
            $("#UserInstituciondefault").attr("checked",false);
        }
    }
</script>
