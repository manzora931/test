
<script>
    function getBaseURL()
    {
        return "<?= Router::url("/") ?>";
    }
</script>
<style>
.form-control {
	height: 25px;
	border-radius: 0;
	border: solid 1px #4160a3;
}
table.table tbody tr td {
	text-align: left;
	font-size: 16px;
}
table.table tbody tr td:nth-child(6) {
	text-align:center;
}

table tbody tr th a {
	text-decoration: none;
	color: #FFFFff;
	background-color: ;
	border-radius:5px ;
}
table.table tbody {
	border-radius:2px 2px 0 0 ;
	border:1px solid #ccc;
}
div.index{
	margin-top: 15px;
	width: 100%;
}
h2{
	color:#cb071a;
	font-size: 1.4em;
}

.table > thead > tr > th.bdr {
	border-top-right-radius: 5px;

}
.table > thead > tr > th.bdr1 {
	border-top-left-radius: 5px;
}
.table > tbody > tr > td:first-child{
	text-align: center;

}
.table > tbody > tr > td:last-child{
	text-align: center;
}

table.table-striped>tbody>tr:nth-child(odd)>td{
	background-color: #ebebeb ;
}
#search_box {
	border-top: solid 6px;
	border-bottom-color: white;
	border-top-color: #4160a3;

}
div.select{
	padding-top: 22px;
	margin-left: 25px;
}
.actions ul .style-btn a{
	font-family: 'Calibri', sans-serif;
	background-image: none;
	background-color: #c0c0c0 ;
	border-color: #606060	;
	border-radius: 1px;
	font-size: 14px;
	margin-left: 15px;

}

.actions ul .style-btn a:hover{
	background: #c0c0c0;
	color:#000;
	border-color: #606060;
}
.actions {
	margin: -33px 0 0 0;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
  /*  $(function(){
        $('#percentage').formProgress({
            speed : 3500,
            style : 'blue',
            bubble : false,

            selector : '.form-control'
        });
    });*/
    </script>


<div class="users index  container">

    <div class="alert alert-success " id="validateUser" style="display: none;">
        <span class="icon icon-cross-circled"></span>
        <span class="message"></span>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php
     if(isset($_SESSION['mail_send'])){
    unset($_SESSION['mail_send']);	?>
    <div class="alert alert-success" id="alerta2" >
        <span class="icon icon-check-circled"></span>
        Se envío un correo a: <?php echo $_SESSION['mail_user'];?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php
    }
    ?>
	<h2><?php echo __('Usuarios'); ?></h2>
	<p>
		<?php
       	echo $this->Paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, mostrando {:current} de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?>
	</p>

    <?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "users";
	?>
	<div id='search_box' class="table-responsive" style="width: 100%;">
		<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table style='width: 80%;'>";
		echo "<tr>";
		echo "<td style='width: 300px; height: auto;
		padding:1px 1px 2px 4px;'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por:', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Nombre, Usuario'));
		echo "</td>";
		echo "<td style='width: 210px;'>";
		echo $this->Form->input('',array('class'=>'form-control','name'=>'data['.$tabla.'][institucion_id]','options'=>$selectInsttucion,'label'=>false, 'empty' => array( '(Seleccionar Institución)')));
		echo "</td>";
		echo "<td style='width: 150px; vertical-align: middle; margin: 0 1px 0 0; padding-left: 30px' >";

		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<label style='padding-top: 22px;' for='inactivos'>Inactivos <input  type='checkbox' id='inactivos' name='data[$tabla][activo]' value='0' checked ='checked' style='margin: 5px 10px 6px 2px;'></label>";
		}else{
			echo "<label style='padding-top: 22px;'  for='inactivos'>Inactivos <input  type='checkbox' id='inactivos' name='data[$tabla][activo]' value='0' style='margin: 5px 10px 6px 2px;'></label>";
		}

		echo "</td>";
		echo "<td style='padding-top: 22px; padding-left: 20px''>";
		echo $this->Form->hidden('Controller', array('value'=>'User', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'username,nombrecompleto', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo " <input type='submit' class='btn btn-default' value='Buscar' style='width: 100px;'>";
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>
		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			//echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
			echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
		}
		?>
	</div >

	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
		<thead>
			<tr>

				<th  width="5%" class="bdr1"><?php echo $this->Paginator->sort('id'); ?></th>
				<th width="400px"><?php echo $this->Paginator->sort('nombrecompleto','Nombre Completo'); ?></th>
				<th><?php echo $this->Paginator->sort('username','Usuario'); ?></th>
                <th><?php echo $this->Paginator->sort('contacto_id','Contacto Relacionado'); ?></th>
				<th><?php echo $this->Paginator->sort('perfile_id','Perfil'); ?></th>
				<th><?php echo $this->Paginator->sort('activo','Activo'); ?></th>
				<th class="bdr"><?php echo $this->Paginator->sort('Acciones'); ?></th>
			</tr>
		</thead>

		<?php
		$i=0;
		foreach ($users as $user):

			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}?>
			<tr   <?= $class;?>>
              	<td ><?php echo h($user['User']['id']); ?></td>
				<td class="text-left"><?php echo h($user['User']['nombrecompleto']); ?></td>
				<td class="text-center"><?php echo h($user['User']['username']); ?></td>
                <td class="text-left"><?php echo h($user['Contacto']['nombres'] . ' ' . $user['Contacto']['apellidos']); ?></td>
				<td class="text-center"><?php echo h($user['Perfile']['perfil']); ?></td>
                <td class="valortabla"><?= h($user['User']['activo'] ? 'Si' : 'No') ?></td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $user['User']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $user['User']['id']),array('class'=>'editar')); ?>
                    <?php echo $this->Html->link(__(' '), array('action' => 'reset', $user['User']['id']),array('class'=>'restore-pass')); ?>
                </td>
			</tr>

		<?php endforeach; ?>
	</table>
	<div class="paging">
		<?php echo $this->paginator->prev('<< '.__('Anterior ', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->paginator->numbers();?>
		<?php echo $this->paginator->next(__('Siguiente ', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
</div>
<div class="actions">
	<ul>
		<li class="style-btn"><?php echo $this->Html->link(__('Crear Usuario'), array('action'=>'add'),array( 'style'=>'width:50px;','class'=>'btn btn-default')) ?></li>		<?php
		if(isset($botones['get_url'])) { ?>
			<li><?php echo $this->Html->link(__($botones['get_url']), array('action' => 'get_url')); ?></li>
			<?php	}
			?>
			<!--li><a href="#" onClick="window.open('<?php echo Router::url('/'); ?>users/get_url','imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Generar url</a></li-->
		</ul>
	</div>
<script>


    jQuery(function(){
        $("#alerta2").slideDown();
        setTimeout(function () {
            $("#alerta2").slideUp();
        }, 4000);


        });


    </script>