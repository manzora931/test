<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="deteventos form container">
<?php echo $this->Form->create('Detevento'); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Detalle de Evento'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $this->Form->input('evento_id',[
                    'label'=>'Evento',
                    'class'=>'form-control validate[required]',
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('detevento',[
                    'label'=>'Detalle de Evento',
                    'class'=>'form-control validate[required]',
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('descripcion',[
                    'label'=>'Descripción',
                    'rows'=>3,
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>
	</fieldset>
</div>
<div class="actions">

    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Detalle de Evento'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>