<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

</style>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<div class="deteventos view container">
<h2><?php echo __('Detalle de Evento'); ?></h2>
    <?php
    if( isset( $_SESSION['detevento_save'] ) ) {
        if( $_SESSION['detevento_save'] == 1 ){
            unset( $_SESSION['detevento_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Detalle de Evento Almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($detevento['Detevento']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Evento</td>
            <td>
                <?=$detevento['Evento']['evento']?>
            </td>
        </tr>
        <tr>
            <td>Detalle de Evento</td>
            <td>
                <?=$detevento['Detevento']['detevento']?>
            </td>
        </tr>
        <tr>
            <td>Descripción</td>
            <td>
                <?=$detevento['Detevento']['descripcion']?>
            </td>
        </tr>
        <tr>
            <td>Activo</td>
            <td>
                <?=($detevento['Detevento']['activo']==1)?"Si":"No"?>
            </td>
        </tr>
        <tr>
            <td>Creado</td>
            <td>
                <?=$detevento['Detevento']['usuario']." (".$detevento['Detevento']['created'].")";?>
            </td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td>
                <?=($detevento['Detevento']['usuariomodif']!='')?$detevento['Detevento']['usuariomodif']." (".$detevento['Detevento']['modified'].")":"";?>
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Detalle'), array('action' => 'edit', $detevento["Detevento"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Detalles de Eventos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
