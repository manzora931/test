<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
    .btn-file{
        margin-left: 0;
    }
</style>
<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="deteventos form container-fluid">
<?php echo $this->Form->create('Detevento'); ?>
	<fieldset>
		<legend><?php echo __('Editar Detelle de Evento'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <?php echo $this->Form->input('id'); ?>
            <div class="col-sm-3">
                <?= $this->Form->input('evento_id',[
                    'label'=>'Evento',
                    'class'=>'form-control validate[required]',
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('detevento',[
                    'label'=>'Detalle de Evento',
                    'class'=>'form-control validate[required]',
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('descripcion',[
                    'label'=>'Descripción',
                    'rows'=>3,
                    'div'=>['class'=>'form-groupp']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>
	</fieldset>
</div>
<div class="actions">
    <div>
        <?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?>
    </div>
    <div>
        <?php echo $this->Html->link(__('Listado de Detalles de Eventos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?>
    </div>

</div>
