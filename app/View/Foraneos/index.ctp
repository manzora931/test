<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<?php
echo $this->Html->css("select2");
echo $this->Html->script("select2");
?>
<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
    $(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:<?=date("Y")?>"
        });
        $.datepicker.regional["es"];
    });
</script>
<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<div class="recursos index container-fluid">
    <h2><?php echo __('Foráneos'); ?></h2>
    <p>
        <?php
        echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));
        ?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "foraneos";
    $fecha=date('d-m-Y');
    $jugador = (isset($_SESSION['tabla['.$tabla.']']['jugadore_id'])) ? $_SESSION['tabla['.$tabla.']']['jugadore_id'] : "";
    $desde = (isset($_SESSION['tabla['.$tabla.']']['desde'])) ? $_SESSION['tabla['.$tabla.']']['desde'] : "";
    $hasta = (isset($_SESSION['tabla['.$tabla.']']['hasta'])) ? $_SESSION['tabla['.$tabla.']']['hasta'] : "";
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table>";
        echo "<tr>";
        echo "<td width='35%'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Jugador','type'=>'hidden', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Jugador'));
        echo $this->Form->input('jugadore_id',array('label'=>'Jugador', 'options'=>$jugadores,'empty'=>'Seleccionar','class'=>'form-control jugador', "value"=>$jugador));
        echo "</td>"; ?>
        <td style='padding-left: 10px;'>
            <?= $this->Form->input('desde', [
            'placeholder' => $fecha,
            'class' => 'form-control datepicker',
            'value' => $desde
        ]) ?>
        </td>
        <td style='padding-left: 10px;'>
            <?= $this->Form->input('hasta', [
                'placeholder' => $fecha,
                'class' => 'form-control datepicker',
                'value' => $hasta
            ]) ?>
        </td>
<?php
        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Foraneo', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'jugadore_id', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
    <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Jugadore.apellido', 'Jugador'); ?></th>
            <th><?php echo $this->Paginator->sort('fecha', 'Fecha'); ?></th>
            <th><?php echo $this->Paginator->sort('rival', 'Rival'); ?></th>
            <th><?php echo $this->Paginator->sort('minutos', 'Minutos'); ?></th>
            <th><?php echo $this->Paginator->sort('goles', 'Goles'); ?></th>
            <th><?php echo $this->Paginator->sort('asistencias', 'Asistencias'); ?></th>
            <th><?php echo $this->Paginator->sort('faltas', 'Faltas'); ?></th>
            <th><?php echo $this->Paginator->sort('tarjetasamarillas', 'Tarjetas Amarillas'); ?></th>
            <th><?php echo $this->Paginator->sort('tarjetasrojas', 'Tarjetas Rojas'); ?></th>
            <th><?php echo $this->Paginator->sort('observacion', 'Observación'); ?></th>
            <th class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i=0;
        $jugador_id = 0;
        $minutos=0;
        $goles = 0;
        $asistencias =0;
        $faltas = 0;
        $amarillas=0;
        $rojas = 0;
        $cont=0;
        foreach ($foraneos as $foraneo):
            if($i==0)
                $jugador_id = $foraneo['Foraneo']['jugadore_id'];
            $class=null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }   ?>
            <tr <?= $class;?> >
                <td><?= h($foraneo['Foraneo']['id']); ?>&nbsp;</td>
                <td style='text-align:left'><?= $this->Html->link($jugadores[$foraneo['Foraneo']['jugadore_id']], ["controller"=>"jugadores", "action"=>"view", $foraneo['Foraneo']['jugadore_id']]); ?>&nbsp;</td>
                <td style='text-align:left'><?= h($foraneo['Foraneo']['fecha']); ?>&nbsp;</td>
                <td style='text-align:left'><?= h($foraneo['Foraneo']['rival']); ?>&nbsp;</td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['minutos']; ?></td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['goles']; ?></td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['asistencias']; ?></td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['faltas']; ?></td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['tarjetasamarillas']; ?></td>
                <td style='text-align:right'><?= $foraneo['Foraneo']['tarjetasrojas']; ?></td>
                <td style='text-align:right'><?= (strlen($foraneo['Foraneo']['observacion']) > 10) ? substr($foraneo['Foraneo']['observacion'], 0, 9)."...":$foraneo['Foraneo']['observacion']; ?></td>
                <td class="">
                    <?php echo $this->Html->link(__(' '), array('action' => 'view', $foraneo['Foraneo']['id']),array('class'=>'ver')); ?>
                    <?php echo $this->Html->link(__(' '), array('action' => 'edit', $foraneo['Foraneo']['id']),array('class'=>'editar')); ?>
                </td>
            </tr>
        <?php
            $minutos = $minutos + $foraneo['Foraneo']['minutos'];
            $goles = $goles + $foraneo['Foraneo']['goles'];
            $asistencias = $asistencias + $foraneo['Foraneo']['asistencias'];
            $faltas = $faltas + $foraneo['Foraneo']['faltas'];
            $amarillas= $amarillas + $foraneo['Foraneo']['tarjetasamarillas'];
            $rojas = $rojas + $foraneo['Foraneo']['tarjetasrojas'];
            $jugador_id = $foraneo['Foraneo']['jugadore_id'];
            $cont++;
            if(isset($foraneos[$cont]['Foraneo']['jugadore_id'])){
                if($foraneos[$cont]['Foraneo']['jugadore_id'] != $jugador_id || count($foraneos) == 1){
                    //Totales
                    ?>
                    <tr>
                        <td></td>
                        <td style='text-align:left'><span style="font-weight: bold;">Total</span></td>
                        <td style='text-align:left'></td>
                        <td style='text-align:left'></td>
                        <td style='text-align:right; font-weight: bold;'><?= $minutos; ?></td>
                        <td style='text-align:right; font-weight: bold;'><?= $goles; ?></td>
                        <td style='text-align:right; font-weight: bold;'><?= $asistencias; ?></td>
                        <td style='text-align:right; font-weight: bold;'><?= $faltas; ?></td>
                        <td style='text-align:right; font-weight: bold;'><?= $amarillas; ?></td>
                        <td style='text-align:right; font-weight: bold;'><?= $rojas; ?></td>
                        <td style='text-align:right'></td>
                        <td class="">

                        </td>
                    </tr>
                    <?php
                    $minutos=0;
                    $goles = 0;
                    $asistencias =0;
                    $faltas = 0;
                    $amarillas=0;
                    $rojas = 0;
                }
            }else{
                ?>
                <tr>
                    <td></td>
                    <td style='text-align:left'><span style="font-weight: bold;">Total</span></td>
                    <td style='text-align:left'></td>
                    <td style='text-align:left'></td>
                    <td style='text-align:right; font-weight: bold;'><?= $minutos; ?></td>
                    <td style='text-align:right; font-weight: bold;'><?= $goles; ?></td>
                    <td style='text-align:right; font-weight: bold;'><?= $asistencias; ?></td>
                    <td style='text-align:right; font-weight: bold;'><?= $faltas; ?></td>
                    <td style='text-align:right; font-weight: bold;'><?= $amarillas; ?></td>
                    <td style='text-align:right; font-weight: bold;'><?= $rojas; ?></td>
                    <td style='text-align:right'></td>
                    <td class="">

                    </td>
                </tr>
                <?php
                $minutos=0;
                $goles = 0;
                $asistencias =0;
                $faltas = 0;
                $amarillas=0;
                $rojas = 0;
            }

        endforeach;
        ?>
        </tbody>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Nuevo Registro'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
    </div>
</div>
<script>
    jQuery(function(){
        $(".jugador").select2();
    });
</script>