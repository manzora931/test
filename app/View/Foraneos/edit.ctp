<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<?php
echo $this->Html->css("select2");
echo $this->Html->script("select2");
?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="tareas form container-fluid">
    <?php echo $this->Form->create('Foraneo',array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Foráneo'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?=
                $this->Form->input("id");
                ?>
                <?= $this->Form->input('jugadore_id',[
                    'label'=>'Jugador',
                    'class'=>'form-control validate[required] jugador',
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],
                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?php
                $fecha = explode('-', $this->request->data['Foraneo']['fecha']);
                $fecha = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                echo $this->Form->input("fecha",[
                    'type'=>'text',
                    'class'=>"form-control datepicker",
                    'required'=>true,
                    'label'=>"Fecha",
                    'readonly',
                    'value'=>$fecha
                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('rival',[
                    'label'=>'Rival',
                    'class'=>'form-control validate[required]',
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],
                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('minutos',[
                    'label'=>'Minutos',
                    'required'=>true,
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('goles',[
                    'label'=>'Goles',
                    'required'=>true,
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('asistencias',[
                    'label'=>'Asistencias',
                    'required'=>true,
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('faltas',[
                    'label'=>'Faltas',
                    'required'=>true,
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('tarjetasamarillas',[
                    'label'=>'Tarjetas Amarillas',
                    'required'=>true,
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('tarjetasrojas',[
                    'label'=>'Tarjetas Rojas',
                    'required'=>true,
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('observacion',['rows'=>3, 'label'=>"Observación", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"", 'style'=>'margin-bottom: 15px']); ?>
            </div>
        </div>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2020'
        });
        $.datepicker.regional["es"];
        $(".jugador").select2();
    });
</script>