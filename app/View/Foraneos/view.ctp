<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="tareas view container">
    <h2><?php echo __('Foráneo'); ?></h2>
    <?php
    if( isset( $_SESSION['foraneo_save'] ) ) {
        if( $_SESSION['foraneo_save'] == 1 ){
            unset( $_SESSION['foraneo_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($foraneo['Foraneo']['id']); ?>
            </td>
        </tr>
        <tr>
            <td>Jugador</td>
            <td>
                <?=$jugadores[$foraneo['Foraneo']['jugadore_id']]?>
            </td>
        </tr>
        <tr>
            <td>Fecha</td>
            <td>
                <?=$foraneo['Foraneo']["fecha"]; ?>
            </td>
        </tr>
        <tr>
            <td>Rivak</td>
            <td>
                <?=$foraneo['Foraneo']["rival"]?>
            </td>
        </tr>
        <tr>
            <td>Minutos</td>
            <td><?=$foraneo['Foraneo']['minutos']?></td>
        </tr>
        <tr>
            <td>Goles</td>
            <td><?=$foraneo['Foraneo']['goles']?></td>
        </tr>
        <tr>
            <td>Asistencias</td>
            <td><?=$foraneo['Foraneo']['asistencias']?></td>
        </tr>
        <tr>
            <td>Faltas</td>
            <td><?=$foraneo['Foraneo']['faltas']?></td>
        </tr>
        <tr>
            <td>Tarjetas Amarillas</td>
            <td><?=$foraneo['Foraneo']['tarjetasamarillas']?></td>
        </tr>
        <tr>
            <td>Tarjetas Rojas</td>
            <td><?=$foraneo['Foraneo']['tarjetasrojas']?></td>
        </tr>
        <tr>
            <td>Observación</td>
            <td>
                <?php echo h($foraneo['Foraneo']['observacion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $foraneo['Foraneo']['usuario']." (".$foraneo['Foraneo']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($foraneo['Foraneo']['usuariomodif']!='')?$foraneo['Foraneo']['usuariomodif']." (".$foraneo['Foraneo']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Foraneo'), array('action' => 'edit', $foraneo['Foraneo']["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
