<?php echo $this->Html->script('denuncias/denuncias');
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>
<div class="denuncias view container-fluid">
	<div class="title-view col-xs-12">
		<h2 class="title-view__title">Instrumento de Registro de Denuncias por Violaciones a Derechos Humanos a personas con VIH</h2>
		<div class="title-view__buttons">
			<?= $this->Html->link('Listado de Denuncias', [
				'action' => 'index',
			], [
				'class' => 'btn btn-primary'
			]) ?>
		</div>
	</div>
	<div id="transitions" class="transitions col-xs-12"></div>
	<?php //etapas/bitácora ?>
	<div class="col-xs-8">
		<div id="etapas" class="panel panel-primary panel-borderless">
			<div class="panel-heading">Etapas</div>
			<div class="panel-body">
                <div id="formularios" class="panel-group" role="tablist" aria-multiselectable="true">
                    <?php foreach ($forms as $form) { ?>
						<?php $formData = json_decode($form['Dataform']['data']); ?>
						<?php $is_empty = isset($formData->empty); ?>
						<?php $class =  $is_empty ? 'stage no-caret' : 'stage'; ?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab">
								<h4 class="panel-title">
									<a role="button" class="<?= $class ?>" data-toggle="collapse" data-parent="#formularios" data-id="<?= $form['Dataform']['id'] ?>" data-denuncia="<?= $form['Denuncia']['id'] ?>" href="#stage-<?= $form['Dataform']['id'] ?>">
										<span class="stage-title"><?= $form['Formdinamic']['nombre'] ?> </span>
										<small class="stage-date"><?= date('d-M-Y H:i:s', strtotime($form['Dataform']['created'])) ?></small>
									</a>
								</h4>
							</div>
							<?php if ( !$is_empty ) { ?>
								<div id="stage-<?= $form['Dataform']['id'] ?>" class="panel-collapse collapse" role="tabpanel">
									<div class="panel-body">
										<div class="container-fluid load">
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } ?>
                </div>
			</div>
		</div>
		<div id="bitacora" class="panel panel-primary panel-borderless">
			<div class="panel-heading">Bitácora</div>
			<?php if ($denuncia['Estado']['id'] > 1) { ?>
				<div class="panel-body" id="secc_bitacorta"></div>
			<?php } ?>
		</div>
	</div>
	<?php //sidebar ?>
	<div class="col-xs-4">
		<div class="panel panel-primary panel-badged">
			<div class="panel-heading text-center"><span class="badge" style="background-color: <?= $denuncia['Estado']['colorbkg'] ?>; color: <?= $denuncia['Estado']['colortext'] ?>"><?= $denuncia['Estado']['estado'] ?></span></div>
			<div class="panel-body">
				<div class="col-xs-6 text-left">
					<p><?= date('d M Y', strtotime($denuncia['Vdenuncia']['recepcion'])) ?></p>
				</div>
				<div class="col-xs-6 text-right">
					<p><?= $denuncia['Paise']['pais'] ?></p>
				</div>
				<h4 class="text-center text-red">
					<p><?= $denuncia['Vdenuncia']['codigo'] ?></p>
				</h4>
				<p class="col-xs-6 text-right">Tipo de Denuncia</p>
				<p class="col-xs-6 text-left"><?= str_replace('"', '', $denuncia['Vdenuncia']['tipo']) ?></p>
				<p class="col-xs-6 text-right">Organismo Referido</p>
				<p class="col-xs-6 text-left">- <?= $denuncia['Vdenuncia']['institucion']?></p>
			</div>
		</div>
		<div class="panel panel-primary">
			<div class="panel-heading">Archivos Adjuntos</div>
			<div class="panel-body">
				<div id="listado_adj" class="container-fluid"></div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		denuncias.loadTransitions({
			estadoId: <?= $denuncia['Estado']['id'] ?>,
			denunciaId: <?= $denuncia['Vdenuncia']['id'] ?>,
			url: "<?= Router::url(['controller' => 'transicions', 'action' => 'getTransitions']) ?>"
		}, "<?= $isFilled ?>");

		//funcion para cargar tablas con ajax en accordeones en webroot/denuncias/denuncias.js
		$(".stage").click( function () {
			denuncias.loadTable({
				dataFormId: $(this).data("id"),
				denunciaId: $(this).data("denuncia"),
				url: "<?= Router::url(['controller' => 'dataforms', 'action' => 'loadTable']) ?>"
			});
		});

		//funcion para autoguardar campos al ser editados
		$(document).on("change", ".field-edit", function () {
			denuncias.saveField({
				value: $(this).val(),
				name: $(this).attr("name"),
				dataFormId: $(this).data("dataform"),
				position: $(this).data("position"),
				url: "<?= Router::url(['controller' => 'dataforms', 'action' => 'saveField']) ?>"
			});
		});

		//funcion para actulizar fecha de formulario
		$(document).on("click", ".save-fields", function () {
			var dataFormId = $(this).data("dataform");
			var denunciaId = $(this).data("denuncia");

			denuncias.saveForm({
				dataFormId: dataFormId,
				denunciaId: denunciaId,
				url: "<?= Router::url(['controller' => 'dataforms', 'action' => 'saveForm']) ?>"
			});

			$.ajax({
				url: "<?= Router::url(['controller' => 'dataforms', 'action' => 'isFilled']) ?>",
				method: "POST",
				data: { dataFormId },
				success: function (isFilled) {
					denuncias.loadTransitions({
						estadoId: <?= $denuncia['Estado']['id'] ?>,
						denunciaId: <?= $denuncia['Vdenuncia']['id'] ?>,
						url: "<?= Router::url(['controller' => 'transicions', 'action' => 'getTransitions']) ?>"
					}, isFilled );

					denuncias.loadTable({
						dataFormId: dataFormId,
						denunciaId: denunciaId,
						url: "<?= Router::url(['controller' => 'dataforms', 'action' => 'loadTable']) ?>"
					});
					
				}
			});
		});

		$(document).on("click", ".modify-fields", function () {
			const dataFormId = $(this).data("dataform");
			const saveFieldsBtn = $(".save-fields-" + dataFormId);
			const fieldsEdit = $(".field-edit-" + dataFormId);
			const fieldsShow = $(".field-show-" + dataFormId);

			saveFieldsBtn.removeClass("hide");
			fieldsEdit.removeClass("hide");
			fieldsShow.addClass("hide");
			$(this).addClass("hide");
			updateUsers({
				dataFormId: dataFormId
			});
		});

		/*$(document).on('click','.transition-button',function () {
			var idDenuncia = $(".stage").data('denuncia');
			var  disabled = $('.transition-button').attr('disabled');
			if(!disabled){
			$.ajax({
				url: "<?php //Router::url(['controller' => 'notifications', 'action' => 'send_notification']) ?>",
				method: "POST",
				data: {idDenuncia:idDenuncia},
				success: function (resp) {
					console.log(resp);
					}
			});
			}
		});*/

		$("#secc_bitacorta").load(getURL()+'bitacora/<?=$denuncia['Vdenuncia']['id']?>');
		$("#listado_adj").load(getURL()+'list_adj/<?=$denuncia['Vdenuncia']['id']?>');
	});
</script>
