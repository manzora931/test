<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'impresion_denuncias')); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php
    $page = 1;
    $meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
    $fecha = date('d-m-Y');
    $fechaactual = explode('-', $fecha);
    $mes=$meses[$fechaactual[1]];

;?>
<div class="container" style="margin-bottom: 35px;">
    <?php if(isset($_SESSION['vdenuncias_search'])){ ?>
        <div class="col-xs-8">
            <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
            <label class="org"><?= $org ?></label>
            <br>
            <label class="tittle"><?php echo __('Registro de Violaciones a Derechos Humanos a personas  con VIH'); ?></label>
            <div class="linea"></div>
            <label class="encabezado"><?php echo __('Seguimiento de Denuncias al '. $fechaactual[0]. ' ' . $mes . ' de ' . $fechaactual[2]); ?></label>
        </div>
        <!-- <div class="col-xs-offset-1 col-xs-3" style="margin-top: 30px;"> -->
        <div class="col-xs-4" style="margin-top: 30px;">

                <div class="panel panel-default">
                    <div class="panel-body" style="min-height: 120px; padding: 10px 0px 10px 0px">
                        <?php if(isset($_SESSION['search_text'])){ ?>
                            <div class="col-xs-6 label-busq">Buscar por </div><div class="col-xs-6 txt-busq"><?php echo $_SESSION['search_text']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['desde'])){ ?>
                            <div class="col-xs-6 label-busq">Fecha desde </div><div class="col-xs-6 txt-busq"><?php echo $_SESSION['desde']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['hasta'])){ ?>
                            <div class="col-xs-6 label-busq">Fecha Hasta </div><div class="col-xs-6 txt-busq"><?php echo $_SESSION['hasta']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['tipo'])){ ?>
                            <div class="col-xs-6 label-busq">Tipo de denuncia </div><div class="col-xs-6 txt-busq"><?php echo $_SESSION['tipo']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['zona'])){ ?>
                            <div class="col-xs-6 label-busq">Zona </div><div class="col-xs-6 txt-busq"><?php echo $_SESSION['zona']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['pais'])){ ?>
                            <div class="col-xs-6 label-busq">País </div><div class="col-xs-6 txt-busq"><?php echo $pais['Paise']['pais']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['estado'])){ ?>
                            <div class="col-xs-6 label-busq">Estado </div><div class="col-xs-6 txt-busq"><?php echo $estado['Estado']['estado']; ?></div>
                            <div class="clearfix"></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['institucion'])){ ?>
                            <div class="col-xs-6 label-busq">Institución Asignada </div><div class="col-xs-6 txt-busq">
                                <?php //echo substr($institucion['Institucion']['nombre'],0,25) . "..."; ?>
                                <?php echo $institucion['Institucion']['nombre']; ?>
                            </div>
                            <div class="clearfix"></div>
                        <?php } ?>
                    </div>
                </div>

        </div>
    <?php } else { ?>
        <div class="col-xs-12">
            <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
            <label class="org"><?= $org ?></label>
            <br>
            <label class="tittle"><?php echo __('Registro de Violaciones a Derechos Humanos a personas  con VIH'); ?></label>
            <div class="linea"></div>
            <label class="encabezado"><?php echo __('Seguimiento de Denuncias al '. $fechaactual[0]. ' ' . $mes . ' de ' . $fechaactual[2]); ?></label>
        </div>
    <?php }  ?>
    <div class="clearfix"></div>
    <div class="col-sm-12" style="margin-top: 20px;">
        <div class="actions do-not-print">
            <ul>
                <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
            </ul>
        </div>

        <table id="table-index" class="interlineado" cellspacing="0" cellpadding="0" style="width: 100%">
            <thead>
            <tr>
                <th>No. Registro</th>
                <th>Fecha de Recepción</th>
                <th>Tipo de Denuncia</th>
                <th>Zona</th>
                <th>Institución Asignada</th>
                <th>País</th>
                <th>Estado</th>
            </tr>
            <tr class="espacio">
                <td class="border2" colspan="7"></td>
            </tr>
            </thead>
            <tbody>
            <?php
            $cont=1;
            if($denuncias) {
                foreach ($denuncias as $denuncia) {

                ?>
                    <tr>
                        <td class="text-center border2 border4" ><?= $denuncia['Vdenuncia']['codigo'] ?></td>
                        <td class="text-center border2 centro"><?= date('d-M-Y', strtotime($denuncia['Vdenuncia']['recepcion'])) ?></td>
                        <td class="text-center border2 centro"><?= Security::decrypt(base64_decode($denuncia['Vdenuncia']['tipo']), Configure::read('cipherAes')) ?></td>
                        <td class="text-center border2 centro"><?= Security::decrypt(base64_decode($denuncia['Vdenuncia']['zona']), Configure::read('cipherAes')) ?></td>
                        <td class="text-center border2 centro"><?= $denuncia['Institucion']['nombre'] ?></td>
                        <td class="text-center border2 centro"><?= $denuncia['Paise']['pais'] ?></td>
                        <td class="text-center border1 border2"><span  style="margin-bottom: 2px;"><?= $denuncia['Estado']['estado'] ?></span></td>
                    </tr>
                <?php }
            } else { ?>
                <tr>
                    <td class="text-center border1 border2" style="border-left: solid 1px;" colspan="7">No se encontraron denuncias</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="container">
    <div class="col-xs-12" style="display: none;">
        <div class="onlyprint" style="width: 100%">
            <div class="linea"></div>
            <label class="text-center onlyprint">2017 - Derechos Reservados, Programa Regional REDCA+</label>
        </div>
    </div>
</div>