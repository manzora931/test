<div class="denuncias form">
<?php echo $this->Form->create('Denuncia'); ?>
	<fieldset>
		<legend><?php echo __('Add Denuncia'); ?></legend>
	<?php
		echo $this->Form->input('codigo');
		echo $this->Form->input('estado_id');
		echo $this->Form->input('paise_id');
		echo $this->Form->input('institucion_id');
		echo $this->Form->input('usuario');
		echo $this->Form->input('usuariomodif');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Almacenar')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Denuncias'), array('action' => 'index')); ?></li>
	</ul>
</div>
