<?php $this->layout=false;
echo $this->html->Script('jquery');
echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main'));
?>
<style>
    #frm_bit{
        display: none;
        width: 75%;
        margin-left: 12%;
    }
    .control-goup{
        margin-bottom: 10px;
    }
    .cont_desc{
        background: #F7F6F6;
    }
    .adj:before{
        font-family: 'icomoon';
        content: '\e905';
    }
    .adj{
        text-decoration: none;
        background: #2E7D32;
        color:#fff;
        font-size: 14px;
        padding: 4px 10px 1px 10px;
        border-radius: 4px;
        float: right;
        margin-right: 10px;
        margin-bottom: 6px;
    }
</style>
<div id="alert" class="alert alert-danger" style="display: none">
    Solo se pueden adjuntar archivos PDF e imágenes (.png, .jpg, .jpeg)
</div>
<div id="alert2" class="alert alert-success" style="display: none">
    Comentario Almacenado
</div>
<div id="alert3" class="alert alert-danger" style="display: none">
    El tamaño del archivo debe de ser menor a 10MB
</div>
<div id="alert4" class="alert alert-danger" style="display: none">
    El tamaño del archivo debe de ser mayor a 0B
</div>
<div id="frm_bit">
    <br>
    <div class="control-goup">
        <input type="text" id="tittle" placeholder="Titulo" class="form-control">
    </div>
    <div class="form-group">
        <textarea id="descripcion" cols="3" class="form-control" placeholder="Descripción"></textarea>
    </div>
    <div class="form-group">
        <input type="file" id="file" class="btn btn-default btn-file">
    </div>
    <div class="text-center">
        <input type="submit" id="save_bit" value="Almacenar" class="btn btn-default">
        <input type="submit" id="cancel_bit" value="Cancelar" class="btn btn-default">
    </div>
    <input type="hidden" id="denuncia_id" value="<?= $id?>">
</div>
<!---PANEL DE BITACORA-->
<div id="list_bit" class="panel-group" role="tablist" aria-multiselectable="true">
</div>
<!---------------------->
<script>
    $("#list_bit").load(getURL()+'load_bitacora/<?=$id;?>');

    $("#cancel_bit").click(function(){
        $("#frm_bit").css({'display':'none'});
        $("#tittle").val("");
        $("#descripcion").val("");
        $("#file").val("");
        $("#cont_btn").css({'display':'block'});
    });

    $("#save_bit").prop('disabled', false);

    $("#save_bit").click(function(){
        var input = document.getElementById('file');
        var file = input.files[0];
        var data = new FormData();
        data.append('archivo',file);
        var url = getURL()+"save_comment";
        var titulo = $("#tittle").val();
        var coment = $("#descripcion").val();
        var maxsize = 10485760;

        if(input.value != '' && file.size >= maxsize) {
            $("#alert3").slideDown();
            setTimeout(function () {
                $("#alert3").slideUp();
            }, 3000);
            $("#file").val("");

            return false;
        } else if(titulo != '' && coment != ''){
            $(this).prop('disabled', true);
            data.append("titulo",titulo);
            data.append("coment",coment);
            var denuncia_id = $("#denuncia_id").val();
            data.append("denuncia_id",denuncia_id);
            $.ajax({
                url:url,
                type:'POST',
                contentType:false,
                data:data,
                dataType:'json',
                cache:false,
                processData:false,
                success:function(resp){
                    if(resp==1){
                        $("#alert").slideDown();
                        setTimeout(function () {
                            $("#alert").slideUp();
                        }, 3000);
                        $("#file").val("");
                    } else if(resp==2) {
                        $("#alert3").slideDown();
                        setTimeout(function () {
                            $("#alert3").slideUp();
                        }, 3000);
                        $("#file").val("");
                    } else if(resp==3){
                        $("#alert4").slideDown();
                        setTimeout(function () {
                            $("#alert4").slideUp();
                        }, 3000);
                        $("#file").val("");
                    } else{
                        $("#alert2").slideDown();
                        setTimeout(function () {
                            $("#alert2").slideUp();
                        }, 3000);
                        $("#list_bit").load(getURL()+'load_bitacora/<?=$id;?>');
                        $("#listado_adj").load(getURL()+'list_adj/<?=$id?>');
                        $("#file").val("");
                        $("#frm_bit").css({'display':'none'});
                        $("#tittle").val("");
                        $("#descripcion").val("");
                        $("#file").val("");
                        $("#cont_btn").css({'display':'block'});
                    }

                    $("#save_bit").prop('disabled', false);
                }
            });
        }else{

        }
    });
</script>
