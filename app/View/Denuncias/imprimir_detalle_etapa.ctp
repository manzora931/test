<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'impresion_denuncias')); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php
$page = 1;
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
$fecha = explode('-', date('d-m-Y', strtotime($denuncia['Vdenuncia']['recepcion'])));
//$fechaactual = explode('-', $fecha);
$mes=$meses[$fecha[1]];

;?>
<div class="container" style="margin-bottom: 35px;">
    <div class="col-xs-8">
        <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
        <label class="org"><?= $org ?></label>
        <br>
        <label class="tittle"><?php echo __('Registro de Violaciones a Derechos Humanos a personas  con VIH'); ?></label>
        <div class="linea"></div>
        <label class="encabezado"><?php echo __('Denuncia recibida el '. $fecha[0]. ' ' . $mes . ' de ' . $fecha[2]); ?></label>
    </div>
    <div class="col-xs-4">
        <div class="panel panel-primary panel-badged" style="margin-top: 20px;">
            <div class="panel-heading text-center"><span class="badge"><?= $estado['Estado']['estado'] ?></span></div>
            <div class="panel-body">
                <div class="col-xs-12 text-center">
                    <p class="den-pais"><?= $denuncia['Paise']['pais'] ?></p>
                </div>
                <!--p class="col-xs-12 text-center den-lcodigo">Código de Denuncia</p-->
                <h4 class="text-center">
                    <p class="den-codigo"><?= $denuncia['Vdenuncia']['codigo'] ?></p>
                </h4>
                <p class="col-xs-6 text-right den-ltipo">Tipo de Denuncia</p>
                <p class="col-xs-6 text-left den-tipo"><?= Security::decrypt(base64_decode($denuncia['Vdenuncia']['tipo']), Configure::read('cipherAes')) ?></p>
                <span class="clearfix"></span>
                <p class="col-xs-6 text-right den-lorg">Institución Referida</p>
                <p class="col-xs-6 text-left den-org">
                    <?php
                    // debug($denuncia['Vdenuncia']['institucion']);
                    if($denuncia['Vdenuncia']['institucion']){
                        echo $denuncia['Vdenuncia']['institucion'];
                    } else {
                        echo '-';
                    }

                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12">
        <div class="actions do-not-print">
            <ul>
                <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
            </ul>
        </div>
    </div><?php // debug($data); ?>
    <div class="col-sm-12">
        <p class="subtitulo">Denuncia - <?= $estado['Estado']['estado'] ?></p>

            <table class="interlineado" cellspacing="0" cellpadding="0" style="width: 100%; margin-bottom: 15px;">
                <thead>
                <tr class="detalle">
                    <th colspan="2">
                        <span class="th-etapa"><?= $estado['Estado']['estado'] ?> </span>
                        <small class="th-fecha" style="margin-left: 40px;">
                            <?php
                            $fecha = explode(' ', date('d-m-Y H:i:s', strtotime($form['Dataform']['created'])));
                            $created = explode('-', $fecha[0]);
                            echo $created[0].' de '.$meses[$created[1]]. ' de ' . $created[2]. ' '. $fecha[1];
                            ?>
                        </small>
                    </th>
                </tr>

                    <tr class="espacio">
                        <td class="border2" colspan="2"></td>
                    </tr>

                </thead>
                <tbody>
                <?php /*$counter = 1;*/ foreach ($formData as $data) { ?>
                    <?php
                        if($data->value != '') {
                            $value = Security::decrypt(base64_decode($data->value), Configure::read('cipherAes'));
                        } else {
                            $value = $data->value;
                        }

                    ?>
                    <?php $limit =strlen($value); ?>
                    <?php if($limit > 80){ ?>
                    <tr>
                        <td class="text-left border1 border2 border3 col-xs-12" colspan="2" style="font-weight: bold;"><?= /*$counter++ . ". ". */$data->label ?></td>
                    </tr>
                    <tr>
                        <td class="text-left border1 border2 border3 col-xs-12" colspan="2"><?= $value ?></td>
                    </tr>
                    <?php } else { ?>
                        <tr>
                            <td class="text-left border1 border2 border3 col-xs-6" style="font-weight: bold;"><?= /*$counter++ . ". ". */$data->label ?></td>
                            <!--<td class="border1 border2 col-xs-6"><?php //$data->value ?></td>-->
                            <?php if ($data->name !== 'pais_violacion') { ?>
                                <td class="border1 border2 col-xs-6"><?= $value ?></td>
                            <?php } else { ?>
                                <td class="border1 border2 col-xs-6"><?= $paises[$value] ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>

    </div>
</div>
<!--<div class="container">
    <div class="col-xs-12">
        <div class="onlyprint" style="width: 100%">
            <div class="linea"></div>
            <label class="text-center onlyprint">2017 - Derechos Reservados, Programa Regional REDCA+</label>
        </div>
    </div>
</div>-->