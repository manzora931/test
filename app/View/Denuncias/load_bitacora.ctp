<div class="text-center panel panel-default" id="cont_btn">
    <input type="submit" value="Agregar Comentario / Achivos" id="btn-bitacora" class="btn btn-default">
</div>
<div class="panel-body">
    <?php
    foreach ($datablog as $blg) { ?>
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-small item_bitacora" role="tab">
                <div class="panel-title panel_bit">
                    <a role="button" class="stage collapsed" data-toggle="collapse" data-parent="#list_bit" data-id="<?= $blg['Datablog']['id'] ?>" href="#comment-<?= $blg['Datablog']['id'] ?>">
                        <span class="badge" style="background: <?= $estados[$blg['Datablog']['estado_id']]['colorbkg'] ?>;color: <?= $estados[$blg['Datablog']['estado_id']]['colortext'] ?>"><?= $estados[$blg['Datablog']['estado_id']]['estado'] ?></span>
                        <small class="comment-date"><?= date("d-M-Y H:i:s", strtotime($blg['Datablog']['created'])) ?></small>
                        <span class="comment-title"><?= Security::decrypt( base64_decode($blg['Datablog']['titulo']), Configure::read('cipherAes') ) ?> </span>
                    </a>
                </div>
            </div>
            <div id="comment-<?= $blg['Datablog']['id'] ?>" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body cont_desc">
                    <p class="descripcion"><?= Security::decrypt( base64_decode($blg['Datablog']['data']), Configure::read('cipherAes') ) ?></p>
                    <?php if(isset($blg['Datafile']) && count($blg['Datafile']) > 0) { ?>
                        <div>
                            <?= $this->Html->link("", array('controller' => 'denuncias', 'action' => 'download', $blg['Datablog']['id']),array('target'=>"_blank",'class'=>'adj')) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script>
    $("#btn-bitacora").click(function(){
        $("#frm_bit").css({"display":'block'});
        $("#cont_btn").css({'display':'none'});
    });
</script>