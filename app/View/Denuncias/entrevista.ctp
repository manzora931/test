<?php $this->layout = 'formulario' ?>
<div class="crearentrevista content clearfix">
	<?php if (isset($denuncia_saved)) { ?>
		<div id="alert" class="alert alert-success" style="display: none">
			Hemos recibido su denuncia. Pronto nos comunicaremos con usted
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span> 
			</button>
		</div>
		<script>
			$(function () {
				$("#alert").slideDown();
				document.getElementById("FormularioFormularioForm").reset();
				$("#FormularioFormularioForm input[type='text'], #FormularioFormularioForm input[type='email'], #FormularioFormularioForm textarea, select").val("");
				$("#FormularioFormularioForm input[type='radio']").prop("checked", false);

				setTimeout(function () {
					$("#alert").slideUp();
				}, 4000);
			});
		</script>
	<?php } ?>
	<?= $this->Form->create('Formulario', [
		'class' => 'form-horizontal'
	]) ?>
	<h2>Formulario de Registro de Violaciones a Derechos Humanos a personas con VIH</h2>
	<hr class="bold">
	<div class="col-xs-12">
		<span class="required-text pull-left"><b>*</b> Datos marcados son obligatorios</span>
	</div>
	<?php foreach($sections as $section) { ?>
		<div class="section clearfix">
		<h3><?= $section->titulo ?></h3>
		<?php foreach($section->campos as $field) { ?>
			<div class="form-group col-xs-12">
				<label for="" class="control-label col-xs-4 <?= $field->requerido ? "required-text" : "" ?>">
					<?= $field->requerido ? "<b>*</b>" : ""?>
					<?= isset($field->label) ? $field->label : "[label]" ?>
				</label>
				<?php switch($field->type) {
					case 'texto':
							echo $this->Form->input(isset($field->name) ? $field->name : "name", [
								'label' => false,
								'class' => 'form-control',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-xs-8' 
								],
								'required' => $field->requerido 
							]); 
						break;
					case 'email':
							echo $this->Form->input(isset($field->name) ? $field->name : "name", [
								'type' => 'email',
								'label' => false,
								'class' => 'form-control',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-xs-8' 
								],
								'required' => $field->requerido 
							]); 
						break;
					case 'lista':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'form-control',
								'empty' => isset($field->placeholder) ? $field->placeholder : 'Seleccionar',
								'div' => [
									'class' => 'col-xs-8' 
								],
								'type' => 'select',
								'options' => json_decode(json_encode($field->valores), true),
								'required' => $field->requerido
							]); 
						break; 
					case 'fecha':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control datepicker',
								'placeholder' => date('d-M-Y'),
								'div' => [
									'class' => 'col-xs-4' 
								],
								'required' => $field->requerido
							]);
						break;
					case 'areatexto':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control datepicker',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-xs-8' 
								],
								'type' => 'textarea',
								'required' => $field->requerido
							]);
						break;
					case 'numero':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-xs-8' 
								],
								'type' => 'number',
								'required' => $field->requerido
							]);
						break;
					case 'radio':
						echo $this->Form->input($field->name, [
							'legend' => false,
							'class' => '',
							'div' => [
								'class' => 'col-xs-8' 
							],
							'type' => 'radio',
							'options' => $field->valores,
							'required' => $field->requerido
						]);
						break;
					case 'paises':
						echo $this->Form->input($field->name, [
							'type' => 'select',
							'label' => false,
							'class' => 'form-control',
							'div' => [
								'class' => 'col-xs-8'
							],
							'empty' => 'Seleccionar',
							'options' => $paises,
							'required' => $field->requerido
						]);
				} ?>
			</div>
		<?php } ?>
		</div>
		<hr>
	<?php } ?>
	<?= $this->Form->submit('Enviar Denuncia', [
		'class' => 'btn btn-primary',
		'div' => [
			'class' => 'text-center'
		]
	]) ?>
	<?= $this->Form->end() ?>
</div>
