<?php $this->layout = 'formulario' ?>
<?php
foreach($datosForm as $data) {
$data = json_decode($data['formdinamics']['descripcion'])
?>
<header class="clearfix">
	<h1 class="form-title text-right"><?=$data->title?></h1>
</header>

<div class="container-fluid">
<?= $this->Html->script("//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js") ?>
<div class="creardenuncia content clearfix">
	<?php  if (isset($denuncia_saved)) { //if(isset($nada)) {?>
		<div id="alert" class="alert alert-success" style="display: none">
			Hemos recibido su denuncia. Pronto nos comunicaremos con usted
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<script>
			$(function () {
				$("#alert").slideDown();
				document.getElementById("FormularioFormularioForm").reset();
				$("#FormularioFormularioForm input[type='text'], #FormularioFormularioForm input[type='email'], #FormularioFormularioForm textarea, select").val("");
				$("#FormularioFormularioForm input[type='radio']").prop("checked", false);
				nobackbutton();
				setTimeout(function () {
					$("#alert").slideUp();
					window.location.assign("http://redca.org");
				}, 5000);
			});
		</script>
	<?php } ?>
	<script>
		$(function () {
			var options = $("#FormularioPaisNacimiento option");
			var selected = $("#FormularioPaisNacimiento").val();

			options.sort(function(a,b) {
				if (a.text > b.text) return 1;
				if (a.text < b.text) return -1;
				return 0
			})

			$("#FormularioPaisNacimiento").empty().append( options );
			$("#FormularioPaisNacimiento").val(selected);
		});
	</script>
	<?= $this->Form->create('Formulario', [
		'class' => 'form-horizontal'
	]) ?>
		<div class="col-xs-12 pull-right logos-container">
			<div id="con">
				<div><?=$data->btnEn?></div>
				<div><?=$data->btnEs?></div>
			</div>
			<?= $this->Html->image('LOGOS-UNIDOS.png') ?>
		</div>
	<h2><?=$data->title?></h2>
	<hr class="bold">
	<div class="col-xs-12">
		<span class="required-text pull-left encabezado-formulario"><b>*</b><?=$data->encabezado?></span>
	</div>
	<input type="hidden" name="data[Formdinamic][formdinamic_id]" value="<?=$id;?>">
	<?php foreach($sections as $section) { ?>
		<div class="section clearfix">
		<h3><?= $section->titulo ?></h3>
		<?php  foreach($section->campos as $field) {
			?>
			<div class="form-group col-xs-12">
				<label for="" class="control-label col-md-4 col-xs-12 <?= $field->requerido ? "required-text" : "" ?>">
					<?= $field->requerido ? "<b>*</b>" : ""?>
					<?= isset($field->label) ? "{$field->label}" : "[label]" ?>
				</label>
				<?= $this->Form->input("label-{$field->name}", [
					'type' => 'hidden',
					'value' => $field->label
				]) ?>
				<?php switch($field->type) {
					case 'texto':
							echo $this->Form->input(isset($field->name) ? $field->name : "name", [
								'label' => false,
								'class' => 'form-control',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-md-8 col-xs-12'
								],
								'required' => $field->requerido
							]);
						break;
					case 'telefono':
						echo $this->Form->input(isset($field->name) ? $field->name : "name", [
							'label' => false,
							'class' => 'form-control telefono',
							'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
							'div' => [
								'class' => 'col-md-4 col-xs-12'
							],
							'required' => $field->requerido
						]);
						break;
					case 'documento':
						echo $this->Form->input(isset($field->name) ? $field->name : "name", [
							'label' => false,
							'class' => 'form-control documento',
							'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
							'div' => [
								'class' => 'col-md-4 col-xs-12'
							],
							'required' => $field->requerido
						]);
						break;
					case 'email':
							echo $this->Form->input(isset($field->name) ? $field->name : "name", [
								'type' => 'email',
								'label' => false,
								'class' => 'form-control validate[custom[email]]',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-md-8 col-xs-12'
								],
								'required' => $field->requerido
							]);
						break;
					case 'lista':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'form-control list',
								'empty' => isset($field->placeholder) ? $field->placeholder : 'Seleccionar',
								'div' => [
									'class' => 'col-md-8 col-xs-12'
								],
								'type' => 'select',
								'options' => json_decode(json_encode($field->valores), true),
								'required' => $field->requerido
							]);
						break;
					case 'fecha':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control datepicker',
								'placeholder' => date('d-M-Y'),
								'div' => [
									'class' => 'col-md-4 col-xs-12'
								],
								'required' => $field->requerido
							]);
						break;
					case 'areatexto':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control datepicker',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-md-8 col-xs-12'
								],
								'type' => 'textarea',
								'required' => $field->requerido
							]);
						break;
					case 'numero':
							echo $this->Form->input($field->name, [
								'label' => false,
								'class' => 'col-xs-6 form-control',
								'placeholder' => isset($field->placeholder) ? $field->placeholder : '',
								'div' => [
									'class' => 'col-md-8 col-xs-12'
								],
								'type' => 'number',
								'required' => $field->requerido
							]);
						break;
					case 'radio':
						echo $this->Form->input($field->name, [
							'legend' => false,
							'class' => '',
							'div' => [
								'class' => 'col-md-8 col-xs-12'
							],
							'type' => 'radio',
							'options' => $field->valores,
							'required' => $field->requerido
						]);
						break;
					case 'paises':
						echo $this->Form->input($field->name, [
							'type' => 'select',
							'label' => false,
							'class' => 'form-control',
							'div' => [
								'class' => 'col-md-8 col-xs-12'
							],
							'empty' => 'Seleccionar',
							'options' => $paises,
							'required' => $field->requerido
						]);
				}
				if(isset($field->descriptions) && $field->descriptions != false){
					foreach($field->descriptions as $description){	?>
						<span class="help-block col-md-8 col-xs-12 pull-right"><?= $description ?></span>
				<?php }
				} ?>
			</div>
		<?php } ?>
		</div>
		<hr>
	<?php } ?>

	<div class="recaptcha">
		<div class="g-recaptcha" data-sitekey="<?=$data->reCAPTCHA?>" data-callback="enableSend" data-expired-callback="disableSend"></div>
	</div>
		<div class="text-center">
			<?=$data->text?>
		</div>
	<?= $this->Form->submit($data->boton, [
		'class' => 'btn btn-primary',
		'id' => 'sendButton',
		'div' => [
			'class' => 'text-center'
		]
	]) ?>

	<?php }?>
	<?= $this->Form->end() ?>
</div>
<script>
	var sendButton = document.getElementById("sendButton");
	$("#FormularioFormularioForm").validationEngine();
	sendButton.setAttribute("disabled", true);

	function enableSend() {
		sendButton.removeAttribute("disabled");
	}

	function disableSend() {
		sendButton.setAttribute("disabled", true);
	}
</script>
</div>