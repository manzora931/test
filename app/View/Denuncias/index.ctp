<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.imprimir{
		background-color: #4160a3;
		color: #FFFFff;
		padding: 0 3px;
		height: 21px;
		border-radius: 3px;
	}
	a.imprimir:hover{
		background-color: #4160a3;
		color: #FFFFff;
	}
	.icono-tringle {
		width: 0;
		height: 0;
		border-style: solid;
		border-width: 8px 6px 0 6px;
		border-color: #ffffff transparent transparent transparent;
		margin-left: 7px;
		margin-top: 2px;
	}
</style>
<script>
	$(function() {
		$('.datepicker').datepicker({
			dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,
			yearRange: "1960:2017"
		});
		$.datepicker.regional["es"];

		var desde = $("#denunciasDesde");
		var hasta = $("#denunciasHasta");

		hasta.datepicker("option", "minDate", $(desde).datepicker("getDate"));
		desde.datepicker("option", "maxDate", $(hasta).datepicker("getDate"));

		desde.on("change", function() {
			hasta.datepicker("option", "minDate", $(this).datepicker("getDate"));
		});

		hasta.on("change", function() {
			desde.datepicker("option", "maxDate", $(this).datepicker("getDate"));
		});
	});
</script>
<div class="dataforms index container-fluid">
	<h2>
		Denuncias Recibidas
		<button class="hide imprimir btn btn-primary btn-xs">Imprimir</button>
		<?=$this->Html->link(__('Imprimir'), array('action' => 'imprimir'),array('class'=>'hide imprimir btn btn-primary btn-xs'));?>
		<li class="hide imprimir"><a href="#" onClick="window.open('<?php echo Router::url('/'); ?>eventos/imprimir/<?= $poa."_".$evento."_".$desde."_".$hasta.'_'.$ev.'_'.$fevento.'_'.$tpoa."_".$estado."_".$t_event."_".$unid;?>','imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir</a></li>
		<a class="imprimir btn" style="margin-left: 20px;" href="javascript:window.open('<?php echo Router::url('/'); ?>denuncias/imprimirDenuncias', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a></li>
	</h2>
	<span>
		<?php echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));	?>
	</span>
	<div id='search_box' class="searchBox clearfix">
		<?php
		$fecha=date('d-m-Y');
		$tabla = 'vdenuncias'; ?>
		<?= $this->Form->create($tabla) ?>
		<table>
			<tbody>
			<td width="13%">
				<?= $this->Form->input('search_text', [
					'label' => 'Buscar por:',
					'placeholder' => 'No. de Registro',
					'class' => 'form-control'
				]) ?>
			</td>
			<td width="14%">
				<?= $this->Form->input('desde', [
					'placeholder' => $fecha,
					'class' => 'form-control datepicker',
				]) ?>
			</td>
			<td width="14%">
				<?= $this->Form->input('hasta', [
					'placeholder' => $fecha,
					'class' => 'form-control datepicker',
				]) ?>
			</td>
			<td>
				<?= $this->Form->input('institucion', [
					'type' => 'select',
					'label' => 'Institución Asignada',
					'empty' => 'Seleccionar',
					'class' => 'form-control',
					'options' => $institutions

				]) ?>
			</td>
			<td width="120px">
				<?= $this->Form->input('pais', [
					'label' => 'País',
					'type' => 'select',
					'empty' => 'Seleccionar',
					'options' => $paises,
					'class' => 'form-control',
				]) ?>
			</td>
			<td width="120px">
				<?= $this->Form->input('estado', [
					'type' => 'select',
					'empty' => 'Seleccionar',
					'options' => $estados,
					'class' => 'form-control',
				]) ?>
			</td>

			<td style="vertical-align: bottom; padding-bottom: 1px">
				<?= $this->Form->input('Buscar', [
					'label' => false,
					'class' => 'btn btn-default',
					'type' => 'submit'
				]) ?>
			</td>
			</tbody>
		</table>
		<?= $this->Form->end() ?>
		<?php if(isset($_SESSION['vdenuncias_search'])) { ?>
			<?= $this->Html->link('ver todos', [
				'controller' => 'denuncias',
				'action' => 'vertodos'
			], [
				'class' => 'btn btn-info pull-left'
			]); ?>
		<?php } ?>
	</div>
	<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?= $this->Paginator->sort('codigo', 'No. Registro'); ?></th>
			<th><?= $this->Paginator->sort('created', 'Fecha de Recepción') ?></th>
			<th><?= $this->Paginator->sort('tipo', 'Tipo de Denuncia'); ?></th>
			<th><?= $this->Paginator->sort('zona'); ?></th>
			<th><?= $this->Paginator->sort('institucion', 'Institución Asignada') ?></th>
			<th><?= $this->Paginator->sort('pais') ?></th>
			<th><?= $this->Paginator->sort('estado') ?></th>
		</tr>
		</thead>
		<tbody>
		<?php  foreach ($denuncias as $denuncia) { ?>
		<tr>
			<td class="text-left"><?= $this->Html->link($denuncia['Vdenuncia']['codigo'], ['controller' => 'denuncias', 'action' => 'view', $denuncia['Vdenuncia']['id']]); ?></td>
			<td><?= date('d-M-Y', strtotime($denuncia['Vdenuncia']['recepcion'])) ?></td>
			<td><?= Security::decrypt(base64_decode($denuncia['Vdenuncia']['tipo']), Configure::read('cipherAes')) ?></td>
			<td class="text-capitalize"><?= Security::decrypt(base64_decode($denuncia['Vdenuncia']['zona']), Configure::read('cipherAes')) ?></td>
			<td><?= $denuncia['Institucion']['nombre'] ?></td>
			<td><?= $denuncia['Paise']['pais'] ?></td>
			<td><span class="badge" style="margin-bottom: 2px;background-color: <?= $denuncia['Estado']['colorbkg'] ?>; color: <?= $denuncia['Estado']['colortext'] ?>"><?= $denuncia['Estado']['estado'] ?></span></td>
			<?php } ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<!-- class="actions">
		<ul>
			<li><a href="javascript:window.open('<?php // echo Router::url('/'); ?>denuncias/imprimirDenuncias', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir</a></li>
		</ul>
	</div>-->
</div>
</div>
