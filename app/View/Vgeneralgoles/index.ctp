<?php
echo $this->Html->css("select2");
echo $this->Html->script("select2");
?>
<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<div class="recursos index container-fluid">
    <h2><?php echo __('Reporte General de Goles'); ?></h2>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "vgeneralgoles";
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table>";
        echo "<tr>";
        echo "<td width='30%'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Jugador','type'=>'hidden', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Jugador'));
        echo $this->Form->input('jugador_id',array('label'=>'Jugador', 'options'=>$jugadores,'empty'=>'Seleccionar','class'=>'form-control jugador'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('torneo_id',array('label'=>'Torneo', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('jornada_id',array('label'=>'Jornada', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('equipo_id1',array('label'=>'Equipo', 'options'=>$equipos,'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('equipo_id2',array('label'=>'Equipo Contrario', 'options'=>$equipos,'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";

        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Vgeneralgole', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'jugador', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Torneo</th>
            <th>Fecha</th>
            <th>Jornada</th>
            <th>Minuto</th>
            <th>Jugador</th>
            <th>Equipo</th>
            <th>Gol</th>
            <th>Comentario</th>
            <th>Equip. Contrario</th>
        </tr>
        </thead>
	<?php
    $contgoles=0;
    foreach ($vgeneralgoles as $vgeneralgole): ?>
	<tr>
		<td><?= $this->Html->link($vgeneralgole['Vgeneralgole']['juego_id'], ["controller"=>"juegos", "action"=>"view", $vgeneralgole['Vgeneralgole']['juego_id']]); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneralgole['Vgeneralgole']['torneo'],['controller'=>"torneos","action"=>"view",$vgeneralgole['Vgeneralgole']['torneo_id']]); ?>&nbsp;</td>
		<td><?php echo date("d/m/Y",strtotime($vgeneralgole['Vgeneralgole']['fecha'])); ?>&nbsp;</td>
		<td><?php echo h($vgeneralgole['Vgeneralgole']['jornada']); ?>&nbsp;</td>
		<td><?php
        if($vgeneralgole['Vgeneralgole']['minadicional']!=''){
            echo $vgeneralgole['Vgeneralgole']['minuto']."<span style='margin-left:2px;color:#6048D1;'>+".$vgeneralgole['Vgeneralgole']['minadicional']."</span>";
        }else{
            echo h($vgeneralgole['Vgeneralgole']['minuto']);
        }
          ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneralgole['Vgeneralgole']['jugador'],["controller"=>"jugadores","action"=>"view",$vgeneralgole['Vgeneralgole']['jugador_id']]); ?>&nbsp;</td>
		<td style="text-align: right;"><?php echo $this->Html->link($vgeneralgole['Vgeneralgole']['equipo'],["controller"=>"equipos","action"=>"view",$vgeneralgole['Vgeneralgole']['equipo_id1']]); ?>&nbsp;</td>
		<td><?php echo h($vgeneralgole['Vgeneralgole']['Gol']); ?>&nbsp;</td>
		<td><?php echo h($vgeneralgole['Vgeneralgole']['comentario']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneralgole['Vgeneralgole']['equipocontrario'],["controller"=>"equipos","action"=>"view",$vgeneralgole['Vgeneralgole']['equipo_id2']]); ?>&nbsp;</td>
<?php
$contgoles += $vgeneralgole['Vgeneralgole']['Gol'];
endforeach; ?>
        <tr>
            <td colspan="7" class="text-left"><span><strong>Total de goles</strong></span></td>
            <td><strong><?=$contgoles?></strong></td>
            <td colspan="2"></td>
        </tr>
	</table>
</div>
<script>
    jQuery(function(){
        $(".jugador").select2();
    });
</script>