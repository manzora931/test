<div class="vgeneralgoles view">
<h2><?php echo __('Vgeneralgole'); ?></h2>
	<dl>
		<dt><?php echo __('Juego Id'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['juego_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo Id'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['torneo_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['torneo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jornada'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['jornada']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Minuto'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['minuto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jugador Id'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['jugador_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jugador'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['jugador']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo Id1'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['equipo_id1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['equipo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Gol'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['Gol']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comentario'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['comentario']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo Id2'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['equipo_id2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipocontrario'); ?></dt>
		<dd>
			<?php echo h($vgeneralgole['Vgeneralgole']['equipocontrario']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Vgeneralgoles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Vgeneralgoles'), array('action' => 'edit', '')); ?> </li>
	</ul>
</div>
