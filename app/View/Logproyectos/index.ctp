<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.regional["es"];
    });
</script>
<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblogs{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
</style>
<div class="logproyectos index container">
    <h2><?php echo __('Bitácora de Proyectos'); ?></h2>
    <span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count}, comienza en {:start}, finalizando en {:end}')
        ));	?>
	</span>
    <?php
        /*Se realizara un nuevo formato de busqueda*/
        /*Inicia formulario de busqueda*/
        $tabla = "logproyectos";
        $session = $this->Session->read('tabla[logproyectos]');
        $search_text = $session['search_text'] ;
        $fecha = date("d-m-Y");
    ?>
    <div id='search_box'>
        <?php
            /*Inicia formulario de busqueda*/
            $tabla = "logproyectos";
        ?>
        <?php
            echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 100%;'));
            echo "<input type='hidden' name='_method' value='POST' />";
            echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
            echo "<tr>";
            echo "<td width='15%' style='padding-right: 10px;'>";
            $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
            echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Usuarios'));
            echo "</td>";
            echo '<td width="25%" style="padding-right: 10px">';
            echo $this->Form->input('institucion_id', ['class' => 'form-control', 'empty' => 'Seleccionar', 'label' => 'Institución', 'options'=>$institucion]);
            echo "</td>";
            echo '<td width="25%" style="padding-right: 10px">';
            echo $this->Form->input('proyecto_id', ['class' => 'form-control', 'empty' => 'Seleccionar', 'label' => 'Proyecto', 'style' => 'width: 100%;' ]);
            echo "</td>";

            echo "<td width='14%'>";
            echo $this->Form->input('desde', ['placeholder' => $fecha, 'class' => 'form-control datepicker']);
            echo "</td>";
            echo '<td width="14%">';
            echo $this->Form->input('hasta', ['placeholder' => $fecha, 'class' => 'form-control datepicker']);
            echo "</td>";

            echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
            echo $this->Form->hidden('Controller', array('value'=>'Logproyecto', 'name'=>'data[tabla][controller]')); //hacer cambio
            echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
            echo $this->Form->hidden('Parametro1', array('value'=>'usuario', 'name'=>'data[tabla][parametro]'));
            echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
            echo $this->Form->hidden('campoFecha', array('value'=>'created', 'name'=>'data['.$tabla.'][campoFecha]'));
            echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
            echo " <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>";

            echo "</td>";
            echo "</tr>";
            echo "</table>";
        ?>
        <?php
            if(isset($_SESSION['logproyectos']))
            {
                $real_url = Router::url(array('controller' => 'logproyectos', 'action' => 'vertodos'));
                echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'\''));
            }
        ?>	</div>
    <script type="text/javascript">
        var searchBy = $("#search-by");
        var active = $("#active");

        searchBy.val('<?= $session['search-by'] ?>');
        active.val('<?= $session['activo'] ?>');
    </script>
    <table id="tblogs" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th width="13%"><?php echo $this->Paginator->sort('created','Fecha y Hora'); ?></th>
            <th width="20%"><?php echo $this->Paginator->sort('institucion_id','Institución del Proyecto'); ?></th>
            <th width="25%"><?php echo $this->Paginator->sort('proyecto_id','Proyecto'); ?></th>
            <th width="10%"><?php echo $this->Paginator->sort('usuario','Usuario'); ?></th>
            <th width="10%">Acción Realizada</th>
            <th width="12%">Pantalla</th>
            <th width="10%" class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($logproyectos as $logproyecto): ?>
            <?php $log = json_decode($logproyecto['Logproyecto']['log']); ?>
            <tr>
                <td class="text-center"><?= date("d-m-Y H:i:s",strtotime($logproyecto['Logproyecto']['created'])); ?>&nbsp;</td>
                <td class="text-left"><?php echo h($logproyecto['Institucion']['nombre']); ?>&nbsp;</td>
                <td class="text-left"><?php echo h($logproyecto['Proyecto']['nombrecorto']); ?>&nbsp;</td>
                <td class="text-center"><?php echo h($logproyecto['Logproyecto']['usuario']); ?></td>
                <td class="text-center"><?php echo h($log->accion); ?></td>
                <td class="text-center"><?php echo h($log->pantalla); ?></td>
                <td>
                    <?php echo $this->Html->link(__(' '), array('action' => 'view', $logproyecto['Logproyecto']['id']), array('class'=>'ver')); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $( "#logproyectosInstitucionId" ).on( "change", function() {
            var url = "<?= Router::url(array('controller' => 'logproyectos', 'action' => 'get_proyectos')); ?>";
            var id = $(this).val();
            var proyectos = $('#logproyectosProyectoId');
            var html = '<option value="">Seleccionar</option>';

            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id: id},
                dataType: 'json',
                success: function (response) {
                    $.each( response.proyecto, function( key, value ) {
                        html+='<option value="' + key + '">' + value + '</option>';
                    });

                    proyectos.html(html);
                }
            });
        });
    });
</script>

