<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 300px;
        text-align: left;
        font-weight: bold;
    }
    table#tblview tr td {
        padding-bottom: 3px;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
        margin-bottom: 7px;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 300px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 600px;
        text-align: left;

    }

    .encabezado-view {
        font-weight: bold;
    }

    .actualizado {
        color: #011880;
    }
</style>
<div class="logproyectos view container-fluid">
    <h2><?php echo __('Bitácora del Proyecto'); ?></h2>

    <table id="tblview" style="margin-bottom: 20px;">
        <tr>
            <td><strong>Id</strong></td><td><?php echo h($logproyecto['Logproyecto']['id']); ?></td>
        </tr>
        <tr>
            <td><strong>Institución del Proyecto</strong></td><td><?php echo h($logproyecto['Institucion']['nombre']); ?></td>
        </tr>
        <tr>
            <td><strong>Nombre Corto del Proyecto</strong></td><td><?php echo h($logproyecto['Proyecto']['nombrecorto']); ?></td>
        </tr>
        <tr>
            <td><strong>Fecha y Hora</strong></td><td><?php echo h(date("d-m-Y H:i:s",strtotime($logproyecto['Logproyecto']['created']))); ?></td>
        </tr>
        <tr>
            <td><strong>Ip</strong></td><td><?php echo h($logproyecto['Logproyecto']['ip']); ?></td>
        </tr>
        <tr>
            <td><strong>Tabla Afectada</strong></td><td><?php echo h($logproyecto['Logproyecto']['tabla']); ?></td>
        </tr>
        <tr>
            <td><strong>Modelo</strong></td><td><?php echo h($logproyecto['Logproyecto']['modelo']); ?></td>
        </tr>
        <tr>
            <td><strong>Pantalla donde se realizó la acción</strong></td><td><?php echo h($logproyecto['Logproyecto']['pantalla']); ?></td>
        </tr>
        <tr>
            <td><strong>Acción Realizada</strong></td><td><?php echo h($logproyecto['Logproyecto']['accion']); ?></td>
        </tr>
        <tr>
            <td><strong>Usuario que realizó la acción</strong></td><td><?php echo h($logproyecto['Logproyecto']['usuario']); ?></td>
        </tr>
    </table>

    <h4>
    <?php
        switch ($logproyecto['Logproyecto']['accion']) {
            case "Agregar":
                echo 'Nueva información almacenada';
                break;
            case "Modificar":
                echo 'Información actualizada';
                break;
            case "Eliminar":
                echo 'Información eliminada';
                break;
        }
    ?>
    </h4>
    <hr>
    <div id="detalle"><?= $logproyecto['Logproyecto']['info']; ?></div>
</div>
<div class="actions">
    <div><?php echo $this->Html->link(__('Listado de Bitácoras de Proyectos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
</div>