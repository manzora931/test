<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';?>

<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
</style>
<div class="rubros index container">
    <?php
    if(isset($_SESSION["delete"])){
        unset($_SESSION["delete"]); ?>
        <div class="alert alert-success " id="alerta" style="display: none;">
            Registro eliminado
        </div>
    <?php   }    ?>
    <?php
    if(isset($_SESSION["delete-no-priv"])){
        unset($_SESSION["delete-no-priv"]); ?>
        <div class="alert alert-warning" id="alerta" style="display: none;">
            No tiene privilegios para eliminar. Consulte con el administrador.
        </div>
    <?php   }    ?>
    <h2><?php echo __('Etapas por torneo'); ?></h2>
    <p>
        <?php echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));	?>
    </p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "catgastos";
    $session = $this->Session->read('tabla[catgastos]');
    $search_text = $session['search_text'] ;
    ?>
    <div id='search_box'>
        <?php
        /*Inicia formulario de busqueda*/
        $tabla = "etapasxtorneos";
        ?>
        <?php
        echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
        echo "<tr>";
       //echo "<td width='400px'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array("type"=>"hidden",'class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Rubro'));
        //echo "</td>";
        echo "<td style='width: 210px; padding-left: 4px;'>";
        echo $this->Form->input('torneo_id',array(
            'class'=>'form-control',
            'name'=>'data['.$tabla.'][torneo_id]',
            'options'=>$torneos,
            'label'=>'Torneo',
            'type'=>'select',
            'empty' => array( '(Seleccionar)'),
            'default'=>(isset($_SESSION['tabla[etapasxtorneos]']))?$_SESSION['tabla[etapasxtorneos]']['torneo_id']:''
        ));
        echo "</td>";
        echo "<td style='width: 210px; padding-left: 4px;'>";
        echo $this->Form->input('etapa_id',array(
            'class'=>'form-control',
            'name'=>'data['.$tabla.'][etapa_id]',
            'options'=>$etapas,
            'label'=>'Etapas',
            'type'=>'select',
            "onchange"=>"getEquipos(this.value)",
            'empty' => array( '(Seleccionar)'),
            'default'=>(isset($_SESSION['tabla[etapasxtorneos]']))?$_SESSION['tabla[etapasxtorneos]']['torneo_id']:''
        ));
        echo "</td>";
        echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
        echo $this->Form->hidden('Controller', array('value'=>'Etapasxtorneo', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo " <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>";

        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>
        <?php
        if(isset($_SESSION['etapasxtorneos']))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
        }
        ?>	</div>

    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id', "Id"); ?></th>
            <th><?php echo $this->Paginator->sort('Torneo.nombrecorto', "Torneo"); ?></th>
            <th><?php echo $this->Paginator->sort('Etapa.etapa', "Etapa"); ?></th>
            <th><?php echo $this->Paginator->sort('actual',"Actual"); ?></th>
            <th class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
	<tbody>
	<?php
    $i=0;
    foreach ($etapasxtorneos as $etapasxtorneo):
        $class=null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
    ?>
    <tr <?= $class;?> >
		<td><?php echo h($etapasxtorneo['Etapasxtorneo']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($etapasxtorneo['Torneo']['nombrecorto'], array('controller' => 'torneos', 'action' => 'view', $etapasxtorneo['Torneo']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($etapasxtorneo['Etapa']['etapa'], array('controller' => 'etapas', 'action' => 'view', $etapasxtorneo['Etapa']['id'])); ?>
		</td>
		<td><?= ($etapasxtorneo['Etapasxtorneo']['actual']==1)?"Si":"No"; ?>&nbsp;</td>
        <td class="">
            <?php echo $this->Html->link(__(' ', true), array('action'=>'view', $etapasxtorneo['Etapasxtorneo']['id']),array('class'=>'ver')); ?>
            <?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $etapasxtorneo['Etapasxtorneo']['id']),array('class'=>'editar')); ?>
            <?php echo $this->Html->link(__(' '), array('action' => 'delete', $etapasxtorneo['Etapasxtorneo']['id']),array('class'=>'deleteDet')); ?>
        </td>
    </tr>
<?php endforeach; ?>
    </tbody>
	</table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
</div>
<div class="actions">
    <ul>
        <li class="style-btn"><?php echo $this->Html->link(__('Crear Etapa por Torneo'), array('action' => 'add'),array('class'=>'btn btn-default')); ?></li>
    </ul>
</div>
<script>
    jQuery(function () {
        $(".deleteDet").click(function (e) {
            var resp = confirm("¿Esta seguro de eliminar el registro?");
            if(!resp){
                e.preventDefault();
            }
        });
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>