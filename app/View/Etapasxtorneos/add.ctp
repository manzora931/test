<?php
echo $this->Html->script(["bootbox.min"]);
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<script >
    function getURL(){
        return '<?=$real_url;?>';
    }
    $(document).ready(function(){

    });
</script>
<style>
    div.form{
        width: 100%;
        margin-bottom: 15px;
    }
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 15px;
        border:1px solid #cb071a;
        color: #011880;
    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }
    div.actions>div{
        display: inline-block;
    }
    .form-group{
        width:700px;
    }
    select.form-control {
        width: 300px;
    }
</style>
<div class="rubros form container">
    <?php echo $this->Form->create('Etapasxtorneo',array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Crear Etapa por Torneo'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php
            echo $this->Form->input('torneo_id', array('label' => 'Torneo',
                'class' => 'form-control txtNombre',
                'div' => ['class'=>'form-group'],
                'required'=>'required',
                "empty"=>"Seleccionar"
            ));
            echo $this->Form->input('etapa_id',array('label' => 'Etapa',
                'class' => 'form-control',
                'div' => ['class'=>'form-group'],
                "required",
                "empty"=>"Seleccionar"
            ));
            echo $this->Form->input('actual', array('label' => 'Actual',
                'div'=>false,'style'=>'margin:0; margin-right:4px;', "type"=>"checkbox"));

            echo $this->Form->input('observacion',array('label' => 'Observación',
                'class' => 'form-control',
                'rows' => 3,
                'div' => ['class'=>'form-group'],
                'value'=>'',
            ));
            ?>
        </div>
    </fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado'), array('controller' => 'etapasxtorneos', 'action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script>
    jQuery(function(){
        $("#EtapasxtorneoActual").click(function () {
            if($("#EtapasxtorneoActual").prop("checked")){
                var url = getURL()+"valActual";
                var torneo = $("#EtapasxtorneoTorneoId").val();
                if(torneo!==''){
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {torneo:torneo},
                        cache: false,
                        async: false,
                        success: function(resp){
                            if(parseInt(resp)>0){
                                /*bootbox.confirm({
                                    message: "Ya existe una etapa actual para el torneo seleccionar. ¿Desea cambiar la etapa actual?",
                                    buttons: {
                                        confirm: {
                                            label: 'Si',
                                            className: 'btn-success'
                                        },
                                        cancel: {
                                            label: 'No',
                                            className: 'btn-danger'
                                        }
                                    },
                                    callback: function (result) {
                                        if(!result) {
                                            $("#EtapasxtorneoActual").prop("checked", false);
                                        }
                                    }
                                });*/
                                var resp = confirm("Ya existe una etapa actual para el torneo seleccionado. ¿Desea cambiar la etapa actual?");
                                if(!resp){
                                    $("#EtapasxtorneoActual").prop("checked", false);
                                }
                            }
                        }
                    });
                }else {
                    $("#EtapasxtorneoActual").prop("checked", false);
                    $("#alerta .message").text("Debe seleccionar un torneo");
                    $("#alerta").slideDown();
                    setTimeout(function () {
                        $("#alerta").slideUp();
                    }, 4000);
                }

            }
        });

    });
</script>
