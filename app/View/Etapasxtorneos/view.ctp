<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style>
<div class="rubros view container-fluid">
    <h2><?php echo __('Etapa por Torneo'); ?></h2>
    <?php
    if(isset($_SESSION['etapaxtorneo_save'])){
        if($_SESSION['etapaxtorneo_save']==1){
            unset($_SESSION['etapaxtorneo_save']);
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro almacenado
            </div>
        <?php		}
    }
    ?>
    <table id="tblview">
        <tr>
            <td><strong>Id</strong></td><td><?php echo h($etapasxtorneo['Etapasxtorneo']['id']); ?></td>
        </tr>
        <tr>
            <td><strong>Torneo</strong></td><td><?php echo h($etapasxtorneo['Torneo']['nombrecorto']); ?></td>
        </tr>
        <tr>
            <td><strong>Etapa</strong></td><td><?php echo h($etapasxtorneo['Etapa']['etapa']); ?></td>
        </tr>
        <tr>
            <td><strong>Actual</strong></td><td><?= ($etapasxtorneo['Etapasxtorneo']['actual']==1)?"Si":"No"; ?></td>
        </tr>
        <tr>
            <td><strong>Observación</strong></td><td><?= $etapasxtorneo['Etapasxtorneo']['observacion']; ?></td>
        </tr>
        <tr>
            <td><strong>Creado</strong></td><td><?php echo h($etapasxtorneo['Etapasxtorneo']['usuario']); ?>
                <?php echo "(".h($etapasxtorneo['Etapasxtorneo']['created']); echo ")"; ?></td>
        </tr>
        <tr>
            <td><strong>Modificado</strong></td><td>
                <?php
                if(isset($etapasxtorneo['Etapasxtorneo']['usuariomodif'])){
                    echo h($etapasxtorneo['Etapasxtorneo']["usuariomodif"]);
                    echo "(".h($etapasxtorneo['Etapasxtorneo']['modified']); echo ")";
                }
                ?>
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'), array('class' => 'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $etapasxtorneo['Etapasxtorneo']['id']), array('class' => 'btn btn-default')); ?></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>