<?php
/* SVN FILE: $Id: default.ctp 7690 2008-10-02 04:56:53Z nate $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *                                1785 E. Sahara Avenue, Suite 490-204
 *                                Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright        Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link                http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package            cake
 * @subpackage        cake.cake.libs.view.templates.layouts
 * @since            CakePHP(tm) v 0.10.0.1076
 * @version            $Revision: 7690 $
 * @modifiedby        $LastChangedBy: nate $
 * @lastmodified    $Date: 2008-10-02 00:56:53 -0400 (Thu, 02 Oct 2008) $
 * @license            http://www.opensource.org/licenses/mit-license.php The MIT License
 */
date_default_timezone_set('America/El_Salvador');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php echo $this->html->charset('utf-8'); ?>
    <title>
        <?php echo __($this->Session->read('nombreorg')); ?>
        <?php echo $title_for_layout; ?>
    </title>
    <?php
    echo $this->html->meta('icon');
    //echo $this->Html->css('ui-lightness/jquery-ui-1.8.23.custom');//NECESARIO PARA EL CALENDARIO
    echo $scripts_for_layout;
    ?>
</head>
<body>
<div id="content">
    <?php echo $content_for_layout; ?>
</div>
<script>
    /*$(".datepicker").after("<button type='button' class='btn btn-default'><i class='fa fa-calendar'></i></button>");
    $(".datepicker + .btn").click(function () {
        $(this).prev(".datepicker").focus();
    });*/
</script>
</body>
</html>
