<?php
/* SVN FILE: $Id: default.ctp 7690 2008-10-02 04:56:53Z nate $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *								1785 E. Sahara Avenue, Suite 490-204
 *								Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link				http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package			cake
 * @subpackage		cake.cake.libs.view.templates.layouts
 * @since			CakePHP(tm) v 0.10.0.1076
 * @version			$Revision: 7690 $
 * @modifiedby		$LastChangedBy: nate $
 * @lastmodified	$Date: 2008-10-02 00:56:53 -0400 (Thu, 02 Oct 2008) $
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
	date_default_timezone_set('America/El_Salvador');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php echo $this->html->charset('utf-8'); ?>
	<title>
        <?php echo __($this->Session->read('nombreorg')); ?>
        <?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->script('jquery');
		echo $this->html->meta('icon');
		echo $this->Html->css('cake.generic2');
        echo $this->Html->css('ui-lightness/jquery-ui-1.8.23.custom');//NECESARIO PARA EL CALENDARIO
		echo $scripts_for_layout;
	?>

    <!--

        Estilo necesario para el menu.

    -->
    <style>
        <?php $a=1; foreach ($modulosM as $key => $modulo) {

            if($a < count($modulosM)){
        ?>
        #sub<?= $modulo['modulos']['id'] ?>,
        <?php }else{
        ?>
        #sub<?= $modulo['modulos']['id'] ?>
        <?php
        } $a++; } ?>
        {
            overflow: hidden;
            display: none;
        }
    </style>
</head>
<body>
		<div id="header" align="left">
			<a href='<?= Router::url('/')?>' title="">
				<img src="<?= Router::url('/')?>img/logo.jpg" alt="<?= $this->Session->read('nombreorg') ?> - Sistema Administrativo Financiero" border="0">
			</a>

			<span id="datos">
				<ul>
					<li >

						<?php
						if ($this->Session->read('nombrecompleto')) {
							echo "Bienvenido: ";
							echo "<a href=''>".$this->Session->read('nombrecompleto')."</a>";
							echo "</li>";
							echo "<li class='datosLi'>
									<a href='".Router::url('/')."paneladmin'>Admin</a>
								  </li >";
						}
						?>
					<li class="datosLi"><a href="">Soporte</a></li>
					<li class="datosLi"><a href="">Acerca de</a></li>
				</ul>
			</span>

		</div>

		<?php
			if ($this->Session->read('nombreusuario')) {
			?>
			<div id="menu">
				<ul>
					<?php
						$arInfId = array();
						foreach ($modulosM as $key => $modulo)
						{
							$conI=0;
							foreach($informe as $keyI => $valueI)
							{
								if($valueI['modulos']['id'] == $modulo['modulos']['id'])
								{
									if($conI==0)
									{
										$arInfId[$modulo['modulos']['id']]=array();
										$arInfId[$modulo['modulos']['id']][$conI]=$valueI['recursos'];
										$conI++;
									}else
									{
										$arInfId[$modulo['modulos']['id']][$conI]=$valueI['recursos'];
										$conI++;
									}

								}
							}
					?>
							<li><a href="#" id="menu<?= $modulo['modulos']['id'] ?>"><?= $modulo['modulos']['modulo'] ?></a></li>
					<?php
						}
					?>

					<li><a id="salir" href='<?= Router::url('/')?>users/logout'>Salir</a></li>
				</ul>
				</div>
                <div id="subMenu">

                    <?php
                    foreach ($modulosM as $key => $modulo)
                    {
                        ?>
                        <div id="sub<?= $modulo['modulos']['id'] ?>">
                            <ul>
                                <?php
                                foreach ($modelosM as $key => $modelo)
                                {
                                    if ($modulo['modulos']['id'] == $modelo['modulos']['id'])
                                    {
                                        ?>
                                        <li><a href="<?= Router::url('/').$modelo['recursos']['modelo'] ?>"><?= $modelo['recursos']['nombre'] ?></a></li>
                                        <?php
                                    }
                                }
                                if (isset($arInfId[$modulo['modulos']['id']]) && count($arInfId[$modulo['modulos']['id']])>0)
                                {
                                    ?>
                                    <li  id="sub-<?= $modulo['modulos']['id'] ?>-I" class="menuSup" style="position: absolute; z-index: 10; height: 20px;"><a href="#">Informes</a>
                                        <ul>

                                            <?php
                                            foreach ($arInfId[$modulo['modulos']['id']] as $key => $modelo)
                                            {
                                                ?>
                                                <li>
                                                    <a href="<?= Router::url('/').$modelo['modelo'] ?>"><?= $modelo['nombre'] ?></a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }else{
                ?>
                <div class="linea" style="background-color: #4e8ccf; height: 20px;">

                </div>
                <?php
            }
        ?>


        <div id="content">
            <?php echo $content_for_layout; ?>

        </div>
        <div id="footer">
            <?php echo $this->html->link(
                'Desarrollo e Implementacion por Tecnologias 101',
                'http://www.tecnologias101.com/',
                array('target'=>'_blank'), null, false
            );
            ?>
        </div>
        </div>
        <?php echo $this->Js->writeBuffer();?>
        <?php echo $this->element('sql_dump');?>
</body>
<script type="text/javascript">

    <?php
        foreach ($modulosM as $key => $modulo)
        {
    ?>
    var m<?= $modulo['modulos']['id'] ?> = document.getElementById('menu<?= $modulo['modulos']['id'] ?>');
    var s<?= $modulo['modulos']['id'] ?> = document.getElementById('sub<?= $modulo['modulos']['id'] ?>');
    m<?= $modulo['modulos']['id'] ?>.addEventListener("mouseover", function ()
    {
        $(".menuSeleccionado").attr('class', 'noSeleccionado');
        $(".subSeleccionado").attr('class', 'subNoSeleccionado');

        m<?= $modulo['modulos']['id'] ?>.setAttribute('class','menuSeleccionado');
        s<?= $modulo['modulos']['id'] ?>.setAttribute('class','subSeleccionado');

    });
    <?php
        }
    ?>

    var salir = document.getElementById('salir');

    salir.addEventListener("mouseover", function ()
    {
        $(".menuSeleccionado").attr('class', 'noSeleccionado');
        $(".subSeleccionado").attr('class', 'subNoSeleccionado');

        salir.setAttribute('class','menuSeleccionado');
    });


    function ancho()
    {
        <?php
            foreach ($modulosM as $key => $modulo)
            {
        ?>
        $("#sub-<?= $modulo['modulos']['id'] ?>-I").css('left',$("#sub<?= $modulo['modulos']['id'] ?>").width());
        <?php
            }
        ?>
    }
    function tamañoInformeMenu()
    {
        var enlaces = $(".menuSup ul li a");
        var MayorCatLetras=0;
        $.each(enlaces, function(index, listado)
        {
            var lick = $(enlaces[index]).text().length;
            if (lick > 50 && lick < 99)
            {
                var por = lick*0.35;
                lick = lick-por;
                if(lick > MayorCatLetras)
                {
                    MayorCatLetras = lick;
                }
            }else if(lick > 100)
            {
                lick = lick/2;
                if(lick > MayorCatLetras)
                {
                    MayorCatLetras = lick;
                }
            }else
            {
                var por = lick*0.20;
                lick = lick-por;
                if(lick > MayorCatLetras)
                {
                    MayorCatLetras = lick;
                }
            }
            $(".menuSup").css('width', MayorCatLetras+'em');
        });
    }
    $(document).ready(function() {
        ancho();
        tamañoInformeMenu();
    });
</script>
</html>