<?php
/**
 *
 * PHP versions 4 and 5
 *
 */
date_default_timezone_set('America/El_Salvador');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php echo $this->html->charset('utf-8'); ?>
    <title>
        <?= $title_for_layout; ?>
    </title>
    <?php
    echo $this->Html->script('jquery');
    echo $this->html->meta('icon');
    echo $this->Html->css(array('bootstrap/bootstrap', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'));
    echo $this->Html->script('bootstrap.min.js');
    echo $scripts_for_layout;
    ?>
</head>
<body>
<div id="content">
    <?php echo $content_for_layout; ?>
</div>


</body>
</html>
