<?php
/* SVN FILE: $Id: default.ctp 7690 2008-10-02 04:56:53Z nate $ */
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *                                1785 E. Sahara Avenue, Suite 490-204
 *                                Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright        Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link                http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package            cake
 * @subpackage        cake.cake.libs.view.templates.layouts
 * @since            CakePHP(tm) v 0.10.0.1076
 * @version            $Revision: 7690 $
 * @modifiedby        $LastChangedBy: nate $
 * @lastmodified    $Date: 2008-10-02 00:56:53 -0400 (Thu, 02 Oct 2008) $
 * @license            http://www.opensource.org/licenses/mit-license.php The MIT License
 */
date_default_timezone_set('America/El_Salvador');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php echo $this->html->charset('utf-8'); ?>
    <title>
        <?php echo __($this->Session->read('nombreorg')); ?>
        <?php echo $title_for_layout; ?>
    </title>
    <?php
    echo $this->Html->script('jquery');
    echo $this->html->meta('icon');
    echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'));
    //echo $this->Html->css('ui-lightness/jquery-ui-1.8.23.custom');//NECESARIO PARA EL CALENDARIO
    echo $this->Html->script('bootstrap.min.js');
    echo $scripts_for_layout;
    ?>
</head>
<body>
<div class="container-fluid">
    <header id="header">
        <div class="header-system">
            <div class="col-sm-9">
                <div id="datos">
                    <span class="system-name">Software Sports Media Analytics</span>
                    <ul>
                        <?php if ($loggedUser['Contacto']['nombres']) { ?>
                            <li>
                                <?php if ($loggedUser['User']['genero'] == 1) { ?>
                                    <span>Bienvenido, </span>
                                <?php } else { ?>
                                    <span>Bienvenida, </span>
                                <?php } ?>
                                <a href="#"><?= $loggedUser['User']['nombrecompleto'] ?></a>
                                <b class="primary-text"> <?= $userInstitucion['Paise']['pais'] ?></b>
                            </li>
                        <?php } else { ?>
                            <li>
                                <?php if ($loggedUser['User']['genero'] == 1) { ?>
                                    <span>Bienvenido, </span>
                                <?php } else { ?>
                                    <span>Bienvenida, </span>
                                <?php } ?>
                                <a href="#"><?= $loggedUser['User']['nombrecompleto'] ?></a>
                                <b class="primary-text"> <?= $pais['Paise']['pais'] ?></b>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <a class="logo pull-right" title="">
                    <?= $this->Html->image('LOGOS-UNIDOS.png') ?>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </header>
    <nav id="menu" class="clearfix menu">
        <div>
            <?php if ($this->Session->read('nombreusuario')) { ?>
                <div id="menu-button" class="pull-left">
                    <span class="bars"></span>
                </div>
                <ul id="main-menu" class="nav-left pull-left">

                    <?php foreach ($modulosM as $key => $modulo) { ?>
                        <li>
                            <a href="#" id="menu<?= $modulo['modulos']['id'] ?>" class="menu-item" data-forsub="<?= $modulo['modulos']['id'] ?>">
                                <?= $modulo['modulos']['modulo'] ?>
                            </a>
                            <ul class="submenu">
                                <?php foreach ($modelosM as $key => $modelo) { ?>
                                    <?php if ($modelo['modulos']['id'] === $modulo['modulos']['id']) { ?>
                                        <li>
                                            <?= $this->Html->link($modelo['recursos']['nombre'], [
                                                'controller' => $modelo['recursos']['modelo'],
                                                'action' => 'index'
                                            ]); ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    <li>
                        <?=  $this->Html->link('Salir', [
                            'controller' => 'users',
                            'action' => 'logout'
                        ]) ?>
                    </li>
                </ul>
                <div class="nav-right pull-right">
                    <a class="soporte btn" href="https://tec101.atlassian.net/servicedesk/customer/portal/6" target="_blank" style="margin-bottom: 0;margin-top: 4px;padding-bottom: 25px;margin-right: 20px;">Soporte</div></a>
                </div>
            <?php } else { ?>
                <div class="nav-left pull-left">
                    <ul id="main-menu">
                        <li><a href="#">Inicio</a></li>
                        <li><a href="#">Indicadores</a></li>
                        <li><a href="#">Soporte</a></li>
                    </ul>
                </div>
                <div class="nav-right pull-right">
                    <span>
                        <button href="#" class="btn btn-primary btn-xs" id="loginButton">Ingresar</button>
                    </span>
                    <span>
                        <button href="#" class="btn btn-primary btn-xs" id="registerButton">Registrarse</button>
                    </span>
                </div>
            <?php } ?>
        </div>
    </nav>
</div>
<div id="content">
    <?php echo $content_for_layout; ?>
</div>
<footer class="text-center">
    <div class="ssl-logos">
        <?= $this->Html->image('logos/ssl-logo.png', ['style'=>' margin-left: 95px;','class' => 'img-responsive']) ?>
        <span><?= date("Y")?> - Derechos Reservados, Sports Media Analytics, <a href="//sportsma.info" target="_blank">sportsma.info</a></span>
        <?= $this->Html->image('logos/100-secure.png', ['style'=>' margin-left: 5px;','class' => 'img-responsive']) ?>
    </div>

</footer>
<script>
    $(".datepicker").after("<button type='button' class='btn btn-default'><i class='fa fa-calendar'></i></button>");
    $(".datepicker + .btn").click(function () {
        $(this).prev(".datepicker").focus();
    });
</script>
<?php echo $this->Js->writeBuffer();?>
<?php echo $this->element('sql_dump');?>
</body>
</html>
