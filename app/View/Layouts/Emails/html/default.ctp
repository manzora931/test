<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Sistema de Denuncias</title>
</head>
<div>
	<img src="https://cursos.redca.org/imagenes/LOGOS-UNIDOS.png" width="185px">
</div>
<body>
	<?php echo $this->fetch('content'); ?>

</body>
<br><br>
<img src="https://cursos.redca.org/imagenes/sellos-de-seguridad.png" style="width:25%;">
<br>
<img src="https://cursos.redca.org/imagenes/footer.png" style="width:100%; height: 15px;">
<div class="footer-links">
	<label style="margin-right: 20px; margin-top: 8px;display: inline-block;"><a href="#">Terminos de Uso</a></label>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<label style="margin-right: 20px; margin-top:8px;margin-left: 30px;display: inline-block;"><a href="#">Soporte o Consultas</a></label>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<label style="margin-right: 20px; margin-top: 8px;margin-left: 30px;display: inline-block;"><a href="#">Recomendaciones</a></label>
</div>
</html>