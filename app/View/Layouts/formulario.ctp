<html>
	<head>
		<title>Formulario Denuncia</title>
		<link rel="icon" type="image/png" href="<?= $this->webroot ?>/img/favicon.png">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
		<?= $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') ?>
		<?= $this->Html->css('formularios') ?>
		<?= $this->Html->script('jquery') ?>
		<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
		<?= $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'); ?>
		<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
		<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-es.min.js') ?>
		<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.js') ?>
		<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css') ?>
		<?= $this->Html->script('datepicker-es') ?>
		<style>
			footer .ssl-logos div {
				float: none;
				display: inline-block;
				clear: none;
				width: 10%;
			}
			footer .ssl-logos div:first-child {
				margin-left: 75px;
			}
			*{
				vertical-align: middle;
			}
		</style>
		<script>
			$(function() {
				var telefono = $('.telefono');

				$('.datepicker').datepicker({
					dateFormat: "dd-mm-yy",
					changeMonth: true,
					changeYear: true,
					yearRange: "1960:2017"
				});
				$.datepicker.regional["es"];

				telefono.on("paste", function (event) {
					event.preventDefault();
				});

				telefono.keydown( function(event) {
					//ASCII CODES FOR KEYBOARD KEY
					var keys = [37, 38, 39, 40, 41, 43, 45, 8, 9, "-", "+"];

					if ((event.which < 48 || event.which > 57)
					&& (event.which < 112 || event.which > 123)
					&& (keys.indexOf(event.which) == -1 && keys.indexOf(event.key) == -1)
					&& (event.which < 96 || event.which > 105)) {
						event.preventDefault();
					}
				});

				$('.documento').keydown( function(event) {
					//ASCII CODES FOR KEYBOARD KEY
					var keys = [37, 38, 39, 40, 45, 8, 9, "-"];

					if ((event.which < 48 || event.which > 57)
					&& (event.which < 112 || event.which > 123)
					&& (keys.indexOf(event.which) == -1 && keys.indexOf(event.key) == -1)
					&& (event.which < 96 || event.which > 105)) {
						event.preventDefault();
					}
				});
			});
		</script>
	</head>
	<script>
		function nobackbutton(){
			window.location.hash = "formulario";
			window.location.hash = "denuncia";
			window.onhashchange = function(){window.location.hash="formulario";}
		}
	</script>
	<body>

			<?= $content_for_layout ?>

		<footer class="text-center">
			<div class="ssl-logos">
				<div>
					<?= $this->Html->image('logos/ssl-logo.png', ['style'=>' margin-left: 95px;','class' => 'img-responsive']) ?>
				</div>
				<span>2017 - Derechos Reservados, Programa Regional REDCA+</span>
				<div>
					<?= $this->Html->image('logos/100-secure.png', ['style'=>' margin-left: 5px;','class' => 'img-responsive']) ?>
				</div>
			</div>
		</footer>
	</body>
</html>
