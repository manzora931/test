
<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style>
<div class="catgastos view container">
    <h2><?php echo __('Tipo de Institución'); ?></h2>
    <?php
    if( isset( $_SESSION['tipoinstitucion_save'] ) ) {
        if( $_SESSION['tipoinstitucion_save'] == 1 ){
            unset( $_SESSION['tipoinstitucion_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Tipo de Institución almacenado
            </div>
        <?php }
    } ?>
    <table  id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($tipoinstitucion['Tipoinstitucion']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Tipo de Institución'); ?></td>
            <td>
                <?php echo h($tipoinstitucion['Tipoinstitucion']['tipoinstitucions']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Activo'); ?></td>
            <td>
                <?php
                $vactivo = 'No';
                if ($tipoinstitucion['Tipoinstitucion']['activo']) $vactivo = 'Si';
                ?>
                <?php echo $vactivo; ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Descripción'); ?></td>
            <td>
                <?= $tipoinstitucion['Tipoinstitucion']["descripcion"] ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php
                $creado = date("Y-m-d H:i", strtotime($tipoinstitucion['Tipoinstitucion']['created']));
                echo h($tipoinstitucion['Tipoinstitucion']['usuario']." (".$creado.")"); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?php
                $modificado = ($tipoinstitucion['Tipoinstitucion']['usuariomodif']!="")?$tipoinstitucion['Tipoinstitucion']['usuariomodif']." (".date("Y-m-d H:i", strtotime($tipoinstitucion['Tipoinstitucion']['modified'])).")":"";
                echo $modificado; ?>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
<div class="actions">

    <div><?php echo $this->Html->link(__('Editar Tipo de Institución'), array('action' => 'edit', $tipoinstitucion['Tipoinstitucion']['id']),array('class'=>'btn btn-default')); ?> </div>
    <div><?php echo $this->Html->link(__('Listado de Tipo de Institución'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>