<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';

?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
    $(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:<?=date("Y")?>"
        });
        $.datepicker.regional["es"];
    });
</script>
<div class="vgeneraljugadors index container-fluid">
    <h2>Reporte General de Juegos</h2>
    <p>
        <?php
        echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));
        ?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "vgeneraljuegos";
    $fecha=date('d-m-Y');
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table>";
        echo "<tr>";
        echo "<td>";
        echo $this->Form->input('torneo_id',array('label'=>'Torneo', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('jornada_id',array('label'=>'Jornada', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";   ?>
        <td style='padding-left: 10px;'>
            <?= $this->Form->input('desde', [
                'placeholder' => $fecha,
                'class' => 'form-control datepicker',
            ]) ?>
        </td>
        <td style='padding-left: 10px;'>
            <?= $this->Form->input('hasta', [
                'placeholder' => $fecha,
                'class' => 'form-control datepicker',
            ]) ?>
        </td>
        <?php
        echo "<td>";
        //$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Jugador', 'type'=>"hidden",'name'=>'data['.$tabla.'][search_text]','placeholder'=>'Jugador'));
        echo $this->Form->input('equipo_id',array('label'=>'Equipo', 'options'=>$equipos,'empty'=>'Seleccionar','class'=>'form-control jugador'));
        echo "</td>";

        echo "<td style='padding-left: 10px;'>";
        //$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Jugador', 'type'=>"hidden",'name'=>'data['.$tabla.'][search_text]','placeholder'=>'Jugador'));
        echo $this->Form->input('estadio_id',array('label'=>'Estadio', 'options'=>$estadios,'empty'=>'Seleccionar','class'=>'form-control jugador'));
        echo "</td>";


        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Vgeneraljuego', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('juego_id','Id'); ?></th>
            <th><?php echo $this->Paginator->sort('torneo','Torneo'); ?></th>
            <th><?php echo $this->Paginator->sort('jornada','Jornada'); ?></th>
            <th><?php echo $this->Paginator->sort('fecha','Fecha'); ?></th>
            <th><?php echo $this->Paginator->sort('equipo1','Equipo Local'); ?></th>
            <th colspan="2"><a>Marcador</a></th>
            <th><?php echo $this->Paginator->sort('equipo2','Equipo Visita'); ?></th>
            <th><?php echo $this->Paginator->sort('estadio','Estadio'); ?></th>
            <th><?php echo $this->Paginator->sort('asistencia','Asistentes'); ?></th>
            <th><?php echo $this->Paginator->sort('taquilla','Taquilla'); ?></th>
        </tr>
        </thead>
	<tbody>
	<?php
    $totasisten=0;
    $tottaquilla=0;
    setlocale(LC_MONETARY,"es_ES");
    foreach ($vgeneraljuegos as $vgeneraljuego): ?>
	<tr>
		<td><?php echo h($vgeneraljuego['Vgeneraljuego']['juego_id']); ?>&nbsp;</td>
		<td class="text-left"><?php echo $this->Html->link($vgeneraljuego['Vgeneraljuego']['torneo'],["controller"=>"torneos","action"=>"view",$vgeneraljuego['Vgeneraljuego']['torneo_id']]); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($vgeneraljuego['Vgeneraljuego']['jornada']); ?>&nbsp;</td>
		<td><?php
            $cad = '';
            if($vgeneraljuego['Vgeneraljuego']['hora']!=''){
                $cad = date('H:i', strtotime($vgeneraljuego['Vgeneraljuego']['hora']));
            }
            echo date("d/m/Y",strtotime($vgeneraljuego['Vgeneraljuego']['fecha']))." ".$cad; ?>&nbsp;</td>

		<td style="text-align: right;"><?php echo $this->Html->link($vgeneraljuego['Vgeneraljuego']['equipo1'],["controller"=>"equipos","action"=>"view",$vgeneraljuego['Vgeneraljuego']['equipo1_id']]); ?>&nbsp;</td>
		<td><?php echo h($vgeneraljuego['Vgeneraljuego']['golesequipo1']); ?>&nbsp;</td>
        <td><?php echo h($vgeneraljuego['Vgeneraljuego']['golesequipo2']); ?>&nbsp;</td>
		<td class="text-left"><?php  echo $this->Html->link($vgeneraljuego['Vgeneraljuego']['equipo2'],["controller"=>"equipos","action"=>"view",$vgeneraljuego['Vgeneraljuego']['equipo2_id']]); ?>&nbsp;</td>
		<td class="text-left"><?php echo $this->Html->link($vgeneraljuego['Vgeneraljuego']['estadio'],["controller"=>"estadios","action"=>"view",$vgeneraljuego['Vgeneraljuego']['estadio_id']]); ?>&nbsp;</td>
		<td ><?php echo number_format($vgeneraljuego['Vgeneraljuego']['asistencia'],2); ?>&nbsp;</td>
		<td><?php echo "$ ".number_format($vgeneraljuego['Vgeneraljuego']['taquilla'],2); ?>&nbsp;</td>
<?php
    $totasisten += $vgeneraljuego['Vgeneraljuego']['asistencia'];
    $tottaquilla += $vgeneraljuego['Vgeneraljuego']['taquilla'];
endforeach; ?>
    <tr>
        <td colspan="9"></td>
        <td><strong><?= number_format($totasisten, 2)?></strong></td>
        <td><strong>$ <?= number_format($tottaquilla, 2)?></strong></td>
    </tr>
    </tbody>
	</table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
</div>

