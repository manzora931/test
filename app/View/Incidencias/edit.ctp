<?= $this->Html->css(['proyecto/main']); ?>
<style>
    .deleteimg {
        cursor: pointer;
    }
    .deleterow {
        padding-top: 9px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-danger" id="error_edit" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="txtinc"></span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <input type="hidden" id="actividadId" value="<?= $this->data['Incidencia']['actividade_id'] ?>">
        <?=
        $this->Form->input('actividade_id',[
            'label'=>"Actividad",
            "required"=>true,
            "empty"=>"Seleccionar",
            "class"=>"form-control",
            'selected'=>$this->data['Incidencia']['actividade_id'],
            "div"=>["class"=>"form-group",
                'options'=>$actividades,
            ]
        ]);
        ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-6">
        <div class="formdetalleedad">
            <div class="col-sm-3"><label for="rangoEdadesIncidencia">Edades</label></div>
            <div class="col-sm-9">
                <span id="addrowrango" class="addrow"><i class="fa fa-plus"></i></span>
            </div>
            <div class="clearfix"></div>
            <div id="detalleedad">
                <input type="hidden" id="rangoId" value="<?=$rango['Rangoedadincidencia']['id']?>">
                <?php
                $cont =0;
                foreach ($this->data['Rangoedadincidencia'] as $rango): ?>
                    <div id="rowrango-<?= $rango['id'] ?>" class="rowrango">
                        <div class="col-sm-6">
                            <?= $this->Form->input("rangoedad_id",[
                                'label'=>false,
                                'required'=>true,
                                'id'=>'rangoEdadesIncidencia-' . $rango['id'],
                                'options'=>$edades,
                                'empty'=>"Seleccionar",
                                'class'=>'form-control rangoEdadesIncidencia form-incidencia',
                                'onchange'=>'verify_rango(this);',
                                'selected'=>$rango['rangoedad_id']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $this->Form->input("cantidadedad",[
                                'label'=>false,
                                'id'=>'cantidadedad-' . $rango['id'],
                                'required'=>true,
                                'class'=>'form-control rangoEdadesCantidad form-incidencia',
                                'value'=>$rango['cantidad'],
                                'onChange'=>'updateEdades();'
                            ]);?>
                        </div>
                        <div class="col-sm-2 deleterow">
                            <span id="deleterango-<?= $rango['id'] ?>" onclick="delete_rango(this);" class="deleterango deleteimg text-danger"><i class="fa fa-trash fa-lg"></i></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php
                $cont += $rango['cantidad'];
                endforeach; ?>
            </div>
            <div class="col-sm-12" style="margin-top: 5px;">
                <div class="col-sm-3" style="text-align: left;">
                    <span id="total1"><strong>Total</strong></span>
                </div>
                <div class="col-sm-offset-3 col-sm-2">
                    <span id="totedades" style="font-weight: bold;"><?=$cont?></span>
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="formdetallegenero">
            <div class="col-sm-3"><label for="generosIncidencia">Géneros</label></div>
            <div class="col-sm-9">
                <span id="addrowgenero" class="addrow"><i class="fa fa-plus"></i></span>
            </div>
            <div class="clearfix"></div>
            <div id="detallegenero">
                <input type="hidden" id="generoId" value="<?=$genero['Generoincidencia']['id']?>">
                <?php
                $cont2=0;
                foreach ($this->data['Generoincidencia'] as $genero): ?>
                    <div id="rowgenero-<?= $genero['id'] ?>" class="rowgenero">
                        <div class="col-sm-6">
                            <?= $this->Form->input("genero_id",[
                                'label'=>false,
                                'required'=>true,
                                'id'=>'generosIncidencia-' . $genero['id'],
                                'options'=>$generos,
                                'empty'=>"Seleccionar",
                                'class'=>'form-control generosIncidencia form-incidencia',
                                'onchange'=>'verify_genero(this);',
                                'selected'=>$genero['genero_id']]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $this->Form->input("cantidadgenero",[
                                'label'=>false,
                                'id'=>'cantidadgenero-' . $genero['id'],
                                'required'=>true,
                                'class'=>'form-control generoscantidad form-incidencia',
                                'value'=>$genero['cantidad'],
                                'onChange'=>'updateGenero();'
                            ]);?>
                        </div>
                        <div class="col-sm-2 deleterow">
                            <span id="deletegenero-<?= $genero['id'] ?>" onclick="delete_genero(this);" class="deletegenero deleteimg text-danger"><i class="fa fa-trash fa-lg"></i></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php
                $cont2 += $genero['cantidad'];
                endforeach; ?>
            </div>
            <div class="col-sm-12" style="margin-top: 5px;">
                <div class="col-sm-3">
                    <span id="total1"><strong>Total</strong></span>
                </div>
                <div class="col-sm-offset-3 col-sm-2">
                    <span id="totgenero" style="font-weight: bold;"><?=$cont2?></span>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateEdades(){
        var acum = 0;
        $(".rangoEdadesCantidad").each(function (key, index) {
            if(index.value !== ""){
                acum = acum + parseInt(index.value);
            }
        });
        $("#totedades").text(acum);
    }
    function updateGenero(){
        var acum=0;
        $(".generoscantidad").each(function(key, index){
            if(index.value !== ''){
                acum = acum + parseInt(index.value);
            }
        });
        $("#totgenero").text(acum);
    }
    $(function () {
        var addrango =  parseInt($('#rangoId').val());
        var addgenero = parseInt($('#generoId').val());

        $('#addrowrango').click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'get_edades')); ?>";
            var div = document.getElementById('detalleedad');
            var rowrango = document.createElement('div');
            var divrango = document.createElement('div');
            var divcantidad = document.createElement('div');
            var divdelete = document.createElement('div');
            var clearfix = document.createElement('div');
            clearfix.setAttribute('class', 'clearfix');
            addrango++;

            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: { id: addrango },
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    rowrango.setAttribute('id', 'rowrango-' + addrango);
                    rowrango.setAttribute('class', 'rowrango');
                    divrango.setAttribute('class', 'col-sm-6');
                    divcantidad.setAttribute('class', 'col-sm-4');
                    divdelete.setAttribute('class', 'col-sm-2 deleterow');

                    var selectrango = document.createElement('select');
                    var opt = document.createElement('option');

                    opt.value = "";
                    opt.innerHTML = "Seleccionar";

                    selectrango.setAttribute('id', 'rangoEdadesIncidencia-' + addrango);
                    selectrango.setAttribute('class', 'form-control rangoEdadesIncidencia form-incidencia');
                    selectrango.setAttribute('onchange', 'verify_rango(this)');
                    selectrango.setAttribute('required', 'required');
                    selectrango.appendChild(opt);
                    divrango.appendChild(selectrango);

                    var inputrango = document.createElement('input');
                    inputrango.setAttribute('id', 'cantidadedad-' + addrango);
                    inputrango.setAttribute('class', 'form-control rangoEdadesCantidad form-incidencia');
                    inputrango.setAttribute('required', 'required');
                    inputrango.setAttribute('onChange', 'updateEdades();');
                    divcantidad.appendChild(inputrango);

                    var span = document.createElement('span');
                    var iEdad = document.createElement('i');
                    span.setAttribute('class', 'deletegenero deleteimg text-danger');
                    span.setAttribute('id', 'deleterango-' + addrango);
                    span.setAttribute('onclick', 'delete_rango(this)');
                    iEdad.setAttribute('class', 'fa fa-trash fa-lg');
                    span.appendChild(iEdad);
                    divdelete.appendChild(span);

                    rowrango.appendChild(divrango);
                    rowrango.appendChild(divcantidad);
                    rowrango.appendChild(divdelete);
                    rowrango.appendChild(clearfix);
                    div.appendChild(rowrango);

                    // Se agrega la informacion de los rango de edades
                    response.edades.forEach(set_rangoedades);
                }
            });
        });

        $('#addrowgenero').click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'get_generos')); ?>";
            var div = document.getElementById('detallegenero');
            var rowgenero = document.createElement('div');
            var divgenero = document.createElement('div');
            var divcantidad = document.createElement('div');
            var divdelete = document.createElement('div');
            var clearfix = document.createElement('div');
            clearfix.setAttribute('class', 'clearfix');

            addgenero++;

            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: { id: addgenero },
                dataType: 'json',
                success: function (response) {
                    rowgenero.setAttribute('id', 'rowgenero-' + addgenero);
                    rowgenero.setAttribute('class', 'rowgenero');
                    divgenero.setAttribute('class', 'col-sm-6');
                    divcantidad.setAttribute('class', 'col-sm-4');
                    divdelete.setAttribute('class', 'col-sm-2 deleterow');

                    var selectgenero = document.createElement('select');
                    var opt = document.createElement('option');

                    opt.value = "";
                    opt.innerHTML = "Seleccionar";

                    selectgenero.setAttribute('id', 'generosIncidencia-' + addgenero);
                    selectgenero.setAttribute('class', 'form-control generosIncidencia form-incidencia');
                    selectgenero.setAttribute('onchange', 'verify_genero(this)');
                    selectgenero.setAttribute('required', 'required');
                    selectgenero.appendChild(opt);
                    divgenero.appendChild(selectgenero);

                    var inputgenero = document.createElement('input');
                    inputgenero.setAttribute('id', 'cantidadgenero-' + addgenero);
                    inputgenero.setAttribute('onChange', 'updateGenero();');
                    inputgenero.setAttribute('class', 'form-control generoscantidad form-incidencia');
                    inputgenero.setAttribute('required', 'required');
                    divcantidad.appendChild(inputgenero);

                    var span = document.createElement('span');
                    var iGenero = document.createElement('i');
                    span.setAttribute('class', 'deletegenero deleteimg text-danger');
                    span.setAttribute('id', 'deletegenero-' + addgenero);
                    span.setAttribute('onclick', 'delete_genero(this)');
                    iGenero.setAttribute('class', 'fa fa-trash fa-lg');
                    span.appendChild(iGenero);
                    divdelete.appendChild(span);

                    rowgenero.appendChild(divgenero);
                    rowgenero.appendChild(divcantidad);
                    rowgenero.appendChild(divdelete);
                    rowgenero.appendChild(clearfix);
                    div.appendChild(rowgenero);

                    // Se agrega la informacion de los generos
                    response.genero.forEach(set_generos);
                }
            });
        });

        // Se adicionan las opciones del combobox de "Rango de Edades"
        function set_rangoedades(item, index) {
            var select = document.getElementById('rangoEdadesIncidencia-' + addrango);
            var opt = document.createElement('option');

            opt.value = item.Rangoedade.id;
            opt.innerHTML = "De "+item.Rangoedade.desde+" Años a "+item.Rangoedade.hasta+" Años";
            select.appendChild(opt);
        }

        // Se adicionan las opciones del combobox de "Generos"
        function set_generos(item, index) {
            var select = document.getElementById('generosIncidencia-' + addgenero);
            var opt = document.createElement('option');

            opt.value = item.Genero.id;
            opt.innerHTML = item.Genero.genero;
            select.appendChild(opt);
        }
    });
</script>