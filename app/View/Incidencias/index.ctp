<?= $this->Html->css(['proyecto/main']); ?>
<style>
    .tbinc {
        background-color: #e6e6e6;
        color: #000 !important;
        border: none;
    }
    .act {
        padding-left: 20px !important;
    }
    .titleModal {
        font-size: 1.14em;
    }
    .link {
        cursor: pointer;
        text-decoration: underline
    }
    #cancelar {
        margin-top: 0px;
    }
    .form-incidencia {
        width: 100%;
        margin-top: 7px;
    }

    .formdetalleedad, .formdetallegenero {
        border: solid 0.5px #e6e6e6;
        border-radius: 5px;
        padding-bottom: 10px;
        padding-top: 5px;
        margin-top: 15px;
    }

    .addrow {
        cursor: pointer;
    }
    .form-group {
        width: 100%;
    }
</style>
<div class="incidencias index container-fluid">
    <div class="row">
        <?php if(isset($_SESSION['saveincidencia'])){
            unset($_SESSION['saveincidencia']); ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="success-incidencia" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Incidencia Almacenada Exitosamente</span>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <div class="alert alert-danger" id="error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="alert alert-success" id="success" style="display: none">
                <span class="icon icon-check-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h2 style="font-size: 1.4em;color:#cb071a;">Incidencias</h2>
        </div><br>
        <div class="col-sm-offset-2 col-sm-6 div-actions">
            <?php if($infoP['Proyecto']["estado_id"]==2){ ?>
            <table width="100%">
                <td class="acciones">
                    <button type="button" class="btn btn-crear acciones-shadow" id="add-details" name="button">Agregar</button>
                </td>
                <td class="acciones">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="edit-details" name="button">Modificar</button-->
                    <button type="button" class="btn btn-acciones acciones-shadow"  id="edit-details" data-toggle="modal">Modificar</button>
                </td>
                <td class="acciones">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                </td>
                <td class="acciones">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/incidencias', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>
            </table>
            <?php   }   ?>
        </div>
        <div class="clearfix"></div>
        <br><br>
    </div>
    <div class="row form-incidencias hide">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="encabezado-panel"></span>
                </div>
                <div class="panel-body">
                    <form id="formulario-incidencias" method="post" style="padding: 5px;">
                        <table width="100%">
                            <tr>
                                <td style="width: 200px; padding-bottom: 5px;">
                                    <input type="hidden" id="proyecto" value="<?= $idProyecto ?>">
                                    <input type="hidden" id="incidenciaid">
                                    <?= $this->Form->input('actividade_id',
                                        array('label' => 'Actividad',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'actividad-incidencia',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('genero_id',
                                        array('label' => 'Género',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'options' => $generos,
                                            'id' => 'genero',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td style="width: 100px; padding-bottom: 5px;">
                                    <?= $this->Form->input('cantidadgenero',
                                        array('label' => 'Cantidad',
                                            'class' => 'form-control',
                                            'id' => 'cantidad-genero',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('rangoedad_id',
                                        array('label' => 'Rango de Edad',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'options' => $rangoedades,
                                            'id' => 'rangoedad',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td style="width: 100px; padding-bottom: 5px;">
                                    <?= $this->Form->input('cantidadrango',
                                        array('label' => 'Cantidad',
                                            'class' => 'form-control',
                                            'id' => 'cantidad-rango',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <div class="acciones-panel pull-right">
                                        <button type="button" class="btn btn-danger acciones-fuente" id="almacenar" name="button" style="width: 100px">Almacenar</button>
                                        <button type="button" class="btn btn-default " id="cancelar" name="button" style="width: 100px">Cancelar</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
        <?php foreach($modules as $modulo) { ?>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span class="stage-title"><?= $modulo['Module']['nombre']?></span>
                        </h4>
                    </div>
                    <div id="frm" class="" role="tabpanel">
                        <div class="panel-body" id="">
                            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
                            <?php if(count($intervenciones[$modulo['Module']['id']]) > 0) { ?>
                                <?php foreach($intervenciones[$modulo['Module']['id']] as $intervencion) { ?>
                                    <div class="panel-info">
                                        <div class="panel-heading" role="tab">
                                            <h4 class="panel-title">
                                                <span class="stage-title" style="color: #000;"><?= $intervencion['nombre'] ?> </span>
                                            </h4>
                                        </div>
                                        <div class="panel-body" id="">
                                            <div class="panel-default">
                                                <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                                                    <thead class="thead-default">
                                                    <tr>
                                                        <?php $cols=($infoP["Proyecto"]["estado_id"]==2)?2:1;?>
                                                        <th colspan="<?=$cols?>" class="text-left tbinc act"><?php echo h('Actividades'); ?></th>
                                                        <th colspan="2" class="text-center tbinc"><?php echo h('Población'); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    <!-- ---------- RECORRIDO DE LAS INCIDENCIAS ------------- -->
                                                    <?php if(isset($incidencias[$intervencion['id']])) { ?>
                                                        <?php if(count($incidencias[$intervencion['id']]) > 0) { ?>
                                                            <?php foreach ($incidencias[$intervencion['id']] as $incidencia): ?>
                                                                <tr>
                                                                    <?php if($infoP["Proyecto"]["estado_id"]==2){   ?>
                                                                    <td style="text-align: center !important; width: 5%;">
                                                                        <input type="radio" name="item-selected" class="item-selected" value="<?= $incidencia['Incidencia']['id'] ?>">
                                                                    </td>
                                                                    <?php } ?>
                                                                    <td style="text-align: left"><?php echo h($incidencia['Actividade']['nombre']); ?></td>
                                                                    <td style="text-align: center; width: 20%;">
                                                                        <a data-toggle="modal" class="vrangoedades link" id="vrangoedades-<?= $incidencia['Incidencia']['id'] ?>" data-incidencia="<?= $incidencia['Incidencia']['id'] ?>">Edades</a>
                                                                    </td>
                                                                    <td style="text-align: center; width: 20%;">
                                                                        <a data-toggle="modal" class="vgeneros link" id="vgeneros-<?= $incidencia['Incidencia']['id'] ?>" data-incidencia="<?= $incidencia['Incidencia']['id'] ?>">Género</a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        <?php } else {
                                                            $cols2=($infoP["Proyecto"]["estado_id"]==2)?4:3;
                                                            ?>
                                                            <tr>
                                                                <td colspan="<?=$cols2?>" class="text-center">No hay incidencias relacionadas</td>
                                                            </tr>
                                                        <?php } ?>
                                                        <!-- ---- FIN IF ISSET INCIDENCIAS ------ -->
                                                    <?php } ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ---- FIN FOREACH INTERVENCIONES ------ -->
                                <?php } ?>
                                <!-- ---- FIN IF COUNT INTERVENCIONES ------ -->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ---- FIN FOREACH MODULOS ------ -->
        <?php } ?>
    </div>
</div>

<!-- MODAL PARA VER DETALLES INCIDENCIA - GENEROS -->
<div class="modal fade" id="viewGenero" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="titleModal" id="generosLabel">Incidencias - Géneros</h2>
            </div>
            <div id="genbody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL PARA VER DETALLES INCIDENCIA - RANGO DE EDADES -->
<div class="modal fade" id="viewEdades" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="titleModal" id="edadesLabel">Incidencias - Rango de Edades</h2>
            </div>
            <div id="rangobody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FORMULARIO DE INCIDENCIA -->
<div class="modal fade" id="modalSaveIncidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="titleModal" id="titleIncidencia"></h2>
            </div>
            <div id="modalBodyIncidenciaSave" class="modal-body">
                <!--form id="modalBodyIncidenciaForm" class="form-inline" method="post"></form-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="saveIncidencia">Almacenar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var row = '';
    var cont = 0;

    $("#success-incidencia").slideDown();
    setTimeout(function(){
        $("#success-incidencia").slideUp();
    },4000);

    $(function () {
        var btnAdd = $(".btn-crear");
        var btnEdit = $("#edit-details");
        var btnDelete = $("#delete-details");
        var btnGenero = $(".vgeneros");
        var btnEdades = $(".vrangoedades");
        var btnIncidencia = $("#saveIncidencia");
        var proyecto = '<?=$idProyecto?>';

        // Se verifica si tiene un proyecto asociado
        if(proyecto == '') {
            // Deshabilita las acciones que se realizan en Incidencias
            btnAdd.attr('disabled', true);
            btnEdit.attr('disabled', true);
            btnDelete.attr('disabled', true);

        } else {

            // Habilita las acciones que se realizan en Incidencias
            btnAdd.attr('disabled', false);
            btnEdit.attr('disabled', false);
            btnDelete.attr('disabled', false);
        }

        btnGenero.click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'get_generos')); ?>";
            var incidenciaid = $(this).attr('data-incidencia');
            var html = '';
            var body = $("#genbody");
            body.html('');

            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:incidenciaid},
                dataType: 'json',
                success: function (data) {
                    html+='<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<h3>' + data.data.Actividade.nombre + '</h3>' +
                        '</div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<table border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">' +
                        '<thead>' +
                        '<tr>' +
                        '<th class="text-center">Genero</th><th class="text-center">Cantidad</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    var cont =0;
                    $.each(data.data.Generoincidencia, function( index, value ) {
                        var genero = $.grep(data.genero, function(e){ return e.Genero.id == value.genero_id; });
                        cont = cont + parseInt(value.cantidad);
                        html+='<tr>' +
                            '<td class="text-left">' + genero[0].Genero.genero +'</td><td class="text-center">' + value.cantidad + '</td>' +
                            '</tr>';
                    });
                    html += '<tr><td><strong>Total</strong></td><td><strong>'+cont+'</strong></td></tr>';
                    html+='</tbody></table></div></div>';
                    body.html(html);
                    $('#viewGenero').modal('show');
                }
            });

            return false;
        });

        btnEdades.click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'get_edades')); ?>";
            var incidenciaid = $(this).attr('data-incidencia');
            var html = '';
            var body = $("#rangobody");
            body.html('');

            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:incidenciaid},
                dataType: 'json',
                success: function (data) {
                    html+='<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<h3>' + data.data.Actividade.nombre + '</h3>' +
                        '</div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-sm-12">' +
                        '<table border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">' +
                        '<thead>' +
                        '<tr>' +
                        '<th class="text-center">Rango de Edad</th><th class="text-center">Cantidad</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    var cont=0;
                    $.each(data.data.Rangoedadincidencia, function( index, value ) {
                        var rangoedad = $.grep(data.edades, function(e){ return e.Rangoedade.id == value.rangoedad_id; });
                        cont = cont + parseInt(value.cantidad);
                        html+='<tr>' +
                            '<td class="text-left">De ' + rangoedad[0].Rangoedade.desde +' Años a '+rangoedad[0].Rangoedade.hasta+' Años</td><td class="text-center">' + value.cantidad + '</td>' +
                            '</tr>';
                    });
                    html += '<tr><td><strong>Total</strong></td><td><strong>'+cont+'</strong></td></tr>';
                    html+='</tbody></table></div></div>';
                    body.html(html);
                    $('#viewEdades').modal('show');
                }
            });

            return false;
        });

        // Al momento de hacer click en el boton "Agregar"
        btnAdd.click( function () {
            $("#incidenciaid").val('');

            $('#titleIncidencia').html('Agregar Incidencia');
            var body = $("#modalBodyIncidenciaSave");
            body.load(getURL()+"incidencias/add/" + proyecto);
            $('#modalSaveIncidencia').modal('show');

            return false;
        });

        btnIncidencia.click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'save_incidencia')); ?>";
            var rangoedades = new Array();
            var nrangoedades = new Array();
            var cantedades = new Array();
            var generos = new Array();
            var ngeneros = new Array();
            var cantgeneros = new Array();
            var actividadid = $('#actividade_id').val();
            var actividad = $("#actividade_id  option:selected").text();
            var hayfaltantes = 0;
            var invalido = 0;

            $('.rangoEdadesIncidencia').each(function(i, obj) {
                if($(obj).val() !== '') {
                    rangoedades.push($(obj).val());
                    nrangoedades.push($(obj).find('option:selected').text());
                } else {
                    hayfaltantes++;
                }
            });

            $('.rangoEdadesCantidad').each(function(i, obj) {
                var valor = parseInt($(obj).val());

                if($(obj).val() === '') {
                    hayfaltantes++;
                } else {
                    if (isNaN(valor)) {
                        invalido++;
                    } if (valor <= 0) {
                        invalido++;
                    } else {
                        if (valor % 1 == 0) {
                            cantedades.push($(obj).val());
                        }
                    }
                }
            });

            $('.generosIncidencia').each(function(i, obj) {
                if($(obj).val() !== '') {
                    generos.push($(obj).val());
                    //ngeneros.push($(obj + ' option:selected').text());
                    ngeneros.push($(obj).find('option:selected').text());
                } else {
                    hayfaltantes++;
                }
            });

            $('.generoscantidad').each(function(i, obj) {
                var valor = parseInt($(obj).val());

                if($(obj).val() === '') {
                    hayfaltantes++;
                } else {
                    if (isNaN(valor)) {
                        invalido++;
                    } if (valor <= 0) {
                        invalido++;
                    } else {
                        if (valor % 1 == 0) {
                            cantgeneros.push($(obj).val());
                        }
                    }
                }
            });

            if(hayfaltantes == 0 && actividadid !== '') {
                if(invalido == 0) {
                    var data = {
                        'incidencia_id': $("#incidenciaid").val(),
                        'actividade_id': actividadid,
                        'actividad': actividad,
                        'rangoedades': rangoedades,
                        'nrangoedades': nrangoedades,
                        'cantedades': cantedades,
                        'generos': generos,
                        'ngeneros': ngeneros,
                        'cantgeneros': cantgeneros
                    };

                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: data,
                        dataType: 'json',
                        success: function (response) {
                            if(response.msg == 'exito') {
                                $('.form-incidencias').toggleClass('hide');
                                $('#tblin').toggleClass('hide');

                                $('#modalSaveIncidencia').modal('toggle');
                                setTimeout(function () {
                                    $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                                }, 300);
                            } else {
                                $("#error .txt").text('Error al almacenar la incidencia');
                                $("#error").slideDown();

                                $('#modalSaveIncidencia').modal('toggle');
                                setTimeout(function () {
                                    $("#error").slideUp();
                                    $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                                }, 4000);
                            }
                        }
                    });
                } else {
                    $("#error_edit .txtinc").text('Las cantidades deben ser valores válidos.');
                    $("#error_edit").slideDown();

                    setTimeout(function () {
                        $("#error_edit").slideUp();
                    }, 4000);
                }
            } else {
                $("#error_edit .txtinc").text('Se deben de ingresar todos los campos.');
                $("#error_edit").slideDown();

                setTimeout(function () {
                    $("#error_edit").slideUp();
                }, 4000);
            }
        });

        // Al momento de hacer click en el boton "Modificar"
        btnEdit.click( function () {
            $('#titleIncidencia').html('Editar Incidencia');
            var id = $(".item-selected:checked").val();
            var body = $("#modalBodyIncidenciaSave");

            $("#incidenciaid").val(id);

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                body.load(getURL()+"incidencias/edit/" + proyecto + "/"  + id);
                $('#modalSaveIncidencia').modal('show');
            }
        });

        // Al momento de hacer click en el boton "Eliminar"
        btnDelete.click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'delete')); ?>";
            var id = $(".item-selected:checked").val();
            var proyectoid = '<?=$idProyecto?>';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                var r = confirm("¿Esta seguro de eliminar la incidencia?");

                if (r == true) {
                    request = $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:id, proyectoid: proyectoid},
                        dataType: 'json'
                    });

                    request.done(function (response, textStatus, jqXHR){
                        if(response.msg == 'exito') {
                            $("#success .txt").text('Incidencia eliminado exitosamente');
                            $("#success").slideDown();

                            setTimeout(function () {
                                $("#success").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error1') {
                            $("#error .txt").text('Incidencia no existe');
                            $("#error").slideDown();

                            setTimeout(function () {
                                $("#error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error2') {
                            $("#error .txt").text('Error al eliminar la incidencia');
                            $("#error").slideDown();

                            setTimeout(function () {
                                $("#error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        }
                    });
                }
            }

            return false;
        });

        // Al momento de almacenar la informacion ingresada del desembolso
        $('#almacenar').click( function () {
            var url = "<?= Router::url(array('controller' => 'incidencias', 'action' => 'almacenar_data')); ?>";

            if(cont === 0){
                cont++;
                var data = new FormData();
                data.append("id",$("#incidenciaid").val());
                data.append("proyectoid",'<?=$idProyecto?>');
                data.append("actividad-incidencia",$('#actividad-incidencia').val());
                data.append("genero",$('#genero').val());
                data.append("cantidad-genero",$('#cantidad-genero').val());
                data.append("rangoedad",$('#rangoedad').val());
                data.append("cantidad-rango",$('#cantidad-rango').val());

                $('#tblin').toggleClass('hide');

                request = $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    dataType:'json',
                    cache:false,
                    processData:false,
                });

                request.done(function (response, textStatus, jqXHR){
                    if(response.msg == 'exito') {
                        $('.form-incidencias').toggleClass('hide');
                        $('#tblin').toggleClass('hide');
                        $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                    } else if(response.msg == 'error1') {
                        $("#error .txt").text('La cantidad ingresada es inválida');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                            $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error2') {
                        $("#error .txt").text('Error al almacenar la incidencia');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                            $("#childs").load('<?= Router::url(['controller'=>'incidencias','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    }
                });
            }
        });
    });

    function verify_rango(obj) {
        var elementid = $(obj).attr('id');
        var res = elementid.split("-");
        var id = $(obj).val();
        var rangoedades = new Array();

        $('.rangoEdadesIncidencia').each(function(i, o) {
            var elementoid = $(o).attr('id');
            var elem = elementoid.split("-");

            if(res[1] !== elem[1]) {
                rangoedades.push($(o).val());
            }
        });

        var result = $.grep(rangoedades, function(e){ return e == id; });
        if(result.length > 0 && id !== '') {
            $(obj).val('');

            $("#error_edit .txtinc").text('El rango de edad ya ha sido seleccionado.');
            $("#error_edit").slideDown();

            setTimeout(function () {
                $("#error_edit").slideUp();
            }, 4000);
        }
    }

    function verify_genero(obj) {
        var elementid = $(obj).attr('id');
        var res = elementid.split("-");
        var id = $(obj).val();
        var generos = new Array();

        $('.generosIncidencia').each(function(i, o) {
            var elementoid = $(o).attr('id');
            var elem = elementoid.split("-");

            if(res[1] !== elem[1]) {
                generos.push($(o).val());
            }
        });

        var result = $.grep(generos, function(e){ return e == id; });
        if(result.length > 0 && id !== '') {
            $(obj).val('');

            $("#error_edit .txtinc").text('El género ya ha sido seleccionado.');
            $("#error_edit").slideDown();

            setTimeout(function () {
                $("#error_edit").slideUp();
            }, 4000);
        }
    }

    function delete_rango(obj) {
        var elementid = $(obj).attr('id');
        var res = elementid.split("-");
        var r = confirm("¿Esta seguro de eliminar el rango de edad de la incidencia?");

        if (r == true) {
            if($('.rowrango').length > 1) {
                $( "#rowrango-" + res[1] ).remove();
            } else {
                $("#error_edit .txtinc").text('Debe almacenar al menos un rango de edad.');
                $("#error_edit").slideDown();

                setTimeout(function () {
                    $("#error_edit").slideUp();
                }, 4000);
            }

        }
    }

    function delete_genero(obj) {
        var elementid = $(obj).attr('id');
        var res = elementid.split("-");
        var r = confirm("¿Esta seguro de eliminar el género de la incidencia?");

        if (r == true) {
            if($('.rowgenero').length > 1) {
                $( "#rowgenero-" + res[1] ).remove();
            } else {
                $("#error_edit .txtinc").text('Debe almacenar al menos un género.');
                $("#error_edit").slideDown();

                setTimeout(function () {
                    $("#error_edit").slideUp();
                }, 4000);
            }
        }
    }

    function crear_formulario(data) {
        var modalbody = document.getElementById('modalBodyIncidenciaSave');

        // Se crean los elementos para crear formulario
        var clearfix = document.createElement('div');
        clearfix.setAttribute('class', 'clearfix');

        var rowAct = document.createElement('div');
        rowAct.setAttribute('class', 'row');

        var divActividad = document.createElement('div');
        divActividad.setAttribute('class', 'col-sm-6');

        var labelActividad = document.createElement('label');
        var tAct = document.createTextNode('Actividad');
        labelActividad.appendChild(tAct);

        var selectActividad = document.createElement('select');
        selectActividad.setAttribute('id', 'actividade-incidencia');
        selectActividad.setAttribute('class', 'form-control');
        selectActividad.setAttribute('required', 'required');

        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "Seleccionar";

        // Se adiciona el combobox de "Actividades"
        divActividad.appendChild(labelActividad);
        divActividad.appendChild(selectActividad);
        rowAct.appendChild(divActividad);
        modalbody.appendChild(rowAct);
        modalbody.appendChild(clearfix);

        // Se agrega la informacion de las actividades
        selectActividad.appendChild(opt);
        data.actividades.forEach(set_actividades);
        $('#actividade-incidencia').val(data.data.Actividade.id);

        var rowDet = document.createElement('div');
        rowDet.setAttribute('class', 'row');

        var divEdad = crear_divEdades(data.data.Rangoedadincidencia, clearfix);
        var divGenero = crear_divGeneros(data.data.Generoincidencia, clearfix);

        rowDet.appendChild(divEdad);
        rowDet.appendChild(divGenero);
        modalbody.appendChild(rowDet);
        modalbody.appendChild(clearfix);
    }

    function crear_divEdades(data, clearfix) {
        var divEdad = document.createElement('div');
        divEdad.setAttribute('class', 'col-sm-6');

        var formdetalle = document.createElement('div');
        formdetalle.setAttribute('class', 'formdetalleedad');

        var divlblEdad = document.createElement('div');
        divlblEdad.setAttribute('class', 'col-sm-12');

        var labelEdad = document.createElement('label');
        var tEdad = document.createTextNode('Edades');
        labelEdad.appendChild(tEdad);

        var iEdad = document.createElement('i');
        iEdad.setAttribute('class', 'fa fa-plus');

        divlblEdad.appendChild(labelEdad);
        divlblEdad.appendChild(iEdad);
        formdetalle.appendChild(divlblEdad);
        formdetalle.appendChild(clearfix);

        divEdad.appendChild(formdetalle);
        return divEdad;
    }

    function crear_divGeneros(data, clearfix) {
        var divGenero = document.createElement('div');
        divGenero.setAttribute('class', 'col-sm-6');

        var formdetalle = document.createElement('div');
        formdetalle.setAttribute('class', 'formdetallegenero');

        var divlblGenero = document.createElement('div');
        divlblGenero.setAttribute('class', 'col-sm-12');

        var labelGenero = document.createElement('label');
        var tGenero = document.createTextNode('Géneros');
        labelGenero.appendChild(tGenero);

        divlblGenero.appendChild(labelGenero);
        formdetalle.appendChild(divlblGenero);
        formdetalle.appendChild(clearfix);

        divGenero.appendChild(formdetalle);
        return divGenero;
    }

    // Se adicionan las opciones del combobox de "Actividades"
    function set_actividades(item, index) {
        var select = document.getElementById('actividade-incidencia');
        var opt = document.createElement('option');

        opt.value = item.Actividade.id;
        opt.innerHTML = item.Actividade.nombre;
        select.appendChild(opt);
    }
</script>