<style>
    div.form{
        width: 100%;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 15px;
        border:1px solid #cb071a;
        color: #011880;

    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }
    div.actions>div{
        display: inline-block;
    }
    .form-group{
        width:300px;
    }
</style>
<div class="privilegios form container">

    <?php echo $this->Form->create('Privilegio', ['class' => 'add-edit']); ?>
    <fieldset>
        <legend><?php echo __('Editar Privilegio'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>

        <div class="col-xs-12">
            <?= $this->Form->input('id'); ?>
            <?= $this->Form->input('group_id', [
                'label' => 'Perfil',
                'class' => 'form-control special-select',
                'div' => [
                    'class' => 'form-group'
                ]
            ]) ?>
            <?= $this->Form->input('recurso_id', [
                'class' => 'form-control special-select',
                'div' => [
                    'class' => 'form-group'
                ]
            ]) ?>
            <div class="form-group">
                <label for="PrivilegioLeer">Leer</label>
                <?= $this->Form->input('leer', [
                    'label' => false,
                    'type' => 'checkbox',
                    'div' => false
                ]); ?>
            </div>
            <div class="form-group">
                <label for="PrivilegioAdicionar">Adicionar</label>
                <?= $this->Form->input('adicionar', [
                    'label' => false,
                    'type' => 'checkbox',
                    'div' => false
                ]); ?>
            </div>
            <div class="form-group">
                <label for="PrivilegioEditar">Adicionar</label>
                <?= $this->Form->input('editar', [
                    'label' => false,
                    'type' => 'checkbox',
                    'div' => false
                ]); ?>
            </div>
            <div class="form-group">
                <label for="PrivilegioBorrar">Borrar</label>
                <?= $this->Form->input('borrar', [
                    'label' => false,
                    'type' => 'checkbox',
                    'div' => false
                ]); ?>
            </div>
            <!--<div class="form-group">
                <label for="Privilegio">Mis registros</label>
                <?php // echo $this->Form->input('solopropietario', ['label' => false, 'type' => 'checkbox', 'div' => false]); ?>
            </div>-->
            <div id="botones" style="margin-bottom: 1em;">
                <?php if(count($detrecursos)){ ?>
                    <h3>Detalles de privilegio - Botones</h3>
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>Nombre a mostrar</th>
                            <th>Funcion</th>
                            <th>Permitir</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($detrecursos as $kRecursos => $vRecursos) {
                            foreach($detprivilegios as $kpriv => $vPriv){
                                if($vRecursos['Detrecurso']['id']==$vPriv['Detprivilegio']['detrecurso_id']){

                                    ?>
                                    <tr>
                                    <td><?= $vRecursos['Detrecurso']['nombre'] ?></td>
                                    <td><?= $vRecursos['Detrecurso']['funcion'] ?></td>
                                    <td style="text-align: center;">
                                    <input type='hidden' name='data[det][<?= $kRecursos ?>][id]' value='<?= $vPriv['Detprivilegio']['id'] ?>' />
                                    <input type='hidden' name='data[det][<?= $kRecursos ?>][detrecurso_id]' value='<?= $vPriv['Detprivilegio']['detrecurso_id'] ?>' />
                                    <input type='hidden' name='data[det][<?= $kRecursos ?>][permitir]' id='det<?= $kRecursos ?>_' value='0' />
                                    <?php
                                    if($vPriv['Detprivilegio']['permitir']==1){
                                        ?>
                                        <input type='checkbox' name='data[det][<?= $kRecursos ?>][permitir]' id='det<?= $kRecursos ?>' style='float:none;' value='1' checked/>
                                        <?php
                                    }else{
                                        ?>
                                        <input type='checkbox' name='data[det][<?= $kRecursos ?>][permitir]' id='det<?= $kRecursos ?>' style='float:none;' value='1' />
                                        <?php
                                    }
                                    unset($detrecursos[$kRecursos]);
                                }
                            }
                            ?>
                            </td>
                            </tr>
                            <?php
                        }
                        foreach ($detrecursos as $kRecursos => $vRecursos) {
                        ?>
                        <tr>
                            <td><?= $vRecursos['Detrecurso']['nombre'] ?></td>
                            <td><?= $vRecursos['Detrecurso']['funcion'] ?></td>
                            <td style="text-align: center;">
                                <input type='hidden' name='data[det][<?= $kRecursos ?>][detrecurso_id]' value='<?= $vRecursos['Detrecurso']['id'] ?>' />
                                <input type='hidden' name='data[det][<?= $kRecursos ?>][permitir]' id='det<?= $kRecursos ?>_' value='0' />
                                <input type='checkbox' name='data[det][<?= $kRecursos ?>][permitir]' id='det<?= $kRecursos ?>' style='float:none;' value='1' />
                                <?php
                                }
                                ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Privilegios'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

    </div>

</div>
