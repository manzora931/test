<style>
    .rec{
        font-weight: bold;
        font-size: 13px;
        border: 1px solid;
        background-color: #787878;
    }

    .menu{
        background-color: #B8B8B8;
        border: 1px solid;
    }

    .alinear{
        text-align: center;
        margin: auto;
        _position: relative;
    }
</style>
<?php
if($bandera==1){    ?>
    <table cellpadding="0" cellspacing="0" style='width: 100%;'>
        <tr>
            <th><?php echo h('Recurso'); ?></th>
            <th><?php echo h('Activo'); ?></th>
            <th><?php echo h('Lectura'); ?></th>
            <th><?php echo h('Adicionar'); ?></th>
            <th><?php echo h('Editar'); ?></th>
            <th><?php echo h('Borrar'); ?></th>
            <th class="actions"><?php echo __('Acciones'); ?></th>
        </tr>
        <?php
        $c=0;
        for($i=0;$i<4;$i++) {
            $class = null;
            if ($c++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            echo "<tr".$class."><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
        }
        ?>
    </table>
    <?php
}else{
    echo $this->Form->hidden('id_group',array('label'=>false,'id'=>'id_group','value'=>$id));   ?>
    <div id="tit"><h2 style="font-size: 18px; ">Privilegios del perfil: <span style="color:#484848;"><?php echo (isset($recs[0]['g']['perfil']))?$recs[0]['g']['perfil']:"";?></span></h2></div>
    <table cellpadding="0" cellspacing="0" style='width: 100%;'>
        <tr style="height: 25px; vertical-align: text-top">
            <th><?php echo h('Recurso'); ?></th>
            <th><?php echo h('Activo'); ?></th>
            <th><?php echo h('Lectura'); ?></th>
            <th><?php echo h('Adicionar'); ?></th>
            <th><?php echo h('Editar'); ?></th>
            <th><?php echo h('Borrar'); ?></th>
            <th class="accion"><?php echo __('Acciones'); ?></th>
        </tr>
        <?php
        $i = 0;
        $cont=0;
        $c=1;
        $anterior='';
        $all = count($recs);
        $m=0;
        $a=0;
        $in=0;
        foreach ($recs as $recursos){
            $class = null;
            if ($i++ % 2 != 0) {
                $class = ' class="altrow"';
            }
            $cont++;


            if($anterior != $recursos['m']['modulo']){
                echo "<tr><td colspan='7' width='10%'><label class='rec'>".$recursos['m']['modulo']."</label></td></tr>";
                $m=0;
                $a=0;
                $in=0;
            }

            ////////////////Pantallas que estan ubicados en el menú/////////////////////////
            if(($recursos['r']['ubicacion']=='Menu' || $recursos['r']['ubicacion']=='menu') && $recursos['r']['tipo']=='pantalla'){
                if($m==0){
                    echo "<tr><td colspan='7'><label class='menu'>Menú</label></td></tr>";
                }

                echo"<tr ".$class." >";
                echo "<td style='display: none'>";echo $this->Form->hidde('id',array('label'=>false,'id'=>'id'.$cont,'value'=>$recursos['r']['id']));
                echo $this->Form->hidden("id_pri",array('id'=>"id_pri".$cont,"value"=>$recursos['p']['id']));
                echo"</td>";
                echo "<td>".$recursos['r']['nombre']."</td>";

                if($recursos['p']['activo']==1){
                    echo"<td class='alinear'><span id='act".$cont."'>SI</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='act".$cont."'>NO</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['leer']==1){
                    echo"<td align='center'><span id='read".$cont."'>SI</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='read".$cont."'>NO</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['adicionar']==1){
                    echo"<td align='center'><span id='add".$cont."'>SI</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='add".$cont."'>NO</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['editar']==1){
                    echo"<td align='center'><span id='edit".$cont."'>SI</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='edit".$cont."'>NO</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['borrar']==1){
                    echo"<td align='center'><span id='delete".$cont."'>SI</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='delete".$cont."'>NO</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }
                ?>
                <td class="actions">
                    <div id="1<?php echo $cont;?>">
                        <?php echo "<span class='check' style='cursor: pointer' onclick='edit($cont)' id='edit$cont' ></span>"; ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'view', $recursos['p']['id']),array('class'=>'ver')); ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'edit', $recursos['p']['id']),array('class'=>'editar')); ?>
                    </div>
                    <div id="2<?php echo $cont;?>" style="display: none; margin-top: 2px; margin-left: 20px">
                        <?php echo "<span class = 'add2' style='cursor: pointer' id='add.$cont' onclick='guardar($cont)'></span>";?>
                        <?php echo "<span class='cancelar' style='cursor: pointer' onclick='cancelar($cont)' id='cancelar$cont'></span>"; ?>
                    </div>
                </td>

                <?php
                echo"   </tr>";
                $m++;

            }

            ////////////////Pantallas que estan ubicados en el admin/////////////////////////
            if(($recursos['r']['ubicacion']=='Admin' || $recursos['r']['ubicacion']=='admin') && $recursos['r']['tipo']=='pantalla'){
                if($a==0){
                    echo "<tr><td colspan='7'><label class='menu'>Admin</label></td></tr>";
                }
                echo"<tr". $class.">";
                echo "<td style='display: none'>";echo $this->Form->hidde('id',array('label'=>false,'id'=>'id'.$cont,'value'=>$recursos['r']['id']));
                echo $this->Form->hidden("id_pri",array('id'=>"id_pri".$cont,"value"=>$recursos['p']['id']));
                echo"</td>";
                echo "<td>".$recursos['r']['nombre']."</td>";

                if($recursos['p']['activo']==1){
                    echo"<td class='alinear'><span id='act".$cont."'>SI</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='act".$cont."'>NO</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['leer']==1){
                    echo"<td align='center'><span id='read".$cont."'>SI</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='read".$cont."'>NO</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['adicionar']==1){
                    echo"<td align='center'><span id='add".$cont."'>SI</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='add".$cont."'>NO</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['editar']==1){
                    echo"<td align='center'><span id='edit".$cont."'>SI</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='edit".$cont."'>NO</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['borrar']==1){
                    echo"<td align='center'><span id='delete".$cont."'>SI</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='delete".$cont."'>NO</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }
                ?>
                <td class="actions">
                    <div id="1<?php echo $cont;?>">
                        <?php echo "<span class='check' style='cursor: pointer' onclick='edit($cont)' id='edit$cont' ></span>"; ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'view', $recursos['p']['id']),array('class'=>'ver')); ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'edit', $recursos['p']['id']),array('class'=>'editar')); ?>
                    </div>
                    <div id="2<?php echo $cont;?>" style="display: none; margin-top: 2px; margin-left: 20px">
                        <?php echo "<span class = 'add2' style='cursor: pointer' id='add.$cont' onclick='guardar($cont)'></span>";?>
                        <?php echo "<span class='cancelar' style='cursor: pointer' onclick='cancelar($cont)' id='cancelar$cont'></span>"; ?>
                    </div>
                </td>

                <?php
                echo"   </tr>";
                $a++;
            }

            ////////////////Pantallas que estan ubicados en el menú y sean de tipo informes/////////////////////////
            if(($recursos['r']['ubicacion']=='Menu' || $recursos['r']['ubicacion']=='menu') && $recursos['r']['tipo']=='informe'){
                if($in==0){
                    echo "<tr><td colspan='7'><label class='menu'>Informers</label></td></tr>";
                }
                echo "<tr".$class.">";
                echo "<td style='display: none'>";echo $this->Form->hidde('id',array('label'=>false,'id'=>'id'.$cont,'value'=>$recursos['r']['id']));
                echo $this->Form->hidden("id_pri",array('id'=>"id_pri".$cont,"value"=>$recursos['p']['id']));
                echo"</td>";
                echo "<td>".$recursos['r']['nombre']."</td>";

                if($recursos['p']['activo']==1){
                    echo"<td class='alinear'><span id='act".$cont."'>SI</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='act".$cont."'>NO</span>";
                    echo $this->Form->hidden('activo',array('type'=>'check','label'=>false,'id'=>'activo'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['leer']==1){
                    echo"<td align='center'><span id='read".$cont."'>SI</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='read".$cont."'>NO</span>";
                    echo $this->Form->hidden('leer',array('type'=>'check','label'=>false,'id'=>'leer'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['adicionar']==1){
                    echo"<td align='center'><span id='add".$cont."'>SI</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='add".$cont."'>NO</span>";
                    echo $this->Form->hidden('adicionar',array('type'=>'check','label'=>false,'id'=>'adicionar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['editar']==1){
                    echo"<td align='center'><span id='edit".$cont."'>SI</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='edit".$cont."'>NO</span>";
                    echo $this->Form->hidden('editar',array('type'=>'check','label'=>false,'id'=>'editar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                if($recursos['p']['borrar']==1){
                    echo"<td align='center'><span id='delete".$cont."'>SI</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'checked', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }else{
                    echo "<td align='center'><span id='delete".$cont."'>NO</span>";
                    echo $this->Form->hidden('borrar',array('type'=>'check','label'=>false,'id'=>'borrar'.$cont, 'checked'=>'', 'style'=>'margin-left:90px; margin-top:3px'));
                    echo "</td>";
                }

                ?>
                <td class="actions">
                    <div id="1<?php echo $cont;?>" >
                        <?php echo "<span class='check' style='cursor: pointer' onclick='edit($cont)' id='edit$cont' ></span>"; ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'view', $recursos['p']['id']),array('class'=>'ver')); ?>
                        <?php echo $this->Html->link(__(' '), array('action' => 'edit', $recursos['p']['id']),array('class'=>'editar')); ?>

                    </div>
                    <div id="2<?php echo $cont;?>" style="display: none; margin-top: 2px; margin-left: 20px">
                        <?php echo "<span class = 'add2' style='cursor: pointer' id='add.$cont' onclick='guardar($cont)'></span>";?>
                        <?php echo "<span class='cancelar' style='cursor: pointer' onclick='cancelar($cont)' id='cancelar$cont'></span>"; ?>
                    </div>
                </td>

                <?php
                echo"   </tr>";
                $in++;
            }

            ?>

            <?php
            $anterior = $recursos['m']['modulo'];
            $c++;
        }

        ?>
    </table>
    <?php
}
?>



<script>
    window.onload = function startrefresh(){
        refreshDivs('tit');
    }

    $(function(){
        $(window).load("ajax/test.html", function(){

            var n=document.getElementById("tab2").rows.length;
            n=n-1;
            $("#num2").text(n);
            $("#num3").text(n);
        });
    });
</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true')); ?>