<style>
    .radio label, .checkbox label{
        padding-left: 14px;
    }
    .radio input[type="radio"],
    .radio-inline input[type="radio"],
    .checkbox input[type="checkbox"],
    .checkbox-inline input[type="checkbox"] {
        position: relative;
        margin-top: 4px \9;
        margin-left: 0px;
    }
    input[type="checkbox"] label{
        margin-left: 5px;
    }
</style>
<div class="privilegios form">
    <?php echo $this->Form->create('Privilegio'); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Privilegio'); ?></legend>
        <?php
        echo $this->Form->input('perfile_id', array('label'=>'Perfil'));
        echo $this->Form->input('modulo_id', array('empty'=>'< Seleccionar >'));
        echo $this->Form->input('recurso_id', array('empty'=>'< Seleccionar >'));

        echo $this->Form->input('activo');

        echo $this->Form->input('leer');

        echo $this->Form->input('adicionar');

        echo $this->Form->input('editar');

        echo $this->Form->input('borrar');

        echo $this->Form->hidden('usuario');

        echo $this->Form->hidden('usuariomodif');
        ?>
        <div id="botones" style="width: 0px; height: 0px; margin-bottom: 1em;">
        </div>
    </fieldset>
    <?php echo $this->Form->end(__('Almacenar')); ?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Listar Privilegios'), array('action' => 'index')); ?></li>
    </ul>
</div>
<script>
    $("#PrivilegioModuloId").change(function(){
        var idMod=$(this).val();
        $.ajax({
            url: "recursos",
            method: "POST",
            data: { idMod : idMod },
            dataType: "html"
        }).done(function( result ) {
            $("#PrivilegioRecursoId").html(result);
        }).fail(function( result ) {
            console.log(result);
        });
    });
    $("#PrivilegioRecursoId").change(function(){
        var idReq=$(this).val();
        $.ajax({
            url: "detRecursos",
            method: "POST",
            data: { idReq : idReq },
            dataType: "html"
        }).done(function( result ) {
            if (result != "N") {
                $("#botones").html(result);
                $("#botones").animate({
                    width: "100%",height: "100%"
                }, 1000, "swing");
            }else {
                $("#botones").animate({
                    width: "0px",height: "0px"
                }, 1000, "swing");
                $("#botones").html("");
            }
        }).fail(function( result ) {
            console.log(result);
        });
    });
</script>