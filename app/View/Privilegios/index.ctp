<?= $this->Html->css('privilegios') ?>
<?= $this->Html->script('utilities') ?>
<?= $this->Html->script('chosen.jquery.min'); ?>
<script type="text/javascript">
	$( function () {
		$(".special-select").chosen({disable_search_threshold: 5});
	});
</script>
<style>
	div.index{

		width: 100%;
	}
	h2{
		margin-top: 15px;
		font-size: 1.4em;
		color:#cb071a;
	}
	.privilegios #perfiles-container{
		margin-bottom: 55px;
		min-height: 350px;
	}
	.privilegios #perfiles-container .perfiles .active{
		background-color: #d8e6f5;
	}
	.privilegios #privilegios #modulos tbody .modulo td {
		background-color: #cb071a;
		color:white;
		text-align: left;
	}

	.table tbody tr td {
		vertical-align:inherit;
	}
	input[type=checkbox] {


		margin: 5px 0 0 25px;
	}
	.create-box table thead th:first-child {
		border-top-left-radius: 5px;
		box-shadow: 0 -1px 0 0 #cccccc, -1px 1px 0 0 #cccccc;
	}
	.create-box table thead th:last-child {
		border-top-right-radius: 5px;
		box-shadow: 0 -1px 0 0 #cccccc, 1px 1px 0 0 #cccccc;
	}
	.create-box table {
		display: block;
		margin-bottom: 15px;
		padding: 8px;
		border: solid 1px #999999;
	}
	.create-box table tbody{
		border:#cccccc ;
	}
	.create-box table tbody tr:last-child td:last-child {
		border-bottom-right-radius: 5px;
		border-bottom: none;
		box-shadow: 1px 1px 0 0 #cccccc;
	}
	.create-box table tbody tr:last-child td:first-child {
		border-bottom-left-radius: 5px;
		border-bottom: none;
		box-shadow: -1px 1px 0 0 #cccccc;

	}

	.create-box table tbody tr td {
		padding: 5px 3px;
		vertical-align: middle;
		border-bottom: solid 1px #cccccc;
		border-top: solid 1px #cccccc;
		border-left: 1px solid #cccccc;
	}
	.create-box table thead th:first-child {
		border-left: none;
	}
	.privilegios #addPrivilegio table td.select {
		border-left: none;
	}
	.privilegios #privilegios #modulos tbody .recurso .actions .actions-container .icon{
		float: left;
		margin-top: 10px;
		margin-right: 5px;
		font-size: 0.8em;
		color: #819FF7;
		cursor: pointer;
	}
	.privilegios #addPrivilegio table td.select{
		width: auto;
	}
	select:required:valid, select:required:invalid {
		border-radius: 5px;
	}
	.table thead {
		background-color: #011884;
	}
	input[type=checkbox] {
		margin: 5px 0 0 31px;
	}
	.table > thead > tr > th {
		border-left: 1px solid #cccccc;
		border-left: 1px solid #cccccc;
		line-height: 0.9em;
	}
	h4 {
		font-size: 1em;
		line-height: 0.9em;
	}
</style>
<div class="privilegios index container">
	<h2>Privilegios por perfil</h2>
	<div class="alert" id="alerta" style="display: none;">
		<span class="icon"></span>
		<span class="message"></span>
	</div>
	<?php /* $this->Element('search', [
		'table' => 'privilegios',
		'controller' => 'Privilegio',
		'options' => [
			1 => 'Perfil',
		],
		'placeholder' => 'Perfil',
		'buttons' => [
			'add' => true,
			'edit' => false,
			'view' => false,
			'clone' => false
		]
	]) */ ?>
	<button type="button" id="vertodos" class="btn btn-primary hide" name="button">Ver todos</button>
<div>
	<div id="perfiles-container" class="pull-left col-xs-2">
		<div class="perfiles-heading">
			<h4>Perfiles</h4>
		</div>
		<div class="perfiles">
			<?php  foreach ($perfiles as $perfil) { ?>
				<div id="perfil-<?= $perfil['Perfile']['id'] ?>" class="perfil" data-id="<?= $perfil['Perfile']['id'] ?>">
					<span><?= $perfil['Perfile']['perfil'] ?></span>
				</div>
			<?php } ?>
		</div>
	</div>
	<div id="addPrivilegio" class=" pull-left col-xs-10 create-box clearfix hide">
		<div class="create-box-fields">
			<?=$this->Form->create("Privilegio", [
				'url' => [
					'controller' => 'privilegios',
					'action' => 'add'
				]
			]) ?>
			<table class="table">
				<thead>
				<tr>
				<th colspan="2" width="317" class="text-left">Crear Privilegio</th>
				<th width="70px">Activo</th>
				<th width="100px">Lectura</th>
				<th width="100px">Adicionar</th>
				<th width="100px">Modificar</th>
				<th width="100px">Borrar</th>
				<th width="130px">Mis registros</th>
				<th width="133px" colspan="2"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td class="select">
						<?= $this->Form->input("modulos", [
							"label" => false,
							"class" => "form-control",
							"empty" => [
								"" => "Seleccionar"
							],
							"required"
						]); ?>
					</td>
					<td class="select">
						<?= $this->Form->input("recursos_id", [
							"label" => false,
							"class" => "form-control ",
							"empty" => [
								"" => "Seleccionar"
							],
							"required"
						]); ?>
					</td>

					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("activo", [
								"label" => false,
								"type" => "checkbox",
								"div" => false,
								'style' =>'margin: 5px 0 0 15px;',
								"checked"
							]) ?>
						</label>
					</td>
					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("lectura", [
								"label" => false,
								"type" => "checkbox",
								"div" => false,
								"checked"
							]) ?>
						</label>
					</td>
					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("adicionar", [
								"label" => false,
								"type" => "checkbox",
								"div" => false,
								"checked"
							]) ?>
						</label>
					</td>
					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("editar", [
								"label" => false,
								"type" => "checkbox",
								"div" => false,
								"checked"
							]) ?>
						</label>
					</td>
					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("borrar", [
								"label" => false,
								"type" => "checkbox",
								"div" => false
							]) ?>
						</label>
					</td>
					<td class="text-center">
						<label for="InstitucionActivo">
							<?= $this->Form->input("solopropietario", [
								"label" => false,
								"type" => "checkbox",
								"div" => false,
								'style' =>'margin: 5px 0 0 45px; display: none;',
								'name'=>'data[Privilegio][solopropietario]'
							]) ?>
						</label>
					</td>
					<td class="create-box-store">
						<button type="submit" class="add2 btn btn-primary btn-sm" name="button">
							<i class="icon icon-save"></i>
							Almacenar
						</button>
					</td>
				</tbody>
			</table>
			<?= $this->Form->end() ?>
		</div>
	</div>
	<?php //tabla privilegios ?>
	<div id="privilegios" class="pull-right col-xs-10 clearfix">
		<?php //Div donde se carga tabla de privilegios ?>
		<div class="loading-overlay hide"></div>
		<div class="load">
			<h3 class="text-center">Seleccione Un Perfil</h3>
		</div>
	</div>
</div>
</div>
<script>
	loadPrivileges("<?= Router::url(['controller' => 'privilegios', 'action' => 'privilegios']) ?>", $(".perfil").first().data("id"));

	$(".search_create .btn").click( function (e) {
		e.preventDefault();
		var canOpen = $(".perfil").hasClass('active');

		if (canOpen) {
			$("#addPrivilegio").toggleClass('hide');
		} else {
			alertMessage("Primero debe seleccionar un perfil", "alert-danger", "icon-cross-circled");
		}
	});

	$(".cancelCreate").click( function () {
		$(this).closest(".create-box").addClass('hide');
		if ($(this).data("open")) {
			$($(this).data("open")).removeClass('hide');
		}
	});

	$(document).on("click", ".perfil", function () {
		loadPrivileges("<?= Router::url(['controller' => 'privilegios', 'action' => 'privilegios']) ?>", $(this).data("id"));
		var id =$(this).data("id");
		//console.log(id);
		$("#PrivilegioModulos").val("");
		$("#PrivilegioModulos").trigger("chosen:updated");
		$("#PrivilegioRecursosId").html('<option value="">Seleccionar Herramienta</option>');
		$("#PrivilegioRecursosId").trigger("chosen:updated");
	});

	$(document).on("change", "#PrivilegioRecursosId", function () {
		var id = $(this).val();
		var url = "<?= Router::url(['action' => 'getRecurso']) ?>";
		console.log(id);
		$.ajax({
			url: url,
			type: 'post',
			data: {
				id: id
			},
			cache: false,
			dataType: 'json',
			success: function (data) {
				if(data[0].recursos.modelo == 'denuncias') {
					$("#PrivilegioSolopropietario").css("display", "block");
				} else {
					$("#PrivilegioSolopropietario").css("display", "none");
				}
			}
		});
	});

	$("#PrivilegioModulos").change(function(){
		var idMod = $(this).val();
		var idPerfil = $(".perfil.active").data("id");
		var url = "<?= Router::url(['action' => 'recursos']) ?>";
		$("#PrivilegioSolopropietario").css("display", "none");

		$.ajax({
			url,
			method: "POST",
			data: { idMod, idPerfil },
			dataType: "html",
			success: function(recursos) {
				//console.log(recursos);
				var html = "<option value=''> Seleccionar Herramienta </option>";

				$.each(JSON.parse(recursos), function(id, el) {
					html += "<option value=" + id + ">";
					html += el;
					html += "</option>";
				});

				$("#PrivilegioRecursosId").html(html);
				$("#PrivilegioRecursosId").trigger("chosen:updated");
			}
		});
	});

	$("#PrivilegioIndexForm").submit( function (e) {
		e.preventDefault();
		var Privilegio = {
			perfile_id : $(".perfil.active").data("id"),
			modulo_id : $("#PrivilegioModulos").val(),
			recurso_id : $("#PrivilegioRecursosId").val(),
			activo : $("#PrivilegioActivo").is(":checked") ? 1 : 0,
			leer : $("#PrivilegioLectura").is(":checked") ? 1 : 0,
			adicionar : $("#PrivilegioAdicionar").is(":checked") ? 1 : 0,
			editar : $("#PrivilegioEditar").is(":checked") ? 1 : 0,
			borrar : $("#PrivilegioBorrar").is(":checked") ? 1 : 0,
			solopropietario : $("#PrivilegioSolopropietario").is(":checked") ? 1 : 0
		};

		$.ajax({
			url: "<?= Router::url(['action' => 'adicionar']) ?>",
			type: "POST",
			data: {Privilegio},
			success: function () {
				loadPrivileges("<?= Router::url(['controller' => 'privilegios', 'action' => 'privilegios']) ?>", Privilegio.perfile_id, Privilegio.modulo_id);

				alertMessage("Privilegio Almacenado","alert-success","icon-check-circled");
				$("#addPrivilegio").addClass("hide");
				$("#PrivilegioModulos").val("");
				$("#PrivilegioModulos").trigger("chosen:updated");
				$("#PrivilegioRecursosId").html('<option value="">Seleccionar Herramienta</option>');
				$("#PrivilegioRecursosId").trigger("chosen:updated");
			}
		});
	});

	$("#privilegiosIndexForm").submit( function (e) {
		e.preventDefault();
		var searchBy = $("#search-by").val();
		var searchText = $("#privilegiosSearchText").val();
		var active = $('#active').is(':checked')?1:0;

		doSearch(searchBy, searchText, active);

		$("#vertodos").removeClass("hide");
	});

	$("#vertodos").click( function () {
		$("#search-by").val('');
		$("#privilegiosSearchText").val('');
		$("#active").prop('checked',false);

		$(this).addClass("hide");
		doSearch('', '', '');
	});

	function doSearch (searchBy, searchText, active) {

		$.ajax({
			url: "<?= Router::url(['controller' => 'privilegios', 'action' => 'searchPerfiles']) ?>",
			type: "POST",
			data: {searchBy, searchText, active},
			success: function (perfiles) {
				html = "";

				$.each(JSON.parse(perfiles), function(index, el) {
					html += "<div id='perfil-" + index + "' class='perfil' data-id='" + index +"'>";
					html += "<span>" + el + "</span>";
					html += "</div>";

				});

				$("#perfiles-container .perfiles").html(html);
				var valores = JSON.parse(perfiles);
				loadPrivileges("<?= Router::url(['controller' => 'privilegios', 'action' => 'privilegios']) ?>",Object.keys(valores),2);
			}
		});
	}
</script>