<?php $this->layout = 'ajax'; ?>
<style>
    .privilegios #privilegios #modulos tbody .recurso .actions .actions-container .row-actions, .privilegios #privilegios #modulos tbody .recurso .actions .actions-container .edit-actions {
        height: 22px;
        margin-top: 7px;
    }
    .privilegios #privilegios #modulos tbody .recurso .actions .actions-container .icon {
        float: left;
        margin-top: 8px;
        margin-right: 5px;
        margin-bottom: 5px;
        font-size: 0.8em;
        color: #819FF7;
        cursor: pointer;
    }

</style>
<table id="modulos" class="table">
    <thead>
    <tr>
        <th  width="34%" class="text-left ">Herramienta</th>
        <th class="9%">Activo</th>
        <th class="view">Lectura</th>
        <th width="10%  ">Adicionar</th>
        <th class="edit">Modificar</th>
        <th class="borrar">Borrar</th>
        <!--<th  width="13%" class="mis-registros">Mis Registros</th>-->
        <th class="">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php //Verificar si posee privilegios ?>
    <?php
    if(isset($privilegios)){
    if ($privilegios) { ?>
        <?php foreach ($modulos as $moduloId => $modulo) { ?>
            <?php if (isset($modulo['Menu']['Modulo']) || isset($modulo['Admin']['Modulo'])) { ?>
                <tr class="modulo" data-change=".for-modulo-<?= $moduloId ?>">
                    <td><?= isset($modulo['Menu']['Modulo']['modulo']) ? $modulo['Menu']['Modulo']['modulo'] : $modulo['Admin']['Modulo']['modulo'] ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <!--<td></td>-->
                    <td></td>
                </tr>
                <?php //Privilegios ?>
                <?php foreach ($ubicaciones as $ubicacion => $titulo) { ?>
                    <?php if (isset($modulo[$ubicacion])) { ?>
                        <?php //RECURSOS/PRIVILEGIOS ?>
                        <tr class="expandible ubicacion for-modulo-<?= $moduloId ?> hide">
                            <td><?= $titulo ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <!--<td></td>-->
                            <td></td>
                            <td></td>
                        </tr>
                        <?php  foreach ($privilegios as $privilegio) {
                            ?>
                            <?php if ($privilegio['Recurso']['modulo_id'] == $moduloId && $privilegio['Recurso']['ubicacion'] == $ubicacion) { ?>
                                <?php //RECURSO ?>
                                <tr id="recurso-<?= $privilegio['Recurso']['id'] ?>" class="expandible recurso for-modulo-<?= $moduloId ?> hide">
                                    <td id="edit-detrecursos-<?= $privilegio['Recurso']['id'] ?>" data-id="<?= $privilegio['Recurso']['id'] ?>" class="edit-detrecursos"><?= $privilegio['Recurso']['nombre'] ?></td>
                                    <td class="text-center">
                                        <input id="edit-info-activo-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" name="" <?= $privilegio['Privilegio']['activo'] ? "checked" : "" ?>>
                                        <label for="edit-info-activo-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                    </td>
                                    <td class="text-center">
                                        <input id="edit-info-view-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" name="" <?= $privilegio['Privilegio']['leer'] ? "checked" : "" ?>>
                                        <label for="edit-info-view-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                    </td>
                                    <td class="text-center">
                                        <input id="edit-info-add-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" name="" <?= $privilegio['Privilegio']['adicionar'] ? "checked" : "" ?>>
                                        <label for="edit-info-add-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                    </td>
                                    <td class="text-center">
                                        <input id="edit-info-edit-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" name="" <?= $privilegio['Privilegio']['editar'] ? "checked" : "" ?>>
                                        <label for="edit-info-edit-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                    </td>
                                    <td class="text-center">
                                        <input id="edit-info-delete-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" <?= $privilegio['Privilegio']['borrar'] ? "checked" : "" ?>>
                                        <label for="edit-info-delete-<?= $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                    </td>
                                    <!-- Mis Registros -->
                                    <!--<td class="text-center">
                                        <?php // if($privilegio['Recurso']['modelo'] == 'denuncias'){ ?>
                                            <input id="edit-info-registro-<?php // echo $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" <?= $privilegio['Privilegio']['solopropietario'] ? "checked" : "" ?>>
                                            <label for="edit-info-registro-<?php // echo $privilegio['Privilegio']['id'] ?>" class="edit-info"></label>
                                        <?php // } else { ?>
                                            <input id="edit-info-registro-<?php // echo $privilegio['Privilegio']['id'] ?>" class="edit-info edit-info-<?= $privilegio['Privilegio']['id'] ?>" disabled type="checkbox" style="display: none;">
                                            <label for="edit-info-registro-<?php // echo $privilegio['Privilegio']['id'] ?>" class="edit-info" style="display: none;"></label>
                                        <?php // } ?>
                                    </td>-->
                                    <td class="accion">
                                        <div class="actions-container">
                                            <div class="row-actions for-recurso-<?= $privilegio['Recurso']['id'] ?> for-privilege-<?= $privilegio['Privilegio']['id'] ?>">
                                                <span id="edit-privileges-<?= $privilegio['Privilegio']['id'] ?>" class="icon icon-check-square edit-privileges check " data-id="<?= $privilegio['Privilegio']['id'] ?>"></span>
                                            </div>

                                            <div id="edit-privileges-actions-<?= $privilegio['Privilegio']['id'] ?>" class="edit-actions hide">
                                                <span id="update-privileges-<?= $privilegio['Privilegio']['id'] ?>" class="icon  add icon-save update-privileges" data-id="<?= $privilegio['Privilegio']['id'] ?>"></span>
                                                <span id="cancel-edit-privileges-<?= $privilegio['Privilegio']['id'] ?>" class="icon cancelar icon-cross-circled cancel-edit-privileges" data-id="<?= $privilegio['Privilegio']['id'] ?>"></span>
                                            </div>

                                            <div id="edit-detrecursos-actions-<?= $privilegio['Recurso']['id'] ?>" class="edit-actions hide">
                                                <span id="update-detrecursos-<?= $privilegio['Recurso']['id'] ?>" class="icon icon-save update-detrecursos" data-id="<?= $privilegio['Recurso']['id'] ?>"></span>
                                                <span id="cancel-edit-detrecursos-<?= $privilegio['Recurso']['id'] ?>" class="icon icon-cross-circled cancel-edit-detrecursos" data-id="<?= $privilegio['Recurso']['id'] ?>"></span>
                                            </div>

                                            <?= $this->Html->link('',
                                                ['action' => 'view', $privilegio['Privilegio']['id']],
                                                ['style'=>'text-decoration:none;','class' => 'icon busqueda edit-buttons']
                                            ) ?>

                                            <?= $this->Html->link('',
                                                ['action' => 'edit', $privilegio['Privilegio']['id']],
                                                ['class' => 'icon editar edit-buttons']
                                            ) ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php //DETRECURSOS ?>
                                <?php if ($detRecursos[$privilegio['Recurso']['id']]) { ?>
                                    <tr class="detrecursos for-recurso-<?= $privilegio['Recurso']['id'] ?> hide">
                                        <td colspan="7">
                                            <table class="table table-condensed table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="privilegio">Privilegio</th>
                                                    <th class="permitir">Permitir</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($detRecursos[$privilegio['Recurso']['id']] as $detRecurso) { $idDetRecurso = $detRecurso['Detrecurso']['id']; ?>
                                                    <tr>
                                                        <td class="text-center"><?= $detRecurso['Detrecurso']['nombre'] ?></td>
                                                        <td class="text-center">
                                                            <?php $checked = (isset($detPrivilegios[$idDetRecurso]['Detprivilegio']['permitir']) && $detPrivilegios[$idDetRecurso]['Detprivilegio']['permitir'] ) ? 'checked' : '';
                                                            $detPrivilegioId = isset($detPrivilegios[$idDetRecurso]['Detprivilegio']['id']) ? $detPrivilegios[$idDetRecurso]['Detprivilegio']['id'] : 0 ?>
                                                            <input class="edit-detrecursos-<?= $privilegio['Recurso']['id'] ?>" <?= $checked ?> data-detrecursoid="<?= $detRecurso['Detrecurso']['id'] ?>" data-privilegioid="<?= $privilegio['Privilegio']['id'] ?>" data-id="<?= $detPrivilegioId ?>" type="checkbox">
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <?php //Si no posee privilegios ?>
    <?php } else { ?>
        <tr>
            <td colspan="8" class="text-muted text-center">El perfil no posee privilegios</td>
        </tr>
    <?php } }else{

        echo ' <tr>
                    <td colspan="8" class="text-muted text-center">La herramienta buscada no existe</td>
               </tr>';
    }?>
    </tbody>
</table>
<script type="text/javascript">
    $(".modulo").click( function () {
        $(".expandible").addClass('hide');
        $($(this).data('change')).toggleClass('hide');
    });

    $(".edit-privileges").click( function () {
        if ($(".row-actions.hide").length === 0) {
            var id = $(this).data("id");

            $(".row-actions.for-privilege-" + id).addClass('hide');
            $("#edit-privileges-actions-" + id).removeClass('hide');
            $(".edit-info-" + id).prop('disabled', false);
        }
    });

    $(".update-privileges").click( function () {
        var id = $(this).data("id");
       // console.log(id);
        var activo = $("#edit-info-activo-" + id).is(':checked') ? 1 : 0;
        var leer = $("#edit-info-view-" + id).is(':checked') ? 1 : 0;
        var adicionar = $("#edit-info-add-" + id).is(':checked') ? 1 : 0;
        var editar = $("#edit-info-edit-" + id).is(':checked') ? 1 : 0;
        var borrar = $("#edit-info-delete-" + id).is(':checked') ? 1 : 0;
       // var propietario = $("#edit-info-propietario-" + id).is(':checked') ? 1 : 0;
        var registroRecurso = $("#edit-info-registro-" + id);

        $.ajax({
            url: "<?= Router::url(['action' => 'update']) ?>",
            type: 'POST',
            data: {id: id, activo: activo, leer: leer, adicionar: adicionar, editar: editar, borrar: borrar, registro: registroRecurso.data('registro'), solopropietario: registroRecurso.is(':checked') ? 1 : 0, privilegioregistro_id: registroRecurso.data('id')},
            success: function (data) {
                alertMessage("Privilegio Actualizado", "alert-success", "icon-check-circled");
                $("#edit-privileges-actions-" + id).addClass('hide');
                $(".row-actions.for-privilege-" + id).removeClass('hide');
                $(".edit-info-" + id).prop('disabled', true);
                registroRecurso.data('id', data);
            }
        });
    });

    $(".cancel-edit-privileges").click( function () {
        var id = $(this).data("id");

        $("#edit-privileges-actions-" + id).addClass('hide');
        $(".row-actions.for-privilege-" + id).removeClass('hide');
        $(".edit-info-" + id).prop('disabled', true);
    });

    $(".edit-detrecursos").click( function () {
        var idRecurso = $(this).data("id");

        if ($(".row-actions.hide").length === 0) {

            if ($(".detrecursos.for-recurso-" + idRecurso).length) {
                $("#recurso-" + idRecurso).addClass("active");
                $(".row-actions.for-recurso-" + idRecurso).addClass("hide");
                $("#edit-detrecursos-actions-" + idRecurso).removeClass("hide");
                $(".detrecursos.for-recurso-" + idRecurso).removeClass("hide");
            } else {
                alertMessage("No contiene detalle", "alert-danger", "icon-cross-circled");
            }
        }
    });

    $(".update-detrecursos").click( function() {
        var idRecurso = $(this).data("id");
        var detRecursos = $(".edit-detrecursos-" + idRecurso);
        var btn = $(this);

        $.each(detRecursos, function(index, el) {
            var idDetPrivilegio = $(detRecursos[index]).data("id");
            var idDetRecurso = $(detRecursos[index]).data("detrecursoid");
            var idPrivilegio = $(detRecursos[index]).data("privilegioid");
            var permitir = $(detRecursos[index]).is(':checked') ? 1 : 0;

            $.ajax({
                url: "<?= Router::url(['controller' => 'privilegios', 'action' => 'updateDetPrivilegio']) ?>",
                type: 'POST',
                data: {idDetPrivilegio: idDetPrivilegio, permitir: permitir, idDetRecurso: idDetRecurso, idPrivilegio: idPrivilegio},
                success: function (data) {
                    data = JSON.parse(data);

                    $(detRecursos[index]).data("id", data.id);

                    if (data.before.Detprivilegio.permitir != data.after.Detprivilegio.permitir) {
                        alertMessage("Detalle Almacenado", "alert-success", "icon-check-circled");
                    }
                }
            });
        });

        $("#recurso-" + idRecurso).removeClass("active");
        $(".row-actions.for-recurso-" + idRecurso).removeClass("hide");
        $("#edit-detrecursos-actions-" + idRecurso).addClass("hide");
        $(".detrecursos.for-recurso-" + idRecurso).addClass("hide");
    });

    $(".cancel-edit-detrecursos").click( function () {
        var idRecurso = $(this).data("id");

        $("#recurso-" + idRecurso).removeClass("active");
        $(".row-actions.for-recurso-" + idRecurso).removeClass("hide");
        $("#edit-detrecursos-actions-" + idRecurso).addClass("hide");
        $(".detrecursos.for-recurso-" + idRecurso).addClass("hide");
    });
</script>