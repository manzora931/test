<style>
	.tbl tr td{
		border: none;
	}
	.tbl{
		border: none;
		margin-left: 5px;
	}
	.td1{
		font-weight: bold;
		text-align: left;
		padding: 4px 0 4px 0;
	}
	.close{
		margin-top: -5px;
	}
	div.view{
		width: 100%;
	}

	.btn-default{
		font-family: 'Calibri', sans-serif;
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border: 1px solid #606060	;
		font-size: 14px;
		color: #000;
		border-radius: 1px;

	}
	.btn-default:hover{
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border:1px solid #606060 ;
		color: #000;
		border-radius: 1px;
	}
	div.view{
		margin-top:15px;
		width: 100%;
	}
	table#tblview tr td:first-child{
		width: 200px;
		text-align: left;
		font-weight: bold;
	}
	h2{
		color:#cb071a;
		font-size: 1.4em;
	}
	div.actions{
		display: inline-block;
	}
	div.actions div > a{
		padding: 0 5px 0 5px;
	}

	div.actions>div{
		display: inline-block;
	}
	table#tblview tr td:first-child {
		width: 200px;
		text-align: left;
		font-weight: bold;
		color: #011880;
	}
</style>
<div class="privilegios view container">
    <h2><?php echo __('Privilegio'); ?></h2>
	<?php
	if(isset($_SESSION['edit_priv'])){
		if($_SESSION['edit_priv']==1){
			unset($_SESSION['edit_priv']);
			?>
			<div class="alert alert-success " id="alerta" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Privilegio almacenado
			</div>
		<?php		}
	}
	?>
	<table id="tblview" cellpadding="0" cellspacing="0" class="tbl">
		<tr>
			<td class="td1">Id</td>
			<td class="td2"><?= $privilegio['Privilegio']['id']; ?></td>
		</tr>
		<tr>
			<td class="td1">Perfil</td>
			<td class="td2"><?= h($privilegio['Perfile']['perfil']); ?></td>
		</tr>
		<tr>
			<td class="td1">Recurso</td>
			<td class="td2"><?= h($privilegio['Recurso']['nombre']);?></td>
		</tr>
		<tr>
			<td class="td1">Activo</td>
			<td class="td2"><?php if($privilegio['Privilegio']['activo'] ==1) $pactivo = "Si";else $pactivo = "No";

				echo $pactivo; ?></td>
		</tr>
		<tr>
			<td class="td1">Leer</td>
			<td class="td2"><?php if($privilegio['Privilegio']['leer']==1) $pleer = "Si";else $pleer = "No";

				echo $pleer;
				?></td>
		</tr>
		<tr>
			<td class="td1">Adicionar</td>
			<td class="td2">			<?php
				if ($privilegio['Privilegio']['adicionar']==1) $padicionar ="Si";else $padicionar = "No";
				echo $padicionar; ?>
			</td>
		</tr>
		<tr>
			<td class="td1">Editar</td>
			<td class="td2"><?php
				if ($privilegio['Privilegio']['editar']==1) $peditar = "Si";else $peditar = "No";
				echo $peditar; ?></td>
		</tr>
		<tr>
			<td class="td1">Borrar</td>
			<td><?php
				if($privilegio['Privilegio']['borrar'] ==1) $pborrar = "Si";else $pborrar="No";
				echo $pborrar; ?>
			</td>
		</tr>
		<!--<tr>
			<td class="td1">Mis resgistros</td>
			<td><?php
			//	if($privilegio['Privilegio']['solopropietario'] ==1) $pborrar = "Si";else $pborrar="No";
			//	echo $pborrar; ?>
			</td>
		</tr>-->
		<tr>
			<td class="td1">Creado</td>
			<td class="td2"><?php
				$creado = date("Y-m-d H:i", strtotime($privilegio['Privilegio']['created']));
				echo h($privilegio['Privilegio']['usuario']." (".$creado.")"); ?></td>
		</tr>
		<tr>
			<td class="td1">Modificado</td>
			<td class="td2"><?php
				$modifi =($privilegio['Privilegio']['usuariomodif']!='')?$privilegio['Privilegio']['usuariomodif']." (".date("Y-m-d H:i", strtotime($privilegio['Privilegio']['modified'])).")":"";
				echo $modifi; ?></td>
		</tr>
	</table>
</div>

<div class="actions">

	<div><?php echo $this->Html->link(__('Editar Privilegio'), array('action' => 'edit', $privilegio['Privilegio']['id']),array('class'=>'btn btn-default')); ?> </div>
	<div><?php echo $this->Html->link(__('Listado de Privilegios'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<div>
    <?php if(count($detrecursos)>0){ ?>
    <h3>Detalles de privilegio - Botones</h3>
    <table style="width: 50%">
        <tr>
            <th>Nombre a mostrar</th>
            <th>Funcion</th>
            <th>Permitir</th>
        </tr>
        <?php
            foreach ($detrecursos as $kRecursos => $vRecursos) {
        ?>
                        <tr>
                            <td><?= $vRecursos['Detrecurso']['nombre'] ?></td>
                            <td><?= $vRecursos['Detrecurso']['funcion'] ?></td>
                            <td style="text-align: center;">
        <?php
                            $cont=0;
                            foreach($detprivilegios as $kpriv => $vPriv){
                                $cont++;
                                if($vRecursos['Detrecurso']['id'] == $vPriv['Detprivilegio']['detrecurso_id']){
                                    if($vPriv['Detprivilegio']['permitir'] == 1){
        ?>
                                        SI
        <?php
                                        break;
                                    }else{
        ?>
                                        NO
        <?php
                                        break;
                                    }
                                }else{
                                    if($cont >= count($detprivilegios))
                                    {

        ?>
                                        NO
        <?php
                                    }
                                }
                            }
        ?>
                            </td>
                        </tr>
        <?php
            }

        ?>
    </table>
    <?php } ?>
</div>
<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>