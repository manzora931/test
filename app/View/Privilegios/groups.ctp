<style>
    .activo{
        background: #98d9fa !important;
    }
</style>
<?php
$real_url2 = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
?>
<p>Página <?= $pag ?> de <?php echo ceil($totgrupos/5); ?>, <span id="num2"></span> registros de un total de <?= $totgrupos ?> , comienza en 1, finalizando en <span id="num3"></span></p>
<table cellpadding = "0" style='width: 100%; margin-top: 2em;' id="tab2" cellspacing="0" >
    <tr style="height: 25px; vertical-align: text-top">
        <th><?php echo __('Id'); ?></th>
        <th><?php echo __('Perfil'); ?></th>
        <th><?php echo __('Creado'); ?></th>
        <th><?php echo __('Última Modificación'); ?></th>
        <th><?php echo __('Activo'); ?></th>
        <th><?php echo __('Acción'); ?></th>
    </tr>
    <?php
    $i = 0;
    $cont=0;
    $pagi=1;
    foreach ($grupos as $group) {
        $class = null;
        if ($i++ % 2 != 0) {
            $class = ' class="altrow"';
        }
        $cont++;
        ?>

        <tr <?php echo $class; ?> >
            <td class="linea selected<?=$group['g']['id'];?>" style="height: 25px"><?php echo $group['g']['id']; ?></td>
            <td class="linea selected<?=$group['g']['id'];?>"><?php echo  $this->Js->submit($group['g']['perfil'], array('class'=>'elem','style'=>' border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; width: 100%; text-align: left; ','url'=> array('controller'=>'privilegios','action'=>'rec/'.$group['g']['id'].'-'.$pagi), 'update'=>'#recursos', 'frequency' => '0.2','id'=>'rec-'.$cont),array('onclick'=>"activar();"));?></td>
            <td class="linea selected<?=$group['g']['id'];?>"><span id="prod<?php echo $cont; ?>"><?php
                    if ($group['g']['created'] != '' && $group['g']['created'] != '0000-00-00 00:00:00'){
                        echo date("d-m-Y", strtotime($group['g']['created']));
                    }   ?></span></td>
            <td class="linea selected<?=$group['g']['id'];?>"><span id="prod<?php echo $cont;?>"><?php
                    if ($group['g']['modified'] != '' && $group['g']['modified'] != '0000-00-00 00:00:00') {
                        echo date("d-m-Y", strtotime($group['g']['modified']));
                    }?></span></td>
            <td class="linea selected<?=$group['g']['id'];?>" style="height: 25px" align="center"><?php if($group['g']['activo']){
                    echo "SI";
                }else{
                    echo "NO";
                } ?></td>
            <td class="selected<?=$group['g']['id'];?>" align="center">
                <?php echo $this->Js->submit('', array('class'=>'elem','style'=>' background:url("'.$real_url2.'/img/ojo.png") no-repeat; margin-left:15px; margin-top:1px; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; width: 12%; height:10%','url'=> array('controller'=>'privilegios','action'=>'rec/'.$group['g']['id'].'-'.$pagi), 'update'=>'#recursos', 'frequency' => '0.2','id'=>'ver-'.$group['g']['id'].'-'.$pagi)); ?>
            </td>
        </tr>

    <?php }

    ?>
</table>

<div class="paging">
    <?php
    if($pag == 1){
        $submit=array('div'=>false,'style'=>'pointer-events:none;color:#c1c1c1; text-decoration: underline; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.($pag-1)), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisPagAnt-'.($pag-1));
    }else {
        $submit=array('div'=>false,'style'=>'color:#003d4c; text-decoration: underline; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.($pag-1)), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisePagAnt-'.($pag-1));
    }
    echo "<span>";
    echo $this->Js->submit('<< Anterior',$submit);
    echo "</span>";
    $j = 1;
    for($i=0; $i < $totgrupos; $i+=5) {
        $submit = array();
        if(isset($pag) && $j==$pag) {
            echo "<span class='disabled'>";
            $submit=array('div'=>false,'style'=>'pointer-events:none;color:#c1c1c1; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.$j), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisPag-'.$j);
        }else {
            echo "<span>";
            $submit=array('div'=>false,'style'=>'color:#003d4c; text-decoration: underline; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.$j), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisPag-'.$j);
        }
        echo "|";
        echo $this->Js->submit($j, $submit);
        echo "</span>";
        $j++;
    }
    $pag = $pag+1;
    if($pag >= $j){
        $submit=array('div'=>false,'style'=>'pointer-events:none;color:#c1c1c1;; text-decoration: underline; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.$pag), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisPagSig-'.$pag);
    }else {
        $submit=array('div'=>false,'style'=>'color:#003d4c; text-decoration: underline; border: none; background-color: rgba(255, 255, 255, 0); cursor: pointer; text-align: left; display: inline-block;','url'=> array('controller'=>'privilegios','action'=>'groups/'.$pag), 'update'=>'#group', 'frequency' => '0.2','id'=>'btnProdnoLisPagSig-'.$pag);
    }
    echo "<span>";
    echo "|";
    echo $this->Js->submit('Siguiente >>',$submit);
    echo "</span>";
    ?>
</div>
<script>
    $(function(){
        $(window).load("ajax/test.html", function(){
            var n=document.getElementById("tab2").rows.length;
            n=n-1;
            $("#num2").text(n);
            $("#num3").text(n);
        });
    });
</script>
<?php echo $this->Js->writeBuffer(array('inline' => 'true')); ?>

<script>
    jQuery(function(){
        $(".elem").click(function(e){
            var divs = $("#"+e.target.id).parents("td");
            var td = divs[0];

            var cls = (divs[0]).attributes[0];
            var tst = $(td).attr("class");
            var arr = tst.split(" ");
            if(arr.length >1){
                $(".activo").removeClass('activo');
                $("."+arr[1]).addClass('activo');
            }else{
                $(".activo").removeClass('activo');
                $("."+arr[0]).addClass('activo');
            }
        });
    });
</script>