<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'proyecto/impresion')); ?>
<?php echo $this->Html->script('bootstrap.min.js');
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
$fecha = date('d-m-Y');
$fechaactual = explode('-', $fecha);
$mes=$meses[$fechaactual[1]];
?>
<div class="container" style="margin-bottom: 35px;">
    <div class="row">
        <div class="col-print-12">
            <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
            <label class="org"><?= $info[0]['Organizacion']['razonsocial'] ?></label>
            <br>
            <label class="tittle"><?php echo __('Sistema de Monitoreo y Evaluación de Proyectos'); ?></label>
            <div class="linea"></div>
            <label class="encabezado"><?php echo __('Seguimiento de Actividades al '. $fechaactual[0]. ' ' . $mes . ' de ' . $fechaactual[2]); ?></label>
        </div>
    <br>
    <table cellpadding="0" cellspacing="0" class="interlineado impresion" style="width: 100%;">
        <thead>
        <tr>
            <th>Actividad Origen</th>
            <th>Actividad Destino</th>
            <th>Monto</th>
            <th>Proyecto</th>
            <th>Usuario</th>
            <th>Creado</th>
        </tr>
        </thead>

        <?php
        $i=0;
        foreach ($vtrasladofondos as $vtrasladofondo):
            ?>
            <tr>
                <td class="text-left border2 border4"><?= h($vtrasladofondo['Vtrasladofondo']['actividad1']); ?></td>
                <td class="text-left border2"><?= h($vtrasladofondo['Vtrasladofondo']['actividad2']); ?></td>
                <td class="text-left border2">$ <?= number_format($vtrasladofondo['Vtrasladofondo']['monto'],2); ?></td>
                <td class="text-left border2"><?= h($vtrasladofondo['Vtrasladofondo']['proyecto']); ?></td>
                <td class="text-left border2"><?= h($vtrasladofondo['Vtrasladofondo']['usuario']); ?></td>
                <td class="text-left border2"><?= h($vtrasladofondo['Vtrasladofondo']['created']); ?></td>
            </tr>

        <?php endforeach; ?>
    </table>
    </div>
    <div class="row">
        <div class="col-print-12">
            <div class="actions do-not-print">
                <ul>
                    <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>