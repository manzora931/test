<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 12%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
</style>
<div class="users index  container">
    <h2><?php echo __('Traslado de Fondos'); ?></h2>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} de {:count} , comienza en {:start}, finalizando en {:end}')
        ));
        ?>
    </p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "vtrasladofondos";
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);   ?>
        <input type='hidden' name='_method' value='POST' />
        <table style='width: 100%; border:none; background-color: transparent;'>
            <tbody>
            <tr>
                <td style='width: 300px; height: auto;padding:1px 1px 2px 4px;'>
                <?php $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
                echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por:', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Actividad, Usuario'));   ?>
                </td>
                <td width="5%"></td>
                <td width="14%">
                <?= $this->Form->input('proyecto_id',array('class'=>'form-control','name'=>'data['.$tabla.'][proyecto_id]','label'=>"Proyecto", 'empty' => 'Seleccionar')); ?>
                </td>
                <td width="3%"></td>
                <td style=' padding-top: 20px; '>
                    <?php echo $this->Form->hidden('Controller', array('value'=>'Vtrasladofondo', 'name'=>'data[tabla][controller]')); //hacer cambio
                    echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
                    echo $this->Form->hidden('Parametro1', array('value'=>'actividad1,actividad2,usuario', 'name'=>'data[tabla][parametro]'));
                    echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
                    echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
                    echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]')); ?>
                    <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        if(isset($_SESSION["$tabla"])) {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
        }
        ?>
    </div>
    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th width="15%"><?= $this->Paginator->sort('actividade1_id','Actividad Origen'); ?></th>
            <th width="15%"><?= $this->Paginator->sort('actividade2_id','Actividad Destino'); ?></th>
            <th><?= $this->Paginator->sort('monto','Monto'); ?></th>
            <th><?= $this->Paginator->sort('proyecto_id','Proyecto'); ?></th>
            <th><?= $this->Paginator->sort('usuario','Usuario'); ?></th>
            <th class="bdr"><?= $this->Paginator->sort('created','Creado'); ?></th>
        </tr>
        </thead>

        <?php
        $i=0;
        foreach ($vtrasladofondos as $vtrasladofondo):
           ?>
            <tr>
                <td class="text-left"><?php echo h($vtrasladofondo['Vtrasladofondo']['actividad1']); ?></td>
                <td class="text-left"><?php echo h($vtrasladofondo['Vtrasladofondo']['actividad2']); ?></td>
                <td class="text-left">$ <?php echo number_format($vtrasladofondo['Vtrasladofondo']['monto'],2); ?></td>
                <td class="text-left"><?php echo h($vtrasladofondo['Vtrasladofondo']['proyecto']); ?></td>
                <td class="text-left"><?php echo h($vtrasladofondo['Vtrasladofondo']['usuario']); ?></td>
                <td class="text-left"><?php echo h($vtrasladofondo['Vtrasladofondo']['created']); ?></td>
            </tr>

        <?php endforeach; ?>
    </table>
    <div class="paging">
        <?php echo $this->paginator->prev('<< '.__('Anterior ', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->paginator->numbers();?>
        <?php echo $this->paginator->next(__('Siguiente ', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>

    <div class="actions">
        <div><a class="btn btn-default" href="javascript:window.open('<?php echo Router::url('/'); ?>vtrasladofondos/imprimir', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir</a></div>
    </div>
</div>
