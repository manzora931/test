<style>
	.tbl tr td{
		border: none;
	}
	.tbl{
		border: none;
		margin-left: 5px;
	}
	.td1{
		font-weight: bold;
		text-align: left;
		padding: 4px 0 4px 0;
	}
	.close{
		margin-top: -5px;
	}
</style>
<div class="privilegiosregistrosrecursos view">
	<h2><?php echo __('Privilegio de recurso'); ?></h2>
	<?php
		if(isset($_SESSION['pri_user_save'])){
				if($_SESSION['pri_user_save']==1){
						unset($_SESSION['pri_user_save']);
			?>
						<div class="alert alert-success " id="alerta" style="display: none;">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								Privilegio de recurso almacenado
							</div>
					<?php		}
	}
	?>
	<table cellpadding="0" cellspacing="0" class="tbl">
		<tr>
			<td class="td1">Id</td>
			<td class="td2"><?php echo h($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['id']); ?></td>
		</tr>
		<tr>
			<td class="td1">Registro de recurso</td>
			<td class="td2"><?php echo $this->Html->link($privilegiosregistrosrecurso['Registrosrecurso']['tabla'], array('controller' => 'registrosrecursos', 'action' => 'view', $privilegiosregistrosrecurso['Registrosrecurso']['id'])); ?></td>
		</tr>
		<tr>
			<td class="td1">Usuario</td>
			<td class="td2"><?php echo $this->Html->link($privilegiosregistrosrecurso['User']['name'], array('controller' => 'users', 'action' => 'view', $privilegiosregistrosrecurso['User']['id'])); ?></td>
		</tr>
		<tr>
			<td class="td1">Mostrar solo registros propios</td>
			<td class="td2"><?php echo ($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['solopropietario']==1)?"SI":"NO"; ?></td>
		</tr>
		<tr>
			<td class="td1">Activo</td>
			<td class="td2"><?php echo ($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['activo']==1)?"SI":"NO"; ?></td>
		</tr>
		<tr>
			<td class="td1">Creado</td>
			<td class="td2"><?php
				if(isset($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['created']) && $privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['created']!="")
				{
					echo h($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['usuario'])." (".h($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['created']).")";
				}
				?></td>
		</tr>
		<tr>
			<td class="td1">Modificado</td>
			<td class="td2"><?php
				if(isset($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['modified']) && $privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['usuariomodif']!="")
				{
					echo h($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['usuariomodif'])." (".h($privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['modified']).")";
				}
				?>
			</td>
		</tr>
	</table>

</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Privilegios de recursos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Privilegio de recurso'), array('action' => 'edit',$privilegiosregistrosrecurso['Privilegiosregistrosrecurso']['id'] )); ?> </li>
	</ul>
</div>
<br />
<div class="related index">
	<table cellpadding = "0" cellspacing = "0" id="detaTablaDetPri" style="width: 400px;">
		<tr>
			<th><?php echo __('Registro'); ?></th>
			<th><?php echo __('Permitir'); ?></th>
		</tr>
		<?php foreach($registros as $k => $v){ ?>
			<tr class="arrow">
				<td>
					<label><?= $this->Html->link($recursos[$v['det_privilegiosregistrosrecursos']['registro_id']], array('controller' => $registrosRec[0]['registrosrecursos']['tabla'], 'action' => 'view', $v['det_privilegiosregistrosrecursos']['registro_id']));
						 ?></label>
				</td>
				<td>
					<label><?= ($v['det_privilegiosregistrosrecursos']['permitir']==1)?"SI":"NO" ?></label>
				</td>
			</tr>
		<?php } ?>
	</table>
</div>
<script type="text/javascript">
		jQuery(function(){
				$("#alerta").slideDown();
				setTimeout(function () {
						$("#alerta").slideUp();
					}, 4000);
			});
	</script>