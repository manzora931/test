<?php
$real_url2 = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
$this->layout = "ventanas";
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#PrivilegiosregistrosrecursoEditForm").validationEngine();
		$('#Guardar2').attr("disabled", false);
	});
	function getURL2(){
		return '<?=$real_url2;?>';
	}
</script>
<style>
	.radio label, .checkbox label{
		padding-left: 14px;
	}
	.radio input[type="radio"],
	.radio-inline input[type="radio"],
	.checkbox input[type="checkbox"],
	.checkbox-inline input[type="checkbox"] {
		position: relative;
		margin-top: 4px \9;
		margin-left: 0px;
	}
	input[type="checkbox"] label{
		margin-left: 5px;
	}

</style>
<div class="privilegiosregistrosrecursos form">
	<?php echo $this->Form->create('Privilegiosregistrosrecurso'); ?>
	<?php
	echo $this->Form->input('id');
	echo $this->Form->input('registrosrecurso_id',array('label'=>'Recurso por pantalla', 'empty'=>'< Seleccionar >','class'=>'validate[required]'));
	echo $this->Form->input('user_id',array('label'=>'Usuario', 'empty'=>'< Seleccionar >','class'=>'validate[required]'));

	echo $this->Form->input('solopropietario',array('label'=>'Mostrar solo registros propios'));

	echo $this->Form->input('activo');
	?>
	<br />
	<div class="related">
		<table cellpadding = "0" cellspacing = "0" id="detaTablaDetPri" class="table table-condensed table-striped" style="width: 200px;">
			<?php $afec=(count($registros)>0)?count($registros):3; for ($i=0; $i<$afec; $i++) : ?>
				<?php
				$class = null;
				$regisId  = 'DetPrivilegiosregistrosrecurso.'.$i.".id";
				$registro = 'DetPrivilegiosregistrosrecurso.'.$i.'.registro_id';
				$permitir = 'DetPrivilegiosregistrosrecurso.'.$i.'.permitir';

				if ($i % 2 == 0) $class = ' class="altrow"';

				if ($i == 0) { ?>
					<tr>
						<th><?php echo __('Registro'); ?></th>
						<th><?php echo __('Permitir'); ?></th>
						<th><?php echo __("Eliminar"); ?></th>
					</tr>
					<?php
				}
				?>
				<tr class="detaTablaDetPri">
					<td>
						<?= $this->Form->input($regisId, array('type'=>'hidden')) ?>
						<?php echo $this->Form->input($registro, array('class'=>'registros','div' => false, 'onChange'=>'val_rec(this.id);','label' => false,'empty'=>'< Seleccionar >'));  ?>
					</td>
					<td>
						<?php echo $this->Form->input($permitir, array('div' => false, 'label' => false,'type'=>'checkbox','style'=>'margin-left:16px;'));  ?>
					</td>
					<td>
						<a class="eliminar" href="javascript:eliminar('DetPrivilegiosregistrosrecurso.<?= $i ?>.RegistroId',10);">X</a>
					</td>
				</tr>
				<?php
			endfor;
			?>
		</table>
	</div>
	<table style="width: 234px;">
		<tr>
			<td style="text-align: right; background-color: rgba(0, 129, 255, 0.11);">
				<label for="cantidadMasAf">Agregar</label>
				<input type="number" name="cantidadMasAf" id="detPri" style="width: 50px;margin-top: 7px;" value="1" min="0">
				<button style="margin-left: -5px;height: 24px;" class="masrc" id="masAf" name="masAf" data-nametabla="detaTablaDetPri" data-mas="detPri">+</button>
			</td>
		</tr>
	</table>
	<?php echo $this->Form->end(); ?>
</div>

<script>

	$("#Guardar2").click(function(e){
		$('#Guardar2').attr("disabled", true);
		var cont = $("#detaTablaDetPri tr").length - 1;
		var detalle = new Array();
		var num=0;
		var tmp_p=0;
		var tmp_s_prop=0;
		var tmp_ac=0;
		var err=0;
		for(var i = 0;i<cont;i++){
			if($("#DetPrivilegiosregistrosrecurso"+i+"Permitir").is(':checked')){
				tmp_p = 1;
			}else{
				tmp_p = 0;
			}
			detalle[i]=$("#DetPrivilegiosregistrosrecurso"+i+"RegistroId").val()+"#"+tmp_p+"#"+$("#DetPrivilegiosregistrosrecurso"+i+"Id").val();
			if($("#DetPrivilegiosregistrosrecurso"+i+"RegistroId").val() == ""){
				err++;
			}
		}
		if(err==cont){
			alert("Debe seleccionar al menos un detalle de privilegio.");
			$('#Guardar2').attr("disabled", false);
		}else{
			var id_pr = $("#PrivilegiosregistrosrecursoId").val();

			var rec_pantalla = $("#PrivilegiosregistrosrecursoRegistrosrecursoId").val();
			var user = $("#PrivilegiosregistrosrecursoUserId").val();
			if($("#PrivilegiosregistrosrecursoSolopropietario").is(':checked')){
				tmp_s_prop = 1;
			}
			if($("#PrivilegiosregistrosrecursoActivo").is(':checked')){
				tmp_ac=1;
			}
			var s_prop = tmp_s_prop;
			var activo = tmp_ac;
			var url = getURL2()+"privilegiosregistrosrecursos/save2";
			$.ajax({
				url:url,
				method:'POST',
				data:{id_pr:id_pr,rec_pantalla:rec_pantalla,user:user,s_prop:s_prop,activo:activo,detalle:detalle},
				success:function(resp){
					if(resp == "ok"){
						$('#Guardar2').attr("disabled", false);
						$("#capa").html("");
						$("#capa2").html("");
						$("#users").load(getURL() + "users/1");
						$("#privilegios").html("");
						setTimeout($("#alerta").slideDown(),2500);
						setTimeout(function() {
							$("#alerta").slideUp();
						},3000);
						$("#cancelar2").click();
					}
				}
			});
		}

		e.preventDefault();
		//$("#PrivilegiosregistrosrecursoAddForm").submit();
	});
	function val_rec(id){
		var cont = $("#detaTablaDetPri tr").length - 1;
		var arr = id.split("DetPrivilegiosregistrosrecurso");
		var arr2 = (arr[1]).split("RegistroId");
		var selected = arr2[0];
		var valor = $("#"+id).val();
		for(var x = 0;x<cont;x++){
			if(x!=selected){
				if($("#DetPrivilegiosregistrosrecurso"+x+"RegistroId").val() == valor){
					$("#"+id).val('');

					$("#"+id).validationEngine("showPrompt","No puede repetir un registro","AlertText");
				}
			}
		}
	}

	//Clona nuevos elementos del detalle.
	$(".masrc").click(function(e){
		e.preventDefault();

		var mas             = $("#"+$(this).data("mas")).val();
		var tablaDet        = $("#"+$(this).data("nametabla"));
		var numElmentDet    = tablaDet.children().children("."+$(this).data("nametabla")).first().children().length;
		var elemetDet       = tablaDet.children().children("."+$(this).data("nametabla")).first().children();
		var elementExis    = tablaDet.children().children("."+$(this).data("nametabla")).length;
		for(var i = 0; i < mas; i++)
		{
			var tr       = $("<tr class='"+$(this).data("nametabla")+"'>");
			var contador = 1;
			var nombre   = "";
			$.each(elemetDet, function (k, v){
				if(contador <= numElmentDet-1 )
				{
					var td = $("<td>");
					if($(v).children().length > 1)
					{
						var multipleHijos = $(v).children();
						$.each(multipleHijos, function (k2, v2){

							var name        = $(v2).attr('name');
							var nameArray   = name.split("[0]");
							var id          = $(v2).attr('id');
							var idArray     = id.split("0");
							var clone       = $(v2).clone();
							$(clone).attr('name',nameArray[0]+'['+(elementExis+i)+']'+nameArray[1]);
							$(clone).attr('id',idArray[0]+(elementExis+i)+idArray[1]);
							$(clone).val("");
							if($(clone).hasClass('fh'))
							{
								$(clone).attr('class','');
								$(clone).datetimepicker();
							}else if($(clone).hasClass('fecha'))
							{
								$(clone).attr('class','');
								$(clone).datepicker();
							}else if($(clone).hasClass('hora'))
							{
								$(clone).attr('class','');
								$(clone).timepicker();
							}
							if($(clone).attr('type')=='hidden'){
								if(idArray[1].endsWith('_')){
									$(clone).val('0');

								}
							}
							if($(clone).attr('type')=='checkbox'){
								$(clone).val('1');
							}
							td.append(clone);
							tr.append(td);
							if(contador == 1)
							{
								nombre = idArray[0]+"."+(elementExis+i)+"."+idArray[1];
							}
						});
					}else{
						var name        = $(v).children().attr('name');
						var nameArray   = name.split("[0]");
						var id          = $(v).children().attr('id');
						var idArray     = id.split("0");
						var clone       = $(v).children().clone();
						$(clone).attr('name',nameArray[0]+'['+(elementExis+i)+']'+nameArray[1]);
						$(clone).attr('id',idArray[0]+(elementExis+i)+idArray[1]);
						$(clone).val("");
						if($(clone).hasClass('fh'))
						{
							$(clone).attr('class','');
							$(clone).datetimepicker();
						}else if($(clone).hasClass('fecha'))
						{
							$(clone).attr('class','');
							$(clone).datepicker();
						}else if($(clone).hasClass('hora'))
						{
							$(clone).attr('class','');
							$(clone).timepicker();
						}
						if($(clone).attr('type')=='hidden'){
							if(idArray[1].endsWith('_')){
								$(clone).val('0');
								console.log($(clone));
							}
						}
						if($(clone).attr('type')=='checkbox'){
							$(clone).val('1');
						}
						td.append(clone);
						tr.append(td);
						if(contador == 1)
						{
							nombre = idArray[0]+"."+(elementExis+i)+"."+idArray[1];
						}
					}
				}
				contador++;
			});
			tr.append($("<td><a href=\"javascript:eliminar('"+nombre+" ');\" class='eliminar'>X</a></td>"));
			tablaDet.children().append(tr);
		}
	});

	/*ELIMINAR LINEA DE DETALLE*/
	function eliminar(datos, n)
	{
		var arrDatos = datos.split(".");
		var det = arrDatos[0];
		var i = arrDatos[1];
		var nom = arrDatos[2];
		var elim=confirm("¿Realmente desea borrar la fila?");
		if (elim){
			if (i!=0) {
				var al=$("#"+det+i+nom);
				elemant = al.parent();
				elemant.parent().remove();
			}else
			{

				$( "input[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
				$( "input[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
				$( "select[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
			}
		}else
		{
			alert("No se elimino la fila");
		}
	}

	//Carga la lista de registro de los detalles.

	$("#PrivilegiosregistrosrecursoRegistrosrecursoId").change(function(){
		var idRes=$(this).val();
		cargarregistro(idRes);
	});

	function cargarregistro(idRes){
		$.ajax({
			url: getURL2()+"privilegiosregistrosrecursos/cargarregistro",
			method: "POST",
			data: { idRes : idRes },
			dataType: "html",
			async:false
		}).done(function( result ) {
			var rt = $(".registros");
			$.each(rt,function(k,v){
				$(v).html(result);
			})
		}).fail(function( result ) {
			console.log("error - PrivilegiosregistrosrecursoRegistrosrecursoId")
			console.log(result);
		});
	}
	<?php
	if(count($registros)>0){
	?>
	$(document).ready(function(){
		var idRes=$("#PrivilegiosregistrosrecursoRegistrosrecursoId").val();
		cargarregistro(idRes);
		<?php
		for($i=0; $i < count($registros);$i++)
		{
		?>
		$("#DetPrivilegiosregistrosrecurso"+<?= $i ?>+"Id").val(<?= $registros[$i]['det_privilegiosregistrosrecursos']['id'] ?>);
		$("#DetPrivilegiosregistrosrecurso"+<?= $i ?>+"RegistroId").val(<?= $registros[$i]['det_privilegiosregistrosrecursos']['registro_id'] ?>);
		$("#DetPrivilegiosregistrosrecurso"+<?= $i ?>+"Permitir").prop("checked", "<?= ($registros[$i]['det_privilegiosregistrosrecursos']['permitir']==1)?"checked":"" ?>" );
		<?php
		}
		?>
	});
	<?php } ?>
</script>