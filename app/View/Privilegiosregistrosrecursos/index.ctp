<?php
$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es');
echo $this->Html->script('jquery.validationEngine');
?>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>


	<h2><?php echo __(' Privilegios por Usuarios'); ?></h2>

	<?php
	$tabla = "privilegiosregistrosrecursos";
	?>
	<div>
		<?php
		echo $this->Form->create($tabla, array('action'=>'index'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table style='width: 60%;'>";
		echo "<tr>";
		echo "<td>";
		if($this->session->check('text_prr')=== true) {
			echo $this->Form->input('SearchText', array('label' => 'Buscar por:', 'placeholder' => 'usuario,grupo','autocomplete'=>"off", 'value'=>$_SESSION['text_prr']));
		}else{
			echo $this->Form->input('SearchText', array('label' => 'Buscar por:', 'placeholder' => 'usuario,grupo','autocomplete'=>"off"));
		}
		echo "</td>";
		echo "<td style='width: 100px;'>";
		if ($this->session->check('act')) {
			echo "<br><input type='checkbox' name='data[$tabla][ac]' checked='checked' id='privilegiosActivo'>Inactivos";
		}else{
			echo "<br><input type='checkbox' name='data[$tabla][ac]' id='privilegiosActivo'>Inactivos ";
		}
		echo "</td>";

		echo "<td style='vertical-align: middle;'>";
		echo $this->Js->submit('Buscar', array('style'=>'cursor:pointer;z-index:100;margin-left:10px;','url'=> array('controller'=>'privilegiosregistrosrecursos','action'=>'users/1'), 'update'=>'#users', 'frequency' => '0.2','id'=>'buscaruser'));
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		if($this->session->check('text_prr')===true || $this->session->check('act')===true){
			echo "<input type='button' value='Ver todos' id='vertodos' style='margin-top:-5px; display: block;width: 120px;'>";
		}else {
			echo "<input type='button' value='Ver todos' id='vertodos' style='margin-top:-5px; display: none;width: 120px;'>";
		}
		?>
	</div><br>
	<div class="alert alert-success " id="alerta" style="display: none">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    Privilegio por Usuario Almacenado.
	</div>
	<div id="users">

	</div>
	<div id="privilegios">

	</div>
	<!--VENTANA MODAL-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title" id="myModalLabel">
						<i class="icon-edit"></i> Asignar Privilegios por Usuarios
					</h3>
				</div>
				<div class="modal-body">
					<div id="capa">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-second" data-dismiss="modal" id="cancelar1">Cancelar</button>
					<button type="button" class="btn btn-primario" id="Guardar">Guardar Cambios</button>
				</div>
			</div>
		</div>
	</div>
	<!--VENTANA MODAL-->
	<!--VENTANA MODAL-->
	<div class="modal fade" id="Modaled" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close2">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title" id="myModalLabel">
						<i class="icon-edit"></i> Editar Privilegios por Usuarios
					</h3>
				</div>
				<div class="modal-body">
					<div id="capa2">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-second" data-dismiss="modal" id="cancelar2">Cancelar</button>
					<button type="button" class="btn btn-primario" id="Guardar2">Guardar Cambios</button>
				</div>
			</div>
		</div>
	</div>
	<!--######################################################-->
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Asignar Privilegios'), array('action' => 'add'),array('data-toggle'=>"modal","data-target"=>"#myModal")); ?></li>
		</ul>
	</div>
	<!--p id="newButtonContainer" class="buttonContainer">
		<button id="newPaiseButton" type="button" data-toggle="modal" data-target="#myModal" >Asignar Privilegios</button>
	</p-->


<script>
	$(document).ready(function() {
		setTimeout(
			function(){
				$("#loader").css({'display':'none'});
			},800);
		$("#users").load(getURL() + "users/1");
		$("#newPaiseButton").click(function(){
			$("#capa").html('');
			$("#capa2").html('');
			$("#capa").load(getURL()+'add');
		});
		$("#cancelar1").click(function(){
			$("#capa").html("");
			$("#capa2").html("");
		});
		$("#ico_close").click(function(){
			$("#capa").html("");
			$("#capa2").html("");
		});
		$("#ico_close2").click(function(){
			$("#capa").html("");
			$("#capa2").html("");
		});
		$("#cancelar2").click(function(){
			$("#capa").html("");
			$("#capa2").html("");
		});
		$("#vertodos").click(function(e){
			e.preventDefault();
			$("#privilegios").html("");
			$("#vertodos").css({'display':'none'});
			$("#privilegiosregistrosrecursosSearchText").val("");
			$("#privilegiosActivo").prop("checked",false);
			$.ajax({
				url:getURL()+'limpiar',
				success:function(){
					$("#users").load(getURL() + "users/1");
				}
			});
		});
		$("#buscaruser").click(function(){
			$("#vertodos").css({'display':'block'});
		});
	});
	/* $("#capa").attr('src',getURL()+'view/'+id);*/
</script>
