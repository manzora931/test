<?php
$real_url2 = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
$this->layout = "ventanas";
?>
<style>
	.radio label, .checkbox label{
		padding-left: 14px;
	}
	.radio input[type="radio"],
	.radio-inline input[type="radio"],
	.checkbox input[type="checkbox"],
	.checkbox-inline input[type="checkbox"] {
		position: relative;
		margin-top: 4px \9;
		margin-left: 0px;
	}
	input[type="checkbox"] label{
		margin-left: 5px;
	}

</style>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#PrivilegiosregistrosrecursoAddForm").validationEngine();
		$('#Guardar').attr("disabled", false);
	});
	function getURL2(){
		return '<?=$real_url2;?>';
	}
</script>
<div class="privilegiosregistrosrecursos form">
	<?php echo $this->Form->create('Privilegiosregistrosrecurso'); ?>

	<?php
	echo $this->Form->input('modulo_id', array('label'=>'Módulo', 'empty'=>'< Seleccionar >','class' => "validate[required]",'required' => 'required'));
	echo $this->Form->input('recurso_id', array('empty'=>'< Seleccionar >','class' => "validate[required]",'required' => 'required'));
	echo $this->Form->input('registrosrecurso_id',array('label'=>'Recurso por pantalla', 'empty'=>'< Seleccionar >','class'=>'validate[required]','onchange'=>"va_privilegio();"));
	echo $this->Form->input('user_id',array('label'=>'Usuario', 'empty'=>'< Seleccionar >','class'=>'validate[required]','onchange'=>"va_privilegio();"));

	echo $this->Form->input('solopropietario',array('label'=>'Mostrar solo registros propios'));

	echo $this->Form->input('activo');
	?>

	<br />
	<div class="related">
		<table cellpadding = "0" cellspacing = "0" class="table table-condensed table-striped" id="detaTablaDetPri" style="width: 300px;">
			<?php $afec=3; for ($i=0; $i<$afec; $i++) : ?>
				<?php
				$class = null;
				$registro = 'DetPrivilegiosregistrosrecurso.'.$i.'.registro_id';
				$permitir = 'DetPrivilegiosregistrosrecurso.'.$i.'.permitir';

				if ($i % 2 == 0) $class = ' class="altrow"';

				if ($i == 0) { ?>
					<tr>
						<th><?php echo __('Registro'); ?></th>
						<th><?php echo __('Permitir'); ?></th>
						<th><?php echo __("Eliminar"); ?></th>
					</tr>
					<?php
				}
				?>
				<tr class="detaTablaDetPri">
					<td>
						<?php echo $this->Form->input($registro, array('class'=>'registros','onChange'=>'val_rec(this.id);','div' => false, 'label' => false,'empty'=>'< Seleccionar >'));  ?>
					</td>
					<td>
						<?php echo $this->Form->input($permitir, array('div' => false, 'label' => false,'type'=>'checkbox','style'=>'margin-left:16px;'));  ?>
					</td>
					<td>
						<a class="eliminar" href="javascript:eliminar('DetPrivilegiosregistrosrecurso.<?= $i ?>.RegistroId',10);">X</a>
					</td>
				</tr>
				<?php
			endfor;
			?>
		</table>
	</div>
	<table style="width: 300px;">
		<tr>

			<td style="text-align: right; background-color: rgba(0, 129, 255, 0.11);">
				<label for="cantidadMasAf">Agregar</label>
				<input type="number" name="cantidadMasAf" id="detPri" style="width: 50px;margin-top: 7px;" value="1" min="0">
				<button class="masrc" style="margin-left: -5px;height: 24px;" id="masAf" name="masAf" data-nametabla="detaTablaDetPri" data-mas="detPri">+</button>
			</td>
		</tr>
	</table>


	<?php
	echo $this->Form->end(); ?>
</div>
<script>
	function va_privilegio(){
		var user = $("#PrivilegiosregistrosrecursoUserId").val();
		var priv = $("#PrivilegiosregistrosrecursoRegistrosrecursoId").val();
		var url = getURL2()+'privilegiosregistrosrecursos/val_privi';
		if(user!='' && priv!=''){
			$.ajax({
				url:url,
				method:'post',
				data:{user:user,priv:priv},
				success:function(resp){
					if(resp==1){
						$("#PrivilegiosregistrosrecursoRegistrosrecursoId").val("");
						$("#PrivilegiosregistrosrecursoUserId").val("");
						$("#PrivilegiosregistrosrecursoRegistrosrecursoId").validationEngine("showPrompt","Ya existe un privilegio creado para el recurso seleccionado. Se recomienda editar el existente.","AlertText");
					}
				}
			});
		}
	}
	$("#Guardar").click(function(e){
		var cont = $("#detaTablaDetPri tr").length - 1;
		var detalle = new Array();
		var num=0;
		var tmp_p=0;
		var tmp_s_prop=0;
		var tmp_ac=0;
		var err=0;
		for(var i = 0;i<cont;i++){
			if($("#DetPrivilegiosregistrosrecurso"+i+"Permitir").is(':checked')){
				tmp_p = 1;
			}else{
				tmp_p = 0;
			}
			detalle[i]=$("#DetPrivilegiosregistrosrecurso"+i+"RegistroId").val()+"#"+tmp_p;
			if($("#DetPrivilegiosregistrosrecurso"+i+"RegistroId").val() == ""){
				err++;
			}
		}
		if(err==cont){
			alert("Debe seleccionar al menos un detalle de privilegio.");
		}else{
			var modulo = $("#PrivilegiosregistrosrecursoModuloId").val();
			var recurso = $("#PrivilegiosregistrosrecursoRecursoId").val();
			var rec_pantalla = $("#PrivilegiosregistrosrecursoRegistrosrecursoId").val();
			var user = $("#PrivilegiosregistrosrecursoUserId").val();
			if($("#PrivilegiosregistrosrecursoSolopropietario").is(':checked')){
				tmp_s_prop = 1;
			}
			if($("#PrivilegiosregistrosrecursoActivo").is(':checked')){
				tmp_ac=1;
			}
			var s_prop = tmp_s_prop;
			var activo = tmp_ac;
			var url = getURL2()+"privilegiosregistrosrecursos/save";
			$.ajax({
				url:url,
				method:'POST',
				data:{modulo:modulo,recurso:recurso,rec_pantalla:rec_pantalla,user:user,s_prop:s_prop,activo:activo,detalle:detalle},
				success:function(resp){
					if(resp == "ok"){
						$("#cancelar1").click();
						$("#capa").html("");
						$("#capa2").html("");
						$("#users").load(getURL() + "users/1");
						setTimeout($("#alerta").slideDown(),2500);
						setTimeout(function() {
							$("#alerta").slideUp();
						},3000);
					}
				}
			});
		}
		e.preventDefault();
		//$("#PrivilegiosregistrosrecursoAddForm").submit();
	});

	function val_rec(id){
		var cont = $("#detaTablaDetPri tr").length - 1;
		var arr = id.split("DetPrivilegiosregistrosrecurso");
		var arr2 = (arr[1]).split("RegistroId");
		var selected = arr2[0];
		var valor = $("#"+id).val();
		for(var x = 0;x<cont;x++){
			if(x!=selected){
				if($("#DetPrivilegiosregistrosrecurso"+x+"RegistroId").val() == valor){
					$("#"+id).val('');

					$("#"+id).validationEngine("showPrompt","No puede repetir un registro","AlertText");
				}
			}
		}
	}

	$("#PrivilegiosregistrosrecursoModuloId").change(function(){
		var idMod=$("#PrivilegiosregistrosrecursoModuloId").val();
		$.ajax({
			url: getURL2()+"privilegios/recursos",
			method: "POST",
			data: { idMod : idMod },
			dataType: "html"
		}).done(function( result ) {
			$("#PrivilegiosregistrosrecursoRecursoId").html(result);
		}).fail(function( result ) {
			console.log("error - PrivilegiosregistrosrecursoModuloId");
			console.log(result);
		});
	});
	$("#PrivilegiosregistrosrecursoRecursoId").change(function(){
		var idRec=$(this).val();
		$.ajax({
			url: getURL2()+"registrosrecursos/recursospantalla",
			method: "POST",
			data: { idRec : idRec },
			dataType: "html"
		}).done(function( result ) {
			$("#PrivilegiosregistrosrecursoRegistrosrecursoId").html(result);
		}).fail(function( result ) {
			console.log("error - PrivilegiosregistrosrecursoRecursoId")
			console.log(result);
		});
	});

	//Carga la lista de registro de los detalles.

	$("#PrivilegiosregistrosrecursoRegistrosrecursoId").change(function(){
		var idRes=$(this).val();
		$.ajax({
			url: getURL2()+"privilegiosregistrosrecursos/cargarregistro",
			method: "POST",
			data: { idRes : idRes },
			dataType: "html"
		}).done(function( result ) {
			var rt = $(".registros");
			$.each(rt,function(k,v){
				$(v).html(result);
			})
		}).fail(function( result ) {
			console.log("error - PrivilegiosregistrosrecursoRegistrosrecursoId")
			console.log(result);
		});
	});

	//Clona nuevos elementos del detalle.
	$(".masrc").click(function(e){
		e.preventDefault();
		var mas             = $("#"+$(this).data("mas")).val();
		var tablaDet        = $("#"+$(this).data("nametabla"));
		var numElmentDet    = tablaDet.children().children("."+$(this).data("nametabla")).first().children().length;
		var elemetDet       = tablaDet.children().children("."+$(this).data("nametabla")).first().children();
		var elementExis    = tablaDet.children().children("."+$(this).data("nametabla")).length;

		for(var i = 0; i < mas; i++)
		{
			var tr       = $("<tr class='"+$(this).data("nametabla")+"'>");
			var contador = 1;
			var nombre   = "";
			$.each(elemetDet, function (k, v){
				if(contador <= numElmentDet-1 )
				{
					var td = $("<td>");
					if($(v).children().length > 1)
					{
						var multipleHijos = $(v).children();
						$.each(multipleHijos, function (k2, v2){

							var name        = $(v2).attr('name');
							var nameArray   = name.split("[0]");
							var id          = $(v2).attr('id');
							var idArray     = id.split("0");
							var clone       = $(v2).clone();
							$(clone).attr('name',nameArray[0]+'['+(elementExis+i)+']'+nameArray[1]);
							$(clone).attr('id',idArray[0]+(elementExis+i)+idArray[1]);
							$(clone).val("");

							if($(clone).hasClass('fh'))
							{
								$(clone).attr('class','');
								$(clone).datetimepicker();
							}else if($(clone).hasClass('fecha'))
							{
								$(clone).attr('class','');
								$(clone).datepicker();
							}else if($(clone).hasClass('hora'))
							{
								$(clone).attr('class','');
								$(clone).timepicker();
							}
							if($(clone).attr('type')=='hidden'){
								if(idArray[1].endsWith('_')){
									$(clone).val('0');
								}
							}
							if($(clone).attr('type')=='checkbox'){
								$(clone).val('1');
							}
							td.append(clone);
							tr.append(td);
							if(contador == 1)
							{
								nombre = idArray[0]+"."+(elementExis+i)+"."+idArray[1];
							}
						});
					}else{
						var name        = $(v).children().attr('name');
						var nameArray   = name.split("[0]");
						var id          = $(v).children().attr('id');
						var idArray     = id.split("0");
						var clone       = $(v).children().clone();
						$(clone).attr('name',nameArray[0]+'['+(elementExis+i)+']'+nameArray[1]);
						$(clone).attr('id',idArray[0]+(elementExis+i)+idArray[1]);
						$(clone).val("");
						if($(clone).hasClass('fh'))
						{
							$(clone).attr('class','');
							$(clone).datetimepicker();
						}else if($(clone).hasClass('fecha'))
						{
							$(clone).attr('class','');
							$(clone).datepicker();
						}else if($(clone).hasClass('hora'))
						{
							$(clone).attr('class','');
							$(clone).timepicker();
						}
						if($(clone).attr('type')=='hidden'){
							if(idArray[1].endsWith('_')){
								$(clone).val('0');
								console.log($(clone));
							}
						}
						if($(clone).attr('type')=='checkbox'){
							$(clone).val('1');
						}
						td.append(clone);
						tr.append(td);
						if(contador == 1)
						{
							nombre = idArray[0]+"."+(elementExis+i)+"."+idArray[1];
						}
					}
				}
				contador++;
			});
			tr.append($("<td><a href=\"javascript:eliminar('"+nombre+" ');\" class='eliminar'>X</a></td>"));
			tablaDet.children().append(tr);
		}
	});

	//Elimina de talles
	function eliminar(datos, n)
	{
		var arrDatos = datos.split(".");
		console.log(arrDatos);
		var det = arrDatos[0];
		var i = arrDatos[1];
		var nom = arrDatos[2];
		var elim=confirm("¿Realmente desea borrar la fila?");
		if (elim){
			if (i!=0) {
				var al=$("#"+det+i+nom);
				elemant = al.parent();
				elemant.parent().remove();
			}else
			{
				$( "input[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
				$( "input[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
				$( "select[name^='data["+arrDatos[0]+"]["+arrDatos[1]+"][']" ).val("");
			}
		}else
		{
			alert("No se elimino la fila");
		}
	}
</script>