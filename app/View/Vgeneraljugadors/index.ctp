<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
echo $this->Html->css("select2");
echo $this->Html->script("select2");
?>
<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="vgeneraljugadors index container-fluid">
    <h2>Reporte general por jugador</h2>
    <p>
        <?php
        echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));
        ?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "vgeneraljugadors";
    ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table>";
        echo "<tr>";
        echo "<td width='30%'>";
        //$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Jugador', 'type'=>"hidden",'name'=>'data['.$tabla.'][search_text]','placeholder'=>'Jugador'));
        echo $this->Form->input('jugador_id',array('label'=>'Jugador', 'options'=>$jugadores,'empty'=>'Seleccionar','class'=>'form-control jugador'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('torneo_id',array('label'=>'Torneo', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('jornada_id',array('label'=>'Jornada', 'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";
        echo "<td style='padding-left: 10px;'>";
        echo $this->Form->input('evento_id',array('label'=>'Evento','options'=>$eventos,'empty'=>'Seleccionar','class'=>'form-control'));
        echo "</td>";


        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Vgeneraljugador', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?=$this->Paginator->sort("juego_id",'Id')?></th>
            <th><?=$this->Paginator->sort("torneo_id",'Torneo')?></th>
            <th><?=$this->Paginator->sort("fecha",'Fecha')?></th>
            <th><?=$this->Paginator->sort("jornada",'Jornada')?></th>
            <th><?=$this->Paginator->sort("minuto",'Minuto')?></th>
            <th><?=$this->Paginator->sort("jugador",'Jugador')?></th>
            <th><?=$this->Paginator->sort("equipo",'Equipo')?></th>
            <th><?=$this->Paginator->sort("evento",'Evento')?></th>

            <th><?=$this->Paginator->sort("comentario",'Comentario')?></th>
            <th><?=$this->Paginator->sort("equipocontrario",'Equip. Contrario')?></th>
        </tr>
        </thead>
	<?php foreach ($vgeneraljugadors as $vgeneraljugador): ?>
	<tr>
		<td><?= $this->Html->link($vgeneraljugador['Vgeneraljugador']['juego_id'], ["controller"=>"juegos", "action"=>"view", $vgeneraljugador['Vgeneraljugador']['juego_id']]); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneraljugador['Vgeneraljugador']['torneo'],["controller"=>"torneos","action"=>"view",$vgeneraljugador['Vgeneraljugador']['torneo_id']]); ?>&nbsp;</td>
		<td><?php echo date("d/m/Y",strtotime($vgeneraljugador['Vgeneraljugador']['fecha'])); ?>&nbsp;</td>
		<td><?php echo h($vgeneraljugador['Vgeneraljugador']['jornada']); ?>&nbsp;</td>
		<td><?php 
            if($vgeneraljugador['Vgeneraljugador']['minadicional']!=''){
                echo $vgeneraljugador['Vgeneraljugador']['minuto']."<span style='margin-left:2px;color:#6048D1;'>+".$vgeneraljugador['Vgeneraljugador']['minadicional']."</span>";
            }else{
                echo h($vgeneraljugador['Vgeneraljugador']['minuto']);
            }
         ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneraljugador['Vgeneraljugador']['jugador'],["controller"=>"jugadores","action"=>"view",$vgeneraljugador['Vgeneraljugador']['jugador_id']]); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneraljugador['Vgeneraljugador']['equipo'],["controller"=>"equipos","action"=>"view",$vgeneraljugador['Vgeneraljugador']['equipo_id1']]); ?>&nbsp;</td>
		<td><?php echo h($vgeneraljugador['Vgeneraljugador']['evento']); ?>&nbsp;</td>

		<td><?php echo h($vgeneraljugador['Vgeneraljugador']['comentario']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($vgeneraljugador['Vgeneraljugador']['equipocontrario'],["controller"=>"equipos","action"=>"view",$vgeneraljugador['Vgeneraljugador']['equipo_id2']]); ?>&nbsp;</td>
<?php endforeach; ?>
	</table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
</div>
<script>
    jQuery(function () {
        getdet();
        $(".jugador").select2();
        $("#vgeneraljugadorsEventoId").change(function(){
            var evento = $("#vgeneraljugadorsEventoId").val();
            if(evento!==''){
                var url = getURL()+"getDet";
                $.ajax({
                    url:url,
                    type: 'post',
                    data: {evento:evento},
                    cache:false,
                    async:false,
                    success:function(resp){
                        if(resp!==''){
                            $("#vgeneraljugadorsDeteventoId").html(resp);
                        }
                    }
                });
            }
        });
    });
    function getdet() {
        var evento = $("#vgeneraljugadorsEventoId").val();
        if(evento!==''){
            var url = getURL()+"getDet";
            $.ajax({
                url:url,
                type: 'post',
                data: {evento:evento},
                cache:false,
                async:false,
                success:function(resp){
                    if(resp!==''){
                        $("#vgeneraljugadorsDeteventoId").html(resp);
                    }
                }
            });
        }
    }
</script>