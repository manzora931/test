<div class="vgeneraljugadors view">
<h2><?php echo __('Vgeneraljugador'); ?></h2>
	<dl>
		<dt><?php echo __('Juego Id'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['juego_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo Id'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['torneo_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Torneo'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['torneo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jornada'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['jornada']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Minuto'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['minuto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jugador Id'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['jugador_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Jugador'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['jugador']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo Id1'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['equipo_id1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['equipo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Evento Id'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['evento_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Evento'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['evento']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Detevento'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['detevento']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comentario'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['comentario']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipo Id2'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['equipo_id2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipocontrario'); ?></dt>
		<dd>
			<?php echo h($vgeneraljugador['Vgeneraljugador']['equipocontrario']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Vgeneraljugadors'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Vgeneraljugadors'), array('action' => 'edit', '')); ?> </li>
	</ul>
</div>
