<div class="vgeneraljugadors form">
<?php echo $this->Form->create('Vgeneraljugador'); ?>
	<fieldset>
		<legend><?php echo __('Edit Vgeneraljugador'); ?></legend>
	<?php
		echo $this->Form->input('juego_id');
		echo $this->Form->input('torneo_id');
		echo $this->Form->input('torneo');
		echo $this->Form->input('fecha');
		echo $this->Form->input('jornada');
		echo $this->Form->input('minuto');
		echo $this->Form->input('jugador_id');
		echo $this->Form->input('jugador');
		echo $this->Form->input('equipo_id1');
		echo $this->Form->input('equipo');
		echo $this->Form->input('evento_id');
		echo $this->Form->input('evento');
		echo $this->Form->input('detevento');
		echo $this->Form->input('comentario');
		echo $this->Form->input('equipo_id2');
		echo $this->Form->input('equipocontrario');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Almacenar')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Vgeneraljugadors'), array('action' => 'index')); ?></li>
	</ul>
</div>
