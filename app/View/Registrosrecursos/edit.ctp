
<?php
	$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
	echo $this->Html->Script('jquery-ui.js',FALSE);
	echo $this->Html->css('validationEngine.jquery');
	echo $this->Html->script('jquery.validationEngine-es');
	echo $this->Html->script('jquery.validationEngine');
	echo $this->Html->Script('jquery-ui.js',FALSE);
	echo $this->Session->flash();
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		// binds form submission and fields to the validation engine
		jQuery("#frm").validationEngine();
	});
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>
<div class="Registrosrecurso form">
	<?php echo $this->Form->create('Registrosrecurso',array('id'=>'frm')); ?>
	<fieldset>
		<legend><?php echo __('Editar Recurso de pantalla'); ?></legend>
		<?php
			echo $this->Form->input('id');
			echo $this->Form->input('modulo_id', array('empty'=>'< Seleccionar >','class' => "validate[required]",'required' => 'required','selected'=>$this->data['Recurso']['modulo_id']));
			echo $this->Form->input('recurso_id', array('empty'=>'< Seleccionar >','class' => "validate[required]"));
			echo $this->Form->input('tabla',array('class' => "validate[required]", 'style'=>'width: 30%;'));
			echo $this->Form->input('campo',array('class' => "validate[required]", 'style'=>'width: 30%;'));
		?>
		<div class="checkbox">
			<label for="RegistrosrecursoActivo">
				<?= $this->Form->input('activo', [
					'label' => false
				]); ?>
				Activo
			</label>
		</div>
		<br>
		<?php echo $this->Form->end(__('Almacenar')); ?>
	</fieldset>

</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Recursos de pantallas'), array('action' => 'index')); ?></li>
	</ul>
</div>
<script>
	$("#RegistrosrecursoModuloId").change(function(){
		var idMod=$(this).val();
		recursos(idMod);
	});
	$(document).ready(function(){
		var idMod=$("#RegistrosrecursoModuloId").val();
		recursos(idMod);
		});
	function recursos(idMod){
		$.ajax({
			url: "../../privilegios/recursos",
			method: "POST",
			data: { idMod : idMod },
			dataType: "html"
		}).done(function( result ) {
			$("#RegistrosrecursoRecursoId").html(result);
			$("#RegistrosrecursoRecursoId > option[value='<?= $this->data['Recurso']['id'] ?>']").attr('selected', 'selected')
		}).fail(function( result ) {
			console.log("error")
			console.log(result);
		});
	}
	jQuery(function(){
		var validate = {
			"id" : "#RegistrosrecursoTabla",
			"form" : "#frm",
			"reg_id":"#RegistrosrecursoId",
			"message" : "El nombre de la tabla ingresada ya existe, ingrese uno diferente"
		};

		$(validate.id).change( function(){
			var val = $(validate.id).val();
			var id = $(validate.reg_id).val();
			var url = getURL() + 'val_table';
			validate_name(val, id, url, '');
		});

		$(validate.form).on("submit", function (e) {
			var val = $(validate.id).val();
			var id =  $(validate.reg_id).val();
			var url = getURL() + 'val_table';
			var form = this;

			e.preventDefault();
			validate_name(val, id, url, form);
		});

		// funcion para validar si nombre ya existe en la base de datos
		function validate_name(val, id, url, form) {
			$.ajax({
				url: url,
				type: 'post',
				data: { val, id},
				cache: false,
				success: function(resp) {
					if ( resp == "error" ) {
						$(validate.id).validationEngine("showPrompt", validate.message,"AlertText");
						$(validate.id).val("");
					} else {
						form.submit();
					}
				}
			});
		}

	});
</script>