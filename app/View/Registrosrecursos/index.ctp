<?php
	$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<style>
	table tr td {
		background-color: transparent;
	}
	#search_box form table td {
		padding:0;
		vertical-align: middle;
	}
	.pad{
		padding-left: 20px !important;
	}
	.pad_b{
		padding-left: 5px !important;
	}
</style>
<div class="registrosrecursos index">
	<h2><?php echo __('Recursos de pantallas'); ?></h2>
	<p>
		<?php
			echo $this->paginator->counter(array(
				'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
			));
		?></p>
	<?php
	$tabla = "registrosrecursos";
	?>
	<div id='search_box'>
		<?php
		echo $this->Form->create($tabla, array('action'=>'index'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td style='visibility:hidden'  >";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: ID O REFERENCIA', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'Placeholder'=>'ID o REFERENCIA','type'=>'hidden'));
		echo "</td>";
		echo "<td class='pad_b'>";
		if (isset($_SESSION['tabla['.$tabla.']']['modulo_id'])){

			echo $this->Form->hidden('selec',array('label'=>false,'id'=> 'selec','value'=>$_SESSION['tabla['.$tabla.']']['modulo_id']));
		}else{
			echo $this->Form->hidden('selec',array('label'=>false,'id'=> 'selec','value'=>0));
		}
		echo $this->Form->input('modulo_id',array('label'=>'Modulo:','name'=> 'data['.$tabla.'][modulo_id]','onChange'=>'llenar(0)','empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";
		echo "<td class='pad_b'>";
		if (isset($_SESSION['tabla['.$tabla.']']['recurso_id'])){

			echo $this->Form->hidden('selected',array('label'=>false,'id'=> 'selected','value'=>$_SESSION['tabla['.$tabla.']']['recurso_id']));
		}else{
			echo $this->Form->hidden('selected',array('label'=>false,'id'=> 'selected','value'=>0));
		}
		echo $this->Form->input('recurso_id',array('label'=>'Recurso:','name'=> 'data['.$tabla.'][recurso_id]','disabled' ,'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";
		echo "<td class='pad'>";
		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' checked='checked'>";
		}else{
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
		}
		echo "</td>";

		echo "<td  class='pad'>";

		echo $this->Form->hidden('Controller', array('value'=>'Registrosrecurso', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'mes', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
		#echo $this->Form->hidden('rangoMeses', array('value'=>'si', 'name'=>'data['.$tabla.'][rangoMeses]'));
		#echo $this->Form->hidden('campoMeses', array('value'=>'mes', 'name'=>'data['.$tabla.'][campoMeses]'));
		#echo $this->Form->hidden('campoAnio', array('value'=>'anio', 'name'=>'data['.$tabla.'][campoAnio]'));
		echo $this->Form->end('Buscar');
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>
		<?php
		if(isset($_SESSION[$tabla]))
		{
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
		}
		?>
	</div>
	<table cellpadding="0" cellspacing="0" class="table-striped">
	<tr>
			<th width="3%"><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('recurso_id'); ?></th>
			<th><?php echo $this->Paginator->sort('tabla'); ?></th>
			<th><?php echo $this->Paginator->sort('campo'); ?></th>
			<th width="5%"><?php echo $this->Paginator->sort('activo'); ?></th>
			<th class="actions" width="10%"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($registrosrecursos as $registrosrecurso): ?>
	<tr>
		<td><?php echo h($registrosrecurso['Registrosrecurso']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($registrosrecurso['Recurso']['modelo'], array('controller' => 'recursos', 'action' => 'view', $registrosrecurso['Recurso']['id'])); ?>
		</td>
		<td><?php echo h($registrosrecurso['Registrosrecurso']['tabla']); ?>&nbsp;</td>
		<td><?php echo h($registrosrecurso['Registrosrecurso']['campo']); ?>&nbsp;</td>
		<td><?php echo ($registrosrecurso['Registrosrecurso']['activo']==1)?"SI":"NO"; ?>&nbsp;</td>

	<td class="actions">
		<?php echo $this->Html->link(__(' ', true), array('action'=>'view', $registrosrecurso['Registrosrecurso']['id']),array('class'=>'ver')); ?>		<?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $registrosrecurso['Registrosrecurso']['id']),array('class'=>'editar')); ?>		</td>
<?php endforeach; ?>
	</table>
	<div class="paging">

		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
			</div>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo Recurso de pantalla'), array('action' => 'add')); ?></li>
	</ul>
</div>

<script>
	function llenar(indice){
		modulo = $("#registrosrecursosModuloId").val();

		sle=$("#registrosrecursosModuloId  option:selected").html();
		if (sle.toString()!="-- Seleccionar --") {

			$.ajax({
				url:'privilegios/recurso',
				type:'POST',
				data:{modulo:modulo,indice:indice},
				dataType:'json',
				success:  function (response) {
					if (response==0) {
						alert("Este modulo no cuenta con recursos en el sistema.");
						//$("#privilegiosRecursoId).html("<option value=''>--Seleccionar--</option>");
						//$("#municipio").html("<option value=''>Seleccionar Municipio</option>");
					}else{
						$("#registrosrecursosRecursoId").html(response);
						$("#registrosrecursosRecursoId").removeAttr("disabled");
					}
				}
			});
		}else{
			$("#registrosrecursoRecursosId").attr("disabled",'disabled');
			//$("#depto").html("<option value=''>Seleccionar Departamento</option>");
			//$("#municipio").html("<option value=''>Seleccionar Municipio</option>");
			//$("#municipio").attr("disabled",'disabled');
		}
		//});
	}
</script>
