<div class="registrosrecursos view">
<style>
	#tb{
		border:none;
		width: 600px;
	}
	#tb tr td{
		border:none;
	}
	#tb th, #tb td {
		background-color: transparent;
		text-align: left;
		padding: 5px;
	}

	table tr td {
		background-color: transparent;
	}

	.close{
		margin-top: -5px;
	}
</style>
<h2><?php echo __('Recurso de pantalla'); ?></h2>
<?php if(isset($_SESSION['recursospantalla_save'])) {
		if($_SESSION['recursospantalla_save']==1) {
			unset($_SESSION['recursospantalla_save']); ?>
			<div class="alert alert-success " id="alerta" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Recurso por pantalla almacenado
			</div>
<?php } } ?>
	<table id="tb">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<td>
				<?php echo h($registrosrecurso['Registrosrecurso']['id']); ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Recurso'); ?></th>
			<td>
				<?php echo $this->Html->link($registrosrecurso['Recurso']['modelo'], array('controller' => 'recursos', 'action' => 'view', $registrosrecurso['Recurso']['id'])); ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Tabla'); ?></th>
			<td>
				<?php echo h($registrosrecurso['Registrosrecurso']['tabla']); ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Campo'); ?></th>
			<td>
				<?php echo h($registrosrecurso['Registrosrecurso']['campo']); ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Activo'); ?></th>
			<td>
				<?php echo ($registrosrecurso['Registrosrecurso']['activo']==1) ? "SI" :  "NO"; ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Creado'); ?></th>
			<td>
				<?php
					if(isset($registrosrecurso['Registrosrecurso']['created']) && $registrosrecurso['Registrosrecurso']['created'] != "")
					{
						echo h($registrosrecurso['Registrosrecurso']['usuario'])." (".h($registrosrecurso['Registrosrecurso']['created']).")";
					};
				?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<th><?php echo __('Modificado'); ?></th>
			<td>
				<?php
					if(isset($registrosrecurso['Registrosrecurso']['modified']) && $registrosrecurso['Registrosrecurso']['modified'] != "")
					{
						echo h($registrosrecurso['Registrosrecurso']['usuariomodif'])." (".h($registrosrecurso['Registrosrecurso']['modified']).")";
					}
				?>
				&nbsp;
			</td>
		</tr>
	</table>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Recursos de pantallas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Recursos de pantalla'), array('action' => 'edit', h($registrosrecurso['Registrosrecurso']['id']))); ?> </li>
	</ul>
</div>
<!--<div class="related">
	<h3><?php /* echo __('Related Privilegiosregistrosrecursos'); ?></h3>
	<?php if (!empty($registrosrecurso['Privilegiosregistrosrecurso'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Registrosrecurso Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Solopropietario'); ?></th>
		<th><?php echo __('Activo'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Usuario'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Usuariomodif'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($registrosrecurso['Privilegiosregistrosrecurso'] as $privilegiosregistrosrecurso): ?>
		<tr>
			<td><?php echo $privilegiosregistrosrecurso['id']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['registrosrecurso_id']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['user_id']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['solopropietario']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['activo']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['created']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['usuario']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['modified']; ?></td>
			<td><?php echo $privilegiosregistrosrecurso['usuariomodif']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'privilegiosregistrosrecursos', 'action' => 'view', $privilegiosregistrosrecurso['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'privilegiosregistrosrecursos', 'action' => 'edit', $privilegiosregistrosrecurso['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'privilegiosregistrosrecursos', 'action' => 'delete', $privilegiosregistrosrecurso['id']), null, __('Are you sure you want to delete # %s?', $privilegiosregistrosrecurso['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Privilegiosregistrosrecurso'), array('controller' => 'privilegiosregistrosrecursos', 'action' => 'add')); */ ?> </li>
		</ul>
	</div>
</div>-->
<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>
