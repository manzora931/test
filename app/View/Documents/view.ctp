<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }

</style>
<div class="documents view container">
    <?php  if(isset($_SESSION['saveDoc'])){
               unset($_SESSION['saveDoc']); ?>
                <div class="alert alert-success" id="alerta">
                    <span class="icon icon-check-circled"></span>
                    Documento Publicado
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
    <?php   }   ?>
    <h2><?php echo __('Documento Publicado'); ?></h2>

    <table id="tblview">
        <tr>
            <td>Id</td>
            <td><?= $document['Document']['id']; ?></td>
        </tr>
        <tr>
            <td>Documento</td>
            <td><?= $document['Document']['documento']; ?></td>
        </tr>
        <tr>
            <td>Proyecto</td>
            <td><?= (isset($document['Proyecto']['id']))?$this->Html->link($document['Proyecto']['nombrecompleto'], array('controller' => 'proyectos', 'action' => 'view', $document['Proyecto']['id'])):""; ?></td>
        </tr>
        <tr>
            <td>Versión</td>
            <td><?= $document['Document']['version']; ?></td>
        </tr>
        <tr>
            <td>Archivo Adjunto</td>
            <td>
                <a href="<?= '../../files/adjuntos/' .$document['Document']['url_doc']; ?>" class="adj" download>
                 <span class="adjunto-gasto" data-gastoid=""><?= $this->Html->image('proyecto/adjuntar_comprobante.png') ?></span>
                </a>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: text-top;">Descripción</td>
            <td><?= $document['Document']['descripcion']; ?></td>
        </tr>
        <tr>
            <td>Creado</td>
            <td><?= $document['Document']['usuario']." (".$document['Document']['created'].")"; ?></td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td><?= ($document['Document']['usuariomodif'])?$document['Document']['usuariomodif']." (".$document['Document']['modified'].")":""; ?></td>
        </tr>
    </table>
</div>

<div class="actions">
    <div><?php echo $this->Html->link(__('Editar Documento'), array('action' => 'edit', $document['Document']['id']),array('class'=>'btn btn-default')); ?> </div>
    <div><?php echo $this->Html->link(__('Listado de Documentos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>

