<style>
    div.actions div > a{
        padding: 0 5px 0 5px;
    }
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 12%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }

</style>
<div class="documents index container">
	<h2><?php echo __('Documentos Publicados'); ?></h2>
	<p class="paginate-count clearfix">
		<?php
	echo $this->paginator->counter(array(
		'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));	?></p>

            <?php $table = "documents";	?>
            <?php $session = $this->Session->read('tabla[documents]'); ?>
            <?php $search_text = $session['search_text'] ?>
            <?php $real_url = '//'. $_SERVER['HTTP_HOST'] . $this->request->base .'/'. $this->params->controller .'/'; ?>
            <div id='search_box'>
                <?php
                /*Inicia formulario de busqueda*/
                $tabla = "documents";
                ?>
                <?php
                echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
                echo "<input type='hidden' name='_method' value='POST' />";
                echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
                echo "<tr>";
                echo "<td width='400px'>";
                $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
                echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Documento, Descripción, Versión'));
                echo "</td>";
                echo "<td width='3%'></td>";
                echo "<td width='20%'>";
                echo $this->Form->input('proyecto_id', [
                    'class' => 'form-control',
                    'empty' => 'Seleccionar',
                    'label' => 'Proyecto'
                ]);
                echo "</td>";
                echo "<td width='3%'></td>";
                echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
                echo $this->Form->hidden('Controller', array('value'=>'Document', 'name'=>'data[tabla][controller]')); //hacer cambio
                echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
                echo $this->Form->hidden('Parametro1', array('value'=>'documento,descripcion,version', 'name'=>'data[tabla][parametro]'));
                echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
                echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
                echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
                echo " <input type='submit' class='btn btn-default' value='Buscar' style='width: 100px;border-color: #606060;'>";

                echo "</td>";
                echo "</tr>";
                echo "</table>";
                ?>
                <?php
                if(isset($_SESSION['documents']))
                {
                    $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
                    echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
                }
                ?>
			</div>
	<table id="tblin" class="table table-condensed table-striped" cellpadding="0" cellspacing="0">
	    <thead>
            <tr>
                <th width="5%"><?php echo $this->Paginator->sort('id'); ?></th>
                <th width="20%"><?php echo $this->Paginator->sort('documento'); ?></th>
                <th width="25%"><?php echo $this->Paginator->sort('proyecto_id'); ?></th>
                <th><?php echo $this->Paginator->sort('descripcion','Descripción'); ?></th>
                <th width="10%"><?php echo $this->Paginator->sort('version','Versión'); ?></th>
                <th><?php echo __('Acciones'); ?></th>
            </tr>
        </thead>
	<?php foreach ($documents as $document): ?>
	<tr>
		<td class="text-left"><?php echo h($document['Document']['id']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($document['Document']['documento']); ?>&nbsp;</td>
		<td class="text-left">
			<?php echo $this->Html->link($document['Proyecto']['nombrecorto'], array('controller' => 'proyectos', 'action' => 'view', $document['Proyecto']['id'])); ?>
		</td>
		<td class="text-left"><?php echo h($document['Document']['descripcion']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($document['Document']['version']); ?>&nbsp;</td>
	<td>
		<?php echo $this->Html->link(__(' ', true), array('action'=>'view', $document['Document']['id']),array('class'=>'ver')); ?>		<?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $document['Document']['id']),array('class'=>'editar')); ?>		</td>
<?php endforeach; ?>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
</div>
<div class="actions">
    <div><?php echo $this->Html->link(__('Crear Documento'), array('action' => 'add'),array( 'style'=>'width:auto;','class'=>'btn btn-default')); ?></div>
</div>
