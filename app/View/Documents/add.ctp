<div class="documents form container">
<?php echo $this->Form->create('Document',array('enctype'=>"multipart/form-data")); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Documento Publicado'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
	<?php
		echo $this->Form->input('documento',[
		    'label'=>'Documento',
            'class' => 'form-control',
            'div' => ['class'=>'form-group'],
            'required'=>'required',
            'type'=>'text'
        ]);
		echo $this->Form->input('proyecto_id',[
		    'label'=>'Proyecto',
            'class' => 'form-control',
            'div' => ['class'=>'form-group'],
            'empty'=>"Seleccionar"
        ]);

		echo $this->Form->input('version',[
		    'label'=>'Versión',
            'class'=>'form-control',
            'div'=>['class'=>'form-group'],
            'type'=>'text'
        ]);
        echo $this->Form->input('url_doc',[
            'label'=>false,
            "type"=>"file",
            'required'=>'required',
            'div'=>['class'=>'form-group']
        ]);
        echo $this->Form->input('descripcion',[
            'label'=>'Descripción',
            'rows'=>3,
            'class'=>'form-control',
            'div'=>['class'=>'form-group']
        ]);
	?>
	</fieldset>

</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Documentos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script>
    jQuery(function(){
        $("#DocumentAddForm").submit(function(e){
            e.preventDefault();
            var doc = $("#DocumentDocumento").val();
            var file = $("#DocumentUrlDoc").val();
            if(doc !== '' && file !==''){
                document.getElementById("DocumentAddForm").submit();
            }else{
                $(".message").text("Complete los campos requeridos");
                $("#alert").slideDown();
                setTimeout(function(){
                    $("#alert").slideUp();
                },4000);
            }
        });
        $("#DocumentUrlDoc").change(function(){
            var inputFileImage = document.getElementById('DocumentUrlDoc');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('archivo',file);
            var url = getURL()+"documents/valFile";
            $.ajax({
                url: url,
                type: 'POST',
                contentType: false,
                data: data,
                dataType: 'json',
                processData: false,
                cache: false,
                success: function (resp) {
                    if(resp===0){
                        $(".message").text("Solo puede adjuntar archivos en formato PDF");
                        $("#alerta").slideDown();
                        setTimeout(function(){
                            $("#alerta").slideUp();
                        },4000);
                        $("#DocumentUrlDoc").val("");
                    }else{
                        $("#ruta").val("yes");
                    }
                }
            });
        });
    });
</script>

