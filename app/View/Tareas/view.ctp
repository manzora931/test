<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="tareas view container">
<h2><?php echo __('Tarea'); ?></h2>
    <?php
    if( isset( $_SESSION['tarea_save'] ) ) {
        if( $_SESSION['tarea_save'] == 1 ){
            unset( $_SESSION['tarea_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($tarea['Tarea']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Módulo</td>
            <td>
                <?=$tarea['Module']['nombre']?>
            </td>
        </tr>
        <tr>
            <td>Intervención</td>
            <td>
                <?=$tarea['Intervencione']['nombre']?>
            </td>
        </tr>
        <tr>
            <td>Actividad</td>
            <td><?=$tarea['Actividade']['nombre']?></td>
        </tr>
        <tr>
            <td>Tarea</td>
            <td><?=$tarea['Tarea']['nombre']?></td>
        </tr>
        <tr>
            <td><?php echo __('Descripción'); ?></td>
            <td>
                <?php echo h($tarea['Tarea']['descripcion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $tarea['Tarea']['usuario']." (".$tarea['Tarea']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($tarea['Tarea']['usuariomodif']!='')?$tarea['Tarea']['usuariomodif']." (".$tarea['Tarea']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Tarea'), array('action' => 'edit', $tarea["Tarea"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Tareas'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
    <div class="tblvr">
        <?php if (!empty($tarea['Subtarea'])): ?>
            <h2><?php echo __('Sub tareas'); ?></h2><br>
            <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th width="7px"><?php echo __('Id'); ?></th>
                    <th width="25%"><?php echo __('Nombre'); ?></th>
                    <th><?php echo __('Descripción'); ?></th>
                </tr>
                </thead>
                <?php foreach ($tarea['Subtarea'] as $subtarea): ?>
                    <tr>
                        <td><?php echo $subtarea['id']; ?></td>
                        <td class="text-left"><?php echo $subtarea['nombre']; ?></td>
                        <td class="text-left"><?php echo $subtarea['descripcion']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
