<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="tareas form container-fluid">
    <?php echo $this->Form->create('Tarea',array('id'=>'tarea')); ?>
    <fieldset>
        <legend><?php echo __('Editar Tarea'); ?></legend>
        <div class="row">
            <div class="col-sm-3">
                <?php
                echo $this->Form->input('id');
                    echo $this->Form->input('module_id',[
                    'label'=>'Módulo',
                    'class'=>'form-control validate[required]',
                    'empty'=>"Seleccionar",
                    'onchange'=>"load_inter(this.id,0);",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('intervencione_id',[
                    'label'=>'Intervención',
                    'class'=>'form-control validate[required]',
                    'empty'=>"Seleccionar",
                    'onchange'=>"load_act(this.id, 0, 0);",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('actividade_id',[
                    'label'=>'Actividad',
                    'class'=>'form-control validate[required]',
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('nombre',[
                    'label'=>'Tarea',
                    'required'=>true,
                    'placeholder'=>'Tarea',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>200
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('descripcion',['rows'=>3, 'label'=>"Descripción", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"Descripción", 'style'=>'margin-bottom: 15px']); ?>
            </div>
        </div>
        <?php
            //debug($this->data["Subtarea"]);
        ?>
        <div class="row">
            <div class="col-sm-12">
                <h3>Sub tareas</h3>
            </div>

            <table style="margin-left: 15px;" class="tb_tarea">

                <?php
                if(count($this->data['Subtarea'])>0) {
                    for ($i = 0; $i < count($this->data["Subtarea"]); $i++) { ?>
                        <tr class="line<?=$i?>">
                            <td>
                                <?=
                                $this->Form->input("subtarea." . $i . ".id", [
                                    'type' => 'hidden',
                                    'value' => $this->data["Subtarea"][$i]['id'],
                                ])
                                ?>
                                <?= $this->Form->input('subtarea.' . $i . ".nombre", [
                                    'label' => false,
                                    'id' => "nombre" . $i,
                                    'value' => $this->data["Subtarea"][$i]['nombre'],
                                    'placeholder' => 'Nombre de subtarea',
                                    'class' => 'form-control nombret',
                                    'div' => ['class' => 'form-groupp'],
                                    //'maxlength'=>200
                                ]); ?>
                            </td>
                            <td>
                                <?= $this->Form->input('subtarea.' . $i . ".desc", [
                                    'label' => false,
                                    'id' => "descripcion" . $i,
                                    'value' => $this->data["Subtarea"][$i]['descripcion'],
                                    'placeholder' => 'Descripción de subtarea',
                                    'class' => 'form-control margen',
                                    'div' => ['class' => 'form-groupp'],
                                    //'maxlength'=>200
                                ]); ?>
                            </td>
                            <td style='width: 200px;'>
                                <button  data-corr='0' onclick='deleteT(<?=$i?>);' class='btn btn-default btn-remove-row btn-search margenbtn' type='button'><span><i class='fa fa-trash icon-search'></i></span></button>
                            </td>
                        </tr>
                    <?php }
                }else{
				for($i=0;$i<=2;$i++){	?>
                    <tr class="line<?=$i?>">
                        <td>
                            <?= $this->Form->input('subtarea.'.$i.".nombre",[
                                'label'=>false,
                                'id'=>"nombre".$i,
                                'placeholder'=>'Nombre de subtarea',
                                'class'=>'form-control nombret',
                                'div'=>['class'=>'form-groupp'],
                                //'maxlength'=>200
                            ]); ?>
                        </td>
                        <td>
                            <?= $this->Form->input('subtarea.'.$i.".desc",[
                                'label'=>false,
                                'id'=>"descripcion".$i,
                                'placeholder'=>'Descripción de subtarea',
                                'class'=>'form-control margen',
                                'div'=>['class'=>'form-groupp'],
                                //'maxlength'=>200
                            ]); ?>
                        </td>
                        <td style='width: 200px;'>
                            <button  data-corr='0' onclick='deleteT(<?=$i?>);' class='btn btn-default btn-remove-row btn-search margenbtn' type='button'><span><i class='fa fa-trash icon-search'></i></span></button>
                        </td>
                    </tr>
                <?php				}
                }

                ?>
            </table><br>
            <table style="margin-left: 15px;">
                <tr>
                    <td colspan="2"><button id="addDet" onclick="newdet();" class="btn btn-default btn-add-row btn-search" type="button"><span><i class="fa fa-plus icon-search"></i></span></button></td>
                </tr>
            </table>
            <div class="col-sm-3">

            </div>
            <div class="col-sm-7">

            </div>
            <!--div class="col-md-2 col-sm-4 col-xs-2 bloque-entidad-plus">
                <button class="btn btn-default btn-add-row btn-search" type="button"><span><i class="fa fa-plus icon-search"></i></span></button>
            </div -->
        </div>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Tareas'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    function deleteT(num){
        $(".line"+num).remove();
    }
    jQuery(function(){
        var intervencion = '<?=$this->data["Tarea"]["intervencione_id"]?>';
        var acti = '<?=$this->data["Tarea"]["actividade_id"]?>';
        load_inter("TareaModuleId", intervencion);

        load_act("TareaIntervencioneId", acti, intervencion);

    });
    function newdet(){
        var cont = $(".nombret").length;
        $(".tb_tarea").append("<tr><td><div class='form-groupp'>" +
            "<input name='data[subtarea]["+cont+"][nombre]' id='nombre"+cont+"' placeholder='Nombre de subtarea' class='form-control nombret' maxlength='255' type='text'>" +
            "</div></td>" +
            "<td><div class='form-groupp'>" +
            "<input name='data[subtarea]["+cont+"][desc]' id='descripcion"+cont+"' placeholder='Descripción de subtarea' class='form-control margen' type='text'>" +
            "</div></td>" +
            "<td style='width: 200px;'>"+
            "<button  data-corr='0' onclick='deleteT("+cont+");' class='btn btn-default btn-remove-row btn-search margenbtn' type='button'><span><i class='fa fa-trash icon-search'></i></span></button>"+
            "</td>"+
            "</tr>");

    }
    function load_inter(id,select){
        var url = getURL()+"loadInt";
        var mod = $("#"+id).val();
        $.ajax({
            url: url,
            type: 'post',
            data: {id:mod, value: select},
            cache:false,
            async: false,
            success: function(resp){
                if(resp!=="error"){
                    $("#TareaIntervencioneId").html(resp);

                    $("#TareaActividadeId").html("<option>Seleccionar</option>");
                }
            }
        });
    }
    function load_act(id,select,inter){
        var url = getURL()+"loadAc";
        if(inter>0){
            var mod = inter;
        }else{
            var mod = $("#"+id).val();
        }
        $.ajax({
            url: url,
            type: 'post',
            data: {id:mod, value: select},
            cache: false,
            async: false,
            success: function(resp){
                if(resp!=="error"){
                    $("#TareaActividadeId").html(resp);

                }
            }
        });
    }
</script>
