<?php
$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es');
echo $this->Html->script('jquery.validationEngine');
?>
<style>
    div.form{
        width: 100%;
        margin-bottom: 15px;
    }
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    fieldset{
        width: 80%;
        margin-top: 15px;
        padding: 15px 15px 15px 15px;
        border:1px solid #cb071a;
        color: #011880;
    }
    fieldset > div label{
        padding-top: 2px;
        color: #011880;
    }
    legend{
        color: #cb071a;
        font-size: 1.4em;
        font-weight: bold;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration:none;
    }
    div.actions>div{
        display: inline-block;
    }
    .form-group{
        width:700px;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // binds form submission and fields to the validation engine
        jQuery("#recur").validationEngine();
    });
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="rubros form container-fluid">
<?php echo $this->Form->create('Rubro',array('id'=>'recur')); ?>
	<fieldset>
		<legend><?php echo __('Editar Rubro'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('rubro',array('label' => 'Rubro','class' => "validate[required]",'required'));
		echo $this->Form->input('descripcion',array('label' => 'Descripción',
            'class' => 'form-control',
            'rows' => 3,
            'div' => ['class'=>'form-group']
        ));
		echo $this->Form->input('activo', array('label' => 'Activo','div'=>false,'style'=>'margin:0; margin-right:4px;'));
	?>
	</fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'id' => 'almacenar',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de rubros'), array('controller' => 'rubros', 'action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>

<script>
    jQuery(function(){
        var validate = {
            "id" : "#RubroRubro",
            "form" : "#recur",
            "message" : "El rubro ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = $("#RubroId").val();

            var url = getURL() + 'val_name';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = $("#RubroId").val();
            var url = getURL() + 'val_name';
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val: val, id: id},
                cache: false,
                success: function(resp) {
                    if ( resp == "error" ) {
                        $(validate.id).val("");
                        $(validate.id).validationEngine("showPrompt", validate.message,"AlertText");

                    } else {
                        form.submit();
                    }
                }
            });
        }

    });
</script>

