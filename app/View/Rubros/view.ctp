<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style>
<div class="rubros view container-fluid">
<h2><?php echo __('Rubro'); ?></h2>
    <?php
    if(isset($_SESSION['rebro_save'])){
        if($_SESSION['rebro_save']==1){
            unset($_SESSION['rebro_save']);
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Rubro almacenado
            </div>
        <?php		}
    }
    ?>
    <table id="tblview">
        <tr>
            <td><strong>Id</strong></td><td><?php echo h($rubro['Rubro']['id']); ?></td>
        </tr>
        <tr>
            <td><strong>Rubro</strong></td><td><?php echo h($rubro['Rubro']['rubro']); ?></td>
        </tr>
        <tr>
            <td><strong>Descripción</strong></td><td><?php echo h($rubro['Rubro']['descripcion']); ?></td>
        </tr>
        <tr>
            <td><strong>Activo</strong></td><td><?= ($rubro['Rubro']['activo']==1)?"Si":"No"; ?></td>
        </tr>
        <tr>
            <td><strong>Creado</strong></td><td><?php echo h($rubro['Rubro']['usuario']); ?>
                <?php echo "(".h($rubro['Rubro']['created']); echo ")"; ?></td>
        </tr>
        <tr>
            <td><strong>Modificado</strong></td><td>
                <?php
                if(isset($rubro['Rubro']['usuariomodif'])){
                    echo h($rubro['Rubro']['usuariomodif']);
                    echo "(".h($rubro['Rubro']['modified']); echo ")";
                }
                ?>
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Listado de Rubro'), array('action' => 'index'), array('class' => 'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Editar Rubro'), array('action' => 'edit', $rubro['Rubro']['id']), array('class' => 'btn btn-default')); ?></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>