<div class="partidas view">

<h1><center><b><?php  __('Coordinadora para la Reconstrucción y el Desarrollo');?></b></center></h1>
<h1><center><b><?php  __('C.R.D.');?></b></center></h1>
<h1><center><b><?php  __('NIT: 0614-200892-112-2');?></b></center></h1>
<h4><b><?php  __('Partida Contable');?></b></h4>
	<b><?php __('Proyecto:'); ?></b>&nbsp;<u><?php echo $partida['Proy']['proyecto']; ?></u><br>
	<b><?php __('Partida No.:'); ?></b>&nbsp;<?php echo $partida['Partida']['numpartida']; ?><br>
	<b><?php __('Tipo de Partida:'); ?></b>&nbsp;<?php echo $partida['Tipopartida']['tipopartida']; ?><br>
	<b><?php __('Referencia:'); ?></b>&nbsp;<?php echo $partida['Partida']['referencia']; ?><br>
	<b><?php __('Fecha:'); ?></b>&nbsp;<?php echo $partida['Partida']['fecha']; ?><br>
	<b><?php __('Concepto:'); ?></b>&nbsp;<?php echo $partida['Partida']['concepto']; ?><br>
	<b><?php __('Mayorizada:'); ?></b>&nbsp;<?php echo $partida['Partida']['mayorizada']; ?>
	<b><?php __('&nbsp;&nbsp;por:'); ?></b>&nbsp;<?php echo $partida['Partida']['usuariomayor']; ?><br>
	<b><?php __('Fecha/hora creado:'); ?></b>&nbsp;<?php echo $partida['Partida']['created']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<b><?php __('Fecha/hora modificado:'); ?></b>&nbsp;<?php echo $partida['Partida']['modified']; ?>

</div>

<!-- <div class="related"> -->
	<h3><?php __('Detalle de la partida');?></h3>
	<?php if (!empty($partida['Detpartida'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Cuenta'); ?></th>
		<th><?php __('Concepto'); ?></th>
		<th><?php __('Referencia'); ?></th>
		<!-- <th><?php __('Proyecto'); ?></th> -->
		<th><?php __('Debe'); ?></th>
		<th><?php __('Haber'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vabonos = 0; 
		$vcargos = 0; 
		//$vdescuento = 0; 
		$vtotal = 0;
		foreach ($partida['Detpartida'] as $detpart):
			$class = null;
			//$producto = 'Madera';
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
				//$producto = 'Pegamento 3M';
			}
		?>
		<tr<?php echo $class;?>>
			
			<td class="texto"><b><?php echo $vcuentas[$detpart['vcuenta_id']];?></b></td>
			<td class="texto"><?php echo $detpart['concepto'];?></td>
			<td class="texto"><?php echo $detpart['referencia'];?></td>
			<!-- <td class="texto"><?php echo $proys[$detpart['proy_id']];?></td> -->
			<td class="money">$&nbsp;<?php echo number_format($detpart['cargo'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detpart['abono'],2);?></td>
		</tr>
		<?php
		$vabonos = $vabonos + $detpart['abono'];
		$vcargos = $vcargos + $detpart['cargo'];
		?>
	<?php endforeach; ?>
	<tr>
		<td colspan=3><b>TOTALES</b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vcargos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vabonos,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>
<br>
<b><?php __('Hecho por:'); ?></b>&nbsp;<?php echo $partida['Partida']['usuario']; ?><br>


