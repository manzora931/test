<div class="solrecursos view">
<h1><center><b><?php  __('CRD - Solicitud de Recursos');?></b></center></h1>
<h4><b><?php  __('Recursos Retornados por el personal');?></b></h4>
	<b><?php __('No. de Solicitud:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['id']; ?><br>
	<b><?php __('Proyecto:'); ?></b>&nbsp;<u><?php echo $solrecurso['Proy']['proyecto']; ?></u><br>
	<b><?php __('Solicitante:'); ?></b>&nbsp;<?php echo $solrecurso['Vrhumano']['nombre']; ?><br>
	<b><?php __('Status:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['status']; ?><br>
	<b><?php __('Destino:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['destinos']; ?><br>
	<b><?php __('Motivo:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['descripcion']; ?><br>
	<b><?php __('Fecha/hora de creación de solicitud:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['created']; ?>
	<b><?php __('&nbsp;&nbsp;por:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['usuario']; ?><br>
	<b><?php __('Fecha a utilizar:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['fecha']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<b><?php __('Fecha a retornar:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['fecharetorno']; ?><br>
	<b><?php __('Fecha/hora de entregado:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['fechaentregado']; ?>
	<b><?php __('&nbsp;&nbsp;por:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['usuarioentrega']; ?><br>
	<b><?php __('Fecha de retorno del equipo:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['fecharetornado']; ?><br>
	<b><?php __('Recibido por:'); ?></b>&nbsp;<?php echo $solrecurso['Solrecurso']['usuariorecibe']; ?><br><br>


	<table cellpadding = "0" cellspacing = "0">
	<tr>
	<?php if (!empty($solrecurso['Detsolrecurso'])):?>
		<?php
		$i = 0;?>
		 <td>
		<b><?php  __('Recursos solicitados');?><br></b>
		<?php
		foreach ($solrecurso['Detsolrecurso'] as $detsolrecurso):
		?>
			<?php echo $Recursos[$detsolrecurso['recurso_id']];?>
			<?php 
			if (!empty($detsolrecurso['km1'])) {
				echo "<br>( Km. Inicial ".$detsolrecurso['km1']."&nbsp;-&nbsp;Km. Final ".$detsolrecurso['km2']." )";
			}
			?>			
			<br>
		<?php endforeach; ?>
		
	<?php endif; ?>
	</td>

	<?php if (!empty($solrecurso['Detsolrhumano'])):?>
	
		<?php
			$i = 0;
			 ?>
			 <td>
			<b><?php __('Recurso Humano');?><br></b>
			<?php
			foreach ($solrecurso['Detsolrhumano'] as $detsolrhumano):
			?>
				<?php echo $RHs[$detsolrhumano['vrhumano_id']];?>
				<?php 
				if ($detsolrhumano['conductor'] ==  1) {
					echo " (conductor)";
				}
				?>
				<br>
		<?php endforeach; ?>
	
	<?php endif; ?>
	</td>
	</tr>
	</table>
	
<TABLE  class='firma'>
<TR>
	<TD><BR><BR><BR>______________________<BR>Firma de Administración</TD>
	<TD><BR><BR><BR>______________________<BR>Firma de Entregado</TD>
</TR>
</TABLE>

