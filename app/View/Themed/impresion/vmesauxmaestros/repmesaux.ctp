<div class="vmesauxmaestros view">
<h2><?php  __('Analítico de Cuentas por Mes');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Organizacion'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vmesauxmaestro['Organizacion']['organizacion']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vmesauxmaestro['Proy']['proyecto']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mes'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b><?php echo $vmesauxmaestro['Vmesauxmaestro']['mestexto']."/".$vmesauxmaestro['Vmesauxmaestro']['anio']; ?></b>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Num. de Registros'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vmesauxmaestro['Vmesauxmaestro']['num']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="related">
	<h3><?php __('Detalle de Movimientos por Cuenta Contable');?></h3>
	<?php if (!empty($vmesauxmaestro['Vmesauxiliare'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Fecha'); ?></th>
		<th><?php __('Partida'); ?></th>
		<th><?php __('Referencia'); ?></th>
		<th><?php __('Concepto'); ?></th>
		<th><?php __('Cargos'); ?></th>
		<th><?php __('Abonos'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vabonos = 0; 
		$vcargos = 0; 
		//$vdescuento = 0; 
		$vtotal = 0;
		$j = 0;

		$vcta = '--'; 
		$vsubcargo = 0; 
		$vsubabono = 0; 
		$vsaldo = 0; 
		$vsaldocta = 0;

		foreach ($vmesauxmaestro['Vmesauxiliare'] as $detpart):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		
			if ($j == 0){
				$vcta = $detpart['cta'];
				$vsubabono = $vsubabono + $detpart['abono'];
				$vsubcargo = $vsubcargo + $detpart['cargo'];
				//$vcattotal = $vcattotal + $det['total'];
			?>

				<tr>
					<td colspan = 4><b>Cuenta Contable:&nbsp;<?php echo $detpart['descripcion'];?>&nbsp;&nbsp;Saldo inicial ....</b></td> 
					<!-- <td><b>TOTALES</b></td> -->
					<?php
						if ($hsaldos[$detpart['vcuenta_id']] < 0){
					?>
						<td><b>&nbsp;</b></td>
						<td><b><?php echo number_format($hsaldos[$detpart['vcuenta_id']]*-1,2);?></b></td>
					<?php
						}
						else{
					?>
						<td><b><?php echo number_format($hsaldos[$detpart['vcuenta_id']],2);?></b></td>
						<td><b>&nbsp;</b></td>
					<?php
						}
					?>
				</tr>
			<?php
			}
			else{
				if ($vcta != $detpart['cta']) { // si cambio de cuenta, insertar subtotales
					?>
						<tr>
							<td colspan = 4><b>Saldo Final de la Cuenta Contable:&nbsp;<?php echo $vcta;?></b></td> 
							<!-- <td><b>TOTALES</b></td> -->
							<?php
								$vsaldocta = $vsaldo + $vsubcargo - $vsubabono;
								if ($vsaldocta < 0){
							?>
									<td><b>&nbsp;</b></td>	
									<td><b>$&nbsp;<?php echo number_format($vsaldocta * -1,2);?></b></td>
							<?php
								}
								else{
							?>
									<td><b>$&nbsp;<?php echo number_format($vsaldocta,2);?></b></td>
									<td><b>&nbsp;</b></td>	
							<?php
								}
							?>		
							
						</tr>
						
					<?php
					$vsubabono = $detpart['abono'];
					$vsubcargo = $detpart['cargo'];
					$vsaldo = $hsaldos[$detpart['vcuenta_id']];
				?>
					<tr>
					<td colspan = 4><b>Cuenta Contable:&nbsp;<?php echo $detpart['descripcion'];?>&nbsp;&nbsp;Saldo inicial .....</b></td> 
					<!-- <td><b>TOTALES</b></td> -->
					
					<?php
						if ($hsaldos[$detpart['vcuenta_id']] < 0){
					?>
						<td><b>&nbsp;</b></td>
						<td><b><?php echo number_format($hsaldos[$detpart['vcuenta_id']]*-1,2);?></b></td>
					<?php
						}
						else{
					?>
						<td><b><?php echo number_format($hsaldos[$detpart['vcuenta_id']],2);?></b></td>
						<td><b>&nbsp;</b></td>
					<?php
						}
					?>
				</tr>
				<?php
				}
				else{
					$vsubabono = $vsubabono + $detpart['abono'];
					$vsubcargo = $vsubcargo + $detpart['cargo'];
				}
				$vcta = $detpart['cta'];
			}
			$j = $j + 1;

		?>
		<tr<?php echo $class;?>>
			
			<td class="texto"><?php echo date('d/m/Y', strtotime($detpart['fecha']));?></td>		
			<td class="texto">
			<?php echo $html->link($detpart['numpartida'], array('controller'=> 'partidas', 'action'=>'view', $detpart['partida_id'])); ?>
			</td>
			<td class="texto"><?php echo $detpart['referencia'];?></td>
			<td class="texto"><?php echo $detpart['concepto'];?></td>
			<!-- <td class="texto"><?php echo $proys[$detpart['proy_id']];?></td> -->
			<td class="money">$&nbsp;<?php echo number_format($detpart['cargo'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detpart['abono'],2);?></td>
		</tr>
		<?php
		$vabonos = $vabonos + $detpart['abono'];
		$vcargos = $vcargos + $detpart['cargo'];
		//$vsubabono = $vsubabono + $detpart['abono'];
		//$vsubcargo = $vsubcargo + $detpart['cargo'];
		?>
	<?php endforeach; ?>
	<tr>
		<td colspan = 4><b>Subtotal por Cuenta Contable:&nbsp;<?php echo $vcta;?></b></td> 
		<!-- <td><b>TOTALES</b></td> -->
		
		<td><b>$&nbsp;<?php echo number_format($vsubcargo,2);?></b></td>
		<td><b>$&nbsp;<?php echo number_format($vsubabono,2);?></b></td>
	</tr>
	<tr>
		<td colspan=4><b>TOTALES</b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vcargos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vabonos,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>

</div>
