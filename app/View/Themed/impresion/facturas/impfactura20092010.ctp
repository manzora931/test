<div class="facturas view">
<h2><?php  __('Factura');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factura['Factura']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Organizacion'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factura['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $factura['Organizacion']['id'])); ?>
			<!-- <?php echo $factura['Factura']['organizacion_id']; ?> -->
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Socio de Negocio'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factura['Socionegocio']['nombre'], array('controller'=> 'socionegocios', 'action'=>'view', $factura['Socionegocio']['id'])); ?>
			<!-- <?php echo $factura['Factura']['socionegocio_id']; ?> -->
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('A nombre de'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factura['Factura']['anombrede']; ?>
			<!-- <?php echo $factura['Factura']['socionegocio_id']; ?> -->
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Tipo documento'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($factura['Tipofactura']['tipofactura'], array('controller'=> 'tipofacturas', 'action'=>'view', $factura['Tipofactura']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Fecha documento'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo date('d/m/Y', strtotime($factura['Factura']['fechadocto'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Observacion'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factura['Factura']['observacion']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Usuario'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $factura['Factura']['usuario']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Creado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo date('d/m/Y G:i:s', strtotime($factura['Factura']['created'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modificado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo date('d/m/Y G:i:s', strtotime($factura['Factura']['modified'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Subtotal Documento'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['subtotal'],2); ?></b>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('IVA:'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['impuesto'],2); ?></b>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Retencion (-):'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['retencion'],2); ?></b>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Total'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['total'],2); ?></b>
			&nbsp;
		</dd>
	</dl>
</div>


<div class="related">
	<h3><?php __('Detalle del Documento');?></h3>
	<?php if (!empty($factura['Detfactura'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Servicio'); ?></th>
		<th><?php __('Detalle'); ?></th>
		<th><?php __('Cantidad'); ?></th>
		<th><?php __('Valor Unitario'); ?></th>
		<th><?php __('Total'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vcantidad = 0; 
		$vsubtotal = 0; 
		$vdescuento = 0; 
		$vtotal = 0;
		foreach ($factura['Detfactura'] as $detfac):
			$class = null;
			//$producto = 'Madera';
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
				//$producto = 'Pegamento 3M';
			}
		?>
		<tr<?php echo $class;?>>
			<!-- <td><?php echo $detmovimiento['Productos']['producto'];?></td>  -->
			<td><?php echo $servicios[$detfac['servicio_id']];?></td>
			<td><?php echo $detfac['detalle'];?></td>
			<td><?php echo $detfac['cantidad'];?></td>
			<td>$&nbsp;<?php echo number_format($detfac['valorsinimp'],2);?></td>
			<td>$&nbsp;<?php echo number_format($detfac['cantidad']*$detfac['valorsinimp'],2);?></td>
		</tr>
		<?php
		$vcantidad = $vcantidad + $detfac['cantidad'];
		//$vsubtotal = $vsubtotal + $detfac['subtotal'];
		//$vdescuento = $vdescuento + $detfac['descuento'];
		$vtotal = $vtotal + ($detfac['cantidad']*$detfac['valorsinimp']);
		?>
	<?php endforeach; ?>
	<tr>
		<td>&nbsp;</td> 
		<td><b>TOTALES</b></td>
		<td><b><?php echo $vcantidad;?></b></td>
		<td>&nbsp;</td>
		<td><b>$&nbsp;<?php echo number_format($vtotal,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('Nuevo Detalle de Documento', true), array('controller'=> 'detfacturas', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>

