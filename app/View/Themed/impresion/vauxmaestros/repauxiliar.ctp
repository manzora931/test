<div class="vauxmaestros view">
<h2><?php  __('Detalle de Auxiliar por Cuenta por Mes');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Organizacion'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vauxmaestro['Organizacion']['organizacion']; ?>
			&nbsp;
		</dd>
			<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vauxmaestro['Proy']['proyecto']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Cuenta Contable'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b><?php echo $vauxmaestro['Vauxmaestro']['descripcion']?></b>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mes'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<b><?php echo $vauxmaestro['Vauxmaestro']['mes']."/".$vauxmaestro['Vauxmaestro']['anio']; ?></b>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Num. de registros'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vauxmaestro['Vauxmaestro']['num']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="related">
	<h3><?php __('Partidas en el Mes');?></h3>
	<?php if (!empty($vauxmaestro['Vauxiliare'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Partida'); ?></th>
		<th><?php __('Fecha'); ?></th>
		<th><?php __('Referencia'); ?></th>
		<th><?php __('Concepto'); ?></th>
		<th><?php __('Cargos'); ?></th>
		<th><?php __('Abonos'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vabonos = 0; 
		$vcargos = 0; 
		//$vdescuento = 0; 
		$vtotal = 0;
		foreach ($vauxmaestro['Vauxiliare'] as $detpart):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			
			<td class="texto">
			<!-- <?php echo $detpart['numpartida'];?> -->
			<?php echo $detpart['numpartida']?></td>
			<td class="texto"><?php echo date('d/m/Y', strtotime($detpart['fecha']));?></td>		
			<td class="texto"><?php echo $detpart['referencia'];?></td>
			<td class="texto"><?php echo $detpart['concepto'];?></td>
			<!-- <td class="texto"><?php echo $proys[$detpart['proy_id']];?></td> -->
			<td class="money">$&nbsp;<?php echo number_format($detpart['cargo'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detpart['abono'],2);?></td>
		</tr>
		<?php
		$vabonos = $vabonos + $detpart['abono'];
		$vcargos = $vcargos + $detpart['cargo'];
		?>
	<?php endforeach; ?>
	<tr>
		<td colspan=4><b>TOTALES</b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vcargos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vabonos,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>

</div>
