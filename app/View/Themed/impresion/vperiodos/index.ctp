<div class="vperiodos index">
<h2><?php __('Per�odos');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Pagina %page% de %pages%, %current% registros de un total de %count%, iniciando en el registro %start%, finalizando en el %end%', true)
));
?></p>

<div id="search_box">
	<form id="vperiodosAddForm" method="post" action="/sistemas/administracion/vperiodos">
		<fieldset style="display:none;"><input type="hidden" name="_method" value="POST" /></fieldset>
		<input name="data[vperiodos][search_text]" type="text" style="width: 150px;" value="<?php if($filtro) echo $filtro; ?>" id="vperiodosSearchText" />
		<input class="small_button" type="submit" value="Buscar" />
	</form>
</div>

<table cellpadding="0" cellspacing="0">
<tr>
	<!-- <th><?php echo $paginator->sort('id');?></th> -->
	<!-- <th><?php echo $paginator->sort('periodo_id');?></th> -->
	<th><?php echo $paginator->sort('organizacion_id');?></th>
	<th><?php echo $paginator->sort('proy_id');?></th>
	<th><?php echo $paginator->sort('anio');?></th>
	<th><?php echo $paginator->sort('mes');?></th>
	<th><?php echo $paginator->sort('estado');?></th>
	<th><?php echo $paginator->sort('mayorizado');?></th>
	<th><?php echo $paginator->sort('cerrado');?></th>
	<th class="actions"><?php __('Acciones');?></th>
</tr>
<?php
$i = 0;
foreach ($vperiodos as $vperiodo):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<!-- <td>
			<?php echo $vperiodo['Vperiodo']['id']; ?>
		</td> -->
		<!-- <td>
			<?php echo $vperiodo['Vperiodo']['periodo_id']; ?>
		</td> -->
		<td>
			<!-- <?php echo $vperiodo['Vperiodo']['organizacion_id']; ?> -->
			<?php echo $html->link($vperiodo['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $vperiodo['Organizacion']['id'])); ?>
		</td>
		<td>
			<!-- <?php echo $vperiodo['Vperiodo']['proy_id']; ?> -->
			<?php echo $html->link($vperiodo['Proy']['proyecto'], array('controller'=> 'proys', 'action'=>'view', $vperiodo['Proy']['id'])); ?>
		</td>
		<td>
			<?php echo $vperiodo['Vperiodo']['anio']; ?>
		</td>
		<td>
			<?php echo $vperiodo['Vperiodo']['mes']; ?>
		</td>
		<td>
			<?php echo $vperiodo['Vperiodo']['estado']; ?>
		</td>
		<td>
			<?php 
				if ($vperiodo['Vperiodo']['mayorizado'] == '0000-00-00 00:00:00') $vmayoriza = 'No';
				else $vmayoriza = $vperiodo['Vperiodo']['mayorizado'];
			?>
			<?php echo $vmayoriza; ?>
			<!-- <?php echo $vperiodo['Vperiodo']['mayorizado']; ?> -->
		</td>
		<td>
			<?php 
				if ($vperiodo['Vperiodo']['cerrado'] == '0000-00-00 00:00:00') $vcerrado = 'No';
				else $vcerrado = $vperiodo['Vperiodo']['cerrado'];
			?>
			<?php echo $vcerrado; ?>
			<!-- <?php echo $vperiodo['Vperiodo']['cerrado']; ?> -->
		</td>
		<td class="actions">
			<?php echo $html->link(__('Ver Balance', true), array('action'=>'view', $vperiodo['Vperiodo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previo', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

