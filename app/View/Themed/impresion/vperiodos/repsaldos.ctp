<div class="vperiodos view">
<h2><?php  __('Balance de Comprobación');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<!-- <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['id']; ?>
			&nbsp;
		</dd> -->
		<!-- <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Periodo Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['periodo_id']; ?>
			&nbsp;
		</dd> -->
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Organizacion'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<!-- <?php echo $vperiodo['Vperiodo']['organizacion_id']; ?> -->
			<?php echo $html->link($vperiodo['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $vperiodo['Organizacion']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Proyecto'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<!-- <?php echo $vperiodo['Vperiodo']['proy_id']; ?> -->
			<?php echo $html->link($vperiodo['Proy']['proyecto'], array('controller'=> 'proys', 'action'=>'view', $vperiodo['Proy']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mes'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['anio']."/".$vperiodo['Vperiodo']['mes']; ?>
			&nbsp;
		</dd>
		<!-- <dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mes'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['mes']; ?>
			&nbsp;
		</dd> -->
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Estado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['estado']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mayorizado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['mayorizado']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Cerrado'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $vperiodo['Vperiodo']['cerrado']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="related">
	<h3><?php __('Cuentas Contables');?></h3>
	<?php if (!empty($vperiodo['Vsaldo'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Cuenta'); ?></th>
		<th><?php __('Descripción'); ?></th>
		<th><?php __('Saldo Anterior'); ?></th>
		<th><?php __('Cargos'); ?></th>
		<th><?php __('Abonos'); ?></th>
		<th><?php __('Nuevo Saldo'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vabonos = 0; 
		$vcargos = 0; 
		$vsaldo = 0;
		$vtotsaldo = 0;
		$vsalant = 0;
		//$vdescuento = 0; 
		$vtotal = 0;
		foreach ($vperiodo['Vsaldo'] as $detsaldo):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<?php
		$vsalant = $vsalant + $detsaldo['saldoanterior'];
		$vabonos = $vabonos + $detsaldo['abono'];
		$vcargos = $vcargos + $detsaldo['cargo'];
		$vsaldo = $detsaldo['saldoanterior'] + $detsaldo['cargo'] - $detsaldo['abono'];
		$vtotsaldo = $vtotsaldo + $vsaldo;
		?>
		<tr<?php echo $class;?>>
			
			<td class="texto">
			<!-- <?php echo $detpart['numpartida'];?> -->
			<?php echo $detsaldo['cta'];?>
			<!-- <?php echo $html->link($detsaldo['cta'], array('controller'=> 'cuentas', 'action'=>'view', $detsaldo['vcuenta_id'])); ?> -->
			</td>
			<td class="texto"><?php echo $detsaldo['cuenta'];?></td>
			<td class="money">$&nbsp;<?php echo number_format($detsaldo['saldoanterior'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detsaldo['cargo'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detsaldo['abono'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($vsaldo,2);?></td>
		</tr>
		
	<?php endforeach; ?>
	<tr>
		<td colspan=2><b>TOTALES</b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vsalant,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vcargos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vabonos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vtotsaldo,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>

</div>
