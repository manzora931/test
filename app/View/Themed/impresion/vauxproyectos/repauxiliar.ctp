<div class="vauxproyectos view">
	<center><?php echo $vauxproyecto['Organizacion']['organizacion']; ?></center>
	<div><span><?php echo "Fuente: vauxproyectos";?></span><span>Auxiliar por Centro de Costo por Cuenta Mensual</span><span>Pagina: 1</span></div>
	<dl><?php $i = 0; //$class = ' class="altrow"';?>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Id'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($vauxproyecto['Vauxproyecto']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Organizaci&oacute;n'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo $this->html->link($vauxproyecto['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $vauxproyecto['Organizacion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Centro de Costo'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<!-- <?php echo $vauxproyecto['Vauxproyecto']['proyecto']; ?> -->
			<?php echo $this->html->link($vauxproyecto['Proyecto']['proyecto'], array('controller'=> 'proyectos', 'action'=>'view', $vauxproyecto['Proyecto']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Cuenta Contable'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<!-- <?php echo $vauxproyecto['Vauxproyecto']['descripcion']; ?> -->
			<?php echo $this->html->link($vauxproyecto['Vauxproyecto']['descripcion'], array('controller'=> 'cuentas', 'action'=>'view', $vauxproyecto['Vauxproyecto']['vcuenta_id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Mes'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($vauxproyecto['Vauxproyecto']['mes']."/".$vauxproyecto['Vauxproyecto']['anio']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Num. de registros'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($vauxproyecto['Vauxproyecto']['num']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php __('Partidas en el Mes');?></h3>
	<?php if (!empty($vauxproyecto['Vdetauxproyecto'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('No. Partida'); ?></th>
		<th><?php echo __('Fecha'); ?></th>
		<th><?php echo __('Referencia'); ?></th>
		<th><?php echo __('Concepto'); ?></th>
		<th><?php echo __('Cargos'); ?></th>
		<th><?php echo __('Abonos'); ?></th>
	</tr>
	<?php
		$i = 0;
		$vabonos = 0; 
		$vcargos = 0; 
		//$vdescuento = 0; 
		$vtotal = 0;
		foreach ($vauxproyecto['Vdetauxproyecto'] as $detpart):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			
			<td class="texto">
			<!-- <?php echo $detpart['numpartida'];?> -->
			<?php echo $this->html->link($detpart['numpartida'], array('controller'=> 'partidas', 'action'=>'view', $detpart['partida_id'])); ?>
			</td>

			<td class="texto"><?php echo date('d/m/Y', strtotime($detpart['fecha']));?></td>		
			<td class="texto"><?php echo $detpart['referencia'];?></td>
			<td class="texto"><?php echo $detpart['concepto'];?></td>
			<!-- <td class="texto"><?php echo $proys[$detpart['proy_id']];?></td> -->
			<td class="money">$&nbsp;<?php echo number_format($detpart['cargo'],2);?></td>
			<td class="money">$&nbsp;<?php echo number_format($detpart['abono'],2);?></td>
		</tr>
		<?php
		$vabonos = $vabonos + $detpart['abono'];
		$vcargos = $vcargos + $detpart['cargo'];
		?>
	<?php endforeach; ?>
	<tr>
		<td colspan=4><b>TOTALES</b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vcargos,2);?></b></td>
		<td class="money"><b>$&nbsp;<?php echo number_format($vabonos,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>
</div>
