<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="tipoproyectos form container">
    <?php echo $this->Form->create('Evento', array('id'=>'formulario','enctype'=>"multipart/form-data")); ?>
    <fieldset>
        <legend><?php echo __('Actualizar Evento'); ?></legend>
        <div class="alert alert-danger col-xs-12" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('id'); ?>
            <?php
            echo $this->Form->input('evento',
                array('label' => 'Nombre',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'required'=>'required'));

            echo $this->Form->input('img',
                array('label' => 'Imagen',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'type' => 'file'
                ));

            echo $this->Form->input('descripcion',
                array('label' => 'Descripción',
                    'class' => 'form-control',
                    'rows' => 3,
                    'div' => ['class'=>'form-group']
                ));
            ?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

    </fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Eventos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        // Verifica si tiene deteventos relacionados
        // al darle  click al checkbox del campo activo
        $( "#EventoActivo" ).on( "change", function() {
            var url = "<?= Router::url(array('controller' => 'eventos', 'action' => 'verify_eventos')); ?>";
            var id = $('#EventoId').val();

            if(!$(this).is( ":checked" )) {
                $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id: id},
                    dataType: 'json',
                    success: function (response) {
                        if(response.tipos) {
                            $( "#TipoproyectoActivo" ).prop('checked', true);

                            $("#alerta .message").text('No se puede desactivar, tiene detalle de eventos relacionados');
                            $("#alerta").slideDown();

                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                        }
                    }
                });
            }
        });
    });
</script>