<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="eventos form container">
    <?php echo $this->Form->create('Evento', array('id'=>'formulario','enctype'=>"multipart/form-data")); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Evento'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('evento',
                array('label' => 'Nombre',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));

            echo $this->Form->input('img',
                array('label' => 'Imagen',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'type' => 'file'
                ));
            echo $this->Form->input('descripcion',
                array('label' => 'Descripción',
                    'class' => 'form-control',
                    'rows' => 3,
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                ));
            ?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

    </fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Eventos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
</div>