<style>
	.btn-default{
		font-family: 'Calibri', sans-serif;
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border: 1px solid #606060	;
		font-size: 14px;
		color: #000;
		border-radius: 1px;

	}
	.btn-default:hover{
		margin-left: 15px;
		background-color: #c0c0c0 ;
		border:1px solid #606060 ;
		color: #000;
		border-radius: 1px;
	}
	div.view{
		margin-top:15px;
		width: 100%;
	}
	table#tblview tr td:first-child{
		width: 200px;
		text-align: left;
		font-weight: bold;
	}
	h2{
		color:#cb071a;
		font-size: 1.4em;
	}
	div.actions{
		display: inline-block;
	}
	div.actions div > a{
		padding: 0 5px 0 5px;
		text-decoration: none;
	}

	div.actions>div{
		display: inline-block;
	}
	table#tblview tr td:first-child {
		width: 200px;
		text-align: left;
		font-weight: bold;
		color: #011880;
	}
	table#tblview tr td:last-child{
		width: 400px;
		text-align: left;

	}

</style>
<div class="recursos view container-fluid">
<h2><?php echo __('Recurso'); ?></h2>
	<?php
	if(isset($_SESSION['recur_save'])){
		if($_SESSION['recur_save']==1){
			unset($_SESSION['recur_save']);
			?>
			<div class="alert alert-success " id="alerta" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Recurso almacenado
			</div>
		<?php		}
	}
	?>

	<table id="tblview">
		<tr>
			<td><strong>Id</strong></td><td><?php echo h($recurso['Recurso']['id']); ?></td>
		</tr>
		<tr>
			<td><strong>M&oacute;dulo</strong></td><td><?php echo h($recurso['Modulo']['modulo']); ?></td>
		</tr>
		<tr>
			<td><strong>Modelo de sistema</strong></td><td><?php echo h($recurso['Recurso']['modelo']); ?></td>
		</tr>
		<tr>
			<td><strong>Nombre a desplegar</strong></td><td><?php echo h($recurso['Recurso']['nombre']); ?></td>
		</tr>
		<tr>
			<td><strong>Tipo</strong></td><td><?php echo h($recurso['Recurso']['tipo']); ?></td>
		</tr>
		<tr>
			<td><strong>Ubicación</strong></td><td><?php echo h($recurso['Recurso']['ubicacion']); ?></td>
		</tr>
		<tr>
			<td><strong>Activo</strong></td><td><?php
				$vactivo = 'No';
				if ($recurso['Recurso']['activo']) $vactivo = 'Si';
				?>
				<?php echo $vactivo; ?></td>
		</tr>
		<tr>
			<td><strong>Creado</strong></td><td><?php echo h($recurso['Recurso']['usuario']); ?>
				<?php echo "(".h($recurso['Recurso']['created']); echo ")"; ?></td>
		</tr>
		<tr>
			<td><strong>Modificado</strong></td><td>
				<?php
					if(isset($recurso['Recurso']['usuariomodif'])){
						echo h($recurso['Recurso']['usuariomodif']);
						echo "(".h($recurso['Recurso']['modified']); echo ")";
					}
				?>
			</td>
		</tr>
	</table>

	<div class="actions">
			<div><?php echo $this->Html->link(__('Listado de Recursos'), array('action' => 'index'), array('class' => 'btn btn-default')); ?></div>
			<div><?php echo $this->Html->link(__('Editar Recurso'), array('action' => 'edit', $recurso['Recurso']['id']), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>

<?php
    if(count($detrecursos)>0){
?>
<div class="related">
    <h3><?= __('Detalles del Recurso - Botones');?></h3>
    <table style="width: 50%;">
        <tr>
            <th>Nombre a mostrar</th>
            <th>Funci&oacute;n</th>
            <th>Activo</th>
        </tr>
        <?php
           foreach($detrecursos as $k => $v)
           {
        ?>
               <tr>
                   <td><?= $v['detrecursos']['nombre'] ?></td>
                   <td><?= $v['detrecursos']['funcion'] ?></td>
                   <td><?php if($v['detrecursos']['activo'] == 1) echo "SI"; else echo "NO"; ?></td>
               </tr>
        <?php
           }
        ?>
    </table>

<?php }
    if(count($detrecursofirmas)>0){?>
    <h3><?= __('Firmas');?></h3>
    <table style="width: 75%;">
        <tr>
            <th>Firma</th>
            <th>Función</th>
            <th>Orden</th>
            <th>Permitir</th>
        </tr>
<?php
    foreach($detrecursofirmas as $d => $row){
        echo "<tr>";
        echo "<td>".$firmas[$row['detrecursofirmas']['firma_id']]."</td>";
        echo "<td>".$row['detrecursofirmas']['funcion']."</td>";
        echo "<td>".$row['detrecursofirmas']['orden']."</td>";
        $perm = ($row['detrecursofirmas']['permitir']==1)?"Si":"No";
        echo "<td>".$perm."</td>";
        echo "</tr>";
    }
    echo "</table>";
    } ?>
</div>
<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>

