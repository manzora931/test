<?php $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>

<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="recursos form container-fluid">
    <?php echo $this->Form->create('Recurso',array('id'=>'recur')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Recurso'); ?></legend>
        <?php
        echo $this->Form->input('organizacion_id');
        echo $this->Form->input('modulo_id', array('label'=>'Módulo','empty' => '-- Seleccionar --','class' => "validate[required]",'required'));
        echo $this->Form->input('modelo', array('label'=>'Modelo en Sistema','class' => "validate[required]",'required'));
        echo $this->Form->input('nombre', array('label'=>'Nombre a Desplegar','class' => "validate[required]",'required'));
        echo $this->Form->input('tipo', array('label'=>'Tipo','empty'=>'-- Seleccionar --','options'=>array('pantalla'=>'pantalla', 'informe'=>'informe'),'class' => "validate[required]",'required'));
        echo $this->Form->input('ubicacion', array('empty'=>'-- Seleccionar --','options'=>array('Menu'=>'Menu', 'Admin'=>'Admin'),'class' => "validate[required]",'required'   ));
        echo "<br>";
        echo  $this->Form->input('activo', array('label' => 'Activo','div'=>false,'style'=>'margin:0; margin-right:4px;'));
        ?>
    </fieldset>

    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Recursos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    var conta=3;
    var canS;
    var genChangeNumber;

    canS=document.getElementsByClassName("detRecursos");
    genChangeNumber=canS.length;
    var nOpt=canS.length;
    var cantidad=document.getElementById("mas");
    var mas=document.getElementById("cantidadMas");
    cantidad.addEventListener("click", function (e){
        e.preventDefault();

        var s= $("#detaTabla");

        nOpt= parseInt(canS.length)+parseInt(mas.value);

        var trd;
        var tds1;
        var tds2;
        var tds3;
        var tds4;

        var c=0;
        for (var i = 1; i <= mas.value; i++) {

            var tr  = $('<tr />');
            var td1 = $('<td />');
            var td2 = $('<td />');
            var td3 = $('<td />',{'style':'text-align:center'});
            var ct  = $('<center />');

            var con     = $("<input />", {'name':'data[Detrecurso][nombre]['+(i+conta)+']', 'class':'detRecursos', 'type':'text', 'id':'DetrecursoNombre'+(i+conta)});
            var ref     = $("<input />", {'name':'data[Detrecurso][funcion]['+(i+conta)+']',  'type':'text', 'id':'DetrecursoFuncion'+(i+conta)});
            var cargh    = $("<input />", {'name':'data[Detrecurso][activo]['+(i+conta)+']',  'type':'hidden', 'id':'DetrecursoActivo'+(i+conta)+'_', 'value':'0'});
            var carg    = $("<input />", {'name':'data[Detrecurso][activo]['+(i+conta)+']',  'type':'checkbox', 'id':'DetrecursoActivo'+(i+conta), 'style':'float: none;', 'value':'1'});
            var abo     = $("<a />", {'href':'#','class':'eliminar'+(i+conta)+']', 'id':'Detpartidalimpiar'+(i+conta), 'style':'float: none;'});

            td1.append(con);
            td2.append(ref);
            td3.append(cargh);
            td3.append(carg);

            tr.append(td1);
            tr.append(td2);
            tr.append(td3);

            trd=s.find('tbody').append(tr);
            c++;
        };
        conta=conta+c;
        canS=document.getElementsByClassName("detPartida");
        genChangeNumber=canS.length;
    });
    /*--------------------------------------------------------------------------*/
    var conta2=3;
    var canS2;
    var genChangeNumber2;

    canS2=document.getElementsByClassName("detFirmas");
    genChangeNumber2=canS2.length;
    var nOpt2=canS2.length;
    var cantidad2=document.getElementById("mas2");
    var mas2=document.getElementById("cantidadMas2");
    cantidad2.addEventListener("click", function (e){
        e.preventDefault();

        var s= $("#detaTabla2");

        nOpt2= parseInt(canS2.length)+parseInt(mas2.value);

        var trd;
        var tds1;
        var tds2;
        var tds3;
        var tds4;

        var c=0;
        for (var i = 1; i <= mas2.value; i++) {

            var tr  = $('<tr />');
            var td1 = $('<td />');
            var td2 = $('<td />');
            var td3 = $('<td />');
            var td4 = $('<td />',{'style':'text-align:center'});
            var td5 = $('<td />',{'style':'text-align:center'});
            var ct  = $('<center />');
            var clone_firm = document.getElementById('Detrecursofirmas0FirmaId').cloneNode(true);
            clone_firm.setAttribute('name','data[Detrecursofirmas]['+(i+conta2)+'][firma_id]');
            clone_firm.setAttribute('id','Detrecursofirmas'+(i+conta2)+'FirmaId');
            clone_firm.value='';
            var funcion     = $("<input />", {'name':'data[Detrecursofirmas]['+(i+conta2)+'][funcion]',  'type':'text', 'id':'Detrecursofirmas'+(i+conta2)+'Funcion'});


            var orden = document.getElementById('Detrecursofirmas0Orden').cloneNode(false);
            orden.setAttribute('name','data[Detrecursofirmas]['+(conta2+i)+'][orden]');
            orden.setAttribute('id','Detrecursofirmas'+(conta2+i)+'Orden');
            orden.value=0;
            var activo = document.getElementById('Detrecursofirmas0Permitir').cloneNode(false);
            activo.setAttribute('name','data[Detrecursofirmas]['+(i+conta2)+'][permitir]');
            activo.setAttribute('id','Detrecursofirmas'+(i+conta2)+'Permitir');
            activo.setAttribute('style','float: none;');
            var limp     = $("<a />", {'href':'#','class':'eliminar'+(i+conta2)+']', 'id':'Detpartidalimpiar'+(i+conta2), 'style':'float: none;'});

            td1.append(clone_firm);
            td2.append(funcion);
            td3.append(orden);
            td4.append(activo);
            td5.append(limp);

            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);

            trd=s.find('tbody').append(tr);
            c++;
        };
        conta2=conta2+c;
        canS2=document.getElementsByClassName("detFirmas");
        genChangeNumber2=canS2.length;
    });

    jQuery(function(){
        var validate = {
            "id" : "#RecursoModelo",
            "form" : "#recur",
            "message" : "El modelo ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;

            var url = getURL() + 'val_model';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_model';
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val: val, id: id},
                cache: false,
                success: function(resp) {
                    if ( resp == "error" ) {
                        $(validate.id).validationEngine("showPrompt", validate.message,"AlertText");
                        $(validate.id).val("");
                    } else {
                        form.submit();
                    }
                }
            });
        }

    });
    jQuery(function(){
        var validate = {
            "id" : "#RecursoNombre",
            "form" : "#recur",
            "message" : "El nombre a desplegar  ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_name';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_name';
            var form = this;

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            $.ajax({
                url: url,
                type: 'post',
                data: { val: val, id: id},
                cache: false,

                success: function(resp) {
                    if ( resp == "error" ) {
                        $(validate.id).validationEngine("showPrompt", validate.message,"AlertText");
                        $(validate.id).val("");
                    } else {

                    }
                }
            });
        }

    });

</script>
