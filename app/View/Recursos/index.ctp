<?php //echo $this->Html->Script('buscar',FALSE); ?>
<?php //echo $this->Html->Script('jquery',FALSE); ?>
<?php //echo $this->Html->Script('jquery-ui.js',FALSE); ?>
<?php //echo $this->html->Script('url'); ?>
<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
	<h2><?php echo __('Recursos'); ?></h2>
	<p>
	<?php
	echo $this->paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	    ));
	?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "recursos";
	?>
	<div id='search_box' class="table-responsive">
	<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Nombre, Modelo', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre, Modelo'));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('modulo_id',array('label'=>'Módulo:', 'name'=> 'data['.$tabla.'][modulo_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' class='checkbox'>";
		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<input style='margin-left: 8px' type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked' id='ac'><label for='ac'>Inactivos </label>";
		} else {
			echo "<input  style='margin-left: 8px'  type='checkbox' name='data[$tabla][activo]' value='0' id='ac'><label for='ac'>Inactivos </label>";
		}
		echo "</td>";

		echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
		echo $this->Form->hidden('Controller', array('value'=>'Recurso', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'modelo,nombre', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";		
		echo "</table>";
	?>

	<?php
	if(isset($_SESSION["$tabla"]))
	{
		$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
		echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
	}
	?>
	</div>

	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre', 'Nombre a Desplegar'); ?></th>
			<th><?php echo $this->Paginator->sort('modulo_id', 'Módulo'); ?></th>
			<th><?php echo $this->Paginator->sort('modelo', 'Modelo de Sistema'); ?></th>
			<th><?php echo $this->Paginator->sort('tipo'); ?></th>
			<th><?php echo $this->Paginator->sort('ubicacion', 'Ubicación'); ?></th>
			<th><?php echo $this->Paginator->sort('activo','Activo');?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($recursos as $recurso):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?php echo h($recurso['Recurso']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?php echo h($recurso['Recurso']['nombre']); ?>&nbsp;</td>
				<td style='text-align:left';><?php echo $this->html->link($recurso['Modulo']['modulo'], array('controller'=> 'modulos', 'action'=>'view', $recurso['Recurso']['modulo_id'])); ?></td>
				<td style='text-align:left';><?php echo h($recurso['Recurso']['modelo']); ?>&nbsp;</td>
				<td><?php echo h($recurso['Recurso']['tipo']); ?>&nbsp;</td>
				<td><?php echo h($recurso['Recurso']['ubicacion']); ?>&nbsp;</td>
				<td>
					<?php if( $recurso['Recurso']['activo'] == 1){
						echo "Si";
					}elseif ($recurso['Recurso']['activo'] == 0) {
						echo "No";
					}
					?>
				</td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $recurso['Recurso']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $recurso['Recurso']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<div><?php echo $this->Html->link(__('Nuevo Recurso'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>

