<div class="socionegocios view">
<h2><?php  echo __('Socio de Negocio');?></h2>
	<dl><?php $i = 0;// $class = ' class="altrow"';?>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Id'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Organizaci&oacute;n'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo $this->Html->link($socionegocio['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $socionegocio['Organizacion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('C&oacute;digo'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<b><?php echo h($socionegocio['Socionegocio']['codigo']); ?></b>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Razón Social'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<b><?php echo h($socionegocio['Socionegocio']['nombre']); ?></b>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Nombre Comercial'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['nombcomercial']); ?>
			&nbsp;
		</dd>
        <dt><?php if ($i % 2 == 0) //echo $class;?>
            <?php echo __('Lista de Precios'); ?></dt>
        <dd><?php if ($i++ % 2 == 0) //echo $class;
                ?>
            <?php echo $this->Html->link($socionegocio['Listaprecio']['nombrelista'], array('controller'=> 'listaprecios', 'action'=>'view', $socionegocio['Listaprecio']['id'])); ?>
            &nbsp;
        </dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Reg. de IVA'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['regiva']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('NIT'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['nit']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Giro'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['giro']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Tipo Persona'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['persona']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Categ. de Socio de Negocio'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo $this->Html->link($socionegocio['Catsocionegocio']['catsocionegocio'], array('controller'=> 'catsocionegocios', 'action'=>'view', $socionegocio['Catsocionegocio']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Num Referencia'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['num_referencia']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Tipo de Contribuyente'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Tiposocionegocio']['tiposocionegocio']); ?>
			&nbsp;
		</dd>
		<!-- <dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Gran Contribuyente'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['grancontribuyente']); ?>
			&nbsp;
		</dd> -->
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Direcci&oacute;n'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['direccion']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Pa&iacute;s'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo $this->Html->link($socionegocio['Paise']['pais'], array('controller'=> 'paises', 'action'=>'view', $socionegocio['Paise']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Teléfonos PBX'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Skype'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['skyp']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Fax'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('TeleFax'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['telefax']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Correo Electrónico'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['correo']); ?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Activo'); ?></dt>
			<?php 
			  if ($socionegocio['Socionegocio']['activo'] == 1) $vactivo = 'Si';
			  else $vactivo = 'No';
			 ?>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo $vactivo; ?>
			&nbsp;
		</dd>
		<h4>Contactos</h4>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Contacto'); ?></th>
				<th><?php echo __('Tel&eacute;fono'); ?></th>
				<th><?php echo __('Skype'); ?></th>
				<th><?php echo __('Correo Electrónico'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($socionegocio['Socionegocio']['contacto']);?></td> 
				<td><?php echo h($socionegocio['Socionegocio']['telefono']);?></td>			
				<td><?php echo h($socionegocio['Socionegocio']['skype']);?></td> 
				<td><?php echo h($socionegocio['Socionegocio']['correocontacto']);?></td>	
			</tr>
			<tr>
				<td><?php echo h($socionegocio['Socionegocio']['contacto2']);?></td> 
				<td><?php echo h($socionegocio['Socionegocio']['telefono2']);?></td>			
				<td><?php echo h($socionegocio['Socionegocio']['skype2']);?></td> 
				<td><?php echo h($socionegocio['Socionegocio']['correocontacto2']);?></td>	
			</tr>
		</table> 
		
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Creado'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['created']);?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Modificado'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['modified']);?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Usuario'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['usuario']);?>
			&nbsp;
		</dd>
		<dt><?php if ($i % 2 == 0) //echo $class;?>
		<?php echo __('Usuario modificó'); ?></dt>
		<dd><?php if ($i++ % 2 == 0) //echo $class;?>
			<?php echo h($socionegocio['Socionegocio']['usuario_modif']);?>
			&nbsp;
		</dd>
	</dl>
</div>
<br>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Editar Socio de Negocio', true), array('action'=>'edit', $socionegocio['Socionegocio']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Listado de Socios de Negocios', true), array('action'=>'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Crear Socio de Negocio', true), array('action'=>'add')); ?> </li>
		<!--<li><?php /* echo $this->Html->link(__('Listado Categoria Socio de Negocios', true), array('controller'=> 'catsocionegocios', 'action'=>'index')); */ ?> </li>-->
		<li><A HREF="javascript:javascript:history.go(-1)">Regresar</A></li>
	</ul>
</div>
<div class="related">
	<br>
	<h3><?php echo __('Movimientos Relacionados');?></h3>
	<?php if (!empty($socionegocio['Movimiento'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Num Docto'); ?></th>
		<th><?php echo __('Catmovimiento Id'); ?></th>
		<th><?php echo __('Fecha Docto'); ?></th>
		<th><?php echo __('Observaciones'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Detalle'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($socionegocio['Movimiento'] as $movimiento):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr>
			<td><?php echo $movimiento['num_docto'];?></td>
			<td><?php echo $movimiento['catmovimiento_id'];?></td>
			<td><?php echo $movimiento['fecha_docto'];?></td>
			<td><?php echo $movimiento['observaciones'];?></td>
			<td><?php echo $movimiento['status'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__(' ', true), array('controller'=> 'movimientos', 'action'=>'view', $movimiento['id']),array('class'=>'ver')); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
