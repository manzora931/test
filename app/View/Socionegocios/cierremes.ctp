<div class="invperiodos form">
<?php echo $this->Form->create('Invperiodo'); ?>
	<fieldset>
		<legend><?php echo __('Cierre mes'); ?></legend>
	<table>
	<tr>
	<td> 
			<label for="InvperiodoBodegaId">Tipo</label>
			<select name="data[Invperiodo][bodega_id]" id="InvperiodoBodegaId" class="validate[required]">
				<option value="">Seleccione un tipo</option>
				<?php
					foreach ($bodegas as $key => $value) {
						foreach ($value as $key2 => $value2) {
							foreach ($value2 as $key3 => $value3) {
								if ($key3=="id") {
									echo "<option value='$value3'>";
								}else echo $value3."</option>";
							}
						}
					}
				?>
			</select>
	<td>
	</tr>
	<?php
		echo "<tr>";
		echo "<td>";
		echo $this->Form->input('observacion');
			echo "</td>";
		echo "</tr>";
	?>
	</fieldset>
<?php echo $this->Form->end(__('Aceptar')); ?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('List Invperiodos'), array('action' => 'index')); ?></li>
		</ul>
</div>