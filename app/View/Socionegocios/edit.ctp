<?php
$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="socionegocios form">
<?php echo $this->Form->create('Socionegocio');
echo $this->Html->Script('jquery-ui.js',FALSE);
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es');
echo $this->Html->script('socionegocios/requeridos_socioneg');
echo $this->Html->script('jquery.validationEngine');
echo $this->Html->script('socionegocios/mask_socioneg');
echo $this->Html->script('socionegocios/unicos_socioneg');
echo $this->Html->Script('jquery.maskedinput.js',FALSE);
echo $this->Html->script('fomatNumero');

?>
<script type="text/javascript">
	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#SocionegocioEditForm").validationEngine();
		});
</script>

	<fieldset>
 		<legend><?php echo __('Editar Socio de Negocio');?></legend>
	<?php
		echo $this->Form->input('id');

	?>
	<table>
	  <tr>
		<td> <?php echo $this->Form->input('organizacion_id',array('label' => 'Organicaci&oacute;n','div' => false,'class' => "validate[required]",'required','empty'=>'--Seleccionar--')); ?></td>
		<td> <?php echo $this->Form->input('codigo',array('label' => 'C&oacute;digo','div' => false,'class' => "validate[required],text-input",'required','onBlur'=>'val_cod();')); ?></td>
		<td> <?php echo $this->Form->input('catsocionegocio_id', array( 'label' => 'Categor&iacute;a de Socio de Negocio', 'div' => false,'class' => "validate[required]",'required','empty'=>'--Seleccionar--')); ?></td>
		<td> <?php echo $this->Form->input('activo', array('options' => array(1=>'Si', 0=>'No'), 'div' => false)); ?></td>
	  </tr>
	</table>
	
	<table>
		<tr>
            <td><?php echo $this->Form->input('listaprecio_id',array('label' => 'Lista de Precios', 'div' => false,'empty'=>'Seleccionar lista de precio')); ?></td>
			<td><?php echo $this->Form->input('tipofactura_id',array('label' => 'Tipo de facturas', 'div' => false,'class' => "validate[required]",'empty'=>'--Seleccionar--','required')); ?></td>
		</tr>
		<tr>
			<td colspan="2"><?php echo $this->Form->input('nombre', array('label' => 'Razon Social', 'div' => false,'class' => "validate[required],text-input",'required'));

			?></td>

		</tr>
		<tr>
			<td colspan="2"><?php echo $this->Form->input('nombcomercial',array('label' => 'Nombre Comercial', 'div' => false,'class' => "validate[required],text-input",'required')); ?></td>
		</tr>
	</table>
		
	  <table>
		<tr>
		<td> <?php echo $this->Form->input('regiva', array( 'label' => 'Reg. de IVA', 'div' => false,'id'=>'regiva')); ?></td>
		<td> <?php echo $this->Form->input('nit',array('label' => 'NIT','div' => false,'id'=>'nit','onclick'=>"formatonit('nit')",'onfocus'=>"formatnit('nit')")); ?></td>
		<td> 
			<label for="SocionegocioTiposocionegocioId">Tipo de Socio de Negocio</label>
			<select name="data[Socionegocio][tiposocionegocio_id]" id="SocionegocioTiposocionegocioId" class="validate[required]" required>
				<option value="">--Seleccionar--</option>
				<?php
					foreach ($tiposocionegocios as $key => $value) {
						foreach ($value as $key2 => $value2) {
							foreach ($value2 as $key3 => $value3) {
								if ($key3=="id") {
									if ($value3==$socionegocios[0]['socionegocios']['tiposocionegocio_id']) {
										echo "<option value='$value3' selected='selected'>";
									}else{
										echo "<option value='$value3'>";
									}
								}else echo $value3."</option>";
							}
						}
					}
				?>
			</select>
		</td>
		<tr>
		<td> <?php echo $this->Form->input('giro', array( 'label' => 'Giro', 'div' => false,'id'=>'giro')); ?></td>
		<td> <?php echo $this->Form->input('num_referencia',array('label' => 'N&uacute;mero Referencia', 'div' => false)); ?></td>
		<!-- <td> <?php echo $this->Form->input('grancontribuyente',array('label' => 'Gran Contribuyente')); ?></td> -->
		<td>
				<label for="SocionegocioPersona">Persona</label>
				<select name="data[Socionegocio][persona]" id="SocionegocioPersona" class="validate[required]" required>
					<option value="">--Seleccionar--</option>
					<?php
						$persona=$socionegocios[0]['socionegocios']['persona'];
						if ($persona=="Natural") {
					?>
							<option value="Natural" selected="selected">Natural</option>
							<option value="Juridica">Jur&iacute;dica</option>
					<?php
						}elseif ($persona=="Juridica") {
					?>
							<option value="Natural">Natural</option>
							<option value="Juridica" selected="selected">Jur&iacutedica</option>
					<?php
						}else
						{
					?>
						<option value="Natural">Natural</option>
						<option value="Juridica">Jur&iacutedica</option>
					<?php
						}
					?>
					
				</select>
			</td>
		</tr>
		
		<tr>
		<td> <?php echo $this->Form->input('dui', array( 'label' => 'DUI', 'div' => false,'id'=>'dui','onclick'=>"formatodui('dui')",'onfocus'=>"formatdui('dui')")); ?></td>
		<td> <?php echo $this->Form->input('pasaporte',array('label' => 'Pasaporte', 'div' => false)); ?></td>
		<td> <?php echo $this->Form->input('otroid', array('label' => 'Otro ID', 'div' => false)); ?></td>
		</tr>
	  </table>

	<table>
	  <tr>
	  	
		<td width=70%> <?php echo $this->Form->input('direccion',array('label' => 'Direcci&oacute;n', 'cols' => '3', 'div' => false)); ?>
		<td> <label for="SocionegocioPaiseId">Pa&iacute;s</label>
			<select name="data[Socionegocio][paise_id]" id="SocionegocioPaiseId"  class="validate[required]", required>
				<option value="">--Seleccionar--</option>
				<?php
					
					foreach ($paises as $key => $value) {
						if($socionegocios[0]['socionegocios']['paise_id']==$key)
						{
							echo "<option selected='selected' value='$key'>$value</option>";
						}else
						{
							echo "<option value='$key'>$value</option>";
						}
								
					}
						
					
				?>
			</select>
			 
			 <label for="SocionegocioDepartamentoId">Departamento</label>
			<select name="data[Socionegocio][departamento_id]" id="SocionegocioDepartamentoId">
				<option value="">--Seleccionar--</option>
				<?php
					foreach ($departamentos as $key => $value) {
						foreach ($value as $key2 => $value2) {
								if ($value2["paise_id"]==$socionegocios[0]['socionegocios']['paise_id']) {
									if($socionegocios[0]['socionegocios']['departamento_id']==$value2["id"])
									{
										echo "<option selected='selected' value='$value2[id]'>";
									}else{
										echo "<option value='$value2[id]'>";
									}
										echo $value2["nombre"]."</option>";
									}
								
							
						}
					}
				?>
			</select>
			<label for="SocionegocioMunicipioId">Municipio</label>
			<select name="data[Socionegocio][municipio_id]" id="SocionegocioMunicipioId">
				<option value="">--Seleccionar--</option>
				<?php
					foreach ($municipios as $key => $value) {
						foreach ($value as $key2 => $value2) {
							if ($value2["departamento_id"]==$socionegocios[0]['socionegocios']['departamento_id']) {
								if($socionegocios[0]['socionegocios']['municipio_id']==$value2["id"])
								{echo "<option selected='selected' value='$value2[id]'>";}else{
									echo "<option value='$value2[id]'>";}
									echo $value2["nombre"]."</option>";
							}
								
							
						}
					}
				?>
			</select>
			
		</td>
	  </tr>
	</table>
	<table>
	  <tr>
		<td> <?php echo $this->Form->input('correo',array('label' => 'Correo Electr&oacute;nico', 'div' => false,'class'=>"validate[custom[email]]",'type'=>'email')); ?></td>
		<td> <?php echo $this->Form->input('tel', array( 'label' => 'Tel&eacute;fonos PBX', 'div' => false,'required','class'=>"validate[required]",'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono()")); ?></td>
		<td> <?php echo $this->Form->input('skyp', array( 'label' => 'Skype', 'div' => false)); ?></td>
	  </tr>

	  </tr>
	  <tr>
		<td><?php echo $this->Form->input('fax',array('label' => 'Fax', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?>
			</td>
		<td> <?php echo $this->Form->input('telefax', array( 'label' => 'Telefax', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?>
		</td>
	  </tr>
	</table>
	<h3>Contactos</h3>
	<table>
	  <tr>
		<td width=35%> <?php echo $this->Form->input('contacto',array('label' => 'Contacto 1', 'div' => false)); ?></td>
	</tr>
	<tr>
		<td> <?php echo $this->Form->input('telefono',array('label' => 'Tel&eacute;fono', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?></td>
		<td> <?php echo $this->Form->input('skype',array('label' => 'Skype', 'div' => false,)); ?></td>

		<td> <?php echo $this->Form->input('celular',array('label' => 'Celular', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?></td>

		<td> <?php echo $this->Form->input('correocontacto',array('label' => 'Correo Electr&oacute;nico', 'div' => false,'class'=>"validate[custom[email]]",'type'=>'email')); ?></td>
	  </tr>
	  <tr style="height: 1.5px; background: rgba(182, 183, 184, 0.53); display: block; width: 287%; margin-top: 5px; margin-bottom: 5px;"></tr>
	    <tr>
		<td width=35%> <?php echo $this->Form->input('contacto2',array('label' => 'Contacto 2', 'div' => false)); ?></td>
	</tr>
	<tr>
		<td> <?php echo $this->Form->input('telefono2',array('label' => 'Tel&eacute;fono', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?></td>
		<td> <?php echo $this->Form->input('skype2',array('label' => 'Skype', 'div' => false)); ?></td>
		<td> <?php echo $this->Form->input('celular2',array('label' => 'Celular', 'div' => false,'onclick'=>"formatotelefono()",'onfocus'=>"verificarmask(),formatotelefono();")); ?></td>
		<td> <?php echo $this->Form->input('correocontacto2',array('label' => 'Correo Electr&oacute;nico','class'=>"validate[custom[email]]",'type'=>'email')); ?></td>
	  </tr>
	</table>
	<?php echo $this->Form->end('Almacenar');?>
	</fieldset>

</div>
<br>
<div class="actions">
	<ul>
		<!--<li><?php /* echo $this->Html->link(__('Eliminar', true), array('action'=>'delete', $this->Form->value('Socionegocio.id')), null,__('Desea elminar el registro # %s?', $this->Form->value('Socionegocio.id'))); */ ?></li>-->
		<li><?php echo $this->Html->link(__('Listado de Socios de Negocios', true), array('action'=>'index'));?></li>
		<!-- <li><?php /* echo $this->Html->link(__('Listado Categoria de Socio de Negocios', true), array('controller'=> 'catsocionegocios', 'action'=>'index')); */ ?> </li>-->
		<li><A HREF="javascript:javascript:history.go(-1)">Regresar</A></li>
	</ul>
</div>
<script type="text/javascript">
	$("#SocionegocioDepartamentoId").on("change",function(){
		id=$("#SocionegocioDepartamentoId").val();
		$.ajax({
		    url: "../municipios",
			type: "POST",
			data: {id:id},
			dataType: "html",
	        success:  function (response) {
	            if (response==0) {
	                alert("Este departamento no cuenta con municipios en el sistema, puede ingresar un nuevo municipio en el area de administracion");
	                $("#SocionegocioMunicipioId").html("<option value=''>Seleccione un municipio</option>");
	            }else{
	                $("#SocionegocioMunicipioId").html(response);
	            }         	
	        }
		});
	});
	$("#SocionegocioPaiseId").on("change",function(){
		id=$("#SocionegocioPaiseId").val();
		$.ajax({
		    url: "../departamento",
			type: "POST",
			data: {id:id},
			dataType: "html",
	        success:  function (response) {
	            if (response==0) {
	                alert("Este pais no cuenta con departamentos en el sistema, puede ingresar un nuevo departamento en el area de administracion");
	            	 $("#SocionegocioDepartamentoId").html("<option value=''>Seleccione un departamento</option>");
	            		 $("#SocionegocioMunicipioId").html("<option value=''>Seleccione un municipio</option>");
	            }else{
	                $("#SocionegocioDepartamentoId").html(response);
	            }         	
	        }
		});
	});
		$("#SocionegocioDui").on("change", function (){
		var valor=$("#SocionegocioDui").val();
			if (valor.length>5) {
				$("#SocionegocioDui").removeAttr("class");
				$("#SocionegocioPasaporte").removeAttr("class");
				$("#SocionegocioOtroid").removeAttr("class");

			}else{
				if ($("#SocionegocioPasaporte").val().length>5 || $("#SocionegocioOtroid").val().length>5) {

				}else{
					$("#SocionegocioDui").addClass("validate[required]");
					$("#SocionegocioPasaporte").addClass("validate[required]");
					$("#SocionegocioOtroid").addClass("validate[required]");
				}
			}
		});
$("#SocionegocioPasaporte").on("change", function(){
	var valor=$("#SocionegocioPasaporte").val();
			if (valor.length>5) {
				$("#SocionegocioDui").removeAttr("class");
				$("#SocionegocioPasaporte").removeAttr("class");
				$("#SocionegocioOtroid").removeAttr("class");

			}else{
				if ($("#SocionegocioDui").val().length>5 || $("#SocionegocioOtroid").val().length>5) {

				}else{
					$("#SocionegocioDui").addClass("validate[required]");
					$("#SocionegocioPasaporte").addClass("validate[required]");
					$("#SocionegocioOtroid").addClass("validate[required]");
				}
			}
		});
$("#SocionegocioOtroid").on("change", function(){
	var valor=$("#SocionegocioOtroid").val();
			if (valor.length>5) {
				$("#SocionegocioDui").removeAttr("class");
				$("#SocionegocioPasaporte").removeAttr("class");
				$("#SocionegocioOtroid").removeAttr("class");

			}else{
				if ($("#SocionegocioPasaporte").val().length>5 || $("#SocionegocioDui").val().length>5) {

				}else{
					$("#SocionegocioDui").addClass("validate[required]");
					$("#SocionegocioPasaporte").addClass("validate[required]");
					$("#SocionegocioOtroid").addClass("validate[required]");
				}
			}
		});
	function verificarmask(){
		var pais = $("#SocionegocioPaiseId").val();
		if(pais==""){
			alert("Seleccione un País");
			$("#SocionegocioPaiseId").focus();
		}
	}
    /*------VALIDACION DE CAMPO CODIGO---------------*/
    function val_cod(){
        var cod = $("#SocionegocioCodigo").val();
        var url = getURL()+'val_cod';
        var id_soc = $("#SocionegocioId").val();
        $.ajax({
            url: url,
            type: 'POST',
            data: {cod:cod,id_soc:id_soc},
            success:function(resp){
                if(resp==1){
                    $("#SocionegocioCodigo").validationEngine("showPrompt",'Código repetido, no se puede repetir el código de un socio de negocios.','AlertText');
                    $("#SocionegocioCodigo").val("");
                    $("#SocionegocioCodigo").focus();
                }
            }
        });
    }
</script>

