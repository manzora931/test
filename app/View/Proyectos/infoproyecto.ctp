<div class="row"><br>
    <div class="col-md-8">
        <div class="col-md-4 text-left">
            <label class="lblproy">Código del Proyecto</label>
        </div>
        <div class="col-md-8">
            <?= $this->Form->input('codigo',[
                'label'=> false,
                'required'=>true,
                'placeholder'=>'PRG0234',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'readonly'=>true,
                'value'=>$proyecto['Proyecto']['codigo']
            ]); ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Nombre del Proyecto (corto)</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('nombrecorto',[
                'label'=>false,
                'required'=>true,
                'placeholder'=>'Fortalecimiento de Capacidades Rurales',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'readonly'=>true,
                'value'=>$proyecto['Proyecto']['nombrecorto']
            ]);
            ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Nombre Completo del Proyecto</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('nombrecompleto',[
                'label'=>false,
                'required'=>true,
                'placeholder'=>'Fortalecimiento de capacidades de personas con VIH en zonas rurales',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'readonly'=>true,
                'value'=>$proyecto['Proyecto']['nombrecompleto']
            ]);?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Tipo de Proyecto</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('tipoproyecto_id',[
                'label'=>false,
                'class'=>'form-control',
                'empty'=>"Seleccionar",
                'required'=>true,
                'div'=>['class'=>"form-groupp"],
                'disabled'=>true,
                'value'=>$proyecto['Tipoproyecto']['id']
            ]);?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-4">
            <label class="lblproy">Institución</label>
        </div>
        <div class="col-md-8">
            <?= $this->Form->input("institucion_id",[
                'label'=>false,
                'disabled'=>true,
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'options'=>$institucion,
                'selected'=>$proyecto['Proyecto']['institucion_id']
            ]);
            ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Estado</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('estado_id',[
                'label'=>false,
                'required'=>true,
                'empty'=>'Seleccionar',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'disabled'=>true,
                'value'=>$proyecto['Estado']['id']
            ]);
            ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">País</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('paise_id',[
                'label'=>false,
                'required'=>true,
                'empty'=>'Seleccionar',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'disabled'=>true,
                'selected'=>$proyecto['Paise']['id']
            ]);
            ?>
        </div>
        <div class="row">
            <div class="col-md-12 line-space">
                <div class="col-md-2"><label class="lblproy">Vigencia</label></div>
                <div class="col-md-5"><?php
                    $desde = (isset($proyecto['Proyecto']['desde']))?date("d-m-Y",strtotime($proyecto['Proyecto']['desde'])):"";
                    echo $this->Form->input("desde",[
                        'type'=>'text',
                        'class'=>"form-control",
                        'required'=>true,
                        'label'=>"",
                        'readonly'=>true,
                        'value'=>$desde
                    ]);?>
                </div>
                <div class="col-md-5">
                    <?php
                    $hasta = (isset($proyecto['Proyecto']['hasta']))?date("d-m-Y",strtotime($proyecto['Proyecto']['hasta'])):"";
                    echo $this->Form->input("hasta",[
                        'type'=>'text',
                        'class'=>"form-control",
                        'required'=>true,
                        'label'=>"",
                        'readonly'=>true,
                        'value'=>$hasta
                    ]);?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <br>
        <div class="col-md-12">
            <?=
            $this->Form->input('descripcion',[
                'rows'=>3,
                'label'=>false,
                'div'=>['class'=>"form-groupp"],
                'placeholder'=>"Descripción",
                'readonly'=>true,
                'value'=>$proyecto['Proyecto']['descripcion']

            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="lblproy">Creado</label>
                    <label><?= $proyecto['Proyecto']['usuario']." (".$proyecto['Proyecto']['created'].")";?></label>
                </div>
                <div class="col-md-4">
                    <label class="lblproy">Modificado</label>
                    <label><?= ($proyecto['Proyecto']['usuariomodif']!='')?$proyecto['Proyecto']['usuariomodif']." (".$proyecto['Proyecto']['modified'].")":"";?></label>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>