<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->css('proyecto/impresion') ?>
<?= $this->Html->script('datepicker-es') ?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:2017"
        });
        $.datepicker.regional["es"];
    });
</script>
<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 12%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }

</style>
<div class="proyectos index container">
    <h2>
        <?php echo __('Proyectos'); ?>
        <a class="imprimir btn" style="margin-left: 20px;" href="javascript:window.open('<?php echo Router::url('/'); ?>proyectos/impresion', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
    </h2>
	<p class="paginate-count clearfix">
		<?php
	echo $this->paginator->counter(array(
		'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));	?></p>

    <div id='search_box' class="searchBox clearfix">
        <?php
        /*Inicia formulario de busqueda*/
        $tabla = "proyectos";
        $fecha = date("d-m-Y");

        ?>
        <?php
        echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle'));
        echo "<input type='hidden' name='_method' value='POST' />"; ?>
        <table style ='width: 100%; border:none; background-color: transparent;'>
            <tbody>
                <tr>
                    <td>

        <?php $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Nombre'));   ?>
                    </td>
                    <td width="14%">
                        <?= $this->Form->input('desde', [
                            'placeholder' => $fecha,
                            'class' => 'form-control datepicker',

                        ]) ?>
                    </td>
                    <td width="14%">
                        <?= $this->Form->input('hasta', [
                            'placeholder' => $fecha,
                            'class' => 'form-control datepicker',
                        ]) ?>
                    </td>
                    <td width="14%">
                        <?= $this->Form->input('tipoproyecto_id', [
                            'class' => 'form-control',
                            'empty' => 'Seleccionar',
                            'label' => 'Tipo de proyecto'
                        ]) ?>
                    </td>
                    <td width="14%">
                        <?= $this->Form->input('institucion_id', [
                            'class' => 'form-control',
                            'empty' => 'Seleccionar',
                            'label' => 'Institución',
                            'options'=>$institucion
                        ]) ?>
                    </td>

                    <td width="10%">
                        <?= $this->Form->input('paise_id', [
                            'class' => 'form-control',
                            'empty' => 'Seleccionar',
                            'label' => 'Paises'
                        ]) ?>
                    </td>
                    <td width="11%">
                        <?= $this->Form->input('estado_id', [
                            'class' => 'form-control',
                            'empty' => 'Seleccionar',
                            'label' => 'Estado'
                        ]) ?>
                    </td>

                    <td style='float:right; padding-top: 20px; '>
                        <?php echo $this->Form->hidden('Controller', array('value'=>'Proyecto', 'name'=>'data[tabla][controller]')); //hacer cambio
                        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
                        echo $this->Form->hidden('Parametro1', array('value'=>'nombrecorto,nombrecompleto', 'name'=>'data[tabla][parametro]'));
                        echo $this->Form->hidden('rangofecha', array('value'=>'si', 'name'=>'data['.$tabla.'][rangofecha]'));
                        echo $this->Form->hidden('campoFecha', array('value'=>'desde', 'name'=>'data['.$tabla.'][campoFecha]'));
                        echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]')); ?>
                        <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>
                    </td>
                </tr>
            </tbody>
        </table>
                <?php
        if(isset($_SESSION['proyectos']))
        {
            $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
        }
        ?>	</div>
	<table id="tblin" class="table table-condensed table-striped" cellpadding="0" cellspacing="0" border="1">
	<thead>
        <tr>
            <th  width="7%"><?php echo $this->Paginator->sort('codigo','Código del Proyecto'); ?></th>
            <th width="28%"><?php echo $this->Paginator->sort('nombrecorto','Nombre del Proyecto'); ?></th>
            <th  width="15%"><?php echo $this->Paginator->sort('nombrecompleto','Institución'); ?></th>
            <th  width="10%"><?php echo $this->Paginator->sort('desde','Fecha de Inicio'); ?></th>
            <th  width="10%"><?php echo $this->Paginator->sort('desde','Fecha de Finalización'); ?></th>
            <th  width="7%"><?php echo $this->Paginator->sort('modified','Última Actualización'); ?></th>
            <th  width="10%"><?php echo $this->Paginator->sort('tipoproyecto_id','Tipo de Proyecto'); ?></th>
            <th  width="8%"><?php echo $this->Paginator->sort('paise_id','País'); ?></th>
            <th  width="6%"><?php echo $this->Paginator->sort('estado_id','Estado'); ?></th>
        </tr>
	</thead>
    <tbody>
	<?php foreach ($proyectos as $proyecto): ?>
	    <tr>
            <td class="text-left"><?= $this->Html->link($proyecto['Proyecto']['codigo'],['controller'=>'proyectos','action'=>'view',$proyecto['Proyecto']['id']]); ?>&nbsp;</td>
            <td class="text-left"><?= h($proyecto['Proyecto']['nombrecorto']); ?>&nbsp;</td>
            <td class="text-left"><?= h($proyecto['Institucion']['nombre']); ?>&nbsp;</td>
            <td class="text-left"><?= date("d-m-Y",strtotime($proyecto['Proyecto']['desde'])); ?>&nbsp;</td>
            <td class="text-left"><?= date("d-m-Y",strtotime($proyecto['Proyecto']['hasta'])); ?>&nbsp;</td>
            <td class="text-left"><?= (!empty($proyecto['Proyecto']['modified']))?date("d-m-Y",strtotime($proyecto['Proyecto']['modified'])):""; ?>&nbsp;</td>
            <td class="text-left"><?= $proyecto['Tipoproyecto']['tipoproyecto']; ?></td>
            <td class="text-left"><?= $proyecto['Paise']['pais']; ?></td>
            <td><div class="badge" style="background: <?=$colores[$proyecto['Proyecto']['estado_id']]['background']?>;color:<?=$colores[$proyecto['Proyecto']['estado_id']]['color']?>;"><?= $proyecto['Estado']['estado']; ?></div></td>
<?php endforeach; ?>
	</table>
	<div class="paging">
		
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <?php  if($perf==1){ ?>
    <div class="actions">
        <ul>
            <li class="style-btn"><?php echo $this->Html->link(__('Crear Proyecto'), array('action' => 'add'),array( 'style'=>'width:50px;','class'=>'btn btn-default')); ?></li>
        </ul>
    </div>
    <?php   }   ?>
</div>
