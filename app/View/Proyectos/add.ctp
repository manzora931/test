<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<div class="proyectos form container-fluid">
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <!--a id="btn-proy" role="button" class="" data-toggle="collapse" href="" data-target="#frm"-->
                        <span class="stage-title">Proyecto</span>
                        <!--/a-->
                    </h4>
                </div>
                <div id="frm" class="" role="tabpanel">
                    <div class="alert alert-danger" id="alerta" style="display: none">
                        <span class="icon icon-check-circled" id="msjalert"></span>
                        <button type="button" class="close" data-dismiss="alert"></button>
                    </div>
                    <div class="panel-body" id="capa">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="actions">
    <div>
        <?php echo $this->Html->link(__('Listado de Proyectos'), array('action' => 'index'),['class'=>'btn btn-default']); ?>
    </div>
</div>
<script>
    jQuery(function(){
        $("#btn-proy").click(function(e){
            e.preventDefault();
        });
        $("#capa").load("form_add");
        /*$("#alerta").slideUp();*/
    });
</script>