<?php echo $this->Html->Script(['proyectos/eventos']);
echo $this->Html->css(['proyecto/main']);
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<script>
    jQuery(function(){
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:2017"
        });
    });
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="proyectos view">
    <br>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading" id="seccionProyecto">
                <h4 class="panel-title">
                    <a id="btn-proy" role="button" class="collapsed" data-toggle="collapse" href="" data-target="#frm" aria-expanded="true">
                        <span class="stage-title" id="proyecto" data-proyecto="<?=$id?>">Proyecto - <?=$name['Proyecto']['nombrecompleto']?></span>
                    </a>
                    <a class="btn-impresion btn" style="margin-left: 20px;" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?=$id?>', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">
                        Imprimir  <div class="icon-tringle"></div>
                    </a>
                </h4>
            </div>
            <div id="frm" class="panel-collapse collapse" role="tabpanel">
                <?php
                    if(isset($_SESSION['saveproy'])){
                        unset( $_SESSION['saveproy'] );
                        ?>
                        <div class="alert alert-success" id="alertaP">
                            <span class="icon icon-check-circled"></span>
                            Proyecto Almacenado
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
<?php               }
                    if(isset($_SESSION['disabled'])) {
                        unset($_SESSION['disabled']);    ?>
                        <div class="alert alert-warning" id="alertaP">
                            <span class="icon icon-check-circled"></span>
                            No puede editar el proyecto
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
                <?php   }   ?>
                <div class="panel-body" id="capa">

                </div>
            </div>
            <!------------------------------------------------------------->
            <div class="row">
                <div class="col-md-12 marg-panel">
                    <ul class="nav nav-pills">
                        <li id="ffc"><a id="ff">Fuentes de Financiamiento</a></li>
                        <li id="actc"><a id="act">Actividades</a></li>
                        <li id="prec"><a id="presupuesto">Presupuesto</a></li>
                        <li id="desc"><a id="desembolso">Desembolsos</a></li>
                        <li id="gasc"><a id="gastos">Gastos</a></li>
                        <li id="incc"><a id="incidencias">Incidencias</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <?php /***************CAPA DONDE SE CARGAN LAS PANTALLAS RELACIONADAS A PROYECTOS*******************/?>
                <div class="container-fluid child" id="childs">

                </div>
            </div>

        </div>
    </div>
    <div class="actions">
        <?php  if($estadoP['Proyecto']['estado_id']==1 || $estadoP['Proyecto']['estado_id']==2){ ?>
        <div><?php echo $this->Html->link(__('Editar Proyectos'), array('action' => 'edit', $id),['class'=>'btn btn-default']); ?> </div>
        <?php   }   ?>
        <div><?php echo $this->Html->link(__('Listado de Proyectos'), array('action' => 'index'),['class'=>'btn btn-default']); ?></div>
    </div>
</div>
<script>
    jQuery(function(){
        var url = getURL();
        $("#alertaP").slideDown();
        setTimeout(function () {
            $("#alertaP").slideUp();
        }, 4000);
        $("#capa").load("<?=Router::url(['controller'=>'proyectos','action'=>'infoproyecto',$id]);?>");
        $("#btn-proy").click(function(e){
            e.preventDefault();
        });
        $("#btn-proy").click();
    });
</script>
