<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'proyecto/impresion')); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php
    $page = 1;
    $meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
    $fecha = date('d-m-Y');
    $fechaactual = explode('-', $fecha);
    $mes=$meses[$fechaactual[1]];
;?>

<div class="container" style="margin-bottom: 35px;">
    <div class="col-print-12">
        <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
        <label class="org"><?= $org ?></label>
        <br>
        <label class="tittle"><?php echo __('Sistema de Monitoreo y Evaluación de Proyectos'); ?></label>
        <div class="linea"></div>
        <label class="encabezado"><?php echo __('Seguimiento de proyectos al '. $fechaactual[0]. ' ' . $mes . ' de ' . $fechaactual[2]); ?></label>
    </div>
    <div class="clearfix"></div>
    <?php if((isset($_SESSION['proyectos'])) &&
        ($_SESSION['tabla[proyectos]']['search_text'] != '' || $_SESSION['tabla[proyectos]']['desde'] != '' || $_SESSION['tabla[proyectos]']['hasta'] != '' ||
            $_SESSION['tabla[proyectos]']['tipoproyecto_id'] != '' || $_SESSION['tabla[proyectos]']['paise_id'] != '' || $_SESSION['tabla[proyectos]']['estado_id'] != '')){ ?>
        <div id="search-box">
            <div class="col-print-12">
                <label class="encabezado-busqueda">Seguimiento de proyectos:</label>
            </div>
            <?php  if($_SESSION['tabla[proyectos]']['search_text'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">Buscar por: </label>
                </div>
                <div class="col-print-7">
                    <label class="resultado"><?= $_SESSION['tabla[proyectos]']['search_text']; ?></label>
                </div>
                <div class="clearfix"></div>
            <?php } ?>
            <?php if($_SESSION['tabla[proyectos]']['desde'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">Fecha desde: </label>
                </div>
                <div class="col-print-2">
                    <label class="resultado"><?= $_SESSION['tabla[proyectos]']['desde']; ?></label>
                </div>
            <?php } ?>
            <?php if($_SESSION['tabla[proyectos]']['desde'] != '' && $_SESSION['tabla[proyectos]']['hasta'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">Fecha hasta: </label>
                </div>
                <div class="col-print-2">
                    <label class="resultado"><?= $_SESSION['tabla[proyectos]']['hasta']; ?></label>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <?php if($_SESSION['tabla[proyectos]']['tipoproyecto_id'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">Tipo de Proyecto: </label>
                </div>
                <div class="col-print-7">
                    <label class="resultado"><?= $tipoproyecto; ?></label>
                </div>
                <div class="clearfix"></div>
            <?php } ?>

            <?php if($_SESSION['tabla[proyectos]']['paise_id'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">País: </label>
                </div>
                <div class="col-print-7">
                    <label class="resultado"><?= $pais; ?></label>
                </div>
                <div class="clearfix"></div>
            <?php } ?>

            <?php if($_SESSION['tabla[proyectos]']['estado_id'] != ''){ ?>
                <div class="col-print-2">
                    <label class="resultado-busqueda">Estado: </label>
                </div>
                <div class="col-print-7">
                    <label class="resultado"><?= $estado; ?></label>
                </div>
                <div class="clearfix"></div>
            <?php } ?>
        </div>

        <div class="clearfix"></div>
    <?php } ?>
    <div class="col-print-12" style="margin-top: 20px;">
        <div class="actions do-not-print">
            <ul>
                <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
            </ul>
        </div>

        <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
            <thead>
            <tr>
                <th>Código del Proyecto</th>
                <th width="30%">Nombre del Proyecto</th>
                <th>Fecha de Inicio</th>
                <th>Última Actualización</th>
                <th>Tipo de Proyecto</th>
                <th>País</th>
                <th>Estado</th>
            </tr>
            <tr class="espacio">
                <td class="border2" colspan="7"></td>
            </tr>
            </thead>
            <tbody>
            <?php
            $cont=1;
            if($proyectos) {
                foreach ($proyectos as $proyecto) {
                    ?>
                    <tr>
                        <td class="text-center border2 border4" ><?= $proyecto['Proyecto']['codigo'] ?></td>
                        <td class="text-left border2 centro"><?= $proyecto['Proyecto']['nombrecorto'] ?></td>
                        <td class="text-center border2 centro"><?= date('d-M-Y', strtotime($proyecto['Proyecto']['desde'])) ?></td>
                        <td class="text-center border2 centro"><?= (!empty($proyecto['Proyecto']['modified']))?date("d-m-Y",strtotime($proyecto['Proyecto']['modified'])):""; ?></td>
                        <td class="text-center border2 centro"><?= $proyecto['Tipoproyecto']['tipoproyecto'] ?></td>
                        <td class="text-center border2 centro"><?= $proyecto['Paise']['pais'] ?></td>
                        <td class="text-center border1 border2"><span  style="margin-bottom: 2px;"><?= $proyecto['Estado']['estado'] ?></span></td>
                    </tr>
                <?php }
            } else { ?>
                <tr>
                    <td class="text-center border1 border2" style="border-left: solid 1px;" colspan="7">No se encontraron denuncias</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="container">
    <div class="col-print-12" style="display: none;">
        <div class="onlyprint" style="width: 100%">
            <div class="linea"></div>
            <label class="text-center onlyprint">2017 - Derechos Reservados, Programa Regional REDCA+</label>
        </div>
    </div>
</div>