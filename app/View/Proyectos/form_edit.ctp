<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.regional["es"];

        /* var desde = $("#desdes");
         var hasta = $("#hastas");

         hasta.datepicker("option", "minDate", $(desde).datepicker("getDate"));
         desde.datepicker("option", "maxDate", $(hasta).datepicker("getDate"));

         desde.on("change", function() {
         hasta.datepicker("option", "minDate", $(this).datepicker("getDate"));
         });

         hasta.on("change", function() {
         desde.datepicker("option", "maxDate", $(this).datepicker("getDate"));
         });*/
    });
</script>
<?php
    $readSelect="";
    $read = "";
    if($admin!=1 && $this->data['Proyecto']['estado_id']==2){
        $read="readonly='readonly'";
        $readSelect="fDisabled(this.id)";
    }

?>
<div class="row">
    <br>
    <div class="col-md-8">
        <div class="col-md-4 text-left">
            <label class="lblproy">Código del Proyecto</label>
        </div>
        <div class="col-md-8">
            <?php
            echo $this->Form->hidden('id',['value'=>$this->data['Proyecto']['id']]);
            echo $this->Form->input('codigo',[
                'label'=> false,
                'required'=>true,
                'placeholder'=>'Código del Proyecto',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'value'=>$this->data['Proyecto']['codigo'],
                'maxlength'=>45,
                $read

            ]); ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Nombre del Proyecto (corto)</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('nombrecorto',[
                'label'=>false,
                'required'=>true,
                'placeholder'=>'Nombre corto del proyecto',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'value'=>$this->data['Proyecto']['nombrecorto'],
                'maxlength'=>200,
                $read
            ]);
            ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Nombre Completo del Proyecto</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('nombrecompleto',[
                'label'=>false,
                'required'=>true,
                'placeholder'=>'Nombre completo del proyecto',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'value'=>$this->data['Proyecto']['nombrecompleto'],
                'maxlength'=>400,
                $read
            ]);?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Tipo de Proyecto</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('tipoproyecto_id',[
                'label'=>false,
                'class'=>'form-control',
                'empty'=>"Seleccionar",
                'required'=>true,
                'div'=>['class'=>"form-groupp"],
                'selected'=>$this->data['Proyecto']['tipoproyecto_id'],
                'onFocus'=>$readSelect
            ]);?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-4">
            <label class="lblproy">Institución</label>
        </div>
        <div class="col-md-8">
            <?=
            $this->Form->input('institucion_id',[
                'label'=>false,
                'class'=>'form-control',
                'empty'=>'Seleccionar',
                'required'=>true,
                'div'=>['class'=>'form-groupp'],
                'options'=>$institucion,
                'selected'=>$this->data['Proyecto']['institucion_id'],
                'onFocus'=>$readSelect

            ]);
            ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">Estado</label>
        </div>
        <div class="col-md-8 line-space">
            <?= $this->Form->input('estado_id',[
                'label'=>false,
                'required'=>true,
                'empty'=>'Seleccionar',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'selected'=>$this->data['Proyecto']['estado_id'],
                'onFocus'=>$readSelect
            ]);
            ?>
        </div>
        <div class="col-md-4 line-space">
            <label class="lblproy">País</label>
        </div>
        <div class="col-md-8 line-space">
            <?=
            $this->Form->input('paise_id',[
                'label'=>false,
                'required'=>true,
                'empty'=>'Seleccionar',
                'class'=>'form-control',
                'div'=>['class'=>'form-groupp'],
                'selected'=>$this->data['Proyecto']['paise_id'],
                'onFocus'=>$readSelect
            ]);
            ?>
        </div>
        <div class="row">
            <br>
            <div class="col-md-12 line-space">
                <div class="col-md-2"><label class="lblproy">Vigencia</label></div>
                <div class="col-md-5"><?= $this->Form->input("desde",[
                        'type'=>'text',
                        'class'=>"form-control datepicker",
                        'required'=>true,
                        'label'=>"",
                        'onChange'=>'RangoFecha();',
                        'value'=>date("d-m-Y",strtotime($this->data['Proyecto']['desde'])),
                        $read

                    ]);?>
                </div>
                <div class="col-md-5">
                    <?= $this->Form->input("hasta",[
                        'type'=>'text',
                        'class'=>"form-control datepicker",
                        'required'=>true,
                        'label'=>"",
                        'onChange'=>'RangoFecha();',
                        'value'=>date("d-m-Y",strtotime($this->data['Proyecto']['hasta'])),
                        $read
                    ]);?>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">
        <br>
        <div class="col-md-12">
        <?=
        $this->Form->input('descripcion',[
            'rows'=>3,
            'label'=>false,
            'div'=>['class'=>"form-groupp"],
            'placeholder'=>"Descripción",
            'value'=>$this->data['Proyecto']['descripcion'],
            $read

        ]);

        ?>
        </div>
    </div>
    <?php   if($admin!=1 && $this->data['Proyecto']['estado_id']==2){   ?>
    <div class="col-md-12">
        <div class="actions">
            <div><?= $this->Html->link(__('Salir'), array('action' => 'view', $this->data['Proyecto']['id']),['class'=>'btn btn-default']); ?></div>
        </div>
    </div>
    <?php   }else{  ?>
       <div class="col-md-12">
           <div class="actions">
               <div><?= $this->Form->button('Almacenar', [
                       'label' => false,
                       'id'=>"saveProyec",
                       'type' => 'submit',
                       'class' => 'btn btn-default',
                       'div' => [
                           'class' => 'form-group'
                       ]
                   ]); ?>
               </div>
           </div>
       </div>
<?php       }   ?>
</div>
<script>
    function RangoFecha(){
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        if(desde !== '' && hasta!==''){
            var cad1 = desde.split("-");
            var cad2 = hasta.split("-");
            var dateStart=new Date(cad1[2]+"/"+(cad1[1])+"/"+cad1[0]+" 00:00");
            var dateEnd=new Date(cad2[2]+"/"+(cad2[1])+"/"+cad2[0]+" 00:00");
            if(dateStart > dateEnd){
                $("#hasta").val("");
                $("#msjalert").text("La fecha hasta en la vigencia no puede ser menor a la fecha desde.");
                $("#alerta").slideDown();
                setTimeout(function () {
                    $("#alerta").slideUp();
                }, 4000);
            }
        }
    }
    function fDisabled(idCompleto)
    {
        var val = $('#'+idCompleto).val();
        var temp_actual = document.getElementById(idCompleto);
        var opts=temp_actual.options;
        for (var j = 0;j<opts.length;j++)
        {
            if(opts[j].value!=val){
                opts[j].setAttribute("disabled","disabled");
            }
        }
    }
    jQuery(function(){
        $("#saveProyec").click(function (e) {
            e.preventDefault();
            /**VARIABLES***/
            var id = $("#id").val();
            var cod = $("#codigo").val();
            var ncorto = $("#nombrecorto").val();
            var ncompleto = $("#nombrecompleto").val();
            var tproyecto = $("#tipoproyecto_id").val();
            var ntipoproyecto = $("#tipoproyecto_id  option:selected").text();
            var estado = $("#estado_id").val();
            var nestado = $("#estado_id  option:selected").text();
            var pais = $("#paise_id").val();
            var npais = $("#paise_id  option:selected").text();
            var fdesde = $("#desde").val();
            var fhasta = $("#hasta").val();
            var desc = $("#descripcion").val();
            var institucion = $("#institucion_id").val();
            var ninstitucion = $("#institucion_id  option:selected").text();

            if(institucion !=='' && cod !== '' && ncorto !== '' && ncompleto!=='' && tproyecto!=='' && estado !== '' && pais !== '' && fdesde !== '' && fhasta !==''){
                var url = '<?= Router::url(['controller'=>'proyectos','action'=>'edproyecto']);?>';
                var url2 = '<?= Router::url(['controller'=>'proyectos','action'=>'view']);?>';
                $.ajax({
                    url:url,
                    type:'post',
                    data:{id:id,cod:cod,ncorto:ncorto,ncompleto:ncompleto,tproyecto:tproyecto,estado:estado,pais:pais,fdesde:fdesde,fhasta:fhasta,desc:desc,institucion:institucion, ntipoproyecto:ntipoproyecto, nestado:nestado, npais:npais, ninstitucion:ninstitucion},
                    cache:false,
                    success:function(resp){
                        if(resp!=="error"){
                            window.location = url2+"/"+resp;
                        }else{
                            $("#msjalert").text("Ya existe un proyecto con el código ingresado.");
                            $("#alerta").slideDown();
                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                        }
                    }
                });
            }else{
                $("#msjalert").text("Debe completar los campos requeridos");
                $("#alerta").slideDown();
                setTimeout(function () {
                    $("#alerta").slideUp();
                }, 4000);
            }
        });
    });
</script>
