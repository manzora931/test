<?php
    $this->layout = false;
    echo $this->Html->css(['proyecto/main']);
    $total_monto = 0;
    $total_desembolsos = 0;
    $total_gastos = 0;
 ?>
<div class="fuentesfinanciamientos index container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger" id="fuentes-error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="alert alert-success" id="fuentes-success" style="display: none">
                <span class="icon icon-check-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
        if($estadoProy == 1){
        ?>
        <div class="col-sm-4">
            <h2><?php echo __('Fuentes de Financiamiento'); ?></h2>
        </div>
        <div class="col-sm-offset-2 col-sm-4 div-actions">

            <table width="100%">
                <td class="acciones form-group">
                    <button type="button" class="btn btn-crear acciones-shadow" id="add-details" name="button">Agregar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="edit-details" name="button">Modificar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                </td>
                <td class="acciones form-group">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/fuentesfinanciamientos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>
            </table>
            <?php   }else{  ?>
            <div class="col-sm-9">
                <h2><?php echo __('Fuentes de Financiamiento'); ?></h2>
            </div>
            <div class="col-sm-offset-1 col-sm-1 div-actions">

                <table width="100%">
                    <td class="acciones form-group">
                        <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/fuentesfinanciamientos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                    </td>
                </table>
            <?php   }   ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <br><br>
    <div class="row form-fuentes hide">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <span class="encabezado-panel"></span>
                </div>
                <div class="panel-body">
                    <form id="formulario-fuentes" method="post">
                        <table>
                            <tr>
                                <td>
                                    <input type="hidden" id="proyecto" value="<?= $idProyecto ?>">
                                    <input type="hidden" id="fuenteid">
                                    <?= $this->Form->input('financista_id',
                                        array('label' => 'Fuente de Financiamiento',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'financista',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('monto',
                                        array('label' => 'Monto',
                                            'class' => 'form-control',
                                            'id' => 'monto',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"number"
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td>
                                    <div class="pull-right acciones-panel">
                                        <button type="button" class="btn btn-danger acciones-fuente" id="almacenar" name="button">Almacenar</button>
                                        <button type="button" class="btn btn-default " id="cancelar" name="button">Cancelar</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr><?php
                    if($estadoProy == 1){
                    ?>
                    <th width="5%" class="select tbf"></th>
        <?php        }  ?>
                    <th class="tbf"><?php echo h('Fuente de Financiamiento'); ?></th>
                    <th class="tbf" width="15%"><?php echo h('Monto Total'); ?></th>
                    <th class="tbf" width="15%"><?php echo h('Desembolsado'); ?></th>
                    <th class="tbf" width="15%"><?php echo h('Ejecutado'); ?></th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php if(count($fuentesfinanciamientos) > 0) { ?>
                    <?php foreach ($fuentesfinanciamientos as $fuentesfinanciamiento): ?>
                        <?php
                        $gastos= 0;
                        $desembolsos= 0;
                        ?>
                        <tr>
                <?php     if($estadoProy == 1){  ?>
                            <td class="select text-center">
                                <input type="radio" name="item-selected" class="item-selected" value="<?= $fuentesfinanciamiento['Fuentesfinanciamiento']['id'] ?>">
                            </td>
                <?php       }   ?>
                            <td class="text-left"><?php echo h($fuentesfinanciamiento['Financista']['nombre']); ?></td>
                            <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($fuentesfinanciamiento['Fuentesfinanciamiento']['monto'],2); ?>&nbsp;</td>
                            <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['desembolso'],2); ?></td>
                            <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['ejecutado'],2); ?></td>
                            <?php
                            $total_monto += $fuentesfinanciamiento['Fuentesfinanciamiento']['monto'];
                            $total_desembolsos += $info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['desembolso'];
                            $total_gastos += $info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['ejecutado'];
                            ?>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="totales">
                        <?php   if($estadoProy == 1){   ?>
                        <td></td>
                        <?php    } ?>
                        <td class="text-right" style="text-align: right;"><?php echo h('Totales'); ?></td>
                        <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($total_monto,2); ?>&nbsp;</td>
                        <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($total_desembolsos,2); ?></td>
                        <td class="text-right" style="text-align: right;"><?php echo  "$ " . number_format($total_gastos,2); ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td colspan="5" class="text-center">No hay fuentes de financiamiento relacionadas</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    var row = '';

    $(function () {
        var btnAdd = $(".btn-crear");
        var btnEdit = $("#edit-details");
        var btnDelete = $("#delete-details");
        var proyecto = '<?=$idProyecto?>';
        var financistas = $("#financista").html();

        // Se verifica si tiene un proyecto asociado
        if(proyecto == '') {
            // Deshabilita las acciones que se realizan en Fuentes de Financiamiento
            btnAdd.attr('disabled', true);
            btnEdit.attr('disabled', true);
            btnDelete.attr('disabled', true);

        } else {

            // Habilita las acciones que se realizan en Fuentes de Financiamiento
            btnAdd.attr('disabled', false);
            btnEdit.attr('disabled', false);
            btnDelete.attr('disabled', false);
        }

        // Al momento de hacer click en el boton "Agregar"
        btnAdd.click( function () {
            $("#financista").html(financistas);

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se deselecciona el item
                $(".item-selected:checked").attr('checked', false);
            }

            // Se le establece el encabezado del formulario de fuente de informacion
            $('.encabezado-panel').html('Agregar Fuente de Financiamiento');

            // Se limpian los campos del formulario
            $('#financista').val('');
            $('#monto').val('');

            // Se muestra el formulario
            $('.form-fuentes').toggleClass('hide');

            return false;
        });

        // Al momento de hacer click en el boton "Modificar"
        btnEdit.click( function () {
            var url = "<?= Router::url(array('controller' => 'fuentesfinanciamientos', 'action' => 'get_data2')); ?>";
            var id = $(".item-selected:checked").val();
            $("#fuenteid").val(id);
            var proyectoid = '<?=$idProyecto;?>';
            $('#financista').html('<option value="" >Seleccionar</option>');
            var item = '';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se obtiene la informacion de la fuente de financiamiento segun el item seleccionado
                request = $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id:id, proyectoid: proyectoid},
                    dataType: 'json'
                });

                request.done(function (response, textStatus, jqXHR){
                    // Se le establece el encabezado del formulario de fuente de informacion
                    $('.encabezado-panel').html('Editar Fuente de Financiamiento');

                    // Se agrega la informacion de los financistas
                    response.financista.forEach(set_data);

                    // Se carga la informacion segun la fuente de financiamiento seleccionada
                    $('#financista').val(response.data[0].fuentesfinanciamientos.financista_id);
                    $('#monto').val(response.data[0].fuentesfinanciamientos.monto);

                    // Se muestra el formulario
                    $('.form-fuentes').toggleClass('hide');
                });
            }
        });

        // Al momento de hacer click en el boton "Eliminar"
        btnDelete.click( function () {
            var url = "<?= Router::url(array('controller' => 'fuentesfinanciamientos', 'action' => 'delete')); ?>";
            var id = $(".item-selected:checked").val();
            var proyectoid = '<?=$idProyecto?>';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                var r = confirm("¿Esta seguro de eliminar la fuente de financiamiento?");

                if (r == true) {
                    request = $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:id, proyectoid: proyectoid},
                        dataType: 'json'
                    });

                    request.done(function (response, textStatus, jqXHR){
                        if(response.msg == 'exito') {
                            // Se refresca la tabla de fuentes de financiamiento
                            response.data.forEach(refresh_table);
                            row+= '<tr class="totales">' +
                                '<td></td>' +
                                '<td>Totales</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].montototal).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].totaldesembolsos).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].totalgastos).toFixed(2) + '</td>' +
                                '</tr>';

                            tbody.innerHTML = row;

                            $("#fuentes-success .txt").text('Fuente de financiamiento eliminado exitosamente');
                            $("#fuentes-success").slideDown();

                            setTimeout(function () {
                                $("#fuentes-success").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error1') {
                            $("#fuentes-error .txt").text('Error, Fuente de financiamiento no existe');
                            $("#fuentes-error").slideDown();

                            setTimeout(function () {
                                $("#fuentes-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error2') {
                            $("#fuentes-error .txt").text('Error al eliminar la fuente de financiamiento');
                            $("#fuentes-error").slideDown();

                            setTimeout(function () {
                                $("#fuentes-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error3') {
                            $("#fuentes-error .txt").text('Error, La fuente de financiamiento no puede ser eliminado porque tiene actividades relacionadas');
                            $("#fuentes-error").slideDown();

                            setTimeout(function () {
                                $("#fuentes-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        }
                    });
                }
            }

            return false;
        });

        // Al momento de almacenar la informacion ingresada de la fuente de financiamiento
        $('#almacenar').click( function () {
            var url = "<?= Router::url(array('controller' => 'fuentesfinanciamientos', 'action' => 'almacenar_data')); ?>";
            var proyectoid = '<?=$idProyecto?>';
            var financistaid = $('#financista').val();
            var financista = $("#financista  option:selected").text();
            var monto = $('#monto').val();
            var id = $("#fuenteid").val();
            var tbody = document.getElementById('tbody');

            $('#tblin').toggleClass('hide');
            $('#tbody').html('');
            // Se prepara la informacion a almacenar
            var data = {id:id, proyectoid: proyectoid, financistaid: financistaid, monto: monto, financista:financista};

            request = $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: data,
                dataType: 'json'
            });

            request.done(function (response, textStatus, jqXHR){
                if(response.msg != 'error') {
                    // Se refresca la tabla de fuentes de financiamiento
                    response.data.forEach(refresh_table);
                    row+= '<tr class="totales">' +
                        '<td></td>' +
                        '<td>Totales</td>' +
                        '<td class="text-right">' + parseFloat(response.totales[0][0].montototal).toFixed(2) + '</td>' +
                        '<td class="text-right">' + parseFloat(response.totales[0][0].totaldesembolsos).toFixed(2) + '</td>' +
                        '<td class="text-right">' + parseFloat(response.totales[0][0].totalgastos).toFixed(2) + '</td>' +
                        '</tr>';

                    tbody.innerHTML = row;

                    $('.form-fuentes').toggleClass('hide');
                    $('#tblin').toggleClass('hide');

                    $("#fuentes-success .txt").text('Fuente de financiamiento almacenado exitosamente');
                    $("#fuentes-success").slideDown();

                    setTimeout(function () {
                        $("#fuentes-success").slideUp();
                        $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                    }, 4000);
                } else {
                    $("#fuentes-error .txt").text('Error al almacenar la fuente de financiamiento');
                    $("#fuentes-error").slideDown();

                    setTimeout(function () {
                        $("#fuentes-error").slideUp();
                        $("#childs").load('<?= Router::url(['controller'=>'fuentesfinanciamientos','action'=>'index',$idProyecto])?>');
                    }, 4000);
                }
            });
        });

        // Al momento de hacer click en el boton "Cancelar" del formulario
        $('#cancelar').click( function () {
            $('.form-fuentes').toggleClass('hide');
        });
    });

    function set_data(item, index) {
        var select = document.getElementById('financista');
        var opt = document.createElement('option');

        opt.value = item.fi.id;
        opt.innerHTML = item.fi.nombre;
        select.appendChild(opt);
    }

    function refresh_table(item, index) {
        var gastos = 0;
        var desembolsos = 0;

        row+= '<tr>' +
                    '<td class="select text-center"><input type="radio" name="item-selected" class="item-selected" value="' + item.Fuentesfinanciamiento.id + '"></td>' +
                    '<td>' + item.Financista.nombre + '</td>' +
                    '<td class="text-right">' + parseFloat(item.Fuentesfinanciamiento.monto).toFixed(2) + '</td>' +
                    '<td class="text-right">' + parseFloat(gastos).toFixed(2) + '</td>' +
                    '<td class="text-right">' + parseFloat(desembolsos).toFixed(2) + '</td>' +
                  '</tr>';
    }
</script>


