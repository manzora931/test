<div class="fuentesfinanciamientos form">
<?php echo $this->Form->create('Fuentesfinanciamiento'); ?>
	<fieldset>
		<legend><?php echo __('Edit Fuentesfinanciamiento'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('financista_id');
		echo $this->Form->input('monto');
		echo $this->Form->input('proyecto_id');
		echo $this->Form->input('usuario');
		echo $this->Form->input('usuariomodif');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Almacenar')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Fuentesfinanciamientos'), array('action' => 'index')); ?></li>
	</ul>
</div>
