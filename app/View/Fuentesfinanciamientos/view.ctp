<div class="fuentesfinanciamientos view">
<h2><?php echo __('Fuentesfinanciamiento'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Financista'); ?></dt>
		<dd>
			<?php echo $this->Html->link($fuentesfinanciamiento['Financista']['nombre'], array('controller' => 'financistas', 'action' => 'view', $fuentesfinanciamiento['Financista']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['monto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Proyecto'); ?></dt>
		<dd>
			<?php echo $this->Html->link($fuentesfinanciamiento['Proyecto']['nombrecorto'], array('controller' => 'proyectos', 'action' => 'view', $fuentesfinanciamiento['Proyecto']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['usuario']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuariomodif'); ?></dt>
		<dd>
			<?php echo h($fuentesfinanciamiento['Fuentesfinanciamiento']['usuariomodif']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Fuentesfinanciamientos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Fuentesfinanciamientos'), array('action' => 'edit', '')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Actividades'); ?></h3>
	<?php if (!empty($fuentesfinanciamiento['Actividade'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Inicio'); ?></th>
		<th><?php echo __('Limite'); ?></th>
		<th><?php echo __('Paise Id'); ?></th>
		<th><?php echo __('Fuentesfinanciamiento Id'); ?></th>
		<th><?php echo __('Intervencione Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Usuario'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Usuariomodif'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($fuentesfinanciamiento['Actividade'] as $actividade): ?>
		<tr>
			<td><?php echo $actividade['id']; ?></td>
			<td><?php echo $actividade['nombre']; ?></td>
			<td><?php echo $actividade['inicio']; ?></td>
			<td><?php echo $actividade['limite']; ?></td>
			<td><?php echo $actividade['paise_id']; ?></td>
			<td><?php echo $actividade['fuentesfinanciamiento_id']; ?></td>
			<td><?php echo $actividade['intervencione_id']; ?></td>
			<td><?php echo $actividade['created']; ?></td>
			<td><?php echo $actividade['usuario']; ?></td>
			<td><?php echo $actividade['modified']; ?></td>
			<td><?php echo $actividade['usuariomodif']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'actividades', 'action' => 'view', $actividade['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'actividades', 'action' => 'edit', $actividade['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'actividades', 'action' => 'delete', $actividade['id']), null, __('Are you sure you want to delete # %s?', $actividade['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Actividade'), array('controller' => 'actividades', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
