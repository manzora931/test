<div class="desembolsos view">
<h2><?php echo __('Desembolso'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fuentesfinanciamiento'); ?></dt>
		<dd>
			<?php echo $this->Html->link($desembolso['Fuentesfinanciamiento']['id'], array('controller' => 'fuentesfinanciamientos', 'action' => 'view', $desembolso['Fuentesfinanciamiento']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bancorigen'); ?></dt>
		<dd>
			<?php echo $this->Html->link($desembolso['Bancorigen']['nombre'], array('controller' => 'bancos', 'action' => 'view', $desembolso['Bancorigen']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bancodestino'); ?></dt>
		<dd>
			<?php echo $this->Html->link($desembolso['Bancodestino']['nombre'], array('controller' => 'bancos', 'action' => 'view', $desembolso['Bancodestino']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destinatario'); ?></dt>
		<dd>
			<?php echo $this->Html->link($desembolso['Destinatario']['contacto_id'], array('controller' => 'destinatarios', 'action' => 'view', $desembolso['Destinatario']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Codigo'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['codigo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['monto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comision'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['comision']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['usuario']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuariomodif'); ?></dt>
		<dd>
			<?php echo h($desembolso['Desembolso']['usuariomodif']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Desembolsos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Editar Desembolsos'), array('action' => 'edit', '')); ?> </li>
	</ul>
</div>
