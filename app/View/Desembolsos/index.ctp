<?php $this->layout = false; ?>
<?= $this->Html->css(['proyecto/main']); ?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
         dateFormat: "dd-mm-yy",
         changeMonth: true,
         changeYear: true,
         yearRange: "1960:2017"
         });
        $.datepicker.regional["es"];
    });
</script>
<div class="desembolsos index container-fluid">
    <div class="row">
        <?php if(isset($_SESSION['savedesembolso'])){
            unset($_SESSION['savedesembolso']); ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="success-desembolso" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Desembolso Almacenado Exitosamente</span>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <div class="alert alert-danger" id="desembolso-error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="alert alert-success" id="desembolso-success" style="display: none">
                <span class="icon icon-check-circled"></span>
                <span class="txt"></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h2><?php echo __('Desembolsos'); ?></h2>
        </div>
        <div class="col-sm-offset-2 col-sm-6 div-actions">
            <?php   if($infoP['Proyecto']['estado_id']==2 && $admin==1){   ?>
            <table width="100%">
                <td class="acciones form-group">
                    <button type="button" class="btn btn-crear acciones-shadow" id="add-details" name="button">Agregar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="edit-details" name="button">Modificar</button>
                </td>
                <td class="acciones form-group">
                    <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                </td>
                <td class="acciones form-group">
                    <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                    <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/desembolsos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                </td>
            </table>
            <?php   }   ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <br><br>
    <div class="row form-desembolsos hide">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="encabezado-panel"></span>
                </div>
                <div class="panel-body">
                    <form id="formulario-desembolsos" method="post" style="padding: 5px;">
                        <table>
                            <tr>
                                <td>
                                    <input type="hidden" id="proyecto" value="<?= $idProyecto ?>">
                                    <input type="hidden" id="desembolsoid">
                                    <?= $this->Form->input('fuentefinanciamiento_id',
                                        array('label' => 'Fuente de Financiamiento',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'options' => $fuentes,
                                            'id' => 'fuente',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('bancorigen_id',
                                        array('label' => 'Banco Origen',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'options' => $bancos,
                                            'id' => 'origen',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('bancdestino_id',
                                        array('label' => 'Banco Destino',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'destino',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('destinatario_id',
                                        array('label' => 'Destinatario',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'destinatario',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td>
                                    <?= $this->Form->input('codigo',
                                        array('label' => 'Código',
                                            'class' => 'form-control',
                                            'id' => 'codigo-desembolso',
                                            'value'=>'',
                                            'required'=>'required'
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td>
                                    <?= $this->Form->input('fecha',
                                        array('label' => 'Fecha',
                                            'class' => 'form-control datepicker',
                                            'id' => 'fecha',
                                            'value'=>'',
                                            'placeholder'=>'dd-mm-yyyy',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td style="width: 120px;">
                                    <?= $this->Form->input('monto',
                                        array('label' => 'Monto',
                                            'class' => 'form-control',
                                            'id' => 'monto',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td style="width: 120px;">
                                    <?= $this->Form->input('comision',
                                        array('label' => 'Comisión',
                                            'class' => 'form-control',
                                            'id' => 'comision',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12"></td>
                                <td>
                                    <div class="acciones-panel">
                                        <button type="button" class="btn btn-danger acciones-fuente" id="almacenar" name="button" style="width: 100%">Almacenar</button>
                                    </div>
                                </td>
                                <td style="width: 10px;"></td>
                                <td>
                                    <div class="acciones-panel">
                                        <button type="button" class="btn btn-default " id="cancelar" name="button" style="width: 100%">Cancelar</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <?= ($infoP['Proyecto']['estado_id']==2)?"<th class='tbf' width='5%' class='select'></th>":"";?>
                    <th class="tbf"><?php echo h('Fecha'); ?></th>
                    <th class="tbf"><?php echo h('Fuente'); ?></th>
                    <th class="tbf"><?php echo h('Banco Origen'); ?></th>
                    <th class="tbf"><?php echo h('Banco Destino'); ?></th>
                    <th class="tbf"><?php echo h('Código'); ?></th>
                    <th class="tbf"><?php echo h('Destinatario'); ?></th>
                    <th class="tbf"><?php echo h('Monto'); ?></th>
                    <th class="tbf"><?php echo h('Comisiones'); ?></th>
                    <th class="tbf"><?php echo h('Monto Total'); ?></th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php if(count($desembolsos) > 0) {
                    $total_monto = 0;
                    $total_comision = 0;
                    $total_total = 0;
                    foreach ($desembolsos as $desembolso): ?>
                        <tr>
                            <?php   if($infoP['Proyecto']['estado_id']==2){ ?>
                            <td style="text-align: center !important;">
                                <input type="radio" name="item-selected" class="item-selected" value="<?= $desembolso['Desembolso']['id'] ?>">
                            </td>
                            <?php } ?>
                            <td class="text-center">
                                <?php
                                $mes = date("n",strtotime($desembolso['Desembolso']['fecha']));
                                echo date("d",strtotime($desembolso['Desembolso']['fecha']))."-".$meses[$mes]."-".date("Y",strtotime($desembolso['Desembolso']['fecha']));
                                ?>
                            </td>
                            <td style="text-align: left"><?php echo h($desembolso['Financista']['nombre']); ?></td>
                            <td style="text-align: left"><?php echo h($desembolso['Bancoorigen']['nombre']); ?></td>
                            <td style="text-align: left"><?php echo h($desembolso['Bancodestino']['nombre']); ?></td>
                            <td><?php echo h($desembolso['Desembolso']['codigo']); ?></td>
                            <td style="text-align: left"><?php echo h($desembolso[0]['nombre']); ?></td>
                            <td style="text-align: right"><?php echo  "$ " . number_format($desembolso['Desembolso']['monto'],2); ?>&nbsp;</td>
                            <td style="text-align: right"><?php echo  "$ " . number_format($desembolso['Desembolso']['comision'],2); ?>&nbsp;</td>
                            <td  style="text-align: right"><?php echo  "$ " . number_format($desembolso['Desembolso']['total'],2); ?>&nbsp;</td>
                            <?php
                            $total_monto += $desembolso['Desembolso']['monto'];
                            $total_comision += $desembolso['Desembolso']['comision'];
                            $total_total += $desembolso['Desembolso']['total'];
                            ?>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="totales">
                        <?=($infoP['Proyecto']['estado_id']==2)?"<td colspan='7' style='text-align: right'>Totales</td>":"<td colspan='6' style='text-align: right'>Totales</td>"; ?>
                        <td style="text-align: right"><?php echo  "$ " . number_format($total_monto, 2); ?>&nbsp;</td>
                        <td style="text-align: right"><?php echo  "$ " . number_format($total_comision, 2); ?></td>
                        <td style="text-align: right"><?php echo  "$ " . number_format($total_total, 2); ?></td>
                    </tr>
                <?php } else {
                    $cols = ($infoP['Proyecto']['estado_id']==2)?10:9;
                    ?>
                    <tr>
                        <td colspan="<?=$cols?>" class="text-center">No hay desembolsos relacionados</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    var row = '';
    var cont = 0;

    $("#success-desembolso").slideDown();
    setTimeout(function(){
        $("#success-desembolso").slideUp();
    },4000);

    $(function () {
        var btnAdd = $(".btn-crear");
        var btnEdit = $("#edit-details");
        var btnDelete = $("#delete-details");
        var proyecto = '<?=$idProyecto?>';
        var fuentes = $("#fuente").html();
        var bancosorigen = $("#origen").html();
        var bancosdestino = $("#destino").html();
        var destinatario = $("#destinatario").html();

        // Se verifica si tiene un proyecto asociado
        if(proyecto == '') {
            // Deshabilita las acciones que se realizan en Desembolsos
            btnAdd.attr('disabled', true);
            btnEdit.attr('disabled', true);
            btnDelete.attr('disabled', true);

        } else {

            // Habilita las acciones que se realizan en Desembolsos
            btnAdd.attr('disabled', false);
            btnEdit.attr('disabled', false);
            btnDelete.attr('disabled', false);
        }

        // Al momento de hacer click en el boton "Agregar"
        btnAdd.click( function () {
            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se deselecciona el item
                $(".item-selected:checked").attr('checked', false);
            }

            // Se le establece el encabezado del formulario de desembolsos
            $('.encabezado-panel').html('Agregar Desembolso');

            // Se limpian los campos del formulario
            $('#fecha').val('');
            $("#fuente").val('');
            $("#origen").val('');
            $("#destino").val('');
            $("#destinatario").val('');
            $('#codigo-desembolso').val('');
            $('#monto').val('');
            $('#comision').val('');

            // Se muestra el formulario
            $('.form-desembolsos').toggleClass('hide');

            return false;
        });

        // Al momento de hacer click en el boton "Modificar"
        btnEdit.click( function () {
            var url = "<?= Router::url(array('controller' => 'desembolsos', 'action' => 'get_data')); ?>";
            var id = $(".item-selected:checked").val();
            $("#desembolsoid").val(id);
            var proyectoid = '<?=$idProyecto;?>';
            $('#destino').html('<option value="" >Seleccionar</option>');
            var item = '';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se obtiene la informacion del desembolso segun el item seleccionado
                request = $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id:id, proyectoid: proyectoid},
                    dataType: 'json'
                });

                request.done(function (response, textStatus, jqXHR){
                    var fechadesembolso = response.data[0].desembolsos.fecha;
                    fechadesembolso = fechadesembolso.split("-");

                    // Se le establece el encabezado del formulario de desembolsos
                    $('.encabezado-panel').html('Editar Desembolso');

                    // Se agrega la informacion de los bancos
                    response.bancos.forEach(set_data);

                    // Se carga la informacion segun el desembolso seleccionado
                    $('#fecha').val(fechadesembolso[2] + '-' + fechadesembolso[1] + '-' + fechadesembolso[0]);
                    $('#fuente').val(response.data[0].desembolsos.fuentefinanciamiento_id);
                    $('#origen').val(response.data[0].desembolsos.bancorigen_id);
                    $('#destino').val(response.data[0].desembolsos.bancdestino_id);
                    $('#destinatario').val(response.data[0].desembolsos.destinatario_id);
                    $('#codigo-desembolso').val(response.data[0].desembolsos.codigo);
                    $('#monto').val(response.data[0].desembolsos.monto);
                    $('#comision').val(response.data[0].desembolsos.comision);

                    // Se muestra el formulario
                    $('.form-desembolsos').toggleClass('hide');
                });
            }
        });

        // Al momento de hacer click en el boton "Eliminar"
        btnDelete.click( function () {
            var url = "<?= Router::url(array('controller' => 'desembolsos', 'action' => 'delete')); ?>";
            var id = $(".item-selected:checked").val();
            var proyectoid = '<?=$idProyecto?>';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                var r = confirm("¿Esta seguro de eliminar el desembolso?");

                if (r == true) {
                    request = $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:id, proyectoid: proyectoid},
                        dataType: 'json'
                    });

                    request.done(function (response, textStatus, jqXHR){
                        if(response.msg == 'exito') {
                            // Se refresca la tabla de desembolsos
                            response.data.forEach(refresh_table);
                            row+= '<tr class="totales">' +
                                '<td></td>' +
                                '<td>Totales</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].montototal).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].$totalcomision).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].totaltotal).toFixed(2) + '</td>' +
                                '</tr>';

                            tbody.innerHTML = row;

                            $("#desembolso-success .txt").text('Desembolso eliminado exitosamente');
                            $("#desembolso-success").slideDown();

                            setTimeout(function () {
                                $("#desembolso-success").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error1') {
                            $("#desembolso-error .txt").text('Desembolso no existe');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error2') {
                            $("#desembolso-error .txt").text('Error al eliminar el desembolso');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error3') {
                            $("#desembolso-error .txt").text('No puede ser eliminado porque el total desembolsado restante no cubre el total de gastos');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        }
                    });
                }
            }
            return false;
        });

        // Al momento de almacenar la informacion ingresada del desembolso
        $('#almacenar').click( function () {
            var url = "<?= Router::url(array('controller' => 'desembolsos', 'action' => 'almacenar_data')); ?>";
            if(cont === 0){
                cont++;
                var proyectoid = '<?=$idProyecto?>';
                var fecha = $('#fecha').val();
                var fuentefinan = $("#fuente").val();
                var ffinanciamiento = $("#fuente  option:selected").text();
                var borigen = $("#origen").val();
                var bancoorigen = $("#origen  option:selected").text();
                var bdestino = $("#destino").val();
                var bancodestino = $("#destino  option:selected").text();
                var destinatario = $("#destinatario").val();
                var ndestinatario = $("#destinatario  option:selected").text();
                var codigo = $('#codigo-desembolso').val();
                var monto_desembolso = parseFloat($('#monto').val());
                var comision = parseFloat($('#comision').val());
                var id = $("#desembolsoid").val();
                var tbody = document.getElementById('tbody');
                var errores = 0;
                if (isNaN(monto_desembolso)) {
                    $('#monto').val('');
                    cont = 0;
                    $("#desembolso-error .txt").text('El valor del monto ingresado es inválido');
                    $("#desembolso-error").slideDown();

                    setTimeout(function () {
                        $("#desembolso-error").slideUp();
                    }, 4000);

                    errores++;
                } else if(monto_desembolso <= 0) {
                    $('#monto').val('');
                    cont = 0;

                    $("#desembolso-error .txt").text('El valor del monto ingresado debe ser mayor a cero');
                    $("#desembolso-error").slideDown();

                    setTimeout(function () {
                        $("#desembolso-error").slideUp();
                    }, 4000);

                    errores++;
                }

                if (isNaN(comision)) {
                    $('#comision').val('');
                    cont = 0;

                    $("#desembolso-error .txt").text('El valor de la comisión ingresada es inválida');
                    $("#desembolso-error").slideDown();

                    setTimeout(function () {
                        $("#desembolso-error").slideUp();
                    }, 4000);

                    errores++;
                } else if(comision <= 0) {
                    $('#comision').val('');
                    cont = 0;

                    $("#desembolso-error .txt").text('El valor de la comisión ingresada debe ser mayor a cero');
                    $("#desembolso-error").slideDown();

                    setTimeout(function () {
                        $("#desembolso-error").slideUp();
                    }, 4000);

                    errores++;
                }

                if(errores == 0) {
                    //$('#tblin').toggleClass('hide');
                    //$('#tbody').html('');
                    // Se prepara la informacion a almacenar
                    var data = {id:id,proyectoid: proyectoid,fuentefinan: fuentefinan,borigen: borigen,bdestino: bdestino,  destinatario: destinatario,  fecha: fecha,  codigo: codigo, monto: monto_desembolso, comision: comision, ffinanciamiento:ffinanciamiento, bancoorigen:bancoorigen, bancodestino:bancodestino, ndestinatario:ndestinatario};
                    request = $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: data,
                        dataType: 'json'
                    });
                    request.done(function (response, textStatus, jqXHR){
                        if(response.msg == 'exito') {
                            // Se refresca la tabla de fuentes de financiamiento
                            response.data.forEach(refresh_table);
                            row+= '<tr class="totales">' +
                                '<td></td>' +
                                '<td>Totales</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].montototal).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].totalcomisiones).toFixed(2) + '</td>' +
                                '<td class="text-right">' + parseFloat(response.totales[0][0].totaltotal).toFixed(2) + '</td>' +
                                '</tr>';
                            //tbody.innerHTML = row;

                            $('.form-desembolsos').toggleClass('hide');
                            //$('#tblin').toggleClass('hide');
                            $("#childs").load(getURL()+"desembolsos/index/<?=$idProyecto?>");
                        } else if(response.msg == 'error1') {
                            $("#desembolso-error .txt").text('Error al almacenar el desembolso');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error2') {
                            $("#desembolso-error .txt").text('El monto ingresado es mayor al monto de la fuente de financiamiento');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'desembolsos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error3') {
                            $("#desembolso-error .txt").text('La fuente de financiamiento no puede cubrir el desembolso');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                                $("#childs").load(getURL()+"desembolsos/index/<?=$idProyecto?>");
                            }, 4000);
                        } else if(response.msg == 'error4') {
                            cont=0;
                            $('#monto').focus();
                            $("#desembolso-error .txt").text('El monto ingresado  no puede ser menor al total de gastos de la fuente de financiamiento');
                            $("#desembolso-error").slideDown();

                            setTimeout(function () {
                                $("#desembolso-error").slideUp();
                            }, 4000);
                        }
                    });
                }
            }
        });

        // Al momento de seleccionar un banco de origen
        $('#origen').change( function () {
            var url = "<?= Router::url(array('controller' => 'desembolsos', 'action' => 'get_bancos')); ?>";
            var id = $(this).val();

            $.ajax({
                url: url,
                type:'post',
                data:{
                    id: id
                },
                success:function(resp){
                    $("#destino").html(resp);
                    $("#destino").val('');
                }
            });
        });

        // Al momento de hacer click en el boton "Cancelar" del formulario
        $('#cancelar').click( function () {
            $('.form-desembolsos').toggleClass('hide');
        });
    });

    function set_data(item, index) {
        var select = document.getElementById('destino');
        var opt = document.createElement('option');

        opt.value = item.ba.id;
        opt.innerHTML = item.ba.nombre;
        select.appendChild(opt);
    }

    function refresh_table(item, index) {
        row+= '<tr>' +
            '<td class="select text-center"><input type="radio" name="item-selected" class="item-selected" value="' + item.Desembolso.id + '"></td>' +
            '<td class="text-center">' + item.Desembolso.fecha + '</td>' +
            '<td>' + item.Financista.nombre + '</td>' +
            '<td>' + item.Bancoorigen.nombre + '</td>' +
            '<td>' + item.Bancodestino.nombre + '</td>' +
            '<td class="text-center">' + item.Desembolso.codigo + '</td>' +
            '<td>' + item[0].nombre + '</td>' +
            '<td class="text-right">' + parseFloat(item.Desembolso.monto).toFixed(2) + '</td>' +
            '<td class="text-right">' + parseFloat(item.Desembolso.comision).toFixed(2) + '</td>' +
            '<td class="text-right">' + parseFloat(item.Desembolso.total).toFixed(2) + '</td>' +
            '</tr>';
    }
</script>