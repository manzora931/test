<div class="desembolsos form">
<?php echo $this->Form->create('Desembolso'); ?>
	<fieldset>
		<legend><?php echo __('Edit Desembolso'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('fecha');
		echo $this->Form->input('fuentefinanciamiento_id');
		echo $this->Form->input('bancorigen_id');
		echo $this->Form->input('bancdestino_id');
		echo $this->Form->input('destinatario_id');
		echo $this->Form->input('codigo');
		echo $this->Form->input('monto');
		echo $this->Form->input('comision');
		echo $this->Form->input('total');
		echo $this->Form->input('usuario');
		echo $this->Form->input('usuariomodif');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Almacenar')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Desembolsos'), array('action' => 'index')); ?></li>
	</ul>
</div>
