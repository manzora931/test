<div class="tiposocionegocios form">
<?php echo $this->Form->create('Tiposocionegocio'); ?>
	<fieldset>
		<legend><?php echo __('Editar Tipo de Socio'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('tiposocionegocio',array('label'=>'Tipo de Socio','div'=>false));
		echo $this->Form->input('descripcion',array('label'=>'Descripci&oacute;n','div'=>false));
	
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Aceptar')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Tipos de Socios'), array('action' => 'index')); ?></li>
	</ul>
</div>
