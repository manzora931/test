<?php echo $this->Html->Script('buscar',FALSE); ?>
<?php echo $this->Html->Script('jquery',FALSE); ?>
<?php echo $this->Html->Script('jquery-ui.js',FALSE); ?>
<?php echo $this->html->Script('url'); ?>
<div class="tiposocionegocios index">
	<h2><?php echo __('Tipos de Socios '); ?></h2>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Pagina {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));
	?>	</p>
	<div id="search_box">
	<!--Para realizar las busquedas con ajax -->
		<?php echo $this->Html->link(__('<-Ver todas'), array('action' => 'index'),array("class"=>"atras")); ?>
		<!--Cuando se quieren buscar en mas de una columna se colocan separadas por coma-->
		<?php echo $this->Form->input('Buscar',array('onkeypress'=>"buscar(this.id, 'tiposocionegocio','Tiposocionegocio','tiposocionegocio_id', 'TiposocionegociosController',url);"));?>
</div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('tiposocionegocio','Tipo de Socio'); ?></th>
			<th><?php echo $this->Paginator->sort('descripcion','Descripción'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($tiposocionegocios as $tiposocionegocio): ?>
	<tr>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['id']); ?>&nbsp;</td>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['tiposocionegocio']); ?>&nbsp;</td>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['descripcion']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__(' '), array('action' => 'view', $tiposocionegocio['Tiposocionegocio']['id']),array('class'=>'ver')); ?>
			<?php echo $this->Html->link(__(' '), array('action' => 'edit', $tiposocionegocio['Tiposocionegocio']['id']),array('class'=>'editar')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previo', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Crear Tipo de Socio'), array('action' => 'add')); ?></li>
		</ul>
</div>
