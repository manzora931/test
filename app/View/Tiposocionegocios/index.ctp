<div class="tiposocionegocios index">
	<h2><?php echo __('Tipos de Socios de Negocio'); ?></h2>
	<p>
<?php
include 'files/pag.php';
?></p>
	<div id='search_box'>
		<?php
		/*Inicia formulario de busqueda*/
		$tabla = "tiposocionegocios";
		?>
		<?php
		echo $this->Form->create($tabla, array('action'=>'index'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Tipo Socio de Negocio, Descripción', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => ' Tipo Socio de Negocio, Descripción'));
		echo "</td>";
		echo "<td style='width: 100px;'>";
		//echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
		if (isset($_SESSION['tabla['.$tabla.']']['activo'])) {
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0' checked ='checked'>";
		}else{
			echo "<br>Inactivos <input type='checkbox' name='data[$tabla][activo]' value='0'>";
		}
		echo "</td>";
		echo "<td>";
		echo $this->Form->hidden('Controller', array('value'=>'Tiposocionegocio', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'tiposocionegocio,descripcion', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo $this->Form->end('Buscar');
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>
		<?php
		if(isset($_SESSION['tiposocionegocios']))
		{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
		}
		?>	</div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('tiposocionegocio','Tipo de Socio de Negocios'); ?></th>
			<th><?php echo $this->Paginator->sort('descripcion','Descripción'); ?></th>
			<th><?php echo $this->Paginator->sort('activo','Activo'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($tiposocionegocios as $tiposocionegocio): ?>
	<tr>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['id']); ?>&nbsp;</td>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['tiposocionegocio']); ?>&nbsp;</td>
		<td><?php echo h($tiposocionegocio['Tiposocionegocio']['descripcion']); ?>&nbsp;</td>
		<td><?php
			$status = ($tiposocionegocio['Tiposocionegocio']['activo'] == 1)?"SI":"NO";
			echo h($status); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__(' '), array('action' => 'view', $tiposocionegocio['Tiposocionegocio']['id']),array('class'=>'ver')); ?>
			<?php echo $this->Html->link(__(' '), array('action' => 'edit', $tiposocionegocio['Tiposocionegocio']['id']),array('class'=>'editar')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
</div>
<div class="paging">
	<?php echo $this->paginator->prev('<< '.__('Anterior ', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->paginator->numbers();?>
	<?php echo $this->paginator->next(__(' Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Crear Tipo de Socio de Negocios'), array('action' => 'add')); ?></li>
		</ul>
</div>
