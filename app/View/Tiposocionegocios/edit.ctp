<?php
$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<div class="tiposocionegocios form">
<?php echo $this->Form->create('Tiposocionegocio'); 
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es'); 
echo $this->Html->script('jquery.validationEngine');
echo $this->Html->script('activar_socioneg');
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#TiposocionegocioEditForm").validationEngine();
		});
</script>
    <script type="text/javascript">
        function getURL(){
            return '<?=$real_url;?>';
        }
    </script>
	<fieldset>
		<legend><?php echo __('Editar Tipo de Socio de Negocios'); ?></legend>
	<?php
		echo $this->Form->input('id',array('id'=>'id'));
		echo $this->Form->input('tiposocionegocio',array('label'=>'Tipo de Socio de Negocios','div'=>false,'class'=>'validate[required]','required'));
		echo $this->Form->input('descripcion',array('label'=>'Descripci&oacute;n','div'=>false));
		echo "</br></br>";
		echo $this->Form->input("activo", array('label'=>'Activo','type'=>'checkbox','id'=>'activo'));
	    echo $this->Form->end(__('Almacenar'));
		
	?>
	</fieldset>
</div>
<br/>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('Listado de Tipos de Socio de Negocios'), array('action' => 'index')); ?></li>
	</ul>
</div>
<script type="text/javascript">
	document.getElementById("TiposocionegocioTiposocionegocio").focus();
</script>