<div class="tiposocionegocios view">
<h2><?php echo __('Tipo de Socio de Negocios'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tiposocionegocio['Tiposocionegocio']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo de Socio de Negocios'); ?></dt>
		<dd>
			<?php echo h($tiposocionegocio['Tiposocionegocio']['tiposocionegocio']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripci&oacute;n'); ?></dt>
		<dd>
			<?php echo h($tiposocionegocio['Tiposocionegocio']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creado'); ?></dt>
		<dd>
			<?php echo $tiposocionegocio['Tiposocionegocio']['usuario']." (".$tiposocionegocio['Tiposocionegocio']['created'].")"; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modificado'); ?></dt>
		<dd>
			<?php echo $tiposocionegocio['Tiposocionegocio']['usuario_modif']." (".$tiposocionegocio['Tiposocionegocio']['modified'].")"; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<br>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Editar Tipo de Socio de Negocios'), array('action' => 'edit', $tiposocionegocio['Tiposocionegocio']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Listado de Tipos de Socios de Negocios'), array('action' => 'index')); ?> </li>
	</ul>
</div>
