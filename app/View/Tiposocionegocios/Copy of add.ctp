<div class="tiposocionegocios form">
<?php echo $this->Form->create('Tiposocionegocio'); ?>
	<fieldset>
		<legend><?php echo __('Add Tiposocionegocio'); ?></legend>
	<?php
		echo $this->Form->input('tiposocionegocio');
		echo $this->Form->input('descripcion');
		echo $this->Form->input('usuario');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tiposocionegocios'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Organizacions'), array('controller' => 'organizacions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Organizacion'), array('controller' => 'organizacions', 'action' => 'add')); ?> </li>
	</ul>
</div>
