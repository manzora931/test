<div class="tiposocionegocios form">
<?php echo $this->Form->create('Tiposocionegocio'); 
echo $this->Html->css('validationEngine.jquery');
echo $this->Html->script('jquery.validationEngine-es'); 
echo $this->Html->script('jquery.validationEngine');
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#TiposocionegocioAddForm").validationEngine();
		});
</script>
	<fieldset>
		<legend><?php echo __('Agregar Tipo de Socio de Negocios'); ?></legend>
	<?php
		echo $this->Form->input('tiposocionegocio',array('label'=>'Tipo de Socio de Negocios','div'=>false,'class'=>'validate[required]','required'));
		echo $this->Form->input('descripcion',array('label'=>'Descripci&oacute;n','div'=>false));
		echo "</br></br>";
		echo $this->Form->input("activo", array('label'=>'Activo','div'=>false,'type'=>'checkbox'));
	    echo $this->Form->end(__('Almacenar'));
	?>
	</fieldset>
</div>
<br>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Listado de Tipos de Socio de Negocios'), array('action' => 'index')); ?></li>
		</ul>
</div>
<script type="text/javascript">
	document.getElementById("TiposocionegocioTiposocionegocio").focus();
</script>
