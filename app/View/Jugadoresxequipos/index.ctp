<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
    <?php
        if(isset($_SESSION["delete"])){
            unset($_SESSION["delete"]); ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                Jugador eliminado
            </div>
<?php   }    ?>
	<h2><?php echo __('Jugadores por Equipos'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "jugadoresxequipos";
	$session = $this->Session->read('tabla[jugadoresxequipos]');
	$search_text = $session['search_text'] ;
	
	?>
	<div id='search_box' class="table-responsive">
		<?php 
		echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 100%;'));
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('class'=>'form-control','label'=>'Buscar por: Nombre, Apellido, Apodo', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre, Apellido, Apodo'));		
		echo "</td>";
		echo "<td style='width: 210px; padding-left: 4px;'>";
		echo $this->Form->input('',array('class'=>'form-control',
			'name'=>'data['.$tabla.'][equipo_id]',
			'options'=>$equipos,
			'label'=>'Equipo', 
			'type'=>'select',
			'default'=>(isset($_SESSION['tabla[jugadoresxequipos]']))?$_SESSION['tabla[jugadoresxequipos]']['equipo_id']:'', 
			'empty' => array( '(Seleccionar Equipo)')));
		echo "</td>";
		echo "<td style='width: 210px; padding-left: 4px;'>";
		echo $this->Form->input('',array('class'=>'form-control',
			'name'=>'data['.$tabla.'][torneo_id]',
			'options'=>$torneos,
			'label'=>'Torneo', 
			'type'=>'select', 
			'default'=>(isset($_SESSION['tabla[jugadoresxequipos]']))?$_SESSION['tabla[jugadoresxequipos]']['torneo_id']:'',
			'empty' => array( '(Seleccionar Torneo)')));
		echo "</td>";
		echo "<td  width='300px' style='float:right; padding-top: 20px; padding-left: 4px;'>";
		echo $this->Form->hidden('Controller', array('value'=>'Jugadore', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Controller2', array('value'=>'Jugadoresxequipo', 'name'=>'data[tabla][controller2]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'nombre,apellido,apodo', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";
		echo "</table>"; 
		?>

		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
		}
		?>
	</div>
	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('torneo_id', 'Torneo'); ?></th>
			<th><?php echo $this->Paginator->sort('equipo_id', 'Equipo'); ?></th>
			<th><?php echo $this->Paginator->sort('jugadore_id', 'Jugador'); ?></th>
			<th><?php echo $this->Paginator->sort('dorsal', 'Dorsal'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($jugadoresxequipos as $jugadoresxequipo):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?= h($jugadoresxequipo['Jugadoresxequipo']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?= $torneos[$jugadoresxequipo['Jugadoresxequipo']['torneo_id']]; ?></td>
				<td style='text-align:center'><?= $equipos[$jugadoresxequipo['Jugadoresxequipo']['equipo_id']]; ?></td>
				<td style='text-align:left'><?= $jugadores[$jugadoresxequipo['Jugadoresxequipo']['jugadore_id']]; ?></td>
				<td style='text-align:center'><?= h($jugadoresxequipo['Jugadoresxequipo']['dorsal']); ?>&nbsp;</td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $jugadoresxequipo['Jugadoresxequipo']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $jugadoresxequipo['Jugadoresxequipo']['id']),array('class'=>'editar')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'delete', $jugadoresxequipo['Jugadoresxequipo']['id']),array('class'=>'deleteDet')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<div><?php echo $this->Html->link(__('Nuevo Jugador'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>
<script>
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function(){
            $("#alerta").slideUp();
        },4000);
        $(".deleteDet").click(function(e){
            var resp = confirm("¿Está seguro de querer eliminar a este jugador?");
            if(!resp){
                e.preventDefault();
            }
        });
    });

</script>