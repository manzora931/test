<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<?= $this->Html->script(['funciones/showalert','funciones/addremoveclass','funciones/crearOptionSelect']) ?>
<style>
	.tb_tarea tr td input {
		margin-top: 10px;
	}
	.margen{
		margin-left: 20px;
	}
	.margenbtn{
		margin-left: 50px !important;
	}
</style>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>
<div class="tareas form container-fluid">
	<?php echo $this->Form->create('Jugadoresxequipo',array('id'=>'formulario')); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Jugador por Equipo'); ?></legend>
		<div class="alert alert-danger" id="alerta" style="display: none">
			<span class="icon icon-check-circled" id="msjalert"></span>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $this->Form->input('torneo_id',[
					'label'=>'Torneo',
					'class'=>'form-control validate[required]',
					//'onchange'=>"load_inter(this.id,0);",
					'empty'=>"Seleccionar",
					'required'=>true,
					'div'=>['class'=>"form-groupp"],
				]);?>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-3">
				<?= $this->Form->input('equipo_id',[
					'label'=>'Equipo',
					'class'=>'form-control validate[required]',
					//'onchange'=>"load_act(this.id, 0, 0);",
					'id'=>'equipo_id',
					'empty'=>"Seleccionar",
					'required'=>true,
					'div'=>['class'=>"form-groupp"],
				]);?>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="row">
			<div class="col-sm-12" style="margin-top: 25px;">
				<h3>Jugadores</h3>
			</div>
			<table style="margin-left: 15px;" class="tb_tarea">
				<?php
				for($i=0;$i<1;$i++){	?>
					<tr class="line<?=$i?>">
						<td>
							<?= $this->Form->input('jugadore_id['.$i . ']',[
								'label'=>false,
								'id'=>"jugadore_id".$i,
								'class'=>'form-control validate[required] nombret',
								'type'=>'select',
								'empty'=>"Seleccionar",
								'required'=>true,
								'options'=>$jugadores,
								'name'=>'data[Jugadoresxequipo][jugadore_id][0]',
								'div'=>['class'=>"form-groupp"]
							]);?>
						</td>
						<td>
							<?= $this->Form->input('dorsal['.$i . ']',[
								'label'=>false,
								'id'=>"dorsal".$i,
								'placeholder'=>'Dorsal',
								'style'=>'margin-top: 0',
								'class'=>'form-control margen',
								'name'=>'data[Jugadoresxequipo][dorsal][0]',
								'div'=>['class'=>'form-groupp']
							]); ?>
						</td>
						<td style='width: 200px;'>
							
						</td>
					</tr>
				<?php				}

				?>
			</table><br>
			<table style="margin-left: 15px;">
				<tr>
					<td colspan="2"><button id="addDet" onclick="newdet();" class="btn btn-default btn-add-row btn-search" type="button"><span><i class="fa fa-plus icon-search"></i></span></button></td>
				</tr>
			</table>
			<div class="col-sm-3"></div>
			<div class="col-sm-7"></div>
		</div>

	</fieldset>

	<div class="actions">

		<div><?= $this->Form->button('Almacenar', [
				'label' => false,
				'type' => 'submit',
				'class' => 'btn btn-default',
				'div' => [
					'class' => 'form-group'
				]
			]); ?>
			<?php echo $this->Form->end();?></div>
		<div><?php echo $this->Html->link(__('Listado de Jugadores por equipos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
	</div>
</div>
<script>
var jugadores_nombre=new Array();
var jugadores_id=new Array();
var cont=0;
<?php foreach ($jugadores as $key => $value): ?>
jugadores_id.push(<?=$key?>);
jugadores_nombre.push('<?=$value?>');
<?php endforeach; ?>
	function deleteT(num){
		$(".line"+num).remove();
	}
	function newdet(){
		cont++;
		var styleLine=" style='margin-top:10px;'";
		$(".tb_tarea").append("<tr class='line"+cont+"'><td><div class='form-groupp'"+styleLine+">" +
			"<select name='data[Jugadoresxequipo][jugadore_id]["+cont+"]' id='jugadore_id"+cont+"' class='form-control nombret'></select>" +
			"</div></td>" +
			"<td><div class='form-groupp'"+styleLine+">" +
			"<input name='data[Jugadoresxequipo][dorsal]["+cont+"]' id='dorsal"+cont+"' placeholder='Dorsal' style='margin-top: 0' class='form-control margen' type='text'>" +
			"</div></td>" +
			"<td style='width: 200px;'>"+
			"<button"+styleLine+" data-corr='0' onclick='deleteT("+cont+");' class='btn btn-default btn-remove-row btn-search margenbtn' type='button'><span><i class='fa fa-trash icon-search'></i></span></button>"+
			"</td>" +
			"</tr>");
		crearOptionSelect('Seleccionar','','jugadore_id'+cont);
		for (var i = 0; i < jugadores_id.length; i++) {
			crearOptionSelect(jugadores_nombre[i],jugadores_id[i],'jugadore_id'+cont);			
		}
	}
</script>