<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="tareas view container">
<h2><?php echo __('Jugadores por Equipo'); ?></h2>
    <?php
    if( isset( $_SESSION['jugadore_save'] ) ) {
        if( $_SESSION['jugadore_save'] == 1 ){
            unset( $_SESSION['jugadore_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Jugador almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($jugadoresxequipo['Jugadoresxequipo']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Torneo</td>
            <td>
                <?=$torneos[$jugadoresxequipo['Jugadoresxequipo']['torneo_id']]?>
            </td>
        </tr>
        <tr>
            <td>Equipo</td>
            <td>
                <?=$equipos[$jugadoresxequipo['Jugadoresxequipo']['equipo_id']]?>
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $jugadoresxequipo['Jugadoresxequipo']['usuario']." (".$jugadoresxequipo['Jugadoresxequipo']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($jugadoresxequipo['Jugadoresxequipo']['usuariomodif']!='')?$jugadoresxequipo['Jugadoresxequipo']['usuariomodif']." (".$jugadoresxequipo['Jugadoresxequipo']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0"  class="table table-condensed table-striped" style="margin-top:10px;">
        <thead>
        <tr>
            <th><?php echo 'Nombre'; ?></th>
            <th><?php echo 'Apellido' ?></th>
            <th><?php echo 'Apodo' ?></th>
            <th><?php echo 'Fecha Nacimiento' ?></th>
            <th><?php echo 'Dorsal' ?></th>
        </tr>
        </thead>
        <tbody id="tbody-minutoxminuto">
        <?php
        $i=0;
        foreach ($jugadoresxequipo_all as $jugador_id => $dorsal) {
            $class=null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?= $class;?> >
                <td style='text-align:center'><?= h($nombres[$jugador_id]); ?>&nbsp;</td>
                <td style='text-align:center'><?= h($apellidos[$jugador_id]); ?>&nbsp;</td>
                <td style='text-align:center'><?= h($apodos[$jugador_id]); ?>&nbsp;</td>
                <td style='text-align:center'><?= (!empty($fechanacimiento[$jugador_id]))?h($fechanacimiento[$jugador_id]):'-'; ?>&nbsp;</td>
                <td style='text-align:center'><?= h($dorsal); ?>&nbsp;</td>
                <!--td class="">
                                <?php // echo $this->Html->link(__(' '), array('action' => 'view', $evento['Juegoevento']['id']),array('class'=>'ver')); ?>
                                <?php// echo $this->Html->link(__(' '), array('action' => 'edit', $evento['Juegoevento']['id']),array('class'=>'editar')); ?>
                            </td-->
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $jugadoresxequipo["Jugadoresxequipo"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Jugadores'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
    <div class="tblvr">
        <?php if (!empty($tarea['Subtarea'])): ?>
            <h2><?php echo __('Sub tareas'); ?></h2><br>
            <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th width="7px"><?php echo __('Id'); ?></th>
                    <th width="25%"><?php echo __('Nombre'); ?></th>
                    <th><?php echo __('Descripción'); ?></th>
                </tr>
                </thead>
                <?php foreach ($tarea['Subtarea'] as $subtarea): ?>
                    <tr>
                        <td><?php echo $subtarea['id']; ?></td>
                        <td class="text-left"><?php echo $subtarea['nombre']; ?></td>
                        <td class="text-left"><?php echo $subtarea['descripcion']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
