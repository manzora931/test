<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'proyecto/impresion')); ?>
<?php echo $this->Html->script('bootstrap.min.js'); ?>
<?php
$page = 1;
$meses = array('01'=>"Enero",'02'=>"Febrero",'03'=>"Marzo",'04'=>"Abril",'05'=>"Mayo",'06'=>"Junio",'07'=>"Julio",'08'=>"Agosto",'09'=>"Septiembre",'10'=>"Octubre",'11'=>'Noviembre','12'=>"Diciembre");
$fecha = date('d-m-Y');
$fechaactual = explode('-', $fecha);
$mes=$meses[$fechaactual[1]];
;?>

<div class="container" style="margin-bottom: 35px;">
    <div class="row">
        <div class="col-print-12">
            <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
            <label class="org"><?= $org ?></label>
            <br>
            <label class="tittle"><?php echo __('Sistema de Monitoreo y Evaluación de Proyectos'); ?></label>
            <div class="linea"></div>
            <label class="encabezado"><?php echo __('Seguimiento de Actividades al '. $fechaactual[0]. ' ' . $mes . ' de ' . $fechaactual[2]); ?></label>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-print-12">
            <div class="actions do-not-print">
                <ul>
                    <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row info-proyecto">
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Código</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['codigo']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Nombre del proyecto (corto)</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['nombrecorto']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Nombre completo del proyecto</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['nombrecompleto']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Tipo de proyecto</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Tipoproyecto']['tipoproyecto']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Descripción</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['descripcion']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Estado</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Estado']['estado']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">País</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Paise']['pais']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Vigencia</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['desde']; ?> - <?= $proyecto['Proyecto']['hasta']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Intitución</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Institucion']['nombre']; ?></label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Creado</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?= $proyecto['Proyecto']['usuario']; ?> (<?= $proyecto['Proyecto']['created']; ?>)</label>
        </div>
        <div class="clearfix"></div>
        <div class="col-print-3">
            <label class="lbl-info-proyecto">Modificado</label>
        </div>
        <div class="col-print-9">
            <label class="val-info-proyecto"><?php if($proyecto['Proyecto']['usuariomodif']) { ?><?= $proyecto['Proyecto']['usuariomodif']; ?> (<?= $proyecto['Proyecto']['modified']; ?>)<?php } ?></label>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <?php if(!is_null($pantalla)) { ?>
            <?= $this->element($pantalla, [
                'data' => $this->data,
                'encabezado' => $encabezado
            ]) ?>
        <?php } else { ?>
            <?php foreach ($pantallas as $key => $row) { ?>
                <div class="">
                    <?= $this->element($row, [
                        'data' => $data[$row],
                        'encabezado' => $data[$key]['encabezado']
                    ]) ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>

</div>
