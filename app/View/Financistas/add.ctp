<?php
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="financistas form container">
    <?php echo $this->Form->create('Financista', array('id'=>'formulario')); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Fuente de Financiamiento'); ?></legend>
        <div class="alert alert-danger col-xs-6" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php echo $this->Form->input('nombre',
                array('label' => 'Nombre',
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group'],
                    'value'=>'',
                    'required'=>'required'));?>
            <div class="form-group">

                <?= $this->Form->input('activo', [
                    'label' => 'Activo',
                    'type' => 'checkbox',
                    'div' => false,
                    'style'=>'margin:5px;'
                ]); ?>
            </div>

        </div>

    </fieldset>
</div>
<div class="actions">

    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div><?php echo $this->Html->link(__('Listado de Fuentes de Financiamiento'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script>
    /*jQuery(function(){
        var validate = {
            "id" : "#FinancistaNombre",
            "form" : "#formulario",
            "message" : "El financista ingresado ya existe, ingrese uno diferente"
        };

        $(validate.id).change( function(){
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_finan';

            validate_name(val, id, url, '');
        });

        $(validate.form).on("submit", function (e) {
            var val = $(validate.id).val();
            var id = 0;
            var url = getURL() + 'val_finan';
            var form = this;
            console.log(form);
            console.log(val);

            e.preventDefault();
            validate_name(val, id, url, form);
        });

        // funcion para validar si nombre ya existe en la base de datos
        function validate_name(val, id, url, form) {
            // console.log('function');
            $.ajax({
                url: url,
                type: 'post',
                data: { val:val, id:id},
                cache: false,
                success: function(resp) {
                    if (resp == "error") {
                        $("#alerta .message").text(validate.message);
                        $("#alerta").slideDown();
                        $(validate.id).val("");

                        setTimeout(function () {
                            $("#alerta").slideUp();
                        }, 4000);
                    }
                    else {
                        form.submit();
                    }
                }
            });
        }
    });*/
</script>
