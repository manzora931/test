<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="contactos view container">
    <h2><?php echo __('Contacto'); ?></h2>
    <table width="100%">
        <tr>
            <td colspan="2">
                <?php
                if( isset( $_SESSION['contacto_save'] ) ) {
                    if( $_SESSION['contacto_save'] == 1 ){
                        unset( $_SESSION['contacto_save'] );
                        ?>
                        <div class="alert alert-success" id="alerta">
                            <span class="icon icon-check-circled"></span>
                            Contacto Almacenado
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
                    <?php }
                } ?>
            </td>
        </tr>
    </table>
    <table id="tblview">
        <tr>
            <td>Id</td>
            <td><?= $contacto['Contacto']['id']; ?></td>
        </tr>
        <tr>
            <td><?php echo __('Institución'); ?></td>
            <td><?php echo $this->Html->link($contacto['Institucion']['nombre'], array('controller' => 'institucions', 'action' => 'view', $contacto['Institucion']['id'])); ?></td>
        </tr>
        <tr>
            <td>Nombre Completo</td>
            <td><?php echo h($contacto['Contacto']['nombres']); ?> <?php echo h($contacto['Contacto']['apellidos']); ?></td>
        </tr>
        <tr>
            <td>Correo Electrónico</td>
            <td><?php echo h($contacto['Contacto']['email']); ?></td>
        </tr>
        <tr>
            <td>Usuario Asociado</td>
            <td><?= (isset($user['User']['username']))?$user['User']['username']:""; ?></td>
        </tr>
        <tr>
            <td>Cargo</td>
            <td><?php echo h($contacto['Contacto']['cargo']); ?></td>
        </tr>
        <tr>
            <td>Teléfono</td>
            <td><?php echo h($contacto['Contacto']['telefono']); ?></td>
        </tr>
        <tr>
            <td>Dirección</td>
            <td><?php echo h($contacto['Contacto']['direccion']); ?></td>
        </tr>
        <tr>
            <td>País</td>
            <td><?php echo h($departamento['Paise']['pais']); ?></td>
        </tr>
        <tr>
            <td>Departamento</td>
            <td><?php echo h($departamento['Departamento']['nombre']); ?></td>
        </tr>
        <tr>
            <td>Municipio</td>
            <td><?php echo h($contacto['Municipio']['nombre']); ?></td>
        </tr>
        <tr>
            <td>Observaciones</td>
            <td><?=$contacto['Contacto']['observaciones']; ?></td>
        </tr>
        <tr>
            <td>Activo</td>
            <td><?= ($contacto['Contacto']['activo']==1)?"Si":"No"; ?></td>
        </tr>
        <tr>
            <td>Creado</td>
            <td><?= $contacto['Contacto']['usuario']." (".$contacto['Contacto']['created'].")"; ?></td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td><?= ($contacto['Contacto']['usuariomodif']!='')?$contacto['Contacto']['usuariomodif']." (".$contacto['Contacto']['modified'].")":""; ?></td>
        </tr>
    </table>
</div>
<div class="actions">

    <div><?php echo $this->Html->link(__('Editar Contacto'), array('action' => 'edit', $contacto['Contacto']['id']),array('class'=>'btn btn-default')); ?> </div>
    <div><?php echo $this->Html->link(__('Listado de Instituciones'), array('controller' => 'institucions', 'action' => 'index'),array('class'=>'btn btn-default')); ?> </div>

</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
