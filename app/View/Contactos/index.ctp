<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<div class="contactos index container-fluid">
	<h2><?php echo __('Contactos'); ?></h2>
	<p>
		<?php
	echo $this->paginator->counter(array(
		'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));	?></p>

		<div id='search_box'>

		<?php
			if(isset($_SESSION['contactos']))
			{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
			}
		?>
            <div class="col-md-offset-10 col-lg-offset-10 col-md-2 col-lg-2" style="text-align: right;padding-top: 0;height: 65px;">
                <div  class="actions" >
                    <div><?php echo $this->Html->link(__('Nuevo Contacto'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
                </div>
            </div>
        </div>

	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('institucion_id'); ?></th>
            <th><?php echo $this->Paginator->sort('nombres'); ?></th>
            <th><?php echo $this->Paginator->sort('apellidos'); ?></th>
            <th><?php echo $this->Paginator->sort('cargo'); ?></th>
            <th><?php echo $this->Paginator->sort('direccion'); ?></th>
            <th><?php echo $this->Paginator->sort('email'); ?></th>
            <th><?php echo $this->Paginator->sort('telefono'); ?></th>
            <th><?php echo $this->Paginator->sort('observaciones'); ?></th>
            <th><?php echo $this->Paginator->sort('activo'); ?></th>

            <th class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
	<?php foreach ($contactos as $contacto): ?>
	<tr>
		<td><?php echo h($contacto['Contacto']['id']); ?>&nbsp;</td>
		<td class="text-left">
			<?php echo $this->Html->link($contacto['Institucion']['nombre'], array('controller' => 'institucions', 'action' => 'view', $contacto['Institucion']['id'])); ?>
		</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['nombres']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['apellidos']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['cargo']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['direccion']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['email']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['telefono']); ?>&nbsp;</td>
		<td class="text-left"><?php echo h($contacto['Contacto']['observaciones']); ?>&nbsp;</td>
		<td><?php echo ($contacto['Contacto']['activo']==1)?"Si":"No"; ?>&nbsp;</td>

	<td class="">
		<?php echo $this->Html->link(__(' ', true), array('action'=>'view', $contacto['Contacto']['id']),array('class'=>'ver')); ?>
        <?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $contacto['Contacto']['id']),array('class'=>'editar')); ?>
    </td>
    </tr>
<?php endforeach; ?>
        </tbody>
	</table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>

</div>


