<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main','proyecto/main']);?>
<script>
    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "1960:<?=date("Y")?>"
        });
        $.datepicker.regional["es"];
    });
</script>
<style>
    .oculto{
        display: none;
    }
    .cursormod{
        cursor: pointer;
    }
</style>
<div class="gastos index container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger" id="error" style="display: none">
                <span class="icon icon-cross-circled"></span>
                <span class="txt"></span>
            </div>
        </div>

        <?php
        if(isset($_SESSION['savegasto'])){
            unset($_SESSION['savegasto']);
            ?>
            <div class="col-sm-12">
                <div class="alert alert-success" id="success" style="display: none">
                    <span class="icon icon-check-circled"></span>
                    <span class="txt">Gasto Almacenado</span>
                </div>
            </div>
        <?php    }
        ?>

    </div>
    <div class="row">
        <div class="col-sm-4">
            <h2 style="font-size: 1.4em;color:#cb071a;">Gastos</h2>
        </div><br>
        <div class="col-sm-offset-2 col-sm-6 div-actions">
            <?php   if($infoP['Proyecto']['estado_id']==2){   ?>
                <table width="100%">
                    <td class="acciones form-group">
                        <button type="button" class="btn btn-crear acciones-shadow" id="add-details" name="button">Agregar</button>
                    </td>
                    <td class="acciones form-group">
                        <button type="button" class="btn btn-acciones acciones-shadow" id="edit-details" name="button">Modificar</button>
                    </td>
                    <td class="acciones form-group">
                        <button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button>
                    </td>
                    <td class="acciones form-group">
                        <!--button type="button" class="btn btn-acciones acciones-shadow" id="delete-details" name="button">Eliminar</button-->
                        <a class="btn btn-imprimir acciones-shadow" href="javascript:window.open('<?php echo Router::url('/'); ?>proyecto/impresion/<?= $idProyecto ?>/gastos', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');">Imprimir  <div class="icono-tringle"></div></a>
                    </td>
                </table>
            <?php   }   ?>
        </div>
        <div class="clearfix"></div>
        <br><br>
    </div>
    <div class="row form-gastos hide">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="encabezado-panel"></span>
                </div>
                <div class="panel-body">
                    <form id="formulario-gastos" method="post" style="padding: 5px;">
                        <table width="100%">
                            <tr>
                                <td style="width: 200px; padding-bottom: 5px;">
                                    <input type="hidden" id="proyecto" value="<?= $idProyecto ?>">
                                    <input type="hidden" id="gastoid">
                                    <?= $this->Form->input('catgasto_id',
                                        array('label' => 'Categoría de Gasto',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'catgasto',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td style="width: 200px; padding-bottom: 5px;">
                                    <?= $this->Form->input('actividade_id',
                                        array('label' => 'Actividad',
                                            'class' => 'form-control',
                                            'div' => ['class'=>'form-groupp','style'=>'margin-right:8px;'],
                                            'value'=>'',
                                            'id' => 'actividad-gasto',
                                            'empty'=>'Seleccionar',
                                            'required'=>'required',
                                            'style'=>'margin-right:8px;')); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td style="width: 120px; padding-bottom: 5px;">
                                    <?= $this->Form->input('fecha',
                                        array('label' => 'Fecha',
                                            'class' => 'form-control datepicker',
                                            'id' => 'fecha-gasto',
                                            'value'=>'',
                                            'placeholder'=>'dd-mm-yyyy',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td style="padding-bottom: 5px;">
                                    <?= $this->Form->input('descripcion',
                                        array('label' => 'Descripción',
                                            'class' => 'form-control',
                                            'id' => 'descripcion-gasto',
                                            'value'=>''
                                        )); ?>
                                </td>
                                <td style="width: 10px"></td>
                                <td style="width: 100px; padding-bottom: 5px;">
                                    <?= $this->Form->input('monto',
                                        array('label' => 'Monto',
                                            'class' => 'form-control',
                                            'id' => 'monto-gasto',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'required'=>'required',
                                            'type'=>"text"
                                        )); ?>
                                </td>
                                <td style="width: 10px;"></td>
                                <td style="padding-bottom: 5px;">
                                    <?= $this->Form->input('comprobante',
                                        array('label' => 'Comprobante',
                                            'class' => 'btn btn-default btn-file',
                                            'id' => 'comprobante',
                                            'div' => ['class'=>'form-groupp'],
                                            'value'=>'',
                                            'type'=>"file",
                                            'style'=>'width: 100%;'
                                        )); ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10"></td>
                                <td>
                                    <div class="acciones-panel pull-right">
                                        <button type="button" class="btn btn-danger acciones-fuente" id="almacenar" name="button" style="width: 100px">Almacenar</button>
                                        <button type="button" class="btn btn-default " id="cancelar" name="button" style="width: 100px">Cancelar</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
        <?php foreach($modules as $modulo) { ?>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span class="stage-title"><?= $modulo['Module']['nombre']?></span>
                        </h4>
                    </div>
                    <div id="frm" class="" role="tabpanel">
                        <div class="panel-body" id="">
                            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
                            <?php if(count($intervenciones[$modulo['Module']['id']]) > 0) { ?>
                                <?php foreach($intervenciones[$modulo['Module']['id']] as $intervencion) { ?>
                                    <div class="panel-info">
                                        <div class="panel-heading" role="tab">
                                            <h4 class="panel-title">
                                                <span class="stage-title" style="color: #000;"><?= $intervencion['nombre'] ?> </span>
                                            </h4>
                                        </div>
                                        <div class="panel-body" id="">
                                            <div class="panel-default">
                                                <div class="panel-heading" role="tab">
                                                    <h4 class="panel-title">
                                                        <span class="stage-title" style="color: #000;">Actividades </span>
                                                    </h4>
                                                </div>
                                                <table id="tblin" border="1" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                                                    <thead>
                                                    <tr>
                                                        <?=($infoP['Proyecto']['estado_id']==2)?"<th class='tbf' width='5%' class='select'></th>":"";?>
                                                        <th class="tbf"><?php echo h('Categoría de Gasto'); ?></th>
                                                        <th class="tbf"><?php echo h('Descripción'); ?></th>
                                                        <th class="tbf"><?php echo h('Actividad'); ?></th>
                                                        <th class="tbf"><?php echo h('Fecha'); ?></th>
                                                        <th class="tbf"><?php echo h('Monto'); ?></th>
                                                        <th class="tbf"><?php echo h('Comprobante'); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    <!-- ---------- RECORRIDO DE LOS GASTOS ------------- -->
                                                    <?php if(isset($gastos[$intervencion['id']])) { ?>
                                                        <?php if(count($gastos[$intervencion['id']]) > 0) { ?>
                                                            <?php $total_monto = 0; ?>
                                                            <?php foreach ($gastos[$intervencion['id']] as $gasto): ?>
                                                                <tr>
                                                                    <?php if($infoP['Proyecto']['estado_id']==2){   ?>
                                                                        <td style="text-align: center !important;">
                                                                            <input type="radio" name="item-selected" class="item-selected" value="<?= $gasto['Gasto']['id'] ?>">
                                                                        </td>
                                                                    <?php } ?>
                                                                    <td style="text-align: left"><?php echo h($gasto['Catgasto']['nombre']); ?></td>
                                                                    <td style="text-align: left"><?php echo h($gasto['Gasto']['descripcion']); ?></td>
                                                                    <td style="text-align: left"><?php echo h($gasto['Actividade']['nombre']); ?></td>
                                                                    <td class="text-center">
                                                                        <?php
                                                                        $mes = date("n",strtotime($gasto['Gasto']['fecha']));
                                                                        echo date("d",strtotime($gasto['Gasto']['fecha']))."-".$meses[$mes]."-".date("Y",strtotime($gasto['Gasto']['fecha']));
                                                                        ?>
                                                                    </td>
                                                                    <td style="text-align: right"><?php echo h("$ " . number_format($gasto['Gasto']['monto'], 2)); ?></td>
                                                                    <td style="text-align: center">
                                                                        <?php
                                                                        if(is_null($gasto['Gasto']['comprobante']) || empty($gasto['Gasto']['comprobante']) || $gasto['Gasto']['comprobante']==='') {?>
                                                                        <?php }else{    ?>
                                                                            <a href="<?= '../../files/adjuntos/' . $gasto['Gasto']['comprobante']; ?>" class="adj" download>
                                                                            <span class="adjunto-gasto" data-gastoid="">
                                                                                <?= $this->Html->image('proyecto/adjuntar_comprobante.png') ?>
                                                                            </span>
                                                                            </a>
                                                                        <?php                                                                       } ?>
                                                                    </td>
                                                                    <?php $total_monto += $gasto['Gasto']['monto']; ?>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr class="totales">
                                                                <?php $cols=($infoP['Proyecto']['estado_id']==2)?5:4;  ?>
                                                                <td colspan="<?=$cols?>" style="text-align: right"><?php echo h('Totales'); ?></td>
                                                                <td style="text-align: right"><?php echo  "$ " . number_format($total_monto, 2); ?>&nbsp;</td>
                                                                <td></td>
                                                            </tr>
                                                        <?php } else {
                                                            $cols=($infoP['Proyecto']['estado_id']==2)?7:6;  ?>
                                                            <tr>
                                                                <td colspan="<?=$cols?>" class="text-center">No hay gastos relacionados</td>
                                                            </tr>
                                                        <?php } ?>
                                                        <!-- ---- FIN IF ISSET GASTOS ------ -->
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ---- FIN FOREACH INTERVENCIONES ------ -->
                                <?php } ?>
                                <!-- ---- FIN IF COUNT INTERVENCIONES ------ -->
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ---- FIN FOREACH MODULOS ------ -->
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    var row = '';
    $("#success").slideDown();
    setTimeout(function(){
        $("#success").slideUp();
    },4000);

    $(function () {
        var btnAdd = $(".btn-crear");
        var btnEdit = $("#edit-details");
        var btnDelete = $("#delete-details");
        var proyecto = '<?=$idProyecto?>';
        var actividadesoptions = $("#actividad-gasto").html();

        // Se verifica si tiene un proyecto asociado
        if(proyecto == '') {
            // Deshabilita las acciones que se realizan en Gastos
            btnAdd.attr('disabled', true);
            btnEdit.attr('disabled', true);
            btnDelete.attr('disabled', true);

        } else {

            // Habilita las acciones que se realizan en Gastos
            btnAdd.attr('disabled', false);
            btnEdit.attr('disabled', false);
            btnDelete.attr('disabled', false);
        }

        // Al momento de hacer click en el boton "Agregar"
        btnAdd.click( function () {
            $("#actividad-gasto").html('');

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se deselecciona el item
                $(".item-selected:checked").attr('checked', false);
            }

            // Se le establece el encabezado del formulario de gastos
            $('.encabezado-panel').html('Agregar Gasto');

            // Se cargan las opciones de la actividad
            $("#actividad-gasto").html(actividadesoptions);

            // Se limpian los campos del formulario
            $('#catgasto').val('');
            $('#actividad-gasto').val('');
            $('#fecha-gasto').val('');
            $('#descripcion-gasto').val('');
            $('#monto-gasto').val('');
            $('#comprobante').val('');

            // Se muestra el formulario
            $('.form-gastos').toggleClass('hide');

            return false;
        });

        // Al momento de hacer click en el boton "Modificar"
        btnEdit.click( function () {
            var url = "<?= Router::url(array('controller' => 'gastos', 'action' => 'get_data')); ?>";
            var id = $(".item-selected:checked").val();
            $("#gastoid").val(id);
            var proyectoid = '<?=$idProyecto;?>';
            $("#actividad-gasto").html('');

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                // Se obtiene la informacion del gasto segun el item seleccionado
                request = $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id:id, proyectoid: proyectoid},
                    dataType: 'json'
                });

                request.done(function (response, textStatus, jqXHR){
                    var fechagasto = response.data[0].gastos.fecha;
                    fechagasto = fechagasto.split("-");

                    // Se le establece el encabezado del formulario de gastos
                    $('.encabezado-panel').html('Editar Gasto');

                    // Se agrega la informacion de las actividades
                    response.actividades.forEach(set_data);

                    // Se carga la informacion segun el gasto seleccionado
                    $('#catgasto').val(response.data[0].gastos.catgasto_id);
                    $('#actividad-gasto').val(response.data[0].gastos.actividade_id);
                    $('#fecha-gasto').val(fechagasto[2] + '-' + fechagasto[1] + '-' + fechagasto[0]);
                    $('#descripcion-gasto').val(response.data[0].gastos.descripcion);
                    $('#monto-gasto').val(response.data[0].gastos.monto);
                    // $('#comprobante').val(response.data[0].gastos.comprobante);

                    // Se muestra el formulario
                    $('.form-gastos').toggleClass('hide');
                });
            }
        });

        // Al momento de hacer click en el boton "Eliminar"
        btnDelete.click( function () {
            var url = "<?= Router::url(array('controller' => 'gastos', 'action' => 'delete')); ?>";
            var id = $(".item-selected:checked").val();
            var proyectoid = '<?=$idProyecto?>';

            // Verifica si existe un item que se encuentre seleccionado
            if ($(".item-selected").is(":checked")) {
                var r = confirm("¿Esta seguro de eliminar el gasto?");

                if (r == true) {
                    request = $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:id, proyectoid: proyectoid},
                        dataType: 'json'
                    });

                    request.done(function (response, textStatus, jqXHR){
                        if(response.msg == 'exito') {
                            $("#success .txt").text('Gasto eliminado exitosamente');
                            $("#success").slideDown();

                            setTimeout(function () {
                                $("#success").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error1') {
                            $("#error .txt").text('Gasto no existe');
                            $("#error").slideDown();

                            setTimeout(function () {
                                $("#error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error2') {
                            $("#error .txt").text('Error al eliminar el gasto');
                            $("#error").slideDown();

                            setTimeout(function () {
                                $("#error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        } else if(response.msg == 'error3') {
                            $("#error .txt").text('El gasto no puede ser eliminado porque la actividad ha sido finalizada');
                            $("#error").slideDown();

                            setTimeout(function () {
                                $("#error").slideUp();
                                $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                            }, 4000);
                        }
                    });
                }
            }

            return false;
        });
        var cont = 0;
        // Al momento de almacenar la informacion ingresada del desembolso
        $('#almacenar').click( function () {
            if(cont === 0){
                cont++;
                var url = "<?= Router::url(array('controller' => 'gastos', 'action' => 'almacenar_data')); ?>";

                // Se obtiene la informacion del archivo adjunto
                var input = document.getElementById('comprobante');
                var data = new FormData();
                var file = input.files[0];

                data.append('archivo',file);
                data.append("id",$("#gastoid").val());
                data.append("proyectoid",'<?=$idProyecto?>');
                data.append("catgasto",$('#catgasto').val());
                data.append("ncatgasto",$("#catgasto  option:selected").text());
                data.append("actividad",$('#actividad-gasto').val());
                data.append("nactividad",$("#actividad-gasto  option:selected").text());
                data.append("fecha",$('#fecha-gasto').val());
                data.append("descripcion",$('#descripcion-gasto').val());
                data.append("monto",$('#monto-gasto').val());

                //$('#tblin').toggleClass('hide');

                request = $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    dataType:'json',
                    cache:false,
                    processData:false,
                });

                request.done(function (response, textStatus, jqXHR){
                    if(response.msg == 'exito') {
                        $('.form-gastos').toggleClass('hide');
                        //$('#tblin').toggleClass('hide');

                        $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                    } else if(response.msg == 'error1') {
                        cont = 0;
                        $('#monto-gasto').focus();
                        $("#error .txt").text('El valor del monto ingresado es inválido');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                            // $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error2') {
                        cont = 0;
                        $('#monto-gasto').focus();
                        $("#error .txt").text('Error al almacenar el desembolso');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                            $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error3') {
                        cont = 0;
                        $('#monto-gasto').focus();
                        $("#error .txt").text('El monto ingresado es mayor al monto de lo presupuestado');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                           // $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error4') {
                        cont = 0;
                        $('#monto-gasto').focus();
                        $("#error .txt").text('El total de gastos es mayor al monto de lo presupuestado');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                          //  $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error5') {
                        cont = 0;
                        $('#monto-gasto').focus();
                        $("#error .txt").text('El total de desembolsos y gastos es mayor al monto de la fuente de financiamiento');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                            $("#childs").load('<?= Router::url(['controller'=>'gastos','action'=>'index',$idProyecto])?>');
                        }, 4000);
                    } else if(response.msg == 'error6') {
                        cont = 0;
                        $("#error .txt").text('Sólo se pueden adjuntar archivos PDF');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                        }, 4000);
                    } else if(response.msg == 'error7') {
                        cont = 0;
                        $("#error .txt").text('El gasto no puede ser modificado porque la actividad ha finalizado');
                        $("#error").slideDown();

                        setTimeout(function () {
                            $("#error").slideUp();
                        }, 4000);
                    }
                });
            }
        });

        // Al momento de hacer click en el boton "Cancelar" del formulario
        $('#cancelar').click( function () {
            $('.form-gastos').toggleClass('hide');
        });
    });

    function set_data(item, index) {
        var select = document.getElementById('actividad-gasto');
        var opt = document.createElement('option');

        opt.value = item.Actividade.id;
        opt.innerHTML = item.Actividade.nombre;
        select.appendChild(opt);
    }
</script>