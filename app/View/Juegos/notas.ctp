<?php $this->layout = 'ajax'; ?>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12" style="border-bottom: solid 1px #011880;">
            <span>Notas</span>
            <?php if(empty($view)): ?>
            <a data-toggle="modal" href="#addNota" class="btn btn-crear pull-right" id="btn-notas">Nueva nota</a>
            <?php endif; ?>
        </div>
        <?php if(count($notas) > 0) { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th><?php echo 'Nota'; ?></th>
                        <th width="20%"><?php echo 'Jugador' ?></th>
                        <?php if(empty($view)): ?>
                        <th><?php echo '' ?></th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    foreach ($notas as $key => $nota) {
                        $class=null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                        ?>
                        <tr <?= $class;?> id="nota_<?=$nota['Juegnota']['id']?>">
                            <td style='text-align:left'><?= h($nota['Juegnota']['nota']); ?>&nbsp;</td>
                            <td style='text-align:center'>
                                <?php
                                if($nota['Jugadore']["id"]!=null){
                                    echo "<span style='font-size: 13px;'>".$dorsalJugadores[$nota['Jugadore']["id"]]."</span>"." - ".$nota['Jugadore']['nombre_jugador'];
                                }
                                ?>&nbsp;</td>
                            <?php if(empty($view)): ?>
                            <td class="">
                                <a class="editar editarNotas" style="cursor:pointer;"></a>
                                <a class="deleteDet deleteNotas" style="cursor:pointer;"></a>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <p class="text-center">No hay información registrada</p>
            </div>
        <?php } ?>
    </div>
</div>
<!-- Modal Para Registrar Nueva Nota -->
<div class="modal fade" id="addNota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    <i class="fa fa-sticky-note"></i> Registrar Nueva Nota
                </h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="alerta_nota" style="display: none">
                    <span class="icon icon-check-circled" id="msjalert">El jugador es requerido</span>
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
                <form id="formulario-notas" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="clearfix"></div>
                            <div class="col-xs-12">
                                <div class="form-groupp">
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioNotas" id="rbNotasEqlocal"  class="rb-equipo-seleccionado" checked>Equipo Local</label>
                                    </div>
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioNotas" id="rbNotasEqvisitante" class="rb-equipo-seleccionado">Equipo Visitante</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-groupp">
                                    <label class="lbl-equipo-seleccionado"></label>
                                </div>
                                <hr>
                            </div>
                            <div class="col-xs-9">
                                <?php echo	$this->Form->input('jugadornotas_id',
                                    array('label'   => 'Jugador',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'jugadornotas-id',
                                        'empty'   =>'Seleccionar'));
                                ?>
                            </div>
                            <div class="col-xs-9">
                                <?= $this->Form->input('txtnotas',[
                                        'label'=>"Nota",
                                        'class'=>"form-control validate[required] form-contacto",
                                        'div'=>['class'=>"form-groupp"],
                                        'placeholder'=>"Nota",
                                        'rows'=>3,
                                        'style'=>'margin-bottom: 15px']);
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-danger" id="registrar-nota">Almacenar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var tipo_accion='add';
    var prefijo_nota="nota_";
    var nota_id=0;
    $(document).ready(function() {
        var juegoid = $( '#juego-id' ).val();
        var notas_url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
        var torneo = $("#JuegoTorneoId").val();
        // Al momento de cargar la pagina
        $.ajax({
            url: notas_url,
            type: "post",
            cache: false,
            data: {id:juegoid,torneo:torneo},
            dataType: 'json',
            success:function(resp){
                $("#jugadornotas-id").html(resp.jugadores);
                $("#jugadornotas-id").val('');
                $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
            }
        });

        // Si se selecciona al equipo local en el modal
        $('#rbNotasEqlocal').change(function (e) {
            var id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
            var torneo = $("#JuegoTorneoId").val();
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadornotas-id").html(resp.jugadores);
                    $("#jugadornotas-id").val('');
                    $("#txtnotas").val('');
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
                }
            });
        });

        // Si se selecciona al equipo visitante en el modal
        $('#rbNotasEqvisitante').change(function (e) {
            var id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_visitante')); ?>";
            var torneo = $("#JuegoTorneoId").val();
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadornotas-id").html(resp.jugadores);
                    $("#jugadornotas-id").val('');
                    $("#txtnotas").val('');
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
                }
            });
        });

        //Al momento de hacer click en almacenar una nota del juego
        $("#registrar-nota").click(function() {
            var juego_id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_nota_juego')); ?>";

            // Se obtiene la informacion ingresada de la nota del juego
            var formData = $('#formulario-notas').serializeArray();

            // Se adiciona el id del juego a la informacion ingresada de la nota del juego
            formData.push({name: 'data[juego_id]', value: juego_id});

            formData.push({name: 'data[tipo]', value: tipo_accion});

            formData.push({name: 'data[nota_id]', value: nota_id});
            var jugador = $("#jugadornotas-id").val();
            //if(jugador!=''){
                request = $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: formData,
                    dataType: 'json'
                });

                request.done(function (response, textStatus, jqXHR){
                    if(response.msg != 'error') {
                        location.reload(true);
                    }
                });
            /*}else{
                $("#alerta_nota").slideDown();
                setTimeout(function () {
                    $("#alerta_nota").slideUp();
                },4000);
            }*/
            return false;
        });
    });

    function editarNota(){
        tipo_accion='edit';
        var label="<i class='fa fa-group'></i> Editar Nota";
        document.getElementById('myModalLabel').innerHTML=label;
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_nota);
        var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'getjuegonota')); ?>";
        nota_id=id[1];
        $.ajax({
            url: url,
            type: "post",
            cache: false,
            data: {id:id[1]},
            dataType: 'json',
            success:function(resp){
                if(resp.chkequipolocal){ 
                    document.getElementById('rbNotasEqlocal').checked=true; 
                }
                else{ 
                    document.getElementById('rbNotasEqvisitante').checked=true;
                }
                //llenado de jugadores
                var camposelect=resp.select1_equipo;
                document.getElementById('jugadornotas-id').innerHTML="";
                crearOptionSelect('Seleccionar','','jugadornotas-id');
                for (var i = 0; i < camposelect.length; i++) {
                    var element=camposelect[i]['Jugadore']['nombre_jugador'];
                    var index=camposelect[i]['Jugadore']['id'];
                    crearOptionSelect(element,index,'jugadornotas-id');
                };
                if(resp.jugadore_id!=null){
                    document.getElementById('jugadornotas-id').value=resp.jugadore_id;
                }

                //nombrando equipo seleccionado
                var className=document.getElementsByClassName('lbl-equipo-seleccionado');
                for (i = 0; i < className.length; i++) {
                    className[i].innerText=resp.nombre_equipo;
                }

                //llenado campo minutos
                document.getElementById('txtnotas').value=resp.nota;
                $('#addNota').modal('show');              
            }
        });
    }

    function deleteNotas(){
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_nota);
        nota_id=id[1];
        bootbox.confirm({
            message: "¿Está seguro de eliminar el registro?, perderá los datos que se han ingresado.",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result) {
                    var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'deletejuegonota')); ?>";
                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:nota_id},
                        dataType: 'json',
                        success:function(resp){
                            bootbox.alert(resp.msj);
                            if(resp.resp){
                                location.reload(true);
                            }                                                  
                        }
                    });                                
                }
            }
        });
    }

    function btnaddNota(){
        tipo_accion ='add';
        nota_id=0;
        var label="<i class='fa fa-group'></i> Registrar Nueva Nota";
        document.getElementById('myModalLabel').innerHTML=label;
        if($("#rbNotasEqlocal").is(':checked')) {
            $('#rbNotasEqlocal').change();
        }else{
            $('#rbNotasEqvisitante').change();            
        }
    }

    var className=document.getElementsByClassName('editarNotas');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarNota,false);
    }
    var className=document.getElementsByClassName('deleteNotas');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteNotas,false);
    }
    document.getElementById('btn-notas').addEventListener('click',btnaddNota,false);
    
</script>