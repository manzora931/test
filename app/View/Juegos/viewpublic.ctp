<?php 
//echo $this->Html->css(['proyecto/main']);
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
?>
<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main',"plantillas/".$host.".css"]);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script(['funciones/addremoveclass']) ?>
<style>

    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
</style>

<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<?php
    $meses = [1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre"];
    $dias = [0=>"Domingo",1=>"Lunes",2=>"Martes",3=>"Miercoles",4=>"Jueves",5=>"Viernes",6=>"Sabado"];

?>
<div class="juegos view">
    <br>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading" id="seccionjuego">
                <h4 class="panel-title">
                    <a id="btn-proy" role="button" class="collapsed" data-toggle="collapse" href="" data-target="#frm" aria-expanded="true">
                        <span class="stage-title" id="juego" data-proyecto="<?=$id?>">Juego: <?=$juego['Equipo1']['nombrecorto']?> vrs <?=$juego['Equipo2']['nombrecorto']?> </span>
                    </a>
                </h4>
            </div>
            <div class="row"><br>
                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="col-md-12 col-xs-12 head-juegos">
                        <div class="col-xs-5 col-md-5">
                            <img class="escudo1" src="../../../<?=$juego['Equipo1']["fotoescudo"]?>">
                            <span class="equipo1"><?=$juego['Equipo1']['nombrecorto']?></span>
                        </div>
                        <div class="col-xs-2 col-md-2" style="display: flex;justify-content: center;align-content: center;flex-direction: column;">
                            <span class="marcador"><?=($juego['Juego']['marcadore1']!=null)?$juego['Juego']['marcadore1']." - ":'0 - ';?>
                                <?=($juego['Juego']['marcadore2']!='')?$juego['Juego']['marcadore2']:"0 "?>
                            </span>
                        </div>
                        <div class="col-md-5 col-xs-5">
                            <span class="equipo1"><?=$juego['Equipo2']['nombrecorto']?></span>
                            <img class="escudo2" src="../../../<?=$juego['Equipo2']["fotoescudo"]?>">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl" style="margin-top: 15px;">
                        <span>ID del juego: </span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato" style="margin-top: 15px;">
                        <span><?=$juego['Juego']['id']?></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <?php
                        $d= date("w",strtotime($juego['Juego']['fecha']));
                        $dia = date("d",strtotime($juego['Juego']['fecha']));
                        $mes = date("n",strtotime($juego['Juego']['fecha']));
                        $year = date("Y",strtotime($juego['Juego']['fecha']));
                        ?>
                        <span>Fecha del encuentro:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=$dias[$d]." ".$dia." de ".$meses[$mes]." ".$year?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Hora: </span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=date("H:i",strtotime($juego["Juego"]["hora"]));?>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="col-md-12 col-xs-12 head-juegos">
                        <span class="subtitulo-view">Datos generales</span>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl" style="margin-top: 15px;">
                        <span>Torneo: </span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato" style="margin-top: 15px;">
                        <span> <?=$juego['Torneo']['nombrecorto']?></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Etapa:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=$juego['Etapa']['etapa']?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Fase de Grupos:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Grupo']['grupo']))?$juego['Grupo']['grupo']:'--'?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Estadio:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=$juego['Estadio']['estadio'];?>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="col-md-12 col-xs-12 head-juegos">
                        <span class="subtitulo-view">Más Información</span>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl" style="margin-top: 15px;">
                        <span>Temperatura: </span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato" style="margin-top: 15px;">
                        <span><?=(!empty($juego['Juego']['temperatura']))?$juego['Juego']['temperatura']:'0'?> °C</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Asistencia:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Juego']['asistencia']))?$juego['Juego']['asistencia']:'0'?> aficionados
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Taquilla:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Juego']['taquilla']))?$juego['Juego']['taquilla']:'0'?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Porcentaje utilizado en estadio:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego["Juego"]["porcestadio"]))?$juego["Juego"]["porcestadio"]."%":"0%"?>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="col-md-12 col-xs-12 head-juegos">
                        <span class="subtitulo-view">Árbitros</span>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12 seccion1">
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl" style="margin-top: 15px;">
                        <span>Principal: </span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato" style="margin-top: 15px;">
                        <span><?=(!empty($juego['Juego']['referi']))?$arbitros[$juego['Juego']['referi']]:'--'?></span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Auxiliar 1:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Juego']['referiaux1']))?$arbitros[$juego['Juego']['referiaux1']]:'--'?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>Auxiliar 2:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Juego']['referiaux2']))?$arbitros[$juego['Juego']['referiaux2']]:'--'?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4 col-xs-4 grid-lbl">
                        <span>4o. árbitro:</span>
                    </div>
                    <div class="col-md-4 col-xs-4 grid-dato">
                        <?=(!empty($juego['Juego']['referi2']))?$arbitros[$juego['Juego']['referi2']]:'--'?>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 marg-panel">
                    <ul class="nav nav-pills">
                        <li id="alineacion" class="option_childs"><a>Alineación</a></li>
                        <li id="estadistica" class="option_childs"><a>Estadisticas</a></li>
                        <li id="minuto" class="option_childs"><a>Minuto a minuto</a></li>
                        <li id="notas" class="option_childs"><a>Notas</a></li>
                        <li id="cronica" class="option_childs"><a>Crónica/Video</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="container-fluid child" id="childs">
                </div>
            </div>

        </div>
    </div>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Listado de Juegos'), array('action' => 'listjuegos',$juego['Torneo']["id"],$host),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    var id_juego=<?=$id?>;
    var equipo1=<?=$juego['Juego']['equipo1_id']?>;
    var equipo2=<?=$juego['Juego']['equipo2_id']?>;
    function loadChild(){
        var id_child=this.id;
        var className=document.getElementsByClassName('option_childs');
        for (var i = 0; i < className.length; i++) {
            removeClass(className[i],'activa');
        };
        addClass(this,'activa');
        if(id_child=='alineacion'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'alineacionespublic')); ?>",
            {id: id_juego, equipo1: equipo1, equipo2: equipo2, view:true}
            );
        }
        else if(id_child=='notas'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'notaspublic')); ?>",
            {id: id_juego, view:true}
            );
        }
        else if(id_child=='cronica'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'cronicapublic')); ?>",
            {id: id_juego, view:true}
            );
        }
        else if(id_child=='estadistica'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'estadisticaviewpublic',)); ?>",
                {id: id_juego}
            );
        }else{
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'minutoxminutopublic',)); ?>",
            {id: id_juego, view:true}
            );
        }

    }

    var className=document.getElementsByClassName('option_childs');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',loadChild,false);
    };
    function updatePage() {
        //location.reload();
    }
    jQuery(function(){
        var url = getURL();
        $("#alertaP").slideDown();
        setTimeout(function () {
            $("#alertaP").slideUp();
        }, 4000);
        $("#minuto").click();
        //setTimeout(updatePage,30000);
    });
</script>
