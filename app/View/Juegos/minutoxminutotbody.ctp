<?php $this->layout = 'ajax'; ?>
<?php
$i=0;
foreach ($eventos as $key => $evento) {
    $class=null;
    if ($i++ % 2 == 0) {
        $class = ' class="altrow"';
    }
    ?>
    <tr <?= $class;?> id="minutoxminuto_<?=$evento['Juegoevento']['id']?>">
        <td style='text-align:center'>
           <?php  
            if($evento['Juegoevento']['minadicional']!=''){
                echo $evento['Juegoevento']['minuto']."<span style='margin-left:2px;color:#6048D1;'>+".$evento['Juegoevento']['minadicional']."</span>";
            }else{
                echo $evento['Juegoevento']['minuto'];
            }
                                ?>
        &nbsp;</td>
        <td style='text-align:left'><span style="font-size: 13px;"><?=(isset($dataJugador[$evento['Juegoevento']['jugador_id']]["dorsal"]))?$dataJugador[$evento['Juegoevento']['jugador_id']]["dorsal"]."-":"-"; ?>&nbsp;&nbsp;</span><?= (isset($dataJugador[$evento['Juegoevento']['jugador_id']]["nombre"]))?$dataJugador[$evento['Juegoevento']['jugador_id']]["nombre"]:"-"; ?>&nbsp;</td>
        <td class="text-center"><?=(isset($dataJugador[$evento['Juegoevento']['jugador_id']]["equipo"]))?$dataJugador[$evento['Juegoevento']['jugador_id']]["equipo"]:"-";?></td>
        <td style='text-align:center'><?= h($evento['Evento']['evento']); ?>&nbsp;</td>
        <td style='text-align:center'><?= h($evento['Detevento']['detevento']); ?>&nbsp;</td>
        <td style='<?=(!empty($evento['Juegoevento']['jugador2_id']))?'text-align:left':'text-align:center'?>'><?=(!empty($evento['Juegoevento']['jugador2_id']))?h($dataJugador[$evento['Juegoevento']['jugador2_id']]["nombre"]):'-'; ?>&nbsp;</td>
        <td style='text-align:left'><?= h($evento['Juegoevento']['comentario']); ?>&nbsp;</td>
        <td class="">
            <a class="editar editarMinutoxminuto" style="cursor:pointer;"></a>
            <a class="deleteDet deleteMinutoxminuto" style="cursor:pointer;"></a>
        </td>
    </tr>
<?php } ?>
<script>
    var className=document.getElementsByClassName('editarMinutoxminuto');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarMinutoxminuto,false);
    }
    var className=document.getElementsByClassName('deleteMinutoxminuto');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteMinutoxminuto,false);
    }
</script>
                    