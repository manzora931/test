<?php //echo $this->Html->Script('buscar',FALSE); ?>
<?php //echo $this->Html->Script('jquery',FALSE); ?>
<?php //echo $this->Html->Script('jquery-ui.js',FALSE); ?>
<?php //echo $this->html->Script('url'); ?>
<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<div class="juegos index container-fluid">
	<h2><?php echo __('Juegos'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "juegos";
    $session = $this->Session->read('tabla[juegos]');
    $search_text = $session['search_text'] ;
    ?>
    <div id='search_box'>
        <div class="col-md-10 col-lg-10">
            <?php
            /*Inicia formulario de busqueda*/
            $tabla = "juegos";
            ?>
            <?php
            echo $this->Form->create($tabla, array('action'=>'index' ,'id'=>'tblstyle',' style'=>'width: 70%;'));
            echo "<input type='hidden' name='_method' value='POST' />";
            echo "<table style ='width: 100%; border:none; background-color: transparent;'>";
            echo "<tr>";
            //echo "<td width='400px'>";
            $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
            echo $this->Form->hidden('SearchText', array('class'=>'form-control','label'=>'Buscar por: ', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text, 'placeholder' => 'Nombre'));
            //echo "</td>";
            echo "<td style='width: 210px; padding-left: 4px;'>";
            echo $this->Form->input('torneo_id',array(
                'class'=>'form-control',
                'name'=>'data['.$tabla.'][torneo_id]',
                'options'=>$torneos,
                'label'=>'Torneo',
                'type'=>'select',
                "onchange"=>"getEquipos(this.value)",
                'empty' => array( '(Seleccionar)'),
                'default'=>(isset($_SESSION['tabla[juegos]']))?$_SESSION['tabla[juegos]']['torneo_id']:''
            ));
            echo "</td>";
            echo "<td style='width: 210px; padding-left: 4px;'>";
            echo $this->Form->input('estadio_id',array(
                'class'=>'form-control',
                'name'=>'data['.$tabla.'][estadio_id]',
                'options'=>$estadios,
                'label'=>'Estadio',
                'type'=>'select',
                'empty' => array( '(Seleccionar)'),
                'default'=>(isset($_SESSION['tabla[juegos]']))?$_SESSION['tabla[juegos]']['estadio_id']:''
            ));
            echo "</td>";
            echo "<td style='width: 210px; padding-left: 4px;'>";
            echo $this->Form->input('equipo_id',array(
                'class'=>'form-control',
                'name'=>'data['.$tabla.'][equipo_id]',
                'label'=>'Equipo',
                'type'=>'select',
                'empty' => array( '(Seleccionar)')
            ));
            echo "</td>";
            echo "<td  width='300px' style='float:right; padding-top: 20px; '>";
            echo $this->Form->hidden('Controller', array('value'=>'Juego', 'name'=>'data[tabla][controller]')); //hacer cambio
            echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
            echo $this->Form->hidden('Parametro1', array('value'=>'', 'name'=>'data[tabla][parametro]'));
            echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
            echo $this->Form->hidden('campoFecha', array('value'=>'fechainicio', 'name'=>'data['.$tabla.'][campoFecha]'));
            echo $this->Form->hidden('FechaHora', array('value'=>'no', 'name'=>'data['.$tabla.'][FechaHora]'));
            echo " <input type='submit' class='btn btn-default' value='Buscar' style=' margin-left:15px; width: 100px;border-color: #606060;'>";

            echo "</td>";
            echo "</tr>";
            echo "</table>";
            ?>
            <?php
            if(isset($_SESSION['juegos']))
            {
                $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
                echo $this->Form->button('Ver todos', array('class'=>'btn btn-info pull-left','type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
            }
            ?>
        </div>
       <div class="col-md-2 col-lg-2" style="text-align: right;">
           <div  class="actions">
               <div><?php echo $this->Html->link(__('Nuevo Juego'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
           </div>
       </div>

    </div>

	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('fecha', 'Fecha y hora'); ?></th>
			<th><?php echo $this->Paginator->sort('torneo_id', 'Torneo'); ?></th>
			<th><?php echo $this->Paginator->sort('estadio_id', 'Estadio'); ?></th>
			<th><?php echo $this->Paginator->sort('equipo1_id', 'Equipo Local'); ?></th>
			<th><?php echo $this->Paginator->sort('equipo2_id', 'Equipo Visitante'); ?></th>
			<th><?php echo __('Marcador Final'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($juegos as $juego):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?php echo h($juego['Juego']['id']); ?>&nbsp;</td>
				<td style='text-align:center'><?php echo h(explode('-', $juego['Juego']['fecha'])[2] . '-' . explode('-', $juego['Juego']['fecha'])[1] . '-' . explode('-', $juego['Juego']['fecha'])[0] . ' ' . $juego['Juego']['hora']); ?>&nbsp;</td>
				<td style='text-align:center';><?php echo $this->html->link($juego['Torneo']['torneo'], array('controller'=> 'torneos', 'action'=>'view', $juego['Juego']['torneo_id'])); ?></td>
				<td style='text-align:center';><?php echo $this->html->link($juego['Estadio']['estadio'], array('controller'=> 'estadios', 'action'=>'view', $juego['Juego']['estadio_id'])); ?></td>
				<td style='text-align:center';><?php echo $this->html->link($equip[$juego['Juego']['equipo1_id']], array('controller'=> 'equipos', 'action'=>'view', $juego['Juego']['equipo1_id'])); ?></td>
				<td style='text-align:center';><?php echo $this->html->link($equip[$juego['Juego']['equipo2_id']], array('controller'=> 'equipos', 'action'=>'view', $juego['Juego']['equipo2_id'])); ?></td>
				<td><?php echo h($juego['Juego']['marcadore1'] . ' - ' . $juego['Juego']['marcadore2']); ?>&nbsp;</td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $juego['Juego']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $juego['Juego']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>

</div>
<script>
    jQuery(function () {
        var torneo = $("#juegosTorneoId").val();
        if(torneo>0){
            getEquipos(torneo);
        }
    });
    function getEquipos(id) {
        var url = getURL()+"getEquipos";
        console.log(id);
        if(id>0){
            $.ajax({
                url: url,
                type: 'post',
                data: {id:id},
                async:false,
                cache: false,
                success:function (resp) {
                    $("#juegosEquipoId").html(resp);
                }
            });
        }
    }
</script>