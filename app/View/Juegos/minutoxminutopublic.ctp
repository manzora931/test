<?php $this->layout = 'ajax'; ?>
<?= $this->Html->script(['funciones/showalert','funciones/addremoveclass']) ?>
<style>
    .display-none{
        display: none;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12">
            <div class="alert alert-success no-display" id="alert-modal-minuto-success" style="display: none;top:8px;">
                <span class="icon icon-cross-circled"></span>
                <span class="message" id="message-modal-minuto-success"></span>
                <button type="button" class="close" data-dismiss="alert"></button>
            </div>
        </div>
        <div class="col-xs-12" style="border-bottom: solid 1px #011880;">
            <span class="subtitulo-view">Minuto a minuto</span>
            <?php if(empty($view)): ?>
                <a data-toggle="modal" href="#addEvento" class="btn btn-crear pull-right" id="btn-eventos">Nuevo evento</a>
            <?php else: ?>
                <div class="col-xs-2 pull-right" style="padding-right:0px;">
                    <a class="btn btn-crear pull-right btn-update-minutos" onclick="updatePage();" style="padding:0px; width:100%;">
                        <i class="fa fa-refresh"></i>
                        <span class="actualizar-en" style="margin-left:5px;">Actualizar en:</span>
                        <span class="span-update-minutos" style="margin-left:5px;">60</span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
        <?php if(count($eventos) > 0) { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <table cellpadding="0" cellspacing="0" id="table-listjuegos" class="table table-condensed table-striped ">
                    <thead>
                    <tr>
                        <th><a>Minuto</a></th>
                        <th><a>Jugador</a></th>
                        <th><a>Equipo</a></th>
                        <th><a>Evento</a></th>
                        <th><a>Detalle</a></th>
                        <th><a>Jugador Adversario/ Saliente</a></th>
                        <th><a>Comentario</a></th>
                        <?php if(empty($view)): ?>
                            <th style="width:100px;"><?php echo '' ?></th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody id="tbody-minutoxminuto">
                    <?php
                    $i=0;
                    foreach ($eventos as $key => $evento) {
                        $class=null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                        ?>
                        <tr <?= $class;?> id="minutoxminuto_<?=$evento['Juegoevento']['id']?>">
                            <td style='text-align:center'>
                                <?php
                                if($evento['Juegoevento']['minadicional']!=''){
                                    echo $evento['Juegoevento']['minuto']."<span style='margin-left:2px;color:#6048D1;'>+".$evento['Juegoevento']['minadicional']."</span>";
                                }else{
                                    echo $evento['Juegoevento']['minuto'];
                                }
                                ?>
                                &nbsp;</td>
                            <td style='text-align:left'><span style="font-size: 13px;"><?=$dataJugador[$evento['Juegoevento']['jugador_id']]["dorsal"]."-"; ?>&nbsp;&nbsp;</span><?= ($dataJugador[$evento['Juegoevento']['jugador_id']]["nombre"]); ?>&nbsp;</td>
                            <td class="text-center"><?=$dataJugador[$evento['Juegoevento']['jugador_id']]["equipo"];?></td>
                            <td style='text-align:center'><?= h($evento['Evento']['evento']); ?>&nbsp;</td>
                            <td style='text-align:center'><?= h($evento['Detevento']['detevento']); ?>&nbsp;</td>
                            <td style='<?=(!empty($evento['Juegoevento']['jugador2_id']))?'text-align:left':'text-align:center'?>'><?=(!empty($evento['Juegoevento']['jugador2_id']))?h($dataJugador[$evento['Juegoevento']['jugador2_id']]["nombre"]):'-'; ?>&nbsp;</td>
                            <td style='text-align:left'><?= h($evento['Juegoevento']['comentario']); ?>&nbsp;</td>
                            <?php if(empty($view)): ?>
                                <td class="">
                                    <a class="editar editarMinutoxminuto" style="cursor:pointer;"></a>
                                    <a class="deleteDet deleteMinutoxminuto" style="cursor:pointer;"></a>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <p class="text-center">No hay información registrada</p>
            </div>
        <?php } ?>
    </div>
</div>
<!-- Modal Para Registrar Nuevo Evento -->
<div class="modal fade" id="addEvento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    <i class="fa fa-tasks"></i> Registrar Nuevo Evento
                </h3>
            </div>
            <div class="modal-body">
                <form id="formulario-eventos" method="post">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="alert alert-success no-display" id="alert-modal-minuto-success" style="top:8px;">
                                <span class="icon icon-cross-circled"></span>
                                <span class="message" id="message-modal-minuto-success"></span>
                                <button type="button" class="close" data-dismiss="alert"></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12">
                                <div class="form-groupp">
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioEventos" id="rbEventosEqlocal"  class="rb-equipo-seleccionado" checked>Equipo Local</label>
                                    </div>
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioEventos" id="rbEventosEqvisitante" class="rb-equipo-seleccionado">Equipo Visitante</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-groupp">
                                    <label class="lbl-equipo-seleccionado"></label>
                                </div>
                                <hr>
                            </div>
                            <div class="col-xs-9">
                                <?php echo	$this->Form->input('jugadoreventos_id',
                                    array('label'   => 'Jugador',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'jugadoreventos-id',
                                        'empty'   =>'Seleccionar',
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="col-xs-4">
                                <?= $this->Form->input('minutoeventos',[
                                    'label'=>'Minuto',
                                    'placeholder'=>'Minuto',
                                    'class'=>'form-control validate[required] form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'id'      => 'minutoeventos',
                                    'required'=>'required']);
                                ?>
                            </div>
                            <div class="col-xs-4 col-xs-offset-1">
                                <?= $this->Form->input('minadicional',[
                                    'label'=>'Minuto Adicional',
                                    'placeholder'=>'',
                                    'class'=>'form-control form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'id'      => 'minadicional']);
                                ?>
                            </div>
                            <div class="col-xs-5">
                                <?php echo	$this->Form->input('eventoeventos_id',
                                    array('label'   => 'Evento',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'eventoeventos-id',
                                        'empty'   =>'Seleccionar',
                                        'options'   =>$meventos,
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="col-xs-4">
                                <?php echo	$this->Form->input('deteventoeventos_id',
                                    array('label'   => 'Detalle',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'deteventoeventos-id',
                                        'empty'   =>'Seleccionar',
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-9 display-none">
                                <?php echo  $this->Form->input('jugadoreventos2_id',
                                    array('label'   => '',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'jugadoreventos2-id',
                                        'empty'   =>'Seleccionar',
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-9">
                                <?= $this->Form->input('comentarioeventos',[
                                    'label'=>'Comentario',
                                    'placeholder'=>'Comentario',
                                    'class'=>'form-control form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'id'      => 'comentarioeventos']);
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-danger" id="registrar-evento">Almacenar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var chk_equipos='local';
    var tipo = $( "#eventoeventos-id option:selected" ).text();
    var tipo_accion='add';
    var prefijo_minutoxminuto="minutoxminuto_";
    var juegoevento_id=0;
    $(document).ready(function() {
        var juegoid = $( '#juego-id' ).val();
        var eventos_url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
        var torneo = $("#JuegoTorneoId").val();
        // Al momento de cargar la pagina
        /*$.ajax({
            url: eventos_url,
            type: "post",
            cache: false,
            data: {id:juegoid, torneo:torneo},
            dataType: 'json',
            success:function(resp){
                $("#jugadoreventos-id").html(resp.jugadores);
                $("#jugadoreventos-id").val('');
                $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
            }
        });*/

        // Si se selecciona al equipo local en el modal
        $('#rbEventosEqlocal').change(function (e) {
            chk_equipos='local';
            var id = $( '#juego-id' ).val();
            var torneo = $("#JuegoTorneoId").val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";

            /*$.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadoreventos-id").html(resp.jugadores);
                    $("#jugadoreventos-id").val('');
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
                }
            });*/
            $('#minutoeventos').val('');
            $('#comentarioeventos').val('');
            $('#eventoeventos-id').val('');
            $('#eventoeventos-id').val('');
            $('#deteventoeventos-id').val('');
            tipo = $( "#eventoeventos-id option:selected" ).text();
            chkEquipo(tipo);
            showJugador2(tipo);
        });

        // Si se selecciona al equipo visitante en el modal
        $('#rbEventosEqvisitante').change(function (e) {
            chk_equipos='visitante';
            var id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_visitante')); ?>";
            var torneo = $("#JuegoTorneoId").val();
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadoreventos-id").html(resp.jugadores);
                    $("#jugadoreventos-id").val('');
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
                }
            });
            $('#minutoeventos').val('');
            $('#comentarioeventos').val('');
            $('#eventoeventos-id').val('');
            $('#eventoeventos-id').val('');
            $('#deteventoeventos-id').val('');
            tipo = $( "#eventoeventos-id option:selected" ).text();
            chkEquipo(tipo);
            showJugador2(tipo);
        });

        // Al momento de seleccionar un evento
        $('#eventoeventos-id').change(function (e) {
            var id = $( this ).val();
            var tipo = this.options[this.selectedIndex].text;
            showJugador2(tipo);
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'get_info_evento')); ?>";

            if(id != '') {
                $.ajax({
                    url: url,
                    type:'post',
                    data: {id:id},
                    success:function(resp){
                        $("#deteventoeventos-id").html(resp);
                        $("#deteventoeventos-id").val('');
                    }
                });
            }
        });

        //Al momento de hacer click en almacenar un evento del juego
        $("#registrar-evento").click(function() {
            var juego_id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_evento_juego')); ?>";

            // Se obtiene la informacion ingresada del evento del juego
            var formData = $('#formulario-eventos').serializeArray();

            // Se adiciona el id del juego a la informacion ingresada de la alineacion
            formData.push({name: 'data[juego_id]', value: juego_id});

            formData.push({name: 'data[tipo]', value: tipo_accion});

            formData.push({name: 'data[juegoevento_id]', value: juegoevento_id});
            request = $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: formData,
                dataType: 'json'
            });

            request.done(function (response, textStatus, jqXHR){
                if(response.msg == 'exito') {
                    $('#tbody-minutoxminuto').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'minutoxminuto')); ?>",
                        {id: juegoid, render:true}
                    );
                    $('#JuegoMarcadore1').val(response.marcadore1);
                    $('#JuegoMarcadore2').val(response.marcadore2);
                    $('#JuegoMarcadore1pt').val(response.marcadore1pt);
                    $('#JuegoMarcadore2pt').val(response.marcadore2pt);
                    $("#addEvento").modal("hide");
                    showAlert('Evento registrado exitosamente','modal-minuto-success');
                }else{
                    showAlert(response.msg,'modal-minuto');
                }
            });

            return false;
        });
    });

    function showJugador2(tipo){
        $("#jugadoreventos2-id").val('');
        var id=document.getElementById('jugadoreventos2-id');
        var padre=id.parentNode.parentNode;
        if(tipo=='Falta'){
            chkEquipo(tipo);
            removeClass(padre,'display-none');
        }
        else if(tipo=='Cambio'){
            chkEquipo(tipo);
            removeClass(padre,'display-none');
        }else{
            addClass(padre,'display-none');
        }
    }

    function chkEquipo(tipo, text){
        var id = $( '#juego-id' ).val();
        if(tipo=='Seleccionar' || tipo=='Falta'){//tipo = undefined = falta
            text="Jugador Adversario";
            if(chk_equipos=='local'){
                var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_visitante')); ?>";
            }else{
                var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
            }
        }else{
            text="Jugador Saliente";
            if(chk_equipos=='local'){
                var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
            }else{
                var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_visitante')); ?>";
            }
        }
        var torneo = $("#JuegoTorneoId").val();
        setNameLabel(text);
        $.ajax({
            url: url,
            type: "post",
            cache: false,
            data: {id:id,torneo:torneo},
            dataType: 'json',
            success:function(resp){
                $("#jugadoreventos2-id").html(resp.jugadores);
                $("#jugadoreventos2-id").val('');
            }
        });
    }

    function setNameLabel(text){
        var padre=document.getElementById('jugadoreventos2-id').parentNode;
        var children=padre.children;
        children[0].innerText=text;

    }

    function loadChildSimple(){
        location.reload(true);
    }

    function changeInputMinuto(){
        var option=this.value.trim();
        if(option!=""){
            var resp=validateInput(option, 'minuto');
            if(!resp[0]){
                this.value='';
                showAlert(resp[1],'modal-minuto');
            }
        }
    }

    function validateInput(valor, tipo){
        var resp=new Array();
        switch (tipo){
            case 'minuto':
                resp=regExpMaster(valor, /^[0-9]{1,}[:]{0,1}[0-9]{0,}$/, 'Se permite solamente valores numéricos enteros positivos');
                break;
        }
        return resp;
    }

    function regExpMaster(valor, pattern, msj) {
        var resp=new Array();
        if(valor.match(pattern)){
            resp[0]=true;
        }else{
            resp[0]=false;
            resp[1]=msj;
        }
        return resp;
    }

    function editarMinutoxminuto(){
        tipo_accion='edit';
        var label="<i class='fa fa-group'></i> Editar Evento";
        document.getElementById('myModalLabel').innerHTML=label;
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_minutoxminuto);
        var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'getjuegominutoxminuto')); ?>";
        juegoevento_id=id[1];
        $.ajax({
            url: url,
            type: "post",
            cache: false,
            data: {id:id[1]},
            dataType: 'json',
            success:function(resp){
                if(resp.chkequipolocal){
                    document.getElementById('rbEventosEqlocal').checked=true;
                }
                else{
                    document.getElementById('rbEventosEqvisitante').checked=true;
                }

                //llenado de jugadores
                var camposelect=resp.select1_equipo;
                document.getElementById('jugadoreventos-id').innerHTML="";
                crearOptionSelect('Seleccionar','','jugadoreventos-id');
                for (var i = 0; i < camposelect.length; i++) {
                    var element=camposelect[i]['Jugadore']['nombre_jugador'];
                    var index=camposelect[i]['Jugadore']['id'];
                    crearOptionSelect(element,index,'jugadoreventos-id');
                };
                if(resp.jugador_id!=null){
                    document.getElementById('jugadoreventos-id').value=resp.jugador_id;
                }

                //nombrando equipo seleccionado
                var className=document.getElementsByClassName('lbl-equipo-seleccionado');
                for (i = 0; i < className.length; i++) {
                    className[i].innerText=resp.nombre_equipo;
                }

                //llenado campo minutos
                document.getElementById('minutoeventos').value=resp.minuto;
                //llenado campo minuto adicional
                document.getElementById('minadicional').value=resp.minadicional;

                //llenando eventos
                camposelect=resp.eventos;
                document.getElementById('eventoeventos-id').innerHTML="";
                crearOptionSelect('Seleccionar','','eventoeventos-id');
                for (i = 0; i < camposelect.length; i++) {
                    element=camposelect[i]['Evento']['evento'];
                    index=camposelect[i]['Evento']['id'];
                    crearOptionSelect(element,index,'eventoeventos-id');
                };
                if(resp.evento_id!=null){
                    document.getElementById('eventoeventos-id').value=resp.evento_id;
                }

                document.getElementById('deteventoeventos-id').innerHTML="";
                crearOptionSelect('Seleccionar','','deteventoeventos-id');
                if(resp.tipoevento=='falta'){
                    //llenado de detalle
                    camposelect=resp.detalle_eventos;
                    document.getElementById('deteventoeventos-id').innerHTML="";
                    crearOptionSelect('Seleccionar','','deteventoeventos-id');
                    for (i = 0; i < camposelect.length; i++) {
                        element=camposelect[i]['Detevento']['detevento'];
                        index=camposelect[i]['Detevento']['id'];
                        crearOptionSelect(element,index,'deteventoeventos-id');
                    };
                    if(resp.detevento_id!=null){
                        document.getElementById('deteventoeventos-id').value=resp.detevento_id;
                    }
                }else{
                     //llenado de detalle
                    camposelect=resp.detalle_eventos;
                    document.getElementById('deteventoeventos-id').innerHTML="";
                    crearOptionSelect('Seleccionar','','deteventoeventos-id');
                    for (i = 0; i < camposelect.length; i++) {
                        element=camposelect[i]['Detevento']['detevento'];
                        index=camposelect[i]['Detevento']['id'];
                        crearOptionSelect(element,index,'deteventoeventos-id');
                    };
                    if(resp.detevento_id!=null){
                        document.getElementById('deteventoeventos-id').value=resp.detevento_id;
                    }
                }

                document.getElementById('jugadoreventos2-id').innerHTML="";
                crearOptionSelect('Seleccionar','','jugadoreventos2-id');
                if(resp.tipoevento=='falta' || resp.tipoevento=='cambio'){
                    //llenado de jugadores
                    camposelect=resp.select2_equipo;
                    crearOptionSelect('Seleccionar','','jugadoreventos2-id');
                    for (i = 0; i < camposelect.length; i++) {
                        element=camposelect[i]['Jugadore']['nombre_jugador'];
                        index=camposelect[i]['Jugadore']['id'];
                        crearOptionSelect(element,index,'jugadoreventos2-id');
                    };
                    if(resp.jugador2_id!=null){
                        document.getElementById('jugadoreventos2-id').value=resp.jugador2_id;
                    }
                }

                var id_select=document.getElementById('jugadoreventos2-id');
                var padre_select=id_select.parentNode.parentNode;
                if(resp.tipoevento=='falta' || resp.tipoevento=='cambio'){
                    setNameLabel(resp.nameLabel);
                    removeClass(padre_select,'display-none');
                }
                else{
                    addClass(padre_select,'display-none');
                }

                //llenando input comentario
                document.getElementById('comentarioeventos').value=resp.comentario;
                $('#addEvento').modal('show');
            }
        });
    }

    function deleteMinutoxminuto(){
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_minutoxminuto);
        juegoevento_id=id[1];
        bootbox.confirm({
            message: "¿Está seguro de eliminar el registro?, perderá los datos que se han ingresado.",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result) {
                    var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'deletejuegoevento')); ?>";
                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:juegoevento_id},
                        dataType: 'json',
                        success:function(resp){
                            bootbox.alert(resp.msj);
                            if(resp.resp){
                                $('#tbody-minutoxminuto').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'minutoxminuto')); ?>",
                                    {id: resp.juego_id, render:true}
                                );
                                $('#JuegoMarcadore1').val(resp.marcadore1);
                                $('#JuegoMarcadore2').val(resp.marcadore2);
                                $('#JuegoMarcadore1pt').val(resp.marcadore1pt);
                                $('#JuegoMarcadore2pt').val(resp.marcadore2pt);
                            }
                        }
                    });
                }
            }
        });
    }

    function btnaddEvento(){
        tipo_accion ='add';
        $("#minadicional").val("");
        juegoevento_id=0;
        var label="<i class='fa fa-group'></i> Registrar Nuevo Evento";
        document.getElementById('myModalLabel').innerHTML=label;
        if($("#rbEventosEqlocal").is(':checked')) {
            $('#rbEventosEqlocal').change();
        }else{
            $('#rbEventosEqvisitante').change();
        }
    }

    document.getElementById('minutoeventos').addEventListener('change',changeInputMinuto,false);
    document.getElementById('minadicional').addEventListener('change',changeInputMinuto,false);
    document.getElementById('btn-eventos').addEventListener('click',btnaddEvento,false);
    var className=document.getElementsByClassName('btn-update-minutos');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',loadChildSimple,false);
    };

    var className=document.getElementsByClassName('editarMinutoxminuto');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarMinutoxminuto,false);
    }

    var className=document.getElementsByClassName('deleteMinutoxminuto');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteMinutoxminuto,false);
    }

    var className=document.getElementsByClassName('span-update-minutos');
    if(className.length>0) {
        setInterval(function(){
            var span_text=parseInt(className[0].innerText);
            if(span_text==0){
                location.reload(true);
            }else{
                span_text--;
                className[0].innerText=span_text;
            }
        },1000);
    }
</script>