<?php $this->layout = 'ajax'; ?>
<?php
$i=0;
foreach ($alineaciones as $key => $local) {
    $class=null;
    if ($i++ % 2 == 0) {
        $class = ' class="altrow"';
    }
    ?>
    <tr <?= $class;?> id="juegoalineacione_<?=$local['Juegoalineacione']['id']?>">
        <td style='text-align:left'><?=(!empty($local['Jugadore']['nombre_jugador']))?h($local['Jugadore']['nombre_jugador']):h($local['Persona']['nombre_persona']); ?>&nbsp;</td>
        <td style='text-align:center'><?=(!empty($local['Posicione']['posicion']))?h($local['Posicione']['posicion']):h($local['Tipopersona']['tipopersonas']); ?>&nbsp;</td>
        <td style='text-align:center'><?= h(($local['Juegoalineacione']['capitan'] == 1) ? 'Si' : 'No' ); ?>&nbsp;</td>
        <td class="">
            <a class="editar editarJuegoalineacione" style="cursor:pointer;"></a>
            <a class="deleteDet deleteJuegoalineacione" style="cursor:pointer;"></a>
        </td>
    </tr>
<?php } ?>
<script>
    className=document.getElementsByClassName('editarJuegoalineacione');
    for (i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarJuegoalineacione,false);
    }

    className=document.getElementsByClassName('deleteJuegoalineacione');
    for (i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteJuegoalineacione,false);
    }
</script>
                          
                