<div class="row">
    <div class="container-fluid">
        <div class="col-xs-12" style="border-bottom: solid 1px #011880;">
            <span>Estadisticas</span>
        </div>
        <br>
        <br>
        <div class="col-md-12 head-juegos">
            <div class="col-md-2" style="margin-top: 15px;">
                <span class="equipo1">Estadística</span>
            </div>
            <div class="col-md-9">
                <span class="equipo1"><?=$data['Equipo1']['nombrecorto']?></span>
                <img class="escudo1" src="../../../<?=$data['Equipo1']["fotoescudo"]?>">
                <span>Vs.</span>
                <img class="escudo2" src="../../../<?=$data['Equipo2']["fotoescudo"]?>">
                <span class="equipo1"><?=$data['Equipo2']['nombrecorto']?></span>
            </div>
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12 interlineado borde2 line-frm">
            <div class="alert alert-danger" id="alertaEstadist" style="display: none;margin-top: -14px">
                <span class="icon icon-check-circled"></span>
                <button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <div class="col-md-4 col-xs-3 col-sm-3">Posesión de balón</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input type="number" min="1" readonly max="100" class="form-control posesionb" data-campo="posesioneq1" id="posesioneq1" value="<?=$data["Juego"]["posesioneq1"]?>">
            </div>
            <div class="col-md-1 col-xs-2 col-sm-2" style="padding-left: 0;"><span><strong>%</strong></span></div>
            <div class="col-md-offset-1 col-md-2 col-xs-offset-0 col-sm-offset-0 col-md-2 col-xs-2 col-sm-2">
                <input type="number" min="1" readonly class="form-control posesionb" data-campo="posesioneq2" id="posesioneq2" value="<?=$data["Juego"]["posesioneq2"]?>">
            </div>
            <div class="col-md-1 col-xs-1 col-sm-1" style="padding-left: 0;"><span><strong>%</strong></span></div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tiros a puerta</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosapuertaeq1" onchange="saveDato(this.id);" id="tirosapuertaeq1" value="<?=$data["Juego"]["tirosapuertaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosapuertaeq2" onchange="saveDato(this.id);" id="tirosapuertaeq2" value="<?=$data["Juego"]["tirosapuertaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde2 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tiros fuera</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosfueraeq1" onchange="saveDato(this.id);" id="tirosfueraeq1" value="<?=$data["Juego"]["tirosfueraeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosfueraeq2" onchange="saveDato(this.id);" id="tirosfueraeq2" value="<?=$data["Juego"]["tirosfueraeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tiros de esquina</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosdeesquinaeq1" onchange="saveDato(this.id);" id="tirosdeesquinaeq1" value="<?=$data["Juego"]["tirosdeesquinaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="tirosdeesquinaeq2" onchange="saveDato(this.id);" id="tirosdeesquinaeq2" value="<?=$data["Juego"]["tirosdeesquinaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde2 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Saque de puerta</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="saquesdepuertaeq1" onchange="saveDato(this.id);" id="saquesdepuertaeq1" value="<?=$data["Juego"]["saquesdepuertaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="saquesdepuertaeq2" id="saquesdepuertaeq2" onchange="saveDato(this.id);" value="<?=$data["Juego"]["saquesdepuertaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Pases realizados</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="paseseq1" onchange="saveDato(this.id);" id="paseseq1" value="<?=$data["Juego"]["paseseq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="paseseq2" onchange="saveDato(this.id);" id="paseseq2" value="<?=$data["Juego"]["paseseq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde2 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tiros al poste</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="disparosalposteeq1" onchange="saveDato(this.id);" id="disparosalposteeq1" value="<?=$data["Juego"]["disparosalposteeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="disparosalposteeq2" onchange="saveDato(this.id);" id="disparosalposteeq2" value="<?=$data["Juego"]["disparosalposteeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Saque de banda</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="saquesdebandaeq1" onchange="saveDato(this.id);" id="saquesdebandaeq1" value="<?=$data["Juego"]["saquesdebandaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="saquesdebandaeq2" onchange="saveDato(this.id);" id="saquesdebandaeq2" value="<?=$data["Juego"]["saquesdebandaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde2 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tarjetas Amarillas</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="tarjetaamarillaeq1" onchange="saveDato(this.id);" id="tarjetaamarillaeq1" value="<?=$data["Juego"]["tarjetaamarillaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="tarjetaamarillaeq2" onchange="saveDato(this.id);" id="tarjetaamarillaeq2" value="<?=$data["Juego"]["tarjetaamarillaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Tarjetas Rojas</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="tarjetarojaeq1" onchange="saveDato(this.id);" id="tarjetarojaeq1" value="<?=$data["Juego"]["tarjetarojaeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="tarjetarojaeq2" onchange="saveDato(this.id);" id="tarjetarojaeq2" value="<?=$data["Juego"]["tarjetarojaeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde2 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Total Faltas</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="faltaseq1" onchange="saveDato(this.id);" id="faltaseq1" value="<?=$data["Juego"]["faltaseq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" data-campo="faltaseq2" onchange="saveDato(this.id);" id="faltaseq2" value="<?=$data["Juego"]["faltaseq2"]?>">
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12 borde1 line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Penales</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="penalesporeq1" onchange="saveDato(this.id);" id="penalesporeq1" value="<?=$data["Juego"]["penalesporeq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="penalesporeq2" onchange="saveDato(this.id);" id="penalesporeq2" value="<?=$data["Juego"]["penalesporeq2"]?>">
            </div>
        </div>
        <div class="col-md-12 borde2 col-xs-12 col-sm-12 interlineado line-frm">
            <div class="col-md-4 col-xs-3 col-sm-3">Penales fallados</div>
            <div class="col-md-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="penalesfalladoseq1" onchange="saveDato(this.id);" id="penalesfalladoseq1" value="<?=$data["Juego"]["penalesfalladoseq1"]?>">
            </div>
            <div class="col-md-offset-2 col-md-2 col-xs-offset-2 col-sm-offset-2 col-xs-2 col-sm-2">
                <input class="form-control" readonly data-campo="penalesfalladoseq2" onchange="saveDato(this.id);" id="penalesfalladoseq2" value="<?=$data["Juego"]["penalesfalladoseq2"]?>">
            </div>
        </div>

    </div>
    <br>
</div>