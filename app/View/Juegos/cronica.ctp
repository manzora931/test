<?php $this->layout = 'ajax';
echo $this->Html->script("ckeditor/ckeditor.js");
?>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12" style="border-bottom: solid 1px #011880;">
            <span>Crónica</span>
            <?php if(empty($view)): ?>
            <a data-toggle="modal" href="#editCronica" class="btn btn-crear pull-right" id="btn-cronica">Crónica</a>
            <?php endif; ?>
        </div>

        <?php if(count($juego['Juego']['cronica']) > 0) { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th style="text-align: left"><?php echo 'Crónica'; ?></th>
                        <?php if(empty($view)): ?>
                        <th><?php echo '' ?></th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                        $class=null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                        ?>
                        <tr <?= $class;?> id="cronica_<?=$juego['Juego']['id']?>">
                            <td style='text-align:left'>
                                <?= $juego['Juego']['cronica']; ?>
                            </td>
                            <?php if(empty($view)): ?>
                            <td class="">
                                <a class="editar editaCronica" style="cursor:pointer;"></a>
                                <a class="deleteDet deleteCronica" style="cursor:pointer;"></a>
                            </td>
                            <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <p class="text-center">No se ha registrado la crónica</p>
            </div>
        <?php } ?>
    </div>
</div>
<!-- Modal Para Registrar Cronica -->
<div class="modal fade" id="editCronica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    <i class="fa fa-comments-o"></i> Registrar Crónica
                </h3>
            </div>
            <div class="modal-body">
                <form id="formulario-cronica" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-10">
                                <?= $this->Form->input('txtcronica',[
                                    'label'=>"Crónica",
                                    'id'=>'txtcronica',
                                    'class'=>"form-control validate[required] form-contacto ckeditor",
                                    'div'=>['class'=>"form-groupp"],
                                    'placeholder'=>"Crónica",
                                    'rows'=>8,
                                    'value'=> $juego['Juego']['cronica'],
                                    'required'=>'required',
                                    'style'=>'margin-bottom: 15px']);
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-danger" id="registrar-cronica">Almacenar</button>
            </div>
        </div>
    </div>
</div>
<script>
var prefijo_cronica='cronica_';
var cronica_id=0;
    $(document).ready(function() {

        //Al momento de hacer click en almacenar la cronica del juego
        $("#registrar-cronica").click(function() {
            var juego_id = $('#juego-id').val();
            var txtcronica = CKEDITOR.instances['txtcronica'].getData();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_cronica_juego')); ?>";

            // Se obtiene la informacion ingresada de la cronica del juego
            var formData = $('#formulario-cronica').serializeArray();

            // Se adiciona el id del juego a la informacion ingresada de la cronica del juego
            formData.push({name: 'data[juego_id]', value: juego_id});
            formData.push({name: 'data[cronica]', value: txtcronica});
            console.log(formData);
            request = $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: formData,
                dataType: 'json'
            });

            request.done(function (response, textStatus, jqXHR){
                if(response.msg != 'error') {
                    location.reload(true)
                }
            });

            //return false;
        });
    });

    function editarCronica(){
        $('#editCronica').modal('show');
    }
    function deleteCronica(){
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_cronica);
        cronica_id=id[1];
        bootbox.confirm({
            message: "¿Está seguro de eliminar el registro?, perderá los datos que se han ingresado.",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result) {
                    var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'deletejuegocronica')); ?>";
                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:cronica_id},
                        dataType: 'json',
                        success:function(resp){
                            bootbox.alert(resp.msj);
                            if(resp.resp){
                                location.reload(true);
                            }                                                  
                        }
                    });                                
                }
            }
        });
    }
    var className=document.getElementsByClassName('editaCronica');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarCronica,false);
    }
    var className=document.getElementsByClassName('deleteCronica');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteCronica,false);
    }
    
</script>