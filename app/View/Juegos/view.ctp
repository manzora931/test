<?php 
//echo $this->Html->css(['proyecto/main']);
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';?>
<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script(['funciones/addremoveclass']) ?>
<style>
    .btn-crear {
        background-color: #4160a3;
        color: #fff;
        border-radius: 5px;
        width: 120px;
        margin-left: 5px;
        margin-top: 20px;
        text-decoration: none;
    }
    .btn-crear:hover {
        color: #fff;
    }
    .btn-impresion{
        background-color: #fff ;
        color: #021f54 !important;
        width: 120px;
        height: 25px;
        float: right;
        white-space: nowrap;
        margin-top: -20px;
        border-radius: 4px;
    }
    a.btn-impresion:hover{
        background-color: #fff;
        color: #021f54 !important;
    }
    .icon-tringle {
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 8px 6px 0 6px;
        border-color: #021f54 transparent transparent transparent;
        margin-left: 77px;
        margin-top: -15px;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
</style>

<script>
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<?php
    $meses = [1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre"];
    $dias = [0=>"Domingo",1=>"Lunes",2=>"Martes",3=>"Miercoles",4=>"Jueves",5=>"Viernes",6=>"Sabado"];

?>
<div class="juegos view">
    <br>
    <div class="container-fluid">
        <div class="panel panel-primary">
            <div class="panel-heading" id="seccionjuego">
                <h4 class="panel-title">
                    <a id="btn-proy" role="button" class="collapsed" data-toggle="collapse" href="" data-target="#frm" aria-expanded="true">
                        <span class="stage-title" id="juego" data-proyecto="<?=$id?>">Juego: <?=$juego['Equipo1']['nombrecorto']?> vrs <?=$juego['Equipo2']['nombrecorto']?> </span>
                    </a>
                    <a class="btn-impresion btn" style="margin-left: 20px;">
                        Imprimir  <div class="icon-tringle"></div>
                    </a>
                </h4>
            </div>
            <div class="row"><br>
                <div class="col-md-5 seccion1">
                    <div class="col-md-12 head-juegos">
                        <img class="escudo1" src="../../<?=$juego['Equipo1']["fotoescudo"]?>">
                        <span class="equipo1"><?=$juego['Equipo1']['nombrecorto']?></span>
                        <span class="marcador"><?=($juego['Juego']['marcadore1']!=null)?$juego['Juego']['marcadore1']." - ":'0 - ';?>
                        <?=($juego['Juego']['marcadore2']!='')?$juego['Juego']['marcadore2']:"0 "?>
                        </span>
                        <span class="equipo1"><?=$juego['Equipo2']['nombrecorto']?></span>
                        <img class="escudo2" src="../../<?=$juego['Equipo2']["fotoescudo"]?>">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 borde1 ">
                        <span>ID del juego: <?=$juego['Juego']['id']?></span>
                    </div>
                    <div class="col-md-12 interlineado borde2">
                        <?php
                        $d= date("w",strtotime($juego['Juego']['fecha']));

                        $dia = date("d",strtotime($juego['Juego']['fecha']));
                        $mes = date("n",strtotime($juego['Juego']['fecha']));
                        $year = date("Y",strtotime($juego['Juego']['fecha']));
                        ?>
                        <span>Fecha del encuentro: <?=$dias[$d]." ".$dia." de ".$meses[$mes]." ".$year?></span>
                    </div>
                    <div class="col-md-12 borde1">
                        <span>Hora: <?=date("H:i",strtotime($juego["Juego"]["hora"]));?></span>
                    </div>
                    <div class="col-md-12 interlineado borde2">
                        <span>Acumula Puntos: <?=($juego["Juego"]["sumapuntos"]==1)?"Si":"No"?></span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="col-md-12 head-juegos">
                        <span style="font-size: 18px;font-weight: bold;">Datos generales</span>
                    </div>
                    <div class="col-md-12 borde1 ">
                        <span>Torneo: <strong><?=$juego['Torneo']['nombrecorto']?></strong></span>
                    </div>
                    <div class="col-md-12 interlineado borde2">
                        <span>Etapa: <strong><?=$juego['Etapa']['etapa']?></strong></span>
                    </div>
                    <div class="col-md-12 borde1">
                        <span>Fase de Grupos: <?=(!empty($juego['Grupo']['grupo']))?$juego['Grupo']['grupo']:'--'?></span>
                    </div>
                    <div class="col-md-12 borde2 interlineado">
                        <span>Estadio: <?=$this->Html->link($juego['Estadio']['estadio'],["controller"=>"estadios","action"=>"view",$juego['Estadio']['id']])?></span>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
                <div class="col-md-5 seccion1">
                    <div class="col-md-12 head-juegos">
                        <span style="font-size: 18px;font-weight: bold;">Más Información</span>
                    </div>
                    <div class="col-md-12 borde1 ">
                        <span>Temperatura: <?=(!empty($juego['Juego']['temperatura']))?$juego['Juego']['temperatura']:'0'?> °C</span>
                    </div>
                    <div class="col-md-12 interlineado borde2">
                        <span>Asistencia: <?=(!empty($juego['Juego']['asistencia']))?$juego['Juego']['asistencia']:'0'?> aficionados</span>
                    </div>
                    <div class="col-md-12 borde1">
                        <span>Taquilla: <?=(!empty($juego['Juego']['taquilla']))?$juego['Juego']['taquilla']:'0'?></span>
                    </div>
                    <div class="col-md-12 borde2 interlineado">
                        <span>Porcentaje utilizado en estadio: <?=(!empty($juego["Juego"]["porcestadio"]))?$juego["Juego"]["porcestadio"]."%":"0%"?></span>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="col-md-12 head-juegos">
                        <span style="font-size: 18px;font-weight: bold;">Árbitros</span>
                    </div>
                    <div class="col-md-12 borde1 ">
                        <span>Principal: <?=(!empty($juego['Juego']['referi']))?$this->Html->link($arbitros[$juego['Juego']['referi']],["controller"=>"personas","action"=>"view",$juego['Juego']['referi']]):'--'?></span>
                    </div>
                    <div class="col-md-12 interlineado borde2">
                        <span>Auxiliar 1: <?=(!empty($juego['Juego']['referiaux1']))?$arbitros[$juego['Juego']['referiaux1']]:'--'?></span>
                    </div>
                    <div class="col-md-12 borde1">
                        <span>Auxiliar 2: <?=(!empty($juego['Juego']['referiaux2']))?$arbitros[$juego['Juego']['referiaux2']]:'--'?></span>
                    </div>
                    <div class="col-md-12 borde2 interlineado">
                        <span>4o. árbitro: <?=(!empty($juego['Juego']['referi2']))?$arbitros[$juego['Juego']['referi2']]:'--'?></span>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 marg-panel">
                    <ul class="nav nav-pills">
                        <li id="alineacion" class="option_childs"><a>Alineación</a></li>
                        <?php
                            if($perfil!=4 && $perfil!=5){   ?>
                                <li id="estadistica" class="option_childs"><a>Estadisticas</a></li>
                       <?php    }                        ?>

                        <li id="minuto" class="option_childs"><a>Minuto a minuto</a></li>
                        <?php
                        if($perfil!=5){   ?>
                        <li id="notas" class="option_childs"><a>Notas</a></li>
                        <?php } ?>
                        <li id="cronica" class="option_childs"><a>Crónica/Video</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="container-fluid child" id="childs">
                </div>
            </div>

        </div>
    </div>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Juego'), array('action' => 'edit', $id), array('class' => 'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Juegos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    var id_juego=<?=$id?>;
    var equipo1=<?=$juego['Juego']['equipo1_id']?>;
    var equipo2=<?=$juego['Juego']['equipo2_id']?>;
    function loadChild(){
        var id_child=this.id;
        var className=document.getElementsByClassName('option_childs');
        for (var i = 0; i < className.length; i++) {
            removeClass(className[i],'activa');
        };
        addClass(this,'activa');
        if(id_child=='alineacion'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'alineaciones')); ?>",
            {id: id_juego, equipo1: equipo1, equipo2: equipo2, view:true}
            );
        }
        else if(id_child=='notas'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'notas')); ?>",
            {id: id_juego, view:true}
            );
        }
        else if(id_child=='cronica'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'cronica')); ?>",
            {id: id_juego, view:true}
            );
        }
        else if(id_child=='estadistica'){
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'estadisticaview',)); ?>",
                {id: id_juego}
            );
        }else{
            $('#childs').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'minutoxminuto',)); ?>",
            {id: id_juego, view:true}
            );
        }

    }

    var className=document.getElementsByClassName('option_childs');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('click',loadChild,false);
    };

    function updatePage() {
        location.reload();
    }
    jQuery(function(){
        var url = getURL();
        $("#alertaP").slideDown();
        setTimeout(function () {
            $("#alertaP").slideUp();
        }, 4000);
        $("#minuto").click();
        setTimeout(updatePage,60000);
        $(".btn-impresion").click(function(){
            window.open('<?php echo Router::url('/'); ?>juegos/impresion/<?=$juego['Juego']['id']?>', 'imp','height=500,width=1000,menubar=1,resizable=1,scrollbars=1');
        });
    });
</script>
