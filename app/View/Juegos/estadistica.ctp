<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="col-md-12 col-lg-12">
            <div class="col-md-11 head-juegos">
                <div class="col-md-2" style="margin-top: 15px;">
                    <span class="equipo1">Estadística</span>
                </div>
                <div class="col-md-9">
                    <span class="equipo1"><?=$data['Equipo1']['nombrecorto']?></span>
                    <img class="escudo1" src="../../<?=$data['Equipo1']["fotoescudo"]?>">
                    <span>Vs.</span>
                    <img class="escudo2" src="../../<?=$data['Equipo2']["fotoescudo"]?>">
                    <span class="equipo1"><?=$data['Equipo2']['nombrecorto']?></span>
                </div>
            </div>

            <div class="col-md-11 col-xs-12 col-sm-12 interlineado borde2 line-frm">
                <div class="alert alert-danger" id="alertaEstadist" style="display: none;margin-top: -14px">
                    <span class="icon icon-check-circled"></span>
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>
                <div class="alert alert-success" id="alertaEstadistSucc" style="display: none;margin-top: -14px">
                    <span class="message" ></span>
                    <button type="button" class="close" data-dismiss="alert"></button>
                </div>

                <div class="col-md-3 col-xs-3 col-sm-3">Posesión de balón</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input type="number" min="1" max="100" class="form-control posesionb" data-campo="posesioneq1" id="posesioneq1" value="<?=$data["Juego"]["posesioneq1"]?>">
                </div>
                <div class="col-md-1 col-xs-2 col-sm-2" style="padding-left: 0;"><span><strong>%</strong></span></div>
                <div class="col-md-offset-1 col-xs-offset-0 col-sm-offset-0 col-md-3 col-xs-2 col-sm-2">
                    <input type="number" min="1"  class="form-control posesionb" data-campo="posesioneq2" id="posesioneq2" value="<?=$data["Juego"]["posesioneq2"]?>">
                </div>
                <div class="col-md-1 col-xs-1 col-sm-1" style="padding-left: 0;"><span><strong>%</strong></span></div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tiros a puerta</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tirosapuertaeq1" onchange="saveDato(this.id);" id="tirosapuertaeq1" value="<?=$data["Juego"]["tirosapuertaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tirosapuertaeq2" onchange="saveDato(this.id);" id="tirosapuertaeq2" value="<?=$data["Juego"]["tirosapuertaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tiros fuera</div>
                <div class="col-md-3 col-sx-2 col-sm-2">
                    <input class="form-control" data-campo="tirosfueraeq1" onchange="saveDato(this.id);" id="tirosfueraeq1" value="<?=$data["Juego"]["tirosfueraeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tirosfueraeq2" onchange="saveDato(this.id);" id="tirosfueraeq2" value="<?=$data["Juego"]["tirosfueraeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tiros de esquina</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tirosdeesquinaeq1" onchange="saveDato(this.id);" id="tirosdeesquinaeq1" value="<?=$data["Juego"]["tirosdeesquinaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tirosdeesquinaeq2" onchange="saveDato(this.id);" id="tirosdeesquinaeq2" value="<?=$data["Juego"]["tirosdeesquinaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Saque de puerta</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="saquesdepuertaeq1" onchange="saveDato(this.id);" id="saquesdepuertaeq1" value="<?=$data["Juego"]["saquesdepuertaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="saquesdepuertaeq2" id="saquesdepuertaeq2" onchange="saveDato(this.id);" value="<?=$data["Juego"]["saquesdepuertaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Pases realizados</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="paseseq1" onchange="saveDato(this.id);" id="paseseq1" value="<?=$data["Juego"]["paseseq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2">
                    <input class="form-control" data-campo="paseseq2" onchange="saveDato(this.id);" id="paseseq2" value="<?=$data["Juego"]["paseseq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tiros al poste</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="disparosalposteeq1" onchange="saveDato(this.id);" id="disparosalposteeq1" value="<?=$data["Juego"]["disparosalposteeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="disparosalposteeq2" onchange="saveDato(this.id);" id="disparosalposteeq2" value="<?=$data["Juego"]["disparosalposteeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-sx-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Saque de banda</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="saquesdebandaeq1" onchange="saveDato(this.id);" id="saquesdebandaeq1" value="<?=$data["Juego"]["saquesdebandaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="saquesdebandaeq2" onchange="saveDato(this.id);" id="saquesdebandaeq2" value="<?=$data["Juego"]["saquesdebandaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tarjetas Amarillas</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tarjetaamarillaeq1" onchange="saveDato(this.id);" id="tarjetaamarillaeq1" value="<?=$data["Juego"]["tarjetaamarillaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tarjetaamarillaeq2" onchange="saveDato(this.id);" id="tarjetaamarillaeq2" value="<?=$data["Juego"]["tarjetaamarillaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Tarjetas Rojas</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tarjetarojaeq1" onchange="saveDato(this.id);" id="tarjetarojaeq1" value="<?=$data["Juego"]["tarjetarojaeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="tarjetarojaeq2" onchange="saveDato(this.id);" id="tarjetarojaeq2" value="<?=$data["Juego"]["tarjetarojaeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Total Faltas</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="faltaseq1" onchange="saveDato(this.id);" id="faltaseq1" value="<?=$data["Juego"]["faltaseq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="faltaseq2" onchange="saveDato(this.id);" id="faltaseq2" value="<?=$data["Juego"]["faltaseq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde1 line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Penales</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="penalesporeq1" onchange="saveDato(this.id);" id="penalesporeq1" value="<?=$data["Juego"]["penalesporeq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="penalesporeq2" onchange="saveDato(this.id);" id="penalesporeq2" value="<?=$data["Juego"]["penalesporeq2"]?>">
                </div>
            </div>
            <div class="col-md-11 col-xs-12 col-sm-12 borde2 interlineado line-frm">
                <div class="col-md-3 col-xs-3 col-sm-3">Penales fallados</div>
                <div class="col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="penalesfalladoseq1" onchange="saveDato(this.id);" id="penalesfalladoseq1" value="<?=$data["Juego"]["penalesfalladoseq1"]?>">
                </div>
                <div class="col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-md-3 col-xs-2 col-sm-2">
                    <input class="form-control" data-campo="penalesfalladoseq2" onchange="saveDato(this.id);" id="penalesfalladoseq2" value="<?=$data["Juego"]["penalesfalladoseq2"]?>">
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    jQuery(function(){
        $(".posesionb").change(function(){
            var valor = parseInt($(this).val());
            var sum=0.00;
            if(valor > 0){
                $(".posesionb").each(function(e,i){
                    if($("#"+i.id).val() > 0){
                        sum = sum + parseFloat($("#"+i.id).val());
                    }
                });
                if(sum > 100){
                    $(this).val("");
                    $("#alertaEstadist span").text("La posesión del balón no puede exceder el 100%");
                    $("#alertaEstadist").slideDown();
                    setTimeout(function(){
                        $("#alertaEstadist").slideUp();
                    },4000);
                }else{
                    //console.log($(this).attr("id"));
                    saveDato($(this).attr("id"));//envia el id del input
                }
            }else{
                //no puede ingresar valores negativos
                $(this).val("");
                $("#alertaEstadist span").text("No puede ingresar valores negativos");
                $("#alertaEstadist").slideDown();
                setTimeout(function(){
                    $("#alertaEstadist").slideUp();
                },4000);
            }

        });
    });
    //la function recibe el id del input
    function saveDato(id){
        var idJuego = $('#juego-id').val();
        var campo = $("#"+id).data("campo");
        var valor = $("#"+id).val();
        var url = getURL()+"updateStadistic";
        if(parseInt(valor)<0){
            $("#"+id).val("");
            $("#alertaEstadist span").text("No puede ingresar valores negativos");
            $("#alertaEstadist").slideDown();
            setTimeout(function(){
                $("#alertaEstadist").slideUp();
            },4000);
        }else{
            $.ajax({
                url: url,
                type: 'post',
                data: {idJuego:idJuego,campo:campo,valor:valor},
                cache: false,
                async: false,
                success: function(resp){
                    if(parseInt(resp)==1){
                        $("#alertaEstadistSucc .message").text("Datos almacenados");
                        $("#alertaEstadistSucc").slideDown();
                        setTimeout(function () {
                            $("#alertaEstadistSucc").slideUp();
                        }, 4000);
                    }
                }
            });
        }
    }
</script>