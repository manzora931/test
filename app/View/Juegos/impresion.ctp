<?php echo $this->Html->css(array('bootstrap/bootstrap', 'cake.generic', 'main', 'font-awesome.min.css', 'impresion_denuncias')); ?>

<?php
$page = 1;

$meses = [1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre"];
$dias = [0=>"Domingo",1=>"Lunes",2=>"Martes",3=>"Miercoles",4=>"Jueves",5=>"Viernes",6=>"Sabado"];
?>
<style>
    @media print{
        body{
            font-size: 12px!important;
        }
        div {
            border: none !important;
        }
        table thead tr th{
            font-weight: bold;
            font-size: 12px !important;
        }
        table tbody tr td{
            font-size: 12px !important;
        }
        h4{
            color: #011880!important;
        }
        .tb-print2{
            display: inline-block;
            width: 80%;
        }
        .col-md-5-print{
            display: inline-block;
            width: 45% !important;
            vertical-align: top !important;
        }
        .img-print{
            width:30px;
            display: inline-block;
        }
    }
    .img-print{
        width:30px;
        display: inline-block;
    }
    .background1{
        background: #f4f4f4;
    }
    .tb-print2{
        border: solid 1px #000;
        border-collapse: collapse;
        display: inline-block;
    }
    .tb-print2 tr th{
        background: #444444;
        color: #fff;
        padding: 3px 3px 4px 3px;
    }
    .tb-print2 tbody tr td{
        border-collapse: collapse;
        border: solid 1px #000 !important;
        padding-left: 10px;
    }

    .tb-print{
        border: solid 1px #000;
        border-collapse: collapse;
        width: 100%;
    }
    .tb-print tr th{
        background: #444444;
        color: #fff;
        padding: 3px 3px 4px 3px;
    }
    .tb-print tbody tr td{
        border-collapse: collapse;
        border: solid 1px #000 !important;
        padding-left: 10px;
    }
</style>
<div class="container" style="margin-bottom: 35px;">
    <div class="row">
        <div class="col-print-12">
            <div><?= $this->Html->image('LOGOS-UNIDOS.png', array('class' => 'imglogo')) ?></div>
            <br>
            <label class="tittle"><?php echo __('Software Sports Media Analytics'); ?></label>
            <div class="linea"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-print-12">
            <div class="actions do-not-print">
                <ul>
                    <li><a class="imprimir btn" href="#" onClick="window.print();" id="ocultar">Imprimir <div class="icono-tringle"></div></a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php
    $cls="";
    ?>
    <div class="clearfix"></div>
    <div class="row info-proyecto">
        <div class="col-md-12">
            <div class="col-md-12" style="text-align: center;">
                <img class="escudo1" src="../../<?=$juego['Equipo1']["fotoescudo"]?>">
                <span class="equipo1"><?=$juego['Equipo1']['nombrecorto']?></span>
                <span class="marcador"><?=($juego['Juego']['marcadore1']!=null)?$juego['Juego']['marcadore1']." - ":'0 - ';?>
                    <?=($juego['Juego']['marcadore2']!='')?$juego['Juego']['marcadore2']:"0 "?>
                        </span>
                <span class="equipo1"><?=$juego['Equipo2']['nombrecorto']?></span>
                <img class="escudo2" src="../../<?=$juego['Equipo2']["fotoescudo"]?>">
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <span>ID del juego: <?=$juego['Juego']['id']?></span>
            </div>
            <div class="col-md-12">
                <?php
                $d= date("w",strtotime($juego['Juego']['fecha']));

                $dia = date("d",strtotime($juego['Juego']['fecha']));
                $mes = date("n",strtotime($juego['Juego']['fecha']));
                $year = date("Y",strtotime($juego['Juego']['fecha']));
                ?>
                <span>Fecha del encuentro: <?=$dias[$d]." ".$dia." de ".$meses[$mes]." ".$year?></span>
            </div>
            <div class="col-md-12">
                <span>Hora: <?=date("H:i",strtotime($juego["Juego"]["hora"]));?></span>
            </div>
            <div class="col-md-12">
                <span>Suma Puntos: <?=($juego["Juego"]["sumapuntos"]==1)?"Si":"No"?></span>
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <div class="col-md-12">
                <span style="font-weight: bold;">Datos generales</span>
            </div>
            <div class="col-md-12">
                <span>Torneo: <strong><?=$juego['Torneo']['nombrecorto']?></strong></span>
            </div>
            <div class="col-md-12">
                <span>Etapa: <strong><?=$juego['Etapa']['etapa']?></strong></span>
            </div>
            <div class="col-md-12">
                <span>Fase de Grupos: <?=(!empty($juego['Grupo']['grupo']))?$juego['Grupo']['grupo']:'--'?></span>
            </div>
            <div class="col-md-12">
                <span>Estadio: <?=$juego['Estadio']['estadio'];?></span>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-12">
            <div class="col-md-12">
                <span style="font-weight: bold;">Más Información</span>
            </div>
            <div class="col-md-12">
                <span>Temperatura: <?=(!empty($juego['Juego']['temperatura']))?$juego['Juego']['temperatura']:'0'?> °C</span>
            </div>
            <div class="col-md-12">
                <span>Asistencia: <?=(!empty($juego['Juego']['asistencia']))?$juego['Juego']['asistencia']:'0'?> aficionados</span>
            </div>
            <div class="col-md-12">
                <span>Taquilla: <?=(!empty($juego['Juego']['taquilla']))?$juego['Juego']['taquilla']:'0'?></span>
            </div>
            <div class="col-md-12">
                <span>Porcentaje utilizado en estadio: <?=(!empty($juego["Juego"]["porcestadio"]))?$juego["Juego"]["porcestadio"]."%":"0%"?></span>
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <div class="col-md-12">
                <span style="font-weight: bold;">Árbitros</span>
            </div>
            <div class="col-md-12">
                <span>Principal: <?=(!empty($juego['Juego']['referi']))?$arbitros[$juego['Juego']['referi']]:'--'?></span>
            </div>
            <div class="col-md-12">
                <span>Auxiliar 1: <?=(!empty($juego['Juego']['referiaux1']))?$arbitros[$juego['Juego']['referiaux1']]:'--'?></span>
            </div>
            <div class="col-md-12">
                <span>Auxiliar 2: <?=(!empty($juego['Juego']['referiaux2']))?$arbitros[$juego['Juego']['referiaux2']]:'--'?></span>
            </div>
            <div class="col-md-12">
                <span>4o. árbitro: <?=(!empty($juego['Juego']['referi2']))?$arbitros[$juego['Juego']['referi2']]:'--'?></span>
            </div>
        </div>
        <div class="col-md-12">
            <br>
            <div class="col-md-12">
                <span style="font-weight: bold;">Alineaciones</span>
            </div><br>
            <div class="col-md-12">
                <div class="col-md-5 col-md-5-print">
                    <h4><?=$equipo1;?></h4>
                    <table class="tb-print2">
                        <thead>
                        <tr>
                            <th>Jugador</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i=0;
                        $jugador="";
                        $cap="";
                        foreach ($alineaciones_local as $item){
                            $cls = ($i%2==0)?"class='background1'":"";
                            ?>
                            <tr>
                                <td <?= $cls;?>>
                                    <?php
                                    if($item["Persona"]["nombre"]!=''){
                                        //Director tecnico
                                        $jugador = $item["Persona"]["nombre"]." ".$item["Persona"]["apellido"]." (DT)";
                                    }else{
                                        //Jugador
                                        $cap=($item["Juegoalineacione"]["capitan"])?"(C)":"";
                                        if(isset($dorsalJugadores[$item["Jugadore"]["id"]])){
                                            $jugador= $dorsalJugadores[$item["Jugadore"]["id"]]["dorsal"]."- ".$item["Jugadore"]["nombre"]." ".$item["Jugadore"]["apellido"]." - ".$item["Posicione"]["posicion"]." ".$cap;
                                        }else{
                                            $jugador=$item["Jugadore"]["nombre"]." ".$item["Jugadore"]["apellido"]." - ".$item["Posicione"]["posicion"]." ".$cap;
                                        }
                                    }
                                    echo $jugador;
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }           ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-5 col-md-5-print">
                    <h4><?=$equipo2;?></h4>
                    <table class="tb-print2">
                        <thead>
                        <tr>
                            <th>Jugador</th>
                        </tr>
                        </thead>
                        <?php
                        $i=0;
                        foreach ($alineaciones_visita as $item){
                            $cls = ($i%2==0)?"class='background1'":"";
                            ?>
                            <tr>
                                <td <?= $cls;?>>
                                    <?php
                                    if($item["Persona"]["nombre"]!=''){
                                        //Director tecnico
                                        $jugador = $item["Persona"]["nombre"]." ".$item["Persona"]["apellido"]." (DT)";
                                    }else{
                                        //Jugador
                                        $cap=($item["Juegoalineacione"]["capitan"])?"(C)":"";
                                        if(isset($dorsalJugadores[$item["Jugadore"]["id"]])){
                                            $jugador= $dorsalJugadores[$item["Jugadore"]["id"]]["dorsal"]."- ".$item["Jugadore"]["nombre"]." ".$item["Jugadore"]["apellido"]." - ".$item["Posicione"]["posicion"]." ".$cap;
                                        }else{
                                            $jugador=$item["Jugadore"]["nombre"]." ".$item["Jugadore"]["apellido"]." - ".$item["Posicione"]["posicion"]." ".$cap;
                                        }
                                    }
                                    echo $jugador;
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }           ?>
                    </table>
                </div>
                <br>
            </div>
            <?php  if($i>=12){ ?>
            <div style="page-break-after: always;"></div>
            <?php   } ?>
        </div>
        <div class="col-md-12">
            <br>
            <div class="col-md-12">
                <span style="font-weight: bold;">Estadísticas</span>
            </div><br>
            <div class="col-md-12">
                <table class="tb-print">
                    <thead>
                    <tr>
                        <th></th>
                        <th>
                            <span class="equipo1" style="color: #fff !important;"><?=$juego['Equipo1']['nombrecorto']?></span>
                            <img class="escudo1" src="../../<?=$juego['Equipo1']["fotoescudo"]?>">
                        </th>
                        <th>
                            <img class="escudo2" src="../../<?=$juego['Equipo2']["fotoescudo"]?>">
                            <span style="color: #fff!important;" class="equipo1"><?=$juego['Equipo2']['nombrecorto']?></span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $cont=0;
                    if($juego["Juego"]["posesioneq1"]!='' || $juego["Juego"]["posesioneq2"]!=''){  ?>
                    <tr>
                        <td class='background1'>Posesión de balón</td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><span ><?=$juego["Juego"]["posesioneq1"]?> %</span></td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><span><?=$juego["Juego"]["posesioneq2"]?> %</span></td>
                    </tr>
                    <?php }else { $cont++; }?>
                    <?php if($juego["Juego"]["tirosapuertaeq1"]!='' || $juego["Juego"]["tirosapuertaeq2"]!=''){  ?>
                    <tr>
                        <td>Tiros a puerta</td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tirosapuertaeq1"]?></td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tirosapuertaeq2"]?></td>
                    </tr>
                    <?php } else{ $cont++; }?>
                    <?php if($juego["Juego"]["tirosfueraeq1"]!='' || $juego["Juego"]["tirosfueraeq2"]!=''){  ?>
                        <tr>
                            <td class='background1'>Tiros fuera</td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><span ><?=$juego["Juego"]["tirosfueraeq1"]?></span></td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><span><?=$juego["Juego"]["tirosfueraeq2"]?></span></td>
                        </tr>

                    <?php } else{ $cont++; }?>

                    <?php if($juego["Juego"]["tirosdeesquinaeq1"]!='' || $juego["Juego"]["tirosdeesquinaeq2"]!=''){  ?>
                    <tr>
                        <td>Tiros de esquina</td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tirosdeesquinaeq1"]?></td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tirosdeesquinaeq2"]?></td>
                    </tr>
                    <?php }else { $cont++; } ?>
                    <?php if($juego["Juego"]["saquesdepuertaeq1"]!='' || $juego["Juego"]["saquesdepuertaeq2"]!=''){  ?>
                    <tr>
                        <td class='background1'>Saque de puerta</td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["saquesdepuertaeq1"]?></td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["saquesdepuertaeq2"]?></td>
                    </tr>
                    <?php }else { $cont++; } ?>
                    <?php if($juego["Juego"]["paseseq1"]!='' || $juego["Juego"]["paseseq2"]!=''){  ?>
                    <tr>
                        <td>Pases realizados</td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["paseseq1"]?></td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["paseseq2"]?></td>
                    </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["disparosalposteeq1"]!='' || $juego["Juego"]["disparosalposteeq2"]!=''){  ?>
                    <tr>
                        <td class='background1'>Tiros al poste</td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["disparosalposteeq1"]?></td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["disparosalposteeq2"]?></td>
                    </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["saquesdebandaeq1"]!='' || $juego["Juego"]["saquesdebandaeq2"]!=''){  ?>
                    <tr>
                        <td>Saque de banda</td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["saquesdebandaeq1"]?></td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["saquesdebandaeq2"]?></td>
                    </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["tarjetaamarillaeq1"]!='' || $juego["Juego"]["tarjetaamarillaeq2"]!=''){  ?>
                        <tr>
                            <td class='background1'>Tarjetas Amarillas</td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tarjetaamarillaeq1"]?></td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tarjetaamarillaeq2"]?></td>
                        </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["tarjetarojaeq1"]!='' || $juego["Juego"]["tarjetarojaeq2"]!=''){  ?>
                        <tr>
                            <td>Tarjetas Rojas</td>
                            <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tarjetarojaeq1"]?></td>
                            <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["tarjetarojaeq2"]?></td>
                        </tr>
                    <?php }else{ $cont++; } ?>

                    <?php if($juego["Juego"]["faltaseq1"]!='' || $juego["Juego"]["faltaseq2"]!=''){  ?>
                        <tr>
                            <td class='background1'>Total Faltas</td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["faltaseq1"]?></td>
                            <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["faltaseq2"]?></td>
                        </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["penalesporeq1"]!='' || $juego["Juego"]["penalesporeq2"]!=''){  ?>
                    <tr>
                        <td>Penales</td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["penalesporeq1"]?></td>
                        <td style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["penalesporeq2"]?></td>
                    </tr>
                    <?php }else{ $cont++; } ?>
                    <?php if($juego["Juego"]["penalesfalladoseq1"]!='' || $juego["Juego"]["penalesfalladoseq2"]!=''){  ?>
                    <tr>
                        <td class='background1'>Penales fallados</td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["penalesfalladoseq1"]?></td>
                        <td class='background1' style="font-weight: bold;text-align: center;"><?=$juego["Juego"]["penalesfalladoseq2"]?></td>
                    </tr>
                    <?php }else{ $cont++; } ?>
                    <?php  if($cont==9){    ?>

                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <br>
        <div class="col-md-12">
            <span style="font-weight: bold;">Minuto a Minuto</span>
        </div><br>
        <table class="tb-print">
            <thead>
            <tr>
                <th>Minuto</th>
                <th>Jugador</th>
                <th>Equipo</th>
                <th style="width: 120px;">Evento</th>
                <th>Detalle</th>
                <th>Jugador Adversario/ Saliente</th>
                <th>Comentario</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i=0;
            foreach ($eventos as $evento){
                $cls = ($i%2==0)?"class='background1'":"";
                ?>
                <tr>
                    <td <?= $cls;?>><?php
                        if($evento['Juegoevento']['minadicional']!=''){
                            echo $evento['Juegoevento']['minuto']."<span style='margin-left:2px;color:#6048D1;'>+".$evento['Juegoevento']['minadicional']."</span>";
                        }else{
                            echo $evento['Juegoevento']['minuto'];
                        }
                        ?>
                    </td>
                    <td <?= $cls;?> style='text-align:left'>
                        <span style="font-size: 13px;"><?=$dorsalJugadores[$evento['Juegoevento']['jugador_id']]["dorsal"]."-"; ?>&nbsp;&nbsp;</span><?= ($dorsalJugadores[$evento['Juegoevento']['jugador_id']]["nombre"]); ?>&nbsp;
                    </td>
                    <td <?= $cls;?>>
                        <?=$dorsalJugadores[$evento['Juegoevento']['jugador_id']]["equipo"];?>
                    </td>
                    <td <?= $cls;?>><?php

                        if($evento["Evento"]["imagen"]!=''){
                            echo $evento['Evento']['evento']."<img class='img-print' src='../../".$evento["Evento"]["imagen"]."'>";
                        }else{
                            echo $evento['Evento']['evento'];
                        }
                        ?></td>
                    <td <?= $cls;?>><?= $evento['Detevento']['detevento']; ?></td>
                    <td <?= $cls;?>><?=(!empty($evento['Juegoevento']['jugador2_id']))?h($dorsalJugadores[$evento['Juegoevento']['jugador2_id']]["nombre"]):'-'; ?></td>
                    <td <?= $cls;?>><?= $evento['Juegoevento']['comentario']; ?></td>
                </tr>
<?php       $i++;
            }     ?>
            </tbody>
        </table>

    </div>
    <div class="col-md-12">
        <br>
        <div class="col-md-12">
            <span style="font-weight: bold;">Notas</span>
        </div><br>
        <table class="tb-print">
            <thead>
            <tr>
                <th>Nota</th>
                <th>Jugador</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i=0;
            foreach ($notas as $nota){
                $cls = ($i%2==0)?"class='background1'":""; ?>
                <tr>
                    <td <?= $cls;?>><?=$nota['Juegnota']['nota'];?></td>
                    <td <?= $cls;?>><?=($nota['Juegnota']['jugadore_id']!='')?$dorsalJugadores[$nota['Juegnota']['jugadore_id']]["dorsal"]." ".$dorsalJugadores[$nota['Juegnota']['jugadore_id']]["nombre"]:"";?></td>
                </tr>
            <?php
            $i++;
            } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <br>
        <div class="col-md-12">
            <span style="font-weight: bold;">Crónica</span>
        </div><br>
        <table>
            <thead>
            <tr>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="text-align: justify;"><?=$juego["Juego"]["cronica"];?></td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
