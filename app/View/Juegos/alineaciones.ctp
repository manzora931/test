<?php $this->layout = 'ajax'; ?>
<?= $this->Html->script(['funciones/showalert','funciones/addremoveclass','funciones/crearOptionSelect']) ?>
<style>
.display-none{
    display: none;
}
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12" style="border-bottom: solid 1px #011880;">
            <span>Alineaciones</span>
            <?php if(empty($view)): ?>
            <a data-toggle="modal" href="#alineacionesSelect" class="btn btn-crear pull-right" id="btn-alineaciones">Alineaciones</a>
            <?php endif; ?>
        </div>

        <?php if(count($alineaciones_local) > -1 && count($alineaciones_visitante) > -1) { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
                    <li class="active"><a href="#tab-equipo1" class="tablinks" role="tab" data-toggle="tab"><?= $equipo1['Equipo']['equipo'] ?> </a></li>
                    <li><a href="#tab-equipo2" class="tablinks" role="tab" data-toggle="tab"><?= $equipo2['Equipo']['equipo'] ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-equipo1" style="margin-top: 5px;">
                        <div id="bloque-alineaciones-eq1">
                            <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th><?php echo 'Jugador'; ?></th>
                                    <th><?php echo 'Posición' ?></th>
                                    <th><?php echo 'Capitán' ?></th>
                                    <?php if(empty($view)): ?>
                                    <th><?php echo '' ?></th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                                <tbody id="tbody-alineacion-local">
                                <?php
                                $i=0;
                                foreach ($alineaciones_local as $key => $local) {
                                    $class=null;
                                    if ($i++ % 2 == 0) {
                                        $class = ' class="altrow"';
                                    }
                                    ?>
                                    <tr <?= $class;?> id="juegoalineacione_<?=$local['Juegoalineacione']['id']?>">
                                        <td style='text-align:left'><?php
                                            if(!empty($local['Jugadore']['nombre_jugador'])){
                                                echo $this->Html->link($dorsalJugadores[$local["Jugadore"]["id"]]." - ".$local['Jugadore']['nombre_jugador'],["controller"=>"jugadores","action"=>"view",$local["Jugadore"]["id"]],["target"=>"_blank"]);
                                            }else{
                                                echo $this->Html->link($local['Persona']['nombre_persona'],["controller"=>"personas","action"=>"view",$local['Persona']["id"]],["target"=>"_blank"]);
                                            } ?>&nbsp;</td>
                                        <td style='text-align:center'><?=(!empty($local['Posicione']['posicion']))?h($local['Posicione']['posicion']):h($local['Tipopersona']['tipopersonas']); ?>&nbsp;</td>
                                        <td style='text-align:center'><?= h(($local['Juegoalineacione']['capitan'] == 1) ? 'Si' : 'No' ); ?>&nbsp;</td>
                                        <?php if(empty($view)): ?>
                                        <td class="">
                                            <a class="editar editarJuegoalineacione" style="cursor:pointer;"></a>
                                            <a class="deleteDet deleteJuegoalineacione" style="cursor:pointer;"></a>
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-equipo2">
                        <div id="bloque-alineaciones-eq2" style="margin-top: 5px;">
                            <table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th><?php echo 'Jugador'; ?></th>
                                    <th><?php echo 'Posición' ?></th>
                                    <th><?php echo 'Capitán' ?></th>
                                    <?php if(empty($view)): ?>
                                    <th><?php echo '' ?></th>
                                    <?php endif; ?>
                                </tr>
                                </thead>
                                <tbody id="tbody-alineacion-visitante">
                                <?php
                                $i=0;
                                foreach ($alineaciones_visitante as $key => $visitante) {
                                    $class=null;
                                    if ($i++ % 2 == 0) {
                                        $class = ' class="altrow"';
                                    }
                                    ?>
                                    <tr <?= $class;?> id="juegoalineacione_<?=$visitante['Juegoalineacione']['id']?>">
                                        <td style='text-align:left'><?php
                                            if(!empty($visitante['Jugadore']['nombre_jugador'])){
                                                echo $this->Html->link($dorsalJugadores[$visitante['Jugadore']["id"]]." - ".$visitante['Jugadore']['nombre_jugador'],["controller"=>"jugadores","action"=>"view",$visitante['Jugadore']["id"]],["target"=>"_blank"]);
                                            }else{
                                                echo $this->Html->link($visitante['Persona']['nombre_persona'],["controller"=>"personas","action"=>"view",$visitante['Persona']["id"]],["target"=>"_blank"]);
                                            } ?>&nbsp;</td>
                                        <td style='text-align:center'><?=(!empty($visitante['Posicione']['posicion']))?h($visitante['Posicione']['posicion']):$visitante['Tipopersona']['tipopersonas']; ?>&nbsp;</td>
                                        <td style='text-align:center'><?= h(($visitante['Juegoalineacione']['capitan'] == 1) ? 'Si' : 'No' ); ?>&nbsp;</td>
                                        <?php if(empty($view)): ?>
                                        <td class="">
                                            <a class="editar editarJuegoalineacione" style="cursor:pointer;"></a>
                                            <a class="deleteDet deleteJuegoalineacione" style="cursor:pointer;"></a>
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-sm-12" style="margin-top: 15px">
                <p class="text-center">No hay información registrada</p>
            </div>
        <?php } ?>
    </div>
</div>
<!-- Modal Para Registrar Alineaciones -->
<div class="modal fade" id="alineacionesSelect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="ico_close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">
                    <i class="fa fa-group"></i> Registrar Alineaciones
                </h3>
            </div>
            <div class="modal-body">
                <form id="formulario-alineaciones" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="alert alert-danger no-display" id="alert-modal" style="top:8px;">
                                <span class="icon icon-cross-circled"></span>
                                <span class="message" id="message-modal"></span>
                                <button type="button" class="close" data-dismiss="alert"></button>
                            </div>
                            <div class="alert alert-success no-display" id="alert-modal-success" style="top:8px;">
                                <span class="icon icon-cross-circled"></span>
                                <span class="message" id="message-modal-success"></span>
                                <button type="button" class="close" data-dismiss="alert"></button>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-6">
                                <div class="form-groupp">
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioAlineaciones" id="rbAlineacionesEqlocal"  class="rb-equipo-seleccionado" checked>Equipo Local</label>
                                    </div>
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioAlineaciones" id="rbAlineacionesEqvisitante" class="rb-equipo-seleccionado">Equipo Visitante</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-groupp">
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioAlineacionesPersona" id="rbAlineacionesEqlocalJugador"  class="rb-equipo-seleccionado chkJP" checked>Jugador</label>
                                    </div>
                                    <div class="radio">
                                        <label class="lbl-modal"><input type="radio" name="optRadioAlineacionesPersona" id="rbAlineacionesEqvisitantePersona" class="rb-equipo-seleccionado chkJP">Persona</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-groupp">
                                    <label class="lbl-equipo-seleccionado"></label>
                                </div>
                                <hr>
                            </div>
                            <div class="col-xs-9 section-jugador">
                                <?php echo	$this->Form->input('jugadoralineaciones_id',
                                    array('label'   => 'Jugador',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'jugadoralineaciones-id',
                                        'empty'   =>'Seleccionar',
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="col-xs-9 section-persona display-none">
                                <?php echo  $this->Form->input('personaalineaciones_id',
                                    array('label'   => 'Persona',
                                        'class'   => 'form-control form-contacto',
                                        'div' => ['class'=>'form-groupp'],
                                        'value'   => '',
                                        'id'      => 'personaalineaciones-id',
                                        'empty'   =>'Seleccionar',
                                        'options'=>$personas,
                                        'required'=>'required'));
                                ?>
                            </div>
                            <div class="col-xs-5 section-jugador">
                                <?= $this->Form->input('posicionalineaciones',[
                                    'label'=>'Posición',
                                    'placeholder'=>'Posición',
                                    'class'=>'form-control form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'disabled' => 'disabled',
                                    'id'      => 'posicionalineaciones']);
                                ?>
                                <?= $this->Form->input('posicionidalineaciones',[
                                    'value'   => '',
                                    'type'    => 'hidden',
                                    'id'      => 'posicionidalineaciones']);
                                ?>
                            </div>
                            <div class="col-xs-5 section-persona display-none">
                                <?= $this->Form->input('cargoalineaciones',[
                                    'label'=>'Cargo',
                                    'placeholder'=>'Cargo',
                                    'class'=>'form-control form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'disabled' => 'disabled',
                                    'id'      => 'cargoalineaciones']);
                                ?>
                                <?= $this->Form->input('cargoidalineaciones',[
                                    'value'   => '',
                                    'type'    => 'hidden',
                                    'id'      => 'cargoidalineaciones']);
                                ?>
                            </div>
                            <div class="col-xs-4 section-jugador">
                                <?= $this->Form->input('dorsalalineaciones',[
                                    'label'=>'Dorsal',
                                    'placeholder'=>'Dorsal',
                                    'class'=>'form-control form-contacto',
                                    'div'=>['class'=>'form-groupp'],
                                    'value'   => '',
                                    'disabled' => 'disabled',
                                    'id'      => 'dorsalalineaciones']);
                                ?>
                            </div>
                            <div class="clearfix section-jugador"></div>
                            <div class="col-xs-9 section-jugador" style="margin-top: 10px;">
                                <?php echo  $this->Form->input('capitanalineaciones', [
                                    'type'  => 'checkbox',
                                    'label' => 'Capitán',
                                    'div'   => false,
                                    'style' =>'margin:0; margin-right:4px; margin-top:6px;',
                                    'id'    => 'capitanalineaciones']);
                                ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-danger" id="registrar-alineaciones">Almacenar</button>
            </div>
        </div>
    </div>
</div>
<script>
    var chkJP=false;
    var tbody_alineacion_local=true;
    var prefijo_juegoalineacione="juegoalineacione_";
    var equipoid_alineacion = '';
    var equipoid_alineacion_persona = <?= $equipo1['Equipo']['id'] ?>;
    var tipo='add';
    var juegoalineacione_id=0;
    function btnaddAlineacione(){
        tipo='add';
        var label="<i class='fa fa-group'></i> Registrar Alineaciones";
        document.getElementById('myModalLabel').innerHTML=label;
        if($("#rbAlineacionesEqlocal").is(':checked')) {
            $('#rbAlineacionesEqlocal').change();
        }else{
            $('#rbAlineacionesEqvisitante').change();            
        }
    }

    function chkJPFunction(){
        if(this.id=='rbAlineacionesEqvisitantePersona'){
            chkJP=true;
            addClassGroup('section-jugador','display-none');            
            removeClassGroup('section-persona','display-none');
        }else{
            chkJP=false;
            removeClassGroup('section-jugador','display-none');
            addClassGroup('section-persona','display-none');
        }
    }

    function addClassGroup(classNameElements, classAdd){
        var className=document.getElementsByClassName(classNameElements);
            for (var i = 0; i < className.length; i++) {
                addClass(className[i],classAdd);
            };        
    }

    function removeClassGroup(classNameElements, classAdd){
        var className=document.getElementsByClassName(classNameElements);
            for (var i = 0; i < className.length; i++) {
                removeClass(className[i],classAdd);
            };        
    }

    function selectPersona(){
        if(this.value!=''){
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'get_info_persona')); ?>";
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:this.value},
                dataType: 'json',
                success:function(resp){
                    $("#cargoalineaciones").val(resp.Tipopersona.tipopersonas);
                    $("#cargoidalineaciones").val(resp.Tipopersona.id);                   
                }
            });
        }
    }

    function editarJuegoalineacione(){
        tipo='edit';
        var label="<i class='fa fa-group'></i> Editar Alineaciones";
        document.getElementById('myModalLabel').innerHTML=label;
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_juegoalineacione);
        var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'getjuegoalineacione')); ?>";
        $.ajax({
            url: url,
            type: "post",
            cache: false,
            data: {id:id[1]},
            dataType: 'json',
            success:function(resp){
                if(resp.chkequipolocal){ 
                    document.getElementById('rbAlineacionesEqlocal').checked=true; 
                }
                else{ 
                    document.getElementById('rbAlineacionesEqvisitante').checked=true;
                }

                //llenado de jugadores
                var jugadores=resp.jugadores;
                document.getElementById('jugadoralineaciones-id').innerHTML="";
                crearOptionSelect('Seleccionar','','jugadoralineaciones-id');
                for (var i = 0; i < jugadores.length; i++) {
                    var element=jugadores[i]['Jugadore']['nombre_jugador'];
                    var index=jugadores[i]['Jugadore']['id'];
                    crearOptionSelect(element,index,'jugadoralineaciones-id');
                };
                
                var className=document.getElementsByClassName('lbl-equipo-seleccionado');
                for (var i = 0; i < className.length; i++) {
                    className[i].innerText=resp.nombre_equipo;
                }
                equipoid_alineacion = resp.equipo_id;
                equipoid_alineacion_persona = resp.equipo_id;

                //lleando de personas
                var jugadores=resp.personas;
                document.getElementById('personaalineaciones-id').innerHTML="";
                crearOptionSelect('Seleccionar','','personaalineaciones-id');
                for (var i = 0; i < jugadores.length; i++) {
                    var element=jugadores[i]['Persona']['nombre_persona'];
                    var index=jugadores[i]['Persona']['id'];
                    crearOptionSelect(element,index,'personaalineaciones-id');
                };
                if(resp.jugadorid!=null){
                    document.getElementById('rbAlineacionesEqlocalJugador').checked=true;
                    chkJP=false;
                    removeClassGroup('section-jugador','display-none');
                    addClassGroup('section-persona','display-none');
                    document.getElementById('jugadoralineaciones-id').value=resp.jugadorid;
                    document.getElementById('posicionalineaciones').value=resp.posicion;
                    document.getElementById('dorsalalineaciones').value=resp.dorsal;
                    if(resp.capitan) document.getElementById('capitanalineaciones').checked=true;
                    else document.getElementById('capitanalineaciones').checked=false;
                }else{
                    document.getElementById('rbAlineacionesEqvisitantePersona').checked=true;
                    chkJP=true;
                    addClassGroup('section-jugador','display-none');            
                    removeClassGroup('section-persona','display-none');
                    document.getElementById('personaalineaciones-id').value=resp.personaid;
                    var cargo=resp.cargo;
                    document.getElementById('cargoalineaciones').value=cargo['Tipopersona']['tipopersonas'];
                    document.getElementById('cargoidalineaciones').value=cargo['Tipopersona']['id'];                    
                }
                $('#alineacionesSelect').modal('show');              
            }
        });
    }

    function deleteJuegoalineacione(){
        var padre=this.parentNode.parentNode;
        var id=padre.id.split(prefijo_juegoalineacione);
        juegoalineacione_id=id[1];
        bootbox.confirm({
            message: "¿Está seguro de eliminar el registro?, perderá los datos que se han ingresado.",
            buttons: {
                confirm: {
                    label: 'Si',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result) {
                    var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'deletejuegoalineacione')); ?>";
                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data: {id:juegoalineacione_id},
                        dataType: 'json',
                        success:function(resp){
                            bootbox.alert(resp.msj);
                            if(resp.resp){
                                var id_tbody_alineacion_equipo="#tbody-alineacion-local";
                                if(!resp.chkequipolocal) id_tbody_alineacion_equipo="#tbody-alineacion-visitante";
                                $(id_tbody_alineacion_equipo).load("<?= Router::url(array('controller' => 'juegos', 'action' => 'alineaciones')); ?>",
                                    {id: resp.juego_id, equipo1: resp.equipo_id, equipo2: null, render:true}
                                );                                
                            }                                                  
                        }
                    });                                
                }
            }
        });
    }

    document.getElementById('personaalineaciones-id').addEventListener('change',selectPersona, false);
    document.getElementById('btn-alineaciones').addEventListener('click',btnaddAlineacione,false);

    var className=document.getElementsByClassName('chkJP');
    for (var i = 0; i < className.length; i++) {
        className[i].addEventListener('change',chkJPFunction,false);
    }

    className=document.getElementsByClassName('editarJuegoalineacione');
    for (i = 0; i < className.length; i++) {
        className[i].addEventListener('click',editarJuegoalineacione,false);
    }
    
    className=document.getElementsByClassName('deleteJuegoalineacione');
    for (i = 0; i < className.length; i++) {
        className[i].addEventListener('click',deleteJuegoalineacione,false);
    }

    $(document).ready(function() {
        var juegoid = $( '#juego-id' ).val();
        var alineacion_url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
        var torneo = $("#JuegoTorneoId").val();
        // Al momento de cargar la pagina
        $.ajax({
            url: alineacion_url,
            type: "post",
            cache: false,
            data: {id:juegoid,torneo:torneo},
            dataType: 'json',
            success:function(resp){
                $("#jugadoralineaciones-id").html(resp.jugadores);
                $("#jugadoralineaciones-id").val('');
                $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);
            }
        });

        //Al momento de hacer click en almacenar un jugador en la alineacion
        $("#registrar-alineaciones").click(function() {
            var juego_id = $( '#juego-id' ).val();
            var url = '';

            // Se obtiene la informacion ingresada de la alineacion
            var formData = $('#formulario-alineaciones').serializeArray();

            // Se adiciona el id del juego a la informacion ingresada de la alineacion
            formData.push({name: 'data[juego_id]', value: juego_id});

            // Se adiciona el id del equipo seleccionado a la informacion ingresada de la alineacion
            formData.push({name: 'data[equipo_id]', value: equipoid_alineacion});

            // Se adiciona el tipo de acción(add/edit) a la informacion ingresada de la alineacion
            formData.push({name: 'data[tipo]', value: tipo});
            var equipoid_extra=equipoid_alineacion;
            if($("#rbAlineacionesEqlocal").is(':checked')) {
                url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_alineacion_local')); ?>";
            } else {
                tbody_alineacion_local=false;
                url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_alineacion_visitante')); ?>";
            }

            if(chkJP){
                formData.push({name: 'data[equipo_id]', value: equipoid_alineacion_persona});
                url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'registrar_alineacion_persona')); ?>";
                equipoid_extra=equipoid_alineacion_persona;                
            }

            request = $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: formData,
                dataType: 'json'
            });

            request.done(function (response, textStatus, jqXHR){
                if(response.msg != 'error') {
                    if(response.msg=='exito'){
                        var id_tbody_alineacion_equipo="#tbody-alineacion-local";
                        if(!tbody_alineacion_local) id_tbody_alineacion_equipo="#tbody-alineacion-visitante";
                        $(id_tbody_alineacion_equipo).load("<?= Router::url(array('controller' => 'juegos', 'action' => 'alineaciones')); ?>",
                            {id: juego_id, equipo1: equipoid_extra, equipo2: null, render:true}
                        );
                        showAlert('Alineación registrada exitosamente','modal-success');
                    }else{
                        showAlert(response.msg,'modal');
                    }
                }
            });

            return false;
        });        

        // Si se selecciona al equipo local en el modal
        $('#rbAlineacionesEqlocal').change(function (e) {
            tipo='add';
            var id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_local')); ?>";
            var torneo = $("#JuegoTorneoId").val();
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadoralineaciones-id").html(resp.jugadores);
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);

                    equipoid_alineacion = '';
                    equipoid_alineacion_persona = resp.equipo_id; 
                    $("#jugadoralineaciones-id").val('');
                    $("#posicionidalineaciones").val('');
                    $("#posicionalineaciones").val('');
                    $("#dorsalalineaciones").val('');
                    $("#personaalineaciones-id").val('');
                    $('#cargoalineaciones').val('');
                    $('#cargoidalineaciones').val('');
                    document.getElementById('capitanalineaciones').checked=false;
                }
            });
        });

        // Si se selecciona al equipo visitante en el modal
        $('#rbAlineacionesEqvisitante').change(function (e) {
            tipo='add';
            var id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'equipo_visitante')); ?>";
            var torneo = $("#JuegoTorneoId").val();
            $.ajax({
                url: url,
                type: "post",
                cache: false,
                data: {id:id,torneo:torneo},
                dataType: 'json',
                success:function(resp){
                    $("#jugadoralineaciones-id").html(resp.jugadores);
                    $(".lbl-equipo-seleccionado").html(resp.nombre_equipo);

                    equipoid_alineacion = '';
                    equipoid_alineacion_persona = resp.equipo_id;
                    $("#jugadoralineaciones-id").val('');
                    $("#posicionidalineaciones").val('');
                    $("#posicionalineaciones").val('');
                    $("#dorsalalineaciones").val('');
                    $("#personaalineaciones-id").val('');
                    $('#cargoalineaciones').val('');
                    $('#cargoidalineaciones').val('');
                    document.getElementById('capitanalineaciones').checked=false;
                }
            });
        });

        // Al momento de seleccionar al jugador en el modal
        $('#jugadoralineaciones-id').change(function (e) {
            var id = $( this ).val();
            var juego_id = $( '#juego-id' ).val();
            var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'get_info_jugador')); ?>";
            var equipo_seleccionado = $('input[name=optRadioAlineaciones]:checked', '#formulario-alineaciones').val();
            var torneo_id = $("#JuegoTorneoId").val();
            if(id != '') {
                $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id:id, juego_id: juego_id,torneo_id:torneo_id},
                    dataType: 'json',
                    success:function(resp){
                        equipoid_alineacion = resp.dorsal.Jugadoresxequipo.equipo_id;
                        $("#posicionidalineaciones").val(resp.jugador.Jugadore.posicione_id);
                        $("#posicionalineaciones").val(resp.posicione_id.Posicione.posicion);
                        $("#dorsalalineaciones").val(resp.dorsal.Jugadoresxequipo.dorsal);
                    }
                });
            }
        });
    });
</script>


