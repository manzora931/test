<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script(['datepicker-es', 'funciones/validaciones.js']) ?>
<?php
echo $this->Html->css("select2");
echo $this->Html->script("select2");
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; // debug($this->data['Juego']);  ?>
<style>
	.tb_tarea tr td input {
		margin-top: 10px;
	}
	.margen{
		margin-left: 20px;
	}
	.margenbtn{
		margin-left: 50px !important;
	}
	form div {
		clear: initial;
	}

	.detalle-juego {
		margin-top: 20px;
	}

	.encabezado-detalle-juego {
		margin-bottom: 10px;
	}

	.tablinks {
		text-decoration: none;
		color: #000000;
	}

	.no-display {
		display: none;
	}

	.btn-crear {
		background-color: #4160a3;
		color: #fff;
		border-radius: 5px;
		width: 120px;
		margin-left: 5px;
		margin-top: 20px;
		text-decoration: none;
	}
	.btn-crear:hover {
		color: #fff;
	}
	.modal-dialog {
		width: 700px;
	}
	#crearContacto {
		margin-top: 0px;
	}
	.form-contacto {
		display: inline;
	}
	select.form-contacto {
		width: 100%;
	}
	.form form fieldset div label.lbl-modal {
		padding-top: 0;
		color: #555;
	}

	.modal-footer .btn + .btn {
		margin-bottom: 10px;
	}

	.radio input[type="radio"] {
		margin-top: 4px;
	}
	input[type="checkbox"] {
		margin-top: 4px;	
	}

	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
    .form form fieldset{
        width: 100%;
    }
    /**SELECT2 STYLE**/
    .select2-container{
        margin-bottom: 7px;
    }
</style>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}

	jQuery(function() {
		$('.datepicker').datepicker({
			dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,
			yearRange: '1900:2030'
		});
		$.datepicker.regional["es"];
	});
</script>
<div class="juegos form container-fluid">
	<?php echo $this->Form->create('Juego',array('id'=>'formulario')); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Juego'); ?></legend>

		<div class="col-md-12">
			<div class="alert alert-danger" id="alerta" style="display: none">
				<span class="icon icon-check-circled" id="msjalert"></span>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
			<div class="alert alert-danger no-display" id="alert">
				<span class="icon icon-cross-circled"></span>
				<span class="message"></span>
				<button type="button" class="close" data-dismiss="alert"></button>
			</div>
		</div>

        <div class="row">
            <div class="col-lg-7 col-md-7">
                <div class="col-md-12 head-juegos" style="vertical-align: middle;">
                    <div class="col-md-1" id="imgEq1" style="padding-left: 0;">

                    </div>
                    <div class="col-md-3" style="margin-top: 13px;">
                        <?=$this->Form->input('equipo1_id',[
                            'label'=>'',
                            'class'=>'form-control validate[required] equipos',
                            'style'=>'margin-bottom:10px;',
                            'options'=> $equipos,
                            'empty'=>"Seleccionar",
                            'required'=>true,
                            'div'=>['class'=>"form-groupp"],
                            "onchange"=>"loadImg(this.value, 1);"
                        ]);?>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2" style="margin-top: 5px;">
                        <?= $this->Form->input('marcadore1',[
                            'label'=>'',
                            'required'=>true,
                            'readonly'=>true,
                            'type'=>'text',
                            'style'=>'height:40px; font-weight: bold; text-align: center; font-size: 18px; margin-bottom:10px;',
                            'class'=>'form-control entero validate[required]',
                            'div'=>['class'=>'form-groupp'],
                        ]); ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-lg-2" style="margin-top: 5px;">
                        <?= $this->Form->input('marcadore2',[
                            'label'=>'',
                            'required'=>true,
                            'readonly'=>true,
                            'type'=>'text',
                            'style'=>'height:40px; font-weight: bold; text-align: center; font-size: 18px; margin-bottom:10px;',
                            'class'=>'form-control entero validate[required]',
                            'div'=>['class'=>'form-groupp'],
                        ]); ?>
                    </div>
                    <div class="col-md-3" style="margin-top: 13px;">
                        <?= $this->Form->input('equipo2_id',[
                            'label'=>'',
                            'class'=>'form-control validate[required] equipos',
                            'style'=>'margin-bottom:10px;',
                            'options'=>$equipos,
                            'empty'=>"Seleccionar",
                            'required'=>true,
                            'div'=>['class'=>"form-groupp"],
                            "onchange"=>"loadImg(this.value, 2);"
                        ]);?>
                    </div>
                    <div class="col-md-1" id="imgEq2">

                    </div>
                    <!------Resultado primer tiempo-------->
                    <div class="clearfix"></div>
                    <div class="col-md-1" id="imgEq1" style="padding-left: 0;">

                    </div>
                    <div class="col-md-3" style="margin-top: 13px;">
                        <label class="pull-right" style="color: #fff; font-weight: bold;">1er. Tiempo:</label>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2" style="margin-top: 5px;">
                        <?= $this->Form->input('marcadore1pt',[
                            'label'=>'',
                            'type'=>'text',
                            'readonly'=>true,
                            'style'=>'height:30px; text-align: center; margin-bottom:10px;',
                            'class'=>'form-control entero validate[required]',
                            'div'=>['class'=>'form-groupp'],
                        ]); ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-lg-2" style="margin-top: 5px;">
                        <?= $this->Form->input('marcadore2pt',[
                            'label'=>'',
                            'type'=>'text',
                            'readonly'=>true,
                            'style'=>'height:30px; text-align: center; margin-bottom:10px;',
                            'class'=>'form-control entero validate[required]',
                            'div'=>['class'=>'form-groupp'],
                        ]); ?>
                    </div>
                    <div class="col-md-3" style="margin-top: 13px;">

                    </div>
                    <div class="col-md-1" id="imgEq2">

                    </div>
                </div>

                <div class="col-md-12 interlineado borde1">
                    <div class="col-md-3"><span>Fecha del encuentro:</span></div>
                    <div class="col-md-4">
                        <?= $this->Form->input("fecha",[
                            'type'=>'text',
                            'class'=>"form-control datepicker",
                            'style'=>'margin-bottom:10px;',
                            'required'=>"required",
                            'label'=>"",
                            'readonly'

                        ]);?>
                    </div>
                    <div class="col-md-5 col-lg-5">
                        <?= $this->Form->input("sumapuntos", [
                            "checked"=>"checked",
                            "label"=>"Acumula Puntos"
                        ]);   ?>
                    </div>
                </div>
                <div class="col-md-12 borde2">
                    <div class="col-md-3"><span>Hora:</span></div>
                    <div class="col-md-7">
                        <?= $this->Form->input('hora',[
                            'label'=>'',
                            'placeholder'=>'Hora',
                            'class'=>'form-control validate[required] times',
                            'style'=>'margin-bottom:10px;',
                            'div'=>['class'=>'form-groupp'],
                            'type'=>'text'
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5">
                <div class="col-md-12 head-juegos">
                    <span style="font-size: 18px;font-weight: bold;">Datos generales</span>
                </div>
                <div class="col-md-12 borde1 ">
                    <div class="col-md-3 text-left">
                        <span>Torneo:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('torneo_id',[
                            'label'=>false,
                            'class'=>'form-control validate[required]',
                            'style'=>'margin-bottom:10px;',
                            //'onchange'=>"load_inter(this.id,0);",
                            'empty'=>"Seleccionar",
                            'required'=>true,
                            'div'=>['class'=>"form-groupp"],

                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 interlineado borde2">
                    <div class="col-md-3"><span>Etapa:</span></div>
                    <div class="col-md-7">
                        <?= $this->Form->input('etapa_id',[
                            'label'=>'',
                            'class'=>'form-control validate[required]',
                            'style'=>'margin-bottom:10px;',
                            //'onchange'=>"load_inter(this.id,0);",
                            'empty'=>"Seleccionar",
                            'required'=>true,
                            'div'=>['class'=>"form-groupp"],

                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 borde1">
                    <div class="col-md-3">
                        <span>Fase de Grupos:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('grupo_id',[
                            'label'=>'',
                            'class'=>'form-control validate[required]',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'div'=>['class'=>"form-groupp"]
                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 borde2 interlineado">
                    <div class="col-md-3">
                        <span>Estadio:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('estadio_id',[
                            'label'=>'',
                            'class'=>'form-control validate[required]',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'required'=>'required',
                            'div'=>['class'=>"form-groupp"]
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-7 col-lg-7">
                <div class="col-md-12 head-juegos">
                    <span style="font-size: 18px;font-weight: bold;">Más Información</span>
                </div>
                <div class="col-md-12 borde1 ">
                    <div class="col-md-3">
                        <span>Temperatura:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('temperatura',[
                            'label'=>'',
                            'placeholder'=>'Temperatura',
                            'class'=>'form-control decimal validate[required]',
                            'style'=>'margin-bottom:10px;',
                            'div'=>['class'=>'form-groupp'],
                            'type'=>"number",
                            "step"=>1
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 interlineado borde2">
                    <div class="col-md-3"><span>Asistencia:</span></div>
                    <div class="col-md-7">
                        <?= $this->Form->input('asistencia',[
                            'label'=>'',
                            'placeholder'=>'Asistencia',
                            'class'=>'form-control entero validate[required]',
                            'style'=>'margin-bottom:10px;',
                            'div'=>['class'=>'form-groupp'],
                            'type'=>'number',
                            "step"=>1,
                            "onchange"=>"valAsistencia(this.value);"
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 borde1">
                    <div class="col-md-3">
                        <span>Taquilla:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('taquilla',[
                            'label'=>'',
                            'placeholder'=>'Taquilla',
                            'class'=>'form-control entero',
                            'style'=>'margin-bottom:10px;',
                            'div'=>['class'=>'form-groupp'],
                            'type'=>'number',
                            "step"=>1,
                            "onchange"=>"valTaquilla(this.value);"
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 borde2 interlineado">
                    <div class="col-md-3">
                        <span>Porcentaje utilizado en estadio:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('porcestadio',[
                            'label'=>'',
                            'placeholder'=>'Porcentaje utilizado en estadio',
                            'class'=>'form-control entero',
                            'style'=>'margin-bottom:10px;',
                            'div'=>['class'=>'form-groupp'],
                            'type'=>'number',
                            "onchange"=>"valPorcentaje(this.value);",
                            "step"=>1
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-lg-5">
                <div class="col-md-12 head-juegos">
                    <span style="font-size: 18px;font-weight: bold;">Árbitros</span>
                </div>
                <div class="col-md-12 borde1 ">
                    <div class="col-md-3">
                        <span>Principal:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('referi',[
                            'label'=>'',
                            'class'=>'form-control validate[required] arbiitro autocomplete',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'options'=>$arbitros,

                            'type'=>'select'
                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 interlineado borde2">
                    <div class="col-md-3">
                        <span>Auxiliar 1:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('referiaux1',[
                            'label'=>'',
                            'class'=>'form-control validate[required] arbiitro autocomplete',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'options'=>$arbitros,

                            'type'=>'select'
                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 borde1">
                    <div class="col-md-3">
                        <span>Auxiliar 2:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('referiaux2',[
                            'label'=>'',
                            'class'=>'form-control validate[required] arbiitro autocomplete',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'options'=>$arbitros,

                            'type'=>'select'
                        ]);?>
                    </div>
                </div>
                <div class="col-md-12 borde2 interlineado">
                    <div class="col-md-3">
                        <span>4o. árbitro:</span>
                    </div>
                    <div class="col-md-7">
                        <?= $this->Form->input('referi2',[
                            'label'=>'',
                            'class'=>'form-control validate[required] arbiitro autocomplete',
                            'style'=>'margin-bottom:10px;',
                            'empty'=>"Seleccionar",
                            'options'=>$arbitros,

                            'type'=>'select'
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
		<?php if(isset($this->data['Juego'])) { ?>
		<?php //if(1 == 2) { ?>
			<div class="row">
				<div class="col-md-offset-1 col-md-10 detalle-juego">
					<h3 class="encabezado-detalle-juego">Detalle del Juego</h3>

					<ul class="nav nav-tabs content-tabs" id="maincontent" role="tablist">
						<li class="active"><a href="#alineaciones" class="tablinks" role="tab" data-toggle="tab">Alineaciones</a></li>
						<li><a href="#minuto" class="tablinks" role="tab" data-toggle="tab">Minuto a minuto</a></li>
						<li><a href="#notas" class="tablinks" role="tab" data-toggle="tab">Notas</a></li>
						<li><a href="#cronica" class="tablinks" role="tab" data-toggle="tab">Crónica</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="alineaciones">
							<div id="bloque-alineaciones"></div>
						</div>
						<div class="tab-pane fade" id="minuto">
							<div id="bloque-minutoxminuto"></div>
						</div>
						<div class="tab-pane fade" id="notas">
							<div id="bloque-notas"></div>
						</div>
						<div class="tab-pane fade" id="cronica">
							<div id="bloque-cronica"></div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</fieldset>

	<div class="actions">
		<div><?= $this->Form->button('Almacenar', [
				'label' => false,
				'type' => 'submit',
				'class' => 'btn btn-default',
				'div' => [
					'class' => 'form-group'
				]
			]); ?>
			<?php echo $this->Form->end();?></div>
		<div><?php echo $this->Html->link(__('Listado de Juegos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
	</div>
</div>
<script>
    function loadImg(id, band){
        if(parseInt(band)==1){
            //equipo 1
            if(id!==''){
                var url = getURL()+"getImg2";
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {id:id},
                    async: false,
                    cache: false,
                    success: function (resp) {
                        $("#imgEq1").html(resp);
                    }
                });
            }
        }else{
            //equipo 2
            if(id!==''){
                var url = getURL()+"getImg2";
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {id:id},
                    async: false,
                    cache: false,
                    success: function (resp) {
                        $("#imgEq2").html(resp);
                    }
                });
            }
        }
    }
    function valPorcentaje(valor){
        var porcentaje = parseFloat(valor);
        if(porcentaje>100){
            $("#JuegoPorcestadio").val('');
            $("#msjalert").text("El porcentaje no puede ser mayor al 100%");
            $("#alerta").slideDown();
            setTimeout(function () {
                $("#alerta").slideUp();
            },4000);
        }else{
            if(porcentaje<0){
                $("#JuegoPorcestadio").val('');
                $("#msjalert").text("El porcentaje no puede ser menor al 1%");
                $("#alerta").slideDown();
                setTimeout(function () {
                    $("#alerta").slideUp();
                },4000);
            }
        }
    }
    function valAsistencia(valor){
        var asistencia = parseFloat(valor);
        if(asistencia<0){
            $("#JuegoAsistencia").val('');
            $("#msjalert").text("No puede ingresar valores negativos");
            $("#alerta").slideDown();
            setTimeout(function () {
                $("#alerta").slideUp();
            },4000);
        }
    }
    function valTaquilla(valor){
        var asistencia = parseFloat(valor);
        if(asistencia<0){
            $("#JuegoTaquilla").val('');
            $("#msjalert").text("No puede ingresar valores negativos");
            $("#alerta").slideDown();
            setTimeout(function () {
                $("#alerta").slideUp();
            },4000);
        }
    }

	$(document).ready(function() {
        //INICIALIZA EL SELECT2
        $(".autocomplete").select2();
		// Cargando data del detalle del juego
		var id = $('#juego-id').val();
		var equipo1 = $('#JuegoEquipo1Id').val();
		var equipo2 = $('#JuegoEquipo2Id').val();
		$('#bloque-alineaciones').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'alineaciones')); ?>",
			{id: id, equipo1: equipo1, equipo2: equipo2}
		);
		$('#bloque-minutoxminuto').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'minutoxminuto')); ?>",
			{id: id}
		);
		$('#bloque-notas').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'notas')); ?>",
			{id: id}
		);
		$('#bloque-cronica').load("<?= Router::url(array('controller' => 'juegos', 'action' => 'cronica')); ?>",
			{id: id}
		);
		// Fin Cargando data del detalle del juego

		// Obteniendo equipos segun torneo seleccionado
		$('#JuegoTorneoId').change(function (e) {
			var id = $( this ).val();
			var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'get_equipos')); ?>";

			if(id != '') {
				$.ajax({
					url: url,
					type:'post',
					data:{
						id: id
					},
                    dataType: 'json',
					success:function(resp){
						$("#JuegoEquipo1Id").html(resp["equipos"]);
						$("#JuegoEquipo1Id").val('');

						$("#JuegoEquipo2Id").html(resp["equipos"]);
						$("#JuegoEquipo2Id").val('');

						$("#JuegoGrupoId").html(resp["grupos"]);
					}
				});

			}
		});

		// Al momento de seleccionar una etapa de la lista desplegable
		$('#JuegoEtapaId').change(function (e) {
			var id = $( this ).val();
			var url = "<?= Router::url(array('controller' => 'juegos', 'action' => 'verificar_etapa')); ?>";
			var element = document.getElementById('JuegoGrupoId');

			if(id == '1') {
				element.disabled = false;
			} else {
				element.disabled = true;
			}
		});

		// Al seleccionar una opcion del listado de equipos
		$('.equipos').change(function (e) {
			var arrayValores = [];
			var elemento = $( this );
			var valorSeleccionado = elemento.val();

			// Se obtiene la opcion seleccionada en cada select de entidades
			$(".equipos").each(function() {
				if($(this).attr('id') !== elemento.attr('id')) {
					arrayValores.push($(this).val());
				}
			});

			// Verifica si la opcion ha sido seleccionada con anterioridad
			if(jQuery.inArray(valorSeleccionado, arrayValores) !== -1) {
				elemento.val("");
				$(".message").text('El equipo ya ha sido seleccionado');
				$("#alert").slideDown();
				setTimeout(function () {
					$("#alert").slideUp();
				}, 5000);
			}
		});

		// Al seleccionar una opcion del listado de arbitros
		$('.arbiitro').change(function (e) {
			var arrayValores = [];
			var elemento = $( this );
			var valorSeleccionado = elemento.val();

			// Se obtiene la opcion seleccionada en cada select de entidades
			$(".arbiitro").each(function() {
				if($(this).attr('id') !== elemento.attr('id')) {
					arrayValores.push($(this).val());
				}
			});

			// Verifica si la opcion ha sido seleccionada con anterioridad
			if(jQuery.inArray(valorSeleccionado, arrayValores) !== -1) {
				elemento.val("");
				$(".message").text('El arbitro ya ha sido seleccionado');
				$("#alert").slideDown();
				setTimeout(function () {
					$("#alert").slideUp();
				}, 5000);
			}
		});

		//Validando hora
		$(".times").keydown(function (e) {
			// Codigo de caracteres permitidos, sin tomar en cuenta numeros y letras.
			var keycodes = [16, 190];
			ValCaracteresValidos(e, keycodes, true, false);
		});

		//Validando numeros enteros
		$(".entero").keydown(function (e) {
			// Codigo de caracteres permitidos, sin tomar en cuenta numeros y letras.
			var keycodes = [];
			ValCaracteresValidos(e, keycodes, true, false);
		});

		// validando numeros decimales
		$(".decimal").keydown(function (e) {
			// Codigo de caracteres permitidos, sin tomar en cuenta numeros y letras.
			// Teclas: punto decimal
			var keycodes = [110, 190];
			ValCaracteresValidos(e, keycodes, true, false);
		});

		$('.nav-tabs a').click(function(e) {
			e.preventDefault();
			$(this).tab('show');
		});

		$('.panel-group').on('shown.bs.collapse', function() {
			var panel = $(this).find('.in');
			$('html, body').animate({
				scrollTop: panel.offset().top + (-60)
			}, 500);
		});
	});
</script>