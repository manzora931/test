<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<div class="bancos form container-fluid">
    <?php echo $this->Form->create('Banco'); ?>
    <fieldset>
        <legend><?php echo __('Adicionar Banco'); ?></legend>
        <div class="alert alert-danger col-xs-12" id="alerta" style="display: none">
            <span class="icon icon-cross-circled"></span>
            <span class="message"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="col-xs-12">
            <?php
            echo $this->Form->input('id');
            echo $this->Form->input('nombre',[
                'label'=>'Nombre',
                'required'=>true,
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('paise_id',[
                'label'=>'País',
                'required'=>true,
                'class' => 'form-control',
                'empty'=>'Seleccionar',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('telefono',[
                'label'=>'Teléfono',
                'placeholder'=>'2222-2222',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('email',[
                'label'=>'Correo',
                'placeholder'=>'correo@correo.com',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('direccion',[
                'label'=>'Dirección',
                'class' => 'form-control',
                'rows'=>3,
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('activo',[
                'label'=>'Activo',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
        </div>
    </fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
    <div>
        <?php echo $this->Html->link(__('Listado de Bancos'), array('action' => 'index'),['class'=>'btn btn-default']); ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        // Verifica si tiene proyectos relacionados al darle  click al checkbox del campo activo
        $( "#BancoActivo" ).on( "change", function() {
            var url = "<?= Router::url(array('controller' => 'bancos', 'action' => 'verify_proyectos')); ?>";
            var id = $('#BancoId').val();

            if(!$(this).is( ":checked" )) {
                $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id: id},
                    dataType: 'json',
                    success: function (response) {
                        if(response.bancos) {
                            $( "#BancoActivo" ).prop('checked', true);

                            $("#alerta .message").text('No se puede desactivar, tiene proyectos relacionados');
                            $("#alerta").slideDown();

                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                        }
                    }
                });
            }
        });
    });

    jQuery(function(){
        // Verifica si tiene proyectos relacionados al darle  click al checkbox del campo activo
        $( "#BancoActivo" ).on( "change", function() {
            var url = "<?= Router::url(array('controller' => 'bancos', 'action' => 'verify_proyectos')); ?>";
            var id = $('#BancoId').val();

            if(!$(this).is( ":checked" )) {
                $.ajax({
                    url: url,
                    type: "post",
                    cache: false,
                    data: {id: id},
                    dataType: 'json',
                    success: function (response) {
                        if(response.bancos) {
                            $( "#BancoActivo" ).prop('checked', true);

                            $("#alerta .message").text('No se puede desactivar, tiene proyectos relacionados');
                            $("#alerta").slideDown();

                            setTimeout(function () {
                                $("#alerta").slideUp();
                            }, 4000);
                        }
                    }
                });
            }
        });
    });
</script>