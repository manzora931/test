<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style>
<div class="bancos view container-fluid">
    <h2><?= "Banco"; ?></h2>
    <?php
    if( isset( $_SESSION['save_banco'] ) ) {
        if( $_SESSION['save_banco'] == 1 ){
            unset( $_SESSION['save_banco'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Banco almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($banco['Banco']['id']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Nombre'); ?></td>
            <td>
                <?php echo h($banco['Banco']['nombre']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('País'); ?></td>
            <td>
                <?php echo h($banco['Paise']['pais']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Teléfono'); ?></td>
            <td>
                <?php echo h($banco['Banco']['telefono']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Correo'); ?></td>
            <td>
                <?php echo h($banco['Banco']['email']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Dirección'); ?></td>
            <td>
                <?php echo h($banco['Banco']['direccion']); ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Activo'); ?></td>
            <td>
                <?php echo ($banco['Banco']['activo']==1)?"Si":"No"; ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Creado'); ?></td>
            <td>
                <?= $banco['Banco']['usuario']." (".$banco['Banco']['created'].")"; ?>
            </td>
        </tr>
        <tr>
            <td scope="row"><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($banco['Banco']['usuariomodif']!='')?$banco['Banco']['usuariomodif']." (".$banco['Banco']['modified'].")":""; ?>
            </td>
        </tr>
    </table>

    <div class="actions">
     <div><?php echo $this->Html->link(__('Editar Banco'), array('action' => 'edit', $banco['Banco']['id']),['class'=>"btn btn-default"]); ?> </div>
        <div><?php echo $this->Html->link(__('Listado de Bancos'), array('action' => 'index'),['class'=>"btn btn-default"]); ?></div>
       
    </div>
</div>



<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>

