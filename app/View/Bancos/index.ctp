<style>
    div.index{
        margin-top: 15px;
        width: 100%;
    }
    #search-by{
        width: 110px;
    }
    table#tblin{
        width: 100%;
    }
    table tr td.form-group {
        padding-left: 15px;
    }
    .table > thead > tr > th.bdr {
        border-top-right-radius: 5px;
    }
    .table > thead > tr > th.bdr1 {
        border-top-left-radius: 5px;
    }
    div.input-group > div{
        display: inline-block;
    }
    div.input-group{
        display: inline-block;
    }
    td#check{
        width: 20%;
        padding-left: 35px;

    }
    .table tbody tr td {
        font-size: 16px;

    }
    .table tbody tr td:first-child {
        width:80px;
    }

    .table tbody tr td:last-child {
        width:110px;

    }

    table.table tbody{
        border:1px solid #ccc;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

    .actions ul .style-btn a{
        font-family: 'Calibri', sans-serif;
        background-image: none;
        background-color: #c0c0c0 ;
        border-color: #606060	;
        border-radius: 1px;
        font-size: 14px;
    }
    .actions {
        margin: -33px 0 0 0;
    }

    .actions ul .style-btn a:hover{
        background: #c0c0c0;
        color:#000;
        border-color: #606060;
    }
    .form-control {
        height: 25px;
        border-radius: 0;
        border: solid 1px #4160a3;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    #search_box {
        border-top: solid 6px;
        border-bottom-color: white;
        border-top-color: #4160a3;

    }
    .index table tr:hover td {
        background: transparent;
    }
</style>
<div class="bancos index container-fluid">
	<h2><?php echo __('Bancos'); ?></h2>
    <span class="paginate-count clearfix">
		<?php echo $this->paginator->counter(array(
            'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
        ));	?>
	</span>

		<div id='search_box'>
		
		<?php
			if(isset($_SESSION['bancos']))
			{
			$real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\''));
			}
		?>	</div>
	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
	    <thead>
            <tr>
                <th width="30%"><?php echo $this->Paginator->sort('nombre'); ?></th>
                <th width="17%"><?php echo $this->Paginator->sort('paise_id'); ?></th>
                <th><?php echo $this->Paginator->sort('email'); ?></th>
                <th width="10%"><?php echo $this->Paginator->sort('telefono'); ?></th>
                <th width="8%"><?php echo $this->Paginator->sort('activo'); ?></th>
                <th width="10%" class=""><?php echo __('Acciones'); ?></th>
	        </tr>
        </thead>
        <tbody>
	<?php foreach ($bancos as $banco): ?>
            <tr>
                <td class="text-left"><?php echo h($banco['Banco']['nombre']); ?>&nbsp;</td>
                <td class="text-left"><?= $banco['Paise']['pais'];?></td>
                <td class="text-left"><?= h($banco['Banco']['email']); ?>&nbsp;</td>
                <td class="text-left"><?= h($banco['Banco']['telefono']); ?>&nbsp;</td>
                <td><?= ($banco['Banco']['activo']==1)?"Si":"No"; ?>&nbsp;</td>
            <td class="">
                <?php echo $this->Html->link(__(' ', true), array('action'=>'view', $banco['Banco']['id']),array('class'=>'ver')); ?>		<?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $banco['Banco']['id']),array('class'=>'editar')); ?>
            </td>
<?php endforeach; ?>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
			</div>
    <div class="actions">
        <ul>
            <li class="style-btn"><?php echo $this->Html->link(__('Crear Banco'), array('action' => 'add')); ?></li>
        </ul>
    </div>
</div>

