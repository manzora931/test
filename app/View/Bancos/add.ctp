<div class="bancos form container-fluid">
<?php echo $this->Form->create('Banco'); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Banco'); ?></legend>
        <div class="col-xs-12">
            <?= $this->Form->input('nombre',[
                    'label'=>'Nombre',
                    'required'=>true,
                    'class' => 'form-control',
                    'div' => ['class'=>'form-group']
                ]);
            ?>
            <?= $this->Form->input('paise_id',[
                'label'=>'País',
                'required'=>true,
                'class' => 'form-control',
                'empty'=>'Seleccionar',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('telefono',[
                'label'=>'Teléfono',
                'placeholder'=>'2222-2222',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('email',[
                'label'=>'Correo',
                'placeholder'=>'correo@correo.com',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('direccion',[
                'label'=>'Dirección',
                'class' => 'form-control',
                'rows'=>3,
                'div' => ['class'=>'form-group']
            ]);
            ?>
            <?= $this->Form->input('activo',[
                'label'=>'Activo',
                'class' => 'form-control',
                'div' => ['class'=>'form-group']
            ]);
            ?>
        </div>
	</fieldset>
</div>
<div class="actions">
    <div><?= $this->Form->button('Almacenar', [
            'label' => false,
            'type' => 'submit',
            'class' => 'btn btn-default',
            'div' => [
                'class' => 'form-group'
            ]
        ]); ?>
        <?php echo $this->Form->end();?></div>
	<div>
		<?php echo $this->Html->link(__('Listado de Bancos'), array('action' => 'index'),['class'=>'btn btn-default']); ?>
	</div>
</div>
