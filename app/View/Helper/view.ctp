<div class="facturas view">
<h2><?php  echo __('Factura');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo $factura['Factura']['id']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Organizacion'); ?></dt>
		<dd>
			<?php echo $this->html->link($factura['Organizacion']['organizacion'], array('controller'=> 'organizacions', 'action'=>'view', $factura['Organizacion']['id'])); ?>
			<!-- <?php echo $factura['Factura']['organizacion_id']; ?> -->
			&nbsp;
		</dd>
		<dt><?php echo __('Socio de Negocio'); ?></dt>
		<dd>
			<?php echo $this->html->link($factura['Socionegocio']['nombre'], array('controller'=> 'socionegocios', 'action'=>'view', $factura['Socionegocio']['id'])); ?>
			<!-- <?php echo $factura['Factura']['socionegocio_id']; ?> -->
			&nbsp;
		</dd>
		<dt><?php echo __('Num. Docto.'); ?></dt>
		<dd>
			<b><?php echo $factura['Factura']['numdocto']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('A nombre de'); ?></dt>
		<dd>
			<b><?php echo $factura['Factura']['anombrede']; ?></b>
			<!-- <?php echo $factura['Factura']['socionegocio_id']; ?> -->
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo documento'); ?></dt>
		<dd>
			<?php echo $this->html->link($factura['Tipofactura']['tipofactura'], array('controller'=> 'tipofacturas', 'action'=>'view', $factura['Tipofactura']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha documento'); ?></dt>
		<dd>
			<?php echo date('d/m/Y', strtotime($factura['Factura']['fechadocto'])); ?>
			&nbsp;
		</dd>
		<?php if ($factura['Factura']['usuarioanulada']) { ?>
		
		<dt><?php echo __('Estado'); ?></dt>
		<dd>
			<b><?php echo "<font color='red'>DOCUMENTO ANULADO el ". date('d/m/Y', strtotime($factura['Factura']['fecanulada']))." por ".$factura['Factura']['usuarioanulada']."</font>" ?></b>
			&nbsp;
		</dd>
		<?php
			   }
		 ?>
		<dt><?php echo __('Observacion'); ?></dt>
		<dd>
			<?php echo $factura['Factura']['observacion']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Usuario'); ?></dt>
		<dd>
			<?php echo $factura['Factura']['usuario']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Creado'); ?></dt>
		<dd>
			<?php echo date('d/m/Y G:i:s', strtotime($factura['Factura']['created'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modificado'); ?></dt>
		<dd>
			<?php echo date('d/m/Y G:i:s', strtotime($factura['Factura']['modified'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subtotal Documento'); ?></dt>
		<dd>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['subtotal'],2); ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('IVA:'); ?></dt>
		<dd>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['impuesto'],2); ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Retencion (-):'); ?></dt>
		<dd>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['retencion'],2); ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<b>$&nbsp;<?php echo number_format($factura['Factura']['total'],2); ?></b>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
	    <li><a href="#" onClick="window.open('/tec101/sistemas/audiomed/inven/facturas/impfactura/<?php echo $factura['Factura']['id']; ?>','imp','height=500,width=900,menubar=1,resizable=1,scrollbars=1');">Imprimir Documento</a></li>
		<li><?php echo $this->html->link(__('Editar Factura', true), array('action'=>'edit', $factura['Factura']['id'])); ?> </li>
		<li><?php echo $this->html->link(__('Anular Factura', true), array('action'=>'anular', $factura['Factura']['id']), null, sprintf(__('Esta seguro de anular la factura del registro # %s?', true), $factura['Factura']['id'])); ?> </li>
		<li><?php echo $this->html->link(__('Listado de Facturas', true), array('action'=>'index')); ?> </li>
		<li><?php echo $this->html->link(__('Nueva Factura', true), array('action'=>'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<h3><?php echo __('Detalle del Documento');?></h3>
	<?php if (!empty($factura['Detfactura'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Servicio'); ?></th>
		<th><?php echo __('Detalle'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Valor Unitario'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th class="actions"><?php echo __('Acciones');?></th>
	</tr>
	<?php
		$i = 0;
		$vcantidad = 0; 
		$vsubtotal = 0; 
		$vdescuento = 0; 
		$vtotal = 0;
		foreach ($factura['Detfactura'] as $detfac):
			$class = null;
			//$producto = 'Madera';
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
				//$producto = 'Pegamento 3M';
			}
		?>
		<tr<?php echo $class;?>>
			<!-- <td><?php echo $detmovimiento['Productos']['producto'];?></td>  -->
			<td><?php echo $servicios[$detfac['servicio_id']];?></td>
			<td><?php echo $detfac['detalle'];?></td>
			<td><?php echo $detfac['cantidad'];?></td>
			<td>$&nbsp;<?php 
						if ($factura['Tipofactura']['id'] == 2) {
							echo number_format($detfac['cantidad']*$detfac['valorunitario'],2);
						}
						else {
							echo number_format($detfac['cantidad']*$detfac['valorsinimp'],2);
						}
						?>
			</td>
			<td>$&nbsp;<?php echo number_format($detfac['total'],2);?></td>
			<td class="actions">
				<?php echo $this->html->link(__('Ver', true), array('controller'=> 'detfacturas', 'action'=>'view', $detfac['id'])); ?>
				<?php echo $this->html->link(__('Editar', true), array('controller'=> 'detfacturas', 'action'=>'edit', $detfac['id'])); ?>
				<?php echo $this->html->link(__('Borrar', true), array('controller'=> 'detfacturas', 'action'=>'delete', $detfac['id']), null, sprintf(__('Esta seguro de borrar este detalle del documento # %s?', true), $detfac['id'])); ?>
			</td>
		</tr>
		<?php
		$vcantidad = $vcantidad + $detfac['cantidad'];
		//$vsubtotal = $vsubtotal + $detfac['subtotal'];
		//$vdescuento = $vdescuento + $detfac['descuento'];
		if ($factura['Tipofactura']['id'] == 2) 
			$vtotal = $vtotal + ($detfac['cantidad']*$detfac['valorunitario']);
		else
			$vtotal = $vtotal + ($detfac['cantidad']*$detfac['valorsinimp']);
		?>
	<?php endforeach; ?>
	<tr>
		<td>&nbsp;</td> 
		<td><b>TOTALES</b></td>
		<td><b><?php echo $vcantidad;?></b></td>
		<td>&nbsp;</td>
		<td><b>$&nbsp;<?php echo number_format($vtotal,2);?></b></td>
	</b></tr>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->html->link(__('Nuevo Detalle de Documento', true), array('controller'=> 'detfacturas', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>

