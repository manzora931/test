<?php

class RFormHelper extends AppHelper {
    public $helpers = ['Form'];

    public function input ($name, $options) {
        switch ($options['type']) {
            case 'table':
                return $this->tableInput($name, $options);
            default:
                return $this->Form->input($name, $options);
        }
    }

    private function tableInput ($name, $options) {
        $model = $options['model'];
        App::import('Model', $model);
        $model = new $model;
        $find = Hash::combine($model->find("all"),
                    '{n}.'.$options['model'].'.id',
                    '{n}.'.$options['model'].'.'.$options['field']
                );

        $options['type'] = 'select';
        $options['options'] = $find;

        $input = $this->Form->input($name, $options);

        return $input;
    }
}
