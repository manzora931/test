
<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }
    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
        text-decoration: none;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 270px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    table#tblview tr td:last-child{
        width: 400px;
        text-align: left;

    }

</style><div class="recursos view container-fluid">
	<h2 style=" text-transform: capitalize;"><?php echo __($titulo['Recurso']['nombre']); ?></h2>
	<?php
	if(isset($_SESSION['recur_save'])){
		if($_SESSION['recur_save']==1){
			unset($_SESSION['recur_save']);
			?>
			<div class="alert alert-success " id="alerta" style="display: none;">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Organización actualizada
			</div>
		<?php		}
	}
	?>

	<table id="tblview">
		<tr>
			<td><strong>Id</strong></td><td><?php echo h($organizacion['Organizacion']['id']); ?></td>
		</tr>
		<tr>
			<td><strong>Nombre de Organización</strong></td><td><?php echo h($organizacion['Organizacion']['organizacion']); ?></td>
		</tr>
		<tr>
			<td><strong>Dirección</strong></td><td><?php echo h($organizacion['Organizacion']['direccion']); ?></td>
		</tr>
		<tr>
			<td><strong>Teléfono</strong></td><td><?php echo h($organizacion['Organizacion']['telefono']); ?></td>
		</tr>
		<tr>
			<td><strong>País</strong></td><td><?php echo h($organizacion['Paise']['pais']); ?></td>
		</tr>

        <tr>
            <td><strong>Institución</strong></td><td><?php echo h($organizacion['Institucion']['nombre']); ?></td>
        </tr>
		<tr>
			<td><strong>Correo Electrónico de Notificación</strong></td><td><?php echo h($organizacion['Organizacion']['emailnotification']); ?></td>
		</tr>
		<tr>
			<td><strong>Nombre de Notificación</strong></td><td><?php echo h($organizacion['Organizacion']['namenotification']); ?></td>
		</tr>
		<tr>
			<td><strong>Correo de Soporte Técnico</strong></td><td><?php echo h($organizacion['Organizacion']['emailsoporte']); ?></td>
		</tr>
		<tr>
			<td><strong>Nombre de Soporte Técnico</strong></td><td><?php echo h($organizacion['Organizacion']['namesoporte']); ?></td>
		</tr>
		<tr>
			<td><strong>Activa</strong></td><td><?php
				$vactivo = 'No';
				if ($organizacion['Organizacion']['activa']) $vactivo = 'Si';
				?>
				<?php echo $vactivo; ?></td>
		</tr>
		<tr>
			<td><strong>Creado</strong></td><td><?php echo h($organizacion['Organizacion']['usuario']); ?>
				<?php echo "(".h($organizacion['Organizacion']['created']); echo ")"; ?></td>
		</tr>
		<tr>
			<td><strong>Modificado</strong></td><td>
				<?php
				if(isset($organizacion['Organizacion']['usuario_modif'])){
					echo h($organizacion['Organizacion']['usuario_modif']);
					echo " (" . h($organizacion['Organizacion']['modified']); echo ")";
				}
				?>
			</td>
		</tr>
	</table>
	<div class="actions">

			<div class="style-btn"><?php echo $this->Html->link(__('Modificar'), array('action' => 'edit', $organizacion['Organizacion']['id']), array('class' => 'btn btn-default')); ?></div>

	</div>
</div>

</div>
<script type="text/javascript">
	jQuery(function(){
		$("#alerta").slideDown();
		setTimeout(function () {
			$("#alerta").slideUp();
		}, 4000);
	});
</script>

