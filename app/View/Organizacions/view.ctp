<div class="organizacions view">
<h2><?php  echo __('Organizaci&oacute;n');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo $organizacion['Organizacion']['id']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Organizaci&oacute;n'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Organizacion']['organizacion']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Raz&oacute;n Social'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Organizacion']['razonsocial']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('No. Registro IVA'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Organizacion']['regiva']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Nit'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Organizacion']['nit']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Giro'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Organizacion']['giro']; ?></b>
			&nbsp;
		</dd>
		<dt><?php echo __('Categor&iacute;a de Contribuyente'); ?></dt>
		<dd>
			<b><?php echo $organizacion['Tiposocionegocio']['tiposocionegocio']; ?></b>
			&nbsp;
		</dd>		
		<dt><?php echo __('Representante'); ?></dt>
		<dd>
			<?php echo $organizacion['Organizacion']['representante']; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Direcci&oacute;n'); ?></dt>
		<dd>
			<?php echo $organizacion['Organizacion']['direccion']; ?>
			&nbsp;
		</dd>
		<?php
		if ($organizacion['Organizacion']['invnegativo']==1) $negativos = 'Si';
		else $negativos = 'No';
		?>
		<dt><?php echo __('Inventario Negativo'); ?></dt>
		<dd>
			<?php echo $negativos; ?>
			&nbsp;
		</dd>
		<?php
		if ($organizacion['Organizacion']['partidacuadrada']==1) $pCuadrada = 'Si';
		else $pCuadrada = 'No';
		?>
		<dt><?php echo __('Partida cuadrada'); ?></dt>
		<dd>
			<?php echo $pCuadrada; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pa&iacute;s'); ?></dt>
		<dd>
			<?php echo $organizacion['Paise']['pais']; ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Formula de vacaciones anuales'); ?></dt>
        <dd>
            <?php echo $organizacion['Organizacion']['vacacionanual']; ?>
            &nbsp;
        </dd>
		<?php
		if ($organizacion['Organizacion']['activa']==1) $vactivo = 'Si';
		else $vactivo = 'No';
		?>
		<dt><?php echo __('Activo'); ?></dt>
		<dd>
			<?php echo $vactivo; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->html->link(__('Editar Organización', true), array('action'=>'edit', $organizacion['Organizacion']['id'])); ?> </li>
		<li><?php echo $this->html->link(__('Listado de Organizaciones', true), array('action'=>'index')); ?> </li>
		<li><A HREF="javascript:javascript:history.go(-1)">Regresar</A></li>
	</ul>
</div>
