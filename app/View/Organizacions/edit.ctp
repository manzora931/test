<?php $real_url = 'http://'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}
</script>
<div class="organizacions form container-fluid">
	<?php echo $this->Form->create('Organizacion',array('id'=>'org')); ?>
	<fieldset>
		<legend><?php echo __('Editar ' . $titulo['Recurso']['nombre']); ?></legend>
		<?php
		echo $this->Form->input('id');
		echo $this->Form->input('organizacion', array('label'=>'Nombre de Organización','class' => "validate[required]",'required'));
		echo $this->Form->input('direccion', array('label'=>'Dirección', 'rows' => 3));
		echo $this->Form->input('telefono', array('label'=>'Teléfono'));
		echo $this->Form->input('paise_id',array('label' => 'País', 'empty'=>'Seleccionar'));
        echo $this->Form->input('institucion_id',array('label' => 'Institucion', 'empty'=>'Seleccionar'));
		echo $this->Form->input('emailnotification', array('label'=>'Correo Electrónico de Notificación','class' => "validate[required]",'required', 'rows' => 1));
		echo $this->Form->input('namenotification', array('label'=>'Nombre de Notificación','class' => "validate[required]",'required', 'rows' => 1));
		echo $this->Form->input('emailsoporte', array('label'=>'Correo de Soporte Técnico', 'rows' => 1));
		echo $this->Form->input('namesoporte', array('label'=>'Nombre de Soporte Técnico', 'rows' => 1));
		echo "<br>";
		echo  $this->Form->input('activa', array('label' => 'Activa','div'=>false,'style'=>'margin:0; margin-right:4px;'));
		?>
	</fieldset>
	<div class="actions">
		<div><?= $this->Form->button('Almacenar', [
				'label' => false,
				'type' => 'submit',
				'class' => 'btn btn-default',
				'div' => [
					'class' => 'form-group'
				]
			]); ?>
			<?php echo $this->Form->end();?></div>
	</div>
</div>

