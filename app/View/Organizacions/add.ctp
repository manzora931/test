<?php
	echo $this->Html->css('validationEngine.jquery'); 
	echo $this->Html->script('jquery.validationEngine-es'); 
	echo $this->Html->script('jquery.validationEngine');
?>

<script type="text/javascript">
	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#OrganizacionAddForm").validationEngine();
		});
</script>
<div class="organizacions form">
<?php echo $this->Form->create('Organizacion');?>
	<fieldset>
 		<legend><?php echo __('Adicionar Organizaci&oacute;n');?></legend>
	<?php
		echo $this->Form->input('organizacion',array('div' => false, 'class' => "validate[required], text-input"));
		echo $this->Form->input('regiva',array('div' => false,'label'=>'No. Registro IVA'));
		echo $this->Form->input('nit',array('div' => false));
		echo $this->Form->input('giro',array('div' => false));
		echo $this->Form->input('tiposocionegocio_id',array('class' => "validate[required], select",'div' => false,'label'=>'Categoria de Contribuyente','empty'=>'Selccionar'));
		echo $this->Form->input('representante',array('div' => false));
		echo $this->Form->input('direccion');
        echo "</br>";
		echo $this->Form->input('invnegativo',array('div' => false,"label"=>"El Inventario acepta saldos negativos"));
		echo $this->Form->input('partidacuadrada',array('div' => false,"label"=>"Contabilidad No acepta partidas descuadradas"));
		echo $this->Form->input('paise_id');
        echo $this->Form->input('vacacionanual',array('label'=>'Formula de vacaciones anuales', 'required', 'class' => "validate[required]"));
        echo "</br>";
		echo $this->Form->input('activa',array('div' => false));
	?>
	</fieldset>
<?php echo $this->Form->end('Almacenar');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Lista de Organizaciones', true), array('action'=>'index'));?></li>
		<li><A HREF="javascript:javascript:history.go(-1)">Regresar</A></li>
	</ul>
</div>
