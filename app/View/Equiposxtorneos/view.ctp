<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }

</style>
<div class="equiposxtorneos view container">
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<h2><?php echo __('Equipos por Torneos'); ?></h2>
    <?php
    if( isset( $_SESSION['equipoxtorneo_save'] ) ) {
        if( $_SESSION['equipoxtorneo_save'] == 1 ){
            unset( $_SESSION['equipoxtorneo_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Registro almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($equiposxtorneo['Equiposxtorneo']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>Torneo</td>
            <td>
                <?=$equiposxtorneo['Torneo']['nombrecorto']?>
            </td>
        </tr>
        <tr>
            <td>Equipo</td>
            <td>
                <?=$equiposxtorneo['Equipo']['nombrecorto']?>
            </td>
        </tr>
        <tr>
            <td>Creado</td>
            <td>
                <?=$equiposxtorneo['Equiposxtorneo']['usuario']." (".$equiposxtorneo['Equiposxtorneo']['created'].")"?>
            </td>
        </tr>
        <tr>
            <td>Modificado</td>
            <td>
                <?=($equiposxtorneo['Equiposxtorneo']['usuariomodif']!='')?$equiposxtorneo['Equiposxtorneo']['usuariomodif']." (".$equiposxtorneo['Equiposxtorneo']['modified'].")":""?>
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $equiposxtorneo["Equiposxtorneo"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
