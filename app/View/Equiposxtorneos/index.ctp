<style>
    h2{
        margin-top: 15px;
        color:#cb071a;
        font-size: 1.4em;
    }
    .table > thead > tr > th:first-child {
        border-top-left-radius: 5px;
    }
    .table > thead > tr > th:last-child {
        border-top-right-radius: 5px;
    }
    .table tbody tr td {
        font-size: 16px;
    }
    .searchBox table tbody td {
        font-size: 16px;
        padding: 0px 5px 0 2px;
    }
    .searchBox table tbody td:nth-child(4){
        width: ;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb;
    }
    table.table tbody{
        border:1px solid #ccc;
    }
    .checkbox {
        padding-top: 18px;
    }
    label{
        margin-left: 7px;
    }
    input#ac {
        margin-top: 5px;
    }

</style>
<div class="equiposxtorneos index container-fluid">
	<h2><?php echo __('Equipos por Torneos'); ?></h2>
	<p>
		<?php
	echo $this->paginator->counter(array(
		'format' => __('Página {:page} de {:pages},{:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
	));	?></p>
    <?php
    /*Se realizara un nuevo formato de busqueda*/
    /*Inicia formulario de busqueda*/
    $tabla = "equiposxtorneos"; ?>
    <div id='search_box' class="table-responsive">
        <?php
        echo $this->Form->create($tabla);
        echo "<input type='hidden' name='_method' value='POST' />";
        echo "<table style='width: 600px;'>";
        echo "<tr>";

        echo "<td style='width: 210px; padding-left: 4px;'>";
        $search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
        echo $this->Form->input('SearchText', array('label'=>'Buscar por: Torneo, Equipo',"type"=>"hidden", 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Torneo, Equipo'));
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][torneo_id]',
            'options'=>$torneos,
            'label'=>'Torneo',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['torneo_id']:'',
            'empty' => array( '(Seleccionar Torneo)')));
        echo "</td>";
        echo "<td style='width: 210px; padding-left: 4px;'>";
        echo $this->Form->input('',array('class'=>'form-control',
            'name'=>'data['.$tabla.'][equipo_id]',
            'options'=>$equipos,
            'label'=>'Equipo',
            'type'=>'select',
            "style"=>"width: 180px",
            'default'=>(isset($_SESSION['tabla['.$tabla.']']))?$_SESSION['tabla['.$tabla.']']['equipo_id']:'',
            'empty' => array( '(Seleccionar Equipo)')));
        echo "</td>";

        echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
        echo $this->Form->hidden('Controller', array('value'=>'Equiposxtorneo', 'name'=>'data[tabla][controller]')); //hacer cambio
        echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
        echo $this->Form->hidden('Parametro1', array('value'=>'equipo,torneo', 'name'=>'data[tabla][parametro]'));
        echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
        echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
        echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
        echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
        echo $this->Form->end();
        echo "</td>";
        echo "</tr>";
        echo "</table>";
        ?>

        <?php
        if(isset($_SESSION["$tabla"]))
        {
            $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
            echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
        }
        ?>
    </div>
	<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
	    <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('torneo_id','Torneo'); ?></th>
            <th><?php echo $this->Paginator->sort('equipo_id','Equipo'); ?></th>
            <th class="bdr"><?php echo __('Acciones'); ?></th>
        </tr>
        </thead>
        <tbody>
	<?php foreach ($equiposxtorneos as $equiposxtorneo): ?>
	<tr>
		<td><?php echo h($equiposxtorneo['Equiposxtorneo']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($equiposxtorneo['Torneo']['nombrecorto'], array('controller' => 'torneos', 'action' => 'view', $equiposxtorneo['Torneo']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($equiposxtorneo['Equipo']['nombrecorto'], array('controller' => 'equipos', 'action' => 'view', $equiposxtorneo['Equipo']['id'])); ?>
		</td>
	<td class="">
		<?php echo $this->Html->link(__(' ', true), array('action'=>'view', $equiposxtorneo['Equiposxtorneo']['id']),array('class'=>'ver')); ?>
        <?php echo $this->Html->link(__(' ', true), array('action'=>'edit', $equiposxtorneo['Equiposxtorneo']['id']),array('class'=>'editar')); ?>		</td>
<?php endforeach; ?>
        </tbody>
	</table>
    <div class="paging">
        <?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
        | 	<?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
    </div>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Nuevo Equipo'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
    </div>
</div>
