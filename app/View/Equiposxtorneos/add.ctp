<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
    .btn-file{
        margin-left: 0;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
    <div class="equiposxtorneos form container-fluid">
<?php echo $this->Form->create('Equiposxtorneo'); ?>
	<fieldset>
		<legend><?php echo __('Adicionar'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $this->Form->input('torneo_id',[
                    'label'=>'Torneo',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"valUnique(this.id,0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('equipo_id',[
                    'label'=>'Equipo',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"valUnique(this.id,1);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
        </div>
	</fieldset>
        <div class="actions">
            <div><?= $this->Form->button('Almacenar', [
                    'label' => false,
                    'type' => 'submit',
                    'class' => 'btn btn-default',
                    'div' => [
                        'class' => 'form-group'
                    ]
                ]); ?>
                <?php echo $this->Form->end();?></div>
            <div><?php echo $this->Html->link(__('Listado'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
        </div>
</div>
<script>
    function valUnique(id, type){
        var torneo = $("#EquiposxtorneoTorneoId").val();
        var equipo = $("#EquiposxtorneoEquipoId").val();
        var id = 0;
        var url = getURL()+"valUnique";
        if(torneo!==''&& equipo!==''){
            $.ajax({
                url: url,
                type: 'post',
                data: {torneo:torneo, equipo: equipo,id:id},
                cache: false,
                success: function(resp){
                    if(parseInt(resp)==1){
                        $("#EquiposxtorneoEquipoId").val("");
                        $("#msjalert").text("El equipo ya se encuentra en el torneo");
                        $("#alerta").slideDown();
                        setTimeout(function(){
                            $("#alerta").slideUp();
                        },4000);
                    }
                }
            });
        }
    }
</script>