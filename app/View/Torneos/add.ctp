<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<?= $this->Html->Script(['funciones/crearOptionSelect']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<style>
	.tb_tarea tr td input {
		margin-top: 10px;
	}
	.margen{
		margin-left: 20px;
	}
	.margenbtn{
		margin-left: 50px !important;
	}
</style>
<script type="text/javascript">
	function getURL(){
		return '<?=$real_url;?>';
	}

	jQuery(function() {
		$('.datepicker').datepicker({
			dateFormat: "dd-mm-yy",
			changeMonth: true,
			changeYear: true,

		});
		$.datepicker.regional["es"];

		var desde = $("#TorneoDesde");
		var hasta = $("#TorneoHasta");

		hasta.datepicker("option", "minDate", $(desde).datepicker("getDate"));
		desde.datepicker("option", "maxDate", $(hasta).datepicker("getDate"));

		desde.on("change", function() {
			hasta.datepicker("option", "minDate", $(this).datepicker("getDate"));
		});

		hasta.on("change", function() {
			desde.datepicker("option", "maxDate", $(this).datepicker("getDate"));
		});
	});
</script>
<div class="tareas form container-fluid">
	<?php echo $this->Form->create('Torneo',array('id'=>'formulario')); ?>
	<fieldset>
		<legend><?php echo __('Adicionar Torneo'); ?></legend>
		<div class="alert alert-danger" id="alerta" style="display: none">
			<span class="icon icon-check-circled" id="msjalert"></span>
			<button type="button" class="close" data-dismiss="alert"></button>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<?= $this->Form->input('paise_id',[
					'label'=>'País',
					'class'=>'form-control validate[required]',
					'onchange'=>"load_inter(this.id,0);",
					'empty'=>"Seleccionar",
					'required'=>true,
					'div'=>['class'=>"form-groupp"],

				]);?>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-3">
				<?= $this->Form->input('tipotorneo_id',[
					'label'=>'Tipo de Torneo',
					'class'=>'form-control validate[required]',
					'onchange'=>"load_act(this.id, 0, 0);",
					'empty'=>"Seleccionar",
					'required'=>true,
					'div'=>['class'=>"form-groupp"],

				]);?>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<?= $this->Form->input('torneo',[
					'label'=>'Nombre',
					'required'=>true,
					'placeholder'=>'Nombre',
					'class'=>'form-control validate[required]',
					'div'=>['class'=>'form-groupp'],
					'maxlength'=>300
				]); ?>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<?= $this->Form->input('nombrecorto',[
					'label'=>'Nombre Corto',
					'required'=>true,
					'placeholder'=>'Nombre Corto',
					'class'=>'form-control validate[required]',
					'div'=>['class'=>'form-groupp'],
					'maxlength'=>150
				]); ?>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				<br>
				<div class="col-md-12 line-space">
					<div class="col-md-2"><label class="lblproy">Duración</label></div>
					<div class="col-md-5"><?= $this->Form->input("desde",[
							'type'=>'text',
							'class'=>"form-control datepicker",
							'required'=>true,
							'label'=>"",
							'onChange'=>'RangoFecha();',
							'readonly'

						]);?>
					</div>
					<div class="col-md-5">
						<?= $this->Form->input("hasta",[
							'type'=>'text',
							'class'=>"form-control datepicker",
							'required'=>true,
							'label'=>"",
							'onChange'=>'RangoFecha();',
							'readonly'
						]);?>
					</div>
				</div>
			</div>
            <div class="clearfix"></div>
            <br>
            <div class="col-sm-6">
                <?= $this->Form->input('mundial',[
                    'label'=>'Mundial',
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                    "type"=>"checkbox",
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('grupos',[
                    'label'=>'Fase de Grupos',
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                    "type"=>"checkbox",
                    "onClick"=>"SetFaseGrupos()"
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('activo',[
                    'label'=>'Activo',
                    "type"=>"checkbox",
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],

                ]); ?>
            </div>
			<div class="clearfix"></div>
			<div class="col-sm-12">
				<?= $this->Form->input('informacion',['rows'=>3, 'label'=>"Información", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"Información", 'style'=>'margin-bottom: 15px']); ?>
			</div>


		</div>

        <div class="row">
            <div class="col-sm-12" style="margin-top: 25px;">
                <h3>Grupos</h3>
            </div>
            <div class="alert alert-danger" id="alertaDet" style="display: none">
                <span class="icon icon-check-circled"></span>
                <button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <table style="margin-left: 15px;" class="tb_tarea">
                <?php
                for($i=0;$i<1;$i++){	?>
                    <tr class="line<?=$i?>">
                        <td>
                            <?= $this->Form->input('grupo_id['.$i . ']',[
                                'label'=>false,
                                'id'=>"grupo_id".$i,
                                'class'=>'form-control nombret',
                                'onChange'=>"validSelected(this.id, this.value)",
                                'type'=>'select',
                                'empty'=>"Seleccionar",
                                'options'=>$grupos,
                                'name'=>'data[Grupoxtorneo][grupo_id][0]',
                                'div'=>['class'=>"form-groupp"]
                            ]);?>
                        </td>
                        <td>
                            <?= $this->Form->input('orden['.$i . ']',[
                                'label'=>false,
                                'id'=>"orden".$i,
                                'placeholder'=>'Orden',
                                'style'=>'margin-top: 0',
                                'class'=>'form-control margen order',
                                'onChange'=>"validSelectedOrder(this.id, this.value)",
                                'name'=>'data[Grupoxtorneo][orden][0]',
                                'div'=>['class'=>'form-groupp']
                            ]); ?>
                        </td>
                        <td style='width: 200px;'>

                        </td>
                    </tr>
                <?php				}

                ?>
            </table><br>
            <table style="margin-left: 15px;">
                <tr>
                    <td colspan="2"><button id="addDet" onclick="newdet();" class="btn btn-default btn-add-row btn-search" type="button"><span><i class="fa fa-plus icon-search"></i></span></button></td>
                </tr>
            </table>
            <div class="col-sm-3"></div>
            <div class="col-sm-7"></div>
        </div>
	</fieldset>
	<div class="actions">
		<div><?= $this->Form->button('Almacenar', [
				'label' => false,
				'type' => 'submit',
				'class' => 'btn btn-default',
				'div' => [
					'class' => 'form-group'
				]
			]); ?>
			<?php echo $this->Form->end();?></div>
		<div><?php echo $this->Html->link(__('Listado de Torneos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
	</div>
</div>
<script>
    var grupo_nombre=new Array();
    var grupo_id=new Array();
    var cont=0;
    <?php foreach ($grupos as $key => $value): ?>
    grupo_id.push(<?=$key?>);
    grupo_nombre.push('<?=$value?>');
    <?php endforeach; ?>
	function RangoFecha(){
		var desde = $("#desde").val();
		var hasta = $("#hasta").val();
		if(desde !== '' && hasta!==''){
			var cad1 = desde.split("-");
			var cad2 = hasta.split("-");
			var dateStart=new Date(cad1[2]+"/"+(cad1[1])+"/"+cad1[0]+" 00:00");
			var dateEnd=new Date(cad2[2]+"/"+(cad2[1])+"/"+cad2[0]+" 00:00");
			if(dateStart > dateEnd){
				$("#hasta").val("");
				$("#msjalert").text("La fecha hasta en la duración del torneo no puede ser menor a la fecha desde.");
				$("#alerta").slideDown();
				setTimeout(function () {
					$("#alerta").slideUp();
				}, 4000);
			}
		}
	}
    function deleteT(num){
        $(".line"+num).remove();
    }
    function newdet(){
        cont++;
        var styleLine=" style='margin-top:10px;'";
        $(".tb_tarea").append("<tr class='line"+cont+"'><td><div class='form-groupp'"+styleLine+">" +
            "<select name='data[Grupoxtorneo][grupo_id]["+cont+"]' id='grupo_id"+cont+"' class='form-control nombret' onChange='validSelected(this.id, this.value)'></select>" +
            "</div></td>" +
            "<td><div class='form-groupp'"+styleLine+">" +
            "<input name='data[Grupoxtorneo][orden]["+cont+"]' id='orden"+cont+"' placeholder='Orden' style='margin-top: 0' onChange='validSelectedOrder(this.id, this.value)' class='form-control margen order' type='number'>" +
            "</div></td>" +
            "<td style='width: 200px;'>"+
            "<button"+styleLine+" data-corr='0' onclick='deleteT("+cont+");' class='btn btn-default btn-remove-row btn-search margenbtn' type='button'><span><i class='fa fa-trash icon-search'></i></span></button>"+
            "</td>" +
            "</tr>");
        crearOptionSelect('Seleccionar','','grupo_id'+cont);
        for (var i = 0; i < grupo_id.length; i++) {
            crearOptionSelect(grupo_nombre[i],grupo_id[i],'grupo_id'+cont);
        }
    }

    function validSelected(id, value){
        var selected = value;
        var rows = $(".nombret").length;
        var error = false;
        for(var x=0; x < rows; x++){
            if(id !== "grupo_id"+x){
                var item = $("#grupo_id"+x).val();
                if(item == selected){
                    error = true;
                }
            }
        }
        if(error){
            $("#"+id).val("");
            $("#alertaDet span").text("El grupo ya fue seleccionado.");
            $("#alertaDet").slideDown();
            setTimeout(function(){
                $("#alertaDet").slideUp();
            }, 4000);
        }
    }

    function validSelectedOrder(id, value){
        var selected = value;
        var rows = $(".order").length;
        var error = false;
        for(var x=0; x < rows; x++){
            if(id !== "orden"+x){
                var item = $("#orden"+x).val();
                if(item == selected){
                    error = true;
                }
            }
        }
        if(error){
            $("#"+id).val("");
            $("#alertaDet span").text("Los grupos deben tener orden diferentes.");
            $("#alertaDet").slideDown();
            setTimeout(function(){
                $("#alertaDet").slideUp();
            }, 4000);
        }
    }

    function SetFaseGrupos(){
        if($("#TorneoGrupos").prop("checked")){
            $(".nombret").addClass("validate[required]");
            $(".nombret").prop("required", true);
        }else{
            $(".nombret").removeClass("validate[required]");
            $(".nombret").prop("required", false);
        }
    }

</script>