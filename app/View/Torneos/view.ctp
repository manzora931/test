<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<div class="tareas view container">
<h2><?php echo __('Torneo'); ?></h2>
    <?php
    if( isset( $_SESSION['torneo_save'] ) ) {
        if( $_SESSION['torneo_save'] == 1 ){
            unset( $_SESSION['torneo_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Torneo almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($torneo['Torneo']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>País</td>
            <td>
                <?=$paises[$torneo['Torneo']['paise_id']]?>
            </td>
        </tr>
        <tr>
            <td>Tipo de Torneo</td>
            <td>
                <?=$tipotorneos[$torneo['Torneo']['tipotorneo_id']]?>
            </td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td><?=$torneo['Torneo']['torneo']?></td>
        </tr>
        <tr>
            <td>Nombre corto</td>
            <td><?=$torneo['Torneo']['nombrecorto']?></td>
        </tr>
        <tr>
            <td>Desde</td>
            <td><?=$torneo['Torneo']['desde']?></td>
        </tr>
        <tr>
            <td>Hasta</td>
            <td><?=$torneo['Torneo']['hasta']?></td>
        </tr>
        <tr>
            <td>Mundial</td>
            <td><?=($torneo["Torneo"]["mundial"]==1)?"Sí":"No";?></td>
        </tr>
        <tr>
            <td>Fase de Grupos</td>
            <td><?=($torneo["Torneo"]["grupos"]==1)?"Sí":"No";?></td>
        </tr>
        <tr>
            <td>Activo</td>
            <td><?=($torneo["Torneo"]["activo"]==1)?"Sí":"No";?></td>
        </tr>
        <tr>
            <td><?php echo __('Información'); ?></td>
            <td>
                <?php echo h($torneo['Torneo']['informacion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $torneo['Torneo']['usuario']." (".$torneo['Torneo']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($torneo['Torneo']['usuariomodif']!='')?$torneo['Torneo']['usuariomodif']." (".$torneo['Torneo']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Torneo'), array('action' => 'edit', $torneo["Torneo"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Torneos'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
    <div class="tblvr">
        <?php if (!empty($tarea['Subtarea'])): ?>
            <h2><?php echo __('Sub tareas'); ?></h2><br>
            <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th width="7px"><?php echo __('Id'); ?></th>
                    <th width="25%"><?php echo __('Nombre'); ?></th>
                    <th><?php echo __('Descripción'); ?></th>
                </tr>
                </thead>
                <?php foreach ($tarea['Subtarea'] as $subtarea): ?>
                    <tr>
                        <td><?php echo $subtarea['id']; ?></td>
                        <td class="text-left"><?php echo $subtarea['nombre']; ?></td>
                        <td class="text-left"><?php echo $subtarea['descripcion']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
    <div class="tblvr" style="width: 30%;">
        <?php if (count($gruposxtorneo)>0): ?>
            <h2><?php echo __('Grupos'); ?></h2><br>
            <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th width="25%"><?= "Grupo" ?></th>
                    <th><?= "Orden" ?></th>
                </tr>
                </thead>
                <?php foreach ($gruposxtorneo as $item): ?>
                    <tr>
                        <td class="text-left"><?= $grupolist[$item["Gruposxtorneo"]['grupo_id']]; ?></td>
                        <td class="text-center"><?= $item["Gruposxtorneo"]['orden']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
