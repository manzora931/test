<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
	<h2><?php echo __('Torneos'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "torneos";
	?>
	<div id='search_box' class="table-responsive">
		<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Nombre', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre'));
		echo "</td>";

		/*echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('module_id',array('label'=>'Módulo:', 'onchange'=>"loadInt(this.id);", 'name'=> 'data['.$tabla.'][module_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('intervencione_id',array('label'=>'Intervención:', 'name'=> 'data['.$tabla.'][intervencione_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";

		echo "<td width='20%' style='padding-left: 10px'>";
		echo $this->Form->input('actividade_id',array('label'=>'Actividad:', 'name'=> 'data['.$tabla.'][actividade_id]', 'style' => 'width: 100%', 'empty' => array(0 => '-- Seleccionar --')));
		echo "</td>";*/

		echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px''>";
		echo $this->Form->hidden('Controller', array('value'=>'Torneo', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'torneo', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>

		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
		}
		?>
	</div>
	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombrecorto', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('paise_id', 'País'); ?></th>
			<th><?php echo $this->Paginator->sort('desde', 'Desde'); ?></th>
			<th><?php echo $this->Paginator->sort('hasta', 'Hasta'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($torneos as $torneo):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?= h($torneo['Torneo']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($torneo['Torneo']['torneo']); ?>&nbsp;</td>
				<td style='text-align:center'><?= $paises[$torneo['Torneo']['paise_id']]; ?></td>
				<td style='text-align:center'><?= explode('-', $torneo['Torneo']['desde'])[2] . '-' . explode('-', $torneo['Torneo']['desde'])[1] . '-' . explode('-', $torneo['Torneo']['desde'])[0]; ?></td>
				<td style='text-align:center'><?= explode('-', $torneo['Torneo']['hasta'])[2] . '-' . explode('-', $torneo['Torneo']['hasta'])[1] . '-' . explode('-', $torneo['Torneo']['hasta'])[0]; ?></td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $torneo['Torneo']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $torneo['Torneo']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>
	<div class="actions">
		<div><?php echo $this->Html->link(__('Nuevo Torneo'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
	</div>
</div>