<?php
echo $this->Html->css(["public.default"]);
echo $this->Html->Script(["funciones/validate.domain"]);
if($host!=null){
    echo $this->Html->css(["plantillas/".$host.'.css']);
}
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
$real_url_base = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/';
?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
    function getURLBase(){
        return '<?=$real_url_base;?>';
    }
</script>
<?php  if($host==null){   ?>
    <script>
        var ruta = document.referrer;
        var datos = ruta.split("//");
        var datos = datos[1].split("/");
        var url = getURL()+"index/<?=$torneo_id."/".$number ?>/"+datos[0];
        location.href=url;
    </script>
<?php   }
if($valid == 0){  ?>
    <!--Sin privilegio-->
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="alert alert-danger" id="alertaP">
            <span class="icon icon-check-circled"></span>
            <strong>Sin resultados que mostrar</strong>
        </div>
    </div>
    <script>
        $("#alertaP").slideDown();
    </script>
<?php   }else{   ?>
<div class="container-fluid content-posiciones">
    <?php if(!$error){  ?>
        <div class="col-md-12 col-lg-12 col-xs-12 content-title">
            <span><?= strtoupper($torneo["Torneo"]["torneo"])?></span>
        </div>
        <div class="col-md-12 col-lg-12 col-xs-12 content-tab">
            <table class="table table-posiciones">
                <thead>
                <tr>
                    <th>POS</th>
                    <th>EQUIPO</th>
                    <th>PG</th>
                    <th>PE</th>
                    <th>PP</th>
                    <th>PTS</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i=1;
                foreach ($tabposisions as $row){    ?>
                    <tr>
                        <td class="text-center"><strong><?=$i?></strong></td>
                        <td>
                            <img class="logoEq" src="../../../../<?=$row["x"]["fotoescudo"]?>">
                            <?=$row["x"]["nombrecorto"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoganado"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoempatado"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoperdido"]?></td>
                        <td class="text-center"><?=$row[0]["puntos"]?></td>
                    </tr>
                    <?php           $i++;
                }            ?>
                </tbody>
            </table>
        </div>
        <!-- Ultimos resultados--->
        <div class="col-md-12 col-lg-12 col-xs-12 content-title text-center">
            <span>ÚLTIMOS RESULTADOS</span><br>
            <span><?= strtoupper($torneo["Torneo"]["torneo"])?></span>
        </div>
        <div class="col-md-12 col-lg-12 col-xs-12 content-tab">
            <div class="col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 col-xs-10">
                <table class="table table-posiciones">
                    <tbody>
                    <?php
                    $i=1;
                    foreach ($resultados as $row){    ?>
                        <tr>

                            <td>
                                <strong><?=$row["Equipo1"]["nombrecorto"]?></strong>
                            </td>
                            <td>
                                <img class="logoEq" src="../../../../<?=$row["Equipo1"]["fotoescudo"]?>">
                            </td>
                            <td>
                                <strong><?=$row["Juego"]["marcadore1"];?></strong>
                            </td>
                            <td><strong>-</strong></td>
                            <td>
                                <strong><?=$row["Juego"]["marcadore2"];?></strong>
                            </td>
                            <td>
                                <img class="logoEq" src="../../../../<?=$row["Equipo2"]["fotoescudo"]?>">
                            </td>
                            <td>
                                <strong><?=$row["Equipo2"]["nombrecorto"]?></strong>
                            </td>
                        </tr>
                        <?php           $i++;
                    }            ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php if(!$error){  ?>
            <div class="col-md-12 col-lg-12 col-xs-12 content-title text-center">
                <span>PRÓXIMOS PARTIDOS</span><br>
                <span><?= strtoupper($torneo["Torneo"]["torneo"])?></span>
            </div>
            <div class="col-md-12 col-lg-12 col-xs-12 content-tab">
                <div class="col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 col-xs-10">
                    <?php  if (count($proximos)>0): ?>
                        <table class="table table-posiciones">
                            <tbody>
                            <?php
                            $i=1;
                            foreach ($proximos as $row){    ?>
                                <tr>
                                    <td class="colum-paddin-butto-0">
                                        <strong><?=$row["Equipo1"]["nombrecorto"]?></strong>
                                    </td>
                                    <td class="colum-paddin-butto-0">
                                        <img class="logoEq" src="../../../../<?=$row["Equipo1"]["fotoescudo"]?>">
                                    </td>
                                    <td class="colum-paddin-butto-0">
                                        <strong><?=$row["Juego"]["marcadore1"];?></strong>
                                    </td>
                                    <td class="colum-paddin-butto-0"><strong>VS</strong></td>
                                    <td class="colum-paddin-butto-0">
                                        <strong><?=$row["Juego"]["marcadore2"];?></strong>
                                    </td>
                                    <td class="colum-paddin-butto-0">
                                        <img class="logoEq" src="../../../../<?=$row["Equipo2"]["fotoescudo"]?>">
                                    </td>
                                    <td class="colum-paddin-butto-0">
                                        <strong><?=$row["Equipo2"]["nombrecorto"]?></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="text-center border-no colum-paddin-0">
                                        <span class="lbl-proximos"><?= date("d/m/Y",strtotime($row["Juego"]["fecha"]))." ".$row["Estadio"]["estadio"];?></span>
                                    </td>
                                </tr>
                                <?php           $i++;
                            }            ?>
                            </tbody>
                        </table>
                    <?php   else: ?>
                        <br>
                        <br>
                        <div class="alert alert-danger" id="alertaP">
                            <span class="icon icon-check-circled"></span>
                            <strong>No hay partidos programados en el torneo</strong>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php }else{ ?>
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="alert alert-danger" id="alertaP">
                    <span class="icon icon-check-circled"></span>
                    <strong>Sin resultados que mostrar</strong>
                </div>
            </div>
        <?php } ?>
    <!-----Botones------->
        <div class="col-md-12 col-lg-12 col-xs-12 text-center seccion-btn">
            <button id="btnEstadistica" onclick="estadisticas();" class="btn btn-primary btn-redirec">Estadísticas</button>
            <button id="btnJuegos"  onclick="listJuegos();" class="btn btn-primary btn-redirec">Todos los Juegos</button>
        </div>
    <?php }else{ ?>
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="alert alert-danger" id="alertaP">
                <span class="icon icon-check-circled"></span>
                <strong>Sin resultados que mostrar</strong>
            </div>
        </div>
    <?php } ?>
    <input type="hidden" id="torneo_id" value="<?=$torneo_id?>">
</div>
<script>
    function listJuegos() {
        var torneo_id = $("#torneo_id").val();
        location.href = getURLBase()+"juegos/listjuegos/"+torneo_id+"/<?=$host?>";
    }
    function estadisticas() {
        var torneo_id = $("#torneo_id").val();
        location.href = getURLBase()+"informes/indexpublic/"+torneo_id+"/<?=$host?>";
    }
</script>
<?php   }    ?>