<?php
echo $this->Html->css(["public.default"]);
echo $this->Html->Script(["funciones/validate.domain"]);
if($host!=null){
    echo $this->Html->css(["plantillas/".$host.'.css']);
}
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';

?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<?php  if($host==null){   ?>
<script>
    var ruta = document.referrer;
    var datos = ruta.split("//");
    var datos = datos[1].split("/");
    var url = getURL()+"posiciones/<?=$torneo_id?>/<?=$numposiciones;?>/"+datos[0];
    location.href=url;
</script>
<?php   }
if($valid == 0){  ?>
<!--Sin privilegio-->
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="alert alert-danger" id="alertaP">
            <span class="icon icon-check-circled"></span>
            <strong>Sin resultados que mostrar</strong>
        </div>
    </div>
    <script>
        $("#alertaP").slideDown();
    </script>
<?php   }else{   ?>
<div class="container-fluid content-posiciones">
    <?php if(!$error){  ?>
    <div class="col-md-12 col-lg-12 col-xs-12 content-title">
        <span><?= strtoupper($torneo["Torneo"]["torneo"])?></span>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 content-tab">
        <table class="table table-posiciones">
            <thead>
            <tr>
                <th>POS</th>
                <th>EQUIPO</th>
                <th>PG</th>
                <th>PE</th>
                <th>PP</th>
                <th>PTS</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i=1;
                foreach ($tabposisions as $row){    ?>
                    <tr>
                        <td class="text-center"><strong><?=$i?></strong></td>
                        <td>
                            <img class="logoEq" src="../../../../<?=$row["x"]["fotoescudo"]?>">
                            <?=$row["x"]["nombrecorto"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoganado"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoempatado"]?></td>
                        <td class="text-center"><?=$row["x"]["partidoperdido"]?></td>
                        <td class="text-center"><?=$row[0]["puntos"]?></td>
                    </tr>
<?php           $i++;
                }            ?>
            </tbody>
        </table>
    </div>
    <?php }else{ ?>
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="alert alert-danger" id="alertaP">
                <span class="icon icon-check-circled"></span>
                <strong>Sin resultados que mostrar</strong>
            </div>
        </div>
    <?php } ?>
</div>
<?php } ?>
