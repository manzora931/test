<?php
echo $this->Html->css(["public.default"]);
echo $this->Html->Script(["funciones/validate.domain"]);
if($host!=null){
    echo $this->Html->css(["plantillas/".$host.'.css']);
}
$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';?>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }
</script>
<?php  if($host==null){   ?>
    <script>
        var ruta = document.referrer;
        var datos = ruta.split("//");
        var datos = datos[1].split("/");
        var url = getURL()+"resultados/<?=$torneo_id?>/<?=$numposiciones;?>/"+datos[0];
        location.href=url;
    </script>
<?php   }
if($valid == 0){  ?>
    <!--Sin privilegio-->
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="alert alert-danger" id="alertaP">
            <span class="icon icon-check-circled"></span>
            <strong>Sin resultados que mostrar</strong>
        </div>
    </div>
    <script>
        $("#alertaP").slideDown();
    </script>
<?php   }else{   ?>
<div class="container-fluid content-posiciones">
    <?php if(!$error){  ?>
        <div class="col-md-12 col-lg-12 col-xs-12 content-title text-center">
            <span>ÚLTIMOS RESULTADOS</span><br>
            <span><?= strtoupper($torneo["Torneo"]["torneo"])?></span>
        </div>
        <div class="col-md-12 col-lg-12 col-xs-12 content-tab">
            <div class="col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 col-xs-10">
                <table class="table table-posiciones">
                    <tbody>
                    <?php
                    $i=1;
                    foreach ($resultados as $row){    ?>
                        <tr>

                            <td>
                                <strong><?=$row["Equipo1"]["nombrecorto"]?></strong>
                            </td>
                            <td>
                                <img class="logoEq" src="../../../../<?=$row["Equipo1"]["fotoescudo"]?>">
                            </td>
                            <td>
                                <strong><?=$row["Juego"]["marcadore1"];?></strong>
                            </td>
                            <td><strong>-</strong></td>
                            <td>
                                <strong><?=$row["Juego"]["marcadore2"];?></strong>
                            </td>
                            <td>
                                <img class="logoEq" src="../../../../<?=$row["Equipo2"]["fotoescudo"]?>">
                            </td>
                            <td>
                                <strong><?=$row["Equipo2"]["nombrecorto"]?></strong>
                            </td>


                        </tr>
                        <?php           $i++;
                    }            ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php }else{ ?>
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="alert alert-danger" id="alertaP">
                <span class="icon icon-check-circled"></span>
                <strong>Sin resultados que mostrar</strong>
            </div>
        </div>
    <?php } ?>
</div>
<?php  } ?>