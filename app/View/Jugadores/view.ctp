<style>
    .btn-default{
        font-family: 'Calibri', sans-serif;
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border: 1px solid #606060	;
        font-size: 14px;
        color: #000;
        border-radius: 1px;

    }
    .btn-default:hover{
        margin-left: 15px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        color: #000;
        border-radius: 1px;
    }
    div.view{
        margin-top:15px;
        width: 100%;
    }
    table#tblview tr td:first-child{
        width: 200px;
        text-align: left;
        font-weight: bold;
    }
    h2{
        color:#cb071a;
        font-size: 1.4em;
    }

    div.actions{
        display: inline-block;
    }
    div.actions div > a{
        padding: 0 5px 0 5px;
    }

    div.actions>div{
        display: inline-block;
    }
    table#tblview tr td:first-child {
        width: 200px;
        text-align: left;
        font-weight: bold;
        color: #011880;
    }
    div#tblvr{
        margin:0 15px 0 15px;
    }

    .table > thead > tr > th:first-child{
        border-top-left-radius: 5px;{}
    }
    .table > thead > tr > th:last-child{
        border-top-right-radius: 5px;{}
    }
    .table > tbody{
        border: 1px solid #c0c0c0;
    }
    table.table-striped>tbody>tr:nth-child(odd)>td{
        background-color: #ebebeb ;
    }
</style>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<div class="tareas view container">
<h2><?php echo __('Jugador'); ?></h2>
    <?php
    if( isset( $_SESSION['jugadore_save'] ) ) {
        if( $_SESSION['jugadore_save'] == 1 ){
            unset( $_SESSION['jugadore_save'] );
            ?>
            <div class="alert alert-success " id="alerta" style="display: none;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Jugador almacenado
            </div>
        <?php }
    } ?>
    <table id="tblview">
        <tr>
            <td scope="row"><?php echo __('Id'); ?></td>
            <td>
                <?php echo h($jugadore['Jugadore']['id']); ?>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>País</td>
            <td>
                <?=$paises[$jugadore['Jugadore']['paise_id']]?>
            </td>
        </tr>
        <tr>
            <td>Nacionalidad</td>
            <td>
                <?= ($jugadore['Jugadore']['nacionalidad_id']!= '' && $jugadore['Jugadore']['nacionalidad_id']!= 0) ? $paises[$jugadore['Jugadore']['nacionalidad_id']] : "";?>
            </td>
        </tr>
        <tr>
            <td>Posición</td>
            <td>
                <?=$posiciones[$jugadore['Jugadore']['posicione_id']]?>
            </td>
        </tr>
        <tr>
            <td>Nombre</td>
            <td><?=$jugadore['Jugadore']['nombre']?></td>
        </tr>
        <tr>
            <td>Apellido</td>
            <td><?=$jugadore['Jugadore']['apellido']?></td>
        </tr>
        <tr>
            <td>Apodo</td>
            <td><?=$jugadore['Jugadore']['apodo']?></td>
        </tr>
        <tr>
            <td>Foto</td>
            <td>
                <?php   if($jugadore['Jugadore']['foto']!=''){  ?>
                <a href="javascript:window.open('<?=$real_url?>fotoOrig/<?=$jugadore['Jugadore']['id']?>','popup','width=700,height=500')" ><img class="fotoJugador" src="../../<?=$jugadore['Jugadore']['foto']?>"></a></td>
                <?php } ?>
        </tr>
        <tr>
            <td>Peso</td>
            <td><?=$jugadore['Jugadore']['peso']?></td>
        </tr>
        <tr>
            <td>Gènero</td>
            <td>
                <?php
                $generos = ["F"=>"Femenino", "M"=> "Masculino"];
                echo $generos[$jugadore['Jugadore']['genero']];
                ?>
            </td>
        </tr>
        <tr>
            <td>Estatura</td>
            <td><?=$jugadore['Jugadore']['estatura']?></td>
        </tr>
        <tr>
            <td>Manager</td>
            <td><?php
                if($jugadore['Jugadore']['persona_id'] != '' && $jugadore['Jugadore']['persona_id'] != 0)
                    echo $this->Html->link($personas[$jugadore['Jugadore']['persona_id']], ["controller"=>"personas" ,"action"=>"view", $jugadore['Jugadore']['persona_id']]);
                ?>
            </td>
        </tr>
        <tr>
            <td>Fecha de Nacimiento</td>
            <td><?=$jugadore['Jugadore']['fechanacimiento']?></td>
        </tr>
        <tr>
            <td><?php echo __('Información'); ?></td>
            <td>
                <?php echo h($jugadore['Jugadore']['informacion']); ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Creado'); ?></td>
            <td>
                <?php echo $jugadore['Jugadore']['usuario']." (".$jugadore['Jugadore']['created'].")"; ?>&nbsp;
            </td>
        </tr>
        <tr>
            <td><?php echo __('Modificado'); ?></td>
            <td>
                <?= ($jugadore['Jugadore']['usuariomodif']!='')?$jugadore['Jugadore']['usuariomodif']." (".$jugadore['Jugadore']['modified'].")":""; ?>&nbsp;
            </td>
        </tr>
    </table>
    <div class="actions">
        <div><?php echo $this->Html->link(__('Editar Jugador'), array('action' => 'edit', $jugadore["Jugadore"]["id"]),array('class'=>'btn btn-default')); ?></div>
        <div><?php echo $this->Html->link(__('Listado de Jugadores'), array('action' => 'index'),array('class'=>'btn btn-default')); ?></div>
    </div>
    <div class="tblvr">

        <?php if (count($estadisticas)>0): ?>
            <h2><?php echo __('Estadística'); ?></h2><br>
            <table style="width: 40%;" cellpadding="0" cellspacing="0" class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th><?php echo __('Torneo'); ?></th>
                    <th><?php echo __('Equipo'); ?></th>
                    <th><?php echo __('Evento'); ?></th>
                    <th><?php echo __('Total'); ?></th>

                </tr>
                </thead>
                <?php foreach ($estadisticas as $row): ?>
                    <tr>
                        <td class="text-left"><?= $row["Vgeneraljugador"]["torneo"]; ?></td>
                        <td class="text-left"><?= $row['Vgeneraljugador']["equipo"]; ?></td>
                        <td class="text-left"><?= $row['Vgeneraljugador']["evento"]; ?></td>
                        <td class="text-right"><?= $row[0]["total"]; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $("#alerta").slideDown();
        setTimeout(function () {
            $("#alerta").slideUp();
        }, 4000);
    });
</script>
