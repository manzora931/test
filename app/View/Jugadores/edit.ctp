<?= $this->Html->css(['//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','main']);?>
<?php $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/'; ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js') ?>
<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
<?= $this->Html->script('datepicker-es') ?>
<style>
    .tb_tarea tr td input {
        margin-top: 10px;
    }
    .margen{
        margin-left: 20px;
    }
    .margenbtn{
        margin-left: 50px !important;
    }
    .btn-file{
        margin-left: 0;
    }
</style>
<script type="text/javascript">
    function getURL(){
        return '<?=$real_url;?>';
    }

    jQuery(function() {
        $('.datepicker').datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:2020'
        });
        $.datepicker.regional["es"];
    });
</script>
<div class="tareas form container-fluid">
    <?php echo $this->Form->create('Jugadore',array('id'=>'tarea','enctype'=>"multipart/form-data")); ?>
    <fieldset>
        <legend><?php echo __('Actualizar Jugador'); ?></legend>
        <div class="alert alert-danger" id="alerta" style="display: none">
            <span class="icon icon-check-circled" id="msjalert"></span>
            <button type="button" class="close" data-dismiss="alert"></button>
        </div>
        <div class="row">
            <?php echo $this->Form->input('id'); ?>
            <div class="col-sm-3">
                <?= $this->Form->input('paise_id',[
                    'label'=>'País',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"load_inter(this.id,0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('nacionalidad_id',[
                    'label'=>'Nacionalidad',
                    'class'=>'form-control',
                    'empty'=>"Seleccionar",
                    'div'=>['class'=>"form-groupp"],
                    "options"=>$paises,
                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-3">
                <?= $this->Form->input('posicione_id',[
                    'label'=>'Posición',
                    'class'=>'form-control validate[required]',
                    'onchange'=>"load_act(this.id, 0, 0);",
                    'empty'=>"Seleccionar",
                    'required'=>true,
                    'div'=>['class'=>"form-groupp"],

                ]);?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('nombre',[
                    'label'=>'Nombre',
                    'required'=>true,
                    'placeholder'=>'Nombre',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>300
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('apellido',[
                    'label'=>'Apellido',
                    'required'=>true,
                    'placeholder'=>'Apellido',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>150
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('apodo',[
                    'label'=>'Apodo',
                    'required'=>false,
                    'placeholder'=>'Apodo',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>150
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?php
                $generos = ["F"=>"Femenino", "M"=> "Masculino"];
                echo $this->Form->input('genero',[
                    'label'=>'Género',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'options'=>$generos,
                    "empty"=> "Seleccionar",
                    "required"=>true
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('peso',[
                    'label'=>'Peso',
                    'placeholder'=>'Peso',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>150
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('estatura',[
                    'label'=>'Estatura',
                    'placeholder'=>'Estatura',
                    'class'=>'form-control validate[required]',
                    'div'=>['class'=>'form-groupp'],
                    'maxlength'=>150
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('persona_id',[
                    'label'=>'Manager',
                    'empty'=>'Seleccionar',
                    'class'=>'form-control',
                    'div'=>['class'=>'form-groupp'],
                    "options" => $personas
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <br>
                <div class="col-md-12 line-space">
                    <div class="col-md-2"><label class="lblproy">Fecha de Nacimiento</label></div>
                    <div class="col-md-5"><?= $this->Form->input("fechanacimiento",[
                            'type'=>'text',
                            'class'=>"form-control datepicker",
                            'required'=>true,
                            'label'=>"",
                            'value' => ($this->data['Jugadore']['fechanacimiento']!='')?explode('-', $this->data['Jugadore']['fechanacimiento'])[2] . '-' . explode('-', $this->data['Jugadore']['fechanacimiento'])[1] . '-' . explode('-', $this->data['Jugadore']['fechanacimiento'])[0]:"",
                            'readonly'

                        ]);?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
                <?= $this->Form->input('img',[
                    'type'=>'file',
                    'label'=>'Foto',
                    'class'=>'btn btn-default btn-file',
                    'div'=>['class'=>'form-group']
                ]); ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <?= $this->Form->input('informacion',['rows'=>3, 'label'=>"Información", 'div'=>['class'=>"form-groupp"], 'placeholder'=>"Información", 'style'=>'margin-bottom: 15px']); ?>
            </div>
        </div>
    </fieldset>
    <div class="actions">

        <div><?= $this->Form->button('Almacenar', [
                'label' => false,
                'type' => 'submit',
                'class' => 'btn btn-default',
                'div' => [
                    'class' => 'form-group'
                ]
            ]); ?>
            <?php echo $this->Form->end();?></div>
        <div><?php echo $this->Html->link(__('Listado de Jugadores'), array('action' => 'index'),array('class'=>'btn btn-default')); ?> </div>
    </div>
</div>
<script>
    jQuery(function(){
        $("#JugadoreImg").change(function(){
            var input = document.getElementById('JugadoreImg');
            var file = input.files[0];
            var data = new FormData();
            data.append('archivo',file);
            var url = getURL()+"valFoto";
            $.ajax({
                url: url,
                type: 'post',
                contentType:false,
                data: data,
                cache: false,
                processData:false,
                success: function(resp){
                    if(parseInt(resp)===1){
                        $("#JugadoreImg").val("");
                        $('body,html').animate({scrollTop : 0}, 500);
                        $("#msjalert").text("El formato de la foto no es valido.");
                        $("#alerta").slideDown();
                        setTimeout(function(){
                            $("#alerta").slideUp();
                        },4000);

                    }
                }
            });
        });
    });
</script>