<?= $this->Html->css("select2");    ?>
<?= $this->Html->script("select2"); ?>
<style>
	h2{
		margin-top: 15px;
		color:#cb071a;
		font-size: 1.4em;
	}
	.table > thead > tr > th:first-child {
		border-top-left-radius: 5px;
	}
	.table > thead > tr > th:last-child {
		border-top-right-radius: 5px;
	}
	.table tbody tr td {
		font-size: 16px;
	}
	.searchBox table tbody td {
		font-size: 16px;
		padding: 0px 5px 0 2px;
	}
	.searchBox table tbody td:nth-child(4){
		width: ;
	}
	table.table-striped>tbody>tr:nth-child(odd)>td{
		background-color: #ebebeb;
	}
	table.table tbody{
		border:1px solid #ccc;
	}
	.checkbox {
		padding-top: 18px;
	}
	label{
		margin-left: 7px;
	}
	input#ac {
		margin-top: 5px;
	}

</style>
<div class="recursos index container-fluid">
	<h2><?php echo __('Jugadores'); ?></h2>
	<p>
		<?php
		echo $this->paginator->counter(array(
			'format' => __('Página {:page} de {:pages}, {:current} registros de un total de {:count} , comienza en {:start}, finalizando en {:end}')
		));
		?></p>
	<?php
	/*Se realizara un nuevo formato de busqueda*/
	/*Inicia formulario de busqueda*/
	$tabla = "jugadores";
	?>
	<div id='search_box'>
        <div class="col-md-5 col-lg-5">
		<?php
		echo $this->Form->create($tabla);
		echo "<input type='hidden' name='_method' value='POST' />";
		echo "<table>";
		echo "<tr>";
		echo "<td width='35%'>";
		$search_text = (isset($_SESSION['tabla['.$tabla.']']['search_text']) && $_SESSION['tabla['.$tabla.']']['search_text'] != "")?$_SESSION['tabla['.$tabla.']']['search_text']:"";
		echo $this->Form->input('SearchText', array('label'=>'Buscar por: Nombre, Apellido, Apodo', 'name'=>'data['.$tabla.'][search_text]', 'value'=>$search_text,'placeholder'=>'Nombre, Apellido, Apodo', 'type'=>"hidden"));
        echo $this->Form->input("id", [
            'class'=>'form-control jugador-ls',
            'name'=>'data['.$tabla.'][id]',
            'options'=>$listJugadores,
            'label'=>'Jugador',
            'type'=>'select',
            'empty' => array( '(Seleccionar)'),
            'default'=>(isset($_SESSION['tabla[jugadores]']))?$_SESSION['tabla[jugadores]']['id']:''
        ]);
		echo "</td>";


		echo "<td width='10%' style='vertical-align: middle;  padding-left: 20px; padding-top: 20px;'>";
		echo $this->Form->hidden('Controller', array('value'=>'Jugadore', 'name'=>'data[tabla][controller]')); //hacer cambio
		echo $this->Form->hidden('Tabla', array('value'=>$tabla, 'name'=>'data[tabla][tabla]'));
		echo $this->Form->hidden('Parametro1', array('value'=>'nombre,apellido,apodo', 'name'=>'data[tabla][parametro]'));
		echo $this->Form->hidden('rangofecha', array('value'=>'no', 'name'=>'data['.$tabla.'][rangofecha]'));
		echo $this->Form->hidden('campoFecha', array('value'=>'fecha', 'name'=>'data['.$tabla.'][campoFecha]'));
		echo $this->Form->hidden('FechaHora', array('value'=>'si', 'name'=>'data['.$tabla.'][FechaHora]'));
		echo '<button class="btn btn-default" style="margin-left:15px; width: 100px;border-color: #606060;" type="submit"><span class="icon icon-search">Buscar</span></button>';
		echo $this->Form->end();
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		?>

		<?php
		if(isset($_SESSION["$tabla"]))
		{
			$real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
			echo $this->Form->button('Ver todos', array('type'=>'reset', 'onclick'=>'window.location.href =  \''.$real_url.'vertodos\'', 'class' => 'btn btn-info pull-left'));
		}
		?>
        </div>
        <div class="col-md-offset-5 col-lg-offset-5 col-md-2 col-lg-2" style="text-align: right; height: 75px;">
            <div  class="actions">
                <div><?php echo $this->Html->link(__('Nuevo Jugador'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></div>
            </div>
        </div>
	</div>
	<table cellpadding="0" cellspacing="0" 	class="table table-condensed table-striped">
		<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre', 'Nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('apellido', 'Apellido'); ?></th>
			<th><?php echo $this->Paginator->sort('apodo', 'Apodo'); ?></th>
			<th><?php echo $this->Paginator->sort('paise_id', 'País'); ?></th>
			<th><?php echo $this->Paginator->sort('posicione_id', 'Posición'); ?></th>
			<th class="bdr"><?php echo __('Acciones'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i=0;
		foreach ($jugadores as $jugadore):
			$class=null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
			?>
			<tr <?= $class;?> >
				<td><?= h($jugadore['Jugadore']['id']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($jugadore['Jugadore']['nombre']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($jugadore['Jugadore']['apellido']); ?>&nbsp;</td>
				<td style='text-align:left'><?= h($jugadore['Jugadore']['apodo']); ?>&nbsp;</td>
				<td style='text-align:center'><?= (isset($paises[$jugadore['Jugadore']['paise_id']]))?$paises[$jugadore['Jugadore']['paise_id']]:""; ?></td>
				<td style='text-align:center'><?= (isset($posiciones[$jugadore['Jugadore']['posicione_id']]))?$posiciones[$jugadore['Jugadore']['posicione_id']]:""; ?></td>
				<td class="">
					<?php echo $this->Html->link(__(' '), array('action' => 'view', $jugadore['Jugadore']['id']),array('class'=>'ver')); ?>
					<?php echo $this->Html->link(__(' '), array('action' => 'edit', $jugadore['Jugadore']['id']),array('class'=>'editar')); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="paging">
		<?php echo $this->Paginator->prev('<< '.__('Anterior', true), array(), null, array('class'=>'disabled'));?>
		| 	<?php echo $this->Paginator->numbers();?>
		<?php echo $this->Paginator->next(__('Siguiente', true).' >>', array(), null, array('class'=>'disabled'));?>
	</div>

</div>
<script>
    jQuery(function () {
        $(".jugador-ls").select2();
    });
</script>