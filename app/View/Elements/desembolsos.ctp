<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Fuente</th>
            <th>Banco Origen</th>
            <th>Banco Destino</th>
            <th>Código</th>
            <th>Destinatario</th>
            <th>Monto</th>
            <th>Comisiones</th>
            <th>Monto Total</th>
        </tr>
        <tr class="espacio">
            <td class="border2" colspan="9"></td>
        </tr>
        </thead>
        <tbody>
        <!-- ---------- RECORRIDO DE LOS DESEMBOLSOS ------------- -->
        <?php if(isset($data['desembolsos'])) { ?>
            <?php if(count($data['desembolsos']) > 0) { ?>
                <?php
                $total_monto = 0;
                $total_comision = 0;
                $total_total = 0;
                ?>
                <?php foreach ($data['desembolsos'] as $desembolso): ?>
                    <tr>
                        <td class="text-left border2 border4" >
                            <?php
                            $mes = date("n",strtotime($desembolso['Desembolso']['fecha']));
                            echo date("d",strtotime($desembolso['Desembolso']['fecha']))."-".$data['meses'][$mes]."-".date("Y",strtotime($desembolso['Desembolso']['fecha']));
                            ?>
                        </td>
                        <td class="text-left border2 centro"><?= $desembolso['Financista']['nombre'] ?></td>
                        <td class="text-left border2 centro"><?= $desembolso['Bancoorigen']['nombre'] ?></td>
                        <td class="text-left border2 centro"><?= $desembolso['Bancodestino']['nombre'] ?></td>
                        <td class="text-center border2 centro"><?= $desembolso['Desembolso']['codigo'] ?></td>
                        <td class="text-center border2 centro"><?= $desembolso[0]['nombre'] ?></td>
                        <td class="text-right border2 centro"><?= "$ " . number_format($desembolso['Desembolso']['monto'], 2); ?></td>
                        <td class="text-right border2 centro"><?= "$ " . number_format($desembolso['Desembolso']['comision'], 2); ?></td>
                        <td class="text-right border1 border2"><?= "$ " . number_format($desembolso['Desembolso']['total'], 2); ?></td>
                        <?php
                        $total_monto += $desembolso['Desembolso']['monto'];
                        $total_comision += $desembolso['Desembolso']['comision'];
                        $total_total += $desembolso['Desembolso']['total'];
                        ?>
                    </tr>
                <?php endforeach; ?>
                <tr class="totales">
                    <td class="border2 border4" colspan="6" style="text-align: right"><?php echo h('Totales'); ?></td>
                    <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($total_monto, 2); ?>&nbsp;</td>
                    <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($total_comision, 2); ?></td>
                    <td class="border1 border2" style="text-align: right"><?php echo  "$ " . number_format($total_total, 2); ?></td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="9" class="text-center border1 border2 border4">No hay desembolsos relacionados</td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>