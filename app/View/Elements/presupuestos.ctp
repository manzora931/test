<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
    <?php foreach($data['modules'] as $modulo) { ?>
    <?php if($data['presupuestosmodulo'][$modulo['Module']['id']] > 0) { ?>
        <div style="margin-bottom: 15px">
            <label class="">Módulo <?= $modulo['Module']['orden']." - ".$modulo['Module']['nombre']?></label>
            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
            <?php if(count($data['intervenciones'][$modulo['Module']['id']]) > 0) { ?>
                <?php foreach($data['intervenciones'][$modulo['Module']['id']] as $intervencion) { ?>
                    <?php if($data['presupuestosintervencion'][$intervencion['id']] > 0) { ?>
                    <div style="margin-bottom: 15px">
                        <label class="">Intervención <?= $intervencion['orden'] . " - " . $intervencion['nombre'] ?></label>
                        <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Actividad</th>
                                <th>Total Asignado</th>
                                <th>Desembolsado</th>
                                <th>Disponible</th>
                                <th>Ejecutado</th>
                            </tr>
                            <tr class="espacio">
                                <td class="border2" colspan="5"></td>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- ---------- RECORRIDO DE LOS PRESUPUESTOS ------------- -->
                            <?php
                                $sum_asignado=0.00;
                                $totdesembolso=0.00;
                                $totdisponible=0.00;
                                $totejecutado=0.00;
                            if(isset($data['presupuestos'][$intervencion['id']])) { ?>
                                <?php if(count($data['presupuestos'][$intervencion['id']]) > 0) { ?>
                                    <?php foreach ($data['presupuestos'][$intervencion['id']] as $presupuesto): ?>
                                        <tr>
                                            <td class="text-left border2 border4" ><?= $presupuesto['Actividade']['nombre'] ?></td>
                                            <td class="text-right border2">$ <?=number_format($presupuesto['Presupuesto']['totalasignado'],2)?></td>
                                            <td class="text-right border2">$ <?=number_format($presupuesto['Desembolso']['monto'],2)?></td>
                                            <td class="text-right border2">$ <?=number_format($presupuesto['Desembolso']['disponible'],2)?></td>
                                            <td class="text-right border1 border2">$ <?=number_format($presupuesto['Desembolso']['ejecutado'],2)?></td>
                                        </tr>
                                        <?php
                                        $sum_asignado+=$presupuesto['Presupuesto']['totalasignado'];
                                        $totdesembolso+=$presupuesto['Desembolso']['monto'];
                                        $totdisponible+=$presupuesto['Desembolso']['disponible'];
                                        $totejecutado+=$presupuesto['Desembolso']['ejecutado'];
                                    endforeach; ?>
                                    <tr class="totales">
                                        <td class="border2 border4" style="text-align: right"><?php echo h('Totales'); ?></td>
                                        <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($sum_asignado, 2); ?>&nbsp;</td>
                                        <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($totdesembolso, 2); ?></td>
                                        <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($totdisponible, 2); ?></td>
                                        <td class="border1 border2" style="text-align: right"><?php echo  "$ " . number_format($totejecutado, 2); ?></td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5" class="text-center border1 border2 border4">No hay presupuestos relacionados</td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>
</div>