<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
        <thead>
        <tr>
            <th>Fuente de Financiamiento</th>
            <th>Monto Total</th>
            <th>Desembolsado</th>
            <th>Ejecutado</th>
        </tr>
        <tr class="espacio">
            <td class="border2" colspan="4"></td>
        </tr>
        </thead>
        <tbody>
        <!-- ---------- RECORRIDO DE LAS FUENTES DE FINANCIAMIENTO ------------- -->
        <?php if(isset($data['fuentesfinanciamientos'])) { ?>
            <?php if(count($data['fuentesfinanciamientos']) > 0) { ?>
                <?php
                    $total_monto = 0;
                    $total_desembolsos = 0;
                    $total_gastos = 0;
                ?>
                <?php foreach ($data['fuentesfinanciamientos'] as $fuentesfinanciamiento): ?>
                    <tr>
                        <td class="text-left border2 border4" ><?= $fuentesfinanciamiento['Financista']['nombre']; ?></td>
                        <td class="text-right border2 centro"><?= "$ " . number_format($fuentesfinanciamiento['Fuentesfinanciamiento']['monto'], 2); ?></td>
                        <td class="text-right border2 centro"><?= "$ " . number_format($info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['desembolso'], 2); ?></td>
                        <td class="text-right border1 border2"><?= "$ " . number_format($info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['ejecutado'], 2); ?></td>
                        <?php
                            $total_monto += $fuentesfinanciamiento['Fuentesfinanciamiento']['monto'];
                            $total_desembolsos += $info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['desembolso'];
                            $total_gastos += $info_ffinanciamiento[$fuentesfinanciamiento['Fuentesfinanciamiento']['id']]['ejecutado'];
                        ?>
                    </tr>
                <?php endforeach; ?>
                <tr class="totales">
                    <td class="border2 border4" style="text-align: right"><?php echo h('Totales'); ?></td>
                    <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($total_monto, 2); ?>&nbsp;</td>
                    <td class="border2" style="text-align: right"><?php echo  "$ " . number_format($total_desembolsos, 2); ?></td>
                    <td class="border1 border2" style="text-align: right"><?php echo  "$ " . number_format($total_gastos, 2); ?></td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="4" class="text-center border1 border2 border4">No hay fuentes de financiamiento relacionadas</td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>