<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
    <?php foreach($data['modules'] as $modulo) { ?>
        <?php if($data['actividadesmodulo'][$modulo['Module']['id']] > 0) { ?>
        <div style="margin-bottom: 15px">
            <label class=""><?= $modulo['Module']['nombre']?></label>
            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
            <?php if(count($data['intervenciones'][$modulo['Module']['id']]) > 0) { ?>
                <?php foreach($data['intervenciones'][$modulo['Module']['id']] as $intervencion) { ?>
                    <?php if($data['actividadesintervencion'][$intervencion['id']] > 0) { ?>
                    <div style="margin-bottom: 15px">
                        <label class=""><?= $intervencion['nombre'] ?></label>
                        <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                            <tr>
                                <th width="35%">Actividad</th>
                                <th>País</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Límite</th>
                                <th width="30%">Fuente de Financiamiento</th>
                            </tr>
                            <tr class="espacio">
                                <td class="border2" colspan="5"></td>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- ---------- RECORRIDO DE LAS ACTIVIDADES ------------- -->
                            <?php if(isset($data['actividades'][$intervencion['id']])) { ?>
                                <?php if(count($data['actividades'][$intervencion['id']]) > 0) { ?>
                                    <?php foreach ($data['actividades'][$intervencion['id']] as $actividad): ?>
                                        <tr>
                                            <td class="text-left border2 border4" ><?= $actividad['Actividade']['nombre'] ?></td>
                                            <td class="text-left border2"><?= $actividad['Paise']['pais'] ?></td>
                                            <td class="text-center border2">
                                                <?php
                                                    $mes = date("n",strtotime($actividad['Actividade']['inicio']));
                                                    echo date("d",strtotime($actividad['Actividade']['inicio']))."-".$data['meses'][$mes].date("Y",strtotime($actividad['Actividade']['inicio']));
                                                ?>
                                            </td>
                                            <td class="text-center border2">
                                                <?php
                                                    $mes = date("n",strtotime($actividad['Actividade']['limite']));
                                                    echo date("d",strtotime($actividad['Actividade']['limite']))."-".$data['meses'][$mes].date("Y",strtotime($actividad['Actividade']['limite']));
                                                ?>
                                            </td>
                                            <td class="text-left border1 border2"><?= $data['fuentesfinan'][$actividad['Actividade']['fuentesfinanciamiento_id']];?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5" class="text-center border1 border2 border4">No hay actividades relacionadas</td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
    <?php } ?>
</div>