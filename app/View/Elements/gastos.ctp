<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
    <?php foreach($data['modules'] as $modulo) { ?>
        <?php // if($data['gastosmodulo'][$modulo['Module']['id']] > 0) { ?>
        <div style="margin-bottom: 15px">
            <label class="">Módulo <?= $modulo['Module']['orden']." - ".$modulo['Module']['nombre']?></label>
            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
            <?php if(count($data['intervenciones'][$modulo['Module']['id']]) > 0) { ?>
                <?php foreach($data['intervenciones'][$modulo['Module']['id']] as $intervencion) { ?>
                    <?php // if($data['gastosintervencion'][$intervencion['id']] > 0) { ?>
                    <div style="margin-bottom: 15px">
                        <label class="">Intervención <?= $intervencion['orden'] . " - " . $intervencion['nombre'] ?></label>
                        <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Categoría de Gasto</th>
                                <th>Descripción</th>
                                <th>Actividad</th>
                                <th>Fecha</th>
                                <th>Monto</th>
                            </tr>
                            <tr class="espacio">
                                <td class="border2" colspan="5"></td>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- ---------- RECORRIDO DE LOS GASTOS ------------- -->
                            <?php
                            $totgeneral=0.00;
                            if(isset($data['gastos'][$intervencion['id']])) { ?>
                                <?php if(count($data['gastos'][$intervencion['id']]) > 0) { ?>
                                    <?php foreach ($data['gastos'][$intervencion['id']] as $gasto): ?>
                                        <tr>
                                            <td class="text-left border2 border4" ><?= $gasto['Catgasto']['nombre'] ?></td>
                                            <td class="text-left border2 centro"><?= $gasto['Gasto']['descripcion'] ?></td>
                                            <td class="text-left border2 centro"><?= $gasto['Actividade']['nombre'] ?></td>
                                            <td class="text-center border2 centro">
                                                <?php
                                                $mes = date("n",strtotime($gasto['Gasto']['fecha']));
                                                echo date("d",strtotime($gasto['Gasto']['fecha']))."-".$data['meses'][$mes]."-".date("Y",strtotime($gasto['Gasto']['fecha']));
                                                ?>
                                            </td>
                                            <td class="text-right border1 border2"><span  style="margin-bottom: 2px;"><?= "$ " . number_format($gasto['Gasto']['monto'], 2); ?></span></td>
                                        </tr>
                                    <?php
                                    $totgeneral += $gasto['Gasto']['monto'];
                                    endforeach; ?>
                                    <tr>
                                        <td colspan="4" class="border2 border4" style="text-align: right"><strong><?php echo h('Totales'); ?></strong></td>
                                        <td class="border1 border2" style="text-align: right"><?php echo  "$ " . number_format($totgeneral, 2); ?>&nbsp;</td>

                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5" class="text-center border1 border2 border4">No hay gastos relacionados</td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                    <?php // } ?>
                <?php } ?>
            <?php } ?>
        </div>
        <?php // } ?>
    <?php } ?>
</div>