<?php $session = $this->Session->read("tabla[$table]"); ?>
<?php $search_text = $session['search_text'] ?>
<div id='search_box' class="searchBox clearfix">
    <table>
        <?php echo $this->Form->create($table, array('action' => 'index')); ?>
        <input type='hidden' name='_method' value='POST' />
        <td class="search_text form-group" style="padding-right: 0">
                <label style="color: #444444;">Buscar por:</label>
                <?= $this->Form->input('search-by', [
                    'label' => false,
                    'div' => false,
                    'type' => 'select',
                    'id' => 'search-by',
                    'class' => 'form-control',
                    'options' => $options,
                    'empty' => [
                        '' => 'Buscar Por'
                    ]
                ]); ?>
        </td>
        <td style="vertical-align: bottom; padding-left: 0">

                <?= $this->Form->input('SearchText', array(
                    'label' => false,
                    'name' => 'data['. $table .'][search_text]',
                    'value' => $search_text,
                    'class' => 'form-control',
                    'div' => false,
                    'placeholder' => $placeholder
                )); ?>
                <!--<button class="btn btn-default" type="submit"><span class="icon icon-search"></span></button>-->
        </td>
        <td class="search_active form-group" style="vertical-align: bottom;">
            <?= $this->Form->input('activo', [
                'label' => false,
                'type' => 'select',
                'id' => 'active',
                'class' => 'form-control special-select',
                'options' => [
                    1 => "Inactivos",
                    2 => "Activos/Inactivos"
                ],
                'empty' => [
                    '' => 'Activos'
                ]
            ]); ?>
        </td>
        <td style="vertical-align: bottom; padding-bottom: 1px">
            <?= $this->Form->input('Buscar', [
                'label' => false,
                'class' => 'btn btn-default',
                'type' => 'submit'
            ]) ?>
        </td>
        <?php
        echo $this->Form->hidden('controller', [
            'value' => $controller,
            'name' => 'data[tabla][controller]'
        ]);
        echo $this->Form->hidden('Tabla', [
            'value' => $table,
            'name'=>'data[tabla][tabla]'
        ]);
        echo $this->Form->end();
        ?>
        <?php if ($buttons['add']) { ?>
            <td class="search_create form-group" style="vertical-align: bottom; padding-bottom: 1px">
                <a class="btn btn-block btn-info" href="<?= Router::url(['action' => 'add']) ?>" name="button">
                    <span class="icon icon-add-cirled"></span>Crear
                </a>
            </td>
        <?php } ?>
        <?php if ($buttons['edit']) { ?>
            <td class="search_modify form-group" style="vertical-align: bottom; padding-bottom: 1px">
                <button type="button" class="btn btn-block btn-info" id="edit-details" name="button">
                    <span class="icon icon-edit"></span>Modificar</button>
            </td>
        <?php } ?>
        <?php if ($buttons['view']) { ?>
            <td class="search_view form-group" style="vertical-align: bottom; padding-bottom: 1px">
                <button type="submit" class="btn btn-block btn-info" id="view-details" name="button">
                    <span class="icon icon-view"></span>Ver Detalles</button>
            </td>
        <?php } ?>
        <?php if ($buttons['clone']) { ?>
            <td class="search_clone form-group" style="vertical-align: bottom; padding-bottom: 1px">
                <button type="button" class="btn btn-block btn-info" id="clone-entry" name="button">
                    <span class="icon icon-clone"></span>Clonar</button>
            </td>
        <?php } ?>
    </table>
    <?php if ($this->Session->check($table)) {
        echo $this->Html->link('Ver todos', [
            'action' => 'vertodos'
        ], [
            'class' => 'btn btn-primary pull-left'
        ]);
    } ?>
    <script type="text/javascript">
        var searchBy = $("#search-by");
        var active = $("#active");

        searchBy.val('<?= $session['search-by'] ?>');
        active.val('<?= $session['activo'] ?>');
    </script>
</div>