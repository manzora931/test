<div class="col-print-12" style="margin-top: 15px;">
    <label class="tab-encabezado"><?= $encabezado ?></label>
</div>
<div class="col-print-12">
    <!-- ---------- RECORRIDO DE LOS MODULOS ------------- -->
    <?php if(count($data['modules']) > 0) { ?>
    <?php foreach($data['modules'] as $modulo) { ?>
        <?php if($data['incidenciasmodulo'][$modulo['Module']['id']] > 0) { ?>
        <div style="margin-bottom: 15px">
            <label class="">Módulo <?= $modulo['Module']['orden']." - ".$modulo['Module']['nombre']?></label>
            <!-- ---------- RECORRIDO DE LAS INTERVENCIONES ------------- -->
            <?php if(count($data['intervenciones'][$modulo['Module']['id']]) > 0) { ?>
                <?php foreach($data['intervenciones'][$modulo['Module']['id']] as $intervencion) { ?>
                    <?php if($data['incidenciasintervencion'][$intervencion['id']] > 0) { ?>
                    <div style="margin-bottom: 15px">
                        <label class="">Intervención <?= $intervencion['orden'] . " - " . $intervencion['nombre'] ?></label>
                        <!-- ---------- RECORRIDO DE LAS INCIDENCIAS ------------- -->
                        <?php if(isset($data['incidencias'][$intervencion['id']])) { ?>
                            <?php if(count($data['incidencias'][$intervencion['id']]) > 0) { ?>
                                <?php foreach ($data['incidencias'][$intervencion['id']] as $incidencia): ?>
                                    <div style="margin-bottom: 15px">
                                        <label class="">Actividad: <?= $incidencia['Actividade']['nombre']; ?></label>
                                        <div class="col-print-6">
                                            <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%; margin-top: 5px !important; ">
                                                <thead>
                                                <tr>
                                                    <th width="50%">Rango de Edad</th>
                                                    <th width="50%">Cantidad</th>
                                                </tr>
                                                <tr class="espacio">
                                                    <td class="border2" colspan="2"></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if(count($incidencia['Rangoedadincidencia']) > 0) { ?>
                                                    <?php
                                                    $acum = 0;
                                                    foreach ($incidencia['Rangoedadincidencia'] as $rango): ?>
                                                        <tr>
                                                            <td class="text-left border2 border4" ><?= $data['rangos'][$rango['rangoedad_id']] ?></td>
                                                            <td class="text-center border1 border2"><?= $rango['cantidad'] ?></td>
                                                        </tr>
                                                    <?php
                                                    $acum += $rango['cantidad'];
                                                    endforeach; ?>
                                                    <tr>
                                                        <td class="border2 border4">
                                                            <strong>Total</strong>
                                                        </td>
                                                        <td class="border1 border2 text-center">
                                                            <?=$acum;?>
                                                        </td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td colspan="2" class="text-center">No hay rango de edades relacionados</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-print-6">
                                            <table id="table-index" class="interlineado impresion" cellspacing="0" cellpadding="0" style="width: 100%; margin-top: 5px;">
                                                <thead>
                                                <tr>
                                                    <th width="50%">Género</th>
                                                    <th width="50%">Cantidad</th>
                                                </tr>
                                                <tr class="espacio">
                                                    <td class="border2" colspan="2"></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if(count($incidencia['Generoincidencia']) > 0) { ?>
                                                    <?php
                                                    $cont =0;
                                                    foreach ($incidencia['Generoincidencia'] as $genero): ?>
                                                        <tr>
                                                            <td class="text-left border2 border4" ><?= $data['generos'][$genero['genero_id']] ?></td>
                                                            <td class="text-center border1 border2"><?= $genero['cantidad'] ?></td>
                                                        </tr>
                                                    <?php
                                                    $cont += $genero["cantidad"];
                                                    endforeach; ?>
                                                    <tr>
                                                        <td class="border2 border4">
                                                            <strong>Total</strong>
                                                        </td>
                                                        <td class="border1 border2 text-center">
                                                            <?=$cont;?>
                                                        </td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td colspan="2" class="text-center">No hay géneros relacionados</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php endforeach; ?>
                            <?php } else { ?>

                            <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            </div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>