<?php $session = $this->Session->read("tabla[$table]"); ?>
<?php $search_text = $session['search_text'] ?>
<style>
    table#tblb{
        margin-top: 5px;
        margin-bottom: 3px;
        background-color:#f6f6f6 ;
    }
    .checkbox label{
        padding: 0;
        padding-bottom: -10px;
    }
    label{
        margin-left: 7px;
    }
    a.btn-default{
        width: 30%;
        font-family: 'Calibri', sans-serif;
        margin-right: 15px;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;
        float: right;
    }
    a.btn-default:hover{
        width: 30%;
        font-family: 'Calibri', sans-serif;
        float: right;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;
    }
    #cancel.btn-default{
        width: 20%;
        font-family: 'Calibri', sans-serif;
        float: right;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;
        margin-right: 15px;

    }
    #cancel.btn-default:hover{
        width: 20%;
        font-family: 'Calibri', sans-serif;
        float: right;
        padding: 0 5px 0 5px;
        background-color: #c0c0c0 ;
        border:1px solid #606060 ;
        font-size: 14px;
        color: #000;
        border-radius: 1px;
        margin-right: 15px;
    }
    .privilegios #search-box{
        border-top:6px solid #4160a3;
        background-color:#f6f6f6 ;
        margin-bottom: 15px;
    }
   .select .form-control{

   }
</style>
<div id="search-box" class="clearfix no-clone">
    <table id="tblb" width="100%">
        <?php echo $this->Form->create($table, array('action' => 'index')); ?>
        <input type='hidden' name='_method' value='POST' />
        <td  width="27%" class="search_text form-group">
            <div class="input-group">
                <!--  <?= $this->Form->input('search-by', [
                    'label' => false,
                    'div' => false,
                    'type' => 'select',
                    'id' => 'search-by',
                    'class' => 'form-control',
                  //  'options' => $options,
                    'empty' => [
                        '' => 'Buscar Por'
                    ]
                ]); ?>-->
                <?= $this->Form->input('SearchText', array(
                    'label' => 'Buscar por:',
                    'name' => 'data['. $table .'][search_text]',
                    'value' => $search_text,
                    'class' => 'form-control',
                    'style'=>'width:300px; display:inline; margin:0 0 10px 7px;',
                    'div' => false,
                    'placeholder' =>'Herramienta'
                )); ?>

            </div>
        </td>
        <td width="100px" class="search_active form-group">
            <?= $this->Form->input('activo', [
                'label' =>'Inactivos',
                'type' => 'checkbox',
                'name'=>'activo',
                'id' => 'active',
                'value'=>false,
                'class' => ' ',

            ]); ?>
        </td>
        <td width="10%">  <button class="btn btn-default" type="submit"><span class="icon icon-search">Buscar</span></button></td>

        <?php
        echo $this->Form->hidden('controller', [
            'value' => $controller,
            'name' => 'data[tabla][controller]'
        ]);
        echo $this->Form->hidden('Tabla', [
            'value' => $table,
            'name'=>'data[tabla][tabla]'
        ]);
        echo $this->Form->end();
        ?>
        <?php if ($buttons['add']) { ?>
            <td  class="search_create form-group">
                <a class="btn  btn-default"  id="create"  href="<?= Router::url(['action' => 'add']) ?>" name="button">
                    <span class="icon icon-add-cirled"></span>Crear Privilegio
                </a>
            </td>
        <?php } ?>
        <?php if ($buttons['edit']) { ?>
            <td class="search_modify form-group">
                <button type="button" class="btn btn-block btn-info" id="edit-details" name="button">
                    <span class="icon icon-edit"></span>Modificar</button>
            </td>
        <?php } ?>
        <?php if ($buttons['view']) { ?>
            <td class="search_view form-group">
                <button type="submit" class="btn btn-block btn-info" id="view-details" name="button">
                    <span class="icon icon-view"></span>Ver Detalles</button>
            </td>
        <?php } ?>
        <?php if ($buttons['clone']) { ?>
            <td class="search_clone form-group">
                <button type="button" class="btn btn-block btn-info" id="clone-entry" name="button">
                    <span class="icon icon-clone"></span>Clonar</button>
            </td>
        <?php } ?>
        <td width="10%">  <button id="cancel" class="btn btn-default" type="submit"><span class="icon icon-search">Cancelar</span></button></td>
    </table>

    <?php if ($this->Session->check($table)) {
        echo $this->Html->link('Ver todos', [
            'action' => 'vertodos'
        ], [
            'class' => 'btn btn-primary pull-left'
        ]);
    } ?>
    <script type="text/javascript">
        var searchBy = $("#search-by");
        var active = $("#active").is('checked')?1:0;
        $("#cancel").addClass('hide');
        searchBy.val('<?= $session['search-by'] ?>');
      //  active.val('<?= $session['activo'] ?>');
        $("#cancel").click(function(){
            $("#addPrivilegio").addClass('hide')
            $("#cancel").addClass('hide');
        });
        $("#create").click(function(){
            $("#cancel").removeClass('hide');
        })
    </script>
</div>