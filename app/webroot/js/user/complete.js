function complete(id){

    var source = getURL()+'empleados/autoCompletado';

        $( "#"+id).autocomplete({
            minLength: 3,
            source: source,
            focus: function( event, ui )  {
                $( "#"+id ).val(ui.item.Empleado.nombres+" "+ui.item.Empleado.apellidos );
                $("#emp").val(ui.item.Empleado.id);
                return false;
            },
            change: function(event,ui){
                if(ui.item == null){
                    $("#emp").val("");
                    $("#"+id).val("");
                    $("#"+id).validationEngine("showPrompt","Empleado Invalido","AlertText");
                }
            },
            select: function( event, ui ) {
                //alert(ui);
                $( "#"+id ).val(ui.item.Empleado.nombres+" "+ui.item.Empleado.apellidos);
                $("#emp").val(ui.item.Empleado.id);
                return false;
            }//select*/
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a href='#'>" + item.Empleado.nombres+ " "+item.Empleado.apellidos+"</a>" )
                .appendTo( ul );
        };

}
function limpiar(id){
    if($("#"+id).val()==""){
        $("#emp").val("");
        $("#UserEmpleado").addClass("invalido");
        $(".invalido").validationEngine("showPrompt",'Empleado Inválido',"AlertText");
    }
}
