function addClass(element, classNameNew){
    var classNameActual=element.className;
    element.className=classNameActual+" "+classNameNew;
}

function removeClass(element, classNameRemove){
    var classNameActual=element.className;
    var classSplit=classNameActual.split(classNameRemove);
    element.className=classSplit[0];
}