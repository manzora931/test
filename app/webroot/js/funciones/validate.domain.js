function validarDomain(torneo_id, numposiciones, display) {
    var ruta = document.referrer;
    var url = getURL()+"validateDomain";
    $.ajax({
        url: url,
        type: 'post',
        data: {ruta: ruta},
        async: false,
        cache: false,
        success: function (resp) {
            if(!resp){
                var redirec = getURL()+display+"/"+torneo_id+"/"+numposiciones+"/"+0;
                location.href=redirec;
            }else{
                var redirec = getURL()+display+"/"+torneo_id+"/"+numposiciones+"/"+1;
                location.href=redirec;
            }
        }
    });
}
function validarDomainv2(torneo_id, display) {
    var ruta = document.referrer;
    var url = getURL()+"validateDomain";
    $.ajax({
        url: url,
        type: 'post',
        data: {ruta: ruta},
        async: false,
        cache: false,
        success: function (resp) {
            if(!resp){
                var redirec = getURL()+display+"/"+torneo_id+"/"+0;
                location.href=redirec;
            }else{
                var redirec = getURL()+display+"/"+torneo_id+"/"+1;
                location.href=redirec;
            }
        }
    });
}