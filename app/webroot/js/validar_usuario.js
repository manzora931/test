$(function() {
    $("#formulario").submit(function() {

        var nombre = $("#name").val();
        var usuario = $('#user').val();
        var pass = $('#clave').val();
        var pass2 = $('#verificarclave').val();
        var correo = $('#email').val();
        var lon = pass.length;

        if (lon < 8) {
            $("#alert .message").text('La contrase\u00f1a debe de tener como minimo 8 caracteres');
            $("#alert").slideDown();

            setTimeout(function () {
                $("#alert").slideUp();
            }, 4000);

            return false;
        }

        if (pass != pass2) {
            $("#alert .message").text('Las contrase\u00f1as no coinciden');
            $("#alert").slideDown();

            setTimeout(function () {
                $("#alert").slideUp();
            }, 4000);

            return false;
        }

        if (correo == '') {
            $("#alert .message").text('El correo electrónonico es obligatorio');
            $("#alert").slideDown();

            setTimeout(function () {
                $("#alert").slideUp();
            }, 4000);

            return false;
        } else {
            if (correo.indexOf('@', 0) == -1 || correo.indexOf('.', 0) == -1) {
                $("#alert .message").text('Correo Invalido');
                $("#alert").slideDown();

                setTimeout(function () {
                    $("#alert").slideUp();
                }, 4000);

                return false;
            }
        }
    });
});