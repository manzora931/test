function complete(id){
    var source = getURL2()+'groups/auto_completado2';
    $( "#"+id).autocomplete({
        minLength: 3,
        source: source,
        focus: function( event, ui )  {
            $( "#"+id ).val(ui.item.Group.name);
            return false;
        },
        select: function( event, ui ) {
            $( "#"+id ).val(ui.item.Group.name);

            return false;
        }
    }).data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a href='#'>" + item.Group.name+"</a>" )
            .appendTo( ul );
    };
}
