jQuery(function(){
    //CARGAR FUENTE DE FINANCIAMIENTO
    $("#ff").click(function(){
        var id = $("#proyecto").data("proyecto");
        $("#childs").load(getURL()+"fuentesfinanciamientos/index/"+id);
        $(".activa").removeClass("activa");
        $("#ffc").addClass("activa");
    });
    //CARGAR ACTIVIDADES
    $("#act").click(function(){
        var id = $("#proyecto").data("proyecto");
        $("#childs").load(getURL()+"actividades/index/"+id);
        $(".activa").removeClass("activa");
        $("#actc").addClass("activa");
    });
    //CARGAR DESEMBOLSOS
    $("#desembolso").click(function (e){
        var id = $("#proyecto").data("proyecto");
        e.preventDefault();
        $("#childs").load(getURL()+"desembolsos/index/"+id);
        $(".activa").removeClass("activa");
        $("#desc").addClass("activa");
    });
    //CARGAR GASTOS
    $("#gastos").click(function (e){
        var id = $("#proyecto").data("proyecto");
        e.preventDefault();
        $("#childs").load(getURL()+"gastos/index/"+id);
        $(".activa").removeClass("activa");
        $("#gasc").addClass("activa");
    });
    //CARGAR PRESUPUESTO
    $("#presupuesto").click(function (e){
        var id = $("#proyecto").data("proyecto");
        e.preventDefault();
        $("#childs").load(getURL()+"presupuestos/index/"+id);
        $(".activa").removeClass("activa");
        $("#prec").addClass("activa");
    });
    //CARGAR INCIDENDENCIAS
    $("#incidencias").click(function (e){
        var id = $("#proyecto").data("proyecto");
        e.preventDefault();
        $("#childs").load(getURL()+"incidencias/index/"+id);
        $(".activa").removeClass("activa");
        $("#incc").addClass("activa");
    });
});