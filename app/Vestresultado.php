<?php
App::uses('AppModel', 'Model');
/**
 * Vestresultado Model
 *
 * @property Organizacion $Organizacion
 * @property Vcuenta $Vcuenta
 */
class Vestresultado extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'vcuenta_id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Organizacion' => array(
			'className' => 'Organizacion',
			'foreignKey' => 'organizacion_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vcuenta' => array(
			'className' => 'Vcuenta',
			'foreignKey' => 'vcuenta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
