<?php
App::uses('AppController', 'Controller');
/**
 * Recursos Controller
 *
 * @property Recurso $Recurso
 * @property PaginatorComponent $Paginator
 */
class RecursosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	//var $components = array('Paginator', 'Session');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
       	$this->ValidarUsuario('Recurso', 'recursos', 'index');

        $this->Paginator->settings = array('conditions' => array('Recurso.activo' => 1),'order'=> array('Recurso.nombre'=>'asc'));
        $this->Recurso->recursive = 0;
        include 'busqueda/recursos.php';
        $data = $this->Paginator->paginate('Recurso');
        $this->set('recursos', $data);

        $modulos = $this->Recurso->Modulo->find('list');
        $this->set(compact('modulos'));
    }

    public function vertodos()
    {
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[recursos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->ValidarUsuario('Recurso', 'recursos', 'view');
		if (!$this->Recurso->exists($id)) {
			throw new NotFoundException(__('Invalid recurso'));
		}
		$options = array('conditions' => array('Recurso.' . $this->Recurso->primaryKey => $id));
		$this->set('recurso', $this->Recurso->find('first', $options));

		$modulos=$this->Recurso->Modulo->find("list");
        $detrecursos = $this->Recurso->query('SELECT * FROM detrecursos WHERE recurso_id = '.$id);
        $detrecursofirmas=array();
        $this->set(compact("detrecursofirmas","modulos", "detrecursos"));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$this->ValidarUsuario('Recurso', 'recursos', 'add');
		if ($this->request->is('post')) {
			$this->Recurso->create();
			$this->request->data['Recurso']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Recurso']['modified'] = 0;

			if ($this->Recurso->save($this->request->data)) {
            	$recurso_id = $this->Recurso->id;
                $perfiles = $this->Recurso->query("SELECT id FROM perfiles");

                foreach($perfiles as $perfil) {
                    $this->Recurso->query("INSERT INTO privilegios (perfile_id, recurso_id, activo, leer, adicionar, editar, borrar, usuario, created)
                                           VALUES (".$perfil['perfiles']['id'].", '".$recurso_id."', '0', '0', '0', '0', '0', '".$this->Session->read('nombreusuario')."', '".date('Y-m-d H:i:s')."');");
                }

                $this->Session->write("recur_save",1);
                $this->redirect(array('action' => 'view',$recurso_id));
                for($i = 0; $i < count($this->data['Detrecurso']['nombre']); $i++){
                    if(isset($this->data['Detrecurso']['nombre'][$i]) && ($this->data['Detrecurso']['nombre'][$i] != "" || $this->data['Detrecurso']['nombre'][$i] != " ")){
                        $this->Recurso->query("INSERT INTO detrecursos(recurso_id, nombre, funcion, activo) VALUES (".$recurso_id.",'".$this->data['Detrecurso']['nombre'][$i]."','".$this->data['Detrecurso']['funcion'][$i]."',".$this->data['Detrecurso']['activo'][$i].")");
                    }
                }
			} else {
				$this->Flash(__('El recurso NO ha sido almacenado. Intentar de nuevo.'));
			}
		}

		$modulos = $this->Recurso->Modulo->find('list', array('conditions'=>array('activo'=>1)));
		$organizacions = $this->Recurso->Organizacion->find('list');

		$this->set(compact('firmas', 'modulos', 'organizacions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario('Recurso', 'recursos', 'edit');
		if (!$this->Recurso->exists($id)) {
			throw new NotFoundException(__('Recurso Inválido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Recurso']['usuariomodif'] = $this->Session->read('nombreusuario');

			if ($this->Recurso->save($this->request->data)) {
                $this->Session->write("recur_save",1);
                $this->redirect(array('action' => 'view',$id));
                for($i = 0; $i < count($this->data['Detrecurso']['nombre']); $i++) {
                    if (isset($this->data['Detrecurso']['id'][$i])) {
                        $this->Recurso->query("UPDATE detrecursos SET recurso_id=".$id.",nombre='" . $this->data['Detrecurso']['nombre'][$i] . "',funcion='" . $this->data['Detrecurso']['funcion'][$i] . "',activo=".$this->data['Detrecurso']['activo'][$i]." WHERE id=".$this->data['Detrecurso']['id'][$i]);
                    }else{
                        if($this->data['Detrecurso']['nombre'][$i] != "" || $this->data['Detrecurso']['nombre'][$i] != " ")
                            $this->Recurso->query("INSERT INTO detrecursos(recurso_id, nombre, funcion, activo) VALUES (".$id.",'" . $this->data['Detrecurso']['nombre'][$i] . "','" . $this->data['Detrecurso']['funcion'][$i] . "',".$this->data['Detrecurso']['activo'][$i].")");
                    }
                }
                /*-------------------------------------------*/
                /*foreach($this->request->data['Detrecursofirmas'] as $indice => $val){
                    if(isset($val['id'])){
                        //SI EXISTE ID ES PORQUE SOLO SE MODIFICARA EL DETALLE
                        if($val['firma_id']!=''&&$val['funcion']!=''&&($val['orden']!=''||$val['orden']!=0)){
                            $user = $this->Session->read("nombreusuario");
                            $date = date("Y-m-d H:i:s");
                            $permi = 0;
                            if(isset($val['permitir'])){
                                $permi=1;
                            }
                            $this->Recurso->query("UPDATE detrecursofirmas set firma_id = ".$val['firma_id'].", funcion = '".$val['funcion']."', orden=".$val['orden'].", permitir = ".$permi.", modified='".$date."',usuariomodif='".$user."' WHERE id = ".$val['id']);
                        }
                    }else{
                        if($val['firma_id']!=''&&$val['funcion']!=''&&($val['orden']!=''||$val['orden']!=0)){
                            $user = $this->Session->read("nombreusuario");
                            $date = date("Y-m-d H:i:s");
                            $mod = "0000-00-00 00:00:00";
                            $permi = 0;
                            if(isset($val['permitir'])){
                                $permi=1;
                            }
                            $this->Recurso->query("INSERT INTO detrecursofirmas(firma_id,recurso_id,funcion,orden,permitir,usuario,created,modified) VALUES (".$val['firma_id'].",".$id.",'".$val['funcion']."','".$val['orden']."',".$permi.",'".$user."','".$date."','".$mod."')");
                        }
                    }
                }*/



			} else {
                $this->Session->write("recur_save",0);
                $this->redirect(array('action' => 'view',$id));
			}
		}
			$options = array('conditions' => array('Recurso.' . $this->Recurso->primaryKey => $id));
			$this->request->data = $this->Recurso->find('first', $options);

			//$modulos=$this->Recurso->Modulo->find("list", array('conditions'=>array('Modulo.activo'=>1)));
			$modulos=$this->Recurso->Modulo->find('list');
            $detrecursos = $this->Recurso->query('SELECT * FROM detrecursos WHERE recurso_id = '.$id);
            /*-----------------------------FIRMAS------------------------------------*/
            //$detrecursofirmas = $this->Recurso->query('SELECT id,firma_id,recurso_id,funcion,orden,permitir FROM detrecursofirmas WHERE recurso_id = '.$id);
            //$sql_f = $this->Recurso->query("SELECT id,nombre FROM firmas WHERE activo=1");
            /*$firmas=array();
            foreach($sql_f as $ind => $val){
                $firmas[$val['firmas']['id']]=$val['firmas']['nombre'];
            }*/
			$this->set(compact("modulos","detrecursos"));
	}
    public function val_model() {
        $valor=$_POST['val'];
        $id =$_POST['id'];
        if($id != 0){
            $datos=$this->Recurso->query("SELECT modelo FROM recursos WHERE modelo='$valor' AND id !=".$id);
        }else{
            $datos=$this->Recurso->query("SELECT modelo FROM recursos WHERE modelo='$valor'");
        }
        if(count($datos)>0){
            echo "error";
        }else{
            echo "ok";
        }
        $this->autoRender=false;
    }
    public function val_name() {
        $valor=$_POST['val'];
        $id =$_POST['id'];
        if($id != 0){
            $datos=$this->Recurso->query("SELECT nombre FROM recursos WHERE nombre='$valor' AND id !=".$id);
        }else{
            $datos=$this->Recurso->query("SELECT nombre FROM recursos WHERE nombre='$valor'");
        }
        if(count($datos)>0){
            echo "error";
        }else{
            echo "ok";
        }
        $this->autoRender=false;
    }

}
