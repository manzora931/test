<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under El MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');


App::uses('CakeTime', 'Utility');
date_default_timezone_set('America/El_Salvador');
//CakeTime::convert(time(), new DateTimeZone('America/El_Salvador'));
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                /*'controller' => 'tipoinventarios',
                'action' => 'index'
                'controller' => 'bodegas',
                'action' => 'index'*/
                'controller' => 'torneos',
                'action' => 'index',
                'home'
            ),
            'logoutRedirect' => array(
                'controller' => 'pages',
                'action' => 'display',
                'home'
            )
        )
    );

    function beforeFilter() {
        /*PROCEDIMIENTO PARA VERIFICAR QUE EL USUARIO ESTE
        ACTIVO PARA PODER INICIAR SESSION EN EL SISTEMA*/
        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                'userModel' => 'User',
                'fields' => array(
                    'username' => 'username',
                ),
                'scope' => array(
                    'User.activo' => 1),
            ),
            'Form',
        );

        $this->loadModel('User');
        if ($this->Session->read('userId')) {
            $loggedUser = $this->User->find('first', [
                'fields' => [
                    'User.nombrecompleto',
                    'User.genero',
                    'Contacto.nombres',
                    'Contacto.apellidos',
                    'Contacto.institucion_id'
                ],
                'conditions' => [
                    'User.id' => $this->Session->read('userId')
                ],
                'recursive' => 0
            ]);

            $this->loadModel('Institucion');
            $userInstitucion = $this->Institucion->find('first', [
                'fields' => [
                    'Paise.pais'
                ],
                'conditions' => [
                    'Institucion.id' => $loggedUser['Contacto']['institucion_id']
                ],
                'recursive' => 0
            ]);

            $this->loadModel('Paise');
            $pais = $this->Paise->find('first', [
                'fields' => [
                    'Paise.pais'
                ],
                'conditions' => [
                    'Paise.predeterminado' => 1
                ],
                'recursive' => 0
            ]);

            $this->set(compact('loggedUser', 'userInstitucion', 'pais'));
        }

        $modulosM = $this->User->query("SELECT modulos.modulo, modulos.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join modulos on modulos.id = recursos.modulo_id where username = '".$this->Session->read('nombreusuario')."' and privilegios.activo = 1 and modulos.activo = 1 and recursos.ubicacion = 'Menu' group by modulos.id ORDER BY  modulos.modulo DESC");
        $modelosM = $this->User->query("SELECT  modulos.id, recursos.modelo,recursos.nombre , privilegios.activo, privilegios.leer, privilegios.adicionar, privilegios.editar, privilegios.borrar FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join modulos on modulos.id = recursos.modulo_id where username = '".$this->Session->read('nombreusuario')."' and privilegios.activo = 1 and modulos.activo = 1 and recursos.ubicacion = 'Menu' and recursos.tipo='pantalla' ORDER BY  recursos.nombre ASC");
        $informe = $this->User->query("SELECT  modulos.id, recursos.modelo,recursos.nombre FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join modulos on modulos.id = recursos.modulo_id where username = '".$this->Session->read('nombreusuario')."' and privilegios.activo = 1 and modulos.activo = 1 and recursos.ubicacion = 'Menu' and recursos.tipo='informe' ORDER BY  recursos.nombre ASC");
        $modulosA = $this->User->query("SELECT  modulos.modulo, modulos.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join modulos on modulos.id = recursos.modulo_id where username = '".$this->Session->read('nombreusuario')."' and privilegios.activo = 1 and modulos.activo = 1 and recursos.ubicacion = 'Admin' group by modulos.id ORDER BY  modulos.modulo ASC");
        $modelosA = $this->User->query("SELECT  modulos.id, recursos.modelo,recursos.nombre , privilegios.activo, privilegios.leer, privilegios.adicionar, privilegios.editar, privilegios.borrar FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join modulos on modulos.id = recursos.modulo_id where username = '".$this->Session->read('nombreusuario')."' and privilegios.activo = 1 and modulos.activo = 1 and recursos.ubicacion = 'Admin' and recursos.tipo='pantalla' ORDER BY  recursos.nombre ASC");
        $this->set(compact('modulosM','modelosM','informe','partidacuadrada','modulosA','modelosA'));
        $this->Session->write('modulosApp', $modulosM);
        $this->Session->write('modelosApp', $modelosM);
        $this->Session->write('informesApp', $informe);
    }

    public function ValidarUsuario($controlador, $tabla, $pantalla) {
        /*VERIFICA QUE EL ESTADO DEL USUARIO SEA ACTIVO=1
         DE LO CONTRARIO CIERRA LA SESSION */
        $username = $this->Session->read('nombreusuario');
        $result = $this->$controlador->query("SELECT activo FROM users WHERE username = '$username' AND activo = 0");
        if (count($result) > 0 ) {
            $this->Session->destroy();
            $this->Auth->logout();
            $this->redirect(array("controller" => "users", 'action' => 'login'));
        }
        /*VERIFICA QUE EL PERFIL DEL USUARIO SE ACTIVO = 1
            DE LO CONTRARIO CIERRA LA SESSION*/
        $sql_perf = $this->$controlador->query("SELECT g.activo FROM users u INNER JOIN perfiles g ON g.id = u.perfile_id WHERE u.username = '".$username."' AND g.activo=0");
        if(count($sql_perf)>0){
            $this->Session->destroy();
            $this->Auth->logout();
            $this->redirect(array("controller" => "users", 'action' => 'login'));
        }

        /*======================================================================*/

        if ($pantalla == "index" || $pantalla == "view")
        {
            $result = $this->$controlador->query("SELECT users.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and privilegios.leer = 1 and privilegios.activo = 1");
        }elseif ($pantalla == "add")
        {
            $result = $this->$controlador->query("SELECT users.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and privilegios.adicionar = 1 and privilegios.activo = 1");
        }elseif ($pantalla == "edit")
        {
            $result = $this->$controlador->query("SELECT users.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and privilegios.editar = 1 and privilegios.activo = 1");
        }elseif ($pantalla == "delete")
        {
            $result = $this->$controlador->query("SELECT users.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and privilegios.borrar = 1 and privilegios.activo = 1");
            if (count($result)<=0)
            {
                return false;
            }else{
                return true;
            }
        }else
        {
            return $this->flash(__('A surgido un inconveniente, póngase en contacto con el administrador del sistema.<br /> Verifique los parámetros colocados en el controlador.'), array('controller'=>'pages','action' => 'home'));
        }

        if (count($result)<=0)
        {
            echo "
                <style>
                    .cont{
                        height: 500%;
                        width: 100%;
                        position: absolute;
                        top: 0;
                        left: 0;
                        display: block;
                        background-color: #D8D8D8;
                        z-index: 150;
                    }
                    .cont .mensaje{
                        height: 100px;
                        width: 600px;
                        margin: 150px auto;
                        font-size: 2em;
                        border: solid 15px #4E8CCF;
                        text-align: justify;
                        padding: 0.5em;
                        box-shadow: 1px 1px 10px #4E8CCF, 2px 2px 10px #1C4673;
                        text-shadow: 1px 1px 1px rgba(188, 210, 221, 0.24);
                    }
                </style>
                <div class='cont'>
                    <div class='mensaje'>
                        <p>No cuenta con los privilegios necesarios para acceder a esta pantalla.
                        Intente con otro usuario ó póngase en contacto con el administrador.</p>
                    </div>
                </div>
                <script>
                    setInterval(function(){
                        window.history.back();
                    },5500);
                </script>
                ";
        }
    }

    public function validarBotones($controlador, $tabla, $boton){
        $result = $this->$controlador->query("SELECT users.id FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join detrecursos on detrecursos.recurso_id = recursos.id inner join detprivilegios on detprivilegios.detrecurso_id = detrecursos.id  where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and detrecursos.funcion = '".$boton."' and detprivilegios.permitir = 1 and privilegios.activo = 1 and detrecursos.activo = 1");
        if (count($result)<=0)
        {
            echo "
                <style>
                    .cont{
                        height: 500%;
                        width: 100%;
                        position: absolute;
                        top: 0;
                        left: 0;
                        display: block;
                        background-color: #D8D8D8;
                        z-index: 150;
                    }
                    .cont .mensaje{
                        height: 100px;
                        width: 600px;
                        margin: 150px auto;
                        font-size: 2em;
                        border: solid 15px #4E8CCF;
                        text-align: justify;
                        padding: 0.5em;
                        box-shadow: 1px 1px 10px #4E8CCF, 2px 2px 10px #1C4673;
                        text-shadow: 1px 1px 1px rgba(188, 210, 221, 0.24);
                    }
                </style>
                <div class='cont'>
                    <div class='mensaje'>
                        <p>No cuenta con los privilegios necesarios para realizar esta acción.
                        Intente con otro usuario ó póngase en contacto con el administrador.</p>
                    </div>
                </div>
                <script>
                    setInterval(function(){
                        window.history.back();
                    },5500);
                </script>
                ";
            return false;
        }else{
            return true;
        }
    }

    public function mostrarBotones($controlador, $tabla) {
        $result = $result = $this->$controlador->query("SELECT users.id, detrecursos.nombre, detrecursos.funcion FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join detrecursos on detrecursos.recurso_id = recursos.id inner join detprivilegios on detprivilegios.detrecurso_id = detrecursos.id and detprivilegios.privilegio_id = privilegios.id  where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and detprivilegios.permitir = 1 and privilegios.activo = 1 and detrecursos.activo = 1");
        $arrayBotones = array();
        foreach ($result as $key => $value) {
            $arrayBotones[$value['detrecursos']['funcion']] = $value['detrecursos']['nombre'];
        }
        if(count($result)>0)
            return $arrayBotones;
        else
            return false;
    }

    //Funcion para cargar las listas desplegables de acuerdo a la tabla privilegiosregistrosrecurso
    public function cargarListado($controlador,$aMostrarLista,$condiciones=false){
        /*
         * Con la $tabla puedo buscar el id de recurso.
         * con $aMostrarlista se busca el campo asociado al recurso en la tabla registrosrecursos y su id
         * verifico que permisos tiene el usuario con user_id y registrorecurso_id, y si esta activo, utilizando la tabla privilegiosregistrosrecursos
         * Utilizando el campo tabla y campo de registrosrecursos y el campo registro_id de la tabla det_privilegiosregistrosrecursos, y verificando si el campo permitir es igual a 1,
         * asociada por medio de privilegiosregistrosrecurso_id a la tabla privilegiosregistrosrecursos, se realiza un consulta.
         * Se recurre el resultado y se almacena en una variable con puesta por un array key = id, valor = campo
         * y retornar dicha cadena.
         *
         */
        $lista      = array();
        $recurso    = strtolower($controlador)."s";
        $idRecurso  = $this->$controlador->query("SELECT id FROM recursos WHERE modelo = '".$recurso."'");
        if(count($idRecurso)>0) {
            $registros = $this->$controlador->query("SELECT id, campo FROM registrosrecursos WHERE activo = 1 AND recurso_id =" . $idRecurso[0]['recursos']['id'] . " AND tabla='" . $aMostrarLista . "'");
            if(count($registros)>0) {
                $privilegiosUsuarios = $this->$controlador->query("SELECT det.registro_id FROM det_privilegiosregistrosrecursos as det INNER JOIN privilegiosregistrosrecursos as pri on pri.id = det.privilegiosregistrosrecurso_id AND pri.registrosrecurso_id = det.det_registrosrecurso_id INNER JOIN users as u on u.id = pri.user_id  WHERE u.username = '" . $this->Session->read('nombreusuario') . "' AND pri.registrosrecurso_id = " . $registros[0]['registrosrecursos']['id'] . " AND pri.activo = 1 AND det.permitir = 1");
                $where = "";
                if (count($privilegiosUsuarios) > 0) {
                    foreach ($privilegiosUsuarios as $k => $v) {
                        if ($k == 0) {
                            $where = "id = " . $v['det']['registro_id'];
                        } else {
                            $where = $where . " OR id = " . $v['det']['registro_id'];
                        }
                        if ($condiciones != false) {
                            $where = $where . $condiciones;
                        }
                    }
                    $listaId = $this->$controlador->query("SELECT id, " . $registros[0]['registrosrecursos']['campo'] . " FROM " . $aMostrarLista . " WHERE " . $where);
                    $lista = array();
                    foreach ($listaId as $k => $v) {
                        $lista[$v[$aMostrarLista]['id']] = $v[$aMostrarLista][$registros[0]['registrosrecursos']['campo']];
                    }
                } else {
                    $lista = array();
                }
            }
        }
        return $lista;
    }

    //Funcion para mostrar los datos seleccionados de acuerdo a los permisos.
    public function mostrarPorPrivilegios($controlador, $index = null) {
        /*
         *
         *Generar un arreglo con otro areglo interno con "AND" como key, "AND"=>array(condiciones);
         *
         * */
        $recurso                = strtolower($controlador)."s";
        $idRecurso              = $this->$controlador->query("SELECT id FROM recursos WHERE modelo = '".$recurso."'");
        $privilegiosUsuarios    = $this->$controlador->query("SELECT det.registro_id, pri.solopropietario, re.tabla FROM det_privilegiosregistrosrecursos as det INNER JOIN privilegiosregistrosrecursos as pri on pri.id = det.privilegiosregistrosrecurso_id AND pri.registrosrecurso_id = det.det_registrosrecurso_id INNER JOIN users as u on u.id = pri.user_id INNER JOIN registrosrecursos as re on re.id = pri.registrosrecurso_id WHERE u.username = '".$this->Session->read('nombreusuario')."' AND pri.activo = 1 AND det.permitir = 1 AND re.activo = 1 AND re.recurso_id =".$idRecurso[0]['recursos']['id']);
        /*
         * Se crea una session para poder verificar si solo se permite ver los registros del mismo usuario, en el caso de necesitarse en otra pantalla
         * como en el caso de el index de partidas con partidas temporales.
         * Se inicializa con false y dentro del for se realiza una comparacion, si en algun registro solo se permiten las porpias
         * se coloca la session a true
         *
        */
        $this->Session->write($controlador.'-propios',false);
        $where['AND'] = array();
        if (count($idRecurso) > 0) {
            if (count($privilegiosUsuarios) > 0) {
                foreach ($privilegiosUsuarios as $k => $v) {
                    if($v['pri']['solopropietario']){
                        $this->Session->write($controlador.'-propios',true);
                    }
                    $tablaId = substr($v['re']['tabla'], 0, -1) . "_id";
                    if ($v['pri']['solopropietario'] == true) {
                        if (count($where['AND']) > 0) {
                            if(isset($where['AND'][count($where['AND'])-1]['OR'][0]['AND'][$controlador . "." . $tablaId])) {
                                $where['AND'][count($where['AND']) - 1]['OR'][count($where['AND'][count($where['AND']) - 1]['OR'])]['AND'][$controlador . '.usuario'] = $this->Session->read('nombreusuario');
                                $where['AND'][count($where['AND']) - 1]['OR'][count($where['AND'][count($where['AND']) - 1]['OR'])-1]['AND'][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                            }else{
                                $where['AND'][count($where['AND'])]['OR'][count($where['AND'][count($where['AND'])]['OR'])]['AND'][$controlador . '.usuario'] = $this->Session->read('nombreusuario');
                                $where['AND'][count($where['AND'])-1]['OR'][count($where['AND'][count($where['AND'])]['OR'])]['AND'][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                            }
                        } else {
                            $where['AND'][0]['OR'][0]['AND'][$controlador . '.usuario'] = $this->Session->read('nombreusuario');
                            $where['AND'][0]['OR'][0]['AND'][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                        }
                    } else {
                        if (isset($where['AND']) && count($where['AND']) > 0) {
                            if(isset($where['AND'][count($where['AND'])-1]['OR'][0][$controlador . "." . $tablaId])) {
                                $where['AND'][count($where['AND']) - 1]['OR'][count($where['AND'][count($where['AND'])-1]['OR'])][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                            }else{
                                $where['AND'][count($where['AND'])]['OR'][count($where['AND'][count($where['AND'])]['OR'])][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                            }
                        } else {
                            $where['AND'][0]['OR'][0][$controlador . "." . $tablaId] = $v['det']['registro_id'];
                        }
                    }
                }
            } else {
                $where['AND'] = array($controlador . '.id' => 0);
                $this->Session->write($controlador.'-propios',true);
            }
        } else {
            $where['AND'] = array($controlador . '.id' => 0);
            $this->Session->write($controlador.'-propios',true);
        }

        if(empty($index)) {
            return $where;
        }elseif($index>0)
        {
            $r = array();
            if(count($where['AND'])>0) {
                $r = $this->$controlador->find('list', array('conditions' => array('id' => $index, $where)));
            }
            if(count($r)<=0){
                echo "
                <style>
                    .cont{
                        height: 500%;
                        width: 100%;
                        position: absolute;
                        top: 0;
                        left: 0;
                        display: block;
                        background-color: #D8D8D8;
                        z-index: 150;
                    }
                    .cont .mensaje{
                        height: 100px;
                        width: 600px;
                        margin: 150px auto;
                        font-size: 2em;
                        border: solid 15px #4E8CCF;
                        text-align: justify;
                        padding: 0.5em;
                        box-shadow: 1px 1px 10px #4E8CCF, 2px 2px 10px #1C4673;
                        text-shadow: 1px 1px 1px rgba(188, 210, 221, 0.24);
                    }
                </style>
                <div class='cont'>
                    <div class='mensaje'>
                        <p>No cuenta con los privilegios necesarios para realizar esta acción.
                        Intente con otro usuario ó póngase en contacto con el administrador.</p>
                    </div>
                </div>
                <script>
                    setInterval(function(){
                        window.history.back();
                    },5500);
                </script>
                ";
            }
            return false;
        }else{
            return false;
        }
    }
    /*----------------------------------FIRMAS-------------------------------------*/
    public function mostrarFirmas($controlador,$tabla,$funcion,$pdfCSS=null) {
        //SELECT users.id, detrecursos.nombre, detrecursos.funcion FROM users inner join perfiles on perfiles.id = users.perfile_id inner join privilegios on privilegios.perfile_id = perfiles.id inner join recursos on privilegios.recurso_id = recursos.id inner join detrecursos on detrecursos.recurso_id = recursos.id inner join detprivilegios on detprivilegios.detrecurso_id = detrecursos.id and detprivilegios.privilegio_id = privilegios.id  where username = '".$this->Session->read('nombreusuario')."' and recursos.modelo='".$tabla."' and detprivilegios.permitir = 1 and privilegios.activo = 1 and detrecursos.activo = 1

        $datos = $this->$controlador->query("SELECT f.nombre as nombre, f.firma as firma FROM recursos r INNER JOIN detrecursofirmas drf on r.id = drf.recurso_id INNER JOIN firmas f on drf.firma_id = f.id WHERE r.modelo = '$tabla' AND drf.funcion = '$funcion' AND drf.permitir = 1 AND f.activo = 1 ORDER BY drf.orden ASC");
        $arrayFirmas=array();
        $i=0;
        $b1=0;
        $b2=0;
        $b3=0;
        $b4=0;
        foreach($datos as $ind => $row){
            $arrayFirmas[$i]['nombre']=$row['f']['nombre'];
            $arrayFirmas[$i]['firma']=$row['f']['firma'];
            $i++;
        }
        $con_f = count($arrayFirmas);
        $dif = 0;
        $html = "";
        if($pdfCSS==null) {
            $html .= "<style>
                         .contfrm{
                            display:none;
                         }
                         @media print{
                            /*ESTA LINEA ES NECESARIA PARA MOSTRAR LAS FIRMAS AL IMPRIMIR EL INFORME*/
                            .contfrm{
                                display:block;
                            }

                        }
                      </style>";
        }

        $html .= "<div class='contfrm' style='width: 100%;margin-top:60px;'>";
        for($x=0;$x<count($arrayFirmas);$x++){
            if($con_f>=4){
                //SI ENTRA EN ESTA CONDICION SE ASIGNARA LA CLASE
                if($b4==0){
                    $html .= "<div class='frm4_1' style='display: inline-block;width: 20%;font-size: 11px;margin-left: 2%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }else{
                    $html .= "<div class='frm4_2' style='display: inline-block;width: 20%;font-size: 11px;margin-left: 5%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }
                $b4+=1;
                if($b4==4){
                    $con_f-=4;
                    $b4=0;
                    $html .= ($funcion == "savepdf")?"<br>":"<br><br><br>";
                }
            }else if($con_f==3){
                //SI ENTRA EN ESTA CONDICION SE ASIGNARA LA CLASE
                if($b3==0){
                    $html .= "<div class='frm3_1' style='display: inline-block;width: 20%;font-size: 11px;margin-left: 8%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }else{
                    $html .= "<div class='frm3_2' style='display: inline-block;width: 20%;font-size: 11px;margin-left: 10%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }
                $b3+=1;
                if($b3==3){
                    $con_f-=3;
                    $b3=0;
                    $html .= ($funcion == "savepdf")?"<br>":"<br><br><br>";
                }
            }else if($con_f==2){
                //SI ENTRA EN ESTA CONDICION SE ASIGNARA LA CLASE
                if($b2==0){
                    $html .= "<div class='frm2_1' style='display: inline-block;width: 25%;font-size: 11px;margin-left: 20%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }else{
                    $html .= "<div class='frm2_2' style='display: inline-block;width: 25%;font-size: 11px;margin-left: 15%;vertical-align: top;'>";
                    $html .= "F.<hr style='width: 220px;height: 0px;color:black;'  align='right'>";
                    $html .= $arrayFirmas[$x]['firma']."</div>";
                }
                $b2+=1;
                if($b2==2){
                    $con_f-=2;
                    $b2=0;
                    $html .= ($funcion == "savepdf")?"<br>":"<br><br><br>";
                }
            }else{
                //ULTIMA FIRMA
                $html .= "<div class='frm1' style='display: inline-block;vertical-align: top;width: 50%;font-size: 11px;margin-left: 30%;'>";
                $html .= "F.<hr style='width: 270px;height: 0px;color:black;'  align='right'>";
                $html .= $arrayFirmas[$x]['firma']."</div>";
                $con_f -=1;
            }
        }
        $html .= "</div>";

        if(count($datos)>0){
            return $html;
        }else{
            return false;
        }
    }
    /***
     * Author: Manuel Anzora
     * create: 26-01-2020
     * description: Metodo para obtener torneos que el usuario puede visualizar
     * return: Listado de id de los torneos
     ****/
    public function getTorneos(){
        $this->loadModel("User");
        $username = $this->Session->read('nombreusuario');
        //Informacion de usuario logeado
        $user = $this->User->find("all", [
            "fields"=>[
                "User.id", "User.perfile_id"
            ],
            "conditions"=>[
                "User.username"=>$username
            ],
            "limit"=>1
        ]);
        $detuser = $this->User->Detusuario->find("all", [
            "fields"=>[
                "Detusuario.torneo_id"
            ],
            "conditions"=>[
                "Detusuario.user_id"=>$user[0]["User"]["id"]
            ]
        ]);
        $torneos = [];
        foreach ($detuser as $det){
            $torneos[$det["Detusuario"]["torneo_id"]] = $det["Detusuario"]["torneo_id"];
        }
        return $torneos;
    }
    public function getPerfil($user){
        $this->loadModel("User");
        $info = $this->User->find("first",[
            "fields"=>["User.perfile_id"],
            "conditions"=>[
                "User.username"=>$user
            ]
        ]);
        return (isset($info["User"]["perfile_id"]))?$info["User"]["perfile_id"]:0;
    }
}
