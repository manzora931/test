<?php
App::uses('AppController', 'Controller');
/**
 * Departamentos Controller
 *
 * @property Departamento $Departamento
 * @property PaginatorComponent $Paginator
 */
class DepartamentosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public function lista_deptos(){
        $pais = $_POST['pais'];
        $sql = $this->Institucion->query("SELECT id, nombre as nombre FROM departamentos WHERE paise_id = " . $pais . " AND activa = 1 ORDER BY nombre");

        $paises = "<option value=''> Seleccionar </option>";
        foreach($sql as $row){
            $paises.= "<option value='".$row['departamentos']['id']."'>".$row['departamentos']['nombre']."</option>";
        }

        echo $paises;
        $this->autoRender=false;
    }

}
