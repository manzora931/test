<?php
App::uses('AppController', 'Controller');
/**
 * Vtrasladofondos Controller
 *
 * @property Vtrasladofondo $VTrasladofondo
 * @property PaginatorComponent $Paginator
 */
class VtrasladofondosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($band = null) {
		$this->ValidarUsuario("Vtrasladofondo", "vtrasladofondos", "index");
		$this->Vtrasladofondo->recursive = 0;
		if($band==1){
		    $this->Paginator->settings=['limit'=>1000000];
        }else{
            $this->Paginator->settings=['limit'=>20];
        }

        $data = $this->Paginator->paginate("Vtrasladofondo");
		include "busqueda/vtrasladofondos.php";
		$this->set('vtrasladofondos', $data);


		$proyectos = $this->Vtrasladofondo->Proyecto->find("list");
		$this->loadModel("Actividade");
        $this->Actividade->recursive=-1;
		$sqlAct = $this->Actividade->find("all",['fields'=>['Actividade.id','Actividade.nombre']]);
        $actividades=[];
		foreach ($sqlAct as $item){
            $actividades[$item['Actividade']['id']]=$item['Actividade']['nombre'];
        }
		$this->set(compact("proyectos",'actividades'));
	}
    public function vertodos() {
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[vtrasladofondos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;

    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

    public function imprimir(){
        $this->layout=false;
        $this->index(1);
        $this->loadModel("Organizacion");
        $this->Organizacion->recursive=-1;
        $info = $this->Organizacion->find("all",[
            'conditions'=>[
                'Organizacion.id'=>1
            ],
            'fields'=>[
                'Organizacion.razonsocial'
            ]
        ]);
        $this->set(compact("info"));
    }

	public function view($id = null) {
		$this->ValidarUsuario("Vtrasladofondo", "vtrasladofondos", "view");
		if (!$this->Vtrasladofondo->exists($id)) {
			throw new NotFoundException(__('Invalid trasladofondo'));
		}
		$options = array('conditions' => array('Vtrasladofondo.' . $this->Trasladofondo->primaryKey => $id));
		$this->set('vtrasladofondo', $this->Vtrasladofondo->find('first', $options));
	}


	}
