<?php
App::uses('AppController', 'Controller');
/**
 * Perfiles Controller
 *
 * @property Perfile $Perfile
 * @property PaginatorComponent $Paginator
 * @property nComponent $n
 */
class PerfilesController extends AppController
{


	var $name = 'Perfiles';
	var $components = array('Acl', 'Paginator', 'Session');
	var $helpers = array('Html', 'Form');
	function index() {
		$this->ValidarUsuario("Perfile", "perfiles", "index");
		$this->Perfile->recursive = 0;
		if ($this->Session->check('tabla[perfiles].activo')) {
			$this->Paginator->settings = array('order'=>array('Perfile.perfil'=>'asc'));
		}else{
			$this->Paginator->settings = array('conditions' => array('Perfile.activo' => 1),'order'=>array('Perfile.perfil'=>'asc'));
		}

		include 'busqueda/perfiles.php';
		$data = $this->Paginator->paginate('Perfile');

		$this->set('perfiles', $data);
	}

	public function vertodos()
	{

		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[perfiles]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;

	}

	function auto_completado2() {
		$this->set('perfiles', $this->Perfile->find('all', array(
			'conditions' => array('OR'=>array(array('Perfile.name LIKE '=> '%'.$this->request->query['term'].'%')),
				'AND'=>array('Perfile.activo'=>1)
			),
			'fields' => array('id','name'), 'limit'=>10
		)));
		$this->layout = 'ajax';
	}

	function getDato($id = null) {
		$consult =& $this->Perfile;
		$sql = "SELECT id,name FROM perfiles where id = '$id'";
		$result = $consult->query($sql);
		if(count($result)>0){
			$this->set('perfile', $result);
			//esta session se borra en el add
		}else{
			$this->set('perfile', "");
		}
		$this->layout = 'ajax';
	}

	function view($id = null) {
		$this->ValidarUsuario("Perfile", "perfiles", "view");
		//if($this->Acl->check($this->Session->read('User.username'), 'admin','read')){
		if (!$id) {
			$this->flash(__('No valido Group', true), array('action'=>'index'));
		}
		$this->set('perfile', $this->Perfile->read(null, $id));
		/*}
		else{
			$this->flash(__($this->Session->read('User.username').' no tiene permisos para entrar a esta opci�n', true),'../');
		}*/
	}

	function add() {
		$this->ValidarUsuario("Perfile", "perfiles", "add");
		if (!empty($this->data)) {
			$this->Perfile->create();

			$this->request->data['Perfile']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Perfile']['created'] = date("Y-m-d H:i:s");
			$this->request->data['Perfile']['modified'] = 0;

			if ($this->Perfile->save($this->data)) {
				$id = $this->Perfile->id;
				$this->Session->write("perfil_save",1);
				$this->redirect(array('action' => 'view',$id));
			}
		}
		/*}
		else{
			$this->flash(__($this->Session->read('User.username').' no tiene permisos para entrar a esta opci�n', true),'../');
		}*/
	}

	function edit($id = null) {
		$this->ValidarUsuario("Perfile", "perfiles", "edit");
		//if($this->Acl->check($this->Session->read('User.username'), 'admin', 'update')){

		if (!$id && empty($this->data)) {

			$this->flash(__('No valido Group', true), array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$this->request->data['Perfile']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Perfile']['modified'] = date("Y-m-d H:i:s");
			if ($this->Perfile->save($this->data)) {
				$this->Session->write("perfil_save",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->write("perfil_save",0);
				$this->redirect(array('action' => 'view',$id));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Perfile->read(null, $id);
		}
		/*}
		else{
			$this->flash(__($this->Session->read('User.username').' no tiene permisos para entrar a esta opci�n', true),'../');
		}	*/
	}

	public function clonar($id){
		$this->ValidarUsuario("Perfile", "perfiles", "edit");
		if (!$id && empty($this->data)) {
			$this->flash(__('No valido Group', true), array('action'=>'index'));
		}
		if (!empty($this->data)) {
			$this->request->data['Perfile']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Perfile']['modified'] = date("Y-m-d H:i:s");
			if ($this->Perfile->save($this->data)) {
				$this->Session->write("perfil_save",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->write("perfil_save",0);
				$this->redirect(array('action' => 'view',$id));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Perfile->read(null, $id);
		}
	}
	//funcion que valida que no se repita un perfil
	public function val_per() {
		$valor=$_POST['val'];
		$id =$_POST['id'];

		if($id != 0){
			$datos=$this->Perfile->query("SELECT perfil FROM perfiles WHERE perfil='$valor' AND id !=".$id);
		}else{
			$datos=$this->Perfile->query("SELECT perfil FROM perfiles WHERE perfil='$valor'");
		}
		if(count($datos)>0){
			echo "error";
		}else{
			echo "ok";
		}
		$this->autoRender=false;
	}
    public function valPri() {
        $valor=$_POST['dato'];
        $id =$_POST['id'];

        if($id != 0){
            $datos=$this->Perfile->query("SELECT perfil FROM perfiles WHERE admin='$valor' AND id !=".$id);
        }else{
            $datos=$this->Perfile->query("SELECT perfil FROM perfiles WHERE admin='$valor'");
        }
        if(count($datos)>0){
            $data="error";
        }else{
            $data="ok";
        }
        echo json_encode($data);
        $this->autoRender=false;
    }

}