<?php
App::uses('AppController', 'Controller');
/**
 * Etapasxtorneos Controller
 *
 * @property Etapasxtorneo $Etapasxtorneo
 * @property PaginatorComponent $Paginator
 */
class EtapasxtorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Etapasxtorneo", "etapasxtorneos", "index");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->Paginator->settings = array('conditions'=>['Etapasxtorneo.torneo_id IN'=>$torneos], 'order'=>array('Etapasxtorneo.torneo_id'=>'asc'));
            }else{
                $this->Paginator->settings = array('conditions'=>['Etapasxtorneo.torneo_id'=>$torneos], 'order'=>array('Etapasxtorneo.torneo_id'=>'asc'));
            }
        }else{
            $this->Paginator->settings = array('order'=>array('Etapasxtorneo.torneo_id'=>'asc'));
        }
        $this->Etapasxtorneo->recursive = 0;
        include 'busqueda/etapasxtorneo.php';
        $data = $this->Paginator->paginate('Etapasxtorneo');
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $torneos = $this->Etapasxtorneo->Torneo->find('list',["conditions"=>["activo"=>1, 'Torneo.id IN'=>$torneos]]);
            }else{
                $torneos = $this->Etapasxtorneo->Torneo->find('list',["conditions"=>["activo"=>1, 'Torneo.id'=>$torneos]]);
            }
        }else{
            $torneos = $this->Etapasxtorneo->Torneo->find('list',["conditions"=>["activo"=>1]]);
        }

        $etapas = $this->Etapasxtorneo->Etapa->find('list',["conditions"=>["activo"=>1]]);
        $this->set(compact("torneos", "etapas"));
        $this->set('etapasxtorneos', $data);
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[etapasxtorneos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Etapasxtorneo", "etapasxtorneos", "view");
		if (!$this->Etapasxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid etapasxtorneo'));
		}
		$options = array('conditions' => array('Etapasxtorneo.' . $this->Etapasxtorneo->primaryKey => $id));
		$this->set('etapasxtorneo', $this->Etapasxtorneo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Etapasxtorneo", "etapasxtorneos", "add");
		if ($this->request->is('post')) {

		    $this->request->data["Etapasxtorneo"]["usuario"] = $this->Session->read('nombreusuario');
		    $this->request->data["Etapasxtorneo"]["created"] = date("Y-m-d H:i:s");
		    $this->request->data["Etapasxtorneo"]["modified"] = 0;
		    $this->request->data["Etapasxtorneo"]["usuariomodif"] = null;
			$this->Etapasxtorneo->create();
			if ($this->Etapasxtorneo->save($this->request->data)) {
			    if($this->request->data["Etapasxtorneo"]["actual"]==1){
			        //Verifica que las demas etapas del torneo no sean actuales(campo actual = 0)
                    $torneo = $this->request->data["Etapasxtorneo"]["torneo_id"];
                    $id     = $this->Etapasxtorneo->id;
                    $this->Etapasxtorneo->query("UPDATE etapasxtorneos set actual = 0 WHERE torneo_id = ".$torneo.
                    " AND id != ".$id);
                }
                $this->Session->write("etapaxtorneo_save",1);
                return $this->redirect(array('action' => 'view',$this->Etapasxtorneo->id));
			} else {
				$this->Session->setFlash(__('The etapasxtorneo could not be saved. Please, try again.'));
			}
		}
        $torneos = $this->Etapasxtorneo->Torneo->find('list',["conditions"=>["activo"=>1]]);
        $etapas = $this->Etapasxtorneo->Etapa->find('list',["conditions"=>["activo"=>1]]);
		$this->set(compact('torneos', 'etapas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Etapasxtorneo", "etapasxtorneos", "edit");
		if (!$this->Etapasxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid etapasxtorneo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data["Etapasxtorneo"]["usuariomodif"] = $this->Session->read('nombreusuario');
            $this->request->data["Etapasxtorneo"]["modified"] = date("Y-m-d H:i:s");
			if ($this->Etapasxtorneo->save($this->request->data)) {
                if($this->request->data["Etapasxtorneo"]["actual"]==1){
                    //Verifica que las demas etapas del torneo no sean actuales(campo actual = 0)
                    $torneo = $this->request->data["Etapasxtorneo"]["torneo_id"];
                    $this->Etapasxtorneo->query("UPDATE etapasxtorneos set actual = 0 WHERE torneo_id = ".$torneo.
                        " AND id != ".$id);
                }
                $this->Session->write("etapaxtorneo_save",1);
                return $this->redirect(array('action' => 'view',$this->Etapasxtorneo->id));
			} else {
				$this->Session->setFlash(__('The etapasxtorneo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Etapasxtorneo.' . $this->Etapasxtorneo->primaryKey => $id));
			$this->request->data = $this->Etapasxtorneo->find('first', $options);
		}
        $torneos = $this->Etapasxtorneo->Torneo->find('list',["conditions"=>["activo"=>1]]);
        $etapas = $this->Etapasxtorneo->Etapa->find('list',["conditions"=>["activo"=>1]]);
		$this->set(compact('torneos', 'etapas'));
	}
	/**
	 * Author: Manuel Anzora
     * date: 26-05-2019
     * description: Metodo para verificar que no exista otra etapa actual de un torneo
     *
     ***/
    public function valActual(){
        $this->autoRender=false;
	    $torneo = $_POST["torneo"];
	    $datos = $this->Etapasxtorneo->find("all",[
	        "fields"=>[
	            "Etapasxtorneo.id"
            ],
	        "conditions"=>[
	            "Etapasxtorneo.torneo_id"=>$torneo,
                "Etapasxtorneo.actual"=>1
            ]
        ]);
	    $band=0;
	    if(count($datos)>0)
	        $band=1;

	    echo $band;
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Etapasxtorneo", "etapasxtorneos", "delete");
		if ($delete == true) {
			$this->Etapasxtorneo->id = $id;
			if (!$this->Etapasxtorneo->exists()) {
				throw new NotFoundException(__('Invalid etapasxtorneo'));
			}
			if ($this->Etapasxtorneo->delete()) {
                $_SESSION["delete"]=1;
                $this->redirect(array('action' => 'index'));
			}
        }else{
            $_SESSION["delete-no-priv"]=1;
            $this->redirect(array('action' => 'index'));
        }
	}
}
