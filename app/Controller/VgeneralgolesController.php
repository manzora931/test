<?php
App::uses('AppController', 'Controller');
/**
 * Vgeneralgoles Controller
 *
 * @property Vgeneralgole $Vgeneralgole
 * @property PaginatorComponent $Paginator
 */
class VgeneralgolesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Vgeneralgole", "vgeneralgoles", "index");
		$this->Vgeneralgole->recursive = 0;
        $this->loadModel("Torneo");
        $this->loadModel("Etapa");
        $this->loadModel("Equipo");
        $this->loadModel("Jugadore");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5) {
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $data = $this->Vgeneralgole->find("all", [
                    "conditions"=>[
                        "Vgeneralgole.torneo_id IN"=>$torneos
                    ]
                ]);
                include "busqueda/vgeneralgoles.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id IN"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }else{
                $data = $this->Vgeneralgole->find("all", [
                    "conditions"=>[
                        "Vgeneralgole.torneo_id"=>$torneos
                    ]
                ]);
                include "busqueda/vgeneralgoles.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }
        }else{
            $data = $this->Vgeneralgole->find("all");
            include "busqueda/vgeneralgoles.php";
            $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
        }
		$this->set('vgeneralgoles', $data);
		$equipos=$this->Equipo->find("list",["order"=>["Equipo.nombrecorto"]]);
		$jornadas=$this->Etapa->find("list",["conditions"=>["Etapa.activo"=>1],"order"=>["Etapa.etapa"]]);
        $jugadores=$this->Jugadore->find("list",["fields"=>["Jugadore.id","Jugadore.nombre_jugador"]]);
		$this->set(compact("torneos","jornadas",'equipos', 'jugadores'));

	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[vgeneralgoles]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Vgeneralgole", "vgeneralgoles", "view");
		if (!$this->Vgeneralgole->exists($id)) {
			throw new NotFoundException(__('Invalid vgeneralgole'));
		}
		$options = array('conditions' => array('Vgeneralgole.' . $this->Vgeneralgole->primaryKey => $id));
		$this->set('vgeneralgole', $this->Vgeneralgole->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Vgeneralgole", "vgeneralgoles", "add");
		if ($this->request->is('post')) {
			$this->Vgeneralgole->create();
			if ($this->Vgeneralgole->save($this->request->data)) {
				$this->Session->setFlash(__('The vgeneralgole has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vgeneralgole could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Vgeneralgole", "vgeneralgoles", "edit");
		if (!$this->Vgeneralgole->exists($id)) {
			throw new NotFoundException(__('Invalid vgeneralgole'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vgeneralgole->save($this->request->data)) {
				$this->Session->setFlash(__('The vgeneralgole has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vgeneralgole could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Vgeneralgole.' . $this->Vgeneralgole->primaryKey => $id));
			$this->request->data = $this->Vgeneralgole->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Vgeneralgole", "vgeneralgoles", "delete");
		if ($delete == true) {
			$this->Vgeneralgole->id = $id;
			if (!$this->Vgeneralgole->exists()) {
				throw new NotFoundException(__('Invalid vgeneralgole'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Vgeneralgole->delete()) {
					$this->Session->setFlash(__('The vgeneralgole has been deleted.'));
			} else {
				$this->Session->setFlash(__('The vgeneralgole could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
