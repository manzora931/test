<?php
App::uses('AppController', 'Controller');
/**
 * Rangoedades Controller
 *
 * @property Rangoedade $Rangoedade
 * @property PaginatorComponent $Paginator
 */
class RangoedadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Rangoedade", "rangoedades", "index");
        $this->Paginator->settings = array('conditions' => array('Rangoedade.activo >=' => 1), 'order'=>array('Rangoedade.nombre'=>'asc'));
        $this->Rangoedade->recursive = 0;
        include 'busqueda/rangoedades.php';
        $data = $this->Paginator->paginate('Rangoedade');
        $this->set('rangoedades', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[rangoedades]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Rangoedade", "rangoedades", "view");
		if (!$this->Rangoedade->exists($id)) {
			throw new NotFoundException(__('Invalid rangoedade'));
		}
		$options = array('conditions' => array('Rangoedade.' . $this->Rangoedade->primaryKey => $id));
		$this->set('rangoedade', $this->Rangoedade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Rangoedade", "rangoedades", "add");
		if ($this->request->is('post')) {
            $this->request->data['Rangoedade']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Rangoedade']['modified']=0;
			$this->Rangoedade->create();
			if ($this->Rangoedade->save($this->request->data)) {
                $rangoedad_id = $this->Rangoedade->id;
                $this->Session->write('rangoedad_save', 1);
                $this->redirect(['action' => 'view', $rangoedad_id]);
			} else {
				$this->Session->setFlash(__('El rango de edad no ha sido creado. Intentar de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Rangoedade", "rangoedades", "edit");
		if (!$this->Rangoedade->exists($id)) {
			throw new NotFoundException(__('Invalid rangoedade'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Rangoedade']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Rangoedade->save($this->request->data)) {
                $this->Session->write('rangoedad_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('El rango de edad no ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Rangoedade.' . $this->Rangoedade->primaryKey => $id));
			$this->request->data = $this->Rangoedade->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Rangoedade", "rangoedades", "delete");
		if ($delete == true) {
			$this->Rangoedade->id = $id;
			if (!$this->Rangoedade->exists()) {
				throw new NotFoundException(__('Invalid rangoedade'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Rangoedade->delete()) {
					$this->Session->setFlash(__('The rangoedade has been deleted.'));
			} else {
				$this->Session->setFlash(__('The rangoedade could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Rangoedadincidencia');
        $this->Rangoedadincidencia->recursive = -1;
        $rango = $this->Rangoedadincidencia->find('all', [
            'conditions' => [
                'Rangoedadincidencia.rangoedad_id' => $id,
            ]
        ]);

        $data['rangos'] = (count($rango) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
