<?php
App::uses('AppController', 'Controller');
/**
 * Vgeneraljuegos Controller
 *
 * @property Vgeneraljuego $Vgeneraljuego
 * @property PaginatorComponent $Paginator
 */
class VgeneraljuegosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Vgeneraljuego", "vgeneraljuegos", "index");
		$this->Vgeneraljuego->recursive = 0;
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        $torneos = $this->getTorneos();
        $this->loadModel("Torneo");
        $this->loadModel("Etapa");
        $this->loadModel("Equipo");
        $this->loadModel("Estadio");

        if($perfil==3 || $perfil==4 || $perfil==5) {
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->paginate = ["conditions"=>["Vgeneraljuego.torneo_id IN"=>$torneos]];
                $data = $this->Paginator->paginate('Vgeneraljuego');
                include "busqueda/vgeneraljuego.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id IN"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }else{
                $this->paginate = ["conditions"=>["Vgeneraljuego.torneo_id"=>$torneos]];
                $data = $this->Paginator->paginate('Vgeneraljuego');
                include "busqueda/vgeneraljuego.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }
        }else{
            $data = $this->Paginator->paginate('Vgeneraljuego');
            include "busqueda/vgeneraljuego.php";
            $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
        }

		$this->set('vgeneraljuegos', $data);
        $equipos=$this->Equipo->find("list",["order"=>["Equipo.nombrecorto"]]);
        $estadios=$this->Estadio->find("list",["order"=>["Estadio.estadio"]]);

        $jornadas=$this->Etapa->find("list",["conditions"=>["Etapa.activo"=>1],"order"=>["Etapa.etapa"]]);

        $this->set(compact("torneos","jornadas",'equipos','estadios'));
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[vgeneraljuegos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Vgeneraljuego", "vgeneraljuegos", "view");
		if (!$this->Vgeneraljuego->exists($id)) {
			throw new NotFoundException(__('Invalid vgeneraljuego'));
		}
		$options = array('conditions' => array('Vgeneraljuego.' . $this->Vgeneraljuego->primaryKey => $id));
		$this->set('vgeneraljuego', $this->Vgeneraljuego->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Vgeneraljuego", "vgeneraljuegos", "add");
		if ($this->request->is('post')) {
			$this->Vgeneraljuego->create();
			if ($this->Vgeneraljuego->save($this->request->data)) {
				$this->Session->setFlash(__('The vgeneraljuego has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vgeneraljuego could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Vgeneraljuego", "vgeneraljuegos", "edit");
		if (!$this->Vgeneraljuego->exists($id)) {
			throw new NotFoundException(__('Invalid vgeneraljuego'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vgeneraljuego->save($this->request->data)) {
				$this->Session->setFlash(__('The vgeneraljuego has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vgeneraljuego could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Vgeneraljuego.' . $this->Vgeneraljuego->primaryKey => $id));
			$this->request->data = $this->Vgeneraljuego->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Vgeneraljuego", "vgeneraljuegos", "delete");
		if ($delete == true) {
			$this->Vgeneraljuego->id = $id;
			if (!$this->Vgeneraljuego->exists()) {
				throw new NotFoundException(__('Invalid vgeneraljuego'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Vgeneraljuego->delete()) {
					$this->Session->setFlash(__('The vgeneraljuego has been deleted.'));
			} else {
				$this->Session->setFlash(__('The vgeneraljuego could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
