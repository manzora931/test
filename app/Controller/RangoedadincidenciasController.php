<?php
App::uses('AppController', 'Controller');
/**
 * Rangoedadincidencias Controller
 *
 * @property Rangoedadincidencia $Rangoedadincidencia
 * @property PaginatorComponent $Paginator
 */
class RangoedadincidenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Rangoedadincidencia", "rangoedadincidencias", "index");
		$this->Rangoedadincidencia->recursive = 0;
		$this->set('rangoedadincidencias', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Rangoedadincidencia", "rangoedadincidencias", "view");
		if (!$this->Rangoedadincidencia->exists($id)) {
			throw new NotFoundException(__('Invalid rangoedadincidencia'));
		}
		$options = array('conditions' => array('Rangoedadincidencia.' . $this->Rangoedadincidencia->primaryKey => $id));
		$this->set('rangoedadincidencia', $this->Rangoedadincidencia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Rangoedadincidencia", "rangoedadincidencias", "add");
		if ($this->request->is('post')) {
			$this->Rangoedadincidencia->create();
			if ($this->Rangoedadincidencia->save($this->request->data)) {
				$this->Session->setFlash(__('The rangoedadincidencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rangoedadincidencia could not be saved. Please, try again.'));
			}
		}
		$incidencias = $this->Rangoedadincidencia->Incidencium->find('list');
		$rangoedads = $this->Rangoedadincidencia->Rangoedad->find('list');
		$this->set(compact('incidencias', 'rangoedads'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Rangoedadincidencia", "rangoedadincidencias", "edit");
		if (!$this->Rangoedadincidencia->exists($id)) {
			throw new NotFoundException(__('Invalid rangoedadincidencia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Rangoedadincidencia->save($this->request->data)) {
				$this->Session->setFlash(__('The rangoedadincidencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The rangoedadincidencia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Rangoedadincidencia.' . $this->Rangoedadincidencia->primaryKey => $id));
			$this->request->data = $this->Rangoedadincidencia->find('first', $options);
		}
		$incidencias = $this->Rangoedadincidencia->Incidencium->find('list');
		$rangoedads = $this->Rangoedadincidencia->Rangoedad->find('list');
		$this->set(compact('incidencias', 'rangoedads'));
	}

    public function delete() {
        $this->Rangoedadincidencia->id = $_POST['id'];

        if (!$this->Rangoedadincidencia->exists()) {
            $resp['msg'] = 'error1';
        } else {
            if ($this->Rangoedadincidencia->delete()) {
                $resp['msg'] = 'exito';
            } else {
                $resp['msg'] = 'error2';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }
}
