<?php
App::uses('AppController', 'Controller');
/**
 * Vgeneraljugadors Controller
 *
 * @property Vgeneraljugador $Vgeneraljugador
 * @property PaginatorComponent $Paginator
 */
class VgeneraljugadorsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Vgeneraljugador", "vgeneraljugadors", "index");
		$this->Vgeneraljugador->recursive = 0;
        $this->loadModel("Torneo");
        $this->loadModel("Etapa");
        $this->loadModel("Evento");
        $this->loadModel("Jugadore");
		$torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5) {
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->paginate = ["conditions"=>["Vgeneraljugador.torneo_id IN"=>$torneos]];
                $data = $this->Paginator->paginate('Vgeneraljugador');
                include "busqueda/vgeneralgoles.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id IN"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }else{
                $this->paginate = ["conditions"=>["Vgeneraljugador.torneo_id"=>$torneos]];
                $data = $this->Paginator->paginate('Vgeneraljugador');
                include "busqueda/vgeneralgoles.php";
                $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1, "Torneo.id"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }
        }else{
            $torneos=$this->Torneo->find("list",["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
            $data = $this->Paginator->paginate('Vgeneraljugador');
            include "busqueda/vgeneralgoles.php";
        }

		$this->set('vgeneraljugadors', $data);
        $eventos=$this->Evento->find("list",["order"=>["Evento.evento"]]);
        $jornadas=$this->Etapa->find("list",["conditions"=>["Etapa.activo"=>1],"order"=>["Etapa.etapa"]]);
        $jugadores=$this->Jugadore->find("list",["fields"=>["Jugadore.id","Jugadore.nombre_jugador"]]);
        $this->set(compact("torneos","jornadas",'eventos','jugadores'));
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[vgeneraljugadors]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
    public function getDet(){
        $this->autoRender=false;
        $this->loadModel("Detevento");
        $det = $this->Detevento->find("all",[
            "fields"=>["Detevento.id","Detevento.detevento"],
            "conditions"=>["Detevento.evento_id"=>$_POST["evento"]]
        ]);
        $html="<option>Seleccionar</option>";
        foreach ($det as $item):
            $html .= "<option value='".$item["Detevento"]["id"]."'>".$item["Detevento"]["detevento"]."</option>";
        endforeach;
        echo $html;
    }
}
