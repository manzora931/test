<?php
App::uses('AppController', 'Controller');
/**
 * Logproyectos Controller
 *
 * @property Logproyecto $Logproyecto
 * @property PaginatorComponent $Paginator
 */
class LogproyectosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Logproyecto", "logproyectos", "index");
		$this->Logproyecto->recursive = -1;
        $user_institu = $this->getInst();
        $perf = $this->get_perfil();

        if($perf==1){
            $this->Paginator->settings = [
                'fields' => ['Logproyecto.*', 'Proyecto.*', 'Institucion.*'],
                'joins'=>[
                    [
                        'table'=>'proyectos', 'alias'=>'Proyecto', 'type'=>'INNER',
                        'conditions'=>[ 'Proyecto.id = Logproyecto.proyecto_id' ]
                    ],
                    [
                        'table'=>'institucions', 'alias'=>'Institucion', 'type'=>'INNER',
                        'conditions'=>[ 'Institucion.id = Proyecto.institucion_id' ]
                    ]
                ],
                'order' => ['Logproyecto.created' => 'desc']
            ];

            $institucion = $this->Logproyecto->Proyecto->Institucion->find("list",[
                'conditions'=>[ 'activo'=>1 ]
            ]);

            $proyectos = $this->Logproyecto->Proyecto->find("list");
        }else{
            $this->Paginator->settings = [
                'fields' => ['Logproyecto.*', 'Proyecto.*', 'Institucion.*'],
                'joins'=>[
                    [
                        'table'=>'proyectos', 'alias'=>'Proyecto', 'type'=>'INNER',
                        'conditions'=>[ 'Proyecto.id = Logproyecto.proyecto_id' ]
                    ],
                    [
                        'table'=>'institucions', 'alias'=>'Institucion', 'type'=>'INNER',
                        'conditions'=>[ 'Institucion.id = Proyecto.institucion_id' ]
                    ]
                ],
                'conditions' => [
                    'Proyecto.institucion_id' => $user_institu
                ],
                'order' => ['Logproyecto.created' => 'desc']
            ];

            $institucion = $this->Logproyecto->Proyecto->Institucion->find("list",[
                'conditions'=>[ 'activo'=>1, 'id'=> $user_institu ]
            ]);

            $proyectos = $this->Logproyecto->Proyecto->find("list", ['conditions'=>[ 'Proyecto.institucion_id'=> $user_institu ]]);
        }


        include "busqueda/logproyecto.php";
        $data = $this->Paginator->paginate('Logproyecto');
        $this->set('logproyectos', $data);

        $this->set(compact("proyectos", "institucion"));
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[logproyectos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Logproyecto", "logproyectos", "view");
        $this->Logproyecto->recursive = -1;

		if (!$this->Logproyecto->exists($id)) {
			throw new NotFoundException(__('Invalid logproyecto'));
		}

		$options = array(
                'fields' => ['Logproyecto.*', 'Proyecto.*', 'Institucion.*'],
                'joins'=>[
                    [
                        'table'=>'proyectos', 'alias'=>'Proyecto', 'type'=>'INNER',
                        'conditions'=>[ 'Proyecto.id = Logproyecto.proyecto_id' ]
                    ],
                    [
                        'table'=>'institucions', 'alias'=>'Institucion', 'type'=>'INNER',
                        'conditions'=>[ 'Institucion.id = Proyecto.institucion_id' ]
                    ]
                ],
		        'conditions' => array('Logproyecto.' . $this->Logproyecto->primaryKey => $id)
            );

		$logproyecto = $this->Logproyecto->find('first', $options);
		$log = json_decode($logproyecto['Logproyecto']['log']);

		$logproyecto['Logproyecto']['accion'] = $log->accion;
        $logproyecto['Logproyecto']['tabla'] = $log->tabla;
        $logproyecto['Logproyecto']['modelo'] = $log->modelo;
        $logproyecto['Logproyecto']['pantalla'] = $log->pantalla;
        $logproyecto['Logproyecto']['data'] = $log->data;

        switch ($log->tabla) {
            case "proyectos":
                $logproyecto['Logproyecto']['info'] = $this->info_proyecto($log, $log->modelo, $log->accion);
                break;
            case "modules":
                $logproyecto['Logproyecto']['info'] = $this->info_modulo($log, $log->modelo, $log->accion);
                break;
            case "intervenciones":
                $logproyecto['Logproyecto']['info'] = $this->info_intervencion($log, $log->modelo, $log->accion);
                break;
            case "actividades":
                $logproyecto['Logproyecto']['info'] = $this->info_actividad($log, $log->modelo, $log->accion);
                break;
            case "fuentesfinanciamientos":
                $logproyecto['Logproyecto']['info'] = $this->info_fuente($log, $log->modelo, $log->accion);
                break;
            case "desembolsos":
                $logproyecto['Logproyecto']['info'] = $this->info_desembolso($log, $log->modelo, $log->accion);
                break;
            case "presupuestos":
                $logproyecto['Logproyecto']['info'] = $this->info_presupuesto($log, $log->modelo, $log->accion);
                break;
            case "gastos":
                $logproyecto['Logproyecto']['info'] = $this->info_gasto($log, $log->modelo, $log->accion);
                break;
            case "incidencias":
                $logproyecto['Logproyecto']['info'] = $this->info_incidencia($log, $log->modelo, $log->accion);
                break;
        }

		$this->set('logproyecto', $logproyecto);
	}

    public function info_proyecto($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("codigo", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Código</label><label class="actualizado">'.$log->data->codigo.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Código</label><label>'.$log->data->codigo.'</label></div>';
                }

                if (in_array("institucion_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Institución</label><label class="actualizado">'.$log->data->ninstitucion.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Institución</label><label>'.$log->data->ninstitucion.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("nombrecorto", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre del Proyecto (corto)</label><label class="actualizado">'.$log->data->nombrecorto.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre del Proyecto (corto)</label><label>'.$log->data->nombrecorto.'</label></div>';
                }

                if (in_array("estado_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Estado</label><label class="actualizado">'.$log->data->nestado.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Estado</label><label>'.$log->data->nestado.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("nombrecompleto", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre Completo del Proyecto</label><label class="actualizado">'.$log->data->nombrecompleto.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre Completo del Proyecto</label><label>'.$log->data->nombrecompleto.'</label></div>';
                }

                if (in_array("paise_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label class="actualizado">'.$log->data->npais.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label>'.$log->data->npais.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("tipoproyecto_id", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Tipo de Proyecto</label><label class="actualizado">'.$log->data->ntipoproyecto.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Tipo de Proyecto</label><label>'.$log->data->ntipoproyecto.'</label></div>';
                }

                $html.='<div class="col-sm-5"><label class="encabezado-view">Vigencia</label><label>';
                if (in_array("desde", $log->campos)){
                    $html.='<span class="actualizado">'.date("d-m-Y",strtotime($log->data->desde)).'</span>';
                } else {
                    $html.='<span>'.date("d-m-Y",strtotime($log->data->desde)).'</span>';
                }

                $html.=' - ';
                if (in_array("hasta", $log->campos)){
                    $html.='<span class="actualizado">'.date("d-m-Y",strtotime($log->data->hasta)).'</span>';
                } else {
                    $html.='<span>'.date("d-m-Y",strtotime($log->data->hasta)).'</span>';
                }

                $html.='</label></div>';
                $html.='<div class="clearfix"></div>';

                if (in_array("descripcion", $log->campos)){
                    $html.='<div class="col-sm-12"><label class="encabezado-view">Descripción</label><label class="actualizado">'.$log->data->descripcion.'</label></div>';
                } else {
                    $html.='<div class="col-sm-12"><label class="encabezado-view">Descripción</label><label>'.$log->data->descripcion.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';
                break;
            default:
                $html.='<div class="col-sm-7"><label class="encabezado-view">Código</label><label>'.$log->data->codigo.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Institución</label><label>'.$log->data->ninstitucion.'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre del Proyecto (corto)</label><label>'.$log->data->nombrecorto.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Estado</label><label>'.$log->data->nestado.'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre Completo del Proyecto</label><label>'.$log->data->nombrecompleto.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label>'.$log->data->npais.'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Tipo de Proyecto</label><label>'.$log->data->ntipoproyecto.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Vigencia</label><label>'
                    .date("d-m-Y",strtotime($log->data->desde)) . ' - '
                    .date("d-m-Y",strtotime($log->data->hasta))
                    .'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-12"><label class="encabezado-view">Descripción</label><label>'.$log->data->descripcion.'</label></div>';
                $html.='<div class="clearfix"></div>';
        }

        return $html;
    }

    public function info_modulo($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("nombre", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Módulo</label></div><div class="col-sm-6"><label class="actualizado">'.$log->data->nombre.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Módulo</label></div><div class="col-sm-6"><label>'.$log->data->nombre.'</label></div>';
                }

                break;
            default:
                $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Módulo</label></div><div class="col-sm-6"><label>'.$log->data->nombre.'</label></div>';
        }

        return $html;
    }

    public function info_intervencion($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("nombre", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Intervención</label></div><div class="col-sm-6"><label class="actualizado">'.$log->data->nombre.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Intervención</label></div><div class="col-sm-6"><label>'.$log->data->nombre.'</label></div>';
                }

                break;
            default:
                $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Intervención</label></div><div class="col-sm-6"><label>'.$log->data->nombre.'</label></div>';
        }

        return $html;
    }

    public function info_actividad($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("nombre", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label class="actualizado">'.$log->data->nombre.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label>'.$log->data->nombre.'</label></div>';
                }

                if (in_array("inicio", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Inicio</label><label class="actualizado">'.date("d-m-Y",strtotime($log->data->inicio)).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Inicio</label><label>'.date("d-m-Y",strtotime($log->data->inicio)).'</label></div>';
                }

                if (in_array("limite", $log->campos)){
                    $html.='<div class="col-sm-3"><label class="encabezado-view">Límite</label><label class="actualizado">'.date("d-m-Y",strtotime($log->data->limite)).'</label></div>';
                } else {
                    $html.='<div class="col-sm-3"><label class="encabezado-view">Límite</label><label>'.date("d-m-Y",strtotime($log->data->limite)).'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("intervencione_id", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Intervención</label><label class="actualizado">'.$log->data->nintervencion.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Intervención</label><label>'.$log->data->nintervencion.'</label></div>';
                }

                if (in_array("paise_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label class="actualizado">'.$log->data->npais.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label>'.$log->data->npais.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("fuentesfinanciamiento_id", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Fuente de Financiamiento</label><label class="actualizado">'.$log->data->ffinanciamiento.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Fuente de Financiamiento</label><label>'.$log->data->ffinanciamiento.'</label></div>';
                }

                if (in_array("finalizado", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Finalizado</label><label class="actualizado">'.($log->data->finalizado  ? 'Si' : 'No').'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Finalizado</label><label>'.($log->data->finalizado  ? 'Si' : 'No').'</label></div>';
                }

                $html.='<div class="clearfix"></div>';
                break;
            default:
                $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label>'.$log->data->nombre.'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Inicio</label><label>'.date("d-m-Y",strtotime($log->data->inicio)).'</label></div>';
                $html.='<div class="col-sm-3"><label class="encabezado-view">Límite</label><label>'.date("d-m-Y",strtotime($log->data->limite)).'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Intervención</label><label>'.$log->data->nintervencion.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">País</label><label>'.$log->data->npais.'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Fuente de Financiamiento</label><label>'.$log->data->ffinanciamiento.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Finalizado</label><label>'.($log->data->finalizado  ? 'Si' : 'No').'</label></div>';
                $html.='<div class="clearfix"></div>';
        }

        return $html;
    }

    public function info_fuente($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("financista_id", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Financista</label></div><div class="col-sm-6"><label class="actualizado">'.$log->data->financista.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Financista</label></div><div class="col-sm-6"><label>'.$log->data->financista.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("monto", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Total</label></div><div class="col-sm-6"><label class="actualizado">$'.number_format($log->data->monto,2).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Total</label></div><div class="col-sm-6"><label>$'.number_format($log->data->monto,2).'</label></div>';
                }

                break;
            default:
                $html.='<div class="col-sm-2"><label class="encabezado-view">Financista</label></div><div class="col-sm-6"><label>'.$log->data->financista.'</label></div>';
                $html.='<div class="clearfix"></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Total</label></div><div class="col-sm-6"><label>$'.number_format($log->data->monto,2).'</label></div>';
        }

        return $html;
    }

    public function info_desembolso($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                if (in_array("fuentefinanciamiento_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Fuente de Financiamiento</label><label class="actualizado">'.$log->data->ffinanciamiento.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Fuente de Financiamiento</label><label>'.$log->data->ffinanciamiento.'</label></div>';
                }

                if (in_array("monto", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto</label><label class="actualizado">$'.number_format($log->data->monto,2).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto</label><label>$'.number_format($log->data->monto,2).'</label></div>';
                }

                if (in_array("comision", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Comisión</label><label class="actualizado">$'.number_format($log->data->comision,2).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Comisión</label><label>$'.number_format($log->data->comision,2).'</label></div>';
                }

                $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Total</label><label>$'.number_format($log->data->total,2).'</label></div>';
                $html.='<div class="clearfix"></div>';

                if (in_array("destinatario_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Destinatario</label><label class="actualizado">'.$log->data->destinatario.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Destinatario</label><label>'.$log->data->destinatario.'</label></div>';
                }

                if (in_array("codigo", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Código</label><label class="actualizado">'.$log->data->codigo.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Código</label><label>'.$log->data->codigo.'</label></div>';
                }

                if (in_array("fecha", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Fecha</label><label class="actualizado">'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Fecha</label><label>'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                }

                if (in_array("bancorigen_id", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Banco de Origen</label><label class="actualizado">'.$log->data->bancoorigen.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Banco de Origen</label><label>'.$log->data->bancoorigen.'</label></div>';
                }

                if (in_array("bancdestino_id", $log->campos)){
                    $html.='<div class="col-sm-6"><label class="encabezado-view">Banco de Destino</label><label class="actualizado">'.$log->data->bancodestino.'</label></div>';
                } else {
                    $html.='<div class="col-sm-6"><label class="encabezado-view">Banco de Destino</label><label>'.$log->data->bancodestino.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';
                break;
            default:
                $html.='<div class="col-sm-5"><label class="encabezado-view">Fuente de Financiamiento</label><label>'.$log->data->ffinanciamiento.'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Monto</label><label>$'.number_format($log->data->monto,2).'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Comisión</label><label>$'.number_format($log->data->comision,2).'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Total</label><label>$'.number_format($log->data->total,2).'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-5"><label class="encabezado-view">Destinatario</label><label>'.$log->data->destinatario.'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Código</label><label>'.$log->data->codigo.'</label></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Fecha</label><label>'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-5"><label class="encabezado-view">Banco de Origen</label><label>'.$log->data->bancoorigen.'</label></div>';
                $html.='<div class="col-sm-6"><label class="encabezado-view">Banco de Destino</label><label>'.$log->data->bancodestino.'</label></div>';

                $html.='<div class="clearfix"></div>';

        }

        return $html;
    }

    public function info_presupuesto($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("actividade_id", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Actividad</label></div><div class="col-sm-6"><label class="actualizado">'.$log->data->actividad.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Actividad</label></div><div class="col-sm-6"><label>'.$log->data->actividad.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("totalasignado", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Asignado</label></div><div class="col-sm-6"><label class="actualizado">$'.number_format($log->data->totalasignado,2).'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Asignado</label></div><div class="col-sm-6"><label>$'.number_format($log->data->totalasignado,2).'</label></div>';
                }
                break;
            default:
                $html.='<div class="col-sm-2"><label class="encabezado-view">Actividad</label></div><div class="col-sm-6"><label>'.$log->data->actividad.'</label></div>';
                $html.='<div class="clearfix"></div>';
                $html.='<div class="col-sm-2"><label class="encabezado-view">Monto Asignado</label></div><div class="col-sm-6"><label>$'.number_format($log->data->totalasignado,2).'</label></div>';
        }

        return $html;
    }

    public function info_gasto($log = null, $modelo = null, $accion = null) {
        $html = '';
        $this->loadModel($modelo);

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("catgasto_id", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Categoría de Gasto</label><label class="actualizado">'.$log->data->ncatgasto.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Categoría de Gasto</label><label>'.$log->data->ncatgasto.'</label></div>';
                }

                if (in_array("fecha", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Fecha</label><label class="actualizado">'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Fecha</label><label>'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("actividade_id", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label class="actualizado">'.$log->data->nactividad.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label>'.$log->data->nactividad.'</label></div>';
                }

                if (in_array("monto", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Monto</label><label class="actualizado">$'.number_format($log->data->monto,2).'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Monto</label><label>$'.number_format($log->data->monto,2).'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                if (in_array("descripcion", $log->campos)){
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Descripción</label><label class="actualizado">'.$log->data->descripcion.'</label></div>';
                } else {
                    $html.='<div class="col-sm-7"><label class="encabezado-view">Descripción</label><label>'.$log->data->descripcion.'</label></div>';
                }

                if (in_array("comprobante", $log->campos)){
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Comprobante</label><label class="actualizado">'.$log->data->comprobante.'</label></div>';
                } else {
                    $html.='<div class="col-sm-5"><label class="encabezado-view">Comprobante</label><label>'.$log->data->comprobante.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';
                break;
            default:
                $html.='<div class="col-sm-7"><label class="encabezado-view">Categoría de Gasto</label><label>'.$log->data->ncatgasto.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Fecha</label><label>'.date("d-m-Y",strtotime($log->data->fecha)).'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Nombre de Actividad</label><label>'.$log->data->nactividad.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Monto</label><label>$'.number_format($log->data->monto,2).'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div class="col-sm-7"><label class="encabezado-view">Descripción</label><label>'.$log->data->descripcion.'</label></div>';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Comprobante</label><label>'.$log->data->comprobante.'</label></div>';
                $html.='<div class="clearfix"></div>';
        }

        return $html;
    }

    public function info_incidencia($log = null, $modelo = null, $accion = null) {
        $html = '';

        switch ($accion) {
            case "Modificar":
                $html.='<div class="col-sm-12 actualizado" style="font-weight: bold; margin-bottom: 7px;">Información actualizada en color azul</div>';

                if (in_array("actividade_id", $log->campos)){
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Actividad</label></div><div class="col-sm-9"><label class="actualizado">'.$log->data->actividad.'</label></div>';
                } else {
                    $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Actividad</label></div><div class="col-sm-9"><label>'.$log->data->actividad.'</label></div>';
                }

                $html.='<div class="clearfix"></div>';

                $html.='<div style="margin-top: 25px;">';
                $html.='<div class="col-sm-offset-1 col-sm-4">';
                $html.='<div class="col-sm-8"><label class="encabezado-view">Rango de Edad</label></div>';
                $html.='<div class="col-sm-4"><label class="encabezado-view text-center">Cantidad</label></div>';

                foreach ($log->data->Rangoedadincidencia as $item) {
                    $ban_rango = 0;
                    foreach ($log->data->anterior->Rangoedadincidencia as $anterior) {
                        if($item->rangoedad_id == $anterior->rangoedad_id && $item->cantidad == $anterior->cantidad) {
                            $ban_rango = 1;
                        }
                    }

                    if($ban_rango == 0) {
                        $html.='<div class="col-sm-8"><label class="actualizado">'.$item->rangoedad.'</label></div>';
                        $html.='<div class="col-sm-4"><label class="text-center actualizado">'.$item->cantidad.'</label></div>';
                    } else {
                        $html.='<div class="col-sm-8"><label>'.$item->rangoedad.'</label></div>';
                        $html.='<div class="col-sm-4"><label class="text-center">'.$item->cantidad.'</label></div>';
                    }

                    $html.='<div class="clearfix"></div>';
                }

                $html.='</div>';

                $html.='<div class="col-sm-4">';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Género</label></div>';
                $html.='<div class="col-sm-4"><label class="encabezado-view text-center">Cantidad</label></div>';
                $html.='<div class="clearfix"></div>';

                foreach ($log->data->Generoincidencia as $item) {
                    $ban_genero = 0;
                    foreach ($log->data->anterior->Generoincidencia as $anterior) {
                        if($item->genero_id == $anterior->genero_id && $item->cantidad == $anterior->cantidad) {
                            $ban_genero = 1;
                        }
                    }

                    if($ban_genero == 0) {
                        $html.='<div class="col-sm-5"><label class="actualizado">'.$item->genero.'</label></div>';
                        $html.='<div class="col-sm-4"><label class="text-center actualizado">'.$item->cantidad.'</label></div>';
                    } else {
                        $html.='<div class="col-sm-5"><label>'.$item->genero.'</label></div>';
                        $html.='<div class="col-sm-4"><label class="text-center">'.$item->cantidad.'</label></div>';
                    }

                    $html.='<div class="clearfix"></div>';
                }
                $html.='</div>';
                $html.='</div>';
                break;
            default:
                $html.='<div class="col-sm-2"><label class="encabezado-view">Nombre de Actividad</label></div><div class="col-sm-9"><label>'.$log->data->actividad.'</label></div>';
                $html.='<div class="clearfix"></div>';

                $html.='<div style="margin-top: 25px;">';
                $html.='<div class="col-sm-offset-1 col-sm-4">';
                $html.='<div class="col-sm-8"><label class="encabezado-view">Rango de Edad</label></div>';
                $html.='<div class="col-sm-4"><label class="encabezado-view text-center">Cantidad</label></div>';
                foreach ($log->data->Rangoedadincidencia as $item) {
                    $html.='<div class="col-sm-8"><label>'.$item->rangoedad.'</label></div>';
                    $html.='<div class="col-sm-4"><label class="text-center">'.$item->cantidad.'</label></div>';
                    $html.='<div class="clearfix"></div>';
                }

                $html.='</div>';

                $html.='<div class="col-sm-4">';
                $html.='<div class="col-sm-5"><label class="encabezado-view">Género</label></div>';
                $html.='<div class="col-sm-4"><label class="encabezado-view text-center">Cantidad</label></div>';
                $html.='<div class="clearfix"></div>';
                foreach ($log->data->Generoincidencia as $item) {
                    $html.='<div class="col-sm-5"><label>'.$item->genero.'</label></div>';
                    $html.='<div class="col-sm-4"><label class="text-center">'.$item->cantidad.'</label></div>';
                    $html.='<div class="clearfix"></div>';
                }
                $html.='</div>';
                $html.='</div>';

        }

        return $html;
    }

    public function get_proyectos() {
        $id = $_POST['id'];

        $options = array('conditions' => array('Proyecto.institucion_id' => $id));
        $data['proyecto'] = $this->Logproyecto->Proyecto->find('list', $options);

        if(count($data['proyecto'] > 0)){
            $data['msg'] = 'exito';
        } else {
            $data['msg'] = 'error';
        }

        echo json_encode($data);
        $this->autoRender=false;
    }

    public function getInst(){
        $user = $this->Session->read("nombreusuario");
        $this->loadModel("User");
        $this->User->recursive=-1;
        $sql = $this->User->find("all",[
            'joins'=>[
                [
                    'table'=>"contactos",
                    'alias'=>'Contacto',
                    'type'=>'INNER',
                    'conditions'=>[
                        'User.contacto_id = Contacto.id'
                    ]
                ]
            ],
            'conditions'=>[
                'User.username'=>$user
            ],
            'fields'=>[
                'Contacto.institucion_id',
                'User.id'
            ]
        ]);
        $var = (count($sql)>0)?$sql[0]['Contacto']['institucion_id']:0;
        return $var;
    }

    public function get_perfil(){
        $this->loadModel("Perfile");
        $this->Perfile->recursive=-1;
        $user = $this->Session->read("nombreusuario");
        $info = $this->Perfile->find("all",[
            'joins'=>[
                [
                    'table'=>'users',
                    'alias'=>'User',
                    'type'=>'INNER',
                    'conditions'=>[
                        "Perfile.id=User.perfile_id"
                    ]
                ]
            ],
            'conditions'=>[
                'User.username'=>$user
            ],
            'fields'=>['Perfile.admin']
        ]);
        $band = ($info[0]['Perfile']['admin']==1)?1:0;
        return $band;
    }
}
