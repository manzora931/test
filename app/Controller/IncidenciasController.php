<?php
App::uses('AppController', 'Controller');
/**
 * Incidencias Controller
 *
 * @property Incidencia $Incidencia
 * @property PaginatorComponent $Paginator
 */
class IncidenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * Helpers
 *
 * @var array
 */
    public $helpers = array('Html','Js', 'Form');

/**
 * index method
 *
 * @return void
 */
	public function index($idProyecto = null) {
        $this->layout=false;
        $data = [];
        $modules=[];
        $intervenciones=[];
        $incidenciasintervencion = [];
        $incidenciasmodulo = [];

        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

        if(!is_null($idProyecto)) {
            /************************* MODULOS ****************************/
            $this->loadModel("Module");
            $this->Module->recursive = 1;
            $modules = $this->Module->find("all",[
                'conditions'=>['Module.proyecto_id'=>$idProyecto],
                'order'=>['Module.orden'=>"ASC"]
            ]);

            foreach ($modules as $row) {
                /*** ALMACENA LAS INTERNVENCIONES DE CADA MODULO ***/
                $intervenciones[$row['Module']['id']] = $row['Intervencione'];
                $incidenciasmodulo[$row['Module']['id']] = 0;
            }

            /*** FOREACH PARA RECORRER INTERVENCIONES ***/
            foreach ($intervenciones as $rw) {
                foreach ($rw as $item){
                    /*** INCIDENCIAS DE ACTIVIDADES POR INTERVENCIONES ***/
                    $incidencias = $this->Incidencia->find("all",['conditions'=>['Actividade.intervencione_id' => $item['id']]]);
                    $data[$item['id']] = $incidencias;

                    $incidenciasintervencion[$item['id']] = count($incidencias);
                    $incidenciasmodulo[$item['module_id']]+=$incidenciasintervencion[$item['id']];
                }
            }
        }

        $this->set('incidencias', $data);

        $this->Incidencia->Actividade->recursive = -1;
        $actividades = $this->Incidencia->Actividade->find("list", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'Actividade.finalizado' => 0, 'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $this->loadModel('Genero');
        $generos = $this->Genero->find("list", [
            'fields' => ['Genero.id', 'Genero.genero'],
            'conditions'=> ['Genero.activo' => 1]
        ]);

        $this->loadModel('Rangoedade');
        $rangoedades = $this->Rangoedade->find("list", [
            'fields' => ['Rangoedade.id', 'Rangoedade.nombre'],
            'conditions'=> ['Rangoedade.activo' => 1]
        ]);

        $rangosedades = $this->Rangoedade->find("all", [
            'fields' => ['Rangoedade.id', 'concat("De ", Rangoedade.desde, " Años a ", Rangoedade.hasta, " Años") as nombre'],
            'conditions'=> ['Rangoedade.activo' => 1]
        ]);

        $rangos = array();
        foreach ($rangosedades as $rango) {
            $rangos[$rango['Rangoedade']['id']] = $rango[0]['nombre'];
        }

        $meses = [1=>"Ene", 2=>"Feb", 3=>"Mar", 4=>"Abr", 5=>"May", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Sep", 10=>"Oct", 11=>"Nov", 12=>"Dic"];
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive=-1;
        $infoP=$this->Proyecto->read("estado_id",$idProyecto);
        $this->set(compact( 'idProyecto', 'modules', 'intervenciones', 'actividades', 'generos', 'rangoedades', 'rangos', 'meses','infoP', 'incidenciasmodulo', 'incidenciasintervencion'));
	}

    public function add($idProyecto =null) {
        $this->layout=false;

        $this->Incidencia->Actividade->recursive = -1;
        $actividades = $this->Incidencia->Actividade->find("list", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'Actividade.finalizado' => 0, 'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $this->loadModel('Genero');
        $generos = $this->Genero->find("list", [
            'fields' => ['Genero.id', 'Genero.genero'],
            'conditions' => ['Genero.activo' => 1]
        ]);

        $this->loadModel('Rangoedade');
        $rangoedades = $this->Rangoedade->find("all", [
            'fields' => ['Rangoedade.id', 'Rangoedade.desde','Rangoedade.hasta'],
            'conditions'=> ['Rangoedade.activo' => 1]
        ]);
        $edades=[];
        foreach ($rangoedades as $item){
            $edades[$item['Rangoedade']['id']]="De ".$item["Rangoedade"]['desde']." Años a ".$item["Rangoedade"]['hasta']." Años";
        }
        $this->set(compact('actividades', 'generos', 'edades'));
    }

    public function edit($idProyecto =null, $id = null) {
        $this->layout=false;
        $this->request->data = $this->Incidencia->find('first', [ 'conditions' => [ 'Incidencia.id' => $id ] ]);

        $this->loadModel('Rangoedadincidencia');
        $rango = $this->Rangoedadincidencia->find('first', [ 'order' => [ 'Rangoedadincidencia.id DESC' ] ]);

        $this->loadModel('Generoincidencia');
        $genero = $this->Generoincidencia->find('first', [ 'order' => [ 'Generoincidencia.id DESC' ] ]);

        $this->Incidencia->Actividade->recursive = -1;
        $actividades = $this->Incidencia->Actividade->find("list", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'Actividade.finalizado' => 0, 'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $this->loadModel('Genero');
        $generosinc = $this->Genero->Generoincidencia->find("list", [
            'fields' => ['Generoincidencia.genero_id'],
            'conditions'=> ['Generoincidencia.incidencia_id' => $id]
        ]);

        $generosid = array();
        foreach ($generosinc as $row) {
            array_push($generosid, $row);
        }

        $generos = $this->Genero->find("list", [
            'fields' => ['Genero.id', 'Genero.genero'],
            'conditions' => ['Genero.activo' => 1]
        ]);

        $this->loadModel('Rangoedade');
        $rangoedades = $this->Rangoedade->find("all", [
            'fields' => ['Rangoedade.id', 'Rangoedade.nombre','Rangoedade.desde','Rangoedade.hasta'],
            'conditions'=> ['Rangoedade.activo' => 1]
        ]);

        $edades=[];

        foreach ($rangoedades as $item){
            $edades[$item['Rangoedade']['id']]="De ".$item["Rangoedade"]['desde']." Años a ".$item["Rangoedade"]['hasta']." Años";
        }
        $this->set(compact('actividades', 'generos', 'rangoedades', 'rango', 'genero','edades'));
    }

    public function delete() {
        $this->Incidencia->id = $_POST['id'];
        $proyectoid = $_POST['proyectoid'];

        if (!$this->Incidencia->exists()) {
            $resp['msg'] = 'error1';
        } else {
            $incidencia = $this->Incidencia->find('first', [ 'conditions' => [ 'Incidencia.id' => $this->Incidencia->id ] ]);
            $log = $incidencia['Incidencia'];
            $log['actividad'] = $incidencia['Actividade']['nombre'];

            $generoincidencias = $this->Incidencia->Generoincidencia->find("all", [
                'conditions'=> ['Incidencia.id' => $this->Incidencia->id]
            ]);

            foreach ($generoincidencias as $key => $generoincidencia) {
                $log['Generoincidencia'][$key] = $generoincidencia['Generoincidencia'];
                $log['Generoincidencia'][$key]['genero'] = $generoincidencia['Genero']['genero'];
                $this->Incidencia->Generoincidencia->id = $generoincidencia['Generoincidencia']['id'];
                $this->Incidencia->Generoincidencia->delete();
            }

            $rangoedadincidencias = $this->Incidencia->Rangoedadincidencia->find("all", [
                'conditions'=> ['Incidencia.id' => $this->Incidencia->id]
            ]);

            foreach ($rangoedadincidencias as $key => $rangoedadincidencia) {
                $log['Rangoedadincidencia'][$key] = $rangoedadincidencia['Rangoedadincidencia'];
                $log['Rangoedadincidencia'][$key]['rangoedad'] = 'De ' . $rangoedadincidencia['Rangoedade']['desde'] . ' Años a ' . $rangoedadincidencia['Rangoedade']['hasta'] . ' Años';
                $this->Incidencia->Rangoedadincidencia->id = $rangoedadincidencia['Rangoedadincidencia']['id'];
                $this->Incidencia->Rangoedadincidencia->delete();
            }

            if ($this->Incidencia->delete()) {
                $resp['msg'] = 'exito';

                $_SESSION['logsproyecto']['accion'] = 'Eliminar';
                $_SESSION['logsproyecto']['data'] = $log;

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Incidencia', 'incidencias', 'Incidencias');
            } else {
                $resp['msg'] = 'error2';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function almacenar_data() {
        $id = $_POST['id'];
        if($id != '') {
            $incidencia = $this->Incidencia->find('first', [
                'conditions' => [
                    'Incidencia.id' => $id
                ]
            ]);

            if(count($incidencia) > 0) {
                $incidencia['Incidencia']['actividade_id'] = $_POST['actividad-incidencia'];
                $incidencia['Incidencia']['usuariomodif'] = $this->Session->read('nombreusuario');
                $incidencia['Incidencia']['modified'] = date('Y-m-d H:i:s');
                if ($this->Incidencia->save($incidencia)) {
                    $this->Session->write("saveincidencia",1);
                    $resp['id'] = $id;

                    $resp['msg'] = 'exito';
                } else {
                    $resp['msg'] = 'error';
                }
            }
        } else {
            $data['actividade_id'] = $_POST['actividad-incidencia'];
            $data['usuario'] = $this->Session->read('nombreusuario');
            $data['created'] = date('Y-m-d H:i:s');
            $data['modified'] = 0;

            $this->Incidencia->create();
            if ($this->Incidencia->save($data)) {
                $resp['id'] = $this->Incidencia ->id;

                $this->Incidencia->Generoincidencia->create();
                $genero['incidencia_id'] = $this->Incidencia ->id;
                $genero['genero_id'] = $_POST['genero'];
                $genero['cantidad'] = $_POST['cantidad-genero'];
                $genero['usuario'] = $this->Session->read('nombreusuario');
                $genero['created'] = date('Y-m-d H:i:s');
                $genero['modified'] = 0;
                $this->Incidencia->Generoincidencia->save($genero);

                $this->Incidencia->Rangoedadincidencia->create();
                $rango['incidencia_id'] = $this->Incidencia ->id;
                $rango['rangoedad_id'] = $_POST['rangoedad'];
                $rango['cantidad'] = $_POST['cantidad-rango'];
                $rango['usuario'] = $this->Session->read('nombreusuario');
                $rango['created'] = date('Y-m-d H:i:s');
                $rango['modified'] = 0;
                $this->Incidencia->Rangoedadincidencia->save($rango);

                $this->Session->write("saveincidencia",1);
                $resp['msg'] = 'exito';
            } else {
                $resp['msg'] = 'error';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function save_incidencia() {
        $resp = [];
	    $id = $_POST['incidencia_id'];
	    $actividadid = $_POST['actividade_id'];
        $incidencia = $this->Incidencia->find('first', [ 'conditions' => [ 'Incidencia.id' => $id ] ]);

        if(count($incidencia) > 0) {
            $_SESSION['logsproyecto']['anterior'] = $incidencia;

            foreach ($incidencia['Generoincidencia'] as $generoincidencia) {
                array_push($_SESSION['logsproyecto']['anterior']['Generoincidencia'], $generoincidencia);
                $this->Incidencia->Generoincidencia->id = $generoincidencia['id'];
                $this->Incidencia->Generoincidencia->delete();
            }

            foreach ($incidencia['Rangoedadincidencia'] as $rangoedadincidencia) {
                array_push($_SESSION['logsproyecto']['anterior']['Rangoedadincidencia'], $rangoedadincidencia);
                $this->Incidencia->Rangoedadincidencia->id = $rangoedadincidencia['id'];
                $this->Incidencia->Rangoedadincidencia->delete();
            }

            $incidencia['Incidencia']['actividade_id'] = $actividadid;
            $incidencia['Incidencia']['usuariomodif'] = $this->Session->read('nombreusuario');
            $incidencia['Incidencia']['modified'] = date('Y-m-d H:i:s');

            if ($this->Incidencia->save($incidencia)) {
                $log = $incidencia['Incidencia'];
                $log['actividad'] = $_POST['actividad'];

                foreach ($_POST['generos'] as $key => $row) {
                    $this->Incidencia->Generoincidencia->create();
                    $genero['incidencia_id'] = $this->Incidencia ->id;
                    $genero['genero_id'] = $row;
                    $genero['cantidad'] = $_POST['cantgeneros'][$key];
                    $genero['usuario'] = $this->Session->read('nombreusuario');
                    $genero['created'] = date('Y-m-d H:i:s');
                    $genero['modified'] = 0;
                    $this->Incidencia->Generoincidencia->save($genero);
                    $genero['id'] = $this->Incidencia->Generoincidencia->id;
                    $genero['genero'] = $_POST['ngeneros'][$key];
                    $log['Generoincidencia'][$key] = $genero;
                }

                foreach ($_POST['rangoedades'] as $key => $row) {
                    $this->Incidencia->Rangoedadincidencia->create();
                    $rango['incidencia_id'] = $this->Incidencia ->id;
                    $rango['rangoedad_id'] = $row;
                    $rango['cantidad'] = $_POST['cantedades'][$key];
                    $rango['usuario'] = $this->Session->read('nombreusuario');
                    $rango['created'] = date('Y-m-d H:i:s');
                    $rango['modified'] = 0;
                    $this->Incidencia->Rangoedadincidencia->save($rango);
                    $rango['id'] = $this->Incidencia->Rangoedadincidencia->id;
                    $rango['rangoedad'] = $_POST['nrangoedades'][$key];
                    $log['Rangoedadincidencia'][$key] = $rango;
                }

                $_SESSION['logsproyecto']['accion'] = 'Modificar';
                $_SESSION['logsproyecto']['data'] = $log;
                $_SESSION['logsproyecto']['data']['anterior'] = $_SESSION['logsproyecto']['anterior'];
                $this->Session->write("saveincidencia",1);
                $resp['id'] = $id;
                $resp['msg'] = 'exito';

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Incidencia', 'incidencias', 'Incidencias');
            } else {
                $resp['msg'] = 'error';
            }
        } else {
            $data['actividade_id'] = $actividadid;
            $data['usuario'] = $this->Session->read('nombreusuario');
            $data['created'] = date('Y-m-d H:i:s');
            $data['modified'] = 0;

            $this->Incidencia->create();
            if ($this->Incidencia->save($data)) {
                $data['actividad'] = $_POST['actividad'];
                $log = $data;

                foreach ($_POST['generos'] as $key => $row) {
                    $this->Incidencia->Generoincidencia->create();
                    $genero['incidencia_id'] = $this->Incidencia ->id;
                    $genero['genero_id'] = $row;
                    $genero['cantidad'] = $_POST['cantgeneros'][$key];
                    $genero['usuario'] = $this->Session->read('nombreusuario');
                    $genero['created'] = date('Y-m-d H:i:s');
                    $genero['modified'] = 0;
                    $this->Incidencia->Generoincidencia->save($genero);
                    $genero['genero'] = $_POST['ngeneros'][$key];
                    $log['Generoincidencia'][$key] = $genero;
                }

                foreach ($_POST['rangoedades'] as $key => $row) {
                    $this->Incidencia->Rangoedadincidencia->create();
                    $rango['incidencia_id'] = $this->Incidencia ->id;
                    $rango['rangoedad_id'] = $row;
                    $rango['cantidad'] = $_POST['cantedades'][$key];
                    $rango['usuario'] = $this->Session->read('nombreusuario');
                    $rango['created'] = date('Y-m-d H:i:s');
                    $rango['modified'] = 0;
                    $this->Incidencia->Rangoedadincidencia->save($rango);
                    $rango['rangoedad'] = $_POST['nrangoedades'][$key];
                    $log['Rangoedadincidencia'][$key] = $rango;
                }

                $_SESSION['logsproyecto']['accion'] = 'Agregar';
                $log['id'] = $this->Incidencia->id;
                $_SESSION['logsproyecto']['data'] = $log;
                $this->Session->write("saveincidencia",1);
                $resp['id'] = $this->Incidencia->id;
                $resp['msg'] = 'exito';

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Incidencia', 'incidencias', 'Incidencias');
            } else {
                $resp['msg'] = 'error';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function get_data() {
        $id = $_POST['id'];

        $resp['data'] = $this->Incidencia->find('first', [ 'conditions' => [ 'Incidencia.id' => $id ] ]);
        $resp['actividades'] = $this->Incidencia->Actividade->find("list", ['fields' => ['Actividade.id', 'Actividade.nombre'],'conditions'=> ['Actividade.finalizado' => 0]]);

        $this->loadModel('Genero');
        $resp['generos'] = $this->Genero->find("list", [ 'fields' => ['Genero.id', 'Genero.genero'], 'conditions'=> ['Genero.activo' => 1] ]);

        $this->loadModel('Rangoedade');
        $resp['rangoedades'] = $this->Rangoedade->find("list", [ 'fields' => ['Rangoedade.id', 'Rangoedade.nombre'], 'conditions'=> ['Rangoedade.activo' => 1] ]);
        $resp['msg'] = 'exito';

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function get_generos(){
        $id = $_POST['id'];
        $data['data'] = $this->Incidencia->find("first", ['conditions'=> ['Incidencia.id' => $id]]);

        $this->loadModel('Genero');
        $data['genero'] = $this->Genero->find("all", [
            'fields' => ['Genero.id', 'Genero.genero'],
            'conditions'=> [
                'Genero.activo' => 1
            ]
        ]);

        echo json_encode($data);
        $this->autoRender=false;
    }

    public function get_edades(){
        $id = $_POST['id'];
        $data['data'] = $this->Incidencia->find("first", ['conditions'=> ['Incidencia.id' => $id]]);

        $this->loadModel('Rangoedade');
        $data['edades'] = $this->Rangoedade->find("all", [
            'fields' => ['Rangoedade.id', 'Rangoedade.nombre','Rangoedade.desde','Rangoedade.hasta'],
            'conditions'=> [
                'Rangoedade.activo' => 1
            ]
        ]);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
