<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Jugadore $Jugadore
 * @property PaginatorComponent $Paginator
 */
class JugadoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Jugadore", "jugadores", "index");
		$this->Paginator->settings = array('order'=>array('Jugadore.nombre'=>'asc', 'Jugadore.apellido'=>'asc'));
		$this->Jugadore->recursive = 0;
		include 'busqueda/jugadores.php';
		$data = $this->Paginator->paginate('Jugadore');
		$this->set('jugadores', $data);

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Posicione");
		$posiciones = $this->Posicione->find('list');
        $arrJugadores = $this->Jugadore->find("all",[
            "fields"=>[
                "Jugadore.id", "Jugadore.nombre", "Jugadore.apellido", "Jugadore.apodo"
            ],
            "order"=>["Jugadore.apellido"]
        ]);
        $listJugadores = [];
        foreach ($arrJugadores as $jugador){

            if($jugador["Jugadore"]["apodo"] != '' && $jugador["Jugadore"]["apodo"] != '-'){
                $listJugadores[$jugador["Jugadore"]["id"]] = $jugador["Jugadore"]["nombre"]." ".$jugador["Jugadore"]["apellido"]." ".$jugador["Jugadore"]["apodo"];
            }else{
                $listJugadores[$jugador["Jugadore"]["id"]] = $jugador["Jugadore"]["nombre"]." ".$jugador["Jugadore"]["apellido"];
            }
        }
		$this->set(compact('paises', 'posiciones', 'listJugadores'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[jugadores]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Jugadore", "jugadores", "view");
		if (!$this->Jugadore->exists($id)) {
			throw new NotFoundException(__('Invalid jugadore'));
		}
		$options = array('conditions' => array('Jugadore.' . $this->Jugadore->primaryKey => $id));
		$this->set('jugadore', $this->Jugadore->find('first', $options));

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Posicione");
		$posiciones = $this->Posicione->find('list');
        //Estadistica de jugador
        $this->loadModel("Vgeneraljugador");
        $estadisticas = $this->Vgeneraljugador->find("all",[
            "fields"=>[
                "count(*) as total", "Vgeneraljugador.torneo","Vgeneraljugador.equipo", "Vgeneraljugador.evento"
            ],
            "conditions" =>["Vgeneraljugador.jugador_id"=>$id],
            "group"=>["Vgeneraljugador.jugador_id","Vgeneraljugador.torneo_id","Vgeneraljugador.torneo","Vgeneraljugador.equipo_id1","Vgeneraljugador.evento_id"]
        ]);

        $this->loadModel("Persona");
        $personas = $this->Persona->find("list", [
            "conditions"=>[
                "Persona.tipopersona_id" => 6
            ],
            "order"=>["Persona.apellido"]
        ]);
		$this->set(compact('paises', 'posiciones', 'estadisticas', 'personas'));
	}
    public function fotoOrig($id){
	    $this->layout=false;
        $options = array('conditions' => array('Jugadore.' . $this->Jugadore->primaryKey => $id));
        $this->set('jugadore', $this->Jugadore->find('first', $options));
    }

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Jugadore", "jugadores", "add");
		if ($this->request->is('post')) {
			$fechanacimiento = explode('-', $this->request->data['Jugadore']['fechanacimiento']);
			$this->request->data['Jugadore']['fechanacimiento'] = $fechanacimiento[2] . '-' . $fechanacimiento[1] . '-' . $fechanacimiento[0];
			$this->request->data['Jugadore']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Jugadore']['modified']=0;
            if(isset($this->data["Jugadore"]["img"]['name']) && $this->data["Jugadore"]["img"]['name']!='') {
                $file = new File($this->request->data["Jugadore"]["img"]['tmp_name']);
                $path_parts = pathinfo($this->request->data["Jugadore"]["img"]['name']);
            }
			$this->Jugadore->create();

			if ($this->Jugadore->save($this->request->data)) {
				$jugadore_id = $this->Jugadore->id;
                if(isset($this->data["Jugadore"]["img"]['name']) && $this->data["Jugadore"]["img"]['name']!='') {
                    $formato = explode('.', $this->data["Jugadore"]["img"]['name']);
                    $filename = $formato[0]."_".$jugadore_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/jugadores/' . $filename;
                    $sql_Uprod = "UPDATE jugadores SET foto='$url' WHERE id='$jugadore_id'";
                    $result = $this->Jugadore->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/jugadores/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
				$this->Session->write('jugadore_save', 1);
				$this->redirect(['action' => 'view', $jugadore_id]);
			} else {
				$this->Session->setFlash(__('El jugador no ha sido creado. Intentar de nuevo.'));
			}
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Posicione");
		$posiciones = $this->Posicione->find('list');
        $this->loadModel("Persona");
        $personas = $this->Persona->find("list", [
           "conditions"=>[
               "Persona.tipopersona_id" => 6
           ],
            "order"=>["Persona.apellido"]
        ]);

		$this->set(compact('paises', 'posiciones', "personas"));
	}
	/***
	funcion para validar el formato de la foto del jugador**/
	public function valFoto(){
        if(isset($_FILES['archivo'])) {
            $type = $_FILES['archivo']['type'];
            $size = $_FILES['archivo']['size'];
            $name = $_FILES['archivo']['name'];
            $tmpName = $_FILES['archivo']['tmp_name'];
            $ext = pathinfo($name);
            $error=0;
            if($ext['extension']=="png"||$ext['extension']=="PNG" || $ext['extension']=="jpg"||$ext['extension']=="JPG" || $ext['extension']=="jpeg"||$ext['extension']=="JPEG"){
                $error=0;
            }else{
                $error=1;
            }
        }
        echo $error;
        $this->autoRender=false;

    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Jugadore", "jugadores", "edit");
		if (!$this->Jugadore->exists($id)) {
			throw new NotFoundException(__('Invalid jugadore'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$fechanacimiento = explode('-', $this->request->data['Jugadore']['fechanacimiento']);
			$this->request->data['Jugadore']['fechanacimiento'] = $fechanacimiento[2] . '-' . $fechanacimiento[1] . '-' . $fechanacimiento[0];
			$this->request->data['Jugadore']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Jugadore']['modified'] = date("Y-m-d H:i:s");
            if(isset($this->data["Jugadore"]["img"]['name']) && $this->data["Jugadore"]["img"]['name']!='') {
                $file = new File($this->request->data["Jugadore"]["img"]['tmp_name']);
                $path_parts = pathinfo($this->request->data["Jugadore"]["img"]['name']);
            }
			if ($this->Jugadore->save($this->request->data)) {
				$jugadore_id = $this->Jugadore->id;
				$fotourl = $this->Jugadore->foto;
                if(isset($this->data["Jugadore"]["img"]['name']) && $this->data["Jugadore"]["img"]['name']!='') {
                    //unlink(WWW_ROOT.$fotourl);
                    $formato = explode('.', $this->data["Jugadore"]["img"]['name']);
                    $filename = $formato[0]."_".$jugadore_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/jugadores/' . $filename;
                    $sql_Uprod = "UPDATE jugadores SET foto='$url' WHERE id='$jugadore_id'";
                    $result = $this->Jugadore->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/jugadores/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
				$this->Session->write('jugadore_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('El jugador no se ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Jugadore.' . $this->Jugadore->primaryKey => $id));
			$this->request->data = $this->Jugadore->find('first', $options);
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Posicione");
		$posiciones = $this->Posicione->find('list');
        $this->loadModel("Persona");
        $personas = $this->Persona->find("list", [
            "conditions"=>[
                "Persona.tipopersona_id" => 6
            ],
            "order"=>["Persona.apellido"]
        ]);
		$this->set(compact('paises', 'posiciones', 'personas'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Jugadore", "jugadores", "delete");
		if ($delete == true) {
			$this->Jugadore->id = $id;
			if (!$this->Jugadore->exists()) {
				throw new NotFoundException(__('Invalid jugadore'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Jugadore->delete()) {
				$this->Session->setFlash(__('The jugadore has been deleted.'));
			} else {
				$this->Session->setFlash(__('The jugadore could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}
}
