<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Jugadoresxequipo $Jugadoresxequipo
 * @property PaginatorComponent $Paginator
 */
class JugadoresxequiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Jugadoresxequipo", "jugadoresxequipos", "index");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        $this->loadModel("Torneo");
        $this->loadModel("Equipo");
        $this->loadModel("Jugadore");
        if($perfil==3 || $perfil==4 || $perfil==5) {
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->Paginator->settings = array(
                    'conditions'=>["Jugadoresxequipo.torneo_id IN"=>$torneos],
                    'order'=>array('Jugadoresxequipo.torneo_id'=>'asc', 'Jugadoresxequipo.equipo_id'=>'asc', 'Jugadoresxequipo.jugadore_id'=>'asc'));
                $this->Jugadoresxequipo->recursive = -1;
                include 'busqueda/jugadoresxequipos.php';
                $torneos = $this->Torneo->find('list',["conditions"=>["Torneo.activo"=>1,"Torneo.id IN"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }else{
                $this->Paginator->settings = array(
                    'conditions'=>["Jugadoresxequipo.torneo_id"=>$torneos],
                    'order'=>array('Jugadoresxequipo.torneo_id'=>'asc', 'Jugadoresxequipo.equipo_id'=>'asc', 'Jugadoresxequipo.jugadore_id'=>'asc'));
                $this->Jugadoresxequipo->recursive = -1;
                include 'busqueda/jugadoresxequipos.php';
                $torneos = $this->Torneo->find('list',["conditions"=>["Torneo.activo"=>1,"Torneo.id"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }
        }else{
            $this->Paginator->settings = array('order'=>array('Jugadoresxequipo.torneo_id'=>'asc', 'Jugadoresxequipo.equipo_id'=>'asc', 'Jugadoresxequipo.jugadore_id'=>'asc'));
            $this->Jugadoresxequipo->recursive = -1;
            include 'busqueda/jugadoresxequipos.php';
            $torneos = $this->Torneo->find('list',["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
        }


		$data = $this->Paginator->paginate('Jugadoresxequipo');
		$this->set('jugadoresxequipos', $data);
		$equipos = $this->Equipo->find('list');

		$jugadores = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.nombre_jugador'
			]
		]);

		$this->set(compact('torneos', 'equipos', 'jugadores'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[jugadoresxequipos]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Jugadoresxequipo", "jugadoresxequipos", "view");
		if (!$this->Jugadoresxequipo->exists($id)) {
			throw new NotFoundException(__('Invalid jugadoresxequipo'));
		}
		$options = array('conditions' => array('Jugadoresxequipo.' . $this->Jugadoresxequipo->primaryKey => $id));
		$jugadoresxequipo=$this->Jugadoresxequipo->find('first', $options);
		$jugadoresxequipo_all = $this->Jugadoresxequipo->find("list", [
			'conditions' => [
				'Jugadoresxequipo.equipo_id' => $jugadoresxequipo['Jugadoresxequipo']['equipo_id'],
				'Jugadoresxequipo.torneo_id' => $jugadoresxequipo['Jugadoresxequipo']['torneo_id']
			],
			'fields' => [
				'Jugadoresxequipo.jugadore_id',
				'Jugadoresxequipo.dorsal'
			]			
		]);
		
		$this->loadModel("Torneo");
		$torneos = $this->Torneo->find('list');
		$this->loadModel("Equipo");
		$equipos = $this->Equipo->find('list');
		$this->loadModel("Jugadore");
		$nombres = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.nombre'
			]
		]);
		$apellidos = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.apellido'
			]
		]);
		$apodos = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.apodo'
			]
		]);
		$fechanacimiento = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.fechanacimiento'
			]
		]);
		
		$this->set(compact('torneos', 'equipos', 'jugadoresxequipo','jugadoresxequipo_all'));
		$this->set(compact('nombres', 'apellidos', 'apodos', 'fechanacimiento'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Jugadoresxequipo", "jugadoresxequipos", "add");
		if ($this->request->is('post')) {
			$save=false;
			$jugadores=$this->request->data['Jugadoresxequipo']['jugadore_id'];
			$dorsal=$this->request->data['Jugadoresxequipo']['dorsal'];
			$insert['equipo_id']=$this->request->data['Jugadoresxequipo']['equipo_id'];
			$insert['torneo_id']=$this->request->data['Jugadoresxequipo']['torneo_id'];
			$insert['usuario']=$this->Session->read('nombreusuario');
			$insert['created'] = date('Y-m-d H:m:s');
			$insert['modified']=0;
			foreach ($jugadores as $key => $value) {
				if(!empty($value)){
					$insert['jugadore_id']=$value;
					$insert['dorsal']=(empty($dorsal[$key]))?0:$dorsal[$key];
					$this->Jugadoresxequipo->create();
					$this->Jugadoresxequipo->set($insert);
					if ($this->Jugadoresxequipo->save()) {
						$save=true;
					}
				} 
			}
			if($save){
				$this->redirect(['action' => 'index']);
			}else{
				$this->Session->setFlash(__('El jugador no ha sido creado. Intentar de nuevo.'));
			}
		}

		$this->loadModel("Torneo");
		$torneos = $this->Torneo->find('list',["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
		$this->loadModel("Equipo");
		$equipos = $this->Equipo->find('list');
		$this->loadModel("Jugadore");
		$jugadores = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.nombre_jugador'
			]
		]);
		$this->set(compact('torneos', 'equipos', 'jugadores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Jugadoresxequipo", "jugadoresxequipos", "edit");
		if (!$this->Jugadoresxequipo->exists($id)) {
			throw new NotFoundException(__('Invalid jugadoresxequipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Jugadoresxequipo']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Jugadoresxequipo']['modified'] = date("Y-m-d H:i:s");
			$data = $this->request->data["Jugadoresxequipo"];
			/**Verifica si se elimino un jugador****/
            $options = array('conditions' => array('Jugadoresxequipo.' . $this->Jugadoresxequipo->primaryKey => $id));
            $jugadoresxequipo=$this->Jugadoresxequipo->find('first', $options);
            $dataBD = $this->Jugadoresxequipo->find("all", [
                'conditions' => [
                    'Jugadoresxequipo.equipo_id' => $jugadoresxequipo['Jugadoresxequipo']['equipo_id'],
                    'Jugadoresxequipo.torneo_id' => $jugadoresxequipo['Jugadoresxequipo']['torneo_id']
                ],
                'fields' => [
                    'Jugadoresxequipo.jugadore_id',
                    'Jugadoresxequipo.dorsal',
                    'Jugadoresxequipo.torneo_id',
                    'Jugadoresxequipo.equipo_id',
                    'Jugadoresxequipo.id',
                ]
            ]);
            $temp = 0;
            $delete=true;

            foreach ($dataBD as $row){
                $temp=$row["Jugadoresxequipo"]["id"];

                foreach ($data as $row2){
                    if(isset($row2["id"]) && $temp==$row2["id"]){
                        $delete=false;
                    }
                }
                /******/
                if($delete){
                    $this->Jugadoresxequipo->id = $temp;
                    $this->Jugadoresxequipo->delete();
                }
                $delete=true;
            }
			/***-------------****/
			$save=[];
			foreach ($data as $item){
			    if(isset($item["id"])){
                    $save["id"]=$item["id"];
                    $save["jugadore_id"]=$item["jugadore_id"];
                    $save["dorsal"]=$item["dorsal"];
                    $save["modified"]=date("Y-m-d H:i:s");
                    $save["usuariomodif"]=$this->Session->read('nombreusuario');
                    $this->Jugadoresxequipo->save($save);
                }else if(isset($item["jugadore_id"])){
                    $save["torneo_id"] = $jugadoresxequipo['Jugadoresxequipo']['torneo_id'];
                    $save["jugadore_id"]=$item["jugadore_id"];
                    $save["dorsal"]=$item["dorsal"];
                    $save["created"]=date("Y-m-d H:i:s");
                    $save["usuario"]=$this->Session->read('nombreusuario');
                    $this->Jugadoresxequipo->create();
                    $this->Jugadoresxequipo->save($save);
                }
            }
            $this->Session->write('jugadoresxequipo_save', 1);
            $this->redirect(['action' => 'view', $id]);

		} else {
            $options = array('conditions' => array('Jugadoresxequipo.' . $this->Jugadoresxequipo->primaryKey => $id));
            $jugadoresxequipo=$this->Jugadoresxequipo->find('first', $options);
            $jugadoresxequipo_all = $this->Jugadoresxequipo->find("all", [
                'conditions' => [
                    'Jugadoresxequipo.equipo_id' => $jugadoresxequipo['Jugadoresxequipo']['equipo_id'],
                    'Jugadoresxequipo.torneo_id' => $jugadoresxequipo['Jugadoresxequipo']['torneo_id']
                ],
                'fields' => [
                    'Jugadoresxequipo.jugadore_id',
                    'Jugadoresxequipo.dorsal',
                    'Jugadoresxequipo.torneo_id',
                    'Jugadoresxequipo.equipo_id',
                    'Jugadoresxequipo.id',
                ]
            ]);

            $this->set(compact("jugadoresxequipo", "jugadoresxequipo_all"));
		    /*$options = array('conditions' => array('Jugadoresxequipo.' . $this->Jugadoresxequipo->primaryKey => $id));
			$this->request->data = $this->Jugadoresxequipo->find('first', $options);*/

		}

		$this->loadModel("Torneo");
		$torneos = $this->Torneo->find('list');
		$this->loadModel("Equipo");
		$equipos = $this->Equipo->find('list');
		$this->loadModel("Jugadore");
		$jugadores = $this->Jugadore->find('list', [
			'fields' => [
				'Jugadore.id',
				'Jugadore.nombre_jugador'
			]
		]);
        $this->loadModel("Paise");
        $paises = $this->Paise->find('list');
        $this->loadModel("Posicione");
        $posiciones = $this->Posicione->find('list');
		$this->set(compact('torneos', 'equipos', 'jugadores','jugadores','posiciones','paises'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Jugadoresxequipo", "jugadoresxequipos", "delete");
		if ($delete == true) {
			$this->Jugadoresxequipo->id = $id;
			if (!$this->Jugadoresxequipo->exists()) {
				throw new NotFoundException(__('Invalid jugadoresxequipo'));
			}

			if ($this->Jugadoresxequipo->delete()) {
				$this->Session->write("delete",1);
			} else {
				$this->Session->write("delete",0);
			}
			$this->redirect(array('action' => 'index'));
		}
	}
}
