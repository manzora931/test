<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Torneo $Torneo
 * @property PaginatorComponent $Paginator
 */
class TorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Torneo", "torneos", "index");
		$this->Paginator->settings = array('order'=>array('Torneo.torneo'=>'asc'));
		$this->Torneo->recursive = 0;
		include 'busqueda/torneos.php';
		$data = $this->Paginator->paginate('Torneo');
		$this->set('torneos', $data);

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipotorneo");
		$tipotorneos = $this->Tipotorneo->find('list');

		$this->set(compact('paises', 'tipotorneos'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[torneos]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Torneo", "torneos", "view");
		if (!$this->Torneo->exists($id)) {
			throw new NotFoundException(__('Invalid torneo'));
		}
		$options = array('conditions' => array('Torneo.' . $this->Torneo->primaryKey => $id));
		$this->set('torneo', $this->Torneo->find('first', $options));

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipotorneo");
		$tipotorneos = $this->Tipotorneo->find('list');

		$gruposxtorneo = $this->Torneo->Gruposxtorneo->find("all", [
		    "conditions"=>[
		        "Gruposxtorneo.torneo_id" => $id
            ]
        ]);
        $this->loadModel("Grupo");
        $this->Grupo->recursive = -1;
        $grupos = $this->Grupo->find("all",
            [
                "conditions"=>["Grupo.activo"=>1],
                "fields"=>["Grupo.id", "Grupo.grupo"],
                "order"=>["Grupo.grupo"]
            ]);
        $grupolist = [];
        foreach ($grupos as $item){
            $grupolist[$item["Grupo"]["id"]] = $item["Grupo"]["grupo"];
        }
        //debug($grupolist).die();
		$this->set(compact('paises', 'tipotorneos', "gruposxtorneo", "grupolist"));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Torneo", "torneos", "add");
		if ($this->request->is('post')) {
		    $this->loadModel("Gruposxtorneo");
			$desde = explode('-', $this->request->data['Torneo']['desde']);
			$hasta = explode('-', $this->request->data['Torneo']['hasta']);
			$this->request->data['Torneo']['desde'] = $desde[2] . '-' . $desde[1] . '-' . $desde[0];
			$this->request->data['Torneo']['hasta'] = $hasta[2] . '-' . $hasta[1] . '-' . $hasta[0];
			$this->request->data['Torneo']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Torneo']['modified']=0;
			$this->Torneo->create();
            $gruposxtorneos = $this->request->data["Grupoxtorneo"]["grupo_id"];
            $orden = $this->request->data["Grupoxtorneo"]["orden"];

			if ($this->Torneo->save($this->request->data)) {
				$torneo_id = $this->Torneo->id;
                foreach ($gruposxtorneos as $key => $value) {
                    if(!empty($value)){
                        $insert['torneo_id']=$torneo_id;
                        $insert['grupo_id']=$value;
                        $insert['orden']=(empty($orden[$key]))?0:$orden[$key];
                        $insert["created"] = date("Y-m-d H:i:s");
                        $insert["usuario"] = $this->Session->read('nombreusuario');
                        $this->Gruposxtorneo->create();
                        $this->Gruposxtorneo->set($insert);
                        if ($this->Gruposxtorneo->save()) {
                            $save=true;
                        }
                    }
                }
				$this->Session->write('torneo_save', 1);
				$this->redirect(['action' => 'view', $torneo_id]);
			} else {
				$this->Session->setFlash(__('El torneo no ha sido creado. Intentar de nuevo.'));
			}
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipotorneo");
		$tipotorneos = $this->Tipotorneo->find('list');
		/*****/
		$this->loadModel("Grupo");
		$grupos = $this->Grupo->find("list",
            ["conditions"=>["Grupo.activo"=>1],
                "order"=>["Grupo.grupo"]
            ]);

		$this->set(compact('paises', 'tipotorneos', "grupos"));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Torneo", "torneos", "edit");
		if (!$this->Torneo->exists($id)) {
			throw new NotFoundException(__('Invalid torneo'));
		}
        $gruposxtorneo = [];
		if ($this->request->is(array('post', 'put'))) {
		    $this->request->data["Torneo"]["desde"] = date("Y-m-d",strtotime($this->request->data["Torneo"]["desde"]));
		    $this->request->data["Torneo"]["hasta"] = date("Y-m-d",strtotime($this->request->data["Torneo"]["hasta"]));
			$this->request->data['Torneo']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Torneo']['modified'] = date("Y-m-d H:i:s");
            $gruposxtorneos = $this->request->data["Grupoxtorneo"]["grupo_id"];
            $orden = $this->request->data["Grupoxtorneo"]["orden"];
            $det_ids = $this->request->data["Grupoxtorneo"]["det_id"];

			if ($this->Torneo->save($this->request->data)) {
				$torneo_id = $this->Torneo->id;
                $dets = $this->Torneo->Gruposxtorneo->find("all", [
                    "conditions"=>[
                        "Gruposxtorneo.torneo_id" => $id
                    ]
                ]);
                $delete=true;
                foreach ($dets as $det){
                    foreach ($gruposxtorneos as $key => $value) {
                        if($det["Gruposxtorneo"]["id"] == $det_ids[$key]){
                            $delete = false;
                        }
                    }
                    if($delete){
                        $this->Torneo->Gruposxtorneo->id = $det["Gruposxtorneo"]["id"];
                        $this->Torneo->Gruposxtorneo->delete();
                    }
                    $delete = true;
                }
                $save=[];
                foreach ($gruposxtorneos as $key => $value) {
                    if($det_ids[$key] > 0){
                        $save["id"]=$det_ids[$key];
                        $save['torneo_id']=$torneo_id;
                        $save['grupo_id']=$value;
                        $save['orden']=(empty($orden[$key]))?0:$orden[$key];
                        $save["modified"] = date("Y-m-d H:i:s");
                        $save["usuariomodif"] = $this->Session->read('nombreusuario');
                        $this->Torneo->Gruposxtorneo->save($save);
                    }else if(!empty($value)){
                        $insert['torneo_id']=$torneo_id;
                        $insert['grupo_id']=$value;
                        $insert['orden']=(empty($orden[$key]))?0:$orden[$key];
                        $insert["created"] = date("Y-m-d H:i:s");
                        $insert["usuario"] = $this->Session->read('nombreusuario');
                        $this->Torneo->Gruposxtorneo->create();
                        $this->Torneo->Gruposxtorneo->set($insert);
                        if ($this->Torneo->Gruposxtorneo->save()) {
                            $saved=true;
                        }
                    }
                }

				$this->Session->write('torneo_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('El torneo no se ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Torneo.' . $this->Torneo->primaryKey => $id));
			$this->request->data = $this->Torneo->find('first', $options);
            //Det Torneos
            $gruposxtorneo = $this->Torneo->Gruposxtorneo->find("all", [
                "conditions"=>[
                    "Gruposxtorneo.torneo_id" => $id
                ]
            ]);
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipotorneo");
		$tipotorneos = $this->Tipotorneo->find('list');

        $this->loadModel("Grupo");
        $this->Grupo->recursive = -1;
        $grupos = $this->Grupo->find("all",
            [
                "conditions"=>["Grupo.activo"=>1],
                "fields"=>["Grupo.id", "Grupo.grupo"],
                "order"=>["Grupo.grupo"]
            ]);
        $grupolist = [];
        foreach ($grupos as $item){
            $grupolist[$item["Grupo"]["id"]] = $item["Grupo"]["grupo"];
        }

		$this->set(compact('paises', 'tipotorneos', "gruposxtorneo", "grupolist"));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Torneo", "torneos", "delete");
		if ($delete == true) {
			$this->Torneo->id = $id;
			if (!$this->Torneo->exists()) {
				throw new NotFoundException(__('Invalid torneo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Torneo->delete()) {
				$this->Session->setFlash(__('The torneo has been deleted.'));
			} else {
				$this->Session->setFlash(__('The torneo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}
}
