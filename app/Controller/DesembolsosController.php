<?php
App::uses('AppController', 'Controller');
/**
 * Desembolsos Controller
 *
 * @property Desembolso $Desembolso
 * @property PaginatorComponent $Paginator
 */
class DesembolsosController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
*/
	public function index($idProyecto = null) {
        $data = array();

        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

        if(!is_null($idProyecto)) {
            $this->Paginator->settings = array(
				'fields' => ['Proyecto.*', 'Financista.nombre', 'Fuentesfinanciamiento.*', 'Desembolso.*', 'Bancoorigen.nombre', 'Bancodestino.nombre','Destinatario.*', 'concat(Contacto.nombres, " ", Contacto.apellidos) as nombre'],
                'joins' => array(
                    array('table' => 'desembolsos', 'alias' => 'Desembolso', 'type' => 'INNER', 'conditions' => array('Desembolso.fuentefinanciamiento_id = Fuentesfinanciamiento.id')),
					array('table' => 'bancos', 'alias' => 'Bancoorigen', 'type' => 'INNER', 'conditions' => array('Desembolso.bancorigen_id = Bancoorigen.id')),
					array('table' => 'bancos', 'alias' => 'Bancodestino', 'type' => 'INNER', 'conditions' => array('Desembolso.bancdestino_id = Bancodestino.id')),
					array('table' => 'destinatarios', 'alias' => 'Destinatario', 'type' => 'INNER', 'conditions' => array('Desembolso.destinatario_id = Destinatario.id')),
					array('table' => 'contactos', 'alias' => 'Contacto', 'type' => 'INNER', 'conditions' => array('Destinatario.contacto_id = Contacto.id'))
                ),
                'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $idProyecto), 'order'=>array('Desembolso.fecha'=>'desc'));

            $this->Desembolso->Fuentesfinanciamiento->recursive = 0;
            $data = $this->Paginator->paginate('Fuentesfinanciamiento');
        }

        $this->set('desembolsos', $data);

        $fuentes = $this->Desembolso->Fuentesfinanciamiento->find("list", [
            'fields' => ['Fuentesfinanciamiento.id', 'Financista.nombre'],
			'joins' => array( array('table' => 'financistas', 'alias' => 'Financista', 'type' => 'INNER', 'conditions' => array('Fuentesfinanciamiento.financista_id = Financista.id'))),
			'conditions'=> ['Fuentesfinanciamiento.proyecto_id' => $idProyecto]
        ]);

		$this->Desembolso->Destinatario->recursive = 0;
		$destinatariosactivos = $this->Desembolso->Destinatario->find("all", [
			'fields' => ['Destinatario.id', 'Destinatario.nombre'],
			'conditions'=> ['Destinatario.activo' => 1]
		]);

		$destinatarios = array();
		foreach ($destinatariosactivos as $destinatario) {
			$destinatarios[$destinatario['Destinatario']['id']] = $destinatario['Destinatario']['nombre'];
		}

		$this->loadModel('Banco');
		$bancos = $this->Banco->find("list", [
			'fields' => ['Banco.id', 'Banco.nombre'],
			'conditions'=> ['Banco.activo' => 1]
		]);

		$meses = [1=>"Ene", 2=>"Feb", 3=>"Mar", 4=>"Abr", 5=>"May", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Sep", 10=>"Oct", 11=>"Nov", 12=>"Dic"];

		/** estado de proyecto***/
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive=-1;
        $infoP = $this->Proyecto->read("estado_id",$idProyecto);
        $admin = $this->get_perfil();
        $this->set(compact( 'idProyecto', 'fuentes', 'destinatarios', 'bancos', 'meses','infoP','admin'));
	}

    public function get_perfil(){
        $this->loadModel("Perfile");
        $this->Perfile->recursive=-1;
        $user = $this->Session->read("nombreusuario");
        $info = $this->Perfile->find("all",[
            'joins'=>[
            [
            'table'=>'users',
            'alias'=>'User',
            'type'=>'INNER',
            'conditions'=>[
            "Perfile.id=User.perfile_id"
                ]
            ]
        ],
        'conditions'=>[
            'User.username'=>$user
        ],
        'fields'=>['Perfile.admin']
        ]);
        $band = ($info[0]['Perfile']['admin']==1)?1:0;
        return $band;
    }


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Desembolso", "desembolsos", "view");
		if (!$this->Desembolso->exists($id)) {
			throw new NotFoundException(__('Invalid desembolso'));
		}
		$options = array('conditions' => array('Desembolso.' . $this->Desembolso->primaryKey => $id));
		$this->set('desembolso', $this->Desembolso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Desembolso", "desembolsos", "add");
		if ($this->request->is('post')) {
			$this->Desembolso->create();
			if ($this->Desembolso->save($this->request->data)) {
				$this->Session->setFlash(__('The desembolso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The desembolso could not be saved. Please, try again.'));
			}
		}
		$fuentefinanciamientos = $this->Desembolso->Fuentefinanciamiento->find('list');
		$bancorigens = $this->Desembolso->Bancorigen->find('list');
		$bancdestinos = $this->Desembolso->Bancdestino->find('list');
		$destinatarios = $this->Desembolso->Destinatario->find('list');
		$this->set(compact('fuentefinanciamientos', 'bancorigens', 'bancdestinos', 'destinatarios'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Desembolso", "desembolsos", "edit");
		if (!$this->Desembolso->exists($id)) {
			throw new NotFoundException(__('Invalid desembolso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Desembolso->save($this->request->data)) {
				$this->Session->setFlash(__('The desembolso has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The desembolso could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Desembolso.' . $this->Desembolso->primaryKey => $id));
			$this->request->data = $this->Desembolso->find('first', $options);
		}
		$fuentefinanciamientos = $this->Desembolso->Fuentefinanciamiento->find('list');
		$bancorigens = $this->Desembolso->Bancorigen->find('list');
		$bancdestinos = $this->Desembolso->Bancdestino->find('list');
		$destinatarios = $this->Desembolso->Destinatario->find('list');
		$this->set(compact('fuentefinanciamientos', 'bancorigens', 'bancdestinos', 'destinatarios'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
		$this->Desembolso->id = $_POST['id'];
		$proyectoid = $_POST['proyectoid'];

		if (!$this->Desembolso->exists()) {
			$resp['msg'] = 'error1';
		}

        $this->Desembolso->Fuentesfinanciamiento->recursive = 0;
        $desembolso = $this->Desembolso->Fuentesfinanciamiento->find('first', [
            'fields' => ['Financista.nombre', 'Desembolso.*', 'Bancoorigen.nombre', 'Bancodestino.nombre', 'concat(Contacto.nombres, " ", Contacto.apellidos) as nombre'],
            'joins' => array(
                array('table' => 'desembolsos', 'alias' => 'Desembolso', 'type' => 'INNER', 'conditions' => array('Desembolso.fuentefinanciamiento_id = Fuentesfinanciamiento.id')),
                array('table' => 'bancos', 'alias' => 'Bancoorigen', 'type' => 'INNER', 'conditions' => array('Desembolso.bancorigen_id = Bancoorigen.id')),
                array('table' => 'bancos', 'alias' => 'Bancodestino', 'type' => 'INNER', 'conditions' => array('Desembolso.bancdestino_id = Bancodestino.id')),
                array('table' => 'destinatarios', 'alias' => 'Destinatario', 'type' => 'INNER', 'conditions' => array('Desembolso.destinatario_id = Destinatario.id')),
                array('table' => 'contactos', 'alias' => 'Contacto', 'type' => 'INNER', 'conditions' => array('Destinatario.contacto_id = Contacto.id'))
            ),
            'conditions' => ['Desembolso.id' => $this->Desembolso->id]
        ]);

        $desembolsos = $this->Desembolso->find("all", [
            'conditions' => [
                'Desembolso.fuentefinanciamiento_id' => $desembolso['Desembolso']['fuentefinanciamiento_id']
            ],
            'fields' => ['SUM(Desembolso.total) as total']
        ]);

        $ocupado = $desembolsos[0][0]["total"];

        $this->loadModel('Gasto');
        $gastos = $this->Gasto->find('first', [
            'fields' => ['SUM(Gasto.monto) as total_gastos'],
            'conditions' => ['Actividade.fuentesfinanciamiento_id' => $desembolso['Desembolso']['fuentefinanciamiento_id']]
        ]);

        $restodesembolso = (((float)$ocupado - (float)$desembolso['Desembolso']['monto']));
        $total_gastos = (float)$gastos[0]['total_gastos'];

        if($restodesembolso >= $total_gastos) {
            if ($this->Desembolso->delete()) {
                $resp['msg'] = 'exito';

                $_SESSION['logsproyecto']['accion'] = 'Eliminar';
                $_SESSION['logsproyecto']['data'] = $desembolso['Desembolso'];
                $_SESSION['logsproyecto']['data']['ffinanciamiento'] = $desembolso['Financista']['nombre'];
                $_SESSION['logsproyecto']['data']['bancoorigen'] = $desembolso['Bancoorigen']['nombre'];
                $_SESSION['logsproyecto']['data']['bancodestino'] = $desembolso['Bancodestino']['nombre'];
                $_SESSION['logsproyecto']['data']['destinatario'] = $desembolso[0]['nombre'];

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Desembolso', 'desembolsos', 'Desembolsos');
            } else {
                $resp['msg'] = 'error2';
            }
        } else {
            $resp['msg'] = 'error3';
        }

		$this->Paginator->settings = array(
			'fields' => ['Proyecto.*', 'Financista.nombre', 'Fuentesfinanciamiento.*', 'Desembolso.*', 'Bancoorigen.nombre', 'Bancodestino.nombre','Destinatario.*', 'concat(Contacto.nombres, " ", Contacto.apellidos) as nombre'],
			'joins' => array(
				array('table' => 'desembolsos', 'alias' => 'Desembolso', 'type' => 'INNER', 'conditions' => array('Desembolso.fuentefinanciamiento_id = Fuentesfinanciamiento.id')),
				array('table' => 'bancos', 'alias' => 'Bancoorigen', 'type' => 'INNER', 'conditions' => array('Desembolso.bancorigen_id = Bancoorigen.id')),
				array('table' => 'bancos', 'alias' => 'Bancodestino', 'type' => 'INNER', 'conditions' => array('Desembolso.bancdestino_id = Bancodestino.id')),
				array('table' => 'destinatarios', 'alias' => 'Destinatario', 'type' => 'INNER', 'conditions' => array('Desembolso.destinatario_id = Destinatario.id')),
				array('table' => 'contactos', 'alias' => 'Contacto', 'type' => 'INNER', 'conditions' => array('Destinatario.contacto_id = Contacto.id'))
			),
			'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $proyectoid), 'order'=>array('Desembolso.fecha'=>'desc', 'Financista.nombre'=>'asc'));

		$this->Desembolso->Fuentesfinanciamiento->recursive = 0;
		$resp['data'] = $this->Paginator->paginate('Fuentesfinanciamiento');

		$sql = "SELECT sum(desembolsos.monto) as montototal, sum(desembolsos.comision) as totalcomisiones,  sum(desembolsos.total) as totaltotal
				FROM desembolsos INNER JOIN fuentesfinanciamientos ON desembolsos.fuentefinanciamiento_id = fuentesfinanciamientos.id
				WHERE fuentesfinanciamientos.proyecto_id = '" . $proyectoid . "'";
		$resp['totales'] = $this->Desembolso->query($sql);

		echo json_encode($resp);
		$this->autoRender=false;
	}

	public function get_data() {
		$id = $_POST['id'];
		$sql = "SELECT fuentefinanciamiento_id, bancorigen_id, bancdestino_id, destinatario_id, fecha, codigo, monto, comision 
				FROM desembolsos WHERE id = " . $id;
		$desembolso['data'] = $this->Desembolso->query($sql);

		$sql = "SELECT ba.id, ba.nombre FROM  bancos ba WHERE ba.id != '" . $_POST['id'] . "' ";
		$desembolso['bancos'] = $this->Desembolso->query($sql);

		$desembolso['msg'] = 'exito';
		echo json_encode($desembolso);
		$this->autoRender=false;
	}

	public function almacenar_data()
    {
        $id = $_POST['id'];
        $proyectoid = $_POST['proyectoid'];
        $fuenteid = $_POST['fuentefinan'];
        $fecha = explode('-', $_POST['fecha']);

        $montototal = (float)$_POST['monto'] + (float)$_POST['comision'];

        $info = $this->Desembolso->find("all", [
            'conditions' => [
                'Desembolso.fuentefinanciamiento_id' => $fuenteid
            ],
            'fields' => ['SUM(Desembolso.total) as total']
        ]);
        $this->loadModel("Fuentesfinanciamiento");
        $this->Fuentesfinanciamiento->recursive=-1;
        $infoF = $this->Fuentesfinanciamiento->find("all",[
            'conditions'=>[
                'Fuentesfinanciamiento.id'=>$fuenteid
            ],
            'fields'=>['Fuentesfinanciamiento.monto']
        ]);
        $montoF = $infoF[0]['Fuentesfinanciamiento']['monto'];
		$ocupado = $info[0][0]["total"];
		$restofinanciamiento = $montoF - $ocupado;

		if($restofinanciamiento >= $montototal) {
			if($id != '') {
                $this->loadModel('Gasto');
                $gastos = $this->Gasto->find('first', [
                    'fields' => ['SUM(Gasto.monto) as total_gastos'],
                    'conditions' => ['Actividade.fuentesfinanciamiento_id' => $fuenteid]
                ]);

                $total_gastos = (float)$gastos[0]['total_gastos'];
                $desembolso = $this->Desembolso->find('first', [
					'fields' => ['Desembolso.*'],
					'conditions' => ['Desembolso.id' => $id]
				]);
                $desembolsado = (((float)$ocupado - (float)$desembolso['Desembolso']['monto']) + (float)$_POST['monto']);

				if($desembolsado >= $total_gastos) {
                    if(count($desembolso) > 0) {
                        $desembolso['Desembolso']['fuentefinanciamiento_id'] = $fuenteid;
                        $desembolso['Desembolso']['bancorigen_id'] = $_POST['borigen'];
                        $desembolso['Desembolso']['bancdestino_id'] = $_POST['bdestino'];
                        $desembolso['Desembolso']['destinatario_id'] = $_POST['destinatario'];
                        $desembolso['Desembolso']['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                        $desembolso['Desembolso']['codigo'] = $_POST['codigo'];
                        $desembolso['Desembolso']['monto'] = $_POST['monto'];
                        $desembolso['Desembolso']['comision'] = $_POST['comision'];
                        $desembolso['Desembolso']['total'] = $montototal;
                        $desembolso['Desembolso']['usuariomodif'] = $this->Session->read('nombreusuario');
                        $desembolso['Desembolso']['modified'] = date('Y-m-d H:i:s');
                        if ($this->Desembolso->save($desembolso)) {
                            $_SESSION['logsproyecto']['accion'] = 'Modificar';
                            $_SESSION['logsproyecto']['data'] = $desembolso['Desembolso'];
                            $_SESSION['logsproyecto']['data']['ffinanciamiento'] = $_POST['ffinanciamiento'];
                            $_SESSION['logsproyecto']['data']['bancoorigen'] = $_POST['bancoorigen'];
                            $_SESSION['logsproyecto']['data']['bancodestino'] = $_POST['bancodestino'];
                            $_SESSION['logsproyecto']['data']['destinatario'] = $_POST['ndestinatario'];
                            $this->Session->write("savedesembolso",1);
                            $resp['id'] = $id;
                            $resp['msg'] = 'exito';
                            $this->loadModel("Logproyecto");
                            $this->Logproyecto->registro_bitacora('Desembolso', 'desembolsos', 'Desembolsos');
                        } else {
                            $resp['msg'] = 'error1';
                        }
                    }
                } else {
                    $resp['msg'] = 'error4';
                }
			} else {
				$data['fuentefinanciamiento_id'] = $fuenteid;
				$data['bancorigen_id'] = $_POST['borigen'];
				$data['bancdestino_id'] = $_POST['bdestino'];
				$data['destinatario_id'] = $_POST['destinatario'];
				$data['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
				$data['codigo'] = $_POST['codigo'];
				$data['monto'] = $_POST['monto'];
				$data['comision'] = $_POST['comision'];
				$data['total'] = $montototal;
				$data['usuario'] = $this->Session->read('nombreusuario');
				$data['created'] = date('Y-m-d H:i:s');
				$data['modified'] = 0;

				$this->Desembolso->create();
				if ($this->Desembolso->save($data)) {
                    $data['id'] = $this->Desembolso->id;
                    $data['ffinanciamiento'] = $_POST['ffinanciamiento'];
                    $data['bancoorigen'] = $_POST['bancoorigen'];
                    $data['bancodestino'] = $_POST['bancodestino'];
                    $data['destinatario'] = $_POST['ndestinatario'];

                    $_SESSION['logsproyecto']['accion'] = 'Agregar';
                    $_SESSION['logsproyecto']['data'] = $data;
                    $this->Session->write("savedesembolso",1);
					$resp['id'] = $this->Desembolso->id;
					$resp['msg'] = 'exito';

                    $this->loadModel("Logproyecto");
                    $this->Logproyecto->registro_bitacora('Desembolso', 'desembolsos', 'Desembolsos');
				} else {
					$resp['msg'] = 'error1';
				}
			}
		} elseif($montototal > $montoF) {
			$resp['msg'] = 'error2';
		} elseif($montototal > $restofinanciamiento) {
			$resp['msg'] = 'error3';
		}

		$this->Paginator->settings = array(
			'fields' => ['Proyecto.*', 'Financista.nombre', 'Fuentesfinanciamiento.*', 'Desembolso.*', 'Bancoorigen.nombre', 'Bancodestino.nombre','Destinatario.*', 'concat(Contacto.nombres, " ", Contacto.apellidos) as nombre'],
			'joins' => array(
				array('table' => 'desembolsos', 'alias' => 'Desembolso', 'type' => 'INNER', 'conditions' => array('Desembolso.fuentefinanciamiento_id = Fuentesfinanciamiento.id')),
				array('table' => 'bancos', 'alias' => 'Bancoorigen', 'type' => 'INNER', 'conditions' => array('Desembolso.bancorigen_id = Bancoorigen.id')),
				array('table' => 'bancos', 'alias' => 'Bancodestino', 'type' => 'INNER', 'conditions' => array('Desembolso.bancdestino_id = Bancodestino.id')),
				array('table' => 'destinatarios', 'alias' => 'Destinatario', 'type' => 'INNER', 'conditions' => array('Desembolso.destinatario_id = Destinatario.id')),
				array('table' => 'contactos', 'alias' => 'Contacto', 'type' => 'INNER', 'conditions' => array('Destinatario.contacto_id = Contacto.id'))
			),
			'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $proyectoid), 'order'=>array('Desembolso.fecha'=>'desc', 'Financista.nombre'=>'asc'));

		$this->Desembolso->Fuentesfinanciamiento->recursive = 0;
		$resp['data'] = $this->Paginator->paginate('Fuentesfinanciamiento');

		$sql = "SELECT sum(desembolsos.monto) as montototal, sum(desembolsos.comision) as totalcomisiones,  sum(desembolsos.total) as totaltotal
				FROM desembolsos INNER JOIN fuentesfinanciamientos ON desembolsos.fuentefinanciamiento_id = fuentesfinanciamientos.id
				WHERE fuentesfinanciamientos.proyecto_id = '" . $proyectoid . "'";

		$resp['totales'] = $this->Desembolso->query($sql);

		echo json_encode($resp);
		$this->autoRender=false;
	}

	public function get_bancos(){
		$id = $_POST['id'];

		$this->loadModel('Banco');
		$data = $this->Banco->find("list", [
			'fields' => ['Banco.id', 'Banco.nombre'],
			'conditions'=> [
				'Banco.id !=' => $id,
				'Banco.activo' => 1
			]
		]);

		$bancos = "<option value=''> Seleccionar </option>";
		foreach($data as $key => $row){
			$bancos.= "<option value='".$key."'>".$row."</option>";
		}

		echo $bancos;
		$this->autoRender=false;
	}
}
