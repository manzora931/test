<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');
/**
 * User Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{
	var $components = array('Paginator', 'Session');
    public function index() {
        if (isset($_SESSION['tabla[users]']['activa']) || isset($_SESSION['tabla[users]']['activo'])) {
            $this->paginate=array('conditions'=>array('User.activo'=>0),'order'=>array('User.name'=>'ASC'));
        } else {
            $this->paginate=array('conditions'=>array('User.activo'=>1),'order'=>array('User.name'=>'ASC'));
        }

        $this->User->recursive = 0;
        include 'busqueda/user.php';
        $data = $this->Paginator->paginate('User');
        $botones = $this->mostrarBotones("User", "users");
        $this->set('users', $data);
        $this->set(compact('botones'));
        $institucion_id=$this->User->query("SELECT id,nombre FROM institucions WHERE activo=1");

        foreach ($institucion_id as $value) {
            $res[$value['institucions']['id']]= $value['institucions']['nombre'];
        }
        $this->set('selectInsttucion',$res);

    }

    public function vertodos() {
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[users]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('No valido user'));
        }
        $this->loadModel("Detusuario");
        $this->loadModel("Torneo");
        $Inst= $this->User->find('first',
            array(
                'joins'=>
                    array(
                        array('table' => 'contactos',
                            'alias' => 'con',
                            'type' => 'inner',
                            'conditions' => array(
                                'User.contacto_id = con.id'
                            )
                        ),
                        array('table' => 'institucions',
                            'alias' => 'ins',
                            'type' => 'inner',
                            'conditions' => array(
                                'con.institucion_id = ins.id'
                            )
                        )
                    ),
                'fields'=>array('ins.nombre'),
                'conditions'=>['User.id'=>$id],
                'recursive'=>-1
            ));

        $this->set('user', $this->User->read(null, $id));
        //Torneos
        $torneos = $this->Torneo->find("list", [
            "conditions" => [
                "Torneo.activo" => 1
            ]
        ]);
        //debug($torneos);
        //torneos del usuario
        $det = $this->Detusuario->find("all",[
            "conditions"=>[
                "Detusuario.user_id"=>$id
            ],
            "fields"=>[
                "Detusuario.torneo_id"
            ]
        ]);
        $this->set(compact("Inst", "torneos", "det"));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['User']['created'] = date('Y-m-d H:m:s');
			$this->request->data['User']['modified'] = 0;
            $pred = $this->request->data["User"]["instituciondefault"];
            if($pred == 1){
                //QUITA EL PREDETERMINADO A TODOS LOS USUARIOS DE LA INSTITUCION
                $this->loadModel("Contacto");
                $this->Contacto->recursive=-1;
                $info = $this->Contacto->read("institucion_id", $this->request->data["User"]["contacto_id"]);
                $institucion = $info["Contacto"]["institucion_id"];
                $this->User->query("UPDATE users set instituciondefault = 0 WHERE institucion_id = ".$institucion);
            }
            //debug($this->request->data);
            //die();
            if ($this->User->save($this->request->data)) {
                $user_id = $this->User->id;
                //Proceso para almacenar torneos
                $torneos = $this->request->data["Torneo"];
                $this->loadModel("Detusuario");
                foreach ($torneos as $item){
                    if($item["torneo_id"]!="Seleccionar" && $item["torneo_id"]!=''){
                        $this->Detusuario->create();
                        $det["user_id"]     = $user_id;
                        $det["torneo_id"]   = $item["torneo_id"];
                        $det["usuario"]     = $this->Session->read('nombreusuario');
                        $det["created"]     = date("Y-m-d H:i:s");
                        $det["modified"]    = 0;
                        //$this->Detusuario->set($det);
                        $this->Detusuario->save($det);
                    }
                }
                $this->Session->write("user_save", 1);
                $this->redirect(array('action' => 'view',$user_id));
                return $this->flash(__('El usuario ha sido creado.'), array('action' => 'view',$user_id));
            } else {
                $this->Session->setFlash(__('El usuario NO ha sido creado. Intentar de nuevo.'));
            }
        }

        $perfiles = $this->User->Perfile->find("list", [
			'conditions'=> [
				'Perfile.activo' => 1
			]
		]);

        $usersasignados = $this->User->find("all", [
            'fields' => [
                'Contacto.id',
            ],
            'conditions'=> [
                'User.activo' => 1,
                'User.contacto_id !=' => null
            ]
        ]);

        $contactosasignados = array();
        foreach ($usersasignados as $contacto) {
            array_push($contactosasignados, $contacto['Contacto']['id']);
        }

        $contactos = $this->User->Contacto->find("list", [
            'fields' => [
                'Contacto.id',
                'Contacto.nombre_contacto'
            ],
            'conditions'=> [
                'Contacto.activo' => 1,
                'Contacto.id !=' => $contactosasignados
            ]
        ]);

        $institucions = $this->User->Contacto->Institucion->find('list');

        $this->loadModel('Paise');
        $this->loadModel('Torneo');
        $paises = $this->Paise->find('list');
        //Torneos
        $torneos = $this->Torneo->find("list", [
            "conditions" => [
                "Torneo.activo" => 1
            ]
        ]);
        $this->set(compact("perfiles", "contactos", "paises", "institucions", "torneos"));
    }

    public function edit($id = null) {
        $this->User->id = $id;
        $this->loadModel("Torneo");
        $this->loadModel("Detusuario");
        $this->loadModel("Contacto");
        if (!$this->User->exists()) {
            throw new NotFoundException(__('No valido user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['User']['usuariomodif'] = $this->Session->read('nombreusuario');
            $pred = $this->request->data["User"]["instituciondefault"];

            if($pred == 1){
                //QUITA EL PREDETERMINADO A TODOS LOS USUARIOS DE LA INSTITUCION
                $this->Contacto->recursive=-1;
                $info = $this->Contacto->read("institucion_id", $this->request->data["User"]["contacto_id"]);
                $institucion = $info["Contacto"]["institucion_id"];
                $this->User->query("UPDATE users set instituciondefault = 0 WHERE institucion_id = ".$institucion);
            }
            //SE VERIFICA SI SE BORRO ALGUN TORNEO
            $det = $this->Detusuario->find("all",[
                "conditions"=>[
                    "Detusuario.user_id"=>$id
                ],
                "fields"=>[
                    "Detusuario.torneo_id","Detusuario.id"
                ]
            ]);
            $delete=1;
            foreach ($det as $item){//Recorre los torneos almacenados
                foreach ($this->request->data["Torneo"] as $item2){//Recorre los torneos enviados en el post
                    if($item["Detusuario"]["torneo_id"] == $item2["torneo_id"]){
                        $delete=0;
                    }
                }
                if($delete==1){
                    $this->Detusuario->id = $item["Detusuario"]["id"];
                    $this->Detusuario->delete();
                }
                $delete=1;
            }

            if ($this->User->save($this->request->data)) {
                //Verifica si hay torneos nuevos
                $save=1;
                foreach ($this->request->data["Torneo"] as $item2){//Recorre los torneos almacenados
                    foreach ($det as $item){//Recorre los torneos enviados en el post
                        if($item["Detusuario"]["torneo_id"] == $item2["torneo_id"]){
                            $save=0;
                        }
                    }
                    if($save==1){
                        if($item2["torneo_id"]!='Seleccionar' || $item2["torneo_id"]!=''){
                            $this->Detusuario->create();
                            $det["user_id"]     = $id;
                            $det["torneo_id"]   = $item2["torneo_id"];
                            $det["usuario"]     = $this->Session->read('nombreusuario');
                            $det["created"]     = date("Y-m-d H:i:s");
                            $det["modified"]    = 0;
                            $this->Detusuario->save($det);
                        }
                    }
                    $save=1;
                }

                $this->Session->write("user_save",1);
                $this->redirect(array('action' => 'view',$id));
                $this->Session->setFlash(__('El user fué almacenado.'));
                return $this->redirect(array('action' => 'view',$id));
            } else {
                $this->Session->write("user_save",0);
                $this->redirect(array('action' => 'view',$id));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }

        $perfiles = $this->User->Perfile->find("list", [
            'conditions' => [
                'Perfile.activo' => 1
            ]
        ]);

        $usersasignados = $this->User->find("all", [
            'fields' => [
                'Contacto.id',
            ],
            'conditions'=> [
                'User.activo' => 1,
                'User.contacto_id !=' => null
            ]
        ]);

        $contactosasignados = array();
        foreach ($usersasignados as $contacto) {
            array_push($contactosasignados, $contacto['Contacto']['id']);
        }

        $contactos = $this->User->Contacto->find("list", [
            'fields' => [
                'Contacto.id',
                'Contacto.nombre_contacto'
            ],
            'conditions'=> [
                'Contacto.activo' => 1,
                'OR' => [
                    'Contacto.id !=' => $contactosasignados,
                    'Contacto.id' => $this->request->data['User']['contacto_id']
                ]
            ]
        ]);

        $institucions = $this->User->Contacto->Institucion->find('list');

        $this->loadModel('Paise');
        $paises = $this->Paise->find('list');

        $Inst= $this->User->find('first',
            array(
                'joins'=>
                    array(
                        array('table' => 'contactos',
                            'alias' => 'con',
                            'type' => 'inner',
                            'conditions' => array(
                                'User.contacto_id = con.id'
                            )
                        ),
                        array('table' => 'institucions',
                            'alias' => 'ins',
                            'type' => 'inner',
                            'conditions' => array(
                                'con.institucion_id = ins.id'
                            )
                        )
                    ),
                'fields'=>array('ins.nombre'),
                'conditions'=>['User.id'=>$id],
                'recursive'=>-1
            ));
        //Torneos

        $torneos = $this->Torneo->find("list", [
            "conditions" => [
                "Torneo.activo" => 1
            ]
        ]);
        //torneos del usuario
        $det = $this->Detusuario->find("all",[
            "conditions"=>[
                "Detusuario.user_id"=>$id
            ],
            "fields"=>[
                "Detusuario.torneo_id"
            ]
        ]);
        //debug($det).die();
        $this->set(compact('perfiles', 'contactos', "paises", "institucions","Inst","torneos","det"));
    }

    public function val_default(){
        $insti = $_POST['insti'];
        //$this->user->recursive=-1;
        $user = $this->User->find("count",[
            'conditions'=>[
                'User.institucion_id' => $insti,
                'User.instituciondefault' => 1,
                'User.id != ' => $_POST['user_id']
            ],
            'fields' =>[
                'User.id'
            ]
        ]);
        echo $user;
        $this->autoRender=false;
    }
    public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
    $this->Auth->allow('logout','sendemail','contrasenia');
	}

	public function login() {
        if ($this->request->is('post')) {
            $ip  =  (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']))?$_SERVER['REMOTE_ADDR']:'';
            $fecha  = date('Y-m-d H:i:s');
            $user   = $this->data['User']['username'];
			if ($this->Auth->login()) {
				$this->Session->write('nombreusuario', $this->data['User']['username']);
                $consulta =& $this->User;
                $val_perf=$this->val_perfil($this->data['User']['username']);
                if($val_perf==0){
                    $this->Session->setFlash(__('Usuario inactivo, consulte con el administrador.'), 'default', array(), 'error');
                    $this->Session->write("perfil_inac",1);

                } else {
                    $usuario = $this->User->find('first', [
                        'fields' => [
                            'id','perfile_id'
                        ],
                        'conditions' => [
                            'User.username' => $this->data['User']['username']
                        ]
                    ]);

                    $this->Session->write('userId', $usuario['User']['id']);

                    $stringJson = '{"login":{"usuario":"'.$user.'","ingreso":"Correcto","ip":"'.$ip.'"}}';
                    $this->User->query("INSERT INTO logs(fecha, log) VALUES ('".$fecha."', '".$stringJson."')");
                    if($usuario['User']["perfile_id"]==5){
                        return $this->redirect(['controller' => 'informes', 'action' => 'index']);
                    }else if($usuario['User']["perfile_id"]==2){
                        return $this->redirect(['controller' => 'juegos', 'action' => 'index']);
                    }else{
                        return $this->redirect(['controller' => 'informes', 'action' => 'index']);
                    }

                }
                // Crea una variable de sesión para el nombre completo del usuario

			}else{ // debug('else');
                $stringJson = '{"login":{"usuario":"'.$user.'","ingreso":"Incorrecto","ip":"'.$ip.'"}}';
                $this->User->query("INSERT INTO logs(fecha, log) VALUES ('".$fecha."', '".$stringJson."')");
                /*QUERY PARA VERIFICAR EL ESTADO DE UN USUARIO*/
                $sql = $this->User->query("SELECT activo FROM users WHERE username = '".$this->data['User']['username']."'");
                /*SI EL QUERY RETORNA RESULTADOS Y EL CAMPO
                  ES IGUAL A 0 EL USUARIO ESTA INACTIVO*/
                if(count($sql)>0 && $sql[0]['users']['activo'] == 0){
                    $this->Session->setFlash(__('Usuario inactivo, consulte con el administrador.'), 'default', array(), 'error');
                    $this->Session->write("user_inac",1);
                }else{
                    $this->Session->setFlash(__('Usuario o contraseña invalida.'), 'default', array(), 'error');
                    $this->Session->write("datos_incorrec",1);
                }

            }
			$this->Session->setFlash(__('Usuario No Valido, intente otra vez'));

		}




	}
    public function val_perfil($user){
        $sqlper = $this->User->query("SELECT p.activo FROM users u INNER JOIN perfiles p ON p.id = u.perfile_id WHERE u.username = '".$user."'");
            if(count($sqlper )>0 &&$sqlper [0]['p']['activo'] == 0){
                return 0;

            }else if(count($sqlper )>0 && $sqlper [0]['p']['activo'] == 1) {
                return 1;
            }
    }

	public function logout() {
	    $this->Session->delete('userId');
	    $this->Session->destroy();
		return $this->redirect($this->Auth->logout());
	}
    public function get_url(){
        $this->validarBotones('User', 'users', 'get_url');
        if($this->request->is('post')){
            //no recibe nada todo se hace con ajax
        }else{
            $users = $this->User->find('list',array('conditions'=>array('User.activo'=>1)));
            $this->set(compact('users'));

        }
    }
    public function generar(){
        $user = $_POST['user'];
        $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
        $now = date("Y-m-d H:i:s");
        $random = rand(100,1000);
        $upd = "UPDATE users SET fechareset = '$now', numrandom = '$random' WHERE id = '$user'";
        $this->User->query($upd);
        $url = $real_url.'contrasenia/'.$user.'-'.$random;
        echo $url;
        $this->autoRender=false;
    }
    //*********ENVIO DE EMAIL PARA REESTABLECER CONTRASENIA
/*
    public function sendemail() {
        if ($this->request->is('post')) {
            $param = $this->request->data['User']['param'];
            if ($this->userExist($param)) {
                $user = $this->getUser($param);
                if ($user['User']['activo']) {
                    if ($user['User']['email']) {
                        $now = date("Y-m-d H:i:s");
                        $random = md5(rand(100,1000));
                        $email = new CakeEmail();
                        $url = Router::url(['controller' => 'users', 'action' => 'contrasenia', $user['User']['id'], $random], true);

                        $this->User->id = $user['User']['id'];
                        $this->User->saveField('fechareset', $now);
                        $this->User->saveField('numrandom', $random);

                        $nuevo_cont="<label>Se recibio una solicitud para restablecer la contraseña, acceda al siguiente enlace para efectuar los cambios.</label>
<br><br/>
<a href='".$url."'>Restablecer Contraseña aqui.</a>
<br/><br/><br/>";

                        $merge_data = new stdClass();
                        $merge_data->PerMessage =
                            array(
                                array(
                                    array(
                                        'Field'=>'DeliveryAddress',
                                        'Value'=>$user['User']['email']  //$correodestino
                                    )
                                )
                            );

                        $request = array(
                            'header' => array('Content-Type' => 'application/json',
                            ),
                        );

                        $data = new stdClass();
                        $data->ServerId='16207';
                        $data->ApiKey= 'Nc95Cog4A2ZrMn87SbLt';
                        /***/
                      /*  $data->Messages =
                            array(
                                array(
                                    'MergeData'=>$merge_data,
                                    'Subject'=>'Soporte - Tenologias101',
                                    'To'=>
                                        array(
                                            array(
                                                'EmailAddress'=>'%%DeliveryAddress%%'
                                            )
                                        ),
                                    'From'=>
                                        array(
                                            'EmailAddress'=>'soporte@tecnologias101.com'
                                        ),
                                    'HtmlBody'=> $nuevo_cont
                                )
                            );
                        //debug($data);
                        $bodyJson = json_encode($data);
                        $HttpSocket = new HttpSocket();
                        $result = $HttpSocket->post('https://inject.socketlabs.com/api/v1/email',$bodyJson,$request);



                        /* $email->sender('soporte@tecnologias101.com', 'Soporte - Tenologias101');
                         $email->template('cambio_password')->viewVars(array('url' => $url))->emailFormat('html');
                         $email->from(array('soporte@tecnologias101.com' => 'Soporte - Tenologias101'));
                         $email->to($user['User']['email']);
                         $email->subject('Cambio Solicitado');
                         $email->send();*/

                    /*    $this->Session->write('passwordRestoreEmailSent', 1);
                        $this->redirect(['action' => 'login']);
                    } else {
                        $this->Session->write('passwordRestoreNoEmail', 1);
                        $this->redirect(['action' => 'login']);
                    }
                } else {
                    $this->Session->write('passwordRestoreInactive', 1);
                    $this->redirect(['action' => 'login']);
                }
            }else{
                $this->Session->write('passwordRestoreNoExist', 1);
                $this->redirect(['action' => 'login']);
            }
        }
    }*/
    private function userExist($param) {
        return $this->User->hasAny([
            'OR' => [
                'User.username' => $param,
                'User.email' => $param
            ]
        ]);
    }

    private function getUser($param) {
        return $this->User->find("first", [
            'conditions' => [
                'OR' => [
                    'User.id' => $param,
                    'User.username' => $param,
                    'User.email' => $param
                ]
            ]
        ]);
    }


    public function contrasenia($id = null, $hash = null)
    {
        $user = $this->getUser($id);
        $resetDate = strtotime($user['User']['fechareset']);
        $now = time();
        $days = ($now - $resetDate) / 86400;

        if ($hash == $user['User']['numrandom']) {
            if ($days < 1) {
                if ($this->request->is('post')) {
                    $this->User->id = $id;
                    if ($this->User->saveField('password', $this->request->data['User']['password'])) {
                        /*$email = new CakeEmail();
                        $email->sender('soporte@tecnologias101.com', 'Soporte - Tenologias101');
                        $email->template('confirm')->emailFormat('html');
                        $email->from(array('soporte@tecnologias101.com' => 'Soporte - Tenologias101'));
                        $email->to($user['User']['email']);
                        $email->subject('Cambio Realizado');
                        $email->send();*/
                        /**---------LECTURA DE DATOS DE ORGANIZACION---------**/
                        $this->loadModel("Organizacion");
                        $mail_org = $this->Organizacion->read("emailnotification",1);//CORREO DE LA INSTITUCION
                        $name_org = $this->Organizacion->read("namenotification",1);//NOMBRE DE LA INSTITUCION
                        /**------------------------------------------*/
                        $sender = $mail_org['Organizacion']['emailnotification'];
                        $name_sender = $name_org['Organizacion']['namenotification'];
                        $nuevo_cont="<div style='width: 100px !important;'><img src='http://vertigo-cloud.com/img/LOGOS-UNIDOS.png' style='width: 100px !important;'></div>
<img src='https://monitoreo.redca.org/img/linea%20azul-email.png' style='width:100%; height: 15px;'>
<p>Contraseña Restablecida.</p>
<p><br /><img src='https://monitoreo.redca.org/img/sellos-de-seguridad.png'></p>
<img src='https://monitoreo.redca.org/img/footer.png' style='width:100%; height: 15px;'>
<div class='footer-links'>
	<label style='margin-top:8px;display: inline-block;'>Más información en <a href='#'>soporte@sportsma.info</a></label>";
                        $merge_data = new stdClass();
                        $merge_data->PerMessage =
                            array(
                                array(
                                    array(
                                        'Field'=>'DeliveryAddress',
                                        'Value'=>$user['User']['email']  //$correodestino
                                    )
                                )
                            );

                        $request = array(
                            'header' => array('Content-Type' => 'application/json',
                            ),
                        );

                        $data = new stdClass();
                        $data->ServerId='16207';
                        $data->ApiKey= 'Nc95Cog4A2ZrMn87SbLt';
                        /***/
                        $data->Messages =
                            array(
                                array(
                                    'MergeData'=>$merge_data,
                                    'Subject'=>"Cambio Realizado - SMA",
                                    'To'=>
                                        array(
                                            array(
                                                'EmailAddress'=>'%%DeliveryAddress%%'
                                            )
                                        ),
                                    'From'=>
                                        array(
                                            'EmailAddress'=>$sender
                                        ),
                                    'HtmlBody'=> $nuevo_cont
                                )
                            );
                        //debug($data);
                        $bodyJson = json_encode($data);
                        $HttpSocket = new HttpSocket();
                        $result = $HttpSocket->post('https://inject.socketlabs.com/api/v1/email',$bodyJson,$request);

                        $this->Session->write('passwordRestoreSuccess', 1);
                        //$this->User->saveField('fechareset', '1000-10-10 00:00:00');
                        $this->redirect(['action' => 'login']);
                    } else {
                        $this->Session->write('passwordRestoreError', 1);
                    }
                } else {
                    $this->Session->write('passwordRestoreHash', $hash);
                    $this->Session->write('passwordRestoreUserId', $id);

                    //$this->redirect(['controller' => 'users', 'action' => 'login']);
                }
            } else {
                $this->Session->write('passwordRestoreExpired', 1);
                $this->redirect(['action' => 'login']);
            }
        } else {
            $this->Session->write('passwordRestoreNotPermited', 1);
            $this->redirect(['action' => 'login']);
        }
    }


    public function crear_contacto() {
        $data = $_POST['data'];
        $data['activo'] = 1;
        $data['usuario'] = $this->Session->read('nombreusuario');
        $data['created'] = date('Y-m-d H:m:s');
        $data['modified'] = 0;

        $this->User->Contacto->create();
        $this->User->Contacto->set($data);

        if ($this->User->Contacto->save()) {
            $contacto['id'] = $this->User->Contacto->id;

            $sql = "SELECT nombres, apellidos FROM contactos WHERE id = " . $contacto['id'];
            $contacto['data'] = $this->User->query($sql);

            $contacto['msg'] = 'exito';
        } else {
            $contacto['msg'] = 'error';
        }

        echo json_encode($contacto);
        $this->autoRender=false;
    }

    public function valNotificion()
    {
        $valor = $_POST['dato'];
        $idCont = $_POST['idCont'];
        $idIns=0;
    /*    if($_POST['ban']!=0)
        {
            $idIns= $this->User->find('all',
                array(
                'joins'=>
                    array(
                        array('table' => 'contactos',
                    'alias' => 'con',
                    'type' => 'inner',
                    'conditions' => [
                        'con.id = User.contacto_id',
                        'con.id' =>$idCont,
                        'User.id !='=>$_POST['ban'],
                        'User.instituciondefaul' => $valor
                    ])),
                'fields'=>array('con.institucion_id'),
            'recursive'=>-1
        ));
          debug($idIns);
        }
        else
        {
            $idIns= $this->User->Contacto->find('list',['conditions'=>array('id'=>$idCont),
                'fields'=>'institucion_id']);

        }*/
        $idIns= $this->User->Contacto->find('list',['conditions'=>array('id'=>$idCont),
            'fields'=>'institucion_id']);
                $options['joins'] = array(
            array('table' => 'contactos',
                'alias' => 'cont',
                'type' => 'inner',
                'conditions' => array(
                    'cont.id = User.contacto_id',
                )
            ),
            array('table' => 'institucions',
                'alias' => 'ins',
                'type' => 'inner',
                'conditions' => array(
                    'cont.institucion_id = ins.id'
                )
            )
        );
        if($_POST['ban']!=0)
        {
            $options['conditions'] = array(
                'User.instituciondefault' => $valor,
                'User.id !='=>$_POST['ban'],
                'ins.id '=>  $idIns
            );

        }
        else
        {
            $options['conditions'] = array(
                'User.instituciondefault' => $valor,
                'ins.id '=>  $idIns
            );
        }

        $this->User->recursive=-1;
        $contacto= $this->User->find('all', $options);




        if (count($contacto) > 0) {
            $data = "error";
        } else {
            $data = "ok";
        }
        echo json_encode($data);
        $this->autoRender = false;
    }
    public function getinstitucion()
    {
        $Inst= $this->User->Contacto->find('first',
            array(
                'joins'=>
                    array(
                        array('table' => 'institucions',
                            'alias' => 'ins',
                            'type' => 'inner',
                            'conditions' => array(
                                'Contacto.institucion_id = ins.id'
                            )
                          )
                        ),
                'fields'=>array('ins.nombre'),
                'conditions'=>['Contacto.id'=>$_POST['dato']],
                'recursive'=>-1
            ));

        echo json_encode($Inst);
        $this->autoRender = false;
    }


    /**METHODO PARA REESTABLECER CONTRASEÑA DESDE EL INDEX**/
    public function reset($id = null){
        $id=trim($id);
        $real_url = '//'.$_SERVER['HTTP_HOST'].$this->request->base.'/'.$this->params->controller.'/';
        $this->User->recursive=-1;
        $datos = $this->User->find("all",['conditions'=>['User.id'=>$id],'fields'=>['User.id','User.email','User.activo']]);
        if(count($datos)>0){
            if($datos[0]['User']['activo']==1){
                /**ENVIO DE EMAIL**/
                //VARS
                $now = date("Y-m-d H:i:s");
                $random = md5(rand(100,1000));
                $para = $datos[0]['User']['email'];
                /**----ACTUALIZACION----**/
                $this->User->id = $id;
                $this->User->saveField('fechareset', $now);
                $this->User->saveField('numrandom', $random);
                /**---------LECTURA DE DATOS DE ORGANIZACION---------**/
                $this->loadModel("Organizacion");
                $mail_org = $this->Organizacion->read("emailnotification",1);//CORREO DE LA INSTITUCION
                $name_org = $this->Organizacion->read("namenotification",1);//NOMBRE DE LA INSTITUCION
                /**------------------------------------------*/
                $sender = $mail_org['Organizacion']['emailnotification'];
                $name_sender = $name_org['Organizacion']['namenotification'];
                $url = Router::url(['controller' => 'users', 'action' => 'contrasenia', $id, $random], true);
                $tittle="Cambio Solicitado - SMA";

                $nuevo_cont="<div style='width: 100px !important;'><img src='http://vertigo-cloud.com/img/LOGOS-UNIDOS.png' style='width: 100px !important;'></div>
<img src='https://monitoreo.redca.org/img/linea%20azul-email.png' style='width:100%; height: 15px;'>
<p>Se recibio una solicitud para restablecer la contraseña, acceda al siguiente enlace para efectuar los cambios.</p><br>
<a href='".$url."'>Restablecer Contraseña aqui.</a>
<p><br /><img src='https://monitoreo.redca.org/img/sellos-de-seguridad.png'></p>
<img src='https://monitoreo.redca.org/img/footer.png' style='width:100%; height: 15px;'>
<div class='footer-links'>
	<label style='margin-top:8px;display: inline-block;'>Más información en <a href='#'>soporte@sportsma.info</a></label>";
                $merge_data = new stdClass();
                $merge_data->PerMessage =
                    array(
                        array(
                            array(
                                'Field'=>'DeliveryAddress',
                                'Value'=>$para  //$correodestino
                            )
                        )
                    );

                $request = array(
                    'header' => array('Content-Type' => 'application/json',
                    ),
                );

                $data = new stdClass();
                $data->ServerId='16207';
                $data->ApiKey= 'Nc95Cog4A2ZrMn87SbLt';
                /***/
                $data->Messages =
                    array(
                        array(
                            'MergeData'=>$merge_data,
                            'Subject'=>$tittle,
                            'To'=>
                                array(
                                    array(
                                        'EmailAddress'=>'%%DeliveryAddress%%'
                                    )
                                ),
                            'From'=>
                                array(
                                    'EmailAddress'=>$sender
                                ),
                            'HtmlBody'=> $nuevo_cont
                        )
                    );
                $bodyJson = json_encode($data);
                $HttpSocket = new HttpSocket();
                $result = $HttpSocket->post('https://inject.socketlabs.com/api/v1/email',$bodyJson,$request);

                /**********************************************************************/
                $msj="";
                    if ($result==false) {
                        //echo 'HTTP Error';
                        }
                    else {
                        $result_obj = json_decode($result);
                        if ($result_obj->ErrorCode == 'Success') {
                           // echo 'Se envio un correo a .'.$sender;
                            $msj=1;
                        } elseif ($result_obj->ErrorCode == 'Warning') {
                            //echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.';

                            //not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
                        } else {
                            //echo 'Failure Code: ' . $result_obj->ErrorCode;
                        }
                    }

                $this->Session->write("mail_send",1);
                $this->Session->write("mail_user",$para);
              $this->redirect(array('controller'=>"users",'action'=> "index"));

            }else{
                $this->Session->write("user_inac",1);
                $this->redirect(array('controller'=>"users",'action'=> "index"));
            }
        }else{
            /**USUARIO NO EXISTE*/
        }

echo $msj;
        $this->autoRender=false;
    }

    public function sendemail()
    {
        $this->loadModel('Organizacion');
        if ($this->request->is('post')) {
            $param = $this->request->data['User']['param'];
            if ($this->userExist($param)) {
                $user = $this->getUser($param);

                if ($user['User']['activo']) {
                    if ($user['User']['email']) {
                        $now = date("Y-m-d H:i:s");
                        $random = md5(rand(100, 1000));
                        //$email = new CakeEmail();
                        $url = Router::url(['controller' => 'users', 'action' => 'contrasenia', $user['User']['id'], $random], true);

                        $this->User->id = $user['User']['id'];
                        $this->User->saveField('fechareset', $now);
                        $this->User->saveField('numrandom', $random);

                        /**---------LECTURA DE DATOS DE ORGANIZACION---------**/
                        $this->loadModel("Organizacion");
                        $mail_org = $this->Organizacion->read("emailnotification",1);//CORREO DE LA INSTITUCION
                        $name_org = $this->Organizacion->read("namenotification",1);//NOMBRE DE LA INSTITUCION
                        /**------------------------------------------*/
                        $sender = $mail_org['Organizacion']['emailnotification'];
                        $name_sender = $name_org['Organizacion']['namenotification'];
                        $nuevo_cont="<div style='width: 100px !important;'><img src='http://vertigo-cloud.com/img/LOGOS-UNIDOS.png' style='width: 100px !important;'></div>
<img src='https://monitoreo.redca.org/img/linea%20azul-email.png' style='width:100%; height: 15px;'>
<p>Se recibio una solicitud para restablecer la contraseña, acceda al siguiente enlace para efectuar los cambios.</p><br>
<a href='".$url."'>Reestablecer Contraseña aqui.</a><br>
<p><br /><img src='https://monitoreo.redca.org/img/sellos-de-seguridad.png'></p>
<img src='https://monitoreo.redca.org/img/footer.png' style='width:100%; height: 15px;'><div class='footer-links'>
	<label style='margin-top:8px;display: inline-block;'>Más información en <a href='#'>soporte@sportsma.info</a></label>";
                        $merge_data = new stdClass();
                        $merge_data->PerMessage =
                            array(
                                array(
                                    array(
                                        'Field'=>'DeliveryAddress',
                                        'Value'=>$user['User']['email']  //$correodestino
                                    )
                                )
                            );

                        $request = array(
                            'header' => array('Content-Type' => 'application/json',
                            ),
                        );
                        $data = new stdClass();


                        $data->ServerId ="16207";
                        $data->ApiKey ="Nc95Cog4A2ZrMn87SbLt";
                        /***/
                        $data->Messages =
                            array(
                                array(
                                    'MergeData'=>$merge_data,
                                    'Subject'=>"Cambio Solicitado - SMA",
                                    'To'=>
                                        array(
                                            array(
                                                'EmailAddress'=>'%%DeliveryAddress%%'
                                            )
                                        ),
                                    'From'=>
                                        array(
                                            'EmailAddress'=>$sender
                                        ),
                                    'HtmlBody'=> $nuevo_cont
                                )
                            );
                        //debug($data);
                        $bodyJson = json_encode($data);
                        $HttpSocket = new HttpSocket();
                        $result = $HttpSocket->post('https://inject.socketlabs.com/api/v1/email',$bodyJson,$request);
                        $msj="";
                        if ($result==false) {
                            //echo 'HTTP Error';
                        }
                        else {
                            $result_obj = json_decode($result);
                            if ($result_obj->ErrorCode == 'Success') {
                                // echo 'Se envio un correo a .'.$sender;
                                $msj=1;
                            } elseif ($result_obj->ErrorCode == 'Warning') {
                                //echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.';

                                //not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
                            } else {
                                //echo 'Failure Code: ' . $result_obj->ErrorCode;
                            }
                        }

                        $this->Session->write('passwordRestoreEmailSent', 1);
                        $this->Session->write('msj', 1);
                        $this->Session->write('em', $user['User']['email']);
                        $this->redirect(['action' => 'login']);

                    } else {
                        $this->Session->write('passwordRestoreNoEmail', 1);
                        $this->redirect(['action' => 'login']);
                    }
                } else {
                    $this->Session->write('passwordRestoreInactive', 1);
                    $this->redirect(['action' => 'login']);
                }
            } else {
                $this->Session->write('passwordRestoreInvalidUser', 1);
                $this->redirect(['action' => 'login']);
            }
        }
    }
/*
      public function contrasenia($id = null){
          $cad = explode("-",$id);
          $uid = $cad[0];
          $rand = $cad[1];
          $datos=$this->User->query("select fechareset,email from users where id=".$uid." AND numrandom = '$rand'");
          if(count($datos)>0){
              $fechaIni = $datos[0]['users']['fechareset'];
              $email = $datos[0]['users']['email'];
              $fechaFin = date("Y-m-d H:i:s");
              $fecha1=mktime(date('H',strtotime($fechaIni)),date('i',strtotime($fechaIni)),date('s',strtotime($fechaIni)),date('m',strtotime($fechaIni)),date('d',strtotime($fechaIni)),date('Y',strtotime($fechaIni)));
              $fecha2=mktime(date('H',strtotime($fechaFin)),date('i',strtotime($fechaFin)),date('s',strtotime($fechaFin)),date('m',strtotime($fechaFin)),date('d',strtotime($fechaFin)),date('Y',strtotime($fechaFin)));

              $segundos=$fecha2-$fecha1;
              $horas=$segundos/(60*60);
              if($horas <= 2){
                  if (!empty($this->request->data)) {
                      if ($this->User->save($this->request->data)) {
                          $para      = $email;
                          $titulo    = 'Cambio Realizado';

                          $Email = new CakeEmail();
                          $Email->sender('soporte@tecnologias101.com', 'Soporte - Tenologias101');
                          $Email->template('confirm')->emailFormat('html');
                          $Email->from(array('soporte@tecnologias101.com' => 'Soporte - Tenologias101'));
                          $Email->to($para);
                          $Email->subject($titulo);
                          $Email->send();

                          $this->flash(__('La contrase&ntilde;a fue actualizada.'),array('action'=>'login'));
                      }else {
                          $this->flash(__('El user could not be Almacenado. Please, try again.'));
                      }
                      //debug($this->data);
                  } else {
                      $this->request->data = $this->User->read(null, $id);
                      unset($this->request->data['User']['password']);
                  }
              }else{
                  $this->flash(__('Tiempo expirado, solicite otro enlace.'),array('action'=>'login'));
              }
          }else{
              $this->flash(__('Tiempo expirado, solicite otro enlace.'),array('action'=>'login'));
          }

      }
  */

    /**
     * Author: Manuel Anzora
     * date: 26-06-2019
     * description: metodo para obtener listado de torneos en formato html
     **/
    public function getTorneos(){
        $this->autoRender=false;
        $this->loadModel("Torneo");
        $torneos = $this->Torneo->find("all",[
            "conditions"=>[
                "Torneo.activo"=>1
            ],
            "fields"=>[
                "Torneo.id", "Torneo.nombrecorto"
            ],
            "order"=>["Torneo.nombrecorto"=>"asc"]
        ]);
        $html="<option>Seleccionar</option>";
        foreach ($torneos as $item){
            $html .= "<option value='".$item["Torneo"]["id"]."'>".$item["Torneo"]["nombrecorto"]."</option>";
        }
        echo $html;
    }
}
