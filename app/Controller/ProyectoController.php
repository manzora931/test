<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Fuentesfinanciamientos');
App::import('Controller', 'Actividades');
App::import('Controller', 'Presupuestos');
App::import('Controller', 'Desembolsos');
App::import('Controller', 'Gastos');
App::import('Controller', 'Incidencias');
/**
 * Proyecto Controller
 *
 * @property Proyecto $Proyecto
 * @property PaginatorComponent $Paginator
 */
class ProyectoController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    var $helpers = array('Html', 'Form');

    public function impresion($id = null, $pantalla = null) {
        $this->layout=false;
        $encabezado = '';
        $data = [];
        $obj = null;
        $pantallas = [1=>"fuentesfinanciamientos", 2=>"actividades", 3=>"presupuestos", 4=>"desembolsos", 5=>"gastos", 6=>"incidencias"];

        $this->loadModel('Proyecto');
        $this->Proyecto->recursive = 0;
        $proyecto = $this->Proyecto->find('first', [
            'fields' => ['Proyecto.*', 'Institucion.nombre', 'Tipoproyecto.tipoproyecto', 'Estado.estado', 'Paise.pais'],
            /*'joins'=>[
                [
                    'table'=>'institucions', 'alias'=>'Institucion', 'type'=>'INNER',
                    'conditions'=>[ 'Institucion.id = Proyecto.institucion_id' ]
                ]
            ],*/
            'conditions' => [
                'Proyecto.id' => $id
            ]
        ]);

        if(!is_null($pantalla)) {
            $obj = $this->get_data($pantalla, $id, 1);
            $this->request->data = $obj['obj']->viewVars;
            $encabezado = $obj['encabezado'];
        } else {
            $this->request->data = null;
            foreach ($pantallas as $key => $row) {
                $obj = $this->get_data($row, $id, 2);

                $data[$row] = $obj['obj']->viewVars;
                $data[$key]['encabezado'] = $obj['encabezado'];
            }
        }

        $this->loadModel('Organizacion');
        $this->Organizacion->recursive = 0;
        $organizacion = $this->Organizacion->find('first', array('fields' => ['Organizacion.organizacion'],'conditions' => array('Organizacion.id' => 1)));
        $org = $organizacion['Organizacion']['organizacion'];

        $this->set(compact('org', 'proyecto', 'pantalla', 'pantallas', 'encabezado', 'data'));
    }

    public function get_data($pantalla = null, $id = null, $ban) {
        $data = [];

        switch ($pantalla) {
            case "fuentesfinanciamientos":
                $data['encabezado'] = 'Fuentes de Financiamiento';
                $this->fuentesfinanciamientos($id);
                $data['obj'] = $this;
                break;
            case "actividades":
                $data['encabezado'] = 'Actividades';
                $obj = new ActividadesController;
                $obj->index($id);
                $data['obj'] = $obj;
                break;
            case "presupuestos":
                $data['encabezado'] = 'Presupuestos';
                $obj = new PresupuestosController;
                $obj->index($id);
                $data['obj'] = $obj;
                break;
            case "desembolsos":
                $data['encabezado'] = 'Desembolsos';
                $this->desembolsos($id);
                $data['obj'] = $this;
                break;
            case "gastos":
                $data['encabezado'] = 'Gastos';
                if($ban == 1) {
                    $obj = new GastosController;
                    $obj->index($id);
                    $data['obj'] = $obj;
                } else {
                    $this->gastos($id);
                    $data['obj'] = $this;
                }
                break;
            case "incidencias":
                $data['encabezado'] = 'Incidencias';
                $obj = new IncidenciasController;
                $obj->index($id);
                $data['obj'] = $obj;
                break;
        }

        return $data;
    }

    public function desembolsos($idProyecto = null) {
        $data = array();
        $this->loadModel('Fuentesfinanciamiento');
        $this->Fuentesfinanciamiento->recursive = 0;

        if(!is_null($idProyecto)) {
            $conditions = array(
                'fields' => ['Financista.nombre', 'Fuentesfinanciamiento.*', 'Desembolso.*', 'Bancoorigen.nombre', 'Bancodestino.nombre','Destinatario.*', 'concat(Contacto.nombres, " ", Contacto.apellidos) as nombre'],
                'joins' => array(
                    array('table' => 'desembolsos', 'alias' => 'Desembolso', 'type' => 'INNER', 'conditions' => array('Desembolso.fuentefinanciamiento_id = Fuentesfinanciamiento.id')),
                    array('table' => 'bancos', 'alias' => 'Bancoorigen', 'type' => 'INNER', 'conditions' => array('Desembolso.bancorigen_id = Bancoorigen.id')),
                    array('table' => 'bancos', 'alias' => 'Bancodestino', 'type' => 'INNER', 'conditions' => array('Desembolso.bancdestino_id = Bancodestino.id')),
                    array('table' => 'destinatarios', 'alias' => 'Destinatario', 'type' => 'INNER', 'conditions' => array('Desembolso.destinatario_id = Destinatario.id')),
                    array('table' => 'contactos', 'alias' => 'Contacto', 'type' => 'INNER', 'conditions' => array('Destinatario.contacto_id = Contacto.id'))
                ),
                'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $idProyecto), 'order'=>array('Desembolso.fecha'=>'desc'));

            $data = $this->Fuentesfinanciamiento->find("all", $conditions);
        }

        $this->set('desembolsos', $data);

        $fuentes = $this->Fuentesfinanciamiento->find("list", [
            'fields' => ['Fuentesfinanciamiento.id', 'Financista.nombre'],
            'joins' => array( array('table' => 'financistas', 'alias' => 'Financista', 'type' => 'INNER', 'conditions' => array('Fuentesfinanciamiento.financista_id = Financista.id'))),
            'conditions'=> ['Fuentesfinanciamiento.proyecto_id' => $idProyecto]
        ]);

        $this->loadModel('Destinatario');
        $this->Destinatario->recursive = 0;
        $destinatariosactivos = $this->Destinatario->find("all", [
            'fields' => ['Destinatario.id', 'Destinatario.nombre'],
            'conditions'=> ['Destinatario.activo' => 1]
        ]);

        $destinatarios = array();
        foreach ($destinatariosactivos as $destinatario) {
            $destinatarios[$destinatario['Destinatario']['id']] = $destinatario['Destinatario']['nombre'];
        }

        $this->loadModel('Banco');
        $bancos = $this->Banco->find("list", [
            'fields' => ['Banco.id', 'Banco.nombre'],
            'conditions'=> ['Banco.activo' => 1]
        ]);

        $meses = [1=>"Ene", 2=>"Feb", 3=>"Mar", 4=>"Abr", 5=>"May", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Sep", 10=>"Oct", 11=>"Nov", 12=>"Dic"];

        $this->set(compact( 'idProyecto', 'fuentes', 'destinatarios', 'bancos', 'meses'));
    }

    public function fuentesfinanciamientos($idProyecto = null) {
        $data = array();
        $this->loadModel('Fuentesfinanciamiento');
        $obj = new FuentesfinanciamientosController;

        if(!is_null($idProyecto)) {
            $conditions = array(
                'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $idProyecto),
                'order'=>array('Fuentesfinanciamiento.financista_id'=>'asc')
            );

            $this->Fuentesfinanciamiento->recursive = 0;
            $data = $this->Fuentesfinanciamiento->find("all", $conditions);
        }
        $this->set('fuentesfinanciamientos', $data);

        $fuentes = $this->Fuentesfinanciamiento->find("all", [
            'fields' => [
                'Fuentesfinanciamiento.financista_id',
                'Fuentesfinanciamiento.id'
            ],
            'conditions'=> [
                'Fuentesfinanciamiento.proyecto_id' => $idProyecto
            ]
        ]);

        $financistasasignados = array();
        foreach ($fuentes as $fuente) {
            $info_ffinanciamiento[$fuente['Fuentesfinanciamiento']['id']]['desembolso'] = $obj->getDesembolsado($fuente['Fuentesfinanciamiento']['id']);
            $info_ffinanciamiento[$fuente['Fuentesfinanciamiento']['id']]['ejecutado'] = $obj->getEjecutado($fuente['Fuentesfinanciamiento']['id']);
            array_push($financistasasignados, $fuente['Fuentesfinanciamiento']['financista_id']);
        }

        $financistas = $this->Fuentesfinanciamiento->Financista->find("list", [
            'fields' => [
                'Financista.id',
                'Financista.nombre'
            ],
            'conditions'=> [
                'Financista.activo' => 1,
                'Financista.id !=' => $financistasasignados
            ]
        ]);

        $this->set(compact( 'idProyecto', 'financistas','info_ffinanciamiento'));
    }

    public function gastos($idProyecto = null) {
        $this->layout=false;
        $data = [];
        $modules=[];
        $intervenciones=[];
        $gastosintervencion = [];
        $gastosmodulo = [];
        $this->loadModel("Gasto");

        if(!is_null($idProyecto)) {
            /************************* MODULOS ****************************/
            $this->loadModel("Module");
            $this->Module->recursive = 1;
            $modules = $this->Module->find("all",[
                'conditions'=>['Module.proyecto_id'=>$idProyecto],
                'order'=>['Module.orden'=>"ASC"]
            ]);

            foreach ($modules as $row) {
                /*** ALMACENA LAS INTERNVENCIONES DE CADA MODULO ***/
                $intervenciones[$row['Module']['id']] = $row['Intervencione'];
                $gastosmodulo[$row['Module']['id']] = 0;
            }

            /*** FOREACH PARA RECORRER INTERVENCIONES ***/
            foreach ($intervenciones as $rw) {
                foreach ($rw as $item){
                    /*** GASTOS DE ACTIVIDADES POR INTERVENCIONES ***/
                    $gastos = $this->Gasto->find("all",
                        [
                            'fields' => ['Actividade.*', 'Gasto.*', 'Catgasto.*'],
                            'joins'=>[
                                [
                                    'table'=>'actividades', 'alias'=>'Actividade', 'type'=>'INNER',
                                    'conditions'=>[ 'Actividade.id = Gasto.actividade_id' ]
                                ],
                                [
                                    'table'=>'catgastos', 'alias'=>'Catgasto', 'type'=>'INNER',
                                    'conditions'=>[ 'Catgasto.id = Gasto.catgasto_id' ]
                                ]
                            ],
                            'conditions'=>['Actividade.intervencione_id' => $item['id']]
                        ]);
                    $data[$item['id']] = $gastos;

                    $gastosintervencion[$item['id']] = count($gastos);
                    $gastosmodulo[$item['module_id']]+=$gastosintervencion[$item['id']];
                }
            }
        }

        $this->set('gastos', $data);
        $actividades = $this->Gasto->Actividade->find("list", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'Actividade.finalizado' => 0, 'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $this->loadModel('Catgasto');
        $catgastos = $this->Catgasto->find("list", [
            'fields' => ['Catgasto.id', 'Catgasto.nombre'],
            'conditions'=> ['Catgasto.activo' => 1]
        ]);

        $meses = [1=>"Ene", 2=>"Feb", 3=>"Mar", 4=>"Abr", 5=>"May", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Sep", 10=>"Oct", 11=>"Nov", 12=>"Dic"];
        $this->set(compact( 'idProyecto', 'modules', 'intervenciones', 'actividades', 'catgastos', 'meses', 'gastosmodulo', 'gastosintervencion'));
    }
}
