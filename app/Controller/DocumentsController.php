<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 * @property PaginatorComponent $Paginator
 */
class DocumentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Document", "documents", "index");
		$this->Document->recursive = 0;
        include "busqueda/document.php";
        $data = $this->Paginator->paginate('Document');
		$this->set('documents', $data);
		$proyectos = $this->Document->Proyecto->find("list");
		$this->set(compact("proyectos"));
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[documents]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Document", "documents", "view");
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$this->set('document', $this->Document->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Document", "documents", "add");
		if ($this->request->is('post')) {
			$this->Document->create();
			$this->request->data['Document']['usuario']=$this->Session->read("nombreusuario");
			$this->request->data['Document']['modified']=0;
            $file = new File($this->request->data["Document"]["url_doc"]['tmp_name']);
            $path_parts = pathinfo($this->request->data["Document"]["url_doc"]['name']);
            $filename="";
            if (isset($path_parts['extension'])) {
                $filename = $this->request->data["Document"]["url_doc"]['name'];
                $data = $file->read();
                $file->close();
                $file = new File(WWW_ROOT . '/files/adjuntos/' . $filename, true);
                $file->write($data);
                $file->close();
            }
            $this->request->data['Document']['url_doc']=$filename;
			if ($this->Document->save($this->request->data)) {
				$this->Session->write("saveDoc",1);
				$id = $this->Document->id;
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$proyectos = $this->Document->Proyecto->find('list');
		$this->set(compact('proyectos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Document", "documents", "edit");
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Document']['usuariomodif']=$this->Session->read("nombreusuario");
            $file = new File($this->request->data["Document"]["url_doc"]['tmp_name']);
            $path_parts = pathinfo($this->request->data["Document"]["url_doc"]['name']);
            $filename="";
            if (isset($path_parts['extension'])) {
                $filename = $this->request->data["Document"]["url_doc"]['name'];
                $data = $file->read();
                $file->close();
                $file = new File(WWW_ROOT . '/files/adjuntos/' . $filename, true);
                $file->write($data);
                $file->close();
            }

            $filename = ($filename=="")?$this->request->data['Document']['url']:$filename;
            $this->request->data['Document']['url_doc']=$filename;
			if ($this->Document->save($this->request->data)) {
                $this->Session->write("saveDoc",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}
		$proyectos = $this->Document->Proyecto->find('list');
		$this->set(compact('proyectos'));
	}

	public function valFile(){
        $info = pathinfo($_FILES['archivo']['name']);
        $bandera=0;
        if($info['extension']=='pdf' || $info['extension']=='PDF'){
            $bandera=1;
        }
        echo $bandera;
       $this->autoRender=false;
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Document", "documents", "delete");
		if ($delete == true) {
			$this->Document->id = $id;
			if (!$this->Document->exists()) {
				throw new NotFoundException(__('Invalid document'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Document->delete()) {
					$this->Session->setFlash(__('The document has been deleted.'));
			} else {
				$this->Session->setFlash(__('The document could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
