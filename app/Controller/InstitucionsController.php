<?php
App::uses('AppController', 'Controller');
/**
 * Institucions Controller
 *
 * @property Institucion $Institucion
 * @property PaginatorComponent $Paginator
 */
class InstitucionsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Html', 'Form', 'Js');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Institucion", "institucions", "index");
		$this->Institucion->recursive = 0;
		if ($this->Session->check('tabla[institucions].activo')) {
			$this->Paginator->settings = array('order'=>array('Institucion.nombre'=>'asc'));
		}else{
			$this->Paginator->settings = array('conditions' => array('Institucion.activo' => 1),'order'=>array('Institucion.nombre'=>'asc'));
		}

		include 'busqueda/instituciones.php';
		$data = $this->Paginator->paginate('Institucion');
		//$paises = $this->Institucion->Paise->find('list');

        $getInstitucions = $this->Institucion->find('all', [
            'order' => [
                'Institucion.nombre' => 'asc'
            ]
        ]);
        $this->loadModel("Contacto");
        $this->Contacto->recursive = 0;
        $contacts = [];
        $getContacts = $this->Contacto->find('all', [
            'order' => [
                'Contacto.nombres' => 'asc'
            ]
        ]);

        foreach ($getInstitucions as $institucion) {
            // $insContacts;

            foreach ($getContacts as $contact) {
                if ($institucion['Institucion']['id'] === $contact['Contacto']['institucion_id']) {
                    $contacts[$institucion['Institucion']['id']][] = $contact['Contacto'];
                }
            }
        }

		$this->set(compact('paises', 'contacts'));
		$this->set('institucions', $data);
	}

	public function vertodos()
	{

		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[institucions]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;

	}


	/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Institucion", "institucions", "view");
		if (!$this->Institucion->exists($id)) {
			throw new NotFoundException(__('Invalid institucion'));
		}
		//$options = array('conditions' => array('Institucion.' . $this->Institucion->primaryKey => $id));
		//$this->set('institucion', $this->Institucion->find('first', $options));

		$options = array('conditions' => array('Institucion.' . $this->Institucion->primaryKey => $id));
		$dat = $this->Institucion->find('first', $options);

        $this->loadModel('Contacto');
        $this->Contacto->recursive = 0;
        $contactos = $this->Contacto->find("all", [
            'conditions'=> [
                'Contacto.activo' => 1,
                'Contacto.institucion_id' => $id
            ]
        ]);

		$this->set('institucion',$dat);
		$this->set(compact("contactos"));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Institucion", "institucions", "add");
		if ($this->request->is('post')) {
			$this->Institucion->create();
			$this->request->data['Institucion']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Institucion']['organizacion_id'] = 1;
			$this->request->data['Institucion']['modified'] = 0;
			if ($this->Institucion->save($this->request->data)) {
				$id = $this->Institucion->id;
				$this->Session->write("inst_save",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The institucion could not be saved. Please, try again.'));
			}
		}
        $paises = $this->Institucion->Paise->find('list',[
            "conditions"=>["Paise.activa"=>1],
            "order"=>["Paise.pais"=>"asc"]
        ]);
        $rubros = $this->Institucion->Rubro->find("list",[
            "conditions"=>["Rubro.activo"=>1],
            "order"=>["Rubro.rubro"=>"asc"]
        ]);
        $tipoinstitucions = $this->Institucion->Tipoinstitucion->find("list",[
            "conditions"=>["Tipoinstitucion.activo"=>1],
            "order"=>["Tipoinstitucion.tipoinstitucions"=>"asc"]
        ]);
        $this->set(compact('paises',"rubros", "tipoinstitucions"));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Institucion", "institucions", "edit");
		if (!$this->Institucion->exists($id)) {
			throw new NotFoundException(__('Invalid institucion'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Institucion']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Institucion->save($this->request->data)) {
				$this->Session->write("inst_save",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->write("inst_save",0);
				$this->redirect(array('action' => 'view',$id));
			}
		} else {
			$options = array('conditions' => array('Institucion.' . $this->Institucion->primaryKey => $id));
			$this->request->data = $this->Institucion->find('first', $options);
		}
		$paises = $this->Institucion->Paise->find('list',[
		    "conditions"=>["Paise.activa"=>1],
            "order"=>["Paise.pais"=>"asc"]
        ]);
		$rubros = $this->Institucion->Rubro->find("list",[
		    "conditions"=>["Rubro.activo"=>1],
            "order"=>["Rubro.rubro"=>"asc"]
        ]);
		$tipoinstitucions = $this->Institucion->Tipoinstitucion->find("list",[
		    "conditions"=>["Tipoinstitucion.activo"=>1],
            "order"=>["Tipoinstitucion.tipoinstitucions"=>"asc"]
        ]);

		$this->set(compact('paises',"rubros", "tipoinstitucions"));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Institucion", "institucions", "delete");
		if ($delete == true) {
			$this->Institucion->id = $id;
			if (!$this->Institucion->exists()) {
				throw new NotFoundException(__('Invalid institucion'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Institucion->delete()) {
					$this->Session->setFlash(__('The institucion has been deleted.'));
			} else {
				$this->Session->setFlash(__('The institucion could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

	public function val_per() {
		$valor=$_POST['val'];
		$id =$_POST['id'];

		if($id != 0){
			$datos=$this->Institucion->query("SELECT nombre FROM institucions WHERE nombre='$valor' AND id !=".$id);
		}else{
			$datos=$this->Institucion->query("SELECT nombre FROM institucions WHERE nombre='$valor'");
		}
		if(count($datos)>0){
			echo "error";
		}else{
			echo "ok";
		}
		$this->autoRender=false;
	}
	public function get_mask() {
		$valor=$_POST['pais'];
		$dat='';
		if($valor != 0){
			$datos=$this->Institucion->query("SELECT codigo FROM paises WHERE id =".$valor);
		}
			foreach ($datos as  $data){
				$dat=$data['paises']['codigo'];
			}

		echo $dat;
		$this->autoRender=false;
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Proyecto');
        $this->Proyecto->recursive = -1;
        $institucion = $this->Proyecto->find('all', [
            'conditions' => [
                'Proyecto.institucion_id' => $id,
            ]
        ]);

        $data['instituciones'] = (count($institucion) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
