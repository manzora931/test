<?php
App::uses('AppController', 'Controller');
/**
 * Catgastos Controller
 *
 * @property Catgasto $Catgasto
 * @property PaginatorComponent $Paginator
 */
class CatgastosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Catgasto", "catgastos", "index");
        $this->Paginator->settings = array('conditions' => array('Catgasto.activo >=' => 1), 'order'=>array('Catgasto.nombre'=>'asc'));
        $this->Catgasto->recursive = 0;
        include 'busqueda/catgastos.php';
        $data = $this->Paginator->paginate('Catgasto');
        $this->set('catgastos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[catgastos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Catgasto", "catgastos", "view");
		if (!$this->Catgasto->exists($id)) {
			throw new NotFoundException(__('Invalid catgasto'));
		}
		$options = array('conditions' => array('Catgasto.' . $this->Catgasto->primaryKey => $id));
		$this->set('catgasto', $this->Catgasto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Catgasto", "catgastos", "add");
		if ($this->request->is('post')) {
			$this->Catgasto->create();
            $this->request->data['Catgasto']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Catgasto']['modified'] = 0;
			if ($this->Catgasto->save($this->request->data)) {
                $catgasto_id = $this->Catgasto->id;
                $this->Session->write('catgasto_save', 1);
                $this->redirect(['action' => 'view', $catgasto_id]);
			} else {
				$this->Session->setFlash(__('La categoría de gasto no ha sido creada. Intentar de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Catgasto", "catgastos", "edit");
		if (!$this->Catgasto->exists($id)) {
			throw new NotFoundException(__('Invalid catgasto'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Catgasto']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Catgasto->save($this->request->data)) {
                $this->Session->write('catgasto_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('La categoría de gasto no ha sido actualizada. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Catgasto.' . $this->Catgasto->primaryKey => $id));
			$this->request->data = $this->Catgasto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Catgasto", "catgastos", "delete");
		if ($delete == true) {
			$this->Catgasto->id = $id;
			if (!$this->Catgasto->exists()) {
				throw new NotFoundException(__('Invalid catgasto'));
			}
			//$this->request->onlyAllow('post', 'delete');
			if ($this->Catgasto->delete()) {
					$this->Session->setFlash(__('The catgasto has been deleted.'));
			} else {
				$this->Session->setFlash(__('The catgasto could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Gasto');
        $this->Gasto->recursive = -1;
        $catgasto = $this->Gasto->find('all', [
            'conditions' => [
                'Gasto.catgasto_id' => $id,
            ]
        ]);

        $data['catgastos'] = (count($catgasto) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
