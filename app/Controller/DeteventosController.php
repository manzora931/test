<?php
App::uses('AppController', 'Controller');
/**
 * Deteventos Controller
 *
 * @property Detevento $Detevento
 * @property PaginatorComponent $Paginator
 */
class DeteventosController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $helpers = array('Html','Js', 'Form');
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Detevento", "deteventos", "index");
        $this->Paginator->settings = array('order'=>array('Detevento.detevento'=>'asc'));
        $this->Detevento->recursive = 0;
        include 'busqueda/detevento.php';
        $data = $this->Paginator->paginate('Detevento');
		$this->set('deteventos', $data);
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[deteventos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Detevento", "deteventos", "view");
		if (!$this->Detevento->exists($id)) {
			throw new NotFoundException(__('Invalid detevento'));
		}
		$options = array('conditions' => array('Detevento.' . $this->Detevento->primaryKey => $id));
		$this->set('detevento', $this->Detevento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Detevento", "deteventos", "add");
		if ($this->request->is('post')) {
            $this->request->data['Detevento']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Detevento']['modified']=0;
		    $this->Detevento->create();
			if ($this->Detevento->save($this->request->data)) {
                $this->Session->write('detevento_save', 1);
                $detevento_id = $this->Detevento->id;
				$this->redirect(array('action' => 'view',$detevento_id));
			} else {
				$this->Session->setFlash(__('The detevento could not be saved. Please, try again.'));
			}
		}
		$eventos = $this->Detevento->Evento->find('list',[
		    "conditions"=>["Evento.activo"=>1]
        ]);
		$this->set(compact('eventos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Detevento", "deteventos", "edit");
		if (!$this->Detevento->exists($id)) {
			throw new NotFoundException(__('Invalid detevento'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Detevento']['usuariomodif'] = $this->Session->read('nombreusuario');
            $this->request->data['Detevento']['modified'] = date("Y-m-d H:i:s");
		    if ($this->Detevento->save($this->request->data)) {
                $detevento_id = $this->Detevento->id;
                $this->Session->write('detevento_save', 1);
				$this->redirect(array('action' => 'view',$detevento_id));
			} else {
				$this->Session->setFlash(__('The detevento could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Detevento.' . $this->Detevento->primaryKey => $id));
			$this->request->data = $this->Detevento->find('first', $options);
		}
		$eventos = $this->Detevento->Evento->find('list');
		$this->set(compact('eventos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Detevento", "deteventos", "delete");
		if ($delete == true) {
			$this->Detevento->id = $id;
			if (!$this->Detevento->exists()) {
				throw new NotFoundException(__('Invalid detevento'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Detevento->delete()) {
					$this->Session->setFlash(__('The detevento has been deleted.'));
			} else {
				$this->Session->setFlash(__('The detevento could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
