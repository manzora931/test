<?php
App::uses('AppController', 'Controller');
/**
 * Municipios Controller
 *
 * @property Municipio $Municipio
 * @property PaginatorComponent $Paginator
 */
class MunicipiosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    public function lista_municipios(){
        $departamento = $_POST['departamento'];
        $sql = $this->Institucion->query("SELECT id, nombre as nombre FROM municipios WHERE departamento_id = " . $departamento . " AND activa = 1 ORDER BY nombre");

        $municipios = "<option value=''> Seleccionar </option>";
        foreach($sql as $row){
            $municipios.= "<option value='".$row['municipios']['id']."'>".$row['municipios']['nombre']."</option>";
        }

        echo $municipios;
        $this->autoRender=false;
    }
}
