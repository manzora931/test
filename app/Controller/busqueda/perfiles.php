<?php
//opciones del paginado
//$pag=$this->Paginator->paginate();
if($this->Session->read($this->params['controller']))
{
    $this->Paginator->settings = $this->paginate=$this->Session->read($this->params['controller']);
}
//opciones de la busqueda
if ($this->request->is(array('post', 'put')))
{
    $busqueda=array();
    $complemento=array();
    $actividad=array();
    $arrayFecha=array();
    $status=array();
    $finalquery = 0;
    $finalquery2 = 0;

    // guardamos todo en la sesion
    //nombre de la tabla
    $tableName = $this->data['tabla']['tabla'];
    $this->Session->write("tabla[".$tableName."]", $this->data[$tableName]);
    //aqui se ingresa cuando el campo basico de search_text lleva informaci?n
    #debug($this->data[$tableName]['estado']);

    if(isset($this->data[$tableName]['search_text']) && $this->data[$tableName]['search_text'] != '' ||  $this->data[$tableName]['search_text']!=' ')
    {
        $c=0;
        $contro=$this->data['tabla']['controller'];
        $comparacion=explode(",",$this->data['tabla']['parametro']);

        if($this->data[$tableName]['search_text'] != '' and  $this->data[$tableName]['search_text'] != ' ' and $this->data['tabla']['parametro'] != 'personalizado')
        {
            foreach ($comparacion as $key => $value)
            {
                $busqueda[$this->data['tabla']['controller'].".$value LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
            }
        }

        $reunionesSearchText = $this->data[$tableName]['search_text'];
    }

    // con este arreglo agregamos todos los campos que queremos validar en index colocar el id y nombre correspondiente
    $camposComparar = array('estado');
    foreach($camposComparar as $k=>$v)
    {
        if(isset($this->data[$tableName][$v]))
        {
            if($this->data[$tableName][$v]>0)
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
            if($this->data[$tableName][$v]==0 && $v == "activo")
            {

                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
            $finalquery = 1;
        }
    }

    if (isset($this->data[$tableName]['activo'])||isset($this->data[$tableName]['activa'])) {
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)

        $finalquery=1;
        if($finalquery==0)
        {

            $condition= array('conditions'=>array(array('OR'=>$busqueda, 'OR' => array($status)),'order'=>array('Perfile.perfil'=>'asc')));
        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento),'order'=>array('Perfile.perfil'=>'asc'));

            }
            else
            {
                $status = array($this->data['tabla']['controller'].".id != " => 0);
                $condition= array('conditions'=>array('OR'=>$busqueda, $complemento,$status),'order'=>array('Perfile.perfil'=>'asc'));
            }
        }
    }else{
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)
        if($finalquery==0)
        {
            $condition= array('conditions'=>array('OR'=>$busqueda, $this->data['tabla']['controller'].".activo"=>1),'order'=>array('Perfile.perfil'=>'asc'));
        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento),'order'=>array('Perfile.perfil'=>'asc'));
            }
            else
            {
                $condition= array('conditions'=>array(' OR'=>$busqueda, 'AND'=>$complemento,$this->data['tabla']['controller'].".activo"=>1),'order'=>array('Perfile.perfil'=>'asc'));
            }
        }
    }

    $this->Session->write($this->params['controller'],$condition);

}

if($this->Session->read($this->params['controller']))
{
    $this->paginate=$this->Session->read($this->params['controller']);
    $data = $this->Paginator->paginate();
}