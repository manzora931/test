<?php
if($this->Session->read($this->params['controller']))
{
    $this->Paginator->settings = $this->paginate=$this->Session->read($this->params['controller']);
}
// Opciones de la busqueda
if ($this->request->is(array('post', 'put')))
{
    $busqueda=array();
    $complemento=array();
    $actividad=array();
    $arrayFecha=array();
    $status=array();
    $finalquery = 0;
    $finalquery2 = 0;
    //guardamos todo en la session
    //nombre de la tabla
    $tableName = $this->data['tabla']['tabla'];
    $this->Session->write("tabla[".$tableName."]", $this->data[$tableName]);

    //aqui se ingresa cuando el campo basico de search_text lleva informacion
    if(isset($this->data[$tableName]['search_text']) && $this->data[$tableName]['search_text'] != '' ||  $this->data[$tableName]['search_text']!=' ')
    {
        $c=0;
        $contro=$this->data['tabla']['controller'];
        $comparacion=explode(",",$this->data['tabla']['parametro']);

        if($this->data[$tableName]['search_text'] != '' and  $this->data[$tableName]['search_text'] != ' ' and $this->data['tabla']['parametro'] != 'personalizado')
        {
            foreach ($comparacion as $key => $value)
            {
                $busqueda[$this->data['tabla']['controller'].".$value LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
            }
        }
    }

    // con este arreglo agregamos todos los campos que queremos validar en index colocar el id y nombre correspondiente
    $camposComparar = array('tipo_periodo','status','proyecto_id','tipopartida_id','mensual','socionegocio_id','estadoactividade_id', 'ubicacionfisica_id', 'estado', 'tipovisita_id', 'oitestado_id', 'activo', 'prioridad', 'resolucion');
    foreach($camposComparar as $k=>$v)
    {
        if(isset($this->data[$tableName][$v]))
        {
            if($this->data[$tableName][$v]>0)
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
            //personalizado para mensajeros
            if($this->data[$tableName][$v]==0 && $v == "activo")
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
        }
    }

    if (isset($this->data[$tableName]['activo'])) {
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)


        if($finalquery==0)
        {
            $condition= array('conditions'=>array('OR'=>$busqueda, $this->data['tabla']['controller'].".activo"=>1, $this->data['tabla']['controller'].".activo"=>0),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));

        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));

            }
            else
            {
                $condition= array('conditions'=>array(' OR'=>$busqueda),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));
                #debug($condition);
            }
        }
    }else{
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)
        if($finalquery==0)
        {
            $condition= array('conditions'=>array('OR'=>$busqueda, $this->data['tabla']['controller'].".activo"=>1),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));
            #debug($condition);
        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));
            }
            else
            {
                $condition= array('conditions'=>array(' OR'=>$busqueda, 'AND'=>$complemento),'order'=>array('Tipoequipo.tipoequipo'=>'asc'));
            }
        }
    }

    $this->Session->write($this->params['controller'],$condition);

}
if($this->Session->read($this->params['controller']))
{
    $this->paginate=$this->Session->read($this->params['controller']);
    $data = $this->Paginator->paginate();
}