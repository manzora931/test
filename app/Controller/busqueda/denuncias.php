<?php
//opciones de la busqueda
if ($this->request->is(array('post', 'put'))) {
	$and = [];

	if ($this->request->data['vdenuncias']['search_text']) {
		$and[] = ['Vdenuncia.codigo LIKE' => "%" . $this->request->data['vdenuncias']['search_text'] . "%"];
		$this->Session->write('search_text', $this->request->data['vdenuncias']['search_text']);
	}

	if ($this->request->data['vdenuncias']['desde'] && !$this->request->data['vdenuncias']['hasta']) {
		$and[] = [
			'DATE(Vdenuncia.recepcion) >=' => date('Y-m-d 00:00:00', strtotime($this->request->data['vdenuncias']['desde'])),
		];
		$this->Session->write('desde', $this->request->data['vdenuncias']['desde']);
	} else if ($this->request->data['vdenuncias']['desde'] && $this->request->data['vdenuncias']['hasta']) {
		$and[] = [
			'DATE(Vdenuncia.recepcion) BETWEEN ? AND ? ' => [
				date('Y-m-d 00:00:00', strtotime($this->request->data['vdenuncias']['desde'])),
				date('Y-m-d 00:00:00', strtotime($this->request->data['vdenuncias']['hasta']))
			]
		];
		$this->Session->write('desde', $this->request->data['vdenuncias']['desde']);
		$this->Session->write('hasta', $this->request->data['vdenuncias']['hasta']);
	}

	if ($this->request->data['vdenuncias']['pais']) {
		$and[] = ['Vdenuncia.paise_id' => $this->request->data['vdenuncias']['pais']];
		$this->Session->write('pais', $this->request->data['vdenuncias']['pais']);
	}

	if ($this->request->data['vdenuncias']['estado']) {
		$and[] = ['Vdenuncia.estado_id' => $this->request->data['vdenuncias']['estado']];
		$this->Session->write('estado', $this->request->data['vdenuncias']['estado']);
	}
	if ($this->request->data['vdenuncias']['institucion']) {
		$and[] = ['Vdenuncia.institucion_id' => $this->request->data['vdenuncias']['institucion']];
		$this->Session->write('institucion', $this->request->data['vdenuncias']['institucion']);
	}

	if($solopropietario[0]['pr']['solopropietario']) {
		$and[] = ['Vdenuncia.user_id' => $userId];
	}


	$condition = [
		'conditions' => [
			'AND' => $and
		],
		'order' => [
			'Vdenuncia.recepcion' => 'DESC'
		]
	];

    $this->Session->write('vdenuncias_search', $condition);

}

if ($this->Session->check('vdenuncias_search')) {
    $this->paginate = $this->Session->read('vdenuncias_search');
    $data = $this->Paginator->paginate('Vdenuncia');
}
