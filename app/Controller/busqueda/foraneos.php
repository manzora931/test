<?php
//opciones del paginado
//$pag=$this->Paginator->paginate();
if($this->Session->read($this->params['controller']))
{
    $this->Paginator->settings = $this->paginate=$this->Session->read($this->params['controller']);
}
//opciones de la busqueda
if ($this->request->is(array('post', 'put')))
{
    $busqueda=array();
    $complemento=array();
    $actividad=array();
    $arrayFecha=array();
    $status=array();
    $finalquery = 0;
    $finalquery2 = 0;
    //guardamos todo en la session
    //nombre de la tabla
    $tableName = $this->data['tabla']['tabla'];
    $this->Session->write("tabla[".$tableName."]", $this->data[$tableName]);
    //aqui se ingresa cuando el campo basico de search_text lleva informaci�n
    // debug($this->data['users']['SearchText']);

    if(isset($this->data[$tableName]['search_text']) && $this->data[$tableName]['search_text'] != '' ||  $this->data[$tableName]['search_text']!=' ')
    {
        // debug('hola');
        $c=0;
        $contro=$this->data['tabla']['controller'];
        $comparacion=explode(",",$this->data['tabla']['parametro']);

        if($this->data[$tableName]['search_text'] != '' and  $this->data[$tableName]['search_text'] != ' ' and $this->data['tabla']['parametro'] != 'personalizado')
        {
            // debug('hola1');
            foreach ($comparacion as $key => $value)
            {
                $busqueda[$this->data['tabla']['controller'].".$value LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
            }
        }
        #$busqueda[$this->data['tabla']['controller'].".activo"]='1';
        /*else
        {
            //Busqueda personalizada para Mensajeros
            $empleados = $this->$contro->query("SELECT id FROM empleados where nombres like _utf8 '%".$this->data[$tableName]['search_text']."%' or apellidos like _utf8 '%".$this->data[$tableName]['search_text']."%';");
            //debug($empleados);
            $c=0;
            foreach ($empleados as $i => $valor)
            {
                $busqueda[$c]=array($this->data['tabla']['controller'].".empleado_id = "=>$valor['empleados']['id']);
                $c++;
            }
        }*/
        $reunionesSearchText = $this->data[$tableName]['search_text'];
    }
#debug($busqueda);

    // con este arreglo agregamos todos los campos que queremos validar en index colocar el id y nombre correspondiente
    $camposComparar = array('torneo_id','jornada_id','jugador_id','equipo_id1','equipo_id2',"evento_id", "jugadore_id");
    //debug($camposComparar);
    foreach($camposComparar as $k=>$v)
    {
        if(isset($this->data[$tableName][$v]))
        {
            if($this->data[$tableName][$v]>0)
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }

            $finalquery = 1;
        }
    }

    if($this->data[$tableName]['rangofecha']!='' ||  $this->data[$tableName]['rangofecha']!= ' ' || $this->data[$tableName]['rangofecha'] == 'si')
    {
        if(isset($this->data[$tableName]['desde']) && $this->data[$tableName]['desde']!='')
        {
            //hacer las validaciones para hacer las busqueda en el rango de fechas
            $fecha1 = "";
            $fecha2 = "";
            $campoFecha = "";
            $arrayFecha1=explode("-", $this->data[$tableName]['desde']);
            $campoFecha = $this->data[$tableName]['campoFecha'];
            if (strlen($arrayFecha1[0])==4) {
                $fecha1=$arrayFecha1[0]."-".$arrayFecha1[1]."-".$arrayFecha1[2];
            }elseif (strlen($arrayFecha1[0])==2 || strlen($arrayFecha1[0])==1) {
                $fecha1=$arrayFecha1[2]."-".$arrayFecha1[1]."-".$arrayFecha1[0];
            }
            if ($this->data[$tableName]['hasta']) {
                $arrayFecha2=explode("-", $this->data[$tableName]['hasta']);
                if (strlen($arrayFecha2[0])==4) {
                    $fecha2=$arrayFecha2[0]."-".$arrayFecha2[1]."-".$arrayFecha2[2];
                }elseif (strlen($arrayFecha2[0])==2 || strlen($arrayFecha2[0])==1) {
                    $fecha2=$arrayFecha2[2]."-".$arrayFecha2[1]."-".$arrayFecha2[0];
                }
            }
            $fecha1 = ($fecha1=="")?$fecha2:$fecha1;
            $fecha2 = ($fecha2=="")?$fecha1:$fecha2;

            if(isset($this->data[$tableName]['FechaHora']) && $this->data[$tableName]['FechaHora'] == 'si')
            {
                $arrayFecha = array($this->data['tabla']['controller'].".".$campoFecha." BETWEEN ? AND ? " => array($fecha1." 00:00:00",$fecha2." 23:59:59"));

            }
            else
            {
                $arrayFecha = array($this->data['tabla']['controller'].".".$campoFecha." BETWEEN ? AND ? " => array($fecha1,$fecha2));
            }
            $finalquery = 1;
            $finalquery2 = 1;
        }
    }

    //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)
    $finalquery=1;
    if($finalquery==0)
    {
        if($perfil==3 || $perfil==4 || $perfil==5) {
            //Usuarios BASICO, ESTANDAR, PREMIUN
            if(count($torneos)>1) {
                $condition= array('conditions'=>array('OR'=>$busqueda, "AND"=>[$this->data['tabla']['controller'].".torneo_id IN"=>$torneos]), "limit"=>20);
            }else{
                $condition= array('conditions'=>array('OR'=>$busqueda, "AND"=>[$this->data['tabla']['controller'].".torneo_id"=>$torneos]), "limit"=>20);
            }
        }else{
            $condition= array('conditions'=>array('OR'=>$busqueda), "limit"=>20);
        }
        //debug($condition);
    }
    else
    {
        if($finalquery2==1)
        {
            if($perfil==3 || $perfil==4 || $perfil==5) {
                //Usuarios BASICO, ESTANDAR, PREMIUN
                if(count($torneos)>1) {
                    $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento, "and"=>[$this->data['tabla']['controller'].".torneo_id IN"=>$torneos]), "limit"=>20);
                }else{
                    $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento, "and"=>[$this->data['tabla']['controller'].".torneo_id"=>$torneos]), "limit"=>20);
                }
            }else{
                $condition= array('conditions'=>array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento), "limit"=>20);
            }

        }
        else
        {
            if($perfil==3 || $perfil==4 || $perfil==5) {
                //Usuarios BASICO, ESTANDAR, PREMIUN
                if(count($torneos)>1) {
                    $condition= array('conditions'=>array(' OR'=>$busqueda, $complemento, "and"=>[$this->data['tabla']['controller'].".torneo_id IN"=>$torneos]), "limit"=>20);
                }else{
                    $condition= array('conditions'=>array(' OR'=>$busqueda, $complemento, "and"=>[$this->data['tabla']['controller'].".torneo_id"=>$torneos]), "limit"=>20);
                }
            }else{
                $condition= array('conditions'=>array(' OR'=>$busqueda, $complemento), "limit"=>20);
            }
        }
    }
    $this->Session->write($this->params['controller'],$condition);
}
if($this->Session->read($this->params['controller']))
{
    $this->paginate=$this->Session->read($this->params['controller']);
    $data = $this->Paginator->paginate();
}
