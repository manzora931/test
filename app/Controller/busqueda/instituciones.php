<?php
//opciones del paginado
//$pag=$this->Paginator->paginate();
if($this->Session->read($this->params['controller']))
{
    $this->Paginator->settings = $this->paginate=$this->Session->read($this->params['controller']);
}
//opciones de la busqueda
if ($this->request->is(array('post', 'put')))
{
    $busqueda=array();
    $complemento=array();
    $actividad=array();
    $arrayFecha=array();
    $status=array();
    $finalquery = 0;
    $finalquery2 = 0;
    //guardamos todo en la session
    //nombre de la tabla
    $tableName = $this->data['tabla']['tabla'];
    $this->Session->write("tabla[".$tableName."]", $this->data[$tableName]);
    //aqui se ingresa cuando el campo basico de search_text lleva informaci?n
    #debug($this->data[$tableName]['estado']);

    if(isset($this->data[$tableName]['search_text']) && $this->data[$tableName]['search_text'] != '' ||  $this->data[$tableName]['search_text']!=' ')
    {
        $c=0;
        $contro=$this->data['tabla']['controller'];
        $comparacion=explode(",",$this->data['tabla']['parametro']);
        $tipoBusqueda=explode(",",$this->data['tabla']['tipobusqueda']);

        if($this->data[$tableName]['search_text'] != '' and  $this->data[$tableName]['search_text'] != ' ' and $this->data['tabla']['parametro'] != 'personalizado')
        {
            foreach ($tipoBusqueda as $clave => $modelo) {
                foreach ($comparacion as $key => $value)
                {
                    if($clave == 0) {
                        $busqueda[$modelo.".$value LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
                    } else {
                        $busqueda[$modelo.".nombres LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
                        $busqueda[$modelo.".apellidos LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
                    }

                }
            }
        }

        $reunionesSearchText = $this->data[$tableName]['search_text'];
    }

    // con este arreglo agregamos todos los campos que queremos validar en index colocar el id y nombre correspondiente
    $camposComparar = array('estado', 'paise_id', 'oitestado_id', 'prioridad', 'resolucion','estructura_id','departamento_id','municipio_id','banco_id','tipobcocuenta_id','nombre');
    foreach($camposComparar as $k=>$v)
    {
        if(isset($this->data[$tableName][$v]))
        {
            if($this->data[$tableName][$v]>0)
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
            if($this->data[$tableName][$v]==0 && $v == "activa")
            {

                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
            $finalquery = 1;
            //personalizado para mensajeros

            /*if($this->data[$tableName][$v]==0 && $v == "activo")
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];

                $finalquery = 1;
            }*/
        }
    }

    if($this->data[$tableName]['rangofecha']!='' ||  $this->data[$tableName]['rangofecha']!= ' ' || $this->data[$tableName]['rangofecha'] == 'si')
    {
        if(isset($this->data[$tableName]['fecha_inicio']) && $this->data[$tableName]['fecha_inicio']!='')
        {
            //hacer las validaciones para hacer las busqueda en el rango de fechas
            $fecha1 = "";
            $fecha2 = "";
            $campoFecha = "";
            $arrayFecha1=explode("-", $this->data[$tableName]['fecha_inicio']);
            $campoFecha = $this->data[$tableName]['campoFecha'];
            if (strlen($arrayFecha1[0])==4) {
                $fecha1=$arrayFecha1[0]."-".$arrayFecha1[1]."-".$arrayFecha1[2];
            }elseif (strlen($arrayFecha1[0])==2 || strlen($arrayFecha1[0])==1) {
                $fecha1=$arrayFecha1[2]."-".$arrayFecha1[1]."-".$arrayFecha1[0];
            }
            if ($this->data[$tableName]['fecha_fin']) {
                $arrayFecha2=explode("-", $this->data[$tableName]['fecha_fin']);
                if (strlen($arrayFecha2[0])==4) {
                    $fecha2=$arrayFecha2[0]."-".$arrayFecha2[1]."-".$arrayFecha2[2];
                }elseif (strlen($arrayFecha2[0])==2 || strlen($arrayFecha2[0])==1) {
                    $fecha2=$arrayFecha2[2]."-".$arrayFecha2[1]."-".$arrayFecha2[0];
                }
            }
            $fecha1 = ($fecha1=="")?$fecha2:$fecha1;
            $fecha2 = ($fecha2=="")?$fecha1:$fecha2;

            if(isset($this->data[$tableName]['FechaHora']) && $this->data[$tableName]['FechaHora'] == 'si')
            {
                $arrayFecha = array($this->data['tabla']['controller'].".".$campoFecha." BETWEEN ? AND ? " => array($fecha1." 00:00:00",$fecha2." 23:59:59"));

            }
            else
            {
                $arrayFecha = array($this->data['tabla']['controller'].".".$campoFecha." BETWEEN ? AND ? " => array($fecha1,$fecha2));
                #debug($arrayFecha);
            }
            $finalquery = 1;
            $finalquery2 = 1;
        }
    }
    if (isset($this->data[$tableName]['activo'])||isset($this->data[$tableName]['activa'])) {
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)

        $finalquery=1;
        if($finalquery==0)
        {
            $condition= array('OR'=>$busqueda, 'OR' => array($status));
        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento);
            }
            else
            {
                $status = array($this->data['tabla']['controller'].".id != " => 0);
                $condition= array(' OR'=>$busqueda, $complemento,$status);
            }
        }
    }else{
        //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)
        if($finalquery==0)
        {
            $condition= array('OR'=>$busqueda, $this->data['tabla']['controller'].".activo"=>1);
        }
        else
        {
            if($finalquery2==1)
            {
                $condition= array($arrayFecha, ' OR'=>$busqueda, 'AND'=>$complemento);
            }
            else
            {
                $condition= array(' OR'=>$busqueda, 'AND'=>$complemento,$this->data['tabla']['controller'].".activo"=>1);
            }
        }
    }

    $condition = array(
        'joins' => array(
            array(
                'table' => 'contactos',
                'alias' => 'Contacto',
                'type' => 'INNER',
                'conditions' => array(
                    'Contacto.institucion_id = Institucion.id'
                )
            )
        ),
        'conditions' => $condition,
        'fields' => array('Institucion.*','Paise.*'),
        'group'=>array('Institucion.id'),
        'order'=>array('Institucion.nombre'=>'asc')

);

    $this->Session->write($this->params['controller'],$condition);
}
// debug($this->Session->read($this->params['controller']));
// die();
if($this->Session->read($this->params['controller']))
{
    $this->paginate=$this->Session->read($this->params['controller']);
    $data = $this->Paginator->paginate();
}