<?php
if($this->Session->read($this->params['controller']))
{
    $this->Paginator->settings = $this->paginate=$this->Session->read($this->params['controller']);
}
// Opciones de la busqueda
if ($this->request->is(array('post', 'put')))
{
    $busqueda=array();
    $complemento=array();
    $complemento2=array();
    $actividad=array();
    $arrayFecha=array();
    $status=array();
    $finalquery = 0;
    $finalquery2 = 0;
    //guardamos todo en la session
    //nombre de la tabla
    $tableName = $this->data['tabla']['tabla'];
    $this->Session->write("tabla[".$tableName."]", $this->data[$tableName]);

    //aqui se ingresa cuando el campo basico de search_text lleva informacion
    if(isset($this->data[$tableName]['search_text']) && $this->data[$tableName]['search_text'] != '' ||  $this->data[$tableName]['search_text']!=' ')
    {
        $c=0;
        $contro=$this->data['tabla']['controller'];
        $comparacion=explode(",",$this->data['tabla']['parametro']);

        if($this->data[$tableName]['search_text'] != '' and  $this->data[$tableName]['search_text'] != ' ' and $this->data['tabla']['parametro'] != 'personalizado')
        {
            foreach ($comparacion as $key => $value)
            {
                $busqueda[$this->data['tabla']['controller'].".$value LIKE "]='%'.$this->data[$tableName]['search_text'].'%';
            }
        }
    }

    // con este arreglo agregamos todos los campos que queremos validar en index colocar el id y nombre correspondiente
    $camposComparar = array('torneo_id','estadio_id',"equipo_id", "etapa_id");
    foreach($camposComparar as $k=>$v)
    {
        if(isset($this->data[$tableName][$v]))
        {
            if($this->data[$tableName][$v]>0)
            {
                if($v=="equipo_id"){
                    $complemento2[$this->data['tabla']['controller'].".equipo1_id"]= $this->data[$tableName][$v];
                    $complemento2[$this->data['tabla']['controller'].".equipo2_id"]= $this->data[$tableName][$v];
                    $finalquery = 1;
                }else{
                    $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                    $finalquery = 1;
                }
            }
            //personalizado para mensajeros
            if($this->data[$tableName][$v]==0 && $v == "activo")
            {
                $complemento[$this->data['tabla']['controller'].".".$v]= $this->data[$tableName][$v];
                $finalquery = 1;
            }
        }
    }

    //con este le realizamos la condicion sin enviar datos extras (como el rango de fechas)
    if($finalquery==0)
    {
        if($perfil==3 || $perfil==4 || $perfil==5){
            if(count($torneos)>1) {
                $condition = array('conditions' => array('OR' => $busqueda, "AND"=>["Juego.torneo_id IN"=>$torneos]), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
            }else{
                $condition = array('conditions' => array('OR' => $busqueda, "AND"=>["Juego.torneo_id"=>$torneos]), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
            }
        }else{
            if($perfil==3 || $perfil==4 || $perfil==5) {
                //Usuarios BASICO, ESTANDAR, PREMIUN
                if(count($torneos)>1) {
                    $condition = array('conditions' => array('OR' => $busqueda, "AND"=>["Juego.torneo_id IN"=>$torneos]), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }else{
                    $condition = array('conditions' => array('OR' => $busqueda, "AND"=>["Juego.torneo_id"=>$torneos]), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }
            }else{
                //USUARIO ADMIN, OPERADOR
                $condition = array('conditions' => array('OR' => $busqueda), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
            }
        }
    }else {
        if($finalquery2==1) {
            if($perfil==3 || $perfil==4 || $perfil==5) {
                //Usuarios BASICO, ESTANDAR, PREMIUN
                if(count($torneos)>1) {
                    $condition = array('conditions' => array($arrayFecha, ' OR' => $busqueda, 'AND' => $complemento, "and"=>["Juego.torneo_id IN"=>$torneos], "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }else{
                    $condition = array('conditions' => array($arrayFecha, ' OR' => $busqueda, 'AND' => $complemento, "and"=>["Juego.torneo_id"=>$torneos], "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }
            }else{
                //USUARIO ADMIN, OPERADOR
                $condition = array('conditions' => array($arrayFecha, ' OR' => $busqueda, 'AND' => $complemento, "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
            }
        }else {
            if($perfil==3 || $perfil==4 || $perfil==5) {
                //Usuarios BASICO, ESTANDAR, PREMIUN
                if(count($torneos)>1) {
                    $condition = array('conditions' => array(' OR' => $busqueda, 'AND' => $complemento, "and"=>["Juego.torneo_id IN"=>$torneos], "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }else {
                    $condition = array('conditions' => array(' OR' => $busqueda, 'AND' => $complemento, "and"=>["Juego.torneo_id"=>$torneos], "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
                }
            }else{
                //USUARIO ADMIN, OPERADOR
                $condition = array('conditions' => array(' OR' => $busqueda, 'AND' => $complemento, "or" => $complemento2), 'order' => array('Juego.fecha' => 'desc', 'Juego.hora' => 'desc'));
            }
        }
    }
    $this->Session->write($this->params['controller'],$condition);
}
if($this->Session->read($this->params['controller']))
{
    $this->paginate=$this->Session->read($this->params['controller']);
    $data = $this->Paginator->paginate();
}