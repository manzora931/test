<?php
App::uses('AppController', 'Controller');
/**
 * Equiposxtorneos Controller
 *
 * @property Equiposxtorneo $Equiposxtorneo
 * @property PaginatorComponent $Paginator
 */
class EquiposxtorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $helpers = array('Html','Js', 'Form');
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Equiposxtorneo", "equiposxtorneos", "index");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->Paginator->settings = array('conditions'=>["Equiposxtorneo.torneo_id IN"=>$torneos] ,'order'=>array('Torneo.nombrecorto'=>'asc'));
            }else{
                $this->Paginator->settings = array('conditions'=>["Equiposxtorneo.torneo_id"=>$torneos] ,'order'=>array('Torneo.nombrecorto'=>'asc'));
            }
        }else{
            $this->Paginator->settings = array('order'=>array('Torneo.nombrecorto'=>'asc'));
        }

        $this->Equiposxtorneo->recursive = 0;
        include 'busqueda/equiposxtorneo.php';
        $data = $this->Paginator->paginate('Equiposxtorneo');
		$this->set('equiposxtorneos', $data);
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $torneos = $this->Equiposxtorneo->Torneo->find('list',["conditions"=>["Torneo.activo"=>1,"Equiposxtorneo.torneo_id IN"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }else{
                $torneos = $this->Equiposxtorneo->Torneo->find('list',["conditions"=>["Torneo.activo"=>1,"Equiposxtorneo.torneo_id"=>$torneos],"order"=>["Torneo.nombrecorto"]]);
            }
        }else{
            $torneos = $this->Equiposxtorneo->Torneo->find('list',["conditions"=>["Torneo.activo"=>1],"order"=>["Torneo.nombrecorto"]]);
        }
        $equipos = $this->Equiposxtorneo->Equipo->find('list');
        $this->set(compact("torneos","equipos"));
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[equiposxtorneos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Equiposxtorneo", "equiposxtorneos", "view");
		if (!$this->Equiposxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid equiposxtorneo'));
		}
		$options = array('conditions' => array('Equiposxtorneo.' . $this->Equiposxtorneo->primaryKey => $id));
		$this->set('equiposxtorneo', $this->Equiposxtorneo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Equiposxtorneo", "equiposxtorneos", "add");
		if ($this->request->is('post')) {
            $this->request->data['Equiposxtorneo']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Equiposxtorneo']['modified']=0;
			$this->Equiposxtorneo->create();
			if ($this->Equiposxtorneo->save($this->request->data)) {
                $id = $this->Equiposxtorneo->id;
                $this->Session->write('equipoxtorneo_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The equiposxtorneo could not be saved. Please, try again.'));
			}
		}
		$torneos = $this->Equiposxtorneo->Torneo->find('list',[
            "conditions"=>["Torneo.activo"=>1],
            "order"=>["Torneo.nombrecorto"=>"asc"]
        ]);
		$equipos = $this->Equiposxtorneo->Equipo->find('list',[
		    "order"=>["Equipo.nombrecorto"=>"asc"]
        ]);
		$this->set(compact('torneos', 'equipos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Equiposxtorneo", "equiposxtorneos", "edit");
		if (!$this->Equiposxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid equiposxtorneo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Equiposxtorneo']['usuariomodif'] = $this->Session->read('nombreusuario');
            $this->request->data['Equiposxtorneo']['modified'] = date("Y-m-d H:i:s");
			if ($this->Equiposxtorneo->save($this->request->data)) {
                $id = $this->Equiposxtorneo->id;
                $this->Session->write('equipoxtorneo_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The equiposxtorneo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Equiposxtorneo.' . $this->Equiposxtorneo->primaryKey => $id));
			$this->request->data = $this->Equiposxtorneo->find('first', $options);
		}
        $torneos = $this->Equiposxtorneo->Torneo->find('list',[
            "conditions"=>["Torneo.activo"=>1],
            "order"=>["Torneo.nombrecorto"=>"asc"]
        ]);
        $equipos = $this->Equiposxtorneo->Equipo->find('list',[
            "order"=>["Equipo.nombrecorto"=>"asc"]
        ]);
		$this->set(compact('torneos', 'equipos'));
	}
    /**METODO PARA VALIDAR QUE EL EQUIPO NO SE INGRESE MAS DE UNA VEZ EN UN TORNEO***/
    public function valUnique(){
        if($_POST["id"]>0){
            $data = $this->Equiposxtorneo->find("all",[
                "fields"=>["Equiposxtorneo.id"],
                "conditions"=>[
                    "Equiposxtorneo.torneo_id"=>$_POST["torneo"],
                    "Equiposxtorneo.equipo_id"=>$_POST["equipo"],
                    "Equiposxtorneo.id !="=>$_POST["id"]
                ]
            ]);
        }else{
            $data = $this->Equiposxtorneo->find("all",[
                "fields"=>["Equiposxtorneo.id"],
                "conditions"=>[
                    "Equiposxtorneo.torneo_id"=>$_POST["torneo"],"Equiposxtorneo.equipo_id"=>$_POST["equipo"]
                ]
            ]);
        }

        $band = 0;
        if(count($data)>0){
            $band=1;
        }
        echo $band;
        $this->autoRender=false;
    }
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Equiposxtorneo", "equiposxtorneos", "delete");
		if ($delete == true) {
			$this->Equiposxtorneo->id = $id;
			if (!$this->Equiposxtorneo->exists()) {
				throw new NotFoundException(__('Invalid equiposxtorneo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Equiposxtorneo->delete()) {
					$this->Session->setFlash(__('The equiposxtorneo has been deleted.'));
			} else {
				$this->Session->setFlash(__('The equiposxtorneo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
