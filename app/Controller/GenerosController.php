<?php
App::uses('AppController', 'Controller');
/**
 * Generos Controller
 *
 * @property Genero $Genero
 * @property PaginatorComponent $Paginator
 */
class GenerosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Genero", "generos", "index");
        $this->Paginator->settings = array('conditions' => array('Genero.activo >=' => 1), 'order'=>array('Genero.genero'=>'asc'));
        $this->Genero->recursive = 0;
        include 'busqueda/generos.php';
        $data = $this->Paginator->paginate('Genero');
        $this->set('generos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[generos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Genero", "generos", "view");
		if (!$this->Genero->exists($id)) {
			throw new NotFoundException(__('Invalid genero'));
		}
		$options = array('conditions' => array('Genero.' . $this->Genero->primaryKey => $id));
		$this->set('genero', $this->Genero->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Genero", "generos", "add");
		if ($this->request->is('post')) {
		    $this->Genero->create();
            $this->request->data['Genero']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Genero']['modified']=0;
			if ($this->Genero->save($this->request->data)) {
                $genero_id = $this->Genero->id;
                $this->Session->write('genero_save', 1);
                $this->redirect(['action' => 'view', $genero_id]);
			} else {
				$this->Session->setFlash(__('El genero no ha sido creado. Intentar de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Genero", "generos", "edit");
		if (!$this->Genero->exists($id)) {
			throw new NotFoundException(__('Invalid genero'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Genero']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Genero->save($this->request->data)) {
                $this->Session->write('genero_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('El genero no ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Genero.' . $this->Genero->primaryKey => $id));
			$this->request->data = $this->Genero->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Genero", "generos", "delete");
		if ($delete == true) {
			$this->Genero->id = $id;
			if (!$this->Genero->exists()) {
				throw new NotFoundException(__('Invalid genero'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Genero->delete()) {
					$this->Session->setFlash(__('The genero has been deleted.'));
			} else {
				$this->Session->setFlash(__('The genero could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Generoincidencia');
        $this->Generoincidencia->recursive = -1;
        $genero = $this->Generoincidencia->find('all', [
            'conditions' => [
                'Generoincidencia.genero_id' => $id,
            ]
        ]);

        $data['generos'] = (count($genero) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
