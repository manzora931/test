<?php
App::uses('AppController', 'Controller');
/**
 * Privilegiosregistrosrecursos Controller
 *
 * @property Privilegiosregistrosrecurso $Privilegiosregistrosrecurso
 * @property PaginatorComponent $Paginator
 */
class PrivilegiosregistrosrecursosController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	var $helpers = array('Html', 'Form','Js','Session');
	public $PaginadosGroup = 10;

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Privilegiosregistrosrecurso", "privilegiosregistrosrecursos", "index");
		$this->Privilegiosregistrosrecurso->recursive = 0;
		//$this->Paginator->settings=array('order'=>array("Privilegiosregistrosrecurso."));
		$this->set('privilegiosregistrosrecursos', $this->Paginator->paginate());
	}

	/*	TABLA MAESTRA DE DETALLE*/
	public function users($pag=null){
		$this->layout = 'ventanas';
		if(!empty($this->data['privilegiosregistrosrecursos'])){
			if($this->data['privilegiosregistrosrecursos']['SearchText']!='') {
				$this->Session->write('text_prr', $this->data['privilegiosregistrosrecursos']['SearchText']);
			}else{
				unset($_SESSION['text_prr']);
			}
			if(isset($this->data['privilegiosregistrosrecursos']['ac'])){
				$this->Session->write('act', 1);
			}else{
				$this->Session->delete("act");
			}
		}
		if($this->Session->check('text_prr')===true or $this->Session->check('act')===true) {
			$users1 = "SELECT p.id, u.username,g.`name`,u.id FROM `privilegiosregistrosrecursos` p INNER JOIN users u on p.user_id = u.id INNER JOIN groups g on g.id = u.group_id WHERE p.activo=1";
			$users2 = "SELECT p.id FROM `privilegiosregistrosrecursos` p INNER JOIN users u on p.user_id = u.id INNER JOIN groups g on g.id = u.group_id WHERE p.activo=1";
			//$groups1 = "SELECT g.id,name,g.created,g.modified,g.activo FROM privilegios p RIGHT JOIN groups g on g.id=p.group_id LEFT JOIN recursos r on p.recurso_id=r.id LEFT JOIN modulos m on m.id=r.modulo_id WHERE g.activo=1";
			//$groups2 = "SELECT count(g.id) as totgroup FROM privilegios p RIGHT JOIN groups g on g.id=p.group_id LEFT JOIN recursos r on p.recurso_id=r.id LEFT JOIN modulos m on m.id=r.modulo_id where g.activo=1";

			if ($this->Session->read('text_prr') != '') {
				$users1 .= " AND ((u.username like'%" . $this->Session->read('text_prr') . "%') OR (g.name like'%" . $this->Session->read('text_prr') . "%'))";
				$users2 .= " AND ((u.username like'%" . $this->Session->read('text_prr') . "%') OR (g.name like'%" . $this->Session->read('text_prr') . "%'))";
			}

			if ($this->Session->read('act') != '' && $this->Session->read('text_prr') == '') {
				$users1 = str_replace("WHERE p.activo=1", " ", $users1);
				$users2 = str_replace("WHERE p.activo=1", " ", $users2);
			}elseif($this->Session->read('act') != '' && $this->Session->read('text_prr') != ''){
				$users1 = str_replace("p.activo=1", "p.activo=0", $users1);
				$users2 = str_replace("p.activo=1", "p.activo=0", $users2);
			}

			if ($pag != null && $pag > 0) {
				if ($pag > 1) {
					$inicio = (($pag - 1) * $this->PaginadosGroup);
				} else {
					$inicio = 0;
				}
				$users1 .= " GROUP BY u.id,g.id ORDER BY u.username DESC limit " . $inicio . ", " . $this->PaginadosGroup;
				$users2 .= " GROUP BY u.id,g.id ORDER BY u.username DESC";

			} else {
				$users1 .= " GROUP BY u.id,g.id ORDER BY u.username DESC limit " . $this->PaginadosGroup;
				$users2 .= " GROUP BY u.id,g.id ORDER BY u.username DESC";
			}

			$users = $this->Privilegiosregistrosrecurso->query($users1);
			$totuser = $this->Privilegiosregistrosrecurso->query($users2);
			$totusers = count($totuser);
			if ($pag != null && $pag > 0) {
				$this->set(compact("users", "totusers", "pag"));
			} else {
				$pag = 1;
				$this->set(compact("users", "totusers", "pag"));
			}
		}else{
			$users1 = "SELECT p.id, u.username,g.`name`,u.id FROM `privilegiosregistrosrecursos` p INNER JOIN users u on p.user_id = u.id INNER JOIN groups g on g.id = u.group_id WHERE p.activo=1";
			$users2 = "SELECT p.id as totuser FROM `privilegiosregistrosrecursos` p INNER JOIN users u on p.user_id = u.id INNER JOIN groups g on g.id = u.group_id WHERE p.activo=1";

			if ($pag != null && $pag > 0) {
				if ($pag > 1) {
					$inicio = (($pag - 1) * $this->PaginadosGroup);

				} else {
					$inicio = 0;
				}
				$users1 .= " GROUP BY u.id,g.id ORDER BY u.username DESC limit " . $inicio . ", " . $this->PaginadosGroup;
				$users2 .= " GROUP BY u.id,g.id ORDER BY u.username DESC ";
			} else {
				$users1 .= " GROUP BY u.id,g.id ORDER BY u.username DESC limit " . $this->PaginadosGroup;
				$users2 .= " GROUP BY u.id,g.id ORDER BY u.username DESC";
			}

			$users = $this->Privilegiosregistrosrecurso->query($users1);
			$totuser = $this->Privilegiosregistrosrecurso->query($users2);
			$totusers = count($totuser);

			if ($pag != null && $pag > 0) {
				$this->set(compact("users", "totusers", "pag"));
			} else {
				$pag = 1;
				$this->set(compact("users", "totusers", "pag"));
			}
		}

		$this->render('users', 'ajax');

	}

	public function priv($var=null){
		$this->layout = 'ventanas';
		$bandera=0;
		$username="";
		if($var==null){
			$bandera=1;
		}else{
			$var=explode('-',$var);
			$user_id=$var[0];
			$pag=$var[1];
			$recs=$this->Privilegiosregistrosrecurso->query("SELECT p.id,rec.modelo,rg.recurso_id,rg.tabla,rg.campo,dt.registro_id,dt.permitir FROM privilegiosregistrosrecursos p INNER JOIN det_privilegiosregistrosrecursos dt on dt.privilegiosregistrosrecurso_id = p.id
INNER JOIN registrosrecursos rg on dt.det_registrosrecurso_id = rg.id INNER JOIN recursos rec on rec.id = rg.recurso_id WHERE p.user_id = ".$user_id." AND p.activo=1 ORDER BY rec.modelo ASC");
			$query2 = $this->Privilegiosregistrosrecurso->query("SELECT username FROM users WHERE id = ".$user_id);
			$username = $query2[0]['users']['username'];
			$data=array();
			$mod="";

			foreach($recs as $rec => $row){
				$mod = $row['rg']['tabla'];
				$query = $this->Privilegiosregistrosrecurso->query("SELECT ".$row['rg']['campo']." FROM ".$row['rg']['tabla']." WHERE id = ".$row['dt']['registro_id']);
				if(count($query)>0){
					$data[$mod][$row['rg']['recurso_id']][$row['dt']['registro_id']]['modelo'] = $mod;
					$data[$mod][$row['rg']['recurso_id']][$row['dt']['registro_id']]['registro'] = $query[0][$row['rg']['tabla']][$row['rg']['campo']];
					$data[$mod][$row['rg']['recurso_id']][$row['dt']['registro_id']]['permitir'] = $row['dt']['permitir'];
					$data[$mod][$row['rg']['recurso_id']][$row['dt']['registro_id']]['privilegio'] = $row['p']['id'];
					$data[$mod][$row['rg']['recurso_id']][$row['dt']['registro_id']]['tabla'] = $row['rg']['tabla'];

				}
			}
			$this->set(compact("data", "pag","user_id",'username'));
		}
		$this->set(compact("bandera"));
		$this->render('priv', 'ajax');
	}

	/*FUNCION PARA ALMACENAR DESDE AJAX*/

	public function save(){
		$detalle = $_POST['detalle'];
		$modulo = $_POST['modulo'];
		$recurso = $_POST['recurso'];
		$rec_pantalla = $_POST['rec_pantalla'];
		$user = $_POST['user'];
		$s_prop = $_POST['s_prop'];
		$activo = $_POST['activo'];
		$general = array();
		$general['registrosrecurso_id'] = $rec_pantalla;
		$general['user_id']=$user;
		$general['solopropietario']=$s_prop;
		$general['activo'] = $activo;
		$general['usuario']=$this->Session->read('nombreusuario');
		$general['modified']="0000-00-00 00:00:00";
		$det=array();
		$now = date("Y-m-d H:i:s");
		$usuario = $this->Session->read("nombreusuario");
		$this->Privilegiosregistrosrecurso->set($general);
		if($this->Privilegiosregistrosrecurso->save()){
			$id_p = $this->Privilegiosregistrosrecurso->id;
			foreach($detalle as $cell => $row){
				$arr = explode("#",$row);
				if($arr[0] != '' || $arr[0] != 0){
					$det['privilegiosregistrosrecurso_id'] = $id_p;
					$det['det_registrosrecurso_id'] = $rec_pantalla;
					$det['registro_id'] = $arr[0];
					$det['permitir'] = $arr[1];
					$det['usuario']=$this->Session->read('nombreusuario');
					$det['modified']='0000-00-00 00:00:00';

					$query = "INSERT INTO `det_privilegiosregistrosrecursos`(`privilegiosregistrosrecurso_id`,
 `det_registrosrecurso_id`, `registro_id`, `permitir`, `created`, `usuario`) VALUES (".$id_p.",".$rec_pantalla.",".$arr[0].",".$arr[1].",'$now','$usuario')";
					$this->Privilegiosregistrosrecurso->query($query);
				}
			}
			echo "ok";
		}
		$this->autoRender=false;
	}
	public function limpiar(){
		$this->Session->delete("act");
		$this->Session->delete("text_prr");
		$this->autoRender=false;
	}
	/*VALIDACION PARA NO INGRESAR PRIVILEGIOS DUPLICADOS*/
	function val_privi(){
		$user = $_POST['user'];
		$priv = $_POST['priv'];
		$sl = $this->Privilegiosregistrosrecurso->query("SELECT id from privilegiosregistrosrecursos WHERE user_id = ".$user." AND registrosrecurso_id = ".$priv." AND activo=1");
		$error = 0;
		if(count($sl)>0){
			$error=1;
		}
		echo $error;
		$this->autoRender=false;
	}
	/*EDITAR PRIVILEGIO CON AJAX*/
	public function save2(){
		$id_privilegio = $_POST['id_pr'];
		$detalle = $_POST['detalle'];
		$rec_pantalla = $_POST['rec_pantalla'];
		$user = $_POST['user'];
		$s_prop = $_POST['s_prop'];
		$activo = $_POST['activo'];
		$general = array();
		$general['id']=$id_privilegio;
		$general['registrosrecurso_id'] = $rec_pantalla;
		$general['user_id']=$user;
		$general['solopropietario']=$s_prop;
		$general['activo'] = $activo;
		$general['usuario']=$this->Session->read('nombreusuario');
		$general['modified']="0000-00-00 00:00:00";
		$det=array();
		$now = date("Y-m-d H:i:s");
		$usuario = $this->Session->read("nombreusuario");
		$ids = array();
		$sl_id = $this->Privilegiosregistrosrecurso->query("SELECT id FROM det_privilegiosregistrosrecursos WHERE privilegiosregistrosrecurso_id = ".$id_privilegio);
		$band = 0;
		foreach($sl_id as $r => $k){
			foreach($detalle as $cell2 => $row2){
				$arr2 = explode("#",$row2);
				if($arr2[0] != '' || $arr2[0] != 0){
					if(isset($arr2[2]) && $arr2[2] != ''){
						if($arr2[2] == $k['det_privilegiosregistrosrecursos']['id']){
							$band=1;
						}
					}
				}
			}
			if($band==0){
				$this->Privilegiosregistrosrecurso->query("DELETE from det_privilegiosregistrosrecursos WHERE id = ".$k['det_privilegiosregistrosrecursos']['id']);
			}
			$band=0;
		}
		$this->Privilegiosregistrosrecurso->set($general);
		if($this->Privilegiosregistrosrecurso->save()){
			$id_p = $this->Privilegiosregistrosrecurso->id;
			foreach($detalle as $cell => $row){
				$arr = explode("#",$row);
				if($arr[0] != '' || $arr[0] != 0){
					$query="";
					$det['id'] = $arr[2];

					$det['privilegiosregistrosrecurso_id'] = $id_p;
					$det['det_registrosrecurso_id'] = $rec_pantalla;
					$det['registro_id'] = $arr[0];
					$det['permitir'] = $arr[1];
					$det['usuario']=$this->Session->read('nombreusuario');
					$det['modified']='0000-00-00 00:00:00';
					if(isset($arr[2]) && $arr[2] != ''){
						$query = "UPDATE det_privilegiosregistrosrecursos SET det_registrosrecurso_id = ".$rec_pantalla.", registro_id = ".$arr[0].",permitir=".$arr[1].",usuariomodif = '$usuario',modified = '$now' WHERE id = ".$arr[2];
					}else{
						$query = "INSERT INTO `det_privilegiosregistrosrecursos`(`privilegiosregistrosrecurso_id`,
 `det_registrosrecurso_id`, `registro_id`, `permitir`, `created`, `usuario`) VALUES (".$id_p.",".$rec_pantalla.",".$arr[0].",".$arr[1].",'$now','$usuario')";
					}
					$this->Privilegiosregistrosrecurso->query($query);
				}
			}
			echo "ok";
		}
		$this->autoRender=false;
	}

	/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Privilegiosregistrosrecurso", "privilegiosregistrosrecursos", "view");
		if (!$this->Privilegiosregistrosrecurso->exists($id)) {
			throw new NotFoundException(__('Privilegio de recurso no valido'));
		}
		$options = array('conditions' => array('Privilegiosregistrosrecurso.' . $this->Privilegiosregistrosrecurso->primaryKey => $id));
		$this->set('privilegiosregistrosrecurso', $this->Privilegiosregistrosrecurso->find('first', $options));
		//$registrosList  = $this->Privilegiosregistrosrecurso->Registrosrecurso->find('list');
		$registros 		= $this->Privilegiosregistrosrecurso->query("SELECT registro_id, det_registrosrecurso_id, permitir FROM `det_privilegiosregistrosrecursos` WHERE `privilegiosregistrosrecurso_id` = ".$id);
		$registrosRec	= $this->Privilegiosregistrosrecurso->query("SELECT tabla, campo, id FROM registrosrecursos WHERE id = ".$registros[0]['det_privilegiosregistrosrecursos']['det_registrosrecurso_id']);
		$recursoArry	= $this->Privilegiosregistrosrecurso->query("SELECT id, ".$registrosRec[0]['registrosrecursos']['campo']." FROM ".$registrosRec[0]['registrosrecursos']['tabla']);
		$recursos 		= array();
		foreach ( $recursoArry as $v) {
			$recursos[$v[$registrosRec[0]['registrosrecursos']['tabla']]['id']] = $v[$registrosRec[0]['registrosrecursos']['tabla']][$registrosRec[0]['registrosrecursos']['campo']];
		}
		$this->set(compact('registros', 'registrosRec','recursos'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Privilegiosregistrosrecurso", "privilegiosregistrosrecursos", "add");
		if ($this->request->is('post')) {
			$this->Privilegiosregistrosrecurso->create();
			$this->request->data['Privilegiosregistrosrecurso']['usuario'] = $this->Session->read('nombreusuario');
			/*
			 *
			 *$this->request->data['DetPrivilegiosregistrosrecurso][0] - ['registro_id'] o ['permitir']
			 * $this->request->data['Privilegiosregistrosrecurso']['usuario']
			 */
			if ($this->Privilegiosregistrosrecurso->save($this->request->data)) {
				$id = $this->Privilegiosregistrosrecurso->id;
				foreach($this->request->data['DetPrivilegiosregistrosrecurso'] as $k => $detalle){//Se almacena el detalle
					if(isset($detalle['registro_id']) && $detalle['registro_id'] != "" && !empty($detalle['registro_id']))
					{
						$this->Privilegiosregistrosrecurso->query("INSERT INTO `det_privilegiosregistrosrecursos`(`privilegiosregistrosrecurso_id`, `det_registrosrecurso_id`, `registro_id`, `permitir`, `created`, `usuario`) VALUES
																  	($id,".$this->request->data['Privilegiosregistrosrecurso']['registrosrecurso_id'].",".$detalle['registro_id'].",'".$detalle['permitir']."','".date("Y-m-d H:i:s")."','".$this->Session->read('nombreusuario')."')");;
					}
				}
				$this->Session->write("pri_user_save",1);
				$this->redirect(array('action' => 'view',$id));
				//return $this->flash(__('Privilegio de recurso Almacenado'), array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('<script> alert("No se pudo almacenar el Privilegio de recurso");</script>'));
			}
		}
		$users = $this->Privilegiosregistrosrecurso->User->find('list',array('conditions'=>array('User.activo'=>1),'order'=>array("User.name"=>"ASC")));
		$modulos=$this->Privilegiosregistrosrecurso->Registrosrecurso->Recurso->Modulo->find("list",array('conditions'=>array("Modulo.activo"=>1),'order'=>array("Modulo.modulo"=>"ASC")));
		$this->set(compact('modulos', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Privilegiosregistrosrecurso", "privilegiosregistrosrecursos", "edit");
		if (!$this->Privilegiosregistrosrecurso->exists($id)) {
			throw new NotFoundException(__('Privilegio de recurso no valido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Privilegiosregistrosrecurso']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Privilegiosregistrosrecurso->save($this->request->data)) {
				$id = $this->Privilegiosregistrosrecurso->id;

				foreach($this->request->data['DetPrivilegiosregistrosrecurso'] as $k => $detalle){//Se almacena el detalle
					if(isset($detalle['registro_id']) && $detalle['registro_id'] != "" && !empty($detalle['registro_id']))
					{
						if(!empty($detalle['id']))
						{
							$this->Privilegiosregistrosrecurso->query("UPDATE `det_privilegiosregistrosrecursos`
							SET
							`det_registrosrecurso_id`=" . $this->request->data['Privilegiosregistrosrecurso']['registrosrecurso_id'] . ",`registro_id`=" . $detalle['registro_id'] . ",`permitir`='" . $detalle['permitir'] . "', `modified`='" . date("Y-m-d H:i:s") . "',`usuariomodif`='" . $this->Session->read('nombreusuario') . "' WHERE `id` = ".$detalle['id']);
						}else {
							$this->Privilegiosregistrosrecurso->query("INSERT INTO `det_privilegiosregistrosrecursos`(`privilegiosregistrosrecurso_id`, `det_registrosrecurso_id`, `registro_id`, `permitir`, `created`, `usuario`) VALUES
																  	($id," . $this->request->data['Privilegiosregistrosrecurso']['registrosrecurso_id'] . "," . $detalle['registro_id'] . ",'" . $detalle['permitir'] . "','" . date("Y-m-d H:i:s") . "','" . $this->Session->read('nombreusuario') . "')");;
						}
					}
				}

				$this->Session->write("pri_user_save",1);
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->write("pri_user_save",0);
				$this->redirect(array('action' => 'view',$id));
			}
		} else {
			$options = array('conditions' => array('Privilegiosregistrosrecurso.' . $this->Privilegiosregistrosrecurso->primaryKey => $id));
			$this->request->data = $this->Privilegiosregistrosrecurso->find('first', $options);
		}
		$registros = $this->Privilegiosregistrosrecurso->query("SELECT id,privilegiosregistrosrecurso_id,det_registrosrecurso_id,registro_id,permitir FROM `det_privilegiosregistrosrecursos` WHERE `privilegiosregistrosrecurso_id` = ".$id);
		$registrosrecursos = $this->Privilegiosregistrosrecurso->Registrosrecurso->find('list',array('conditions'=>array('Registrosrecurso.activo'=>1)));
		$users = $this->Privilegiosregistrosrecurso->User->find('list',array("conditions"=>array("User.activo"=>1),'order'=>array("User.name"=>"ASC")));
		$this->set(compact('registrosrecursos', 'users','registros'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Privilegiosregistrosrecurso", "privilegiosregistrosrecursos", "delete");
		if ($delete == true) {
			$this->Privilegiosregistrosrecurso->id = $id;
			if (!$this->Privilegiosregistrosrecurso->exists()) {
				throw new NotFoundException(__('Invalid privilegiosregistrosrecurso'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Privilegiosregistrosrecurso->delete()) {
					$this->Session->setFlash(__('The privilegiosregistrosrecurso has been deleted.'));
			} else {
				$this->Session->setFlash(__('The privilegiosregistrosrecurso could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}
	function cargarregistro(){
		$query="SELECT * FROM registrosrecursos WHERE id = ".$_POST['idRes'];
		$registrosrecurso = $this->Privilegiosregistrosrecurso->query($query);

		$query="SELECT id,".$registrosrecurso[0]['registrosrecursos']['campo']." FROM ".$registrosrecurso[0]['registrosrecursos']['tabla'];
		$registros = $this->Privilegiosregistrosrecurso->query($query);

		$select="<option value=''>< Seleccionar ></option>";
		for($i = 0; $i < count($registros); $i++) {
			foreach ($registros[$i] as $key => $value) {
				$select .= "<option value='".$value['id']."'>".$value[$registrosrecurso[0]['registrosrecursos']['campo']]."</option>";
			}
		}
		echo  $select;
		$this->autoRender = false;
	}
}
