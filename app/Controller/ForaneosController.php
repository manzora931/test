<?php
App::uses('AppController', 'Controller');
/**
 * Foraneos Controller
 *
 * @property Foraneo $Foraneo
 * @property PaginatorComponent $Paginator
 */
class ForaneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
        $this->ValidarUsuario("Foraneo", "foraneos", "index");
        $this->Paginator->settings = array('order'=>array('Foraneo.jugadore_id'=>'asc'));
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        $this->Foraneo->recursive = 0;
        include "busqueda/foraneos.php";
        $data = $this->Paginator->paginate('Foraneo');
        $this->set('foraneos', $data);

        $this->loadModel("Jugadore");
        $jugadores = $this->Jugadore->find('list', [
            'fields' => [
                'Jugadore.id',
                'Jugadore.nombre_jugador'
            ]
        ]);

        $this->set(compact('jugadores'));

	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[foraneos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Foraneo", "foraneos", "view");
		if (!$this->Foraneo->exists($id)) {
			throw new NotFoundException(__('Invalid foraneo'));
		}
		$options = array('conditions' => array('Foraneo.' . $this->Foraneo->primaryKey => $id));
		$this->set('foraneo', $this->Foraneo->find('first', $options));
        $jugadores = $this->Foraneo->Jugadore->find('list',[
            'fields' => [
                'Jugadore.id',
                'Jugadore.nombre_jugador'
            ]
        ]);
        $this->set(compact("jugadores"));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Foraneo", "foraneos", "add");
		if ($this->request->is('post')) {
            $fecha = explode('-', $this->request->data['Foraneo']['fecha']);
            $this->request->data['Foraneo']['fecha'] = ($this->request->data['Foraneo']['fecha'] != '') ? $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0] : null;
            $this->request->data['Foraneo']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Foraneo']['modified']=0;
			$this->Foraneo->create();
			if ($this->Foraneo->save($this->request->data)) {
                $foraneo_id = $this->Foraneo->id;
                $this->Session->write('foraneo_save', 1);
                $this->redirect(['action' => 'view', $foraneo_id]);
			}
		}
		$jugadores = $this->Foraneo->Jugadore->find('list', [
            'fields' => [
                'Jugadore.id',
                'Jugadore.nombre_jugador'
            ]
        ]);
		$this->set(compact('jugadores'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Foraneo", "foraneos", "edit");
		if (!$this->Foraneo->exists($id)) {
			throw new NotFoundException(__('Invalid foraneo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $fecha = explode('-', $this->request->data['Foraneo']['fecha']);
		    $this->request->data['Foraneo']['fecha'] = ($this->request->data['Foraneo']['fecha'] != '') ? $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0] : null;
            $this->request->data['Foraneo']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Foraneo->save($this->request->data)) {
                $foraneo_id = $this->Foraneo->id;

                $this->Session->write('foraneo_save', 1);
                $this->redirect(['action' => 'view', $foraneo_id]);
			}
		} else {
			$options = array('conditions' => array('Foraneo.' . $this->Foraneo->primaryKey => $id));
			$this->request->data = $this->Foraneo->find('first', $options);
		}
		$jugadores = $this->Foraneo->Jugadore->find('list', [
            'fields' => [
                'Jugadore.id',
                'Jugadore.nombre_jugador'
            ]
        ]);
		$this->set(compact('jugadores'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Foraneo", "foraneos", "delete");
		if ($delete == true) {
			$this->Foraneo->id = $id;
			if (!$this->Foraneo->exists()) {
				throw new NotFoundException(__('Invalid foraneo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Foraneo->delete()) {
					return $this->flash(__('The foraneo has been deleted.'), array('action' => 'index'));
			} else {
				return $this->flash(__('The foraneo could not be deleted. Please, try again.'), array('action' => 'index'));
			}
			}
	}}
