<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Tarea $Tarea
 * @property PaginatorComponent $Paginator
 */
class TareasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tarea", "tareas", "index");
		$this->Paginator->settings = array('order'=>array('Tarea.nombre'=>'asc'));
		$this->Tarea->recursive = 0;
		include 'busqueda/tareas.php';
		$data = $this->Paginator->paginate('Tarea');
		$this->set('tareas', $data);

		$modules = $this->Tarea->Module->find('list');
		$intervenciones = $this->Tarea->Intervencione->find('list');
		$actividades = $this->Tarea->Actividade->find('list');
		$this->set(compact('modules', 'intervenciones', 'actividades'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[tareas]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tarea", "tareas", "view");
		if (!$this->Tarea->exists($id)) {
			throw new NotFoundException(__('Invalid tarea'));
		}
		$options = array('conditions' => array('Tarea.' . $this->Tarea->primaryKey => $id));
		$this->set('tarea', $this->Tarea->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tarea", "tareas", "add");
		if ($this->request->is('post')) {
			$this->request->data['Tarea']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Tarea']['modified']=0;
			$this->Tarea->create();

			if ($this->Tarea->save($this->request->data)) {
				$tarea_id = $this->Tarea->id;
				foreach($this->request->data["subtarea"] as $item){
					if($item["nombre"]!=''){
						$data['tarea_id'] = $tarea_id;
						$data['nombre'] = $item["nombre"];
						$data['descripcion'] = $item["desc"];
						$data['usuario'] = $this->Session->read("nombreusuario");
						$data['created'] = date("Y-m-d H:i:s");
						$this->loadModel("Subtarea");
						$this->Subtarea->create();
						if($this->Subtarea->save($data)){

						}
					}
				}
				$this->Session->write('tarea_save', 1);
				$this->redirect(['action' => 'view', $tarea_id]);
			} else {
				$this->Session->setFlash(__('La tarea no ha sido creado. Intentar de nuevo.'));
			}
		}
		$modules = $this->Tarea->Module->find('list');
		$intervenciones = $this->Tarea->Intervencione->find('list');
		$actividades = $this->Tarea->Actividade->find('list');
		$this->set(compact('modules', 'intervenciones', 'actividades'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tarea", "tareas", "edit");
		if (!$this->Tarea->exists($id)) {
			throw new NotFoundException(__('Invalid tarea'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Tarea']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Tarea']['modified'] = date("Y-m-d H:i:s");
			if ($this->Tarea->save($this->request->data)) {
				$tarea_id = $this->Tarea->id;
				$bandera=0;
				if(isset($this->request->data['subtarea'])){
					$subt = $this->Tarea->Subtarea->find("all",[
						'conditions'=>['Subtarea.tarea_id'=>$id],
						'field'=>['Subtarea.id']
					]);
					foreach($subt as $log){
						foreach($this->request->data["subtarea"] as $itemd){
							if(isset($itemd['id']) && $log['Subtarea']['id'] === $itemd['id']){
								$bandera=1;
							}
						}
						if($bandera==0){
							$this->Tarea->Subtarea->delete($log['Subtarea']['id']);
						}
						$bandera=0;
					}
					foreach($this->request->data["subtarea"] as $item){
						if($item["nombre"]!=''){
							$data=[];
							if(isset($item["id"])){
								$data['id'] = $item["id"];
								$data['tarea_id'] = $tarea_id;
								$data['nombre'] = $item["nombre"];
								$data['descripcion'] = $item["desc"];
								$data['usuariomodif'] = $this->Session->read("nombreusuario");
								$data['modified'] = date("Y-m-d H:i:s");
								$this->loadModel("Subtarea");
								if($this->Subtarea->save($data)){

								}
							}else{
								$data['tarea_id'] = $tarea_id;
								$data['nombre'] = $item["nombre"];
								$data['descripcion'] = $item["desc"];
								$data['usuario'] = $this->Session->read("nombreusuario");
								$data['created'] = date("Y-m-d H:i:s");
								$this->loadModel("Subtarea");
								$this->Subtarea->create();
								if($this->Subtarea->save($data)){

								}
							}
						}
					}
				}else{
					/**ELIMINA TODAS LAS SUBTAREAS**/
					$subt = $this->Tarea->Subtarea->find("all",[
						'conditions'=>['Subtarea.tarea_id'=>$id],
						'field'=>['Subtarea.id']
					]);
					foreach($subt as $log){
						$this->Tarea->Subtarea->delete($log['Subtarea']['id']);
					}
				}

				$this->Session->write('tarea_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('La tarea no se ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Tarea.' . $this->Tarea->primaryKey => $id));
			$this->request->data = $this->Tarea->find('first', $options);
		}
		$modules = $this->Tarea->Module->find('list');
		$this->set(compact('modules', 'intervenciones', 'actividades'));
	}

	/*retorna intervencion*/
	public function loadInt(){
	    $this->loadModel("Intervencione");
	    $info=$this->Intervencione->find('all',
            [
                'conditions'=>['Intervencione.module_id'=>$_POST["id"]],
                'field'=>["Intervencione.id","Intervencione.nombre"],
                'order'=>["Intervencione.nombre"=>"ASC"]
            ]);
	    $html = "<option>Seleccionar</option>";
	    foreach ($info as $item){
			if($_POST["value"]==$item['Intervencione']['id']){
				$html .= "<option selected value='".$item['Intervencione']['id']."'>".$item['Intervencione']['nombre']."</option>";
			}else{
				$html .= "<option value='".$item['Intervencione']['id']."'>".$item['Intervencione']['nombre']."</option>";
			}

        }
        echo ($html);
        $this->autoRender=false;
    }
	public function loadAc(){
		$this->loadModel("Actividade");
		$info = $this->Actividade->find("all",[
			'conditions'=>['Actividade.intervencione_id'=>$_POST['id']],
			'field'=>['Actividade.id','Actividade.nombre'],
			'order'=>['Actividade.nombre'=>'ASC']
		]);
		$html = "<option>Seleccionar</option>";
		foreach($info as $item){
			if($_POST["value"]==$item['Actividade']['id']){
				$html .= "<option selected value='".$item["Actividade"]["id"]."'>".$item["Actividade"]["nombre"]."</option>";
			}else{
				$html .= "<option value='".$item["Actividade"]["id"]."'>".$item["Actividade"]["nombre"]."</option>";
			}
		}
		echo $html;
		$this->autoRender=false;
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tarea", "tareas", "delete");
		if ($delete == true) {
			$this->Tarea->id = $id;
			if (!$this->Tarea->exists()) {
				throw new NotFoundException(__('Invalid tarea'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Tarea->delete()) {
				$this->Session->setFlash(__('The tarea has been deleted.'));
			} else {
				$this->Session->setFlash(__('The tarea could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}
}
