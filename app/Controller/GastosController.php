<?php
App::uses('AppController', 'Controller');
/**
 * Gastos Controller
 *
 * @property Gasto $Gasto
 * @property PaginatorComponent $Paginator
 */
class GastosController extends AppController {

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array('Html','Js', 'Form');

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index($idProyecto = null) {
        $this->layout=false;
        $data = [];
        $modules=[];
        $intervenciones=[];

        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

        if(!is_null($idProyecto)) {
            /************************* MODULOS ****************************/
            $this->loadModel("Module");
            $this->Module->recursive = 1;
            $modules = $this->Module->find("all",[
                'conditions'=>['Module.proyecto_id'=>$idProyecto],
                'order'=>['Module.orden'=>"ASC"]
            ]);

            foreach ($modules as $row) {
                /*** ALMACENA LAS INTERNVENCIONES DE CADA MODULO ***/
                $intervenciones[$row['Module']['id']] = $row['Intervencione'];
            }

            /*** FOREACH PARA RECORRER INTERVENCIONES ***/
            foreach ($intervenciones as $rw) {
                foreach ($rw as $item){
                    /*** GASTOS DE ACTIVIDADES POR INTERVENCIONES ***/
                    $gastos = $this->Gasto->find("all",[
                        'conditions'=>['Actividade.intervencione_id' => $item['id']],
                        'order' => ['Actividade.nombre' => 'asc', 'Gasto.fecha' => 'asc']
                    ]);
                    $data[$item['id']] = $gastos;
                }
            }
        }

        $this->set('gastos', $data);

        $actividades = $this->Gasto->Actividade->find("list", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'Actividade.finalizado' => 0, 'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $this->loadModel('Catgasto');
        $catgastos = $this->Catgasto->find("list", [
            'fields' => ['Catgasto.id', 'Catgasto.nombre'],
            'conditions'=> ['Catgasto.activo' => 1]
        ]);

        $meses = [1=>"Ene", 2=>"Feb", 3=>"Mar", 4=>"Abr", 5=>"May", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Sep", 10=>"Oct", 11=>"Nov", 12=>"Dic"];
        /***-----------------------**/
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive=-1;
        $infoP = $this->Proyecto->read("estado_id",$idProyecto);
        $this->set(compact( 'idProyecto', 'modules', 'intervenciones', 'actividades', 'catgastos', 'meses','infoP'));
    }

    public function delete() {
        $this->Gasto->id = $_POST['id'];
        $proyectoid = $_POST['proyectoid'];

        if (!$this->Gasto->exists()) {
            $resp['msg'] = 'error1';
        }

        $gasto = $this->Gasto->find('first', ['conditions' => ['Gasto.id' => $this->Gasto->id]]);
        if(!$gasto['Actividade']['finalizado']) {
            if ($this->Gasto->delete()) {
                $resp['msg'] = 'exito';

                $_SESSION['logsproyecto']['accion'] = 'Eliminar';
                $_SESSION['logsproyecto']['data'] = $gasto['Gasto'];
                $_SESSION['logsproyecto']['data']['ncatgasto'] = $gasto['Catgasto']['nombre'];
                $_SESSION['logsproyecto']['data']['nactividad'] = $gasto['Actividade']['nombre'];

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Gasto', 'gastos', 'Gastos');
            } else {
                $resp['msg'] = 'error2';
            }
        } else {
            $resp['msg'] = 'error3';
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function get_data() {
        $id = $_POST['id'];
        $idProyecto = $_POST['proyectoid'];

        $sql = "SELECT catgasto_id, actividade_id, fecha, monto, comprobante, descripcion 
				FROM gastos WHERE id = " . $id;
        $gasto['data'] = $this->Gasto->query($sql);

        $this->Gasto->Actividade->recursive = -1;
        $gasto['actividades'] = $this->Gasto->Actividade->find("all", [
            'fields' => ['Actividade.id', 'Actividade.nombre'],
            'joins'=>[
                [
                    'table'=>'intervenciones', 'alias'=>'Intervencione', 'type'=>'INNER',
                    'conditions'=>[ 'Intervencione.id = Actividade.intervencione_id' ]
                ],
                [
                    'table'=>'modules', 'alias'=>'Module', 'type'=>'INNER',
                    'conditions'=>[ 'Module.id = Intervencione.module_id' ]
                ]
            ],
            'conditions'=> [
                'OR' => [
                    'Actividade.finalizado' => 0,
                    'Actividade.id' => $gasto['data'][0]['gastos']['actividade_id'],
                ],
                'Module.proyecto_id'=>$idProyecto
            ]
        ]);

        $gasto['msg'] = 'exito';
        echo json_encode($gasto);
        $this->autoRender=false;
    }

    public function almacenar_data() {
        $id = $_POST['id'];
        $proyectoid = $_POST['proyectoid'];
        $actividadid = $_POST['actividad'];
        $fecha = explode('-', $_POST['fecha']);
        $resp = [];
        $resp['msg'] = '';
        $totalgastos = 0;
        $gasto = array();
        $monto = (float)$_POST['monto'];
        $adjunto = '';

        $actividad = $this->Gasto->Actividade->find("first", [
            'fields' => ['Actividade.fuentesfinanciamiento_id', 'Actividade.finalizado', 'Fuentesfinanciamiento.monto'],
            'conditions'=> ['Actividade.id' => $actividadid]
        ]);
        $ffinan_id =$actividad["Actividade"]["fuentesfinanciamiento_id"];
        $totalgasto = $this->Gasto->find('all', [
            'fields' => ['sum(Gasto.monto) as montototal'],
            'conditions' => ['Actividade.id' => $actividad['Actividade']['id']],
        ]);

        $this->loadModel('Presupuesto');
        $sqlasig = $this->Presupuesto->find('all', [
            'fields' => ['Presupuesto.totalasignado'],
            'conditions' => ['Presupuesto.actividade_id' => $actividad['Actividade']['id']],
        ]);
        /**PRESUPUESTO DE LA ACTIVIDAD**/
        $presup = (isset($sqlasig[0]['Presupuesto']['totalasignado']))?$sqlasig[0]['Presupuesto']['totalasignado']:0.00;

        if($id != '') {
            $gasto = $this->Gasto->find('first', [
                'fields' => ['Gasto.*'],
                'conditions' => ['Gasto.id' => $id]
            ]);

            $totalgastos = (((float)$totalgasto[0][0]['montototal'] - (float)$gasto['Gasto']['monto']) + $monto);
        } else {
            $totalgastos = (float)$totalgasto[0][0]['montototal'] + $monto;
        }

        $restofinanciamiento = $presup - $totalgastos;
        if($monto > 0) {
            if($restofinanciamiento >= 0) {
                /*DATOS DEL ARCHIVO ADJUNTO**/
                if(isset($_FILES['archivo'])){
                    $type = $_FILES['archivo']['type'];
                    $size = $_FILES['archivo']['size'];
                    $name = $_FILES['archivo']['name'];
                    $tmpName = $_FILES['archivo']['tmp_name'];
                    $ext = pathinfo($name);
                    $maxsize = 10485760;

                    /**SOLO SE PUEDEN ADJUNTAR PDF e Imagenes **/
                    if( $ext['extension']=="pdf" || $ext['extension']=="PDF" ) {
                        /**FUNCIONES PARA LEER EL ARCHIVO**/
                        $file = new File($_FILES['archivo']['tmp_name']);
                        $path_parts = pathinfo($_FILES['archivo']['name']);
                        if (isset($path_parts['extension'])) {
                            $filename = $_FILES['archivo']['name'];
                            $adjunto = $_FILES['archivo']['name'];
                            $data = $file->read();
                            $file->close();

                            $file = new File(WWW_ROOT . '/files/adjuntos/' . $filename, true);
                            $file->write($data);
                            $file->close();
                        }
                    } else {
                        /**SI EL ARCHIVO QUE SE INTENTA ADJUNTAR ES DE OTRA EXTENCION NO PUEDE ALMACENARSE**/
                        $resp['msg'] = 'error6';
                    }
                }

                if($id != '' && $resp['msg'] != 'error6') {
                    $gasto = $this->Gasto->find('first', [
                        'fields' => ['Gasto.*'],
                        'conditions' => ['Gasto.id' => $id]
                    ]);

                    //Se almacena la informacion del gasto antes de actualizar
                    $loganterior = $gasto;
                    if(count($gasto) > 0 && !$actividad['Actividade']['finalizado']) {
                        $gasto['Gasto']['catgasto_id'] = $_POST['catgasto'];
                        $gasto['Gasto']['actividade_id'] = $actividadid;
                        $gasto['Gasto']['descripcion'] = $_POST['descripcion'];
                        $gasto['Gasto']['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                        $gasto['Gasto']['monto'] = $monto;
                        if($adjunto!=''){
                            $gasto['Gasto']['comprobante'] = $adjunto;
                        }
                        $gasto['Gasto']['usuariomodif'] = $this->Session->read('nombreusuario');
                         $gasto['Gasto']['modified'] = date('Y-m-d H:i:s');
                        if ($this->Gasto->save($gasto)) {
                             $_SESSION['logsproyecto']['accion'] = 'Modificar';
                             $_SESSION['logsproyecto']['anterior'] = $loganterior;
                             $_SESSION['logsproyecto']['data'] = $gasto['Gasto'];
                             $_SESSION['logsproyecto']['data']['ncatgasto'] = $_POST['ncatgasto'];
                             $_SESSION['logsproyecto']['data']['nactividad'] = $_POST['nactividad'];
                             $this->Session->write("savegasto",1);
                             $resp['id'] = $id;
                             $resp['msg'] = 'exito';

                            $this->loadModel("Logproyecto");
                            $this->Logproyecto->registro_bitacora('Gasto', 'gastos', 'Gastos');
                        } else {
                            $resp['msg'] = 'error1';
                        }
                    } elseif($actividad['Actividade']['finalizado']) {
                        $resp['msg'] = 'error7';
                    }
                } elseif($resp['msg'] != 'error6') {
                    $data=[];
                    $data['catgasto_id'] = $_POST['catgasto'];
                    $data['actividade_id'] = $actividadid;
                    $data['descripcion'] = $_POST['descripcion'];
                    $data['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                    $data['monto'] = $_POST['monto'];
                    $data['comprobante'] = $adjunto;
                    $data['usuario'] = $this->Session->read('nombreusuario');
                    $data['created'] = date('Y-m-d H:i:s');
                    $data['modified'] = 0;

                    $this->Gasto->create();
                    if ($this->Gasto->save($data)) {
                        $data['id'] = $this->Gasto ->id;
                        $_SESSION['logsproyecto']['accion'] = 'Agregar';
                        $_SESSION['logsproyecto']['data'] = $data;
                        $_SESSION['logsproyecto']['data']['ncatgasto'] = $_POST['ncatgasto'];
                        $_SESSION['logsproyecto']['data']['nactividad'] = $_POST['nactividad'];
                        $this->Session->write("savegasto",1);
                        $resp['id'] = $this->Gasto ->id;
                        $resp['msg'] = 'exito';

                        $this->loadModel("Logproyecto");
                        $this->Logproyecto->registro_bitacora('Gasto', 'gastos', 'Gastos');
                    } else {
                        $resp['msg'] = 'error2';
                    }
                }
            }  elseif($monto > $presup) {
                $resp['msg'] = 'error3';
            } elseif($totalgastos > $presup) {
                $resp['msg'] = 'error4';
            }/* elseif($totalgastos > $do) {
                $resp['msg'] = 'error5';
            }*/
        } else {
            $resp['msg'] = 'error1';
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }
}
