<?php
App::uses('AppController', 'Controller');
/**
 * Presupuestos Controller
 *
 * @property Presupuesto $Presupuesto
 * @property PaginatorComponent $Paginator
 */
class PresupuestosController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($idProyecto) {
	    $this->layout=false;

        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

        $this->loadModel("Module");
        $this->Module->recursive = 1;
        $modules = $this->Module->find("all", [
            'conditions' => [
                'Module.proyecto_id' => $idProyecto
            ],
            'order' => [
                'Module.orden' => "ASC"
            ]
        ]);

        $intervenciones = [];
        foreach ($modules as $row) {
            /**ALMACENA LAS INTERNVENCIONES DE CADA MODULO*/
            $intervenciones[$row['Module']['id']] = $row['Intervencione'];
            $presupuestosmodulo[$row['Module']['id']] = 0;
        }

        $presupuestos = [];
        /***EL PRIMER FOREACH RECORRE LAS INTERVENCIONES DE CADA MODULO**/
        foreach ($intervenciones as $int) {
            /***EL SEGUNDO FOREACH RECORRE CADA INTERVENCION**/
            foreach ($int as $rw) {
                $info = $this->Presupuesto->find("all",
                    [
                        'joins' => [
                            [
                                'table' => 'actividades',
                                'alias' => 'Actividade',
                                'type' => 'INNER',
                                'conditions' => [
                                    'Presupuesto.actividade_id = Actividade.id'
                                ]
                            ],
                            [
                                'table' => 'intervenciones',
                                'alias' => 'Intervencione',
                                'type' => 'INNER',
                                'conditions' => [
                                    'Intervencione.id = Actividade.intervencione_id'
                                ]
                            ]
                        ],
                        'conditions' => [
                            'Actividade.intervencione_id' => $rw['id']
                        ],
                        'fields' => [
                            'Presupuesto.id', 'Presupuesto.actividade_id', 'Presupuesto.totalasignado', 'Actividade.nombre', 'Actividade.intervencione_id', 'Actividade.fuentesfinanciamiento_id'
                        ]
                    ]
                );

                $presupuestosintervencion[$rw['id']] = count($info);
                $presupuestosmodulo[$rw['module_id']] += $presupuestosintervencion[$rw['id']];

                /****ACTIVIDADES CON PRESUPUESTO****/
                $desembolsado = 0.00;
                $ejecutado = 0.00;
                $cont = 0;
                foreach ($info as $det) {
                    $desembolsado = $this->desembolsado($det['Actividade']['fuentesfinanciamiento_id']);
                    $ejecutado = $this->ejutado($det['Presupuesto']['actividade_id']);
                    $info[$cont]['Desembolso']['monto'] = $desembolsado;
                    $info[$cont]['Desembolso']['ejecutado'] = $ejecutado;
                    $info[$cont]['Desembolso']['disponible'] = $det['Presupuesto']['totalasignado'] - $ejecutado;
                    $cont++;
                }
                $presupuestos[$rw['id']] = $info;
            }
        }
        /**INFO DE PROIYECTO**/
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive = -1;
        $infoP = $this->Proyecto->read("estado_id", $idProyecto);
        $admin = $this->get_perfil();
        $this->set(compact("idProyecto", "modules", "presupuestos", 'intervenciones', 'infoP', 'presupuestosmodulo', 'presupuestosintervencion', 'admin'));
    }

    public function get_perfil()
    {
        $this->loadModel("Perfile");
        $this->Perfile->recursive = -1;
        $user = $_SESSION["nombreusuario"];
        $info = $this->Perfile->find("all", [
            'joins' => [
                [
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => [
                        "Perfile.id=User.perfile_id"
                    ]
                ]
            ],
            'conditions' => [
                'User.username' => $user
            ],
            'fields' => ['Perfile.admin']
        ]);
        $band = ($info[0]['Perfile']['admin'] == 1) ? 1 : 0;
        return $band;
    }

    public function reasignar($idProyecto = null)
    {
        $this->layout = false;
        $this->loadModel("Actividade");
        $this->Actividade->recursive = -1;
        $sqlAct = $this->Actividade->find('all', [
            'joins' => [
                [
                    'table' => 'intervenciones',
                    'alias' => 'Intervencione',
                    'type' => 'INNER',
                    'conditions' => [
                        'Intervencione.id = Actividade.intervencione_id'
                    ]
                ],
                [
                    'table' => 'modules',
                    'alias' => 'Module',
                    'type' => 'INNER',
                    'conditions' => [
                        'Module.id = Intervencione.module_id'
                    ]
                ],
                [
                    'table' => 'presupuestos',
                    'alias' => 'Presupuesto',
                    'type' => 'INNER',
                    'conditions' => [
                        'Presupuesto.actividade_id = Actividade.id'
                    ]
                ]

            ],
            'conditions' => [
                'Module.proyecto_id' => $idProyecto
            ],
            'fields' => [
                'Actividade.id', 'Actividade.nombre', 'Presupuesto.actividade_id', 'Presupuesto.totalasignado'
            ]
        ]);
        $actividades = [];
        foreach ($sqlAct as $item) {
            $ejecu = $this->ejutado($item['Actividade']['id']);
            if($item['Presupuesto']['totalasignado']>$ejecu) {
                $actividades[$item['Actividade']['id']]=$item['Actividade']['nombre'];
            }
        }
        $this->set(compact("actividades"));
    }

    public function transferir(){
        $origen = $_POST['activ_origen'];
        $destino = $_POST['activ_dest'];
        $monto = $_POST['monto'];
        $proyecto = $_POST['proy'];
        $ip  =  (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']))?$_SERVER['REMOTE_ADDR']:'';
        $user = $this->Session->read("nombreusuario");
        $date = date("Y-m-d H:i:s");
        /**ACTUALIZA EL PRESUPUESTO DE LA ACTIVIDAD ORIGEN**/
        $sqlAsig = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$origen
            ],
            'fields'=>[
                'Presupuesto.totalasignado',
                'Presupuesto.id'
            ]
        ]);
        $asig = (isset($sqlAsig[0]['Presupuesto']['totalasignado']))?$sqlAsig[0]['Presupuesto']['totalasignado']:0.00;
        $idpre1 = (isset($sqlAsig[0]['Presupuesto']['id']))?$sqlAsig[0]['Presupuesto']['id']:0;
        $newPres = $asig - $monto;
        $data = ["id"=>$idpre1,"actividade_id"=>$origen,"totalasignado"=>$newPres,"usuariomodif"=>$user,"modified"=>$date];
        $this->Presupuesto->save($data);
        /****ACTUALIZACION DEL PRESUPUESTO AL CUAL SE TRASFIEREN FONDOS******/
        $sqlAsig = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$destino
            ],
            'fields'=>[
                'Presupuesto.totalasignado',
                'Presupuesto.id'
            ]
        ]);
        $asig = (isset($sqlAsig[0]['Presupuesto']['totalasignado']))?$sqlAsig[0]['Presupuesto']['totalasignado']:0.00;
        $idpre2=(isset($sqlAsig[0]['Presupuesto']['id']))?$sqlAsig[0]['Presupuesto']['id']:0;
        $newPres = $asig + $monto;
        $data = ["id"=>$idpre2,"actividade_id"=>$destino,"totalasignado"=>$newPres,"usuariomodif"=>$user,"modified"=>$date];
        $this->Presupuesto->save($data);
        /**REGISTRO DEL TRASLADO DE FONDO EN LA BITACORA***/
        $this->loadModel("Trasladofondo");
        $data = ["actividade1_id"=>$origen,"actividade2_id"=>$destino,"proyecto_id"=>$proyecto,"monto"=>$monto,'ip'=>$ip,'usuario'=>$user,"created"=>$date];
        $this->Trasladofondo->save($data);
        $this->Session->write("savetransfe",1);
        echo 1;
        $this->autoRender=false;
    }


    /*******/
    public function presuDisp($id=null){
        $ejecutado = $this->ejutado($id);
        $info = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$id
            ],
            'fields'=>[
                'Presupuesto.id','Presupuesto.actividade_id','Presupuesto.totalasignado'
            ]
        ]);
        $asignado = (isset($info[0]['Presupuesto']['totalasignado']))?$info[0]['Presupuesto']['totalasignado']:0.00;
        $disponible= $asignado - $ejecutado;
        $monto = ($disponible>0)?$disponible:0.00;
        $datos = $monto;
        return $datos;
    }
    public function getDisponible(){
        $ejecutado = $this->ejutado($_POST['actividad_id']);
        $info = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$_POST['actividad_id']
            ],
            'fields'=>[
                'Presupuesto.id','Presupuesto.actividade_id','Presupuesto.totalasignado'
            ]
        ]);

        $asignado = (isset($info[0]['Presupuesto']['totalasignado']))?$info[0]['Presupuesto']['totalasignado']:0.00;
        $disponible= $asignado - $ejecutado;
        $monto = ($disponible>0)?$disponible:0.00;
        /***FUENTE DE LA ACTIVIDAD***/
        $this->loadModel("Actividade");
        $this->Actividade->recursive=-1;
        $fuente = $this->Actividade->read("fuentesfinanciamiento_id",$_POST['actividad_id']);
        $datos = ['fuente'=>$fuente['Actividade']['fuentesfinanciamiento_id'],"monto"=>$monto];
        echo json_encode($datos);
        $this->autoRender=false;
    }

    public function valMontoTranf(){
        $monto = $_POST['monto'];
        $ejecutado = $this->ejutado($_POST['actividad']);
        $info = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$_POST['actividad']
            ],
            'fields'=>[
                'Presupuesto.id','Presupuesto.actividade_id','Presupuesto.totalasignado'
            ]
        ]);

        $asignado = (isset($info[0]['Presupuesto']['totalasignado']))?$info[0]['Presupuesto']['totalasignado']:0.00;
        $disponible= $asignado - $ejecutado;
        if($monto > $disponible){
            echo json_encode(['disponible'=>$disponible,'msj'=>"No puede reasignar un fondo mayor al presupuesto disponible"]);
        }else{
            echo json_encode(['disponible'=>$disponible,'msj'=>"exito"]);
        }
        $this->autoRender=false;
    }

    public function getActiv(){
        $fuente = $_POST['id'];
        $seleccionada = $_POST['actividad'];
        $this->loadModel("Actividade");
        $this->Actividade->recursive=-1;
        $info = $this->Actividade->find("all",[
            'joins'=>[
                [
                    'table'=>'presupuestos',
                    'alias'=>'Presupuesto',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Presupuesto.actividade_id = Actividade.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Actividade.fuentesfinanciamiento_id'=>$fuente,
                'Actividade.id != '=>$seleccionada
            ],
            'fields'=>[
                'Actividade.id','Actividade.nombre'
            ]
        ]);

        echo json_encode($info);
        $this->autoRender=false;
    }

    public function ejutado($id = null)
    {
        $this->loadModel("Gasto");
        $this->Gasto->recursive = -1;
        $query = $this->Gasto->find("all", [
            'conditions' => [
                'Gasto.actividade_id' => $id
            ],
            'fields' => [
                'sum(monto) as gasto'
            ]
        ]);
        $monto = (isset($query[0][0]['gasto'])) ? $query[0][0]['gasto'] : 0.00;
        return $monto;
    }

    public function desembolsado($id = null)
    {
        $this->loadModel("Desembolso");
        $this->Desembolso->recursive = -1;
        /**SUMA TODOS LOS DESEMBOLSOS DE LA FUENTE DE FINANCIAMIENTO QUE TIENE LA ACTIVIDAD**/
        $query = $this->Desembolso->find("all", [
            'conditions' => [
                'Desembolso.fuentefinanciamiento_id' => $id
            ],
            'fields' => [
                'sum(total) as total'
            ]
        ]);
        $monto = (isset($query[0][0]['total'])) ? $query[0][0]['total'] : 0.00;
        return $monto;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        $this->ValidarUsuario("Presupuesto", "presupuestos", "view");
        if (!$this->Presupuesto->exists($id)) {
            throw new NotFoundException(__('Invalid presupuesto'));
        }
        $options = array('conditions' => array('Presupuesto.' . $this->Presupuesto->primaryKey => $id));
        $this->set('presupuesto', $this->Presupuesto->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($idProyecto = null)
    {
        $this->layout = false;
        $this->Presupuesto->recursive = -1;
        $ocupados = $this->Presupuesto->find("all", [
            'joins' => [
                [
                    'table' => 'actividades',
                    'alias' => 'Actividade',
                    'type' => 'INNER',
                    'conditions' => [
                        'Presupuesto.actividade_id = Actividade.id'
                    ]
                ],
                [
                    'table' => 'intervenciones',
                    'alias' => 'Intervencione',
                    'type' => 'INNER',
                    'conditions' => [
                        'Intervencione.id = Actividade.intervencione_id'
                    ]
                ],
                [
                    'table' => 'modules',
                    'alias' => 'Module',
                    'type' => 'INNER',
                    'conditions' => [
                        'Module.id = Intervencione.module_id'
                    ]
                ]
            ],
            'conditions' => [
                'Module.proyecto_id' => $idProyecto
            ],
            'fields' => [
                'Presupuesto.actividade_id'
            ]
        ]);
        $desabilitados = [];
        foreach ($ocupados as $disabled) {
            $desabilitados[] = $disabled['Presupuesto']['actividade_id'];
        }
        if (count($desabilitados) > 0) {
            $this->loadModel("Actividade");
            $this->Actividade->recursive = -1;
            if (count($desabilitados) == 1) {
                $actividades = $this->Actividade->find('list', [
                    'joins' => [
                        [
                            'table' => 'intervenciones',
                            'alias' => 'Intervencione',
                            'type' => 'INNER',
                            'conditions' => [
                                'Intervencione.id = Actividade.intervencione_id'
                            ]
                        ],
                        [
                            'table' => 'modules',
                            'alias' => 'Module',
                            'type' => 'INNER',
                            'conditions' => [
                                'Module.id = Intervencione.module_id'
                            ]
                        ]
                    ],
                    'conditions' => [
                        'Module.proyecto_id' => $idProyecto,
                        'Actividade.id != ' => $desabilitados[0]

                    ]
                ]);
            } else {
                $actividades = $this->Actividade->find('list', [
                    'joins' => [
                        [
                            'table' => 'intervenciones',
                            'alias' => 'Intervencione',
                            'type' => 'INNER',
                            'conditions' => [
                                'Intervencione.id = Actividade.intervencione_id'
                            ]
                        ],
                        [
                            'table' => 'modules',
                            'alias' => 'Module',
                            'type' => 'INNER',
                            'conditions' => [
                                'Module.id = Intervencione.module_id'
                            ]
                        ]
                    ],
                    'conditions' => [
                        'Module.proyecto_id' => $idProyecto,
                        'Actividade.id NOT IN ' => $desabilitados

                    ]
                ]);
            }

        } else {
            $this->loadModel("Actividade");
            $this->Actividade->recursive = -1;
            $actividades = $this->Actividade->find('list', [
                'joins' => [
                    [
                        'table' => 'intervenciones',
                        'alias' => 'Intervencione',
                        'type' => 'INNER',
                        'conditions' => [
                            'Intervencione.id = Actividade.intervencione_id'
                        ]
                    ],
                    [
                        'table' => 'modules',
                        'alias' => 'Module',
                        'type' => 'INNER',
                        'conditions' => [
                            'Module.id = Intervencione.module_id'
                        ]
                    ]
                ],
                'conditions' => [
                    'Module.proyecto_id' => $idProyecto

                ]
            ]);

        }

        $this->set(compact('actividades'));
    }

    public function savepresupuesto(){
	    $user = $this->Session->read("nombreusuario");
	    if($_POST['id']>0){
            $options = array('conditions' => array('Presupuesto.' . $this->Presupuesto->primaryKey => $_POST['id']));
            $_SESSION['logsproyecto']['anterior'] = $this->Presupuesto->find('first', $options);

            $data=['id'=>$_POST['id'],"actividade_id"=>$_POST['actividad'],"totalasignado"=>$_POST['monto'],"usuariomodif"=>$user,'modified'=>date("Y-m-d H:i:s")];
            $accion = 'Modificar';
        }else{
            $data=["actividade_id"=>$_POST['actividad'],"totalasignado"=>$_POST['monto'],"usuario"=>$user,'created'=>date("Y-m-d H:i:s")];
            $accion = 'Agregar';
        }
        /**---------**/
        $disponible = $this->valMonto($_POST['actividad'], $_POST['monto']);
        if ($disponible > 0) {
            /***LA FUENTE DE FINANCIAMIENTO NO PUEDE CUBRIR EL MONTO DEL PRESUPUESTO**/
            echo 2;
        }else{
            $band="error";
            if ($this->Presupuesto->save($data)){
                $data['id'] = $this->Presupuesto->id;
                $data['actividad'] = $_POST['nactividad'];
                $_SESSION['logsproyecto']['accion'] = $accion;
                $_SESSION['logsproyecto']['data'] = $data;
                $this->Session->write("savepresu",1);
                $band="exito";

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Presupuesto', 'presupuestos', 'Presupuestos');
            }
            echo $band;
        }
        $this->autoRender = false;
    }

    /***METODO PARA VALIDAR EL MONTO DEL PRESUPUESTO***/
    public function valMonto($id = null, $monto = null)
    {
        $this->loadModel("Actividade");
        $this->Actividade->recursive = -1;
        $info = $this->Actividade->find("all", [
            'conditions' => [
                'Actividade.id' => $id
            ],
            'fields' => [
                'Actividade.fuentesfinanciamiento_id'
            ]
        ]);
        $fuente_id = $info[0]['Actividade']['fuentesfinanciamiento_id'];
        $this->Presupuesto->recursive = -1;
        $sql = $this->Presupuesto->find("all", [
            'joins' => [
                [
                    'table' => 'actividades',
                    'alias' => 'Actividade',
                    'type' => 'INNER',
                    'conditions' => [
                        'Actividade.id = Presupuesto.actividade_id'
                    ]
                ],
                [
                    'table' => 'fuentesfinanciamientos',
                    'alias' => 'Fuentesfinanciamiento',
                    'type' => 'INNER',
                    'conditions' => [
                        'Actividade.fuentesfinanciamiento_id = Fuentesfinanciamiento.id'
                    ]
                ]
            ],
            'conditions' => [
                'Fuentesfinanciamiento.id' => $fuente_id
            ],
            'fields' => [
                'SUM(Presupuesto.totalasignado) as total'
            ]
        ]);
        $total_presupuesto = (isset($sql[0][0]['total'])) ? $sql[0][0]['total'] : 0.00;//SUMATORIA DE TODOS LOS PRESUPUESTOS DE LA MISMA F. FINANCIAMIENTO
        /***TOTAL DE LA FUENTE DE FINANCIAMIENTO***/

        $this->loadModel("Fuentesfinanciamiento");
        $this->Fuentesfinanciamiento->recursive = -1;
        $infoF = $this->Fuentesfinanciamiento->find("all", [
            'conditions' => [
                'Fuentesfinanciamiento.id' => $fuente_id
            ],
            'fields' => [
                'Fuentesfinanciamiento.monto'
            ]
        ]);
        $montoF = (isset($infoF[0]['Fuentesfinanciamiento']['monto'])) ? $infoF[0]['Fuentesfinanciamiento']['monto'] : 0.00;
        $disponible = $montoF - $total_presupuesto;
        $bandera = 0;
        if ($monto > $disponible) {
            $bandera = 1;
        }
        return $bandera;
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($info = null)
    {
        $this->layout = false;
        $ids = explode("-", $info);
        $id = $ids[0];
        $idProyecto = $ids[1];
        $options = array('conditions' => array('Presupuesto.' . $this->Presupuesto->primaryKey => $id));
        $this->request->data = $this->Presupuesto->find('first', $options);
        $this->loadModel("Actividade");
        $this->Actividade->recursive = -1;
        $actividades = $this->Actividade->find('list', [
            'joins' => [
                [
                    'table' => 'intervenciones',
                    'alias' => 'Intervencione',
                    'type' => 'INNER',
                    'conditions' => [
                        'Intervencione.id = Actividade.intervencione_id'
                    ]
                ],
                [
                    'table' => 'modules',
                    'alias' => 'Module',
                    'type' => 'INNER',
                    'conditions' => [
                        'Module.id = Intervencione.module_id'
                    ]
                ]
            ],
            'conditions' => [
                'Module.proyecto_id' => $idProyecto
            ]
        ]);

        $this->set(compact('actividades', 'idProyecto', 'id'));
    }

    public function val()
    {
        $presupuesto = $_POST['id'];
        $proyecto = $_POST['proyecto'];
        $actividad = $_POST["actividad"];
        $info = $this->Presupuesto->find("all",
            [
                'conditions' => [
                    'Presupuesto.actividade_id' => $actividad,
                    'Presupuesto.id !=' => $presupuesto
                ],
                'fields' => [
                    'Presupuesto.id',
                    'Presupuesto.actividade_id'
                ]
            ]);

        $band = "exito";
        if (count($info) > 0) {
            $band = "error";
        }
        echo $band;
        $this->autoRender = false;
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete()
    {
        $id = $_POST['idpres'];
       // $delete = $this->ValidarUsuario("Presupuesto", "presupuestos", "delete");
      // if ($delete == true) {
            $this->Presupuesto->id = $id;
            $band = 0;
            if ($this->Presupuesto->delete()) {
                $this->Session->write("presDelete",1);
                $band=1;
            }
        echo $band;
        $this->autoRender=false;
            //return $this->redirect(array('action' => 'index'));
    }
    //}
}
