<?php
App::uses('AppController', 'Controller');
/**
 * Tipoinstitucions Controller
 *
 * @property Tipoinstitucion $Tipoinstitucion
 * @property PaginatorComponent $Paginator
 */
class TipoinstitucionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tipoinstitucion", "tipoinstitucions", "index");
        $this->Paginator->settings = array('conditions' => array('Tipoinstitucion.activo >=' => 1), 'order'=>array('Tipoinstitucion.tipoinstitucions'=>'asc'));
        $this->Tipoinstitucion->recursive = 0;
        include 'busqueda/catgastos.php';
        $data = $this->Paginator->paginate('Tipoinstitucion');
        $this->set('tipoinstitucions', $data);
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[tipoinstitucions]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tipoinstitucion", "tipoinstitucions", "view");
		if (!$this->Tipoinstitucion->exists($id)) {
			throw new NotFoundException(__('Invalid tipoinstitucion'));
		}
		$options = array('conditions' => array('Tipoinstitucion.' . $this->Tipoinstitucion->primaryKey => $id));
		$this->set('tipoinstitucion', $this->Tipoinstitucion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tipoinstitucion", "tipoinstitucions", "add");
		if ($this->request->is('post')) {
			$this->Tipoinstitucion->create();
            $this->request->data["Tipoinstitucion"]["usuario"]=$this->Session->read('nombreusuario');
            $this->request->data["Tipoinstitucion"]["modified"]=0;
			if ($this->Tipoinstitucion->save($this->request->data)) {
                $tipo_id = $this->Tipoinstitucion->id;
                $this->Session->write('tipoinstitucion_save', 1);
                $this->redirect(['action' => 'view', $tipo_id]);
			} else {
				$this->Session->setFlash(__('The tipoinstitucion could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tipoinstitucion", "tipoinstitucions", "edit");
		if (!$this->Tipoinstitucion->exists($id)) {
			throw new NotFoundException(__('Invalid tipoinstitucion'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data["Tipoinstitucion"]["usuariomodif"]=$this->Session->read('nombreusuario');
			if ($this->Tipoinstitucion->save($this->request->data)) {
                $this->Session->write('tipoinstitucion_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The tipoinstitucion could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipoinstitucion.' . $this->Tipoinstitucion->primaryKey => $id));
			$this->request->data = $this->Tipoinstitucion->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tipoinstitucion", "tipoinstitucions", "delete");
		if ($delete == true) {
			$this->Tipoinstitucion->id = $id;
			if (!$this->Tipoinstitucion->exists()) {
				throw new NotFoundException(__('Invalid tipoinstitucion'));
			}
			if ($this->Tipoinstitucion->delete()) {
                $_SESSION["delete"]=1;
                $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tipoinstitucion could not be deleted. Please, try again.'));
			}

        }else{
            $_SESSION["delete-no-priv"]=1;
            $this->redirect(array('action' => 'index'));
        }
	}
}
