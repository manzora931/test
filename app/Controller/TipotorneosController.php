<?php
App::uses('AppController', 'Controller');
/**
 * Tipotorneos Controller
 *
 * @property Tipotorneo $Tipotorneo
 * @property PaginatorComponent $Paginator
 */
class TipotorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tipotorneo", "tipotorneos", "index");
        $this->Paginator->settings = array('conditions' => array('Tipotorneo.activo >=' => 1), 'order'=>array('Tipotorneo.tipotorneo'=>'asc'));
        $this->Tipotorneo->recursive = 0;
        include 'busqueda/tipotorneos.php';
        $data = $this->Paginator->paginate('Tipotorneo');
        $this->set('tipotorneos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[tipotorneos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tipotorneo", "tipotorneos", "view");
		if (!$this->Tipotorneo->exists($id)) {
			throw new NotFoundException(__('Invalid tipotorneo'));
		}
		$options = array('conditions' => array('Tipotorneo.' . $this->Tipotorneo->primaryKey => $id));
		$this->set('tipotorneo', $this->Tipotorneo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tipotorneo", "tipotorneos", "add");
		if ($this->request->is('post')) {
			$this->Tipotorneo->create();
            $this->request->data['Tipotorneo']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Tipotorneo']['modified']=0;
			if ($this->Tipotorneo->save($this->request->data)) {
				$tipotorneo_id = $this->Tipotorneo->id;
                $this->Session->write('tipotorneo_save', 1);
                $this->redirect(['action' => 'view', $tipotorneo_id]);
			} else {
				$this->Session->setFlash(__('The Tipotorneo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tipotorneo", "tipotorneos", "edit");
		if (!$this->Tipotorneo->exists($id)) {
			throw new NotFoundException(__('Invalid tipotorneo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Tipotorneo']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Tipotorneo->save($this->request->data)) {
                $this->Session->write('tipotorneo_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The tipotorneo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipotorneo.' . $this->Tipotorneo->primaryKey => $id));
			$this->request->data = $this->Tipotorneo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tipotorneo", "tipotorneos", "delete");
		if ($delete == true) {
			$this->Tipotorneo->id = $id;
			if (!$this->Tipotorneo->exists()) {
				throw new NotFoundException(__('Invalid tipotorneo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Tipotorneo->delete()) {
					$this->Session->setFlash(__('The tipotorneo has been deleted.'));
			} else {
				$this->Session->setFlash(__('The tipotorneo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_tipotorneos() {
        $id = $_POST['id'];
        $data = array();

        // $this->loadModel('Torneo');
        $this->Tipotorneo->Torneo->recursive = -1;
        $tipo = $this->Tipotorneo->Torneo->find('all', [
            'conditions' => [
                'Torneo.tipotorneo_id' => $id,
            ]
        ]);

        $data['tipos'] = (count($tipo) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
