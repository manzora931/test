<?php
App::uses('AppController', 'Controller');
/**
 * Tipoproyectos Controller
 *
 * @property Tipoproyecto $Tipoproyecto
 * @property PaginatorComponent $Paginator
 */
class TipoproyectosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tipoproyecto", "tipoproyectos", "index");
        $this->Paginator->settings = array('conditions' => array('Tipoproyecto.activo >=' => 1), 'order'=>array('Tipoproyecto.nombre'=>'asc'));
        $this->Tipoproyecto->recursive = 0;
        include 'busqueda/tipoproyectos.php';
        $data = $this->Paginator->paginate('Tipoproyecto');
        $this->set('tipoproyectos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[tipoproyectos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tipoproyecto", "tipoproyectos", "view");
		if (!$this->Tipoproyecto->exists($id)) {
			throw new NotFoundException(__('Invalid tipoproyecto'));
		}
		$options = array('conditions' => array('Tipoproyecto.' . $this->Tipoproyecto->primaryKey => $id));
		$this->set('tipoproyecto', $this->Tipoproyecto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tipoproyecto", "tipoproyectos", "add");
		if ($this->request->is('post')) {
			$this->Tipoproyecto->create();
            $this->request->data['Tipoproyecto']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Tipoproyecto']['modified']=0;
			if ($this->Tipoproyecto->save($this->request->data)) {
                $tipoproyecto_id = $this->Tipoproyecto->id;
                $this->Session->write('tipoproyecto_save', 1);
                $this->redirect(['action' => 'view', $tipoproyecto_id]);
			} else {
				$this->Session->setFlash(__('The tipoproyecto could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tipoproyecto", "tipoproyectos", "edit");
		if (!$this->Tipoproyecto->exists($id)) {
			throw new NotFoundException(__('Invalid tipoproyecto'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Tipoproyecto']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Tipoproyecto->save($this->request->data)) {
                $this->Session->write('tipoproyecto_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The tipoproyecto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipoproyecto.' . $this->Tipoproyecto->primaryKey => $id));
			$this->request->data = $this->Tipoproyecto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tipoproyecto", "tipoproyectos", "delete");
		if ($delete == true) {
			$this->Tipoproyecto->id = $id;
			if (!$this->Tipoproyecto->exists()) {
				throw new NotFoundException(__('Invalid tipoproyecto'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Tipoproyecto->delete()) {
					$this->Session->setFlash(__('The tipoproyecto has been deleted.'));
			} else {
				$this->Session->setFlash(__('The tipoproyecto could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Proyecto');
        $this->Proyecto->recursive = -1;
        $tipo = $this->Proyecto->find('all', [
            'conditions' => [
                'Proyecto.tipoproyecto_id' => $id,
            ]
        ]);

        $data['tipos'] = (count($tipo) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
