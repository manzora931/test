<?php
App::uses('AppController', 'Controller');
/**
 * Eventos Controller
 *
 * @property Posicione $Posicione
 * @property PaginatorComponent $Paginator
 */
class PosicionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Posicione", "posiciones", "index");
        $this->Paginator->settings = array('conditions' => array('Posicione.activo >=' => 1), 'order'=>array('Posicione.posicion'=>'asc'));
        $this->Posicione->recursive = 0;
        include 'busqueda/posiciones.php';
        $data = $this->Paginator->paginate('Posicione');
        $this->set('posiciones', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[posiciones]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Posicione", "posiciones", "view");
		if (!$this->Posicione->exists($id)) {
			throw new NotFoundException(__('Invalid posicione'));
		}
		$options = array('conditions' => array('Posicione.' . $this->Posicione->primaryKey => $id));
		$this->set('posicione', $this->Posicione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Posicione", "posiciones", "add");
		if ($this->request->is('post')) {
			$this->Posicione->create();
            $this->request->data['Posicione']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Posicione']['modified']=0;
			if ($this->Posicione->save($this->request->data)) {
				$posicione_id = $this->Posicione->id;
                $this->Session->write('posicione_save', 1);
                $this->redirect(['action' => 'view', $posicione_id]);
			} else {
				$this->Session->setFlash(__('The posicione could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Posicione", "posiciones", "edit");
		if (!$this->Posicione->exists($id)) {
			throw new NotFoundException(__('Invalid posicione'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Posicione']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Posicione->save($this->request->data)) {
                $this->Session->write('posicione_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The posicione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Posicione.' . $this->Posicione->primaryKey => $id));
			$this->request->data = $this->Posicione->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Posicione", "posiciones", "delete");
		if ($delete == true) {
			$this->Posicione->id = $id;
			if (!$this->Posicione->exists()) {
				throw new NotFoundException(__('Invalid posicione'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Posicione->delete()) {
					$this->Session->setFlash(__('The posicione has been deleted.'));
			} else {
				$this->Session->setFlash(__('The posicione could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_posiciones() {
        $id = $_POST['id'];
        $data = array();

        // $this->loadModel('Jugadore');
        $this->Posicione->Jugadore->recursive = -1;
		$evento = $this->Posicione->Jugadore->find('all', [
            'conditions' => [
                'Jugadore.posicione_id' => $id,
            ]
        ]);

        $data['eventos'] = (count($evento) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
