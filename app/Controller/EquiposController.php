<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Equipo $Equipo
 * @property PaginatorComponent $Paginator
 */
class EquiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Equipo", "equipos", "index");
		$this->Paginator->settings = array('order'=>array('Equipo.nombrecorto'=>'asc'));
		$this->Equipo->recursive = 0;
		include 'busqueda/equipos.php';
		$data = $this->Paginator->paginate('Equipo');
		$this->set('equipos', $data);

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list',["conditions"=>["activa"=>1]]);
		$this->loadModel("Tipoequipo");
		$tipoequipos = $this->Tipoequipo->find('list',["conditions"=>["activo"=>1]]);
		$this->loadModel("Estadio");
		$estadios = $this->Estadio->find('list');

		$this->set(compact('paises', 'tipoequipos', 'estadios'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[equipos]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}
    /***
    funcion para validar el formato de la foto del jugador**/
    public function valFoto(){
        if(isset($_FILES['archivo'])) {
            $type = $_FILES['archivo']['type'];
            $size = $_FILES['archivo']['size'];
            $name = $_FILES['archivo']['name'];
            $tmpName = $_FILES['archivo']['tmp_name'];
            $ext = pathinfo($name);
            $error=0;
            if($ext['extension']=="png"||$ext['extension']=="PNG" || $ext['extension']=="jpg"||$ext['extension']=="JPG" || $ext['extension']=="jpeg"||$ext['extension']=="JPEG"){
                $error=0;
            }else{
                $error=1;
            }
        }
        echo $error;
        $this->autoRender=false;

    }
    public function fotoOrig($id){
        $this->layout=false;
        $options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
        $this->set('equipo', $this->Equipo->find('first', $options));
    }
    public function fotoOrig2($id){
        $this->layout=false;
        $options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
        $this->set('equipo', $this->Equipo->find('first', $options));
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Equipo", "equipos", "view");
		if (!$this->Equipo->exists($id)) {
			throw new NotFoundException(__('Invalid equipo'));
		}
		$options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
		$this->set('equipo', $this->Equipo->find('first', $options));

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipoequipo");
		$tipoequipos = $this->Tipoequipo->find('list');
		$this->loadModel("Estadio");
		$estadios = $this->Estadio->find('list');

		$this->set(compact('paises', 'tipoequipos', 'estadios'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Equipo", "equipos", "add");
		if ($this->request->is('post')) {
			$fechafundacion = explode('-', $this->request->data['Equipo']['fechafundacion']);
			$this->request->data['Equipo']['fechafundacion'] = $fechafundacion[2] . '-' . $fechafundacion[1] . '-' . $fechafundacion[0];
			$this->request->data['Equipo']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Equipo']['modified']=0;
			$this->Equipo->create();
            $file = new File($this->request->data["Equipo"]["fotoeq"]['tmp_name']);
            $file2 = new File($this->request->data["Equipo"]["fotoes"]['tmp_name']);

			if ($this->Equipo->save($this->request->data)) {
				$equipo_id = $this->Equipo->id;
                if(isset($this->data["Equipo"]["fotoeq"]['name']) && $this->data["Equipo"]["fotoeq"]['name']!='') {
                    $formato = explode('.', $this->data["Equipo"]["fotoeq"]['name']);
                    $filename = $formato[0]."_".$equipo_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/equipos/' . $filename;
                    $sql_Uprod = "UPDATE equipos SET fotoequipo='$url' WHERE id='$equipo_id'";
                    $result = $this->Equipo->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/equipos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                if(isset($this->data["Equipo"]["fotoes"]['name']) && $this->data["Equipo"]["fotoes"]['name']!='') {
                    $formato = explode('.', $this->data["Equipo"]["fotoes"]['name']);
                    $filename = $formato[0]."_".$equipo_id . '.' . $formato[1];
                    $data = $file2->read();
                    $url = 'img/equipos/' . $filename;
                    $sql_Uprod = "UPDATE equipos SET fotoescudo='$url' WHERE id='$equipo_id'";
                    $result = $this->Equipo->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/equipos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
				$this->Session->write('equipo_save', 1);
				$this->redirect(['action' => 'view', $equipo_id]);
			} else {
				$this->Session->setFlash(__('El equipo no ha sido creado. Intentar de nuevo.'));
			}
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipoequipo");
		$tipoequipos = $this->Tipoequipo->find('list');
		$this->loadModel("Estadio");
		$estadios = $this->Estadio->find('list');

		$this->set(compact('paises', 'tipoequipos', 'estadios'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Equipo", "equipos", "edit");
		if (!$this->Equipo->exists($id)) {
			throw new NotFoundException(__('Invalid equipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$fechafundacion = explode('-', $this->request->data['Equipo']['fechafundacion']);
			$this->request->data['Equipo']['fechafundacion'] = $fechafundacion[2] . '-' . $fechafundacion[1] . '-' . $fechafundacion[0];
			$this->request->data['Equipo']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Equipo']['modified'] = date("Y-m-d H:i:s");
            if ($this->Equipo->save($this->request->data)) {
				$equipo_id = $this->Equipo->id;
                if(isset($this->data["Equipo"]["fotoeq"]['name']) && $this->data["Equipo"]["fotoeq"]['name']!='') {
                    $file = new File($this->request->data["Equipo"]["fotoeq"]['tmp_name']);
                    $formato = explode('.', $this->data["Equipo"]["fotoeq"]['name']);
                    $filename = $formato[0]."_".$equipo_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/equipos/' . $filename;
                    $sql_Uprod = "UPDATE equipos SET fotoequipo='$url' WHERE id='$equipo_id'";
                    $result = $this->Equipo->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/equipos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                if(isset($this->data["Equipo"]["fotoes"]['name']) && $this->data["Equipo"]["fotoes"]['name']!='') {
                    $file2 = new File($this->request->data["Equipo"]["fotoes"]['tmp_name']);
                    $formato = explode('.', $this->data["Equipo"]["fotoes"]['name']);
                    $filename = $formato[0]."_".$equipo_id . '.' . $formato[1];
                    $data = $file2->read();
                    $url = 'img/equipos/' . $filename;
                    $sql_Uprod = "UPDATE equipos SET fotoescudo='$url' WHERE id='$equipo_id'";
                    $result = $this->Equipo->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/equipos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
				$this->Session->write('equipo_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('El equipo no se ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $id));
			$this->request->data = $this->Equipo->find('first', $options);
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipoequipo");
		$tipoequipos = $this->Tipoequipo->find('list');
		$this->loadModel("Estadio");
		$estadios = $this->Estadio->find('list');

		$this->set(compact('paises', 'tipoequipos', 'estadios'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Equipo", "equipos", "delete");
		if ($delete == true) {
			$this->Equipo->id = $id;
			if (!$this->Equipo->exists()) {
				throw new NotFoundException(__('Invalid equipo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Torneo->delete()) {
				$this->Session->setFlash(__('The equipo has been deleted.'));
			} else {
				$this->Session->setFlash(__('The equipo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}
}
