<?php
App::uses('AppController', 'Controller');
/**
 * Fuentesfinanciamientos Controller
 *
 * @property Fuentesfinanciamiento $Fuentesfinanciamiento
 * @property PaginatorComponent $Paginator
 */
class FuentesfinanciamientosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
    public $helpers = array('Html', 'Form', 'Js');
/**
 * index method
 *
 * @return void
 */
	public function index($idProyecto = null) {
        $data = array();

        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

		if(!is_null($idProyecto)) {
            $this->Paginator->settings = array(
                'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $idProyecto),
                'order'=>array('Fuentesfinanciamiento.financista_id'=>'asc')
            );

            $this->Fuentesfinanciamiento->recursive = 0;
            $data = $this->Paginator->paginate('Fuentesfinanciamiento');
        }
        $this->set('fuentesfinanciamientos', $data);

        $fuentes = $this->Fuentesfinanciamiento->find("all", [
            'fields' => [
                'Fuentesfinanciamiento.financista_id',
                'Fuentesfinanciamiento.id'
            ],
            'conditions'=> [
            'Fuentesfinanciamiento.proyecto_id' => $idProyecto
        ]
        ]);

        $financistasasignados = array();
        foreach ($fuentes as $fuente) {
            $info_ffinanciamiento[$fuente['Fuentesfinanciamiento']['id']]['desembolso']=$this->getDesembolsado($fuente['Fuentesfinanciamiento']['id']);
            $info_ffinanciamiento[$fuente['Fuentesfinanciamiento']['id']]['ejecutado']=$this->getEjecutado($fuente['Fuentesfinanciamiento']['id']);
            array_push($financistasasignados, $fuente['Fuentesfinanciamiento']['financista_id']);
        }

        $financistas = $this->Fuentesfinanciamiento->Financista->find("list", [
            'fields' => [
                'Financista.id',
                'Financista.nombre'
            ],
            'conditions'=> [
                'Financista.activo' => 1,
                'Financista.id !=' => $financistasasignados
            ]
        ]);
        /**EST5ADO PROYECTO**/
        $this->Fuentesfinanciamiento->Proyecto->recursive=-1;
        $infoP = $this->Fuentesfinanciamiento->Proyecto->read("estado_id",$idProyecto);
        $estadoProy = $infoP['Proyecto']['estado_id'];
        $this->set(compact( 'idProyecto', 'financistas','info_ffinanciamiento','estadoProy'));
	}
    public function getDesembolsado($id=null){
        $this->loadModel("Desembolso");
        $this->Desembolso->recursive=-1;
        $sql = $this->Desembolso->find("all",[
            'conditions'=>[
                'Desembolso.fuentefinanciamiento_id'=>$id
            ],
            'fields'=>['SUM(Desembolso.total) as total']
        ]);
        $monto = ($sql[0][0]['total']!=null)?$sql[0][0]['total']:0.00;
        return $monto;
    }
    public function getEjecutado($id=null){
        $this->loadModel("Gasto");
        $this->Gasto->recursive=-1;
        $sql = $this->Gasto->find("all",[
            'joins'=>[
                [
                    'table'=>'actividades',
                    'alias'=>'Actividade',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Actividade.id = Gasto.actividade_id'
                    ]
                ],
                [
                    'table'=>'fuentesfinanciamientos',
                    'alias'=>'Fuentesfinanciamiento',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Actividade.fuentesfinanciamiento_id = Fuentesfinanciamiento.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Fuentesfinanciamiento.id'=>$id
            ],
            'fields'=>[
                'Fuentesfinanciamiento.id','Actividade.id','Gasto.monto'
            ]
        ]);
        $monto = 0.00;
        foreach ($sql as $rw) {
            $monto+=$rw['Gasto']['monto'];
        }
        return $monto;
    }


    /**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		// $this->ValidarUsuario("Fuentesfinanciamiento", "fuentesfinanciamientos", "view");
		if (!$this->Fuentesfinanciamiento->exists($id)) {
			throw new NotFoundException(__('Invalid fuentesfinanciamiento'));
		}
		$options = array('conditions' => array('Fuentesfinanciamiento.' . $this->Fuentesfinanciamiento->primaryKey => $id));
		$this->set('fuentesfinanciamiento', $this->Fuentesfinanciamiento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		// $this->ValidarUsuario("Fuentesfinanciamiento", "fuentesfinanciamientos", "add");
		if ($this->request->is('post')) {
			$this->Fuentesfinanciamiento->create();
			if ($this->Fuentesfinanciamiento->save($this->request->data)) {
				$this->Session->setFlash(__('The fuentesfinanciamiento has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fuentesfinanciamiento could not be saved. Please, try again.'));
			}
		}
		$financistas = $this->Fuentesfinanciamiento->Financista->find('list');
		$proyectos = $this->Fuentesfinanciamiento->Proyecto->find('list');
		$this->set(compact('financistas', 'proyectos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		// $this->ValidarUsuario("Fuentesfinanciamiento", "fuentesfinanciamientos", "edit");
		if (!$this->Fuentesfinanciamiento->exists($id)) {
			throw new NotFoundException(__('Invalid fuentesfinanciamiento'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Fuentesfinanciamiento->save($this->request->data)) {
				$this->Session->setFlash(__('The fuentesfinanciamiento has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The fuentesfinanciamiento could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Fuentesfinanciamiento.' . $this->Fuentesfinanciamiento->primaryKey => $id));
			$this->request->data = $this->Fuentesfinanciamiento->find('first', $options);
		}
		$financistas = $this->Fuentesfinanciamiento->Financista->find('list');
		$proyectos = $this->Fuentesfinanciamiento->Proyecto->find('list');
		$this->set(compact('financistas', 'proyectos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete() {
        $this->Fuentesfinanciamiento->id = $_POST['id'];
        if (!$this->Fuentesfinanciamiento->exists()) {
            $fuente['msg'] = 'error1';
        }

        $this->loadModel('Actividade');
        $actividad = $this->Actividade->find('all', [
            'conditions' => [
                'Actividade.fuentesfinanciamiento_id' => $_POST['id'],
            ]
        ]);

        if(count($actividad) > 0){
            $fuente['msg'] = 'error3';
        } else {
            $fuentefinanciamiento = $this->Fuentesfinanciamiento->find('first', [
                'conditions' => ['Fuentesfinanciamiento.id' => $this->Fuentesfinanciamiento->id]
            ]);

            if ($this->Fuentesfinanciamiento->delete()) {
                $fuente['msg'] = 'exito';

                $_SESSION['logsproyecto']['accion'] = 'Eliminar';
                $_SESSION['logsproyecto']['data'] = $fuentefinanciamiento['Fuentesfinanciamiento'];
                $_SESSION['logsproyecto']['data']['financista'] = $fuentefinanciamiento['Financista']['nombre'];

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Fuentesfinanciamiento', 'fuentesfinanciamientos', 'Fuentes de Financiamiento');
            } else {
                $fuente['msg'] = 'error2';
            }
        }

        $this->Paginator->settings = array(
            'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $_POST['proyectoid']),
            'order'=>array('Fuentesfinanciamiento.financista_id'=>'asc')
        );

        $this->Fuentesfinanciamiento->recursive = 0;
        $fuente['data'] = $this->Paginator->paginate('Fuentesfinanciamiento');

        $sql = "SELECT sum(monto) as montototal, 0 as totalgastos,  0 as totaldesembolsos FROM fuentesfinanciamientos WHERE proyecto_id = '" . $_POST['proyectoid'] . "'";
        $fuente['totales'] = $this->Fuentesfinanciamiento->query($sql);

        echo json_encode($fuente);
        $this->autoRender=false;
	}

    public function get_data() {
        $id = $_POST['id'];
        $sql = "SELECT financista_id, monto FROM fuentesfinanciamientos WHERE id = " . $id;
        $fuente['data'] = $this->Fuentesfinanciamiento->query($sql);

        $sql = "SELECT fi.id, fi.nombre FROM  financistas fi WHERE fi.id = '" . $_POST['id'] . "' OR  NOT fi.id IN (
              SELECT financista_id FROM fuentesfinanciamientos WHERE proyecto_id = '" . $_POST['proyectoid'] . "') ";

        $fuente['financista'] = $this->Fuentesfinanciamiento->query($sql);

        $fuente['msg'] = 'exito';

        echo json_encode($fuente);
        $this->autoRender=false;
    }
    public function get_data2() {
        $id = $_POST['id'];
        $sql = "SELECT financista_id, monto FROM fuentesfinanciamientos WHERE id = " . $id;
        $fuente['data'] = $this->Fuentesfinanciamiento->query($sql);
        $inf = $this->Fuentesfinanciamiento->query($sql);
        $id_sel = $inf[0]['fuentesfinanciamientos']['financista_id'];
        $sql = "SELECT fi.id, fi.nombre FROM  financistas fi WHERE fi.id = '" . $_POST['id'] . "' OR  NOT fi.id IN (
              SELECT financista_id FROM fuentesfinanciamientos WHERE financista_id != '".$id_sel."' AND proyecto_id = '" . $_POST['proyectoid'] . "') ";

        $fuente['financista'] = $this->Fuentesfinanciamiento->query($sql);

        $fuente['msg'] = 'exito';

        echo json_encode($fuente);
        $this->autoRender=false;
    }

    public function almacenar_data() {
        $id = $_POST['id'];
        if($id != '') {
            $fuentefinanciamiento = $this->Fuentesfinanciamiento->find('first', [
                'conditions' => [
                    'Fuentesfinanciamiento.id' => $id
                ]
            ]);

            $_SESSION['logsproyecto']['anterior'] = $fuentefinanciamiento;
            if(count($fuentefinanciamiento) > 0) {
                $fuentefinanciamiento['Fuentesfinanciamiento']['financista_id'] = $_POST['financistaid'];
                $fuentefinanciamiento['Fuentesfinanciamiento']['monto'] = $_POST['monto'];
                $fuentefinanciamiento['Fuentesfinanciamiento']['usuariomodif'] = $this->Session->read('nombreusuario');
                $fuentefinanciamiento['Fuentesfinanciamiento']['modified'] = date('Y-m-d H:i:s');
                $log = $fuentefinanciamiento['Fuentesfinanciamiento'];

                if ($this->Fuentesfinanciamiento->save($fuentefinanciamiento)) {
                    $fuente['id'] = $id;
                    $log['financista'] = $_POST['financista'];
                    $_SESSION['logsproyecto']['accion'] = 'Modificar';
                    $_SESSION['logsproyecto']['data'] = $log;

                    $this->loadModel("Logproyecto");
                    $this->Logproyecto->registro_bitacora('Fuentesfinanciamiento', 'fuentesfinanciamientos', 'Fuentes de Financiamiento');
                } else {
                    $fuente['msg'] = 'error';
                }
            }
        } else {
            $data['proyecto_id'] = $_POST['proyectoid'];
            $data['financista_id'] = $_POST['financistaid'];
            $data['monto'] = $_POST['monto'];
            $data['usuario'] = $this->Session->read('nombreusuario');
            $data['created'] = date('Y-m-d H:i:s');
            $data['modified'] = 0;
            $log = $data;

            $this->Fuentesfinanciamiento->create();
            if ($this->Fuentesfinanciamiento->save($data)) {
                $fuente['id'] = $this->Fuentesfinanciamiento ->id;
                $log['id'] = $this->Fuentesfinanciamiento ->id;
                $log['financista'] = $_POST['financista'];
                $_SESSION['logsproyecto']['accion'] = 'Agregar';
                $_SESSION['logsproyecto']['data'] = $log;

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Fuentesfinanciamiento', 'fuentesfinanciamientos', 'Fuentes de Financiamiento');
            } else {
                $fuente['msg'] = 'error';
            }
        }

        $this->Paginator->settings = array(
            'conditions' => array('Fuentesfinanciamiento.proyecto_id' => $_POST['proyectoid']),
            'order'=>array('Fuentesfinanciamiento.financista_id'=>'asc')
        );

        $this->Fuentesfinanciamiento->recursive = 0;
        $fuente['data'] = $this->Paginator->paginate('Fuentesfinanciamiento');

        $sql = "SELECT sum(monto) as montototal, 0 as totalgastos,  0 as totaldesembolsos FROM fuentesfinanciamientos WHERE proyecto_id = '" . $_POST['proyectoid'] . "'";
        $fuente['totales'] = $this->Fuentesfinanciamiento->query($sql);

        echo json_encode($fuente);
        $this->autoRender=false;
    }
}
