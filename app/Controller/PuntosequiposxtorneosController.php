<?php
App::uses('AppController', 'Controller');
/**
 * Puntosequiposxtorneos Controller
 *
 * @property Puntosequiposxtorneo $Puntosequiposxtorneo
 * @property PaginatorComponent $Paginator
 */
class PuntosequiposxtorneosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Puntosequiposxtorneo", "puntosequiposxtorneos", "index");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->Paginator->settings = array('conditions'=>[
                    "Puntosequiposxtorneo.torneo_id IN"=>$torneos
                ],'order'=>array('Puntosequiposxtorneo.Torneo.nombrecorto'=>'asc'));
            }else{
                $this->Paginator->settings = array('conditions'=>[
                    "Puntosequiposxtorneo.torneo_id"=>$torneos
                ],'order'=>array('Puntosequiposxtorneo.Torneo.nombrecorto'=>'asc'));
            }
        }else{
            $this->Paginator->settings = array('order'=>array('Puntosequiposxtorneo.Torneo.nombrecorto'=>'asc'));
        }

        $this->Puntosequiposxtorneo->recursive = 0;
        include 'busqueda/personas.php';
        $data = $this->Paginator->paginate('Puntosequiposxtorneo');
        $this->set('puntosequiposxtorneos', $data);
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $torneos = $this->Puntosequiposxtorneo->Torneo->find('list',[
                    "conditions"=>["Torneo.activo"=>1,"Torneo.id IN"=>$torneos],
                    "order"=>["Torneo.nombrecorto"=>"ASC"]
                ]);
            }else{
                $torneos = $this->Puntosequiposxtorneo->Torneo->find('list',[
                    "conditions"=>["Torneo.activo"=>1,"Torneo.id"=>$torneos],
                    "order"=>["Torneo.nombrecorto"=>"ASC"]
                ]);
            }
        }else{
            $torneos = $this->Puntosequiposxtorneo->Torneo->find('list',[
                "conditions"=>["Torneo.activo"=>1],
                "order"=>["Torneo.nombrecorto"=>"ASC"]
            ]);
        }

        $this->loadModel("Tipopersona");


        $this->set(compact('torneos'));
	}
    /***
     * author: Manuel Anzora
     * create: 10:02-2019
     * descripcion: metodo para resetear la busqueda**/
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[puntosequiposxtorneos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
    /***
     * author: Manuel Anzora
     * create: 10:02-2019
     * descripcion: metodo para obtener equipos segun el torneo seleccionado**/
    public function getEquipos(){
        $this->autoRender=false;
        $this->Puntosequiposxtorneo->Equipo->recursive=-1;
        $sql = $this->Puntosequiposxtorneo->Equipo->find("all",[
            "fields"=>["Equipo.nombrecorto","Equipo.id"],
            "joins"=>[
                [
                    "table"=>"equiposxtorneos",
                    "alias"=>"equipotorneo",
                    "type"=>"inner",
                    "conditions"=>"Equipo.id=equipotorneo.equipo_id"
                ]
            ],
            "conditions"=>["equipotorneo.torneo_id"=>$_POST["torneo"]],
            "order"=>["Equipo.nombrecorto"=>"Asc"]
        ]);
        $html = "<option>Seleccionar</option>";
        foreach ($sql as $item){
            if($_POST["equipo"]==$item["Equipo"]["id"]){
                $html .= "<option value='".$item["Equipo"]["id"]."' selected>".$item["Equipo"]["nombrecorto"]."</option>";
            }else{
                $html .= "<option value='".$item["Equipo"]["id"]."'>".$item["Equipo"]["nombrecorto"]."</option>";
            }
        }
        echo json_encode(["error"=>0,"option"=>$html]);
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Puntosequiposxtorneo", "puntosequiposxtorneos", "view");
		if (!$this->Puntosequiposxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid puntosequiposxtorneo'));
		}
		$options = array('conditions' => array('Puntosequiposxtorneo.' . $this->Puntosequiposxtorneo->primaryKey => $id));
		$this->set('puntosequiposxtorneo', $this->Puntosequiposxtorneo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Puntosequiposxtorneo", "puntosequiposxtorneos", "add");
		if ($this->request->is('post')) {
            $this->request->data['Puntosequiposxtorneo']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Puntosequiposxtorneo']['modified']=0;
			$this->Puntosequiposxtorneo->create();
			if ($this->Puntosequiposxtorneo->save($this->request->data)) {
                $this->Session->write('punto_save', 1);
                $this->redirect(['action' => 'view', $this->Puntosequiposxtorneo->id]);
			} else {
				$this->Session->setFlash(__('No se pudo almacenar el registro. Intente nuevamente'));
			}
		}
        $torneos = $this->Puntosequiposxtorneo->Torneo->find('list',[
            "conditions"=>["Torneo.activo"=>1],
            "order"=>["Torneo.nombrecorto"=>"ASC"]
        ]);
		$this->set(compact('torneos', 'equipos'));
	}
	public function valUnique(){
	    $this->autoRender=false;
	    $where =[];
	    if($_POST["id"]>0){
	        $where=["Puntosequiposxtorneo.torneo_id"=>$_POST["torneo"],"Puntosequiposxtorneo.equipo_id"=>$_POST["equipo"],"Puntosequiposxtorneo.id !="=>$_POST["id"]];
        }else{
	        $where=["Puntosequiposxtorneo.torneo_id"=>$_POST["torneo"],"Puntosequiposxtorneo.equipo_id"=>$_POST["equipo"]];
        }
	    $sql = $this->Puntosequiposxtorneo->find("all",[
	        "fields"=>["Puntosequiposxtorneo.id"],
	        "conditions"=>$where
        ]);
	    if(count($sql)>0){
	        echo json_encode(["error"=>1]);
        }else{
	        echo json_encode(["error"=>0]);
        }
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Puntosequiposxtorneo", "puntosequiposxtorneos", "edit");
		if (!$this->Puntosequiposxtorneo->exists($id)) {
			throw new NotFoundException(__('Invalid puntosequiposxtorneo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Puntosequiposxtorneo']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Puntosequiposxtorneo->save($this->request->data)) {
                $this->Session->write('punto_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The puntosequiposxtorneo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Puntosequiposxtorneo.' . $this->Puntosequiposxtorneo->primaryKey => $id));
			$this->request->data = $this->Puntosequiposxtorneo->find('first', $options);
            $equipos = $this->Puntosequiposxtorneo->Equipo->find('list',[
                "conditions"=>["Equipo.id"=>$this->request->data["Puntosequiposxtorneo"]["equipo_id"]]
            ]);
		}
		$torneos = $this->Puntosequiposxtorneo->Torneo->find('list');

		$this->set(compact('torneos', 'equipos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Puntosequiposxtorneo", "puntosequiposxtorneos", "delete");
		if ($delete == true) {
			$this->Puntosequiposxtorneo->id = $id;
			if (!$this->Puntosequiposxtorneo->exists()) {
				throw new NotFoundException(__('Invalid puntosequiposxtorneo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Puntosequiposxtorneo->delete()) {
                $this->Session->write('punto_delete', 1);
                $this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('The puntosequiposxtorneo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}}
