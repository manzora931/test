<?php
App::uses('AppController', 'Controller');
/**
 * Privilegios Controller
 *
 * @property Privilegio $Privilegio
 * @property PaginatorComponent $Paginator
 */
class PrivilegiosController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    var $helpers = array('Html', 'Form','Js','Session');
    public $components = array('Paginator');
    public $PaginadosGroup = 5;
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->ValidarUsuario("Privilegio", "privilegios", "index");
        $this->Paginator->settings = array('conditions' => array('Privilegio.activo' => 1));
        $this->Privilegio->recursive = 0;
        $data = $this->Paginator->paginate('Privilegio');

        $perfiles = $this->Privilegio->Perfile->find('all', [
            'conditions' => [
                'Perfile.activo' => 1
            ]
        ]);

        $modulos = $this->Privilegio->Recurso->Modulo->find('list',array('conditions' => array('Modulo.activo' => 1),'order'=>array('Modulo.modulo'=>'asc')));
        $this->set(compact("perfiles","modulos"));
    }

    function getRecurso() {
        $id = $_POST['id'];
        $result = $this->Privilegio->query("SELECT id, nombre, modelo FROM recursos where id = '".$id."' AND activo = 1");
        echo json_encode($result);

        $this->autoRender = false;
    }

    public function searchPerfiles () {
      //  $searchBy = $this->request->data['searchBy'];
        $searchText = $this->request->data['searchText'];
        $active=$this->request->data['active'];


        switch ($active) {
            case 1:
                $active = ["Perfile.activo >=" => 0];
                break;
            case 2:
                $active = ["Perfile.id !=" => 0];
                break;
            default:
                $active = ["Perfile.activo" => 1];
                break;
        }

        if($searchText != ''){
            $result = array();

            $json = $this->Privilegio->query("SELECT perfiles.id, perfiles.perfil FROM perfiles
            inner join privilegios on (perfiles.id = privilegios.perfile_id)
            inner join recursos on (recursos.id = privilegios.recurso_id)
            where recursos.nombre like '%".$searchText."%' or recursos.modelo like '%".$searchText."%'  and perfiles.id=privilegios.perfile_id");
            foreach($json  as $data){
                $result[$data['perfiles']['id']]=$data['perfiles']['perfil'];
            }


            echo json_encode($result);
        }else{
            echo json_encode( $this->Privilegio->Perfile->find("list", [
                'fields' => [
                    'Perfile.perfil'
                ],
                'conditions' => [
                    'AND' => $active
                ]
            ]));
        }

        $this->autoRender = false;
    }

    function privilegios () {
        if(isset( $this->request->data['id'])){
            $id = $this->request->data['id'];




        $privilegios = $this->Privilegio->find("all", [
            "conditions" => [
                "Privilegio.perfile_id" => $id
            ]
          /*  'JOIN' => [
                'table' => 'recursos',
                'alias' => 'r',
                'conditions' => [
                    'r.id' => 'Privilegio.recurso_id'
                ]
            ]*/
        ]);

        $perfiles = $this->Privilegio->Perfile->find("all", [
            "conditions" => [
                "Perfile.id" => $id
            ]
        ]);

        $modulos = [];
        $detRecursos = [];

        $detPrivilegios = Set::combine($this->Privilegio->Detprivilegio->find('all', [
            'conditions' => [
                'Privilegio.perfile_id' => $id
            ]
        ]), '{n}.Detrecurso.id', '{n}');

        foreach ($privilegios as $privilegio) {
            if (!in_array($privilegio['Recurso']['modulo_id'], $modulos)) {
                $modulos[$privilegio['Recurso']['modulo_id']][$privilegio['Recurso']['ubicacion']] = $this->Privilegio->Recurso->Modulo->find('first', [
                    'conditions' => [
                        'AND' => [
                            'Modulo.id' => $privilegio['Recurso']['modulo_id'],
                            'Modulo.activo' => 1
                        ]
                    ]
                ]);
            }

            $detRecursos[$privilegio['Recurso']['id']] = $this->Privilegio->Recurso->Detrecurso->find('all', [
                'conditions' => [
                    'Detrecurso.recurso_id' => $privilegio['Recurso']['id']
                ]
            ]);
        }

        $userId = $this->Privilegio->query("SELECT id FROM users WHERE username='{$this->Session->read('nombreusuario')}'");
        $userId = $userId[0]['users']['id'];
        $ubicaciones = ['Admin' => 'Panel de Control', 'Menu' => 'Menú'];
        $privilegiosRegistrosRecursos = $this->Privilegio->query("SELECT * FROM privilegiosregistrosrecursos WHERE user_id = '$userId'");
        $privilegiosRegistrosRecursos = Set::combine($privilegiosRegistrosRecursos, '{n}.privilegiosregistrosrecursos.registrosrecurso_id', '{n}');
        $registrosRecursos = $this->Privilegio->query("SELECT * FROM registrosrecursos");
        $registrosRecursos = Set::combine($registrosRecursos, '{n}.registrosrecursos.recurso_id', '{n}');
        krsort($modulos);
        $this->set(compact('privilegios', 'modulos', 'detRecursos', 'permitir', 'detPrivilegios','ubicaciones', 'privilegiosRegistrosRecursos', 'registrosRecursos'));
        }
    }

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[privilegios]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

    public function update() {
        $registro = $this->request->data['registro'];
        $activo = $this->request->data['activo'];

        $solopropietario = $this->request->data['solopropietario'] == 'true' ? 1 : 0;
        $currentDate = date("Y-m-j H:i:s");
        $userId = $this->Privilegio->query("SELECT id FROM users WHERE username='{$this->Session->read('nombreusuario')}'");
        $userId = $userId[0]['users']['id'];
            //debug($this->request->data['privilegioregistro_id']);
        //Actualizar registro si existe
        if ($this->request->data['id']) {
            $this->Privilegio->query("UPDATE privilegiosregistrosrecursos SET solopropietario={$solopropietario}, modified='{$currentDate}' WHERE id = {$this->request->data['id']}");
            echo $this->request->data['id'];
        } else {
            debug('else');
            //crear registro si no existe
            $this->Privilegio->query("INSERT INTO privilegiosregistrosrecursos (registrosrecurso_id, user_id, solopropietario, activo, created, usuario, modified, usuariomodif) VALUES ({$registro}, {$userId}, {$solopropietario},{$activo}, '{$currentDate}', '{$_SESSION['nombreusuario']}', '1000-01-01 00:00:00', '')");
            $id = $this->Privilegio->query("SELECT id FROM privilegiosregistrosrecursos ORDER BY id DESC LIMIT 1");
            $id = $id[0]['privilegiosregistrosrecursos']['id'];
            echo $id;
        }
        $this->Privilegio->create();
        $this->request->data['Privilegio'] = $this->request->data;
       // $this->request->data['Privilegio']['activo'] = 1;

        if ($this->Privilegio->save($this->request->data)) {
        }

        $this->autoRender = false;
    }

    public function updateDetPrivilegio () {
        $detPrivilegioId = $this->request->data['idDetPrivilegio'];
        $privilegioId = $this->request->data['idPrivilegio'];
        $detRecursoId = $this->request->data['idDetRecurso'];
        $permitir = $this->request->data['permitir'];
        $ref = $this->Privilegio->Detprivilegio;

        if ($detPrivilegioId) {
            $ref->read(null, $detPrivilegioId);
            $before = $ref->read('permitir');
            $ref->set('permitir', $permitir);
            if ($ref->save()) {
                echo json_encode(['id' => $ref->id, 'before' => $before, 'after' => $ref->read('permitir')]);
            }
        } else {
            $ref->create();
            $data = [
                'Detprivilegio' => [
                    'privilegio_id' => $privilegioId,
                    'detrecurso_id' => $detRecursoId,
                    'permitir' => $permitir
                ]
            ];

            if ($ref->save($data)) {
                $before = [
                    'Detprivilegio' => [
                        'permitir' => false
                    ]
                ];

                echo json_encode(['id' => $ref->id, 'before' => $before, 'after' => $ref->read('permitir')]);
            }
        }

        $this->autoRender = false;
    }

    public function adicionar() {
        $privilegio = $this->request->data;
        $this->request->data['Privilegio']['solopropietario'];
        $this->Privilegio->create();
        $this->request->data['Privilegio']['usuario'] = $this->Session->read('nombreusuario');
        $this->request->data['Privilegio']['activo'] = 1;
        $this->request->data['Privilegio']['modified'] = 0;
        if ($this->Privilegio->save($this->request->data)) {
            echo "success!";
        }

        $this->autoRender=false;
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->ValidarUsuario("Privilegio", "privilegios", "view");
        if (!$this->Privilegio->exists($id)) {
            throw new NotFoundException(__('Privilegio no valido'));
        }
        $options = array('conditions' => array('Privilegio.' . $this->Privilegio->primaryKey => $id));
        $privilegio = $this->Privilegio->find('first', $options);
        $detprivilegios = $this->Privilegio->Detprivilegio->find('all', array('conditions'=>array('Detprivilegio.privilegio_id'=>$id,'AND'=>array('Detrecurso.activo'=>1)),'fields'=>'Detprivilegio.permitir,Detprivilegio.detrecurso_id,Detrecurso.activo'));
        $detrecursos = $this->Privilegio->Detprivilegio->Detrecurso->find('all',array('conditions'=>array('Detrecurso.recurso_id'=>$privilegio['Privilegio']['recurso_id'], 'AND'=>array('Detrecurso.activo'=>1)),'fields'=>'Detrecurso.id,Detrecurso.nombre,Detrecurso.funcion,Detrecurso.activo'));

        $this->set(compact("privilegio","detrecursos","detprivilegios"));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->ValidarUsuario("Privilegio", "privilegios", "add");
        if ($this->request->is('post')) {
            $this->Privilegio->create();
            $this->request->data['Privilegio']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Privilegio']['modified'] = 0;

            if ($this->Privilegio->save($this->request->data)) {
                $idPrivilegio = $this->Privilegio->id;
                if(isset($this->request->data['det']))
                {
                    foreach($this->request->data['det'] as $k => $v) {
                        $this->Privilegio->query("INSERT INTO detprivilegios(privilegio_id, detrecurso_id, permitir) VALUES (" . $idPrivilegio . ",".$v['id'].",".$v['permitir'].")");
                    }
                }
                $this->Session->write("edit_priv",1);
                return $this->redirect(array('action' => 'view/' . $idPrivilegio));
            } else {
                $this->Session->setFlash(__('The privilegio could not be saved. Please, try again.'));
            }
        }
        $groups=$this->Privilegio->Perfile->find("list");
        $modulos=$this->Privilegio->Recurso->Modulo->find("list");
        $this->set(compact("groups", "modulos"));
    }

    public function recursos() {
        $idModulo = $_POST['idMod'];
        $idPerfil = $_POST['idPerfil'];

        $privilegios = $this->Privilegio->find('list', [
            'fields' => [
                'Privilegio.recurso_id'
            ],
            'conditions' => [
                'Privilegio.perfile_id' => $idPerfil
            ]
        ]);

        $recursos = $this->Privilegio->Recurso->find('list', [
            'conditions' => [
                'Recurso.modulo_id' => $idModulo,
                'NOT' => [
                    'Recurso.id' => $privilegios
                ]
            ]
        ]);

        echo json_encode($recursos);

        $this->autoRender = false;
    }

    public function detRecursos(){
        $detRecursos = $this->Privilegio->Recurso->Detrecurso->find("all",array('conditions'=>array('Detrecurso.recurso_id'=>$_POST['idReq'], 'Detrecurso.activo'=>1),
            'fields' => array('id','funcion','nombre')));
        if(count($detRecursos)>0)
        {
            $resul = "<h3>Detalles de privilegio - Botones</h3><table style = 'width: 50%; margin-left: 1.5em;' ><tr><th>Nombre a mostrar</th><th>Funcion</th><th>Permitir</th></tr>";
            foreach($detRecursos as $k => $v){

                $resul .= "<tr><td>".$v['Detrecurso']['nombre']."</td><td>".$v['Detrecurso']['funcion']."</td><td style ='text-align:center;'><input type='hidden' name='data[det][".$k."][id]' value='".$v['Detrecurso']['id']."' /><input type='hidden' name='data[det][".$k."][permitir]' id='det".$k."_' value='0' /><input type='checkbox' name='data[det][".$k."][permitir]' id='det".$k."' style='float:none;' value='1' /></td></tr>";
            }
            $resul .= "</table>";
        }else{
            $resul = "N";
        }
        echo $resul;
        $this->autoRender = false;
    }

    public function edit($id = null) {
        $this->ValidarUsuario("Privilegio", "privilegios", "edit");
        if (!$this->Privilegio->exists($id)) {
            throw new NotFoundException(__('Invalid privilegio'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Privilegio']['usuariomodif'] = $this->Session->read('nombreusuario');
            if ($this->Privilegio->save($this->request->data)) {
                if(isset($this->request->data['det']))
                {
                    foreach($this->request->data['det'] as $k => $v) {
                        if(isset($v['id'])){
                            $this->Privilegio->query("UPDATE detprivilegios SET privilegio_id = ".$id." ,detrecurso_id = ".$v['detrecurso_id'].",permitir = ".$v['permitir']." WHERE id = ".$v['id']);
                        }else{
                            $this->Privilegio->query("INSERT INTO detprivilegios(privilegio_id, detrecurso_id, permitir) VALUES (".$id.",".$v['detrecurso_id'].",".$v['permitir'].")");
                        }
                    }
                }
                $this->Session->write("edit_priv",1);
                return $this->redirect(array('action' => 'view/'.$id));
            } else {
                $this->Session->setFlash(__('Error al editar'), 'default', array(), 'editado');
            }
        } else {
            $options = array('conditions' => array('Privilegio.' . $this->Privilegio->primaryKey => $id));
            $privilegio = $this->Privilegio->find('first', $options);
            $this->request->data = $privilegio;
            $detprivilegios = $this->Privilegio->Detprivilegio->find('all', array('conditions'=>array('Detprivilegio.privilegio_id'=>$id,'AND'=>array('Detrecurso.activo'=>1)),'fields'=>'Detprivilegio.permitir,Detprivilegio.detrecurso_id,Detrecurso.activo,Detprivilegio.id'));
            $detrecursos = $this->Privilegio->Detprivilegio->Detrecurso->find('all',array('conditions'=>array('Detrecurso.recurso_id'=>$privilegio['Privilegio']['recurso_id'], 'AND'=>array('Detrecurso.activo'=>1)),'fields'=>'Detrecurso.id,Detrecurso.nombre,Detrecurso.funcion,Detrecurso.activo'));
            $groups=$this->Privilegio->Perfile->find("list");
            $recursos=$this->Privilegio->Recurso->find("list",array('fields' =>array('id','nombre'),'conditions'=>array('Recurso.activo'=>1),'order'=>array('Recurso.nombre'=>'asc')));
            $this->set(compact("groups", "recursos",'detrecursos','detprivilegios'));
        }
    }
}