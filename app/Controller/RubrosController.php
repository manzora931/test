<?php
App::uses('AppController', 'Controller');
/**
 * Rubros Controller
 *
 * @property Rubro $Rubro
 * @property PaginatorComponent $Paginator
 */
class RubrosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Rubro", "rubros", "index");
        $this->Paginator->settings = array('conditions' => array('Rubro.activo >=' => 1), 'order'=>array('Rubro.rubro'=>'asc'));
        $this->Rubro->recursive = 0;
        include 'busqueda/catgastos.php';
        $data = $this->Paginator->paginate('Rubro');
        $this->set('rubros', $data);
	}
    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[rubros]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Rubro", "rubros", "view");
		if (!$this->Rubro->exists($id)) {
			throw new NotFoundException(__('Invalid rubro'));
		}
		$options = array('conditions' => array('Rubro.' . $this->Rubro->primaryKey => $id));
		$this->set('rubro', $this->Rubro->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Rubro", "rubros", "add");
		if ($this->request->is('post')) {
			$this->Rubro->create();
            $this->request->data['Rubro']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Rubro']['modified'] = 0;
			if ($this->Rubro->save($this->request->data)) {
                $this->Session->write("rebro_save",1);
				return $this->redirect(array('action' => 'view',$this->Rubro->id));
			} else {
				$this->Session->setFlash(__('The rubro could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Rubro", "rubros", "edit");
		if (!$this->Rubro->exists($id)) {
			throw new NotFoundException(__('Invalid rubro'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Rubro']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Rubro->save($this->request->data)) {
                $this->Session->write("rebro_save",1);
                return $this->redirect(array('action' => 'view',$this->Rubro->id));
			} else {
				$this->Session->setFlash(__('The rubro could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Rubro.' . $this->Rubro->primaryKey => $id));
			$this->request->data = $this->Rubro->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $delete = $this->ValidarUsuario("Rubro", "rubros", "delete");
        if ($delete == true) {

            $this->Rubro->id = $id;
            if (!$this->Rubro->exists()) {
                throw new NotFoundException(__('Invalid jugadoresxequipo'));
            }

            if ($this->Rubro->delete()) {
                $this->Session->write("delete",1);
            } else {
                $this->Session->write("delete",0);
            }
            $this->redirect(array('action' => 'index'));
        }else{
            $this->Session->write("delete-no-priv",1);
            $this->redirect(array('action' => 'index'));
        }

    }
    public function val_name() {
        $valor=$_POST['val'];
        $id =$_POST['id'];
        if($id != 0){
            $datos=$this->Rubro->query("SELECT rubro FROM rubros WHERE rubro='$valor' AND id !=".$id);
        }else{
            $datos=$this->Rubro->query("SELECT rubro FROM rubros WHERE rubro='$valor'");
        }
        if(count($datos)>0){
            echo "error";
        }else{
            echo "ok";
        }
        $this->autoRender=false;
    }
}
