<?php
App::uses('AppController', 'Controller');
/**
 * Destinatarios Controller
 *
 * @property Destinatario $Destinatario
 * @property PaginatorComponent $Paginator
 */
class DestinatariosController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js', 'Form');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Destinatario", "destinatarios", "index");
        $this->Paginator->settings = array('conditions' => array('Destinatario.activo >=' => 1), 'order'=>array('Destinatario.nombre'=>'asc'));
        $this->Destinatario->recursive = 0;
        include 'busqueda/destinatarios.php';
        $data = $this->Paginator->paginate('Destinatario');
        $this->set('destinatarios', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[destinatarios]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Destinatario", "destinatarios", "view");
		if (!$this->Destinatario->exists($id)) {
			throw new NotFoundException(__('Invalid destinatario'));
		}
		$options = array('conditions' => array('Destinatario.' . $this->Destinatario->primaryKey => $id));
		$this->set('destinatario', $this->Destinatario->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Destinatario", "destinatarios", "add");
		if ($this->request->is('post')) {
			$this->Destinatario->create();
            $this->request->data['Destinatario']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Destinatario']['modified']=0;
			if ($this->Destinatario->save($this->request->data)) {
                $destinatario_id = $this->Destinatario->id;
                $this->Session->write('destinatario_save', 1);
                $this->redirect(['action' => 'view', $destinatario_id]);
			}
		}

        $destasignados = $this->Destinatario->find("all", [
            'fields' => [
                'Destinatario.contacto_id',
            ],
            'conditions'=> [
                'Destinatario.activo' => 1
            ]
        ]);

        $contactosasignados = array();
        foreach ($destasignados as $contacto) {
            array_push($contactosasignados, $contacto['Destinatario']['contacto_id']);
        }

        $contactos = $this->Destinatario->Contacto->find("list", [
            'fields' => [
                'Contacto.id',
                'Contacto.nombre_contacto'
            ],
            'conditions'=> [
                'Contacto.activo' => 1,
                'Contacto.id !=' => $contactosasignados
            ]
        ]);

		$this->set(compact('contactos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Destinatario", "destinatarios", "edit");
		if (!$this->Destinatario->exists($id)) {
			throw new NotFoundException(__('Invalid destinatario'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Destinatario']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Destinatario->save($this->request->data)) {
                $this->Session->write('destinatario_save', 1);
                $this->redirect(['action' => 'view', $id]);
			}
		} else {
			$options = array('conditions' => array('Destinatario.' . $this->Destinatario->primaryKey => $id));
			$this->request->data = $this->Destinatario->find('first', $options);
		}

        $destsasignados = $this->Destinatario->find("all", [
            'fields' => [
                'Destinatario.contacto_id',
            ],
            'conditions'=> [
                'Destinatario.activo' => 1
            ]
        ]);

        $contactosasignados = array();
        foreach ($destsasignados as $contacto) {
            array_push($contactosasignados, $contacto['Destinatario']['contacto_id']);
        }

        $contactos = $this->Destinatario->Contacto->find("list", [
            'fields' => [
                'Contacto.id',
                'Contacto.nombre_contacto'
            ],
            'conditions'=> [
                'Contacto.activo' => 1,
                'OR' => [
                    'Contacto.id !=' => $contactosasignados,
                    'Contacto.id' => $this->request->data['Destinatario']['contacto_id']
                ]
            ]
        ]);

		$this->set(compact('contactos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Destinatario", "destinatarios", "delete");
		if ($delete == true) {
			$this->Destinatario->id = $id;
			if (!$this->Destinatario->exists()) {
				throw new NotFoundException(__('Invalid destinatario'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Destinatario->delete()) {
					return $this->flash(__('The destinatario has been deleted.'), array('action' => 'index'));
			} else {
				return $this->flash(__('The destinatario could not be deleted. Please, try again.'), array('action' => 'index'));
			}
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Desembolso');
        $this->Desembolso->recursive = -1;
        $destinatario = $this->Desembolso->find('all', [
            'conditions' => [
                'Desembolso.destinatario_id' => $id,
            ]
        ]);

        $data['destinatarios'] = (count($destinatario) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
