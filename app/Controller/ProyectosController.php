<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
/**
 * Proyectos Controller
 *
 * @property Proyecto $Proyecto
 * @property PaginatorComponent $Paginator
 */
class ProyectosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
    var $helpers = array('Html', 'Form');

/**
 * index method
 *
 * @return void
 */
	public function index()
    {
       // $this->ValidarUsuario("Proyecto", "proyectos", "index");
        $user_institu = $this->getInst();
        $perf = $this->get_perfil();

        if($perf==1){
        $this->Paginator->settings = [
            'order' => ['Proyecto.nombrecorto' => 'asc']

        ];
            $institucion = $this->Proyecto->Institucion->find("list",[
                'conditions'=>[
                    'activo'=>1
                ]
            ]);
        }else{
            $this->Paginator->settings = ['conditions' => [
                'Proyecto.institucion_id'   => $user_institu,
                'Proyecto.estado_id != '    => 1
            ],
                'order' => ['Proyecto.nombrecorto' => 'asc']
            ];

            $institucion = $this->Proyecto->Institucion->find("list",[
                'conditions'=>[
                    'activo'=>1,
                    'id'=> $user_institu
                ]
            ]);
        }

        $this->Proyecto->recursive = 0;
        include "busqueda/proyecto.php";
		$data = $this->Paginator->paginate('Proyecto');
		$this->set('proyectos', $data);
		/**LISTAS DESPLEGABLES  **/
		$tipoproyectos = $this->Proyecto->Tipoproyecto->find("list",[
		    'conditions'=>[
		        'activo'=>1
            ]]);

        $paises = $this->Proyecto->Paise->find("list",[
            'conditions'=>[
                'activa'=>1
            ]
        ]);
        $estados = $this->Proyecto->Estado->find("list",[
            'conditions'=>[
                'activo'=>1
            ]
        ]);
        $this->Proyecto->Estado->recursive=-1;

        $infoE = $this->Proyecto->Estado->find("all",[
            'fields'=>[
                'Estado.id','Estado.colorbkg','Estado.colortext','Estado.estado'
            ]
        ]);
        $colores = [];
        foreach ($infoE as $item):
            $colores[$item['Estado']['id']]['background']=$item['Estado']['colorbkg'];
            $colores[$item['Estado']['id']]['color']=$item['Estado']['colortext'];
        endforeach;
		$this->set(compact("tipoproyectos",'paises','estados','colores','institucion','perf'));
	}

    public function get_perfil(){
        $this->loadModel("Perfile");
        $this->Perfile->recursive=-1;
        $user = $this->Session->read("nombreusuario");
        $info = $this->Perfile->find("all",[
            'joins'=>[
            [
                'table'=>'users',
                'alias'=>'User',
                'type'=>'INNER',
                'conditions'=>[
                    "Perfile.id=User.perfile_id"
                ]
            ]
            ],
            'conditions'=>[
                'User.username'=>$user
            ],
            'fields'=>['Perfile.admin']
        ]);
        $band = ($info[0]['Perfile']['admin']==1)?1:0;
        return $band;
    }

	/**METODO PARA OBTENER LA INSTITUCION DEL USUARIO
     SI EL USUARIO NO POSEE INSTITUCION NO MUESTRA NINGUN REGISTRO EN EL INDEX DE PROYECTOS***/
	public function getInst(){
	    $user = $this->Session->read("nombreusuario");
	    $this->loadModel("User");
	    $this->User->recursive=-1;
	    $sql = $this->User->find("all",[
                'joins'=>[
                    [
                    'table'=>"contactos",
                    'alias'=>'Contacto',
                    'type'=>'INNER',
                    'conditions'=>[
                        'User.contacto_id = Contacto.id'
                    ]
                ]
            ],
            'conditions'=>[
                'User.username'=>$user
            ],
            'fields'=>[
                'Contacto.institucion_id',
                'User.id'
            ]
        ]);
	    $var = (count($sql)>0)?$sql[0]['Contacto']['institucion_id']:0;
	    return $var;
    }

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[proyectos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Proyecto", "proyectos", "view");
		$name = $this->Proyecto->read("nombrecompleto",$id);
		$estadoP = $this->Proyecto->read("estado_id",$id);
		$admin = $this->get_perfil();
		if($estadoP['Proyecto']['estado_id']==1 && $admin!=1){
            $this->redirect(['action'=>'index']);
        }
		$this->set(compact("id","name",'estadoP'));
	}

    public function infoproyecto($id){
	    $this->layout=false;
        $options = array('conditions' => array('Proyecto.' . $this->Proyecto->primaryKey => $id));
        $this->set('proyecto', $this->Proyecto->find('first', $options));
        $tipoproyectos = $this->Proyecto->Tipoproyecto->find('list',[
            'conditions'=>[
                'Tipoproyecto.activo'=>1
            ]
        ]);
        $paises = $this->Proyecto->Paise->find('list',[
            'conditions'=>[
                'Paise.activa'=>1
            ]
        ]);
        $estados = $this->Proyecto->Estado->find('list',[
            'conditions'=>[
                'Estado.activo'=>1
            ]
        ]);
        $this->loadModel('Institucion');
        $this->Institucion->recursive=-1;
        $institucion = $this->Institucion->find('list',[
            'conditions'=>[
                'Institucion.activo'=>1
            ]
        ]);
        $this->set(compact('tipoproyectos', 'paises', 'estados','institucion'));
    }
/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Proyecto", "proyectos", "add");
		if ($this->request->is('post')) {
			$this->Proyecto->create();
			if ($this->Proyecto->save($this->request->data)) {
				$this->Session->setFlash(__('The proyecto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proyecto could not be saved. Please, try again.'));
			}
		}
	}

	public function form_add(){
	    $this->layout=false;
        $tipoproyectos = $this->Proyecto->Tipoproyecto->find('list',[
            'conditions'=>[
                'Tipoproyecto.activo'=>1
            ]
        ]);
        $paises = $this->Proyecto->Paise->find('list',[
            'conditions'=>[
                'Paise.activa'=>1
            ]
        ]);
        $estados = $this->Proyecto->Estado->find('list',[
            'conditions'=>[
                'Estado.activo'=>1
            ]
        ]);
        $this->loadModel('Institucion');
        $this->Institucion->recursive=-1;
        $institucion = $this->Institucion->find('list',[
            'conditions'=>[
                'Institucion.activo'=>1
            ]
        ]);
        $this->set(compact('tipoproyectos', 'paises', 'estados','institucion'));
    }

    public function saveproyecto() {
	    /**-----ALMACENAR PROYECTO**/
	    $data = ['nombrecompleto'=>$_POST['ncompleto'],
            'nombrecorto'=>$_POST['ncorto'],
            'codigo'=>$_POST['cod'],
            'tipoproyecto_id'=>$_POST['tproyecto'],
            'paise_id'=>$_POST['pais'],
            'descripcion'=>$_POST['desc'],
            'estado_id'=>$_POST['estado'],
            'institucion_id'=>$_POST['institucion'],
            'desde'=>date("Y-m-d",strtotime($_POST['fdesde'])),
            'hasta'=>date("Y-m-d",strtotime($_POST['fhasta'])),
            'usuario'=>$this->Session->read('nombreusuario'),
            'created'=>date("Y-m-d H:i:s"),
            'modified'=>0
        ];
        $val_cod = $this->Proyecto->find("all",[
            'conditions'=>[
                'Proyecto.codigo'=>$_POST['cod']
            ],
            'fields'=>[
                'Proyecto.id'
            ]
        ]);
	    $bandera = "error";
        if(count($val_cod)>0){
            $bandera = "error";
        }else{
            $this->Proyecto->create();
            if($this->Proyecto->save($data)){
                $bandera = $this->Proyecto->id;
                $data['id'] = $this->Proyecto->id;
                $data['ntipoproyecto'] = $_POST['ntipoproyecto'];
                $data['nestado'] = $_POST['nestado'];
                $data['npais'] = $_POST['npais'];
                $data['ninstitucion'] = $_POST['ninstitucion'];

                $log['data'] = $data;
                $log['accion'] = 'Agregar';
                $log['proyecto_id'] = $this->Proyecto->id;
                $this->Session->write('logsproyecto', $log);

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Proyecto', 'proyectos', 'Datos Generales');
            }
        }

        if($bandera!="error"){
	        $this->Session->write("saveproy",1);
        }
        echo $bandera;
        $this->autoRender = false;
    }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Proyecto", "proyectos", "edit");
        $estadoP = $this->Proyecto->read("estado_id",$id);
        $admin = $this->get_perfil();
		if ($estadoP['Proyecto']['estado_id']==2) {
            if($admin!=1){
                //SI EL PROYECTO ESTA EN EJECUCION Y EL USUARIO NO ES ADMINISTRADOR NO PUEDE EDITARLO
                $this->Session->write("disabled",1);
                $this->redirect(['action'=>'view',$id]);
            }
		}else if($estadoP['Proyecto']['estado_id']==4||$estadoP['Proyecto']['estado_id']==3){
		    //SI EL PROYECTO ESTA CANCELADO NO SE PUEDE EDITAR
            $this->Session->write("disabled",1);
            $this->redirect(['action'=>'view',$id]);
        }
		$this->set(compact('id','admin'));
	}
    public function form_edit($var = null){
        $this->layout=false;
        $vars = explode("-",$var);
        $id = $vars[0];
        $admin = $vars[1];
        $tipoproyectos = $this->Proyecto->Tipoproyecto->find('list',[
            'conditions'=>[
                'Tipoproyecto.activo'=>1
            ]
        ]);
        $paises = $this->Proyecto->Paise->find('list',[
            'conditions'=>[
                'Paise.activa'=>1
            ]
        ]);
        $estados = $this->Proyecto->Estado->find('list',[
            'conditions'=>[
                'Estado.activo'=>1
            ]
        ]);
        $this->loadModel('Institucion');
        $this->Institucion->recursive=-1;
        $institucion = $this->Institucion->find('list',[
            'conditions'=>[
                'Institucion.activo'=>1
            ]
        ]);
        $options = array('conditions' => array('Proyecto.' . $this->Proyecto->primaryKey => $id));
        $this->request->data = $this->Proyecto->find('first', $options);
        $this->set(compact('tipoproyectos', 'paises', 'estados','institucion','admin'));
    }
    public function edproyecto() {
        //$this->Proyecto->recursive = -1;
        $options = array('conditions' => array('Proyecto.' . $this->Proyecto->primaryKey => $_POST['id']));
        $log['anterior'] = $this->Proyecto->find('first', $options);

        /**-----ALMACENAR PROYECTO**/
        $data = ['id'=>$_POST['id'],
            'nombrecompleto'=>$_POST['ncompleto'],
            'nombrecorto'=>$_POST['ncorto'],
            'codigo'=>$_POST['cod'],
            'tipoproyecto_id'=>$_POST['tproyecto'],
            'institucion_id'=>$_POST['institucion'],
            'paise_id'=>$_POST['pais'],
            'descripcion'=>$_POST['desc'],
            'estado_id'=>$_POST['estado'],
            'desde'=>date("Y-m-d",strtotime($_POST['fdesde'])),
            'hasta'=>date("Y-m-d",strtotime($_POST['fhasta'])),
            'usuariomodif'=>$this->Session->read('nombreusuario'),
            'modified'=>date("Y-m-d H:i:s"),
        ];
        $statusOld=$this->Proyecto->read("estado_id",$_POST['id']);

        $info_cod = $this->Proyecto->find("all",[
            'conditions'=>[
                'Proyecto.codigo'=>$_POST['cod'],
                'Proyecto.id != '=>$_POST['id']
            ],
            'fields'=>[
                'Proyecto.id'
            ]
        ]);
        $bandera = "error";
        if(count($info_cod)>0){
            $bandera = "error";
        }else{
            if($this->Proyecto->save($data)){
                $data['ntipoproyecto'] = $_POST['ntipoproyecto'];
                $data['nestado'] = $_POST['nestado'];
                $data['npais'] = $_POST['npais'];
                $data['ninstitucion'] = $_POST['ninstitucion'];

                $bandera = $this->Proyecto->id;
                $log['data'] = $data;
                $log['accion'] = 'Modificar';
                $log['proyecto_id'] = $this->Proyecto->id;
                $this->Session->write('logsproyecto', $log);

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Proyecto', 'proyectos', 'Datos Generales');

                if($statusOld['Proyecto']['estado_id']!=$_POST['estado']){
                    /**TRANSICION POSFUNTION***/
                    $this->loadModel('Transicionpostfunction');
                    $this->loadModel('Postfunction');
                    $this->loadModel('Transicion');
                    $this->Postfunction->recursive=-1;
                    $this->Transicion->recursive=-1;
                    // $this->Transicionpostfunction->recursive=-1;
                    /***GET TRNASITION FOR STATUS****/
                    $transition = $this->Transicion->find('first', [
                        'fields' => [
                            'Transicion.id'
                        ],
                        'conditions' => [
                            'Transicion.estado2_id' => $_POST['estado']
                        ]
                    ]);
                    if(count($transition)>0){
                        /**GET POSTFUNTIONS***/
                        $postFunctions = $this->Transicionpostfunction->find('all', [
                            'fields' => [
                                'Postfunction.controlador',
                                'Postfunction.metodo'
                            ],
                            'conditions' => [
                                'Transicionpostfunction.transicion_id' => $transition['Transicion']['id']
                            ]
                        ]);

                        foreach ($postFunctions as $key => $postFunction) {
                            $controller = $postFunction['Postfunction']['controlador'] . 'Controller';
                            $metodo = $postFunction['Postfunction']['metodo'];
                            App::import('Controller', $postFunction['Postfunction']['controlador']);
                            $controller = new $controller();
                            $controller->$metodo($_POST['id'],$transition['Transicion']['id'],$_POST['cod']);
                        }
                    }
                }
            }
        }
        if($bandera!="error"){
            $this->Session->write("saveproy",1);
        }
        echo $bandera;
        $this->autoRender = false;
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Proyecto", "proyectos", "delete");
		if ($delete == true) {
			$this->Proyecto->id = $id;
			if (!$this->Proyecto->exists()) {
				throw new NotFoundException(__('Invalid proyecto'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Proyecto->delete()) {
					$this->Session->setFlash(__('The proyecto has been deleted.'));
			} else {
				$this->Session->setFlash(__('The proyecto could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function impresion(){
        $this->layout=false;
        $this->Proyecto->recursive = 0;
        $user_institu = $this->getInst();
        $perf = $this->get_perfil();

        if(isset($_SESSION['proyectos'])){
            $data = $this->Proyecto->find('all', $_SESSION['proyectos']);
            if($_SESSION['tabla[proyectos]']['paise_id'] != ''){
                $resultado = $this->Proyecto->Paise->find('first', array('conditions' => array('Paise.id' => $_SESSION['tabla[proyectos]']['paise_id'])));
                $pais = $resultado['Paise']['pais'];
            } else {
                $pais = '';
            }

            if($_SESSION['tabla[proyectos]']['estado_id'] != ''){
                $resultado = $this->Proyecto->Estado->find('first', array('conditions' => array('Estado.id' => $_SESSION['tabla[proyectos]']['estado_id'])));
                $estado = $resultado['Estado']['estado'];
            }else {
                $estado = '';
            }

            if($_SESSION['tabla[proyectos]']['tipoproyecto_id'] != ''){
                $resultado = $this->Proyecto->Tipoproyecto->find('first', array('fields' => ['Tipoproyecto.tipoproyecto'], 'conditions' => array('Tipoproyecto.id' => $_SESSION['tabla[proyectos]']['tipoproyecto_id'])));
                $tipoproyecto = $resultado['Tipoproyecto']['tipoproyecto'];
            } else {
                $tipoproyecto = '';
            }
        } else {
            if($perf==1){
                $data = $this->Proyecto->find('all', [ 'order' => ['Proyecto.nombrecorto' => 'asc'] ]);
            } else {
                $data = $this->Proyecto->find('all', ['conditions'=>[ 'Proyecto.institucion_id'=>$user_institu ], 'order'=>['Proyecto.nombrecorto'=>'asc'] ]);
            }

            $pais = '';
            $estado = '';
            $tipoproyecto = '';
        }

        $this->set('proyectos', $data);

        $this->loadModel('Organizacion');
        $this->Organizacion->recursive = 0;
        $organizacion = $this->Organizacion->find('first', array('fields' => ['Organizacion.organizacion'],'conditions' => array('Organizacion.id' => 1)));
        $org = $organizacion['Organizacion']['organizacion'];

        $this->set(compact('org', 'pais', 'estado', 'tipoproyecto'));
    }
}
