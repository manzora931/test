<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Persona $Persona
 * @property PaginatorComponent $Paginator
 */
class PersonasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Persona", "personas", "index");
		$this->Paginator->settings = array('order'=>array('Persona.nombre'=>'asc', 'Persona.apellido'=>'asc'));
		$this->Persona->recursive = 0;
		include 'busqueda/personas.php';
		$data = $this->Paginator->paginate('Persona');
		$this->set('personas', $data);

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list',[
		    "conditions"=>["Paise.activa"=>1],
            "order"=>["Paise.pais"=>"ASC"]
        ]);
		$this->loadModel("Tipopersona");
		$tipopersonas = $this->Tipopersona->find('list',[
		    "conditions"=>[
		        "Tipopersona.activo"=>1
            ],
            "order"=>["Tipopersona.tipopersonas"=>"ASC"]
        ]);

		$this->set(compact('paises', 'tipopersonas'));
	}

	function vertodos(){
		$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[personas]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Persona", "personas", "view");
		if (!$this->Persona->exists($id)) {
			throw new NotFoundException(__('Invalid persona'));
		}
		$options = array('conditions' => array('Persona.' . $this->Persona->primaryKey => $id));
		$this->set('persona', $this->Persona->find('first', $options));

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipopersona");
		$tipopersonas = $this->Tipopersona->find('list');

		$this->set(compact('paises', 'tipopersonas'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Persona", "personas", "add");
		if ($this->request->is('post')) {
			$fechanacimiento = explode('-', $this->request->data['Persona']['fechanacimiento']);
			$this->request->data['Persona']['fechanacimiento'] = ($this->request->data['Persona']['fechanacimiento'] != '') ? $fechanacimiento[2] . '-' . $fechanacimiento[1] . '-' . $fechanacimiento[0] : null;
			$this->request->data['Persona']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Persona']['modified']=0;
			$this->Persona->create();

			if ($this->Persona->save($this->request->data)) {
				$persona_id = $this->Persona->id;

				$this->Session->write('persona_save', 1);
				$this->redirect(['action' => 'view', $persona_id]);
			} else {
				$this->Session->setFlash(__('La persona no ha sido creada. Intentar de nuevo.'));
			}
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipopersona");
		$tipopersonas = $this->Tipopersona->find('list');

		$this->set(compact('paises', 'tipopersonas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Persona", "personas", "edit");
		if (!$this->Persona->exists($id)) {
			throw new NotFoundException(__('Invalid persona'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$fechanacimiento = explode('-', $this->request->data['Persona']['fechanacimiento']);
			$this->request->data['Persona']['fechanacimiento'] = ($this->request->data['Persona']['fechanacimiento'] != '') ? $fechanacimiento[2] . '-' . $fechanacimiento[1] . '-' . $fechanacimiento[0] : null;
			$this->request->data['Persona']['usuariomodif'] = $this->Session->read('nombreusuario');
			$this->request->data['Persona']['modified'] = date("Y-m-d H:i:s");
			if ($this->Persona->save($this->request->data)) {
				$persona_id = $this->Persona->id;

				$this->Session->write('persona_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('La persona no se ha sido actualizada. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Persona.' . $this->Persona->primaryKey => $id));
			$this->request->data = $this->Persona->find('first', $options);
		}

		$this->loadModel("Paise");
		$paises = $this->Paise->find('list');
		$this->loadModel("Tipopersona");
		$tipopersonas = $this->Tipopersona->find('list');

		$this->set(compact('paises', 'tipopersonas'));
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Persona", "personas", "delete");
		if ($delete == true) {
			$this->Persona->id = $id;
			if (!$this->Persona->exists()) {
				throw new NotFoundException(__('Invalid persona'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Persona->delete()) {
				$this->Session->setFlash(__('The persona has been deleted.'));
			} else {
				$this->Session->setFlash(__('The persona could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}
}
