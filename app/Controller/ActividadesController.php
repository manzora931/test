<?php
App::uses('AppController', 'Controller');
/**
 * Actividades Controller
 *
 * @property Actividade $Actividade
 * @property PaginatorComponent $Paginator
 */
class ActividadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');
    public $helpers = array('Html','Js', 'Form');
/**
 * index method
 *
 * @return void
 */
	public function index($idProyecto=null) {
		$this->Actividade->recursive = 0;
        $this->layout="proyecto";
        $actividadesintervencion = [];
        $actividadesmodulo = [];


        if(isset($_SESSION['logsproyecto'])) {
            unset( $_SESSION['logsproyecto'] );
        }

        $logproyecto = ['proyecto_id' => $idProyecto];
        $_SESSION['logsproyecto'] = $logproyecto;

        /*************************MODULOS****************************/
        $this->loadModel("Module");
        $this->Module->recursive=1;
        $modules = $this->Module->find("all",[
            'conditions'=>[
                'Module.proyecto_id'=>$idProyecto
            ],
            'order'=>[
                'Module.orden'=>"ASC"
            ]
        ]);
        $intervenciones=[];
        foreach ($modules as $row) {
            /** ALMACENA LAS INTERNVENCIONES DE CADA MODULO **/
            $intervenciones[$row['Module']['id']] = $row['Intervencione'];
            $actividadesmodulo[$row['Module']['id']] = 0;
        }

        /*** FOREACH PARA RECORRER INTERVENCIONES ***/
        $actividades=[];
        /**FOREACH PARA RECORRER INTERVENCIONES**/
        foreach ($intervenciones as $rw) {
            foreach ($rw as $item){
                /***ACTIVIDADES POR INTERVENCIONES***/
                $activities = $this->Actividade->find("all",[
                    'conditions'=>[
                        'intervencione_id'=>$item['id']
                    ]
                ]);
                $actividades[$item['id']]=$activities;

                $actividadesintervencion[$item['id']] = count($activities);
                $actividadesmodulo[$item['module_id']]+=$actividadesintervencion[$item['id']];
            }
        }

		$meses = [1=>"Ene",2=>"Feb",3=>"Mar",4=>"Abr",5=>"May",6=>"Jun",7=>"Jul",8=>"Ago",9=>"Sep",10=>"Oct",11=>"Nov",12=>"Dic"];
        $this->loadModel("Financista");
        $this->Financista->recursive=-1;
        $fuentes = $this->Financista->find("all",[
            'joins'=>[
                [
                    'table'=>'fuentesfinanciamientos',
                    'alias'=>'Fuentesfinanciamiento',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Fuentesfinanciamiento.financista_id = Financista.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Fuentesfinanciamiento.proyecto_id'=>$idProyecto
            ],
            'fields'=>['Fuentesfinanciamiento.id','Financista.nombre']
        ]);

        $fuentesfinan = [];
        foreach ($fuentes as $f) {
            $fuentesfinan[$f['Fuentesfinanciamiento']['id']]=$f['Financista']['nombre'];
        }

        /***----------------**/
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive=-1;
        $infoP = $this->Proyecto->read("estado_id",$idProyecto);

        $admin = $this->get_perfil();

        $this->set(compact("idProyecto","modules","intervenciones","actividades","meses",'financistas','fuentes','fuentesfinan','infoP', 'actividadesintervencion', 'actividadesmodulo','admin'));
	}


    public function get_perfil(){
        $this->loadModel("Perfile");
        $this->Perfile->recursive=-1;
        $user = $_SESSION["nombreusuario"];
        $info = $this->Perfile->find("all",[
            'joins'=>[
                [
                    'table'=>'users',
                    'alias'=>'User',
                    'type'=>'INNER',
                    'conditions'=>[
                        "Perfile.id=User.perfile_id"
                    ]
                ]
            ],
            'conditions'=>[
                'User.username'=>$user
            ],
            'fields'=>['Perfile.admin']
        ]);
        $band = ($info[0]['Perfile']['admin']==1)?1:0;
        return $band;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Actividade", "actividades", "view");
		if (!$this->Actividade->exists($id)) {
			throw new NotFoundException(__('Invalid actividade'));
		}
		$options = array('conditions' => array('Actividade.' . $this->Actividade->primaryKey => $id));
		$this->set('actividade', $this->Actividade->find('first', $options));
	}

	public function savemod(){
		$name = $_POST['name'];
		$model = $_POST['model'];
		$this->loadModel($model);
		if($_POST['band']==1){
            $this->$model->recursive=-1;
		    $cont = $this->$model->find("count",[$model.'.proyecto_id'=>$_POST['proy']]);
			$cont++;
			$data=['nombre'=>$name,'orden'=>$cont,'proyecto_id'=>$_POST['proy'],'usuario'=>$this->Session->read('nombreusuario'),'modified'=>0];
			$log =['nombre'=>$name,'orden'=>$cont];
			$tabla = 'modules';
		}else{
		    $this->$model->recursive=-1;
			$cont = $this->$model->find("count",[$model.'.module_id'=>$_POST['proy']]);
			$cont++;
			$data=['nombre'=>$name,'orden'=>$cont,'module_id'=>$_POST['proy'],'usuario'=>$this->Session->read('nombreusuario'),'modified'=>0];
            $log =['nombre'=>$name,'orden'=>$cont,'module_id'=>$_POST['proy']];
            $tabla = 'intervenciones';
		}

		$bandera="";
		if($this->$model->save($data)){
		    $log['id'] = $this->$model->id;
			$bandera=[0=>"exito",1=>$cont];
            $_SESSION['logsproyecto']['accion'] = 'Agregar';
            $_SESSION['logsproyecto']['data'] = $log;
            $_SESSION['alertaAct'] = $model;
            $this->loadModel("Logproyecto");
            $this->Logproyecto->registro_bitacora($model, $tabla, 'Actividades');
		}
		echo json_encode($bandera);
		$this->autoRender=false;
	}
    /**FINALIZAR ACTIVIDAD***/
    public function finalizar(){
        $id = $_POST["idact"];
        $this->Actividade->recursive=-1;
        $info = $this->Actividade->read("finalizado",$id);
        $band = 0;
        if(isset($info['Actividade']['finalizado'])){
            if($info['Actividade']['finalizado']==1){
                $band=1;
            }else{
                $data=['id'=>$id,"finalizado"=>1,'usuariomodif'=>$this->Session->read("nombreusuario"),"modified"=>date("Y-m-d H:i:s")];
                $this->Actividade->save($data);
                $this->Session->write("actFinal",1);
            }
        }
        echo $band;
        $this->autoRender=false;
    }
    /**Activar ACTIVIDAD***/
    public function activar(){
        $id = $_POST["idact"];
        $this->Actividade->recursive=-1;
        $info = $this->Actividade->read("finalizado",$id);
        $band = 0;
        if(isset($info['Actividade']['finalizado'])){
            if($info['Actividade']['finalizado']==0){
                $band=1;
            }else{
                $data=['id'=>$id,"finalizado"=>0,'usuariomodif'=>$this->Session->read("nombreusuario"),"modified"=>date("Y-m-d H:i:s")];
                $this->Actividade->save($data);
                $this->Session->write("actActivar",1);
            }
        }
        echo $band;
        $this->autoRender=false;
    }
    /**ACTUALIZAR MODULO**/
    function updateMod(){
        $this->loadModel("Module");
        $this->Module->recursive=-1;
        $data=['id'=>$_POST['id'],'nombre'=>$_POST['mod'],'usuariomodif'=>$this->Session->read('nombreusuario'),'modified'=>date("Y-m-d H:i:s")];
        $band =1;
        if($this->Module->save($data)){
            $band=0;
            $this->Session->write("alertaAct","Module");
        }
        echo $band;
        $this->autoRender=false;
    }
    function updateInt(){
        $this->loadModel("Intervencione");
        $this->Intervencione->recursive=-1;
        $data=['id'=>$_POST['id'],'nombre'=>$_POST['inter'],'usuariomodif'=>$this->Session->read('nombreusuario'),'modified'=>date("Y-m-d H:i:s")];
        $band =1;
        if($this->Intervencione->save($data)){
            $band=0;
            $this->Session->write("alertaAct","Intervencione");
        }
        echo $band;
        $this->autoRender=false;
    }

/**
 * add method
 *
 * @return void
 */
/**
* metodo para mostrar tareas y subtareas relacionadas a una actividad
 */
    public function showt($id=null){
        $this->layout="proyecto";
        $this->loadModel("Tarea");
        $tareas = $this->Tarea->find("all",[
            'conditions'=>["Tarea.actividade_id"=>$id],
            'fields'=>["Tarea.nombre","Tarea.created","Tarea.usuario","Tarea.id"],
            'order'=>["Tarea.nombre"]
        ]);
        $subtareas=[];
        foreach ($tareas as $sub){
            $this->loadModel("Subtarea");
            $info = $this->Subtarea->find("all",[
                'conditions'=>["Subtarea.tarea_id"=>$sub["Tarea"]['id']],
                'fields'=>["Subtarea.nombre",'Subtarea.id'],
                'order'=>["Subtarea.nombre"]
            ]);
            $subtareas[$sub['Tarea']["id"]]=$info;
        }
        $this->set(compact("tareas","subtareas"));
    }
    public function add($idProyecto){
        $this->layout=false;
        $this->loadModel("Paise");
        $paises = $this->Paise->find("list",[
            'conditions'=>[
                'Paise.activa'=>1
            ]
        ]);
        $this->loadModel("Intervencione");
        $this->Intervencione->recursive=-1;
        $interdata = $this->Intervencione->find("list",[
            'joins'=>[
                [
                    'table'=>'modules',
                    'alias'=>'Modules',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Intervencione.module_id = Modules.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Modules.proyecto_id'=>$idProyecto
            ]
        ]);
        $this->loadModel("Financista");
        $this->Financista->recursive=-1;
        $fuentes = $this->Financista->find("all",[
            'joins'=>[
                [
                    'table'=>'fuentesfinanciamientos',
                    'alias'=>'Fuentesfinanciamiento',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Fuentesfinanciamiento.financista_id = Financista.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Fuentesfinanciamiento.proyecto_id'=>$idProyecto
            ],
            'fields'=>['Fuentesfinanciamiento.id','Financista.nombre']
        ]);
        //debug($fuentes);
        $fuentesfinan = [];

        foreach ($fuentes as $f) {
            $fuentesfinan[$f['Fuentesfinanciamiento']['id']]=$f['Financista']['nombre'];
        }
        $this->set(compact("fuentesfinan","paises",'interdata'));
    }

	public function save_actividad() {
	    $finicio = date("Y-m-d",strtotime($_POST['finicio']));
	    $flimite = date("Y-m-d",strtotime($_POST['flimite']));
	    $user = $this->Session->read("nombreusuario");
	    if($_POST['id']>0){
            $options = array('conditions' => array('Actividade.' . $this->Actividade->primaryKey => $_POST['id']));
            $_SESSION['logsproyecto']['anterior'] = $this->Actividade->find('first', $options);

            $data=['id'=>$_POST['id'],'nombre'=>$_POST['actividad'],"inicio"=>$finicio,'limite'=>$flimite,"paise_id"=>$_POST['apais_id'],'fuentesfinanciamiento_id'=>$_POST['ffinanciamiento_id'],'intervencione_id'=>$_POST['intervencion_id'],'finalizado'=>$_POST['afinalizado'],'usuario'=>$user];
            $accion = 'Modificar';
        }else{
            $data=['nombre'=>$_POST['actividad'],"inicio"=>$finicio,'limite'=>$flimite,"paise_id"=>$_POST['apais_id'],'fuentesfinanciamiento_id'=>$_POST['ffinanciamiento_id'],'intervencione_id'=>$_POST['intervencion_id'],'finalizado'=>$_POST['afinalizado'],'usuario'=>$user];
            $accion = 'Agregar';
        }
        $bandera="error";
        if ($this->Actividade->save($data)) {
            $this->Session->write("saveact",1);
            $data['id'] = $this->Actividade->id;
            $data['npais'] = $_POST['npais'];
            $data['nintervencion'] = $_POST['nintervencion'];
            $data['ffinanciamiento'] = $_POST['ffinanciamiento'];
            $bandera="exito";

            $_SESSION['logsproyecto']['accion'] = $accion;
            $_SESSION['logsproyecto']['data'] = $data;

            $this->loadModel("Logproyecto");
            $this->Logproyecto->registro_bitacora('Actividade', 'actividades', 'Actividades');
        }
        echo $bandera;
        $this->autoRender=false;
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->layout=false;
        $ar = explode("-",$id);
        $idProyecto = $ar[1];
        $options = array('conditions' => array('Actividade.' . $this->Actividade->primaryKey => $ar[0]));
        $this->request->data = $this->Actividade->find('first', $options);
		$paises = $this->Actividade->Paise->find('list');
        $this->loadModel("Financista");
        $this->Financista->recursive=-1;
        $fuentes = $this->Financista->find("all",[
            'joins'=>[
                [
                    'table'=>'fuentesfinanciamientos',
                    'alias'=>'Fuentesfinanciamiento',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Fuentesfinanciamiento.financista_id = Financista.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Fuentesfinanciamiento.proyecto_id'=>$idProyecto
            ],
            'fields'=>['Fuentesfinanciamiento.id','Financista.nombre']
        ]);
        //debug($fuentes);
        $fuentesfinan = [];

        foreach ($fuentes as $f) {
            $fuentesfinan[$f['Fuentesfinanciamiento']['id']]=$f['Financista']['nombre'];
        }

        $this->loadModel("Intervencione");
        $this->Intervencione->recursive=-1;
        $interdata = $this->Intervencione->find("list",[
            'joins'=>[
                [
                    'table'=>'modules',
                    'alias'=>'Modules',
                    'type'=>'INNER',
                    'conditions'=>[
                        'Intervencione.module_id = Modules.id'
                    ]
                ]
            ],
            'conditions'=>[
                'Modules.proyecto_id'=>$idProyecto
            ]
        ]);
		$this->set(compact('paises', 'fuentesfinan', 'interdata'));
	}

	/**DELETE DE MODULOS**/
	function deleteMod(){
	    $this->loadModel("Intervencione");
	    $this->Intervencione->recursive=-1;
	    $intervenciones=[];
	    $activities=[];


	    $inters = $this->Intervencione->find("all",[
	        'conditions'=>[
	            'Intervencione.module_id'=>$_POST['id']
            ],
            'fields'=>[
                'Intervencione.id',
                'Intervencione.nombre'
            ]
        ]);
        $intervenciones = $inters;
	    $this->Actividade->recursive=-1;
	    $bandera=0;
	    foreach ($inters as $inter){
	        $idInter = $inter['Intervencione']['id'];
	        $actividades = $this->Actividade->find("all",[
	            'conditions'=>[
	                'Actividade.intervencione_id'=>$idInter
                ],
                'fields'=>[
                    'Actividade.id',
                    'Actividade.nombre'
                ]
            ]);
            $activities = $actividades;
	        if(count($actividades)>0){
	            $this->loadModel("Gasto");
                $this->Gasto->recursive=-1;
                foreach ($actividades as $act){
                    $ActividadId = $act['Actividade']['id'];
                    $gastos = $this->Gasto->find("all",[
                        'conditions'=>[
                            'Gasto.actividade_id'=>$ActividadId
                        ],
                        'fields'=>[
                            'Gasto.id', 'Gasto.actividade_id'
                        ]
                    ]);
                    if(count($gastos)>0){
                        $bandera=1;
                        break;
                    }

                }
            }
            if($bandera>0){
                break;
            }
        }
        if($bandera==0){
            foreach ($intervenciones as $inter) {
                $idInter = $inter['Intervencione']['id'];
                $actividades = $this->Actividade->find("all", [
                    'conditions' => [
                        'Actividade.intervencione_id' => $idInter
                    ],
                    'fields' => [
                        'Actividade.id',
                        'Actividade.nombre'
                    ]
                ]);
                foreach ($actividades as $act){
                    $this->loadModel("Presupuesto");
                    $this->Presupuesto->recursive=-1;
                    $presp = $this->Presupuesto->find("all",[
                        'conditions'=>[
                            'Presupuesto.actividade_id'=>$act['Actividade']['id']
                        ],
                        'fields'=>[
                            'Presupuesto.id'
                        ]
                    ]);
                    foreach ($presp as $pre){
                        $this->Presupuesto->delete($pre['Presupuesto']['id']);
                    }
                    $this->loadModel("Gasto");
                    $this->Gasto->recursive=-1;
                    $gastos = $this->Gasto->find("all",[
                        'conditions'=>[
                            'Gasto.actividade_id'=>$act['Actividade']['id']
                        ],
                        'fields'=>[
                            'Gasto.id', 'Gasto.actividade_id'
                        ]
                    ]);
                    foreach ($gastos as $gasto){
                        $this->Gasto->delete($gasto['Gasto']['id']);
                    }
                    $this->Actividade->delete($act['Actividade']['id']);
                }
                $this->Intervencione->delete($idInter);
            }
            $this->loadModel("Module");
            $this->Module->delete($_POST['id']);
            $this->Session->write("deleteMod","Modulo");
        }
        echo $bandera;
        $this->autoRender=false;
    }
    function deleteInt(){
        $this->loadModel("Intervencione");
        $this->Intervencione->recursive=-1;

        $activities=[];

        $this->Actividade->recursive=-1;
        $bandera=0;
        $actividades = $this->Actividade->find("all",[
            'conditions'=>[
                'Actividade.intervencione_id'=>$_POST['id']
            ],
            'fields'=>[
                'Actividade.id',
                'Actividade.nombre'
            ]
        ]);
        $activities = $actividades;
        if(count($actividades)>0){
            $this->loadModel("Gasto");
            $this->Gasto->recursive=-1;
            foreach ($actividades as $act){
                $ActividadId = $act['Actividade']['id'];
                $gastos = $this->Gasto->find("all",[
                    'conditions'=>[
                        'Gasto.actividade_id'=>$ActividadId
                    ],
                    'fields'=>[
                        'Gasto.id', 'Gasto.actividade_id'
                    ]
                ]);
                if(count($gastos)>0){
                    $bandera=1;
                    break;
                }
            }
        }
        if($bandera==0){
            foreach ($activities as $act){
                $this->loadModel("Presupuesto");
                $this->Presupuesto->recursive=-1;
                $presp = $this->Presupuesto->find("all",[
                    'conditions'=>[
                        'Presupuesto.actividade_id'=>$act['Actividade']['id']
                    ],
                    'fields'=>[
                        'Presupuesto.id'
                    ]
                ]);
                foreach ($presp as $pre){
                    $this->Presupuesto->delete($pre['Presupuesto']['id']);
                }
                $this->loadModel("Gasto");
                $this->Gasto->recursive=-1;
                $gastos = $this->Gasto->find("all",[
                    'conditions'=>[
                        'Gasto.actividade_id'=>$act['Actividade']['id']
                    ],
                    'fields'=>[
                        'Gasto.id', 'Gasto.actividade_id'
                    ]
                ]);
                foreach ($gastos as $gasto){
                    $this->Gasto->delete($gasto['Gasto']['id']);
                }
                $this->Actividade->delete($act['Actividade']['id']);
            }
            $this->Intervencione->delete($_POST['id']);
            $this->Session->write("deleteMod","Intervencione");
        }
        echo $bandera;
        $this->autoRender=false;
    }

    /**VALIDACIONES PARA BORRAR ACTIVIDADES***/
    public function valDelete($id = null){
        /**VERIFICA QUE LA ACTIVIDAD NO ESTE SIENDO UTILIZADA EN PRESUPUESTO**/
        $this->loadModel("Presupuesto");
        $this->Presupuesto->recursive=-1;
        $bandera=0;
        $info = $this->Presupuesto->find("all",[
            'conditions'=>[
                'Presupuesto.actividade_id'=>$id
            ],
            'fields'=>[
                'Presupuesto.id'
            ]
        ]);
        if(count($info)>0){
            $bandera=1;
        }else{
            $this->loadModel("Gasto");
            $this->Gasto->recursive=-1;
            $infoG = $this->Gasto->find("all",[
                'conditions'=>[
                    'Gasto.actividade_id'=>$id
                ],
                'fields'=>[
                    'Gasto.id'
                ]
            ]);
            if(count($infoG)>0){
                $bandera=2;
            }else{
                $this->loadModel("Incidencia");
                $this->Incidencia->recursive=-1;
                $infoI = $this->Incidencia->find("all",[
                    'conditions'=>[
                        'Incidencia.actividade_id'=>$id
                    ],
                    'fields'=>[
                        'Incidencia.id'
                    ]
                ]);
                if(count($infoI)>0){
                    $bandera=3;
                }
            }
        }
        return $bandera;
    }

	public function delete() {
        $delete = $this->ValidarUsuario("Actividade", "proyectos", "delete");
        $id = $_POST['idact'];
        /***
        ERRORES SEGUN LA BANDERA
         * 1 => LA ACTIVIDAD TIENE PRESUPUESTO
         * 2 => LA ACTIVIDAD TIENE GASTOS ASIGNADOS
         * 3 => LA ACTIVIDAD TIENE INCIDENCIAS REGISTRADAS
         * 4 => NO TIENE PERMISO DE BORRAR
         ***/
		if ($delete == true) {
            $val = $this->valDelete($id);
            if($val==0){
                $this->Actividade->recursive = -1;
                $actividad = $this->Actividade->find('first', [
                    'fields' => ['Actividade.*', 'Financista.nombre', 'Intervencione.nombre', 'Paise.pais'],
                    'joins' => array(
                        array('table' => 'fuentesfinanciamientos', 'alias' => 'Fuentesfinanciamiento', 'type' => 'INNER', 'conditions' => array('Actividade.fuentesfinanciamiento_id = Fuentesfinanciamiento.id')),
                        array('table' => 'financistas', 'alias' => 'Financista', 'type' => 'INNER', 'conditions' => array('Fuentesfinanciamiento.financista_id = Financista.id')),
                        array('table' => 'intervenciones', 'alias' => 'Intervencione', 'type' => 'INNER', 'conditions' => array('Actividade.intervencione_id = Intervencione.id')),
                        array('table' => 'paises', 'alias' => 'Paise', 'type' => 'INNER', 'conditions' => array('Actividade.paise_id = Paise.id'))
                    ),
                    'conditions' => ['Actividade.id' => $id]
                ]);
                $this->Actividade->delete($id);
                $this->Session->write("deleteAct",1);

                $_SESSION['logsproyecto']['accion'] = 'Eliminar';
                $_SESSION['logsproyecto']['data'] = $actividad['Actividade'];
                $_SESSION['logsproyecto']['data']['npais'] = $actividad['Paise']['pais'];
                $_SESSION['logsproyecto']['data']['nintervencion'] = $actividad['Intervencione']['nombre'];
                $_SESSION['logsproyecto']['data']['ffinanciamiento'] = $actividad['Financista']['nombre'];

                $this->loadModel("Logproyecto");
                $this->Logproyecto->registro_bitacora('Actividade', 'actividades', 'Actividades');
            }
		}else{
		    $val=4;
        }
        echo $val;
		$this->autoRender=false;
	}
}
