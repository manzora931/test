<?php
class PublicsController extends AppController{

    public $components = array('Paginator');//agregado
    var $name = 'Publics';
    var $helpers = array('Html', 'Form');
    /***
     * author: Manuel Anzora
     * create: 26-03-2019
     * description: Metodo para definir que un metodo es publico(no requiere autenticacion de usuario)
     ****/
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('posiciones','resultados','proximosJuegos',"index","listJuegos","validateDomain");
    }
    /***
     * Author: Manuel Anzora
     * created: 26-03-2019
     * description: Metodo para mostrar tabla de posiciones, el contenido es filtrado mediante el torneo
     * params: recibe como parametro el id del torneo que se desea consultar y el numero de posisciones que
     * a mostrar, ejemplo: 3(mostrar las primeras 3 posiciones)
     **/
    public function posiciones($torneo_id=null, $numposiciones=null, $host=null){
        $this->layout="public";
        $error = true;
        $torneo=[];
        $tabposisions=[];
        $valid=0;
        if($host!=null){
            $this->loadModel("Institucion");
            $this->Institucion->recursive=-1;
            $instituciones = $this->Institucion->find("all",[
                "fields"=>["url1", "url2", "url3"],
                "conditions"=>[
                    "Institucion.activo" => 1
                ]
            ]);
            $band = false;
            $url1="";
            $url2="";
            $url3="";
            foreach ($instituciones as $institucion){

                $url1 = strpos($institucion["Institucion"]["url1"],$host);
                $url2 = strpos($institucion["Institucion"]["url2"],$host);
                $url3 = strpos($institucion["Institucion"]["url3"],$host);
                if($url1!=false || $url2!=false || $url3 != false){
                    $valid=1;
                    break;
                }
            }
        }
        if($torneo_id!=null || $numposiciones!=null){
            $error=false;
            $this->loadModel("Vposesionbalon");
            $this->loadModel("Torneo");
            $tabposisions = $this->Vposesionbalon->query("select x.torneo_id, x.equipo_id, x.nombrecorto, x.fotoescudo, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
        from (
        select z.torneo_id, z.equipo_id, z.nombrecorto,z.fotoescudo, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
        from (
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 > j.marcadore2
        UNION ALL 
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore2 > j.marcadore1
        UNION ALL
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 = j.marcadore2
        UNION ALL
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore1 = j.marcadore2    
        UNION ALL
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 < j.marcadore2
        UNION ALL
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore1 > j.marcadore2      
        ) z 
        where z.torneo_id = ".$torneo_id."
        group by z.torneo_id, z.equipo_id, z.nombrecorto
        ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
        order by puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc LIMIT ".$numposiciones);

            $this->Torneo->recursive=-1;
            $torneo = $this->Torneo->find("first",[
                "fields"=>["torneo"],
                "conditions"=>[
                    "id"=>$torneo_id
                ]
            ]);
            $this->set(compact("tabposisions","torneo","error","valid","torneo_id","numposiciones","host"));
        }else{
            //Mostrar mensaje de advertencia
            $this->set(compact("tabposisions","torneo", "error","valid","torneo_id","numposiciones","host"));
        }

    }
    public function resultados($torneo_id=null, $numposiciones=null, $host=null){
        $this->loadModel("Juego");
        $this->loadModel("Torneo");
        $this->layout="public";
        $error = true;
        $torneo=[];
        $resultados=[];
        $valid=0;
        if($host!=null){
            $this->loadModel("Institucion");
            $this->Institucion->recursive=-1;
            $instituciones = $this->Institucion->find("all",[
                "fields"=>["url1", "url2", "url3"],
                "conditions"=>[
                    "Institucion.activo" => 1
                ]
            ]);
            $band = false;
            $url1="";
            $url2="";
            $url3="";
            foreach ($instituciones as $institucion){

                $url1 = strpos($institucion["Institucion"]["url1"],$host);
                $url2 = strpos($institucion["Institucion"]["url2"],$host);
                $url3 = strpos($institucion["Institucion"]["url3"],$host);
                if($url1!=false || $url2!=false || $url3 != false){
                    $valid=1;
                    break;
                }
            }
        }
        if($torneo_id!=null || $numposiciones!=null){
            $error=false;
            $resultados = $this->Juego->find("all",[
                "conditions"=>[
                    "torneo_id"=>$torneo_id,
                    "marcadore1 !="=>null,
                    "marcadore2 !="=>null
                ],
                "order"=>[
                    "fecha"=>"DESC"
                ],
                "limit"=>$numposiciones
            ]);
            $this->Torneo->recursive=-1;
            $torneo = $this->Torneo->find("first",[
                "fields"=>["torneo"],
                "conditions"=>[
                    "id"=>$torneo_id
                ]
            ]);
            $this->set(compact("resultados","torneo", "error","valid","torneo_id","numposiciones","host"));
        }else{
            //Mostrar mensaje de advertencia
            $this->set(compact("resultados","torneo", "error","valid","torneo_id","numposiciones","host"));
        }

    }
    public function proximosJuegos($torneo_id=null, $numposiciones=null, $host=null){
        $this->loadModel("Juego");
        $this->loadModel("Torneo");
        $this->layout="public";
        $error = true;
        $torneo=[];
        $proximos=[];
        $valid=0;
        if($host!=null){
            $this->loadModel("Institucion");
            $this->Institucion->recursive=-1;
            $instituciones = $this->Institucion->find("all",[
                "fields"=>["url1", "url2", "url3"],
                "conditions"=>[
                    "Institucion.activo" => 1
                ]
            ]);
            $band = false;
            $url1="";
            $url2="";
            $url3="";
            foreach ($instituciones as $institucion){

                $url1 = strpos($institucion["Institucion"]["url1"],$host);
                $url2 = strpos($institucion["Institucion"]["url2"],$host);
                $url3 = strpos($institucion["Institucion"]["url3"],$host);
                if($url1!=false || $url2!=false || $url3 != false){
                    $valid=1;
                    break;
                }
            }
        }

        if($torneo_id!=null || $numposiciones!=null){
            $error=false;
            $proximos = $this->Juego->find("all",[
                "conditions"=>[
                    "torneo_id"=>$torneo_id,
                    "marcadore1"=>null,
                    "marcadore2"=>null
                ],
                "order"=>[
                    "fecha"=>"DESC"
                ],
                "limit"=>$numposiciones
            ]);
            $this->Torneo->recursive=-1;
            $torneo = $this->Torneo->find("first",[
                "fields"=>["torneo"],
                "conditions"=>[
                    "id"=>$torneo_id
                ]
            ]);
            $this->set(compact("proximos","torneo", "error","torneo_id","numposiciones","valid","host"));
        }else{
            //Mostrar mensaje de advertencia
            $this->set(compact("proximos","torneo", "error","torneo_id","numposiciones","valid","host"));
        }

    }
    /***
     * Author: Manuel Anzora
     * created: 31-03-2019
     * description: Metodo para mostrar tabla de posiciones, ultimos resultados y para acceder al listado de juegos y el view
     * params: recibe como parametro el id del torneo que se desea consultar
     **/
    public function index($torneo_id,$number=10,$host=null){
        $this->layout="public";
        $error = true;
        $torneo=[];
        $tabposisions=[];
        $valid=0;
        if($host!=null){
            $this->loadModel("Institucion");
            $this->Institucion->recursive=-1;
            $instituciones = $this->Institucion->find("all",[
                "fields"=>["url1", "url2", "url3"],
                "conditions"=>[
                    "Institucion.activo" => 1
                ]
            ]);
            $band = false;
            $url1="";
            $url2="";
            $url3="";
            foreach ($instituciones as $institucion){

                $url1 = strpos($institucion["Institucion"]["url1"],$host);
                $url2 = strpos($institucion["Institucion"]["url2"],$host);
                $url3 = strpos($institucion["Institucion"]["url3"],$host);
                if($url1!=false || $url2!=false || $url3 != false){
                    $valid=1;
                    break;
                }
            }
        }
        if($torneo_id!=null){
            $error=false;
            $this->loadModel("Vposesionbalon");
            $this->loadModel("Juego");
            $this->loadModel("Torneo");
            ///Query para tabla de posiciones
            $tabposisions = $this->Vposesionbalon->query("select x.torneo_id, x.equipo_id, x.nombrecorto, x.fotoescudo, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
        from (
        select z.torneo_id, z.equipo_id, z.nombrecorto,z.fotoescudo, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
        from (
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 > j.marcadore2
        UNION ALL 
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore2 > j.marcadore1
        UNION ALL
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 = j.marcadore2
        UNION ALL
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore1 = j.marcadore2    
        UNION ALL
        select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo1_id
        and j.marcadore1 < j.marcadore2
        UNION ALL
        select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto,e.fotoescudo, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
        from juegos j, equipos e 
        where e.id = j.equipo2_id
        and j.marcadore1 > j.marcadore2      
        ) z 
        where z.torneo_id = ".$torneo_id."
        group by z.torneo_id, z.equipo_id, z.nombrecorto
        ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
        order by puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc");
        //Query para ultimos resultados
            $resultados = $this->Juego->find("all",[
                "conditions"=>[
                    "torneo_id"=>$torneo_id,
                    "marcadore1 !="=>null,
                    "marcadore2 !="=>null
                ],
                "order"=>[
                    "fecha"=>"DESC"
                ],
                "limit"=>$number
            ]);
            //Query para obtener los proximos partidos
            $proximos = $this->Juego->find("all",[
                "conditions"=>[
                    "torneo_id"=>$torneo_id,
                    "marcadore1"=>null,
                    "marcadore2"=>null
                ],
                "order"=>[
                    "fecha"=>"DESC"
                ],
                "limit"=>$number
            ]);
            $this->Torneo->recursive=-1;
            $torneo = $this->Torneo->find("first",[
                "fields"=>["torneo"],
                "conditions"=>[
                    "id"=>$torneo_id
                ]
            ]);

            $this->set(compact("tabposisions","torneo","error","resultados","proximos","torneo_id","valid","host","number"));
        }else{
            //Mostrar mensaje de advertencia
            $this->set(compact("tabposisions","torneo", "error","valid","host","number"));
        }
    }

    /***
     * Author: Manuel Anzora
     * created: 01-04-2019
     * description: Validacion del dominio de donde es llamado el iframe
     **/
    public function validateDomain(){
        $this->autoRender=false;
        $url = $_POST["ruta"];
        $this->loadModel("Institucion");
        $instituciones = $this->Institucion->find("all",[
            "fields"=>["url1", "url2", "url3"],
            "conditions"=>[
                "Institucion.activo" => 1
            ]
        ]);
        $band = false;
        $url1="";
        $url2="";
        $url3="";
        foreach ($instituciones as $institucion){
            $url1 = strpos($url, $institucion["Institucion"]["url1"]);
            $url2 = strpos($url, $institucion["Institucion"]["url2"]);
            $url3 = strpos($url, $institucion["Institucion"]["url3"]);
            if($url1==true || $url2==true || $url3 == true){
                $band=true;
                break;
            }
        }
        echo $band;
    }
}