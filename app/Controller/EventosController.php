<?php
App::uses('AppController', 'Controller');
/**
 * Eventos Controller
 *
 * @property Evento $Evento
 * @property PaginatorComponent $Paginator
 */
class EventosController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $helpers = array('Html','Js', 'Form');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Evento", "eventos", "index");
        $this->Paginator->settings = array('conditions' => array('Evento.activo >=' => 1), 'order'=>array('Evento.evento'=>'asc'));
        $this->Evento->recursive = 0;
        include 'busqueda/eventos.php';
        $data = $this->Paginator->paginate('Evento');
        $this->set('eventos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[eventos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Evento", "eventos", "view");
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid evento'));
		}
		$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
		$this->set('evento', $this->Evento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Evento", "eventos", "add");
		if ($this->request->is('post')) {
			$this->Evento->create();
            if(isset($this->data["Evento"]["img"]['name']) && $this->data["Evento"]["img"]['name']!='') {
                $file = new File($this->request->data["Evento"]["img"]['tmp_name']);
            }
            $this->request->data['Evento']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Evento']['modified']=0;
			if ($this->Evento->save($this->request->data)) {
				$evento_id = $this->Evento->id;
				if(isset($this->data["Evento"]["img"]['name']) && $this->data["Evento"]["img"]['name']!=''){
                    $formato = explode('.', $this->data["Evento"]["img"]['name']);
                    $filename = $formato[0]."_".$evento_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/eventos/' . $filename;
                    $sql_Uprod = "UPDATE eventos SET imagen='$url' WHERE id='$evento_id'";
                    $result = $this->Evento->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/eventos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                $this->Session->write('evento_save', 1);
                $this->redirect(['action' => 'view', $evento_id]);
			} else {
				$this->Session->setFlash(__('The evento could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Evento", "eventos", "edit");
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid evento'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Evento']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Evento->save($this->request->data)) {
                $this->Session->write('evento_save', 1);
                if(isset($this->data["Evento"]["img"]['name']) && $this->data["Evento"]["img"]['name']!='') {
                    $file = new File($this->data["Evento"]["img"]['tmp_name']);
                    $formato = explode('.', $this->data["Evento"]["img"]['name']);
                    $filename = $formato[0]."_".$id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/eventos/' . $filename;
                    $sql_Uprod = "UPDATE eventos SET imagen='$url' WHERE id='$id'";
                    $result = $this->Evento->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/eventos/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The evento could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
			$this->request->data = $this->Evento->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Evento", "eventos", "delete");
		if ($delete == true) {
			$this->Evento->id = $id;
			if (!$this->Evento->exists()) {
				throw new NotFoundException(__('Invalid evento'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Evento->delete()) {
					$this->Session->setFlash(__('The evento has been deleted.'));
			} else {
				$this->Session->setFlash(__('The evento could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_eventos() {
        $id = $_POST['id'];
        $data = array();

        // $this->loadModel('Detevento');
        $this->Evento->Detevento->recursive = -1;
		$evento = $this->Evento->Detevento->find('all', [
            'conditions' => [
                'Detevento.evento_id' => $id,
            ]
        ]);

        $data['eventos'] = (count($evento) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
    public function fotoOrig($id){
        $this->layout=false;
        $options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
        $this->set('evento', $this->Evento->find('first', $options));
    }
}
