<?php
App::uses('AppController', 'Controller');
/**
 * Generoincidencias Controller
 *
 * @property Generoincidencia $Generoincidencia
 * @property PaginatorComponent $Paginator
 */
class GeneroincidenciasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Generoincidencia", "generoincidencias", "index");
		$this->Generoincidencia->recursive = 0;
		$this->set('generoincidencias', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Generoincidencia", "generoincidencias", "view");
		if (!$this->Generoincidencia->exists($id)) {
			throw new NotFoundException(__('Invalid generoincidencia'));
		}
		$options = array('conditions' => array('Generoincidencia.' . $this->Generoincidencia->primaryKey => $id));
		$this->set('generoincidencia', $this->Generoincidencia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Generoincidencia", "generoincidencias", "add");
		if ($this->request->is('post')) {
			$this->Generoincidencia->create();
			if ($this->Generoincidencia->save($this->request->data)) {
				$this->Session->setFlash(__('The generoincidencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generoincidencia could not be saved. Please, try again.'));
			}
		}
		$generos = $this->Generoincidencia->Genero->find('list');
		$incidencias = $this->Generoincidencia->Incidencium->find('list');
		$this->set(compact('generos', 'incidencias'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Generoincidencia", "generoincidencias", "edit");
		if (!$this->Generoincidencia->exists($id)) {
			throw new NotFoundException(__('Invalid generoincidencia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Generoincidencia->save($this->request->data)) {
				$this->Session->setFlash(__('The generoincidencia has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The generoincidencia could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Generoincidencia.' . $this->Generoincidencia->primaryKey => $id));
			$this->request->data = $this->Generoincidencia->find('first', $options);
		}
		$generos = $this->Generoincidencia->Genero->find('list');
		$incidencias = $this->Generoincidencia->Incidencium->find('list');
		$this->set(compact('generos', 'incidencias'));
	}

    public function delete() {
        $this->Generoincidencia->id = $_POST['id'];

        if (!$this->Generoincidencia->exists()) {
            $resp['msg'] = 'error1';
        } else {
            if ($this->Generoincidencia->delete()) {
                $resp['msg'] = 'exito';
            } else {
                $resp['msg'] = 'error2';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }
}
