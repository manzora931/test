<?php
App::uses('AppController', 'Controller');
/**
 * Estadios Controller
 *
 * @property Estadio $Estadio
 * @property PaginatorComponent $Paginator
 */
class EstadiosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	//var $components = array('Paginator', 'Session');
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
       	$this->ValidarUsuario('Estadio', 'estadios', 'index');

        $this->Paginator->settings = array('order'=> array('Estadio.estadio'=>'asc'));
        $this->Estadio->recursive = 0;
        include 'busqueda/estadios.php';
        $data = $this->Paginator->paginate('Estadio');
        $this->set('estadios', $data);

        $paises = $this->Estadio->Paise->find('list');
        $this->set(compact('paises'));
    }

    public function vertodos()
    {
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[recursos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
    public function fotoOrig($id){
        $this->layout=false;
        $options = array('conditions' => array('Estadio.' . $this->Estadio->primaryKey => $id));
        $this->set('estadio', $this->Estadio->find('first', $options));
    }
	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		$this->ValidarUsuario('Estadio', 'estadios', 'view');
		if (!$this->Estadio->exists($id)) {
			throw new NotFoundException(__('Invalid estadio'));
		}
		$options = array('conditions' => array('Estadio.' . $this->Estadio->primaryKey => $id));
		$this->set('estadio', $this->Estadio->find('first', $options));

		$paises=$this->Estadio->Paise->find("list");

        $this->set(compact("paises"));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$this->ValidarUsuario('Estadio', 'estadios', 'add');
		if ($this->request->is('post')) {
			$this->Estadio->create();
			$this->request->data['Estadio']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Estadio']['modified'] = 0;
            $file = new File($this->request->data["Estadio"]["fotoes"]['tmp_name']);
			if ($this->Estadio->save($this->request->data)) {
            	$estadio_id = $this->Estadio->id;
                if(isset($this->data["Estadio"]["fotoes"]['name']) && $this->data["Estadio"]["fotoes"]['name']!='') {
                    $formato = explode('.', $this->data["Estadio"]["fotoes"]['name']);
                    $filename = $formato[0]."_".$estadio_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/estadios/' . $filename;
                    $sql_Uprod = "UPDATE estadios SET foto='$url' WHERE id='$estadio_id'";
                    $result = $this->Estadio->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/estadios/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                $this->Session->write("estadio_save",1);
                $this->redirect(array('action' => 'view',$estadio_id));

			} else {
				$this->Flash(__('El estadio NO ha sido almacenado. Intentar de nuevo.'));
			}
		}

		$paises = $this->Estadio->Paise->find('list');

		$this->set(compact('paises'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario('Estadio', 'estadios', 'edit');
		if (!$this->Estadio->exists($id)) {
			throw new NotFoundException(__('Estadio Inválido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Estadio']['usuariomodif'] = $this->Session->read('nombreusuario');

			if ($this->Estadio->save($this->request->data)) {
                $estadio_id = $this->Estadio->id;
                if(isset($this->data["Estadio"]["fotoes"]['name']) && $this->data["Estadio"]["fotoes"]['name']!='') {
                    $file = new File($this->request->data["Estadio"]["fotoes"]['tmp_name']);
                    $formato = explode('.', $this->data["Estadio"]["fotoes"]['name']);
                    $filename = $formato[0]."_".$estadio_id . '.' . $formato[1];
                    $data = $file->read();
                    $url = 'img/estadios/' . $filename;
                    $sql_Uprod = "UPDATE estadios SET foto='$url' WHERE id='$estadio_id'";
                    $result = $this->Estadio->query($sql_Uprod);

                    $file = new File(WWW_ROOT . 'img/estadios/' . $filename, true);
                    $file->write($data);
                    $file->close();
                }
                $this->Session->write("estadio_save",1);
                $this->redirect(array('action' => 'view',$id));
			} else {
                $this->Session->write("estadio_save",0);
                $this->redirect(array('action' => 'view',$id));
			}
		}

        $options = array('conditions' => array('Estadio.' . $this->Estadio->primaryKey => $id));
        $this->request->data = $this->Estadio->find('first', $options);

        $paises=$this->Estadio->Paise->find('list');

        $this->set(compact("paises"));
	}
}
