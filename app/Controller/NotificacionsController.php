<?php
App::uses('AppController', 'Controller');
//App::uses('CakeEmail', 'Network/Email');
App::uses('HttpSocket', 'Network/Http');

/*App::uses('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
App::uses('SOCKETLABS_SERVERID', '16207');
App::uses('SOCKETLABS_KEY', 'c3SQj58MoAi2y9NEs64L');*/


//header('Content-Type: application/json');
//define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
//define('SOCKETLABS_SERVERID', '16207');
//define('SOCKETLABS_KEY', 'c3SQj58MoAi2y9NEs64L');
/**
 * Notifications Controller
 *
 * @property Notificacion $Notification
 * @property PaginatorComponent $Paginator
 */
class NotificacionsController extends AppController {

	/**
	 * Helpers
	 *
	 * @var array
	 */
	public $helpers = array('Html', 'Form', 'Js');

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator');

	//FUNCION PARA ENVIAR NOTIFICACIONES
	public	function send_notification($proy_id,$transicion_id,$codProy){
	    /**DATOS DE LA ORGANIZACION**/
        $this->loadModel("Organizacion");
        $this->Organizacion->recursive=-1;
        $data_org = $this->Organizacion->find("all",[
            'conditions'=>[
                'Organizacion.id'=>1
            ],
            'fields'=>[
                'Organizacion.emailnotification',
                'Organizacion.namenotification'
            ]
        ]);
        /**---------------GET NOTIFICACION POR TRANSICION-----------------**/
        $this->loadModel("Transicionnotificacion");

        $transnoti = $this->Transicionnotificacion->find("all",[
            'joins'=>[[
                'table'=>'notificacions',
                'alias'=>'Notificacion',
                'type'=>'INNER',
                'conditions'=>[
                    'Transicionnotificacion.notificacion_id=Notificacion.id'
                ]
            ]],
            'conditions'=>[
                'Transicionnotificacion.transicion_id'=> $transicion_id,
                'Transicionnotificacion.activo'=> 1
            ],
            'fields'=>[
                'Notificacion.notificacion',
                'Notificacion.param'
            ]
        ]);
        $this->loadModel("Proyecto");
        $this->Proyecto->recursive=-1;
        $infoP = $this->Proyecto->read("institucion_id",$proy_id);
        $this->loadModel("User");
        $this->User->recursive=-1;

        $infoU = $this->User->find("all",[
            'joins'=>[
                [
                    'table'=>'contactos',
                    'alias'=>'Contacto',
                    'type'=>'INNER',
                    'conditions'=>[
                        'User.contacto_id = Contacto.id'
                    ]
                ]
            ],
            'conditions'=>[
                'User.instituciondefault'=>1,
                'Contacto.institucion_id'=>$infoP['Proyecto']['institucion_id']
            ],
            'fields'=>[
                'User.email','User.nombrecompleto'
            ]
        ]);
        if(count($infoU)>0){
            foreach ($transnoti as $item){
                $data_noti = json_decode($item['Notificacion']['param']);
                /***INFO DE LA NOTIFICACION**/
                $contenido = $data_noti->contenido;
                $asunto = $data_noti->asunto;

                $merge_data = new stdClass();
                $merge_data->PerMessage =
                    array(
                        array(
                            array(
                                'Field'=>'DeliveryAddress',
                                'Value'=>$infoU[0]['User']['email']  //$correodestino
                            )
                        )
                    );

                $request = array(
                    'header' => array('Content-Type' => 'application/json',
                    ),
                );
                $data = new stdClass();
                $data->ServerId = $this->Organizacion->getConfig('SOCKETLAB_SERVER_ID');
                $data->ApiKey = $this->Organizacion->getConfig('SOCKETLAB_API_KEY');

                $new_cont = str_replace('$codigo',$codProy,$contenido);
                $data->Messages =
                    array(
                        array(
                            'MergeData'=>$merge_data,
                            'Subject'=>$asunto,
                            'To'=>
                                array(
                                    array(
                                        'EmailAddress'=>'%%DeliveryAddress%%'
                                    )
                                ),
                            'From'=>
                                array(
                                    'EmailAddress'=>$data_org[0]['Organizacion']['emailnotification']
                                ),
                            'HtmlBody'=> $new_cont
                        )
                    );
                $bodyJson = json_encode($data);
                $HttpSocket = new HttpSocket();
                $result = $HttpSocket->post('https://inject.socketlabs.com/api/v1/email',$bodyJson,$request);
                if ($result==false) {
                    //echo 'HTTP Error';
                }else {
                    $result_obj = json_decode($result);
                    if ($result_obj->ErrorCode=='Success') {
                        //echo 'Successfully sent all messages.';
                    }
                    elseif ($result_obj->ErrorCode=='Warning') {
                        //echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.';

                        //not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
                    }
                    else {
                        //echo 'Failure Code: ' . $result_obj->ErrorCode;
                    }
                }
            }
        }
	}
}
