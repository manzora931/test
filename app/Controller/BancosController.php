<?php
App::uses('AppController', 'Controller');
/**
 * Bancos Controller
 *
 * @property Banco $Banco
 * @property PaginatorComponent $Paginator
 */
class BancosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Banco", "bancos", "index");
		$this->Banco->recursive = 0;
		$this->set('bancos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Banco", "bancos", "view");
		if (!$this->Banco->exists($id)) {
			throw new NotFoundException(__('Invalid banco'));
		}
		$options = array('conditions' => array('Banco.' . $this->Banco->primaryKey => $id));
		$this->set('banco', $this->Banco->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Banco", "bancos", "add");
		if ($this->request->is('post')) {
			$this->Banco->create();
			$this->request->data['Banco']['usuario']=$this->Session->read("nombreusuario");
			$this->request->data['Banco']['modified']=0;
			if ($this->Banco->save($this->request->data)) {
				$this->Session->write("save_banco",1);
				$id = $this->Banco->id;
				$this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The banco could not be saved. Please, try again.'));
			}
		}
		$paises = $this->Banco->Paise->find('list',['conditions'=>['Paise.activa'=>1]]);
		$this->set(compact('paises'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Banco", "bancos", "edit");
		if (!$this->Banco->exists($id)) {
			throw new NotFoundException(__('Invalid banco'));
		}
		if ($this->request->is(array('post', 'put'))) {
		    $this->request->data['Banco']['usuariomodif']=$this->Session->read("nombreusuario");
		    $this->request->data['Banco']['modified']=date("Y-m-d H:i:s");
			if ($this->Banco->save($this->request->data)) {
                $this->Session->write("save_banco",1);
                $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The banco could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Banco.' . $this->Banco->primaryKey => $id));
			$this->request->data = $this->Banco->find('first', $options);
		}
		$paises = $this->Banco->Paise->find('list');
		$this->set(compact('paises'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Banco", "bancos", "delete");
		if ($delete == true) {
			$this->Banco->id = $id;
			if (!$this->Banco->exists()) {
				throw new NotFoundException(__('Invalid banco'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Banco->delete()) {
					$this->Session->setFlash(__('The banco has been deleted.'));
			} else {
				$this->Session->setFlash(__('The banco could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Desembolso');
        $this->Desembolso->recursive = -1;
        $banco = $this->Desembolso->find('all', [
            'conditions' => [
                'OR' => array(
                    'Desembolso.bancorigen_id' => $id,
                    'Desembolso.bancdestino_id' => $id
                )
            ]
        ]);

        $data['bancos'] = (count($banco) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
