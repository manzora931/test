<?php
App::uses('AppController', 'Controller');
/**
 * Contactos Controller
 *
 * @property Contacto $Contacto
 * @property PaginatorComponent $Paginator
 */
class ContactosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Institucion", "institucions", "index");
		$this->Contacto->recursive = 0;
		$this->set('contactos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
        $this->Contacto->recursive = 0;
		$this->ValidarUsuario("Institucion", "institucions", "view");
		if (!$this->Contacto->exists($id)) {
			throw new NotFoundException(__('Invalid contacto'));
		}
		$options = array('conditions' => array('Contacto.' . $this->Contacto->primaryKey => $id));
		$contacto = $this->Contacto->find('first', $options);
		$this->set('contacto', $contacto);

        $this->loadModel('Departamento');
        $departamento = $this->Departamento->find('first', [
            'conditions' => [
                'Departamento.id' => $contacto['Municipio']['departamento_id']
            ]
        ]);

        $this->loadModel('User');
        $user = $this->User->find('first', [
            'conditions' => [
                'User.contacto_id' => $id
            ]
        ]);

        $this->set(compact('departamento', 'user'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($idInstitucion = null) {
		$this->ValidarUsuario("Institucion", "institucions", "add");
        $this->Contacto->recursive = 0;

		if ($this->request->is('post')) {
			$this->Contacto->create();
            $this->request->data['Contacto']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Contacto']['modified'] = 0;
			if ($this->Contacto->save($this->request->data)) {
                $id = $this->Contacto->id;
                $this->Session->write("contacto_save",1);
                $this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The contacto could not be saved. Please, try again.'));
			}
		}
		$institucions = $this->Contacto->Institucion->find('list');
        $this->loadModel('Paise');
        $paises = $this->Paise->find('list');
		$municipios = $this->Contacto->Municipio->find('list');

        $institucion = $this->Contacto->Institucion->find('first', [
            'conditions' => [
                'Institucion.id' => $idInstitucion
            ]
        ]);
		$this->set(compact('institucions', 'paises', 'institucion'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Institucion", "institucions", "edit");
        $this->Contacto->recursive = 0;

		if (!$this->Contacto->exists($id)) {
			throw new NotFoundException(__('Invalid contacto'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Contacto']['usuariomodif'] = $this->Session->read('nombreusuario');

			if ($this->Contacto->save($this->request->data)) {
                $this->Session->write("contacto_save",1);
				$this->Session->setFlash(__('The contacto has been saved.'));
                $this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The contacto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contacto.' . $this->Contacto->primaryKey => $id));
			$this->request->data = $this->Contacto->find('first', $options);
		}

        $this->loadModel('Departamento');
        $departamento = $this->Departamento->find('first', [
            'conditions' => [
                'Departamento.id' => $this->request->data['Municipio']['departamento_id']
            ]
        ]);

        $institucions = $this->Contacto->Institucion->find('list');
        $paises = $this->Departamento->Paise->find('list');
        $departamentos = $this->Departamento->find('list');
		$municipios = $this->Contacto->Municipio->find('list');

		$this->set(compact('institucions', 'municipios','departamentos', 'paises', 'departamento'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Institucion", "institucions", "delete");
        $this->Contacto->recursive = 0;

		if ($delete == true) {
            $this->Contacto->id = $id;
            if (!$this->Contacto->exists()) {
                throw new NotFoundException(__('Invalid contacto'));
            }

            $this->loadModel('User');
            $user = $this->User->find('all', [
                'conditions' => [
                    'User.contacto_id' => $id,
                ]
            ]);

            if(count($user)>0){
                $this->Session->write("contact_notDelete",1);
                $this->redirect(array('controller' => 'institucions', 'action' => 'index'));
            } else {
                if ($this->Contacto->delete()) {
                    $this->Session->write("contact_delete",1);
                } else {
                    $this->Session->setFlash(__('The institucion could not be deleted. Please, try again.'));
                }

                return $this->redirect(array('controller' => 'institucions', 'action' => 'index'));
            }
        }  else {
            echo "
                <style>
                    .cont{
                        height: 500%;
                        width: 100%;
                        position: absolute;
                        top: 0;
                        left: 0;
                        display: block;
                        background-color: #D8D8D8;
                        z-index: 150;
                    }
                    .cont .mensaje{
                        height: 100px;
                        width: 600px;
                        margin: 150px auto;
                        font-size: 2em;
                        border: solid 15px #4E8CCF;
                        text-align: justify;
                        padding: 0.5em;
                        box-shadow: 1px 1px 10px #4E8CCF, 2px 2px 10px #1C4673;
                        text-shadow: 1px 1px 1px rgba(188, 210, 221, 0.24);
                    }
                </style>
                <div class='cont'>
                    <div class='mensaje'>
                        <p>No cuenta con los privilegios necesarios para acceder a esta pantalla.
                        Intente con otro usuario ó póngase en contacto con el administrador.</p>
                    </div>
                </div>
                <script>
                    setInterval(function(){
                        window.history.back();
                    },5500);
                </script>
                ";
        }
	}

    public function validarCotacto()
    {

        if(empty( $_POST['contact']))
        {$DataQueryN = $this->Contacto
            ->query("SELECT  CONCAT(c.nombres,' ',c.apellidos) as nombre FROM contactos as c
                                WHERE CONCAT(UPPER(c.nombres),' ',UPPER(c.apellidos))=CONCAT(UPPER('" . $_POST['nom'] . "'),' ',UPPER('" . $_POST['ap'] . "'))");

            $DataQueryA = $this->Contacto
                ->query("SELECT  c.email as email  FROM contactos as c WHERE c.email='" . $_POST['em'] . "'");
            if ($DataQueryN) {
                $data['valor'] = 1;
                $data['data'] = $DataQueryN;

            } else if ($DataQueryA) {
                $data['valor'] = 2;
                $data['data'] = $DataQueryA;

            } else {
                $data['valor'] = 0;

            }
            echo json_encode($data);
            $this->autoRender = false;

        }
        else if(!empty($_POST['contact']))
        {
            $DataQueryN = $this->Contacto
                ->query("SELECT  CONCAT(c.nombres,' ',c.apellidos) as nombre FROM contactos as c
                                WHERE CONCAT(UPPER(c.nombres),' ',UPPER(c.apellidos))=CONCAT(UPPER('" . $_POST['nom'] . "'),' ',UPPER('" . $_POST['ap'] . "')) AND c.id <> ".$_POST['contact']);

            $DataQueryA = $this->Contacto
                ->query("SELECT  c.email as email  FROM contactos as c WHERE c.email='" . $_POST['em'] . "' AND c.id <> ".$_POST['contact']);
            if ($DataQueryN) {
                $data['valor'] = 1;
                $data['data'] = $DataQueryN;

            } else if ($DataQueryA) {
                $data['valor'] = 2;
                $data['data'] = $DataQueryA;

            } else {
                $data['valor'] = 0;

            }
            echo json_encode($data);
            $this->autoRender = false;
        }

    }
}
