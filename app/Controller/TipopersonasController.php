<?php
App::uses('AppController', 'Controller');
/**
 * Tipopersonas Controller
 *
 * @property Tipopersona $Tipopersona
 * @property PaginatorComponent $Paginator
 */
class TipopersonasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tipopersona", "tipopersonas", "index");
        $this->Paginator->settings = array('conditions' => array('Tipopersona.activo >=' => 1), 'order'=>array('Tipopersona.tipopersonas'=>'asc'));
        $this->Tipopersona->recursive = 0;
        include 'busqueda/tipopersonas.php';
        $data = $this->Paginator->paginate('Tipopersona');
        $this->set('tipopersonas', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[tipopersonas]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tipopersona", "tipopersonas", "view");
		if (!$this->Tipopersona->exists($id)) {
			throw new NotFoundException(__('Invalid tipopersona'));
		}
		$options = array('conditions' => array('Tipopersona.' . $this->Tipopersona->primaryKey => $id));
		$this->set('tipopersona', $this->Tipopersona->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tipopersona", "tipopersonas", "add");
		if ($this->request->is('post')) {
			$this->Tipopersona->create();
            $this->request->data['Tipopersona']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Tipopersona']['modified']=0;
			if ($this->Tipopersona->save($this->request->data)) {
				$tipopersona_id = $this->Tipopersona->id;
                $this->Session->write('tipopersona_save', 1);
                $this->redirect(['action' => 'view', $tipopersona_id]);
			} else {
				$this->Session->setFlash(__('The Tipopersona could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tipopersona", "tipopersonas", "edit");
		if (!$this->Tipopersona->exists($id)) {
			throw new NotFoundException(__('Invalid tipopersona'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Tipopersona']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Tipopersona->save($this->request->data)) {
                $this->Session->write('tipopersona_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The tipopersona could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipopersona.' . $this->Tipopersona->primaryKey => $id));
			$this->request->data = $this->Tipopersona->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tipopersona", "tipopersonas", "delete");
		if ($delete == true) {
			$this->Tipopersona->id = $id;
			if (!$this->Tipopersona->exists()) {
				throw new NotFoundException(__('Invalid tipopersona'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Tipopersona->delete()) {
					$this->Session->setFlash(__('The tipopersona has been deleted.'));
			} else {
				$this->Session->setFlash(__('The tipopersona could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_tipopersonas() {
        $id = $_POST['id'];
        $data = array();

        // $this->loadModel('Persona');
        $this->Tipopersona->Persona->recursive = -1;
        $tipo = $this->Tipopersona->Persona->find('all', [
            'conditions' => [
                'Persona.tipopersona_id' => $id,
            ]
        ]);

        $data['tipos'] = (count($tipo) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
