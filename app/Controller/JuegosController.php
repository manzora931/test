<?php
App::uses('AppController', 'Controller');
/**
 * Tareas Controller
 *
 * @property Juego $Juego
 * @property PaginatorComponent $Paginator
 */
class JuegosController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $helpers = array('Html','Js', 'Form');
    public $components = array('Paginator');
    private $tipopersona_alineacion=array(1,3);
    /***
     * author: Manuel Anzora
     * create: 26-03-2019
     * description: Metodo para definir que un metodo es publico(no requiere autenticacion de usuario)
     ****/
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow("listJuegos","viewpublic","alineacionespublic","notaspublic","cronicapublic","minutoxminutopublic",'estadisticaviewpublic',"vertodospublic");
    }
    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->ValidarUsuario("Juego", "juegos", "index");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $this->Paginator->settings = array("conditions"=>["Juego.torneo_id IN "=>$torneos],'order'=>array('Juego.fecha'=>'desc', 'Juego.hora'=>'desc'));
            }else{
                $this->Paginator->settings = array("conditions"=>["Juego.torneo_id"=>$torneos],'order'=>array('Juego.fecha'=>'desc', 'Juego.hora'=>'desc'));
            }

        }else{
            $this->Paginator->settings = array('order'=>array('Juego.fecha'=>'desc', 'Juego.hora'=>'desc'));
        }

        $this->Juego->recursive = 0;
        include 'busqueda/juegos.php';
        $data = $this->Paginator->paginate('Juego');
        $this->set('juegos', $data);

        $this->loadModel("Equipo");
        if($perfil==3 || $perfil==4 || $perfil==5) {
            if (count($torneos) > 1)
                $torneos = $this->Juego->Torneo->find('list', ["conditions" => ["activo" => 1, "Torneo.id IN" => $torneos], "order" => ["Torneo.nombrecorto"]]);
            else
                $torneos = $this->Juego->Torneo->find('list', ["conditions" => ["activo" => 1, "Torneo.id" => $torneos], "order" => ["Torneo.nombrecorto"]]);
        }else{
            $torneos = $this->Juego->Torneo->find('list', ["conditions" => ["activo" => 1], "order" => ["Torneo.nombrecorto"]]);
        }
        //$this->loadModel("Posicione");
        $estadios = $this->Juego->Estadio->find('list',["order"=>["Estadio.estadio"]]);
        $equip = $this->Equipo->find("list");
        $this->set(compact('equip', 'torneos', 'estadios'));
    }

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[juegos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
    function vertodospublic($torneo, $host){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[juegos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "listjuegos",$torneo,$host));
        $this->autoRender=false;
    }
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->ValidarUsuario("Juego", "juegos", "view");
        if (!$this->Juego->exists($id)) {
            throw new NotFoundException(__('Invalid juego'));
        }
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
        $juego=$this->Juego->find('first', $options);
        $this->loadModel("Persona");
        $arbitros = $this->Persona->find('list', [
            'fields' => ['Persona.id', 'Persona.nombre_persona'],
            'conditions'=> [
                'Persona.tipopersona_id' => 4
            ]
        ]);
        $perfil = $this->getPerfil($this->Session->read('nombreusuario'));
        $this->set(compact('juego','id','arbitros','perfil'));
    }


//Función para eliminar campos no deseados al momento de guardar
    public function clearRequestData($data, $campos){
        $newdata=array();
        foreach ($data as $key => $value) {
            if(!isset($newdata[$key])) $newdata[$key]=array();
            foreach ($value as $key2 => $value2) {
                $find=false;
                for ($i=0; $i <count($campos); $i++) {
                    if(strcmp($key2, $campos[$i])==0) $find=true;
                }
                if(!$find) $newdata[$key][$key2]=$value2;
            }
        }
        return $newdata;
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($id = null) {
        $this->ValidarUsuario("Juego", "juegos", "add");
        if ($this->request->is(array('post', 'put'))) {
            $fecha = explode('-', $this->request->data['Juego']['fecha']);
            $this->request->data['Juego']['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
            $this->request->data['Juego']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Juego']['modified']=0;
            $campos=array(0=>'marcadore1',
                1=>'marcadore2',
                2=>'marcadore1pt',
                3=>'marcadore2pt');
            $this->request->data=$this->clearRequestData($this->request->data,$campos);
            $this->Juego->create();
            if ($this->Juego->save($this->request->data)) {
                $juego_id = $this->Juego->id;

                $this->Session->write('juego_save', 1);
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('El juego no ha sido almacenado. Intentar de nuevo.'));
            }
        } elseif (!is_null($id)) {
            $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
            $this->request->data = $this->Juego->find('first', $options);
        }

        $juego = (!is_null($id)) ? $this->Juego->find('first', ['conditions' => ['Juego.id' => $id]]) : null ;
        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find('list');
        $torneos = $this->Juego->Torneo->find('list', [
            "conditions"=>["Torneo.activo"=>1]
        ]);
        $estadios = $this->Juego->Estadio->find('list');
        $etapas = $this->Juego->Etapa->find('list');
        $grupos = $this->Juego->Grupo->find('list');

        $this->loadModel("Persona");
        $arbitros = $this->Persona->find('list', [
            'fields' => ['Persona.id', 'Persona.nombre_persona'],
            'conditions'=> [
                'Persona.tipopersona_id' => 4
            ]
        ]);

        $this->set(compact('id', 'juego', 'equipos', 'torneos', 'estadios', 'etapas', 'grupos', 'arbitros'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->ValidarUsuario("Juego", "juegos", "edit");
        if (!$this->Juego->exists($id)) {
            throw new NotFoundException(__('Invalid juego'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $fecha = explode('-', $this->request->data['Juego']['fecha']);
            $this->request->data['Juego']['fecha'] = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
            $this->request->data['Juego']['usuariomodif'] = $this->Session->read('nombreusuario');
            $this->request->data['Juego']['modified'] = date("Y-m-d H:i:s");
            $campos=array(0=>'marcadore1',
                1=>'marcadore2',
                2=>'marcadore1pt',
                3=>'marcadore2pt');
            $this->request->data=$this->clearRequestData($this->request->data,$campos);
            if ($this->Juego->save($this->request->data)) {
                $juego_id = $this->Juego->id;

                $this->Session->write('juego_save', 1);
                $this->redirect(['action' => 'view', $id]);
            } else {
                $this->Session->setFlash(__('El juego no se ha sido actualizado. Intentar de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
            $this->request->data = $this->Juego->find('first', $options);
        }

        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find('list');
        $torneos = $this->Juego->Torneo->find('list', [
            "conditions"=>["Torneo.activo"=>1]
        ]);
        $estadios = $this->Juego->Estadio->find('list');
        $etapas = $this->Juego->Etapa->find('list');
        $grupos = $this->Juego->Grupo->find('list');

        $this->loadModel("Persona");
        $arbitros = $this->Persona->find('list', [
            'fields' => ['Persona.id', 'Persona.nombre_persona'],
            'conditions'=> [
                'Persona.tipopersona_id' => 4
            ]
        ]);

        $this->set(compact('equipos', 'torneos', 'estadios', 'etapas', 'grupos', 'arbitros'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $delete = $this->ValidarUsuario("Juego", "juegos", "delete");
        if ($delete == true) {
            $this->Juego->id = $id;
            if (!$this->Juego->exists()) {
                throw new NotFoundException(__('Invalid juego'));
            }
            $this->request->onlyAllow('post', 'delete');
            if ($this->Juego->delete()) {
                $this->Session->setFlash(__('The juego has been deleted.'));
            } else {
                $this->Session->setFlash(__('The juego could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function deletejuegoalineacione(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $chkequipolocal=true;
        $options = array('conditions' => array('Juegoalineacione.' . $this->Juego->primaryKey => $id));
        $juegoalineacione = $this->Juego->Juegoalineacione->find('first', $options);
        $equipo_id=$juegoalineacione['Juegoalineacione']['equipo_id'];
        $juego_id=$juegoalineacione['Juegoalineacione']['juego_id'];
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $juego_id));
        $juego = $this->Juego->find('first', $options);
        if($juego['Juego']['equipo2_id']==$juegoalineacione['Juegoalineacione']['equipo_id'])
            $chkequipolocal=false;
        $this->Juego->Juegoalineacione->id=$id;
        if (!$this->Juego->Juegoalineacione->exists()) {
            $delete=false;
            $msj='Registro Inválido';
        }else{
            if ($this->Juego->Juegoalineacione->delete()) {
                $delete=true;
                $msj='Registro Eliminado Exitosamente';
            } else {
                $delete=false;
                $msj='Ocurrió un problema. Inténtalo más tarde';
            }
        }
        echo json_encode(array('resp'=>$delete,
            'equipo_id'=>$equipo_id,
            'juego_id'=>$juego_id,
            'chkequipolocal'=>$chkequipolocal,
            'msj'=>$msj));
    }

    public function deletejuegoevento(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $marcadore1=0;
        $marcadore2=0;
        $marcadore1pt=0;
        $marcadore2pt=0;
        //info de evento
        $options = array('conditions' => array('Juegoevento.' . $this->Juego->primaryKey => $id));
        $evento = $this->Juego->Juegoevento->find('first', [
            'conditions' => [
                'Juegoevento.id' => $id,
            ]
        ]);
        $juego_id=$evento['Juegoevento']['juego_id'];
        $this->Juego->Juegoevento->id=$id;
        if (!$this->Juego->Juegoevento->exists()) {
            $delete=false;
            $msj='Registro Inválido';
        }else{
            if ($this->Juego->Juegoevento->delete()) {
                $delete=true;
                $msj='Registro Eliminado Exitosamente';
                $array=$this->setMarcadorJuego($juego_id);
                $marcadore1 = $array['marcadore1'];
                $marcadore2 = $array['marcadore2'];
                $marcadore1pt = $array['marcadore1pt'];
                $marcadore2pt = $array['marcadore2pt'];
            } else {
                $delete=false;
                $msj='Ocurrió un problema. Inténtalo más tarde';
            }
        }
        echo json_encode(array('resp'=>$delete,
            'marcadore1'=>$marcadore1,
            'marcadore2'=>$marcadore2,
            'marcadore1pt'=>$marcadore1pt,
            'marcadore2pt'=>$marcadore2pt,
            'juego_id'=>$juego_id,
            'msj'=>$msj));
    }

    public function deletejuegonota(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $this->Juego->Juegnota->id=$id;
        if (!$this->Juego->Juegnota->exists()) {
            $delete=false;
            $msj='Registro Inválido';
        }else{
            if ($this->Juego->Juegnota->delete()) {
                $delete=true;
                $msj='Registro Eliminado Exitosamente';
            } else {
                $delete=false;
                $msj='Ocurrió un problema. Inténtalo más tarde';
            }
        }
        echo json_encode(array('resp'=>$delete,
            'msj'=>$msj));
    }

    public function deletejuegocronica(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $juego = $this->Juego->find('first', [
            'conditions' => [
                'Juego.id' => $id,
            ]
        ]);
        $registro['Juego']['id']=$juego['Juego']['id'];
        $registro['Juego']['cronica']=null;
        if ($this->Juego->save($registro)) {
            $delete=true;
            $msj='Registro Eliminado Exitosamente';
        } else {
            $delete=false;
            $msj='Ocurrió un problema. Inténtalo más tarde';
        }

        echo json_encode(array('resp'=>$delete,
            'msj'=>$msj));
    }

    //función para obtener información del juegoalineacione y llenar modal
    public function getjuegoalineacione(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $options = array('conditions' => array('Juegoalineacione.' . $this->Juego->primaryKey => $id));
        $juegoalineacione = $this->Juego->Juegoalineacione->find('first', $options);
        $juego_id=$juegoalineacione['Juegoalineacione']['juego_id'];
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $juego_id));
        $juego = $this->Juego->find('first', $options);
        $chkequipolocal=true;
        $posicion=null;
        $dorsal=null;
        $jugadorid=null;
        $personaid=null;
        $cargo=null;
        $capitan=($juegoalineacione['Juegoalineacione']['capitan']==1)?true:false;
        $equipo_id = $juegoalineacione['Juegoalineacione']['equipo_id'];
        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find('list');
        $nombre_equipo = $equipos[$juegoalineacione['Juegoalineacione']['equipo_id']];

        //listado de jugadores
        $this->loadModel('Jugadoresxequipo');
        $jugadoresequipo = $this->Jugadoresxequipo->find("all", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $juegoalineacione['Juegoalineacione']['equipo_id']
            ],
            'fields' => ['Jugadoresxequipo.jugadore_id']
        ]);

        $jugadoresId = array();
        foreach($jugadoresequipo as $key => $jugador){
            array_push($jugadoresId, $jugador['Jugadoresxequipo']['jugadore_id']);
        }

        $this->loadModel('Jugadore');
        $jugadores = $this->Jugadore->find("all", [
            'fields' => ['Jugadore.id', 'Jugadore.nombre_jugador'],
            'conditions'=> [
                'Jugadore.id' => $jugadoresId
            ]
        ]);

        //listado de personas; director tecnico, etc
        $this->loadModel("Persona");
        $personas = $this->Persona->find('all', [
            'fields' => ['Persona.id', 'Persona.nombre_persona'],
            'conditions'=> [
                'Persona.tipopersona_id IN' => $this->tipopersona_alineacion
            ]
        ]);
        if($juego['Juego']['equipo2_id']==$juegoalineacione['Juegoalineacione']['equipo_id'])
            $chkequipolocal=false;
        if(!empty($juegoalineacione['Juegoalineacione']['jugadore_id'])){
            $jugadorid=$juegoalineacione['Juegoalineacione']['jugadore_id'];
            $this->loadModel('Posicione');
            $posicion = $this->Posicione->find("first", [
                'fields' => ['Posicione.id', 'Posicione.posicion'],
                'conditions'=> [
                    'Posicione.id' => $juegoalineacione['Juegoalineacione']['posicione_id']
                ]
            ]);
            $posicion=$posicion['Posicione']['posicion'];
            $this->loadModel('Jugadoresxequipo');
            $dorsal = $this->Jugadoresxequipo->find("first", [
                'fields' => ['Jugadoresxequipo.equipo_id', 'Jugadoresxequipo.dorsal'],
                'conditions' => [
                    'Jugadoresxequipo.jugadore_id' => $juegoalineacione['Juegoalineacione']['jugadore_id']
                ]
            ]);
            $dorsal=$dorsal['Jugadoresxequipo']['dorsal'];
        }else{
            $personaid=$juegoalineacione['Juegoalineacione']['persona_id'];
            $this->loadModel('Persona');
            $cargo = $this->Persona->find("first", [
                'conditions'=> [
                    'Persona.id' => $personaid
                ]
            ]);
        }
        $data=array('chkequipolocal'=>$chkequipolocal,
            'equipo_id'=>$equipo_id,
            'jugadores'=>$jugadores,
            'nombre_equipo'=>$nombre_equipo,
            'jugadorid'=>$jugadorid,
            'posicion'=>$posicion,
            'dorsal'=>$dorsal,
            'capitan'=>$capitan,
            'personaid'=>$personaid,
            'personas'=>$personas,
            'cargo'=>$cargo);
        echo json_encode($data);
    }

    public function getjuegominutoxminuto(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $chkequipolocal=true;
        $select1_equipo=array();
        $select2_equipo=array();
        $detalle_eventos=array();

        //info de evento
        $options = array('conditions' => array('Juegoevento.' . $this->Juego->primaryKey => $id));
        $evento = $this->Juego->Juegoevento->find('first', [
            'conditions' => [
                'Juegoevento.id' => $id,
            ],
            'order'=> [
                'Juegoevento.minuto' => 'desc'
            ]
        ]);
        $juego=$evento['Juego'];

        //extracción de jugadores por equipo
        $this->loadModel('Jugadoresxequipo');
        $equipo1 = $this->getjugadoresxequipo($juego['equipo1_id']);
        $equipo2 = $this->getjugadoresxequipo($juego['equipo2_id']);

        $jugadoreequipo = $this->Jugadoresxequipo->find("first", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $juego['equipo1_id'],
                'Jugadoresxequipo.jugadore_id' => $evento['Juegoevento']['jugador_id']
            ],
            'fields' => ['Jugadoresxequipo.equipo_id']
        ]);

        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find('list');
        $nombre_equipo = $equipos[$juego['equipo1_id']];
        if(count($jugadoreequipo)>0){
            $select1_equipo=$equipo1;
            $select2_equipo=$equipo2;
        }else{
            $select1_equipo=$equipo2;
            $select2_equipo=$equipo1;
            $chkequipolocal=false;
            $nombre_equipo = $equipos[$juego['equipo2_id']];
        }

        $this->loadModel("Evento");
        $eventos = $this->Evento->find('all',[
            'fields' => ['Evento.id','Evento.evento']
        ]);

        $nameLabel='Jugador Adversario';
        $tipoevento=strtolower($evento['Evento']['evento']);
        if(strcmp($tipoevento, 'cambio')==0){
            $select2_equipo=$select1_equipo;
            $nameLabel='Jugador Saliente';
        }
        elseif(strcmp($tipoevento, 'falta')==0){
            $this->loadModel('Detevento');
            $detalle_eventos = $this->Detevento->find("all", [
                'fields' => ['Detevento.id', 'Detevento.detevento'],
                'conditions'=> [
                    'Detevento.evento_id' => $evento['Juegoevento']['evento_id']
                ]
            ]);
        }else{
            $this->loadModel('Detevento');
            $detalle_eventos = $this->Detevento->find("all", [
                'fields' => ['Detevento.id', 'Detevento.detevento'],
                'conditions'=> [
                    'Detevento.evento_id' => $evento['Juegoevento']['evento_id']
                ]
            ]);
        }
        
        $data=array('chkequipolocal'=>$chkequipolocal,
            'select1_equipo'=>$select1_equipo,
            'select2_equipo'=>$select2_equipo,
            'eventos'=>$eventos,
            'detalle_eventos'=>$detalle_eventos,
            'nombre_equipo'=>$nombre_equipo,
            'nameLabel'=>$nameLabel,
            'jugador_id'=>$evento['Juegoevento']['jugador_id'],
            'jugador2_id'=>$evento['Juegoevento']['jugador2_id'],
            'evento_id'=>$evento['Juegoevento']['evento_id'],
            'tipoevento'=>$tipoevento,
            'detevento_id'=>$evento['Juegoevento']['detevento_id'],
            'comentario'=>$evento['Juegoevento']['comentario'],
            'minuto'=>$evento['Juegoevento']['minuto'],
            'minadicional'=>$evento['Juegoevento']['minadicional']
        );
        echo json_encode($data);
    }

    public function getjuegonota(){
        $this->autoRender=false;
        $id=$_POST['id'];
        $chkequipolocal=true;
        $select1_equipo=array();

        //info de evento
        $options = array('conditions' => array('Juegonota.' . $this->Juego->primaryKey => $id));
        $nota = $this->Juego->Juegnota->find('first', [
            'conditions' => [
                'Juegnota.id' => $id,
            ]
        ]);
        $juego=$nota['Juego'];

        //extracción de jugadores por equipo
        $this->loadModel('Jugadoresxequipo');
        $equipo1 = $this->getjugadoresxequipo($juego['equipo1_id']);
        $equipo2 = $this->getjugadoresxequipo($juego['equipo2_id']);
        $jugadoreequipo = $this->Jugadoresxequipo->find("first", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $juego['equipo1_id'],
                'Jugadoresxequipo.jugadore_id' => $nota['Juegnota']['jugadore_id']
            ],
            'fields' => ['Jugadoresxequipo.equipo_id']
        ]);

        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find('list');
        $nombre_equipo = $equipos[$juego['equipo1_id']];
        if(count($jugadoreequipo)>0){
            $select1_equipo=$equipo1;
        }else{
            $select1_equipo=$equipo2;
            $chkequipolocal=false;
            $nombre_equipo = $equipos[$juego['equipo2_id']];
        }

        $data=array('chkequipolocal'=>$chkequipolocal,
            'select1_equipo'=>$select1_equipo,
            'nombre_equipo'=>$nombre_equipo,
            'jugadore_id'=>$nota['Juegnota']['jugadore_id'],
            'nota'=>$nota['Juegnota']['nota']);
        echo json_encode($data);
    }

    public function getjugadoresxequipo($equipo_id){
        //extracción de jugadores por equipo
        $jugadores=array();
        $this->loadModel('Jugadoresxequipo');
        $jugadoresequipo = $this->Jugadoresxequipo->find("all", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $equipo_id
            ],
            'fields' => ['Jugadoresxequipo.jugadore_id']
        ]);
        $jugadoresId = array();
        foreach($jugadoresequipo as $key => $jugador){
            array_push($jugadoresId, $jugador['Jugadoresxequipo']['jugadore_id']);
        }

        $this->loadModel('Jugadore');
        $jugadores = $this->Jugadore->find("all", [
            'fields' => ['Jugadore.id', 'Jugadore.nombre_jugador'],
            'conditions'=> [
                'Jugadore.id' => $jugadoresId
            ]
        ]);

        return $jugadores;
    }

    // Se obtienen los equipos para el juego segun el torneo seleccionado
    public function get_equipos(){
        $id = $_POST['id'];

        $this->loadModel('Jugadoresxequipo');
        $equipostorneo = $this->Jugadoresxequipo->find("all", [
            'conditions' => [
                'Jugadoresxequipo.torneo_id' => $id
            ],
            'fields' => ['Jugadoresxequipo.equipo_id']
        ]);

        $equiposId = array();
        foreach($equipostorneo as $key => $equipo){
            array_push($equiposId, $equipo['Jugadoresxequipo']['equipo_id']);
        }

        $this->loadModel('Equipo');
        $data = $this->Equipo->find("list", [
            'fields' => ['Equipo.id', 'Equipo.nombrecorto'],
            'conditions'=> [
                'Equipo.id' => $equiposId
            ]
        ]);

        $equipos = "<option value=''> Seleccionar </option>";
        foreach($data as $key => $row){
            $equipos.= "<option value='".$key."'>".$row."</option>";
        }
        $this->loadModel("Gruposxtorneo");
        $gruposxtorneo = $this->Gruposxtorneo->find("all", [
            "conditions"=>[
                "Gruposxtorneo.torneo_id" => $id
            ]
        ]);
        $this->loadModel("Grupo");
        $this->Grupo->recursive = -1;
        $grupos = $this->Grupo->find("all",
            [
                "conditions"=>["Grupo.activo"=>1],
                "fields"=>["Grupo.id", "Grupo.grupo"],
                "order"=>["Grupo.grupo"]
            ]);
        $grupolist = [];
        foreach ($grupos as $item){
            $grupolist[$item["Grupo"]["id"]] = $item["Grupo"]["grupo"];
        }
        $grupos = "<option value=''> Seleccionar </option>";
        if(count($gruposxtorneo)>0){
            foreach($gruposxtorneo as $row){
                $grupos .= "<option value='".$row["Gruposxtorneo"]["grupo_id"]."'>".$grupolist[$row["Gruposxtorneo"]["grupo_id"]]."</option>";
            }
        }else{
            foreach($grupolist as $key => $value){
                $grupos .= "<option value='".$key."'>".$value."</option>";
            }
        }

        echo json_encode(["equipos" => $equipos, "grupos"=> $grupos]);

        $this->autoRender=false;
    }

    // Al momento de seleccionar al equipo local
    public function equipo_local(){
        $id = $_POST['id'];
        $torneo = $_POST["torneo"];
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
        $juego = $this->Juego->find('first', $options);

        $this->loadModel('Jugadoresxequipo');
        $jugadoresequipo = $this->Jugadoresxequipo->find("all", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $juego['Juego']['equipo1_id'],
                "Jugadoresxequipo.torneo_id" => $torneo
            ],
            'fields' => ['Jugadoresxequipo.jugadore_id']
        ]);
        $jugadoresId = array();
        foreach($jugadoresequipo as $key => $jugador){
            $jugadoresId[$jugador['Jugadoresxequipo']['jugadore_id']]=$jugador['Jugadoresxequipo']['jugadore_id'];
        }
        $this->loadModel('Jugadore');
        $data = $this->Jugadore->find("all", [
            "fields"=>["Jugadore.id","Jugadore.nombre_jugador","jugadoresxequipo.dorsal"],
            'joins'=>[
                [
                    "table"=>"jugadoresxequipos",
                    "alias"=>"jugadoresxequipo",
                    "type"=>"inner",
                    "conditions"=>[
                        "Jugadore.id = jugadoresxequipo.jugadore_id"
                    ]
                ]
            ],
            'conditions'=> [
                'Jugadore.id' => $jugadoresId,
                "jugadoresxequipo.torneo_id"=>$torneo
            ]
        ]);
        $jugadores = "<option value=''> Seleccionar </option>";
        foreach($data as $key){
            $jugadores.= "<option value='".$key["Jugadore"]["id"]."'>".$key["jugadoresxequipo"]["dorsal"]." - ".$key["Jugadore"]["nombre_jugador"]."</option>";
        }

        $this->loadModel("Equipo");
        $data['equipos'] = $this->Equipo->find('list');
        $data['equipo_id'] = $juego['Juego']['equipo1_id'];
        $data['nombre_equipo'] = $data['equipos'][$data['equipo_id']];
        $data['jugadores'] = $jugadores;
        $data['juego'] = $juego;
        echo json_encode($data);

        $this->autoRender=false;
    }

    // Al momento de seleccionar al equipo visitante
    public function equipo_visitante(){
        $id = $_POST['id'];
        $torneo = $_POST["torneo"];
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
        $juego = $this->Juego->find('first', $options);

        $this->loadModel('Jugadoresxequipo');
        $jugadoresequipo = $this->Jugadoresxequipo->find("all", [
            'conditions' => [
                'Jugadoresxequipo.equipo_id' => $juego['Juego']['equipo2_id'],
                "Jugadoresxequipo.torneo_id" => $torneo
            ],
            'fields' => ['Jugadoresxequipo.jugadore_id']
        ]);

        $jugadoresId = array();
        foreach($jugadoresequipo as $key => $jugador){
            array_push($jugadoresId, $jugador['Jugadoresxequipo']['jugadore_id']);
        }

        $this->loadModel('Jugadore');
        $data = $this->Jugadore->find("all", [
            "fields"=>["Jugadore.id","Jugadore.nombre_jugador","jugadoresxequipo.dorsal"],
            'joins'=>[
                [
                    "table"=>"jugadoresxequipos",
                    "alias"=>"jugadoresxequipo",
                    "type"=>"inner",
                    "conditions"=>[
                        "Jugadore.id = jugadoresxequipo.jugadore_id"
                    ]
                ]
            ],
            'conditions'=> [
                'Jugadore.id' => $jugadoresId,
                "jugadoresxequipo.torneo_id" => $torneo
            ]
        ]);

        $jugadores = "<option value=''> Seleccionar </option>";
        foreach($data as $key){
            $jugadores.= "<option value='".$key["Jugadore"]["id"]."'>".$key["jugadoresxequipo"]["dorsal"]." - ".$key["Jugadore"]["nombre_jugador"]."</option>";
        }

        $this->loadModel("Equipo");
        $data['equipos'] = $this->Equipo->find('list');
        $data['equipo_id'] = $juego['Juego']['equipo2_id'];
        $data['nombre_equipo'] = $data['equipos'][$data['equipo_id']];
        $data['jugadores'] = $jugadores;
        $data['juego'] = $juego;
        echo json_encode($data);

        $this->autoRender=false;
    }

    public function get_info_evento(){
        $id = $_POST['id'];

        $this->loadModel('Detevento');
        $data = $this->Detevento->find("list", [
            'fields' => ['Detevento.id', 'Detevento.detevento'],
            'conditions'=> [
                'Detevento.evento_id' => $id
            ]
        ]);

        $deteventos = "<option value=''> Seleccionar </option>";
        foreach($data as $key => $row){
            $deteventos.= "<option value='".$key."'>".$row."</option>";
        }

        echo $deteventos;
        //echo json_encode($info);

        $this->autoRender=false;
    }

    public function alineaciones()
    {
        $id = $_POST['id'];
        $equipoid1 = $_POST['equipo1'];
        $equipoid2 = $_POST['equipo2'];
        $render = (isset($_POST['render']))?$_POST['render']:null;
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $alineaciones_local = array();
        $alineaciones_visitante = array();
        $alineaciones = array();
        $equipo1 = array();
        $equipo2 = array();
        if($id != '') {
            $this->loadModel("Equipo");
            $options1 = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $equipoid1));
            $equipo1 = $this->Equipo->find('first', $options1);

            if(!empty($equipoid2)){
                $options2 = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $equipoid2));
                $equipo2 = $this->Equipo->find('first', $options2);
            }

            $alineaciones_local = $this->Juego->Juegoalineacione->find('all', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $id,
                    'Juegoalineacione.equipo_id' => $equipoid1,
                ]
            ]);

            if(!empty($equipoid2)){
                $alineaciones_visitante = $this->Juego->Juegoalineacione->find('all', [
                    'conditions' => [
                        'Juegoalineacione.juego_id' => $id,
                        'Juegoalineacione.equipo_id' => $equipoid2,
                    ]
                ]);
            }

            $this->loadModel("Persona");
            $personas = $this->Persona->find('list', [
                'fields' => ['Persona.id', 'Persona.nombre_persona'],
                'conditions'=> [
                    'Persona.tipopersona_id IN' => $this->tipopersona_alineacion
                ]
            ]);
        }
        if(!empty($render)){
            $alineaciones=$alineaciones_local;
            $this->set(compact('id', 'alineaciones', 'equipo1', 'equipo2','personas','view'));
            $this->render('alineacionestbody');
        }
        $this->loadModel("Jugadoresxequipo");
        $numjugadores = $this->Jugadoresxequipo->find("all",[
            "fields"=>["Jugadoresxequipo.jugadore_id","Jugadoresxequipo.dorsal","Jugadoresxequipo.equipo_id"]
        ]);
        $dorsalJugadores = [];
        foreach ($numjugadores as $item):
            $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]=$item["Jugadoresxequipo"]["dorsal"];
        endforeach;
        $this->set(compact('id', 'alineaciones_local', 'alineaciones_visitante', 'equipo1', 'equipo2','personas','view',"dorsalJugadores"));
    }

    public function minutoxminuto()
    {
        $id = $_POST['id'];
        $render = (isset($_POST['render']))?$_POST['render']:null;
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $eventos = array();
        $juego=[];
        $equipos_local_visitante=[];
        if($id != '') {
            $juego = $this->Juego->find("first",["conditions"=>["Juego.id"=>$_POST["id"]]]);
            $eventos = $this->Juego->Juegoevento->find('all', [
                'conditions' => [
                    'Juegoevento.juego_id' => $id,
                ],
                'order'=> [
                    'Juegoevento.minuto' => 'desc'
                ]
            ]);
            $equipos_local_visitante=[
                $juego["Juego"]["equipo1_id"]=>$juego["Juego"]["equipo1_id"],
                $juego["Juego"]["equipo2_id"]=>$juego["Juego"]["equipo2_id"]];
        }

        $this->loadModel("Evento");
        $meventos = $this->Evento->find('list',[
            "conditions"=>[
                "Evento.activo"=>1
            ],
            "order"=>["Evento.evento"=>"asc"]
        ]);

        $this->loadModel('Jugadore');
        $jugadores = $this->Jugadore->find("all",
            [
                "joins"=>[
                    [
                        "table"=>"jugadoresxequipos",
                        "alias"=>"jugadorxequipo",
                        "type"=>"inner",
                        "conditions"=>[
                            "Jugadore.id = jugadorxequipo.jugadore_id"
                        ]
                    ],
                    [
                        "table"=>"equipos",
                        "alias"=>"equipo",
                        "type"=>"inner",
                        "conditions"=>[
                            "equipo.id = jugadorxequipo.equipo_id"
                        ]
                    ]
                ],
                "conditions"=>[
                    "jugadorxequipo.torneo_id"=>$juego["Juego"]["torneo_id"],
                    "jugadorxequipo.equipo_id in "=>$equipos_local_visitante],
                "fields"=>[
                    "Jugadore.id","Jugadore.nombre_jugador","jugadorxequipo.dorsal","equipo.nombrecorto"
                ]
            ]);
            //debug($jugadores);
        $dataJugador = [];
        foreach ($jugadores as $data){
            $dataJugador[$data["Jugadore"]["id"]]["nombre"]=$data["Jugadore"]["nombre_jugador"];
            $dataJugador[$data["Jugadore"]["id"]]["dorsal"]=$data["jugadorxequipo"]["dorsal"];
            $dataJugador[$data["Jugadore"]["id"]]["equipo"]=$data["equipo"]["nombrecorto"];
        }
        $this->set(compact('id', 'eventos', 'meventos', 'jugadores','view','dataJugador'));
        if(!empty($render)) $this->render('minutoxminutotbody');
    }
    public function estadistica(){
        $this->layout=false;
        $data = $this->Juego->find("first",[
            "fields"=>["Juego.id","Equipo1.nombrecorto","Equipo1.fotoescudo","Equipo2.nombrecorto","Equipo2.fotoescudo","Juego.id","Juego.posesioneq1","Juego.posesioneq2",
                "Juego.tirosapuertaeq1","Juego.tirosapuertaeq2","Juego.tirosdeesquinaeq1","Juego.tirosdeesquinaeq2","Juego.saquesdepuertaeq1","Juego.saquesdepuertaeq2",
                "Juego.paseseq1","Juego.paseseq2","Juego.disparosalposteeq1","Juego.disparosalposteeq2","Juego.saquesdebandaeq1","Juego.saquesdebandaeq2","Juego.penalesporeq1",
                "Juego.penalesporeq2","Juego.penalesfalladoseq1","Juego.penalesfalladoseq2","Juego.tarjetaamarillaeq1"
                ,"Juego.tarjetaamarillaeq2","Juego.tarjetarojaeq1","Juego.tarjetarojaeq2", "Juego.faltaseq1", "Juego.faltaseq2", "Juego.tirosfueraeq1", "Juego.tirosfueraeq2"],
            "conditions"=>[
                "Juego.id"=>$_POST["id"]
            ]
        ]);
        $this->set(compact("data"));
    }
    public function estadisticaview(){
        $this->layout=false;
        $data = $this->Juego->find("first",[
            "fields"=>["Juego.id","Equipo1.nombrecorto","Equipo1.fotoescudo","Equipo2.nombrecorto","Equipo2.fotoescudo","Juego.id","Juego.posesioneq1","Juego.posesioneq2",
                "Juego.tirosapuertaeq1","Juego.tirosapuertaeq2","Juego.tirosdeesquinaeq1","Juego.tirosdeesquinaeq2","Juego.saquesdepuertaeq1","Juego.saquesdepuertaeq2",
                "Juego.paseseq1","Juego.paseseq2","Juego.disparosalposteeq1","Juego.disparosalposteeq2","Juego.saquesdebandaeq1","Juego.saquesdebandaeq2","Juego.penalesporeq1",
                "Juego.penalesporeq2","Juego.penalesfalladoseq1","Juego.penalesfalladoseq2","Juego.tarjetaamarillaeq1"
                ,"Juego.tarjetaamarillaeq2","Juego.tarjetarojaeq1","Juego.tarjetarojaeq2", "Juego.faltaseq1", "Juego.faltaseq2", "Juego.tirosfueraeq1", "Juego.tirosfueraeq2"],
            "conditions"=>[
                "Juego.id"=>$_POST["id"]
            ]
        ]);
        $this->set(compact("data"));
    }
    public function updateStadistic(){
        $data = [
            "id"=>$_POST["idJuego"],
            $_POST["campo"]=>$_POST["valor"],
            "usuariomodif"=>$this->Session->read("nombreusuario")
        ];
        if($this->Juego->save($data)){
            echo 1;
        }
        $this->autoRender=false;
    }

    public function notas()
    {
        $id = $_POST['id'];
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $notas = array();
        if($id != '') {
            $notas = $this->Juego->Juegnota->find('all', [
                'conditions' => [
                    'Juegnota.juego_id' => $id,
                ]
            ]);
        }
        $this->loadModel("Jugadoresxequipo");
        $numjugadores = $this->Jugadoresxequipo->find("all",[
            "fields"=>["Jugadoresxequipo.jugadore_id","Jugadoresxequipo.dorsal","Jugadoresxequipo.equipo_id"]
        ]);
        $dorsalJugadores = [];
        foreach ($numjugadores as $item):
            $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]=$item["Jugadoresxequipo"]["dorsal"];
        endforeach;
        $this->set(compact('id', 'notas','view',"dorsalJugadores"));
    }

    public function cronica()
    {
        $id = $_POST['id'];
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $juego = array();
        if($id != '') {
            $juego = $this->Juego->find('first', [
                'conditions' => [
                    'Juego.id' => $id,
                ]
            ]);
        }

        $this->set(compact('id', 'juego','view'));
    }

    // Se obtienen la informacion del jugador seleccionado
    public function get_info_jugador(){
        $id = $_POST['id'];
        $juego_id = $_POST['juego_id'];

        $this->loadModel('Jugadoresxequipo');
        $data['dorsal'] = $this->Jugadoresxequipo->find("first", [
            'conditions' => [
                'Jugadoresxequipo.jugadore_id' => $id,
                "Jugadoresxequipo.torneo_id"=>$_POST["torneo_id"]
            ],
            'fields' => ['Jugadoresxequipo.equipo_id', 'Jugadoresxequipo.dorsal']
        ]);

        $this->loadModel('Jugadore');
        $data['jugador'] = $this->Jugadore->find("first", [
            'fields' => ['Jugadore.id', 'Jugadore.posicione_id'],
            'conditions'=> [
                'Jugadore.id' => $id
            ]
        ]);

        $this->loadModel('Posicione');
        $data['posicione_id'] = $this->Posicione->find("first", [
            'fields' => ['Posicione.id', 'Posicione.posicion'],
            'conditions'=> [
                'Posicione.id' => $data['jugador']['Jugadore']['posicione_id']
            ]
        ]);

        echo json_encode($data);
        $this->autoRender=false;
    }

    // Se obtienen la informacion del jugador seleccionado
    public function get_info_persona(){
        $id = $_POST['id'];
        $this->loadModel('Persona');
        $data = $this->Persona->find("first", [
            'conditions'=> [
                'Persona.id' => $id
            ]
        ]);
        echo json_encode($data);
        $this->autoRender=false;
    }

    //funcion para verificar si ya se ingresaron 11 jugadores al equipo seleccionado
    public function getCountPlayers($juego_id, $equipo_id){
        $array=array(0=>true);
        $alineaciones = $this->Juego->Juegoalineacione->find('count', [
            'conditions' => [
                'Juegoalineacione.juego_id' => $juego_id,
                'Juegoalineacione.equipo_id' => $equipo_id,
            ]
        ]);
        if($alineaciones>=11){
            $array=array(0=>false,
                1=>'Ya existen 11 jugadores en la alineación del equipo seleccionado.');
        }
        return $array;
    }

    //funcion para verificar si ya se ingreso 1 capitán al equipo seleccionado
    public function getCountCaptain($juego_id, $equipo_id, $capitan){
        $array=array(0=>true);
        $alineaciones = $this->Juego->Juegoalineacione->find('count', [
            'conditions' => [
                'Juegoalineacione.juego_id' => $juego_id,
                'Juegoalineacione.equipo_id' => $equipo_id,
                'Juegoalineacione.capitan' => $capitan,
            ]
        ]);
        if($capitan==1){
            if($alineaciones==1){
                $array=array(0=>false,
                    1=>'Ya existen un capitán asignado en la alineación del equipo seleccionado.');
            }
        }
        return $array;
    }

    //funcion para verificar si ya se ingreso el jugador al equipo seleccionado
    public function getCountPlayer($juego_id, $equipo_id, $jugador_id){
        $array=array(0=>true);
        $alineaciones = $this->Juego->Juegoalineacione->find('count', [
            'conditions' => [
                'Juegoalineacione.juego_id' => $juego_id,
                'Juegoalineacione.equipo_id' => $equipo_id,
                'Juegoalineacione.jugadore_id' => $jugador_id,
            ]
        ]);
        if($alineaciones==1){
            $array=array(0=>false,
                1=>'Ya existen el jugador en la alineación del equipo seleccionado.');
        }
        return $array;
    }

    //funcion para verificar si ya se ingreso el jugador al equipo seleccionado
    public function getCountPersona($juego_id, $equipo_id, $persona_id){
        $array=array(0=>true);
        $alineaciones = $this->Juego->Juegoalineacione->find('count', [
            'conditions' => [
                'Juegoalineacione.juego_id' => $juego_id,
                'Juegoalineacione.equipo_id' => $equipo_id,
                'Juegoalineacione.persona_id' => $persona_id,
            ]
        ]);
        if($alineaciones==1){
            $array=array(0=>false,
                1=>'Ya existen la persona en la alineación del equipo seleccionado.');
        }
        return $array;
    }

    public function registrar_alineacion_local() {
        $data = $_POST['data'];
        $accion = $data['tipo'];
        if(strcmp($accion, 'add')==0){
            $res=$this->getCountPlayers($data['juego_id'], $data['equipo_id']);
            if($res[0]){
                $res=$this->getCountCaptain($data['juego_id'], $data['equipo_id'], $data['capitanalineaciones']);
                if($res[0]){
                    $res=$this->getCountPlayer($data['juego_id'], $data['equipo_id'], $data['jugadoralineaciones_id']);
                    if($res[0]){
                        $alineacione['juego_id'] = $data['juego_id'];
                        $alineacione['equipo_id'] = $data['equipo_id'];
                        $alineacione['jugadore_id'] = $data['jugadoralineaciones_id'];
                        $alineacione['posicione_id'] = $data['posicionidalineaciones'];
                        $alineacione['capitan'] = $data['capitanalineaciones'];
                        $alineacione['usuario'] = $this->Session->read('nombreusuario');
                        $alineacione['created'] = date('Y-m-d H:m:s');
                        $alineacione['modified'] = 0;

                        $this->Juego->Juegoalineacione->create();
                        $this->Juego->Juegoalineacione->set($alineacione);

                        if ($this->Juego->Juegoalineacione->save()) {
                            $alineacion['id'] = $this->Juego->Juegoalineacione->id;

                            $alineacion['msg'] = 'exito';
                        } else {
                            $alineacion['msg'] = 'error';
                        }
                    }else{
                        $alineacion['msg'] = $res[1];
                    }
                }else{
                    $alineacion['msg'] = $res[1];
                }
            }else{
                $alineacion['msg'] = $res[1];
            }
        }else{
            $edit=true;
            $alineaciones = $this->Juego->Juegoalineacione->find('first', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $data['juego_id'],
                    'Juegoalineacione.equipo_id' => $data['equipo_id'],
                    'Juegoalineacione.jugadore_id' => $data['jugadoralineaciones_id'],
                ]
            ]);

            if(count($alineaciones)>0){
                if($alineaciones['Juegoalineacione']['capitan']==false && $data['capitanalineaciones']==1){
                    $res=$this->getCountCaptain($data['juego_id'], $data['equipo_id'], $data['capitanalineaciones']);
                    if(!$res[0]) $edit=false;
                }
                if($edit){
                    $alineaciones['Juegoalineacione']['capitan'] = $data['capitanalineaciones'];
                    $alineaciones['Juegoalineacione']['usuariomodif'] = $this->Session->read('nombreusuario');
                    $alineaciones['Juegoalineacione']['modified'] = date("Y-m-d H:i:s");
                    if ($this->Juego->Juegoalineacione->save($alineaciones)) {
                        $alineacion['id'] = $this->Juego->Juegoalineacione->id;
                        $alineacion['msg'] = 'exito';
                    } else {
                        $alineacion['msg'] = 'error';
                    }
                }else{
                    $alineacion['msg'] = $res[1];
                }
            }else{
                $alineacion['msg'] = 'Para editar, primero debes agregar al jugador seleccionado';
            }
        }

        echo json_encode($alineacion);
        $this->autoRender=false;
    }

    public function registrar_alineacion_visitante() {
        $data = $_POST['data'];
        $accion = $data['tipo'];
        if(strcmp($accion, 'add')==0){
            $res=$this->getCountPlayers($data['juego_id'], $data['equipo_id']);
            if($res[0]){
                $res=$this->getCountCaptain($data['juego_id'], $data['equipo_id'], $data['capitanalineaciones']);
                if($res[0]){
                    $res=$this->getCountPlayer($data['juego_id'], $data['equipo_id'], $data['jugadoralineaciones_id']);
                    if($res[0]){
                        $alineacione['juego_id'] = $data['juego_id'];
                        $alineacione['equipo_id'] = $data['equipo_id'];
                        $alineacione['jugadore_id'] = $data['jugadoralineaciones_id'];
                        $alineacione['posicione_id'] = $data['posicionidalineaciones'];
                        $alineacione['capitan'] = $data['capitanalineaciones'];
                        $alineacione['usuario'] = $this->Session->read('nombreusuario');
                        $alineacione['created'] = date('Y-m-d H:m:s');
                        $alineacione['modified'] = 0;

                        $this->Juego->Juegoalineacione->create();
                        $this->Juego->Juegoalineacione->set($alineacione);

                        if ($this->Juego->Juegoalineacione->save()) {
                            $alineacion['id'] = $this->Juego->Juegoalineacione->id;

                            $alineacion['msg'] = 'exito';
                        } else {
                            $alineacion['msg'] = 'error';
                        }
                    }else{
                        $alineacion['msg'] = $res[1];
                    }
                }else{
                    $alineacion['msg'] = $res[1];
                }
            }else{
                $alineacion['msg'] = $res[1];
            }
        }else{
            $edit=true;
            $alineaciones = $this->Juego->Juegoalineacione->find('first', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $data['juego_id'],
                    'Juegoalineacione.equipo_id' => $data['equipo_id'],
                    'Juegoalineacione.jugadore_id' => $data['jugadoralineaciones_id'],
                ]
            ]);

            if(count($alineaciones)>0){
                if($alineaciones['Juegoalineacione']['capitan']==false && $data['capitanalineaciones']==1){
                    $res=$this->getCountCaptain($data['juego_id'], $data['equipo_id'], $data['capitanalineaciones']);
                    if(!$res[0]) $edit=false;
                }
                if($edit){
                    $alineaciones['Juegoalineacione']['capitan'] = $data['capitanalineaciones'];
                    $alineaciones['Juegoalineacione']['usuariomodif'] = $this->Session->read('nombreusuario');
                    $alineaciones['Juegoalineacione']['modified'] = date("Y-m-d H:i:s");
                    if ($this->Juego->Juegoalineacione->save($alineaciones)) {
                        $alineacion['id'] = $this->Juego->Juegoalineacione->id;
                        $alineacion['msg'] = 'exito';
                    } else {
                        $alineacion['msg'] = 'error';
                    }
                }else{
                    $alineacion['msg'] = $res[1];
                }
            }else{
                $alineacion['msg'] = 'Para editar, primero debes agregar al jugador seleccionado';
            }
        }

        echo json_encode($alineacion);
        $this->autoRender=false;
    }

    public function registrar_alineacion_persona() {
        $data = $_POST['data'];
        $accion = $data['tipo'];
        if(strcmp($accion, 'add')==0){
            $res=$this->getCountPersona($data['juego_id'], $data['equipo_id'], $data['personaalineaciones_id']);
            if($res[0]){
                $alineacione['juego_id'] = $data['juego_id'];
                $alineacione['equipo_id'] = $data['equipo_id'];
                $alineacione['jugadore_id'] = null;
                $alineacione['posicione_id'] = null;
                $alineacione['persona_id'] = $data['personaalineaciones_id'];
                $alineacione['tipopersona_id'] = $data['cargoidalineaciones'];
                $alineacione['capitan'] = 0;
                $alineacione['usuario'] = $this->Session->read('nombreusuario');
                $alineacione['created'] = date('Y-m-d H:m:s');
                $alineacione['modified'] = 0;

                $this->Juego->Juegoalineacione->create();
                $this->Juego->Juegoalineacione->set($alineacione);

                if ($this->Juego->Juegoalineacione->save()) {
                    $alineacion['id'] = $this->Juego->Juegoalineacione->id;

                    $alineacion['msg'] = 'exito';
                } else {
                    $alineacion['msg'] = 'error';
                }
            }else{
                $alineacion['msg'] = $res[1];
            }
        }else{
            $edit=true;
            $alineaciones = $this->Juego->Juegoalineacione->find('first', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $data['juego_id'],
                    'Juegoalineacione.equipo_id' => $data['equipo_id'],
                    'Juegoalineacione.persona_id' => $data['personaalineaciones_id'],
                ]
            ]);

            if(count($alineaciones)>0){
                if($edit){
                    $alineaciones['Juegoalineacione']['usuariomodif'] = $this->Session->read('nombreusuario');
                    $alineaciones['Juegoalineacione']['modified'] = date("Y-m-d H:i:s");
                    if ($this->Juego->Juegoalineacione->save($alineaciones)) {
                        $alineacion['id'] = $this->Juego->Juegoalineacione->id;
                        $alineacion['msg'] = 'exito';
                    } else {
                        $alineacion['msg'] = 'error';
                    }
                }else{
                    $alineacion['msg'] = 'No es posible editar el registro';
                }
            }else{
                $alineacion['msg'] = 'Para editar, primero debes agregar a la persona seleccionada';
            }
        }

        echo json_encode($alineacion);
        $this->autoRender=false;
    }

    public function getEquipoAnotacion($juego_id, $jugador_id, $evento, $torneo_id){
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $juego_id));
        $juego = $this->Juego->find('first', $options);

        $this->loadModel('Jugadoresxequipo');
        $jugadoresequipo = $this->Jugadoresxequipo->find("first", [
            'conditions' => [
                'Jugadoresxequipo.jugadore_id' => $jugador_id,
                'Jugadoresxequipo.equipo_id' => $juego['Juego']['equipo1_id'],
                'Jugadoresxequipo.torneo_id'=>$torneo_id
            ]
        ]);

        $jugadoresequipo2 = $this->Jugadoresxequipo->find("first", [
            'conditions' => [
                'Jugadoresxequipo.jugadore_id' => $jugador_id,
                'Jugadoresxequipo.equipo_id' => $juego['Juego']['equipo2_id'],
                'Jugadoresxequipo.torneo_id'=>$torneo_id
            ]
        ]);

        $marcadore1=0;
        $marcadore2=0;
        switch ($evento) {
            case 'gol':
                if(count($jugadoresequipo)>0) $marcadore1=1;
                if(count($jugadoresequipo2)>0) $marcadore2=1;
                break;
            case 'autogol':
                if(count($jugadoresequipo)>0) $marcadore2=1;
                if(count($jugadoresequipo2)>0) $marcadore1=1;
                break;
        }
        return $array=array('marcadore1'=>$marcadore1,
            'marcadore2'=>$marcadore2);
    }

    public function setMarcadorJuego($juego_id, $torneo_id){
        $time_first=45;
        $this->loadModel("Evento");
        $meventos = $this->Evento->find('list');

        $this->loadModel('Juegoevento');
        $juegoeventos = $this->Juegoevento->find("all", [
            'conditions' => [
                'Juegoevento.juego_id' => $juego_id
            ]
        ]);

        $marcadore1=0;
        $marcadore2=0;
        $marcadore1pt=0;
        $marcadore2pt=0;
        foreach ($juegoeventos as $key) {
            $evento=strtolower($meventos[$key['Juegoevento']['evento_id']]);
            $array=$this->getEquipoAnotacion($juego_id,$key['Juegoevento']['jugador_id'], $evento, $torneo_id);
            $marcadore1+=$array['marcadore1'];
            $marcadore2+=$array['marcadore2'];
            if($key['Juegoevento']['minuto']<=$time_first){
                $marcadore1pt+=$array['marcadore1'];
                $marcadore2pt+=$array['marcadore2'];
            }
        }
        if($marcadore1>0 || $marcadore2>0){
            $juego = $this->Juego->find("first", [
                'conditions'=> [
                    'Juego.id' => $juego_id
                ]
            ]);

            $juego['Juego']['marcadore1'] = $marcadore1;
            $juego['Juego']['marcadore2'] = $marcadore2;
            $juego['Juego']['marcadore1pt'] = $marcadore1pt;
            $juego['Juego']['marcadore2pt'] = $marcadore2pt;
            $juego['Juego']['usuariomodif'] = $this->Session->read('nombreusuario');
            $juego['Juego']['modified'] = date('Y-m-d H:m:s');
            $this->Juego->set($juego);
            $this->Juego->save();
        }

        return array('marcadore1'=>$marcadore1,
            'marcadore2'=>$marcadore2,
            'marcadore1pt'=>$marcadore1pt,
            'marcadore2pt'=>$marcadore2pt);
    }

    public function registrar_evento_juego() {
        $data = $_POST['data'];
        $accion = $data['tipo'];
        $torneo_id = $data['torneo_id'];
        if(strcmp($accion, 'add')==0){
            $evento['juego_id'] = $data['juego_id'];
            $evento['evento_id'] = $data['eventoeventos_id'];
            $evento['detevento_id'] = $data['deteventoeventos_id'];
            $evento['minuto'] = $data['minutoeventos'];
            $evento['jugador_id'] = $data['jugadoreventos_id'];
            $evento['minadicional'] = ($data['minadicional']!='')?$data['minadicional']:null;
            if(!empty($data['jugadoreventos2_id'])) $evento['jugador2_id'] = $data['jugadoreventos2_id'];
            $evento['comentario'] = $data['comentarioeventos'];
            $evento['usuario'] = $this->Session->read('nombreusuario');
            $evento['created'] = date('Y-m-d H:m:s');
            $evento['modified'] = 0;

            $this->Juego->Juegoevento->create();
            $this->Juego->Juegoevento->set($evento);

            if ($this->Juego->Juegoevento->save()) {
                $resp['id'] = $this->Juego->Juegoevento->id;
                $array=$this->setMarcadorJuego($data['juego_id'], $torneo_id);
                $resp['msg'] = 'exito';
                $resp['marcadore1'] = $array['marcadore1'];
                $resp['marcadore2'] = $array['marcadore2'];
                $resp['marcadore1pt'] = $array['marcadore1pt'];
                $resp['marcadore2pt'] = $array['marcadore2pt'];
            } else {
                $resp['msg'] = 'error';
            }
        }else{
            $evento['Juegoevento']['id'] = $data['juegoevento_id'];
            $evento['Juegoevento']['evento_id'] = $data['eventoeventos_id'];
            $evento['Juegoevento']['detevento_id'] = $data['deteventoeventos_id'];
            $evento['Juegoevento']['minuto'] = $data['minutoeventos'];
            $evento['minadicional'] = ($data['minadicional']!='')?$data['minadicional']:null;
            $evento['Juegoevento']['jugador_id'] = $data['jugadoreventos_id'];
            $evento['Juegoevento']['jugador2_id'] = $data['jugadoreventos2_id'];
            $evento['Juegoevento']['comentario'] = $data['comentarioeventos'];
            $evento['Juegoevento']['minadicional'] = ($data['minadicional']!='')?$data['minadicional']:null;
            $evento['Juegoevento']['usuariomodif'] = $this->Session->read('nombreusuario');
            $evento['Juegoevento']['modified'] = date("Y-m-d H:i:s");
            if ($this->Juego->Juegoevento->save($evento)) {
                $resp['id'] = $this->Juego->Juegoevento->id;
                $array=$this->setMarcadorJuego($data['juego_id'], $torneo_id);
                $resp['msg'] = 'exito';
                $resp['marcadore1'] = $array['marcadore1'];
                $resp['marcadore2'] = $array['marcadore2'];
                $resp['marcadore1pt'] = $array['marcadore1pt'];
                $resp['marcadore2pt'] = $array['marcadore2pt'];
            } else {
                $resp['msg'] = 'error';
            }
        }
        $juego = $this->Juego->find("first",["conditions"=>["Juego.id"=>$data['juego_id']]]);
        if($juego["Juego"]["marcadore1"]==null && $juego["Juego"]["marcadore2"]==null){
            $game["id"]             =$data['juego_id'];
            $game["marcadore1pt"]   =0;
            $game["marcadore2pt"]   =0;
            $game["marcadore1"]     =0;
            $game["marcadore2"]     =0;
            $this->Juego->set($game);
            $this->Juego->save($game);
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function registrar_nota_juego() {
        $data = $_POST['data'];
        $accion = $data['tipo'];
        if(strcmp($accion, 'add')==0){
            $nota['juego_id'] = $data['juego_id'];
            $nota['jugadore_id'] = $data['jugadornotas_id'];
            $nota['nota'] = $data['txtnotas'];
            $nota['usuario'] = $this->Session->read('nombreusuario');
            $nota['created'] = date('Y-m-d H:m:s');
            $nota['modified'] = 0;

            $this->Juego->Juegnota->create();
            $this->Juego->Juegnota->set($nota);

            if ($this->Juego->Juegnota->save()) {
                $resp['id'] = $this->Juego->Juegnota->id;

                $resp['msg'] = 'exito';
            } else {
                $resp['msg'] = 'error';
            }
        }else{
            $nota['Juegnota']['id'] = $data['nota_id'];
            $nota['Juegnota']['nota'] = $data['txtnotas'];
            $nota['Juegnota']['jugadore_id'] = $data['jugadornotas_id'];
            $nota['Juegnota']['usuariomodif'] = $this->Session->read('nombreusuario');
            $nota['Juegnota']['modified'] = date("Y-m-d H:i:s");
            if ($this->Juego->Juegnota->save($nota)) {
                $resp['id'] = $this->Juego->Juegnota->id;
                $resp['msg'] = 'exito';
            } else {
                $resp['msg'] = 'error';
            }
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }

    public function registrar_cronica_juego() {
        $data = $_POST['data'];

        $juego = $this->Juego->find("first", [
            'conditions'=> [
                'Juego.id' => $data['juego_id']
            ]
        ]);

        $juego['Juego']['cronica'] = $data['cronica'];
        $juego['Juego']['usuariomodif'] = $this->Session->read('nombreusuario');
        $juego['Juego']['modified'] = date('Y-m-d H:m:s');
        $this->Juego->set($juego);

        if ($this->Juego->save()) {
            $resp['id'] = $this->Juego->id;

            $resp['msg'] = 'exito';
        } else {
            $resp['msg'] = 'error';
        }

        echo json_encode($resp);
        $this->autoRender=false;
    }
    public function getEquipos(){
        $id = $_POST["id"];
        $equipo_id = (isset($_SESSION['tabla[juegos]']['equipo_id']))?$_SESSION['tabla[juegos]']['equipo_id']:0;
        $this->loadModel("Equipo");
        $this->Equipo->recursive=-1;
        $equipos = $this->Equipo->find("all",[
            "fields"=>["Equipo.id","Equipo.nombrecorto"],
            "joins"=>[
                [
                    "table"=>"equiposxtorneos",
                    "alias"=>"Equipoxt",
                    "type"=>"inner",
                    "conditions"=>[
                        "Equipo.id=Equipoxt.equipo_id"
                    ]
                ]
            ],
            "conditions"=>["Equipoxt.torneo_id"=>$id]
        ]);
        $html = "<option>Seleccionar</option>";

        foreach ($equipos as $equipo){
            if($equipo["Equipo"]["id"]==$equipo_id){
                $html .= "<option value='".$equipo["Equipo"]["id"]."' selected>".$equipo["Equipo"]["nombrecorto"]."</option>";
            }else{
                $html .= "<option value='".$equipo["Equipo"]["id"]."'>".$equipo["Equipo"]["nombrecorto"]."</option>";
            }
        }
        echo $html;
        $this->autoRender=false;
    }
    /**
     * Author: Manuel Anzora
     * create: 09-02-2019
     * description: Metodo para obtener el escudo del equipo
     * params: id del equipo
     ***/
    public function getImg(){
        $this->loadModel("Equipo");
        $info = $this->Equipo->find("all",[
            "fields"=>["Equipo.fotoescudo"],
            "conditions"=>["Equipo.id"=>$_POST["id"]]
        ]);
        $html = "";
        foreach ($info as $row){
            $html .= "<img src='../../".$row["Equipo"]["fotoescudo"]."' class='escudo1'>";
        }
        echo $html;
        $this->autoRender=false;
    }
    /**
     * Author: Manuel Anzora
     * create: 09-02-2019
     * description: Metodo para obtener el escudo del equipo
     * params: id del equipo
     ***/
    public function getImg2(){
        $this->loadModel("Equipo");
        $info = $this->Equipo->find("all",[
            "fields"=>["Equipo.fotoescudo"],
            "conditions"=>["Equipo.id"=>$_POST["id"]]
        ]);
        $html = "";
        foreach ($info as $row){
            $html .= "<img src='../".$row["Equipo"]["fotoescudo"]."' class='escudo1'>";
        }
        echo $html;
        $this->autoRender=false;
    }
    /***
     *author: Manuel Anzora
     *description: Metodo para imprimir view de juegos***/
    public function impresion($id){
        $this->layout=false;
        $this->view($id);
        $this->Juego->recursive=-1;
        $info = $this->Juego->find("all",[
            "fields"=>[
                "Juego.equipo1_id","Juego.equipo2_id"
            ],
            "conditions"=>[
                "Juego.id"=>$id
            ]
        ]);
        $alineaciones_local=[];
        $alineaciones_visita=[];
        $equipo1="";
        $equipo2="";
        if(count($info)>0){
            $alineaciones_local = $this->Juego->Juegoalineacione->find('all', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $id,
                    'Juegoalineacione.equipo_id' => $info[0]["Juego"]["equipo1_id"],
                ]
            ]);

            $equipo1=(count($alineaciones_local)>0)?$alineaciones_local[0]["Equipo"]["equipo"]:"";
            //debug($alineaciones_local);
            $alineaciones_visita = $this->Juego->Juegoalineacione->find('all', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $id,
                    'Juegoalineacione.equipo_id' => $info[0]["Juego"]["equipo2_id"],
                ]
            ]);
            $equipo2=(count($alineaciones_visita)>0)?$alineaciones_visita[0]["Equipo"]["equipo"]:"";
            /*******Dorsal de los jugadores********/
            $this->loadModel("Jugadoresxequipo");
            $numjugadores = $this->Jugadoresxequipo->find("all",[
                "fields"=>["Jugadoresxequipo.jugadore_id","Jugadoresxequipo.dorsal","Jugadoresxequipo.equipo_id","Equipo.id","Equipo.nombrecorto","Jugadore.nombre","Jugadore.apellido"]
            ]);
            $dorsalJugadores = [];
            foreach ($numjugadores as $item):
                $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]["dorsal"]=$item["Jugadoresxequipo"]["dorsal"];
                $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]["equipo"]=$item["Equipo"]["nombrecorto"];
                $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]["nombre"]=$item["Jugadore"]["nombre"]." ".$item["Jugadore"]["apellido"];
            endforeach;
            /**MINUTO A MINUTO***/
            $this->loadModel("Juegoevento");
            $eventos = $this->Juego->Juegoevento->find('all', [
                'conditions' => [
                    'Juegoevento.juego_id' => $id,
                ],
                'order'=> [
                    'Juegoevento.minuto' => 'desc'
                ]
            ]);
            /****Notas****/
            $notas = $this->Juego->Juegnota->find('all', [
                'conditions' => [
                    'Juegnota.juego_id' => $id,
                ]
            ]);
        }
        $this->set(compact("alineaciones_local","alineaciones_visita","equipo1","equipo2","dorsalJugadores","eventos","notas"));
    }
    /***
     * Author: Manuel Anzora
     * created: 31-03-2019
     * description: Metodo para desplegar el listado de los juegos, limitados a un torneo en especifico
     * params: recibe como parametro el id del torneo que se desea consultar
     **/
    public function listJuegos($torneo_id=null,$host=null){
        $this->layout="public";

        if($this->request->is(array('post', 'put'))){
            $torneo_id = $this->data["torneo"];
            $host = $this->data["servidor"];
        }
        if($torneo_id!=null){
            $_SESSION["torneo_id"] = $torneo_id;
        }
        if($torneo_id!=null){
            $this->Paginator->settings = array(
                "conditions"=>array("Juego.torneo_id"=>$torneo_id),
                'order'=>array('Juego.fecha'=>'desc', 'Juego.hora'=>'desc'));
            $this->Juego->recursive = 0;
            include 'busqueda/juegos.php';
            $data = $this->Paginator->paginate('Juego');
            $this->set('juegos', $data);

            $this->loadModel("Equipo");
            $torneos = $this->Juego->Torneo->find('list',["conditions"=>["activo"=>1]]);
            //$this->loadModel("Posicione");
            $estadios = $this->Juego->Estadio->find('list');
            $etapas = $this->Juego->Etapa->find("list", [
                "conditions"=>["Etapa.activo"=>1],
                "order"=>["Etapa.etapa"=>"asc"]
            ]);
            $equipos = $this->Equipo->find("list",[
                "fields"=>["Equipo.id","Equipo.nombrecorto"],
                "joins"=>[
                    [
                        "table"=>"equiposxtorneos",
                        "alias"=>"Equipoxt",
                        "type"=>"inner",
                        "conditions"=>[
                            "Equipo.id=Equipoxt.equipo_id"
                        ]
                    ]
                ],
                "conditions"=>["Equipoxt.torneo_id"=>$torneo_id]
            ]);
            $this->set(compact('equipos', 'torneos', 'estadios',"torneo_id","host", "etapas"));
        }else{
            $torneo_id = $_SESSION["torneo_id"];
            $this->Paginator->settings = array(
                "conditions"=>array("Juego.torneo_id"=>$torneo_id),
                'order'=>array('Juego.fecha'=>'desc', 'Juego.hora'=>'desc'));
            $this->Juego->recursive = 0;
            include 'busqueda/juegos.php';
            $data = $this->Paginator->paginate('Juego');
            $this->set('juegos', $data);

            $this->loadModel("Equipo");
            $torneos = $this->Juego->Torneo->find('list',["conditions"=>["activo"=>1]]);
            //$this->loadModel("Posicione");
            $estadios = $this->Juego->Estadio->find('list');
            $etapas = $this->Juego->Etapa->find("list", [
                "conditions"=>["Etapa.activo"=>1],
                "order"=>["Etapa.etapa"=>"asc"]
            ]);
            $equipos = $this->Equipo->find("list",[
                "fields"=>["Equipo.id","Equipo.nombrecorto"],
                "joins"=>[
                    [
                        "table"=>"equiposxtorneos",
                        "alias"=>"Equipoxt",
                        "type"=>"inner",
                        "conditions"=>[
                            "Equipo.id=Equipoxt.equipo_id"
                        ]
                    ]
                ],
                "conditions"=>["Equipoxt.torneo_id"=>$torneo_id]
            ]);
            $this->set(compact('equipos', 'torneos', 'estadios',"torneo_id","host", "etapas"));
        }
    }
    public function viewpublic($id = null,$host) {
        $this->layout="public";
        if (!$this->Juego->exists($id)) {
            throw new NotFoundException(__('Invalid juego'));
        }
        $options = array('conditions' => array('Juego.' . $this->Juego->primaryKey => $id));
        $juego=$this->Juego->find('first', $options);
        $this->loadModel("Persona");
        $arbitros = $this->Persona->find('list', [
            'fields' => ['Persona.id', 'Persona.nombre_persona'],
            'conditions'=> [
                'Persona.tipopersona_id' => 4
            ]
        ]);
        $perfil = $this->getPerfil($this->Session->read('nombreusuario'));
        $this->set(compact('juego','id','arbitros','perfil',"host"));
    }
    public function alineacionespublic()
    {
        $this->layout="public";
        $id = $_POST['id'];
        $equipoid1 = $_POST['equipo1'];
        $equipoid2 = $_POST['equipo2'];
        $render = (isset($_POST['render']))?$_POST['render']:null;
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $alineaciones_local = array();
        $alineaciones_visitante = array();
        $alineaciones = array();
        $equipo1 = array();
        $equipo2 = array();
        if($id != '') {
            $this->loadModel("Equipo");
            $options1 = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $equipoid1));
            $equipo1 = $this->Equipo->find('first', $options1);

            if(!empty($equipoid2)){
                $options2 = array('conditions' => array('Equipo.' . $this->Equipo->primaryKey => $equipoid2));
                $equipo2 = $this->Equipo->find('first', $options2);
            }

            $alineaciones_local = $this->Juego->Juegoalineacione->find('all', [
                'conditions' => [
                    'Juegoalineacione.juego_id' => $id,
                    'Juegoalineacione.equipo_id' => $equipoid1,
                ]
            ]);

            if(!empty($equipoid2)){
                $alineaciones_visitante = $this->Juego->Juegoalineacione->find('all', [
                    'conditions' => [
                        'Juegoalineacione.juego_id' => $id,
                        'Juegoalineacione.equipo_id' => $equipoid2,
                    ]
                ]);
            }

            $this->loadModel("Persona");
            $personas = $this->Persona->find('list', [
                'fields' => ['Persona.id', 'Persona.nombre_persona'],
                'conditions'=> [
                    'Persona.tipopersona_id IN' => $this->tipopersona_alineacion
                ]
            ]);
        }
        if(!empty($render)){
            $alineaciones=$alineaciones_local;
            $this->set(compact('id', 'alineaciones', 'equipo1', 'equipo2','personas','view'));
            $this->render('alineacionestbody');
        }
        $this->loadModel("Jugadoresxequipo");
        $numjugadores = $this->Jugadoresxequipo->find("all",[
            "fields"=>["Jugadoresxequipo.jugadore_id","Jugadoresxequipo.dorsal","Jugadoresxequipo.equipo_id"]
        ]);
        $dorsalJugadores = [];
        foreach ($numjugadores as $item):
            $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]=$item["Jugadoresxequipo"]["dorsal"];
        endforeach;
        $this->set(compact('id', 'alineaciones_local', 'alineaciones_visitante', 'equipo1', 'equipo2','personas','view',"dorsalJugadores"));
    }
    public function notaspublic()
    {
        $id = $_POST['id'];
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $notas = array();
        if($id != '') {
            $notas = $this->Juego->Juegnota->find('all', [
                'conditions' => [
                    'Juegnota.juego_id' => $id,
                ]
            ]);
        }
        $this->loadModel("Jugadoresxequipo");
        $numjugadores = $this->Jugadoresxequipo->find("all",[
            "fields"=>["Jugadoresxequipo.jugadore_id","Jugadoresxequipo.dorsal","Jugadoresxequipo.equipo_id"]
        ]);
        $dorsalJugadores = [];
        foreach ($numjugadores as $item):
            $dorsalJugadores[$item["Jugadoresxequipo"]["jugadore_id"]]=$item["Jugadoresxequipo"]["dorsal"];
        endforeach;
        $this->set(compact('id', 'notas','view',"dorsalJugadores"));
    }
    public function cronicapublic()
    {
        $id = $_POST['id'];
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $juego = array();
        if($id != '') {
            $juego = $this->Juego->find('first', [
                'conditions' => [
                    'Juego.id' => $id,
                ]
            ]);
        }

        $this->set(compact('id', 'juego','view'));
    }
    public function minutoxminutopublic()
    {
        $id = $_POST['id'];
        $render = (isset($_POST['render']))?$_POST['render']:null;
        $view = (isset($_POST['view']))?$_POST['view']:null;
        $eventos = array();
        $juego=[];
        $equipos_local_visitante=[];
        if($id != '') {
            $juego = $this->Juego->find("first",["conditions"=>["Juego.id"=>$_POST["id"]]]);
            $eventos = $this->Juego->Juegoevento->find('all', [
                'conditions' => [
                    'Juegoevento.juego_id' => $id,
                ],
                'order'=> [
                    'Juegoevento.minuto' => 'desc'
                ]
            ]);
            $equipos_local_visitante=[
                $juego["Juego"]["equipo1_id"]=>$juego["Juego"]["equipo1_id"],
                $juego["Juego"]["equipo2_id"]=>$juego["Juego"]["equipo2_id"]];
        }

        $this->loadModel("Evento");
        $meventos = $this->Evento->find('list');

        $this->loadModel('Jugadore');
        $jugadores = $this->Jugadore->find("all",
            [
                "joins"=>[
                    [
                        "table"=>"jugadoresxequipos",
                        "alias"=>"jugadorxequipo",
                        "type"=>"inner",
                        "conditions"=>[
                            "Jugadore.id = jugadorxequipo.jugadore_id"
                        ]
                    ],
                    [
                        "table"=>"equipos",
                        "alias"=>"equipo",
                        "type"=>"inner",
                        "conditions"=>[
                            "equipo.id = jugadorxequipo.equipo_id"
                        ]
                    ]
                ],
                "conditions"=>[
                    "jugadorxequipo.torneo_id"=>$juego["Juego"]["torneo_id"],
                    "jugadorxequipo.equipo_id in "=>$equipos_local_visitante],
                "fields"=>[
                    "Jugadore.id","Jugadore.nombre_jugador","jugadorxequipo.dorsal","equipo.nombrecorto"
                ]
            ]);
        //debug($jugadores);
        $dataJugador = [];
        foreach ($jugadores as $data){
            $dataJugador[$data["Jugadore"]["id"]]["nombre"]=$data["Jugadore"]["nombre_jugador"];
            $dataJugador[$data["Jugadore"]["id"]]["dorsal"]=$data["jugadorxequipo"]["dorsal"];
            $dataJugador[$data["Jugadore"]["id"]]["equipo"]=$data["equipo"]["nombrecorto"];
        }
        $this->set(compact('id', 'eventos', 'meventos', 'jugadores','view','dataJugador'));
        if(!empty($render)) $this->render('minutoxminutotbody');
    }
    public function estadisticaviewpublic(){
        $this->layout=false;
        $data = $this->Juego->find("first",[
            "fields"=>["Juego.id","Equipo1.nombrecorto","Equipo1.fotoescudo","Equipo2.nombrecorto","Equipo2.fotoescudo","Juego.id","Juego.posesioneq1","Juego.posesioneq2",
                "Juego.tirosapuertaeq1","Juego.tirosapuertaeq2","Juego.tirosdeesquinaeq1","Juego.tirosdeesquinaeq2","Juego.saquesdepuertaeq1","Juego.saquesdepuertaeq2",
                "Juego.paseseq1","Juego.paseseq2","Juego.disparosalposteeq1","Juego.disparosalposteeq2","Juego.saquesdebandaeq1","Juego.saquesdebandaeq2","Juego.penalesporeq1",
                "Juego.penalesporeq2","Juego.penalesfalladoseq1","Juego.penalesfalladoseq2"],
            "conditions"=>[
                "Juego.id"=>$_POST["id"]
            ]
        ]);
        $this->set(compact("data"));
    }
}
