<?php
class OrganizacionsController extends AppController {

	var $name = 'Organizacions';
	var $components = array('Acl','Paginator');
	var $helpers = array('Html', 'Form');


	function index() {
		$this->ValidarUsuario('Organizacion', 'organizacions', 'index');
		$this->Organizacion->recursive = 0;

		$this->loadModel('Recurso');
		$titulo = $this->Recurso->find('first', [
			'conditions' => [
				'Recurso.modelo' => 'organizacions'
			]
		]);

		$this->set('organizacion', $this->Organizacion->find('first'));
		$this->set(compact('titulo'));
	}

	function view($id = null) {
        $this->ValidarUsuario('Organizacion', 'organizacions', 'view');
        if (!$this->Organizacion->exists($id)) {
            throw new NotFoundException(__('Invalid organizacion'));
        }
        $options = array('conditions' => array('Organizacion.' . $this->Organizacion->primaryKey => $id));
        $this->set('organizacion', $this->Organizacion->find('first', $options));

		$this->loadModel('Recurso');
		$titulo = $this->Recurso->find('first', [
			'conditions' => [
				'Recurso.modelo' => 'organizacions'
			]
		]);

		$this->set(compact('titulo'));
	}

	function add() {
		$this->ValidarUsuario('Organizacion', 'organizacions', 'add');
		//if($this->Acl->check($this->Session->read('User.username'), 'admin','create')){
			if (!empty($this->data)) {
				$this->Organizacion->create();
				$this->request->data['Organizacion']['usuario'] = $this->Session->read('nombreusuario');
				$this->request->data['Organizacion']['modified']=0;
				if ($this->Organizacion->save($this->data)) {
					$this->flash(__('Organizacion Almacenada.', true), array('action'=>'index'));
				} else {
				}
			}
			$paises = $this->Organizacion->Paise->find('list');
			$tiposocionegocios = $this->Organizacion->Tiposocionegocio->find('list');
			$this->set(compact('paises','tiposocionegocios'));
		/*}
		else{
			$this->flash(__($this->Session->read('User.username').' no tiene privilegios para ver esta opci�n.', true),'../');
		}*/

	}

	function edit($id = null) {
		$this->ValidarUsuario('Organizacion', 'organizacions', 'edit');
			if (!$id && empty($this->data)) {
				$this->flash(__('Organizacion no valida', true), array('action'=>'index'));
			}
			if (!empty($this->data)) {
			   $this->request->data['Organizacion']['usuario_modif'] = $this->Session->read('nombreusuario');
				if ($this->Organizacion->save($this->data)) {
					$this->Session->write("recur_save",1);
					$this->redirect(array('action' => 'index'));
				} else {
				}
			}
			if (empty($this->data)) {
				$this->data = $this->Organizacion->read(null, $id);
			}

		$paises = $this->Organizacion->Paise->find('list');
		$institucions = $this->Organizacion->Institucion->find('list', ['conditions' => ['activo' => 1]]);

		$this->loadModel('Recurso');
		$titulo = $this->Recurso->find('first', [
			'conditions' => [
				'Recurso.modelo' => 'organizacions'
			]
		]);

		$this->set(compact('paises','institucions', 'titulo'));

	}

}
?>