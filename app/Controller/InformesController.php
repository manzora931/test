<?php
class InformesController extends AppController {

	public $components = array('Paginator');//agregado
	var $name = 'Informes';
	var $helpers = array('Html', 'Form');
    /***
     * author: Manuel Anzora
     * create: 26-03-2019
     * description: Metodo para definir que un metodo es publico(no requiere autenticacion de usuario)
     ****/
    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow("indexpublic");
    }
	function index($torneo = null) {
        $this->ValidarUsuario('Informe', 'informes', 'index');
        $this->loadModel("Torneo");
        $this->loadModel("Vregistrogole");
        $this->loadModel("Vgolesxequipo");
        $this->loadModel("Vposesionbalon");
        $this->loadModel("Vequipotaquillero");
        $this->loadModel("Vgolescontra");

        $this->loadModel("Vtirosapuerta");
        $this->loadModel("Vtirosfuera");
        $this->loadModel("Gruposxtorneo");
        $torneos = $this->getTorneos();
        $perfil = $this->getPerfil($this->Session->read("nombreusuario"));
        if($perfil==3 || $perfil==4 || $perfil==5){
            //Filtra solo por los torneos configurados en el usuario
            if(count($torneos)>1){
                $torneos = $this->Torneo->find("list", ["conditions"=>["activo"=>1, "Torneo.id IN"=>$torneos], "order"=>["nombrecorto"=>"asc"]]);
            }else{
                $torneos = $this->Torneo->find("list", ["conditions"=>["activo"=>1, "Torneo.id"=>$torneos], "order"=>["nombrecorto"=>"asc"]]);
            }
        }else{
            $torneos = $this->Torneo->find("list", ["conditions"=>["activo"=>1], "order"=>["nombrecorto"=>"asc"]]);
        }
        $gruposxtorneo = [];
        $tipoTorneo=0;
        if($torneo==null){
            //GOLEADORES
            $goleadores = [];
            //GOLES POR EQUIPO
            $golexequipos = [];
            //POSESION DE BALON
            $posesion = [];
            //TABLA DE POSISIONES
            $tabposision = [];
            //EQUIPO CON MAS TAQUILLA
            $taquileros = [];
            //EQUIPOS MENOS GOLEADOS
            $equiposmenosgoleados = [];
            //TARJETAS AMARILLAS POR EQUIPO
            $tarjetasamarillas = [];
            //TARJETAS ROJAS POR EQUIPO
            $tarjetasrojas = [];
            //TIROS A PUERTA POR EQUIPO
            $tirosapuerta = [];
            //TIROS FUERA
            $tirosfuera = [];
        }else{

            //GOLEADORES
            $goleadores = $this->Vregistrogole->find("all",[
                "conditions"=>[
                    "Vregistrogole.torneo_id"=>$torneo
                ],
                "order"=>["Vregistrogole.goles"=>"desc","jugador"=>"asc"],
                "limit"=>10
            ]);
            //GOLES POR EQUIPO
            $golexequipos = $this->Vgolesxequipo->find("all",[
                "fields"=>["Vgolesxequipo.golafavor",'Vgolesxequipo.equipo'],
                "conditions"=>[
                    "Vgolesxequipo.torneo_id"=>$torneo
                ],
                "order"=>["Vgolesxequipo.golafavor"=>"desc"],
                "limit"=>12
            ]);
            //POSESION BALON
            /*$posesion = $this->Vposesionbalon->find("all",[
                "conditions"=>[
                      "Vposesionbalon.torneo_id"=>$torneo
                ],
                "group"=>["Vposesionbalon.torneo_id","Vposesionbalon.equipo_id","Vposesionbalon.nombrecorto"]
            ]);*/
            $posesion = $this->Vposesionbalon->find("all",[
                "fields"=>[
                    "AVG(posesion) as posesion", "nombrecorto"
                ],
                "conditions"=>[
                    "Vposesionbalon.torneo_id"=>$torneo
                ],
                "group"=>["Vposesionbalon.torneo_id","Vposesionbalon.equipo_id","Vposesionbalon.nombrecorto"],
                "order"=>["AVG(posesion)"=>"desc"]
            ]);
            //con grupos
            $gruposxtorneo = $this->Gruposxtorneo->find("all",
                [
                    "conditions"=> [
                        "Gruposxtorneo.torneo_id" =>$torneo
                    ],
                    "order"=>["Gruposxtorneo.orden"=>"asc"]
                ]
            );
            //TABLA DE POSISIONES

            $getDataPosicion = $this->getPosiciones($torneo);
            $tabposision = $getDataPosicion["datos"];
            //debug($tabposision).die();
            $tipoTorneo = $getDataPosicion["type"];
            //debug($tipoTorneo).die();
            //EQUIPO CON MAS TAQUILLA
            $taquileros = $this->Vequipotaquillero->find("all",
                ['conditions'=>[
                    "Vequipotaquillero.torneo_id"=>$torneo
                ]
            ]);
            //GOLES RECIBIDOS POR EQUIPO
            $equiposmenosgoleados = $this->Vgolescontra->find("all", [
                "fields"=>[
                    "Vgolescontra.equipo", "SUM(Vgolescontra.golesencontra) AS golescontra"
                ],
                "conditions"=>[
                    "Vgolescontra.torneo_id"=>$torneo
                ],
                "order"=>["SUM(Vgolescontra.golesencontra)"=>"DESC"],
                "limit"=>12,
                "group"=>["Vgolescontra.equipo_id"]
            ]);
            //TARJETAS AMARILLAS POR EQUIPO
            $query = "SELECT count(je.id) as tarjetas, je.torneo_id, je.jugador_id, je.juego_id, eq.id as equipo_id, eq.nombrecorto as equipo FROM 
            (SELECT je.id, j.torneo_id, je.jugador_id, j.id as juego_id FROM juegos j 
            inner join juegoeventos je on j.id = je.juego_id
            WHERE j.torneo_id = ".$torneo."  and je.evento_id = 5) je 
            inner join jugadoresxequipos  jeq on jeq.jugadore_id = je.jugador_id and jeq.torneo_id = ".$torneo."
            inner join equipos eq on eq.id = jeq.equipo_id
            group by eq.id ORDER BY count(je.id) DESC LIMIT 12 ";
            $tarjetasamarillas = $this->Torneo->query($query);

            //TARJETAS ROJAS POR EQUIPO
            $query = "SELECT count(je.id) as tarjetas, je.torneo_id, je.jugador_id, je.juego_id, eq.id as equipo_id, eq.nombrecorto as equipo FROM 
            (SELECT je.id, j.torneo_id, je.jugador_id, j.id as juego_id FROM juegos j 
            inner join juegoeventos je on j.id = je.juego_id
            WHERE j.torneo_id = ".$torneo."  and je.evento_id = 6) je 
            inner join jugadoresxequipos  jeq on jeq.jugadore_id = je.jugador_id and jeq.torneo_id = ".$torneo."
            inner join equipos eq on eq.id = jeq.equipo_id
            group by eq.id ORDER BY count(je.id) DESC LIMIT 12 ";
            $tarjetasrojas = $this->Torneo->query($query);

            //TIROS A PUERTA POR EQUIPO
            $tirosapuerta = $this->Vtirosapuerta->find("all", [
                "fields"=>[
                    "Vtirosapuerta.equipo", "SUM(Vtirosapuerta.tiros) as tiros"
                ],
                "conditions"=>[
                    "Vtirosapuerta.torneo_id"=>$torneo
                ],
                "limit"=>12,
                "group"=>[
                    "Vtirosapuerta.equipo_id"
                ],
                "order"=>[
                    "SUM(Vtirosapuerta.tiros)"=>"DESC"
                ]
            ]);
            //TIROS FUERA
            $tirosfuera = $this->Vtirosfuera->find("all", [
                "fields"=>[
                    "Vtirosfuera.equipo", "SUM(Vtirosfuera.tiros) as tiros"
                ],
                "conditions"=>[
                    "Vtirosfuera.torneo_id"=>$torneo
                ],
                "limit"=>12,
                "group"=>[
                    "Vtirosfuera.equipo_id"
                ],
                "order"=>["SUM(Vtirosfuera.tiros)"=>"desc"]
            ]);

        }
        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find("all",[
            "fields"=>["Equipo.id","Equipo.fotoescudo"]
        ]);
        $imgEquipos=[];
        foreach ($equipos as $item){
            $imgEquipos[$item["Equipo"]["id"]]["img"]=$item["Equipo"]["fotoescudo"];
        }
        $this->set(compact('taquileros','torneo','torneos','goleadores','golexequipos','posesion','imgEquipos','tabposision', 'gruposxtorneo'));
        $this->set(compact("tipoTorneo", "equiposmenosgoleados", "tarjetasamarillas", "tarjetasrojas", "tirosapuerta", "tirosfuera"));
	}
    public function vertodos() {
        $this->Session->delete($this->params['controller']);
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }
    public function impresion($torneo = null) {
        $this->layout=false;
        $this->index($torneo);

        $this->loadModel('Organizacion');
        $this->Organizacion->recursive = 0;
        $organizacion = $this->Organizacion->find('first', array('fields' => ['Organizacion.organizacion'],'conditions' => array('Organizacion.id' => 1)));
        $org = $organizacion['Organizacion']['organizacion'];

        $this->set(compact('org'));
    }
	function view($id = null) {
		if (!$id) {
			$this->flash(__('No valido Paise', true), array('action'=>'index'));
		}
		$this->set('paise', $this->Paise->read(null, $id));
		$this->set('departamento', $this->Paise->Departamento->find('first'));
		//$this->set('municipios', $this->Paise->Departamento->Municipio->find('list'));
	}
	function delete($id = null) {
		if (!$id) {
			$this->flash(__('No valido Paise', true), array('action'=>'index'));
		}
		if ($this->Paise->delete($id)) {
			$this->flash(__('Paise deleted', true), array('action'=>'index'));
		}
	}
    function indexpublic($torneo = null,$host) {
        $this->layout="public";
        $this->loadModel("Torneo");
        $this->loadModel("Vregistrogole");
        $this->loadModel("Vgolesxequipo");
        $this->loadModel("Vposesionbalon");
        $this->loadModel("Vequipotaquillero");
        $torneos = $this->Torneo->find("list", ["conditions"=>["activo"=>1], "order"=>["nombrecorto"=>"asc"]]);
        if($torneo==null){
            //GOLEADORES
            $goleadores = [];
            //GOLES POR EQUIPO
            $golexequipos = [];
            //POSESION DE BALON
            $posesion = [];
            //TABLA DE POSISIONES
            $tabposision = [];
            //EQUIPO CON MAS TAQUILLA
            $taquileros = [];

        }else{
            //GOLEADORES
            $goleadores = $this->Vregistrogole->find("all",[
                "conditions"=>[
                    "Vregistrogole.torneo_id"=>$torneo
                ],
                "order"=>["Vregistrogole.goles"=>"desc","jugador"=>"asc"],
                "limit"=>8
            ]);
            //GOLES POR EQUIPO
            $golexequipos = $this->Vgolesxequipo->find("all",[
                "fields"=>["Vgolesxequipo.golafavor",'Vgolesxequipo.equipo'],
                "conditions"=>[
                    "Vgolesxequipo.torneo_id"=>$torneo
                ],
                "order"=>["Vgolesxequipo.golafavor"=>"desc"]
            ]);
            //POSESION BALON
            /*$posesion = $this->Vposesionbalon->find("all",[
                "conditions"=>[
                    "Vposesionbalon.torneo_id"=>$torneo
                ],
                "group"=>["Vposesionbalon.torneo_id","Vposesionbalon.equipo_id","Vposesionbalon.nombrecorto"]
            ]);*/
            $posesion = $this->Vposesionbalon->find("all",[
                "fields"=>[
                    "AVG(posesion) as posesion", "nombrecorto"
                ],
                "conditions"=>[
                    "Vposesionbalon.torneo_id"=>$torneo
                ],
                "group"=>["Vposesionbalon.torneo_id","Vposesionbalon.equipo_id","Vposesionbalon.nombrecorto"]
            ]);
            //TABLA DE POSISIONES
            $tabposision = $this->Vposesionbalon->query("select x.torneo_id, x.equipo_id, x.nombrecorto, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
                            from (
                            select z.torneo_id, z.equipo_id, z.nombrecorto, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
                            from (
                            select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo1_id
                            and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL 
                            select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo2_id
                            and j.marcadore2 > j.marcadore1 AND j.sumapuntos = 1
                            UNION ALL
                            select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo1_id
                            and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                            select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo2_id
                            and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                            select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo1_id
                            and j.marcadore1 < j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                            select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo2_id
                            and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                    ) z 
                    where z.torneo_id = ".$torneo."
                    group by z.torneo_id, z.equipo_id, z.nombrecorto
            ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
            order by puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc");
            //EQUIPO CON MAS TAQUILLA
            $taquileros = $this->Vequipotaquillero->find("all",
                ['conditions'=>[
                    "Vequipotaquillero.torneo_id"=>$torneo
                ]
                ]);
        }
        $this->loadModel("Equipo");
        $equipos = $this->Equipo->find("all",[
            "fields"=>["Equipo.id","Equipo.fotoescudo"]
        ]);
        $imgEquipos=[];
        foreach ($equipos as $item){
            $imgEquipos[$item["Equipo"]["id"]]["img"]=$item["Equipo"]["fotoescudo"];
        }
        $this->set(compact('taquileros','torneo','torneos','goleadores','golexequipos','posesion','imgEquipos','tabposision',"host"));
    }
    /****
     * Author: Manuel Anzora
     * date: 17-06-2019
     * description: Metodo para obtener resultados de tabla de posiciones
     ******/
    public function getPosiciones($torneo_id){
        $this->loadModel("Torneo");
        $this->loadModel("Gruposxtorneo");

        $this->Torneo->recursive=-1;
        $info = $this->Torneo->read("grupos", $torneo_id);
        $gruposxtorneo = $this->Gruposxtorneo->find("all",
            [
                "conditions"=> [
                    "Gruposxtorneo.torneo_id" =>$torneo_id
                ],
                "order"=>["Gruposxtorneo.orden"=>"asc"]
            ]
        );
        $sql="";
        $data=[];
        $i=0;
        ///Verifica si el torneo es por grupos
        if(count($gruposxtorneo)>0){
            $sql = "select x.juego_id, x.torneo_id, x.grupo_id, x.grupo, x.equipo_id, x.nombrecorto, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
                    from (
                        select z.juego_id, z.torneo_id, z.grupo_id, z.grupo, z.equipo_id, z.nombrecorto, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
                        from (
                              select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                                from juegos j, equipos e, torneos t, grupos g
                                where e.id = j.equipo1_id AND t.id = j.torneo_id AND g.id = j.grupo_id 
                                and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL 
                            select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                              from juegos j, equipos e, torneos t, grupos g
                              where e.id = j.equipo2_id AND t.id = j.torneo_id AND g.id = j.grupo_id 
                              and j.marcadore2 > j.marcadore1 AND j.sumapuntos = 1
                        UNION ALL
                            select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
                              from juegos j, equipos e, torneos t, grupos g
                              where e.id = j.equipo1_id AND t.id = j.torneo_id AND g.id = j.grupo_id
                              and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                            select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
                              from juegos j, equipos e, torneos t, grupos g
                              where e.id = j.equipo2_id AND t.id = j.torneo_id AND g.id = j.grupo_id
                              and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                            select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                              from juegos j, equipos e, torneos t, grupos g
                              where e.id = j.equipo1_id AND t.id = j.torneo_id AND g.id = j.grupo_id
                              and j.marcadore1 < j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                            select j.id juego_id, j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                              from juegos j, equipos e, torneos t, grupos g
                              where e.id = j.equipo2_id AND t.id = j.torneo_id AND g.id = j.grupo_id 
                              and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                    ) z   
                    where z.torneo_id = ".$torneo_id." group by z.torneo_id, z.grupo, z.equipo_id
                ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
                INNER JOIN gruposxtorneos gxt on (gxt.torneo_id = x.torneo_id and gxt.grupo_id = x.grupo_id)
                group by x.juego_id
                order by gxt.orden,x.grupo_id, puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc";
            $datos = $this->Torneo->query($sql);
            // debug($datos);
            $grupo = 0;
            foreach ($datos as $item){
                if($item["x"]["grupo_id"] != $grupo){
                    $i=0;
                }
                $data[$item["x"]["grupo_id"]][$i]["equipo_id"]=$item["x"]["equipo_id"];
                $data[$item["x"]["grupo_id"]][$i]["nombrecorto"]=$item["x"]["nombrecorto"];
                $data[$item["x"]["grupo_id"]][$i]["puntosextra"]=$item["pet"]["puntosextra"];
                $data[$item["x"]["grupo_id"]][$i]["puntos"]=$item[0]["puntos"];
                $data[$item["x"]["grupo_id"]][$i]["comentario"]=$item["pet"]["comentario"];
                $data[$item["x"]["grupo_id"]][$i]["partidosjugados"]=$item["x"]["partidosjugados"];
                $data[$item["x"]["grupo_id"]][$i]["partidoganado"]=$item["x"]["partidoganado"];
                $data[$item["x"]["grupo_id"]][$i]["partidoempatado"]=$item["x"]["partidoempatado"];
                $data[$item["x"]["grupo_id"]][$i]["partidoperdido"]=$item["x"]["partidoperdido"];
                $data[$item["x"]["grupo_id"]][$i]["golafavor"]=$item["x"]["golafavor"];
                $data[$item["x"]["grupo_id"]][$i]["golencontra"]=$item["x"]["golencontra"];
                $data[$item["x"]["grupo_id"]][$i]["goldiferencia"]=$item["x"]["goldiferencia"];
                $data[$item["x"]["grupo_id"]][$i]["grupo"]=$item["x"]["grupo"];
                $data[$item["x"]["grupo_id"]][$i]["grupo_id"]=$item["x"]["grupo_id"];
                $grupo = $item["x"]["grupo_id"];
                $i++;
            }
            return ["datos"=>$data, "type"=>2];
        }else{
            if($info["Torneo"]["grupos"]){
                $sql = "select x.torneo_id, x.grupo_id, x.grupo, x.equipo_id, x.nombrecorto, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
                    from (
                        select z.torneo_id, z.grupo_id, z.grupo, z.equipo_id, z.nombrecorto, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
                        from (
                          select j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                            from juegos j, equipos e, grupos g
                            where e.id = j.equipo1_id AND g.id = j.grupo_id AND j.etapa_id = 1
                            and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL 
                        select j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) AS puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                          from juegos j, equipos e, grupos g
                          where e.id = j.equipo2_id AND g.id = j.grupo_id AND j.etapa_id = 1
                          and j.marcadore2 > j.marcadore1 AND j.sumapuntos = 1
                        UNION ALL
                        select j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
                          from juegos j, equipos e, grupos g
                          where e.id = j.equipo1_id AND g.id = j.grupo_id AND j.etapa_id = 1
                          and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                        select j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) AS puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
                          from juegos j, equipos e, grupos g
                          where e.id = j.equipo2_id AND g.id = j.grupo_id AND j.etapa_id = 1
                          and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                        select j.torneo_id, j.grupo_id, g.grupo, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                          from juegos j, equipos e, grupos g
                          where e.id = j.equipo1_id AND g.id = j.grupo_id AND j.etapa_id = 1
                          and j.marcadore1 < j.marcadore2 AND j.sumapuntos = 1
                        UNION ALL
                        select j.torneo_id, j.grupo_id, g.grupo, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                          from juegos j, equipos e, grupos g
                          where e.id = j.equipo2_id AND g.id = j.grupo_id AND j.etapa_id = 1
                          and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                    ) z 
                    where z.torneo_id = ".$torneo_id." group by z.torneo_id, z.grupo, z.equipo_id
                ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
                order by x.grupo_id, puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc";
                $datos = $this->Torneo->query($sql);
                // debug($datos);
                $grupo = 0;
                foreach ($datos as $item){
                    if($item["x"]["grupo_id"] != $grupo){
                        $i=0;
                    }
                    $data[$item["x"]["grupo_id"]][$i]["equipo_id"]=$item["x"]["equipo_id"];
                    $data[$item["x"]["grupo_id"]][$i]["nombrecorto"]=$item["x"]["nombrecorto"];
                    $data[$item["x"]["grupo_id"]][$i]["puntosextra"]=$item["pet"]["puntosextra"];
                    $data[$item["x"]["grupo_id"]][$i]["puntos"]=$item[0]["puntos"];
                    $data[$item["x"]["grupo_id"]][$i]["comentario"]=$item["pet"]["comentario"];
                    $data[$item["x"]["grupo_id"]][$i]["partidosjugados"]=$item["x"]["partidosjugados"];
                    $data[$item["x"]["grupo_id"]][$i]["partidoganado"]=$item["x"]["partidoganado"];
                    $data[$item["x"]["grupo_id"]][$i]["partidoempatado"]=$item["x"]["partidoempatado"];
                    $data[$item["x"]["grupo_id"]][$i]["partidoperdido"]=$item["x"]["partidoperdido"];
                    $data[$item["x"]["grupo_id"]][$i]["golafavor"]=$item["x"]["golafavor"];
                    $data[$item["x"]["grupo_id"]][$i]["golencontra"]=$item["x"]["golencontra"];
                    $data[$item["x"]["grupo_id"]][$i]["goldiferencia"]=$item["x"]["goldiferencia"];
                    $data[$item["x"]["grupo_id"]][$i]["grupo"]=$item["x"]["grupo"];
                    $data[$item["x"]["grupo_id"]][$i]["grupo_id"]=$item["x"]["grupo_id"];
                    $grupo = $item["x"]["grupo_id"];
                    $i++;
                }
                return ["datos"=>$data, "type"=>1];
            }else{
                $sql = "select x.torneo_id,x.equipo_id, x.nombrecorto, x.partidosjugados, if(pet.puntos is not null, (x.puntos + pet.puntos), x.puntos) as puntos, x.partidoganado, x.partidoempatado, x.partidoperdido, x.golafavor, x.golencontra, x.goldiferencia, pet.puntos as puntosextra, pet.comentario
                    from (
                      select z.torneo_id, z.equipo_id, z.nombrecorto, sum(z.partidojugado) partidosjugados, sum(z.puntos) puntos, sum(z.partidoganado) partidoganado, sum(z.partidoempatado) partidoempatado, sum(z.partidoperdido) partidoperdido, sum(z.golafavor) golafavor, sum(z.golencontra) golencontra, sum(z.goldiferencia) goldiferencia
                        from (
                            select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) as puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                            from juegos j, equipos e 
                            where e.id = j.equipo1_id
                            and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL 
                                select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 3, 0) as puntos, 1 partidoganado, 0 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                                from juegos j, equipos e 
                                where e.id = j.equipo2_id
                                and j.marcadore2 > j.marcadore1 AND j.sumapuntos = 1
                            UNION ALL
                                select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) as puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, 0 goldiferencia
                                from juegos j, equipos e 
                                where e.id = j.equipo1_id
                                and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                                select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, if(j.sumapuntos = 1, 1, 0) as puntos, 0 partidoganado, 1 partidoempatado, 0 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, 0 goldiferencia
                                from juegos j, equipos e 
                                where e.id = j.equipo2_id
                                and j.marcadore1 = j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                                select j.torneo_id, j.equipo1_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore1 golafavor, j.marcadore2 golencontra, (j.marcadore1-j.marcadore2) goldiferencia
                                from juegos j, equipos e 
                                where e.id = j.equipo1_id
                                and j.marcadore1 < j.marcadore2 AND j.sumapuntos = 1
                            UNION ALL
                                select j.torneo_id, j.equipo2_id equipo_id, e.nombrecorto, 1 partidojugado, 0 puntos, 0 partidoganado, 0 partidoempatado, 1 partidoperdido, j.marcadore2 golafavor, j.marcadore1 golencontra, (j.marcadore2-j.marcadore1) goldiferencia
                                from juegos j, equipos e 
                                where e.id = j.equipo2_id
                                and j.marcadore1 > j.marcadore2 AND j.sumapuntos = 1
                ) z 
            where z.torneo_id = ".$torneo_id."
            group by z.torneo_id, z.equipo_id, z.nombrecorto
            ) x LEFT JOIN puntosequiposxtorneos pet on (x.torneo_id= pet.torneo_id AND x.equipo_id = pet.equipo_id and pet.activo=1)
        order by puntos desc, x.goldiferencia desc, x.golafavor desc, x.nombrecorto asc";
                $datos = $this->Torneo->query($sql);

                return ["datos"=>$datos, "type"=>0];
            }
        }
    }
}
?>