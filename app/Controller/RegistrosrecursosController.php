<?php
App::uses('AppController', 'Controller');
/**
 * Registrosrecursos Controller
 *
 * @property Registrosrecurso $Registrosrecurso
 * @property PaginatorComponent $Paginator
 */
class RegistrosrecursosController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('Js');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Registrosrecurso", "registrosrecursos", "index");
        $this->Paginator->settings = array('conditions' => array('Registrosrecurso.activo' => 1),'order'=>array('Registrosrecurso.tabla'=>'asc'));
		$this->Registrosrecurso->recursive = 0;
        include 'busqueda/registrosrecursos.php';
        $this->set('registrosrecursos', $this->Paginator->paginate());
        $modulos=$this->Registrosrecurso->Recurso->Modulo->find("list");
        $this->set(compact('modulos'));
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[registrosrecursos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Registrosrecurso", "registrosrecursos", "view");
		if (!$this->Registrosrecurso->exists($id)) {
			throw new NotFoundException(__('Invalid registrosrecurso'));
		}
		$options = array('conditions' => array('Registrosrecurso.' . $this->Registrosrecurso->primaryKey => $id));
		$this->set('registrosrecurso', $this->Registrosrecurso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Registrosrecurso", "registrosrecursos", "add");
		if ($this->request->is('post')) {
			$this->Registrosrecurso->create();
			$this->request->data['Registrosrecurso']['usuario'] = $this->Session->read('nombreusuario');
			if ($this->Registrosrecurso->save($this->request->data)) {
				$id = $this->Registrosrecurso->id;
				$this->Session->write('recursospantalla_save', 1);
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('<script> alert("No se pudo almacenar el Recurso de pantalla"); </script>'));
			}
		}
		$modulos=$this->Registrosrecurso->Recurso->Modulo->find("list");
		$this->set(compact('modulos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Registrosrecurso", "registrosrecursos", "edit");
		if (!$this->Registrosrecurso->exists($id)) {
			throw new NotFoundException(__('Recurso de pantalla no valido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Registrosrecurso']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Registrosrecurso->save($this->request->data)) {
				$this->Session->write('recursospantalla_save', 1);
				$this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('<script> alert("No se pudo editar el Recurso de pantalla"); </script>'));
			}
		} else {
			$options = array('conditions' => array('Registrosrecurso.' . $this->Registrosrecurso->primaryKey => $id));
			$this->request->data = $this->Registrosrecurso->find('first', $options);
		}
		$modulos=$this->Registrosrecurso->Recurso->Modulo->find("list");
		$this->set(compact('modulos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Registrosrecurso", "registrosrecursos", "delete");
		if ($delete == true) {
			$this->Registrosrecurso->id = $id;
			if (!$this->Registrosrecurso->exists()) {
				throw new NotFoundException(__('Invalid registrosrecurso'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Registrosrecurso->delete()) {
					$this->Session->setFlash(__('The registrosrecurso has been deleted.'));
			} else {
				$this->Session->setFlash(__('The registrosrecurso could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}
	public function val_table() {
		$valor=$_POST['val'];
		$id =$_POST['id'];
		if($id != 0){
			$datos=$this->Registrosrecurso->query("SELECT tabla FROM registrosrecursos WHERE tabla='$valor' AND id !=".$id);
		}else{
			$datos=$this->Registrosrecurso->query("SELECT tabla FROM registrosrecursos WHERE tabla='$valor'");
		}
		if(count($datos)>0){
			echo "error";
		}else{
			echo "ok";
		}
		$this->autoRender=false;
	}
	public function recursospantalla(){
		$registrosrecurso = $this->Registrosrecurso->find("list",array('conditions'=>array('Registrosrecurso.recurso_id'=>$_POST['idRec'])));
		$select="<option value=''>< Seleccionar ></option>";
		foreach($registrosrecurso as $key => $value)
		{
			$select .= "<option value='$key'>$value</option>";
		}
		echo  $select;
		$this->autoRender = false;
	}

}
