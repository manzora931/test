<?php
App::uses('AppController', 'Controller');
/**
 * Modulos Controller
 *
 * @property Modulo $Modulo
 * @property PaginatorComponent $Paginator
 */
class ModulosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		#$this->Modulo->recursive = 0;
		#$this->set('modulos', $this->Paginator->paginate());
		$this->ValidarUsuario('Modulo', 'modulos', 'index');
   		$this->Paginator->settings = array('conditions' => array('Modulo.activo >=' => 1),'order'=>array('Modulo.modulo'=>'asc'));
		$this->Modulo->recursive = 0;
		include 'busqueda/modulos.php';
		$data = $this->Paginator->paginate('Modulo');
		$this->set('modulos', $data);
	}

	function vertodos(){
    	$this->Session->delete($this->params['controller']);
		$this->Session->delete('tabla[modulos]');
		$this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
		$this->autoRender=false;
    }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario('Modulo', 'modulos', 'view');
		if (!$this->Modulo->exists($id)) {
			throw new NotFoundException(__('Invalid modulo'));
		}
		$options = array('conditions' => array('Modulo.' . $this->Modulo->primaryKey => $id));
		$this->set('modulo', $this->Modulo->find('first', $options));

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario('Modulo', 'modulos', 'add');
		if ($this->request->is('post')) {
			$this->Modulo->create();
			$this->request->data['Modulo']['usuario'] = $this->Session->read('nombreusuario');
			$this->request->data['Modulo']['modified']=0;
			if ($this->Modulo->save($this->request->data)) {
				$modulo_id = $this->Modulo->id;
				$this->Session->write('modulo_save', 1);
				$this->redirect(['action' => 'view', $modulo_id]);
			} else {
				$this->Session->setFlash(__('El módulo NO ha sido creado. Intentar de nuevo.'));
			}
		}
        $organizacions = $this->Modulo->Organizacion->find("list",["conditions"=>['Organizacion.activa'=>1]]);
		$this->set(compact("organizacions"));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario('Modulo', 'modulos', 'edit');
		if (!$this->Modulo->exists($id)) {
			throw new NotFoundException(__('Módulo Inválido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Modulo']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Modulo->save($this->request->data)) {
				$this->Session->write('modulo_save', 1);
				$this->redirect(['action' => 'view', $id]);
			}
		} else {
			$options = array('conditions' => array('Modulo.' . $this->Modulo->primaryKey => $id));
			$this->request->data = $this->Modulo->find('first', $options);

		}

        $organizacions = $this->Modulo->Organizacion->find("list",["conditions"=>['Organizacion.activa'=>1]]);
        $this->set(compact("organizacions"));
	}

	public function val_mod() {
		$value = $_POST['val'];

		if ($_POST['id'] == 0) {
			$sql = $this->Modulo->query("SELECT modulo FROM modulos where modulo = '$value'");
		} else {
			$sql = $this->Modulo->query("SELECT modulo FROM modulos where modulo = '$value' AND id != ".$_POST['id']);
		}



		if(count($sql)>0){
			echo "error";
		}else{
			echo "ok";
		}


		$this->autoRender=false;
	}
}
