<?php
App::uses('AppController', 'Controller');
/**
 * Tipotorneos Controller
 *
 * @property Tipoequipo $Tipoequipo
 * @property PaginatorComponent $Paginator
 */
class TipoequiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Tipoequipo", "tipoequipos", "index");
        $this->Paginator->settings = array('conditions' => array('Tipoequipo.activo >=' => 1), 'order'=>array('Tipoequipo.tipoequipo'=>'asc'));
        $this->Tipoequipo->recursive = 0;
        include 'busqueda/tipoequipos.php';
        $data = $this->Paginator->paginate('Tipoequipo');
        $this->set('tipoequipos', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[tipoequipos]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Tipoequipo", "tipoequipos", "view");
		if (!$this->Tipoequipo->exists($id)) {
			throw new NotFoundException(__('Invalid tipoequipo'));
		}
		$options = array('conditions' => array('Tipoequipo.' . $this->Tipoequipo->primaryKey => $id));
		$this->set('tipoequipo', $this->Tipoequipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Tipoequipo", "tipoequipos", "add");
		if ($this->request->is('post')) {
			$this->Tipoequipo->create();
            $this->request->data['Tipoequipo']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Tipoequipo']['modified']=0;
			if ($this->Tipoequipo->save($this->request->data)) {
				$tipoequipo_id = $this->Tipoequipo->id;
                $this->Session->write('tipoequipo_save', 1);
                $this->redirect(['action' => 'view', $tipoequipo_id]);
			} else {
				$this->Session->setFlash(__('The tipoequipo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Tipoequipo", "tipoequipos", "edit");
		if (!$this->Tipoequipo->exists($id)) {
			throw new NotFoundException(__('Invalid tipoequipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Tipoequipo']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Tipoequipo->save($this->request->data)) {
                $this->Session->write('tipoequipo_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('The tipoequipo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipoequipo.' . $this->Tipoequipo->primaryKey => $id));
			$this->request->data = $this->Tipoequipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Tipoequipo", "tipoequipos", "delete");
		if ($delete == true) {
			$this->Tipoequipo->id = $id;
			if (!$this->Tipoequipo->exists()) {
				throw new NotFoundException(__('Invalid tipoequipo'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Tipoequipo->delete()) {
					$this->Session->setFlash(__('The tipoequipo has been deleted.'));
			} else {
				$this->Session->setFlash(__('The tipoequipo could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_tipoequipos() {
        $id = $_POST['id'];
        $data = array();

        // $this->loadModel('Equipo');
        $this->Tipoequipo->Equipo->recursive = -1;
        $tipo = $this->Tipoequipo->Equipo->find('all', [
            'conditions' => [
                'Equipo.tipoequipo_id' => $id,
            ]
        ]);

        $data['tipos'] = (count($tipo) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
