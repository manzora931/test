<?php
App::uses('AppController', 'Controller');
/**
 * Financistas Controller
 *
 * @property Financista $Financista
 * @property PaginatorComponent $Paginator
 */
class FinancistasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ValidarUsuario("Financista", "financistas", "index");
		$this->Paginator->settings = array('conditions' => array('Financista.activo >=' => 1), 'order'=>array('Financista.nombre'=>'asc'));
        $this->Financista->recursive = 0;
        include 'busqueda/financistas.php';
        $data = $this->Paginator->paginate('Financista');
        $this->set('financistas', $data);
	}

    function vertodos(){
        $this->Session->delete($this->params['controller']);
        $this->Session->delete('tabla[financistas]');
        $this->redirect(array('controller'=>$this->params['controller'],'action'=> "index"));
        $this->autoRender=false;
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ValidarUsuario("Financista", "financistas", "view");
		if (!$this->Financista->exists($id)) {
			throw new NotFoundException(__('Invalid financista'));
		}
		$options = array('conditions' => array('Financista.' . $this->Financista->primaryKey => $id));
		$this->set('financista', $this->Financista->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->ValidarUsuario("Financista", "financistas", "add");
		if ($this->request->is('post')) {
			$this->Financista->create();

            $this->request->data['Financista']['usuario'] = $this->Session->read('nombreusuario');
            $this->request->data['Financista']['modified']=0;
			if ($this->Financista->save($this->request->data)) {
                $financista_id = $this->Financista->id;
                $this->Session->write('financista_save', 1);
                $this->redirect(['action' => 'view', $financista_id]);
			} else {
				$this->Session->setFlash(__('La fuente de financiamiento no ha sido creada. Intentar de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ValidarUsuario("Financista", "financistas", "edit");
		if (!$this->Financista->exists($id)) {
			throw new NotFoundException(__('Invalid financista'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Financista']['usuariomodif'] = $this->Session->read('nombreusuario');
			if ($this->Financista->save($this->request->data)) {
                $this->Session->write('financista_save', 1);
                $this->redirect(['action' => 'view', $id]);
			} else {
				$this->Session->setFlash(__('La fuente de financiamiento no ha sido actualizado. Intentar de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Financista.' . $this->Financista->primaryKey => $id));
			$this->request->data = $this->Financista->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$delete = $this->ValidarUsuario("Financista", "financistas", "delete");
		if ($delete == true) {
			$this->Financista->id = $id;
			if (!$this->Financista->exists()) {
				throw new NotFoundException(__('Invalid financista'));
			}
			$this->request->onlyAllow('post', 'delete');
			if ($this->Financista->delete()) {
					$this->Session->setFlash(__('The financista has been deleted.'));
			} else {
				$this->Session->setFlash(__('The financista could not be deleted. Please, try again.'));
			}
			return $this->redirect(array('action' => 'index'));
			}
	}

    public function verify_proyectos() {
        $id = $_POST['id'];
        $data = array();

        $this->loadModel('Fuentesfinanciamiento');
        $this->Fuentesfinanciamiento->recursive = -1;
        $fuente = $this->Fuentesfinanciamiento->find('all', [
            'conditions' => [
                'Fuentesfinanciamiento.financista_id' => $id,
            ]
        ]);

        $data['fuentes'] = (count($fuente) > 0);

        echo json_encode($data);
        $this->autoRender=false;
    }
}
