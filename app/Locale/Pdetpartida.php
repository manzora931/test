<?php
App::uses('AppModel', 'Model');
/**
 * Pdetpartida Model
 *
 * @property Ppartida $Ppartida
 * @property Vcuenta $Vcuenta
 * @property Proyecto $Proyecto
 */
class Pdetpartida extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Ppartida' => array(
			'className' => 'Ppartida',
			'foreignKey' => 'ppartida_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vcuenta' => array(
			'className' => 'Vcuenta',
			'foreignKey' => 'vcuenta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proyecto' => array(
			'className' => 'Proyecto',
			'foreignKey' => 'proyecto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
